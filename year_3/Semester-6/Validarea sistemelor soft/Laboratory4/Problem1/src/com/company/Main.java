package com.company;

import controller.doctorController;
import persistence.Repository;
import view.doctorView;

public class Main {

    public static void main(String[] args) {
        Repository rep = new Repository("pat.txt", "cons.txt");
        doctorController dc = new doctorController(rep);
        doctorView view = new doctorView(dc);
        view.EntryPoint();
    }
}
