import controller.doctorController;
import model.Consultation;
import model.Patient;
import org.junit.Before;
import org.junit.Test;
import persistence.IRepository;
import utils.CustomException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by Cipri on 4/25/2016.
 */
public class Controller_TopDown {
    private doctorController _controller;

    @Before
    public void TestInitialize() {
        IRepository repository = new MockRepository();
        _controller = new doctorController(repository);
    }
    //A
    @Test
    public void TC_01_FamilyDoctor() throws CustomException {
        Patient expectedPatient = new Patient();
        expectedPatient.setSSN("2");
        expectedPatient.setAddress("cuca macaii");
        expectedPatient.setName("gigel");
        try {
            _controller.addPatient(expectedPatient);
            Patient actualPatient = _controller.getPatientBySSN(expectedPatient.getSSN());
            assertEquals(expectedPatient, actualPatient);
        } catch (CustomException e) {
            e.printStackTrace();
        }
    }

    //A,B
    @Test
    public void TC_02_FamilyDoctor() {
        Patient simplePatient = new Patient();
        simplePatient.setSSN("1");
        simplePatient.setAddress("cuca macaii");
        simplePatient.setName("gigel");
        Consultation expectedConsultation = new Consultation();
        expectedConsultation.setConsID("1");
        expectedConsultation.setPatientSSN("1");
        expectedConsultation.setConsultation_date(new Date("18/03/2016"));
        expectedConsultation.setDiag("test");
        List<String> exptectedMeds = new ArrayList<>();
        exptectedMeds.add("test");
        expectedConsultation.setMeds(exptectedMeds);
        try {
            _controller.addPatient(simplePatient);
        } catch (CustomException e) {
            e.printStackTrace();
        }
        try {
            _controller.addConsultation(expectedConsultation);
        } catch (CustomException e) {
            e.printStackTrace();
        }
        Consultation actualConsultation = _controller.getConsByID(expectedConsultation.getConsID());
        assertEquals(expectedConsultation,actualConsultation);
    }

    //A,B,C
    @Test
    public void TC_03_FamilyDoctor() throws CustomException {
        Patient simplePatient = new Patient();
        simplePatient.setSSN("1");
        simplePatient.setAddress("cuca macaii");
        simplePatient.setName("gigel");

        Consultation expectedConsultation = new Consultation();
        expectedConsultation.setConsID("2");
        expectedConsultation.setPatientSSN("1");
        expectedConsultation.setConsultation_date(new Date("18/03/2016"));
        expectedConsultation.setDiag("test");
        List<String> exptectedMeds = new ArrayList<>();
        exptectedMeds.add("test");
        expectedConsultation.setMeds(exptectedMeds);

        _controller.addPatient(simplePatient);
        _controller.addConsultation(expectedConsultation);

        String givenDisease = "test";
        List<Patient> actualList = _controller.getPatientsWithDisease(givenDisease);
        assertEquals(1, actualList.size());
    }
}
