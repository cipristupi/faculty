package persistence;

import model.Consultation;
import model.Patient;

import java.io.IOException;
import java.util.List;

/**
 * Created by Cipri on 3/30/2016.
 */
public interface IRepository {
    void cleanFiles(String fileName);

    String[] getPatientsFromFile() throws IOException;

    String[] getConsultationsFromFile() throws IOException;

    List<Patient> getPatientList();

    List<Consultation> getConsultationList();

    void savePatientToFile(List<Patient> patientsList) throws IOException;


    void saveConsultationToFile(List<Consultation> consultationList) throws IOException;
}
