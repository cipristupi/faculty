package model;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Consultation {
    private String consID;
    private String PatientSSN;
    private String diag;
    private List<String> meds;
    private Date consultation_date;

    public Consultation() {
        this.consID = new String();
        this.PatientSSN = new String();
        this.diag = new String();
        this.meds = new ArrayList<String>();
        this.consultation_date = new Date();
    }

    ;

    public Consultation(String consID, String PatientSSN, String diag, List<String> meds, Date date) {
        this.consID = consID;
        this.PatientSSN = PatientSSN;
        this.diag = diag;
        this.meds = meds;
        this.consultation_date = date;
    }

    public String getConsID() {
        return consID;
    }

    public void setConsID(String v_consID) {
        consID = v_consID;
    }

    public String getPatientSSN() {
        return PatientSSN;
    }

    public void setPatientSSN(String patientSSN) {
        PatientSSN = patientSSN;
    }

    public String getDiag() {
        return diag;
    }

    public void setDiag(String diag) {
        this.diag = diag;
    }

    public List<String> getMeds() {
        return meds;
    }

    public void setMeds(List<String> meds) {
        this.meds = meds;
    }

    public Date getConsultation_date() {
        return consultation_date;
    }

    public void setConsultation_date(Date consultation_date) {
        this.consultation_date = consultation_date;
    }

    public String toString() {
        String res;
        res = consID + "," + PatientSSN + ',' + diag + ",";

        for (int i = 0; i < meds.size(); i++) {
            res = res + meds.get(i) + "+";
        }

        res = res.substring(0, res.length() - 1);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        res = res + "," + simpleDateFormat.format(consultation_date);

        return res;

    }

}
