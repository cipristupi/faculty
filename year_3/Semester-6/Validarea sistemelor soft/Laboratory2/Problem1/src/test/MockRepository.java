package test;

import model.Consultation;
import model.Patient;
import persistence.IRepository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Cipri on 3/30/2016.
 */
public class MockRepository implements IRepository {
    private List<Patient> patients;
    private List<Consultation> consultations;

    public MockRepository() {
        patients = new ArrayList<>();
        consultations = new ArrayList<>();
    }

    @Override
    public void cleanFiles(String fileName) {

    }

    @Override
    public String[] getPatientsFromFile() throws IOException {
        return new String[0];
    }

    @Override
    public String[] getConsultationsFromFile() throws IOException {
        return new String[0];
    }

    @Override
    public List<Patient> getPatientList() {
        return patients;
    }

    @Override
    public List<Consultation> getConsultationList() {
        return consultations;
    }

    @Override
    public void savePatientToFile(List<Patient> patientsList) throws IOException {
        patients = patientsList;
    }

    @Override
    public void saveConsultationToFile(List<Consultation> consultationList) throws IOException {
        consultations = consultationList;
    }
}
