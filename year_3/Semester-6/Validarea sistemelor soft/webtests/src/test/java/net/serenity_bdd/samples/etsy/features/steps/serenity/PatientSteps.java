package net.serenity_bdd.samples.etsy.features.steps.serenity;

import models.Patient;
import net.serenity_bdd.samples.etsy.pages.PatientPage;
import net.thucydides.core.annotations.Step;

/**
 * Created by Cipri on 5/15/2016.
 */
public class PatientSteps {

    PatientPage patientPage;
//    public PatientSteps(){
//        patientPage = new PatientPage();
//    }
    @Step                                                       // <2>
    public void opens_home_page() {
        patientPage.open();
    }

    @Step
    public void fill_up_inputs(Patient validPatient) {
        patientPage.addPatient(validPatient.getName(),validPatient.getSSN(),validPatient.getAddress());
    }

    @Step
    public void savePatient() {
        patientPage.savePatient();
    }

    @Step
    public String getMessage() {
        return patientPage.getMessage();
    }
}
