package net.serenity_bdd.samples.etsy.features;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        plugin = {"pretty" ,"html:target/site/serenity"},
//        format = {"html:target/site/serenity"},
        features="src/test/resources/features")
public class AcceptanceTests {}
