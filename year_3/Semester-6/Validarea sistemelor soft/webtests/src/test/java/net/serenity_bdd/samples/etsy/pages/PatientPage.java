package net.serenity_bdd.samples.etsy.pages;

import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by Cipri on 5/15/2016.
 */
@DefaultUrl("http://localhost:8080/FamiliyDoctor1/")
public class PatientPage extends PageObject {

    @FindBy(xpath = "/html/body/form/input[4]")
    WebElement submitButton;

    @FindBy(xpath = "/html/body/form/input[1]")
    WebElement nameInput;

    @FindBy(xpath = "/html/body/form/input[2]")
    WebElement ssnInput;


    @FindBy(xpath = "/html/body/form/input[3]")
    WebElement addressInput;

    @FindBy(xpath = "//*[@id=\"message\"]")
    WebElement messageLabel;

    public void addPatient(String name,String ssn,String address) {
        nameInput.sendKeys(name);
        ssnInput.sendKeys(ssn);
        addressInput.sendKeys(address);
    }
    public void savePatient(){
        submitButton.click();
    }

    public String getMessage() {
        return messageLabel.getText();
    }
}
