package net.serenity_bdd.samples.etsy.features.steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import models.Patient;
import net.serenity_bdd.samples.etsy.features.steps.serenity.PatientSteps;
import net.thucydides.core.annotations.Steps;

import java.util.Random;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Cipri on 5/15/2016.
 */
public class PatientStepDefinition {

    @Steps
    PatientSteps patientSteps;

    Patient patient;

    // end::header[]
    // tag::woolscenario[]
    @Given("^I add a new patient$")
    public void IAddANewPatient() {
        Random random = new Random();
        String ssn = String.valueOf(random.nextInt(Integer.MAX_VALUE));
        patientSteps.opens_home_page();
        patient = new Patient("Patient", ssn, "Address" + ssn);
        patientSteps.fill_up_inputs(patient);
    }

    @When("^I save the patient$")
    public void iSaveThePatient() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        patientSteps.savePatient();
    }

    @Then("^I should see the success message$")
    public void iShouldSeeTheSuccessMessage() throws Throwable {
        String message = patientSteps.getMessage();
        assertThat(message).isEqualTo("Patient created "+patient.getName());
    }

    @Given("^I add a new patient  with invalid$")
    public void iAddANewPatientWithInvalid() throws Throwable {
        patientSteps.opens_home_page();
        patient = new Patient("", "", "");
        patientSteps.fill_up_inputs(patient);
    }

    @Then("^I should see the error message$")
    public void iShouldSeeTheErrorMessage() throws Throwable {
        String message = patientSteps.getMessage();
        assertThat(message).isEqualTo("Name must be not empty, Address must be not empty, SSN must be not empty");
    }
}
