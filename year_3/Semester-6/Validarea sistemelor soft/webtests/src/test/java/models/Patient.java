package models;

/**
 * Created by Cipri on 5/15/2016.
 */
public class Patient {
    private String Name;
    private String SSN;
    private String address;

    /** Constructors */

    public Patient() {};

    public Patient(String Name, String SSN, String address)
    {
        this.Name = Name;
        this.SSN = SSN;
        this.address = address;
    }


    /** Getters and setters */

    public String getName() {
        return Name;
    }
    public void setName(String name) {
        Name = name;
    }
    public String getSSN() {
        return SSN;
    }
    public void setSSN(String sSN) {
        SSN = sSN;
    }
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }

    //new version
    public String toString() {
        return Name + "," + SSN + "," +address;
    }
}
