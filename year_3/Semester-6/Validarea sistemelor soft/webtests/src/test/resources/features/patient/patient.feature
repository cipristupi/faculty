# tag::header[]
Feature: Add a new patient
  In add a new patient to the system

# end::header[]
# tag::woolscenario[]
  Scenario: Add a new patient with valid data
    Given I add a new patient
    When I save the patient
    Then I should see the success message

# end::woolscenario[]
# tag::handmadescenario[]
  Scenario: Add a new patient with invalid data
    Given I add a new patient  with invalid
    When I save the patient
    Then I should see the error message
