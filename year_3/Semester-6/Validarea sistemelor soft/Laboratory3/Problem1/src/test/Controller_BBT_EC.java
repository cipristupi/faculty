package test;

import controller.doctorController;
import model.Consultation;
import model.Patient;
import org.junit.Before;
import org.junit.Test;
import persistence.IRepository;
import utils.CustomException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by Cipri on 3/30/2016.
 */
public class Controller_BBT_EC {

    private doctorController _controller;

    @Before
    public void TestInitialize() {
        IRepository repository = new MockRepository();
        _controller = new doctorController(repository);
    }

    @Test
    public void TC_EC_01_FamilyDoctor() {
        Patient expectedPatient = new Patient();
        expectedPatient.setSSN("1");
        expectedPatient.setAddress("cuca macaii");
        expectedPatient.setName("gigel");
        try {
            _controller.addPatient(expectedPatient);
            Patient actualPatient = _controller.getPatientBySSN(expectedPatient.getSSN());
            assertEquals(expectedPatient, actualPatient);
        } catch (CustomException e) {
            e.printStackTrace();
        }
    }

    @Test(expected = CustomException.class)
    public void TC_EC_02_FamilyDoctor() throws CustomException {
        Patient expectedPatient = new Patient();
        expectedPatient.setSSN("1");
        expectedPatient.setAddress("test");
        expectedPatient.setName("");
        _controller.addPatient(expectedPatient);
    }

    @Test(expected = CustomException.class)
    public void TC_EC_03_FamilyDoctor() throws CustomException {
        Patient expectedPatient = new Patient();
        expectedPatient.setSSN("1");
        expectedPatient.setAddress("test");
        expectedPatient.setName("test123");
        _controller.addPatient(expectedPatient);
    }

    @Test(expected = CustomException.class)
    public void TC_EC_04_FamilyDoctor() throws CustomException {
        Patient expectedPatient = new Patient();
        expectedPatient.setSSN("1");
        expectedPatient.setAddress("");
        expectedPatient.setName("test");
        _controller.addPatient(expectedPatient);
    }

    @Test(expected = CustomException.class)
    public void TC_EC_05_FamilyDoctor() throws CustomException {
        Patient expectedPatient1 = new Patient();
        Patient expectedPatient = new Patient();
        expectedPatient1.setSSN("1");
        expectedPatient1.setAddress("cuca macaii");
        expectedPatient1.setName("gigel");

        expectedPatient.setSSN("1");
        expectedPatient.setAddress("test");
        expectedPatient.setName("test");
        _controller.addPatient(expectedPatient1);
        _controller.addPatient(expectedPatient);
    }

    @Test(expected = CustomException.class)
    public void TC_EC_06_FamilyDoctor() throws CustomException {
        Patient expectedPatient = new Patient();
        expectedPatient.setSSN("");
        expectedPatient.setAddress("test");
        expectedPatient.setName("test");
        _controller.addPatient(expectedPatient);
    }

    @Test(expected = CustomException.class)
    public void TC_EC_07_FamilyDoctor() throws CustomException {
        Patient expectedPatient = new Patient();
        expectedPatient.setSSN("test");
        expectedPatient.setAddress("test");
        expectedPatient.setName("test");
        _controller.addPatient(expectedPatient);
    }


    @Test
    public void TC_EC_8_FamilyDoctor() {
        Patient simplePatient = new Patient();
        simplePatient.setSSN("1");
        simplePatient.setAddress("cuca macaii");
        simplePatient.setName("gigel");
        Consultation expectedConsultation = new Consultation();
        expectedConsultation.setConsID("1");
        expectedConsultation.setPatientSSN("1");
        expectedConsultation.setConsultation_date(new Date("18/03/2016"));
        expectedConsultation.setDiag("raceala");
        List<String> exptectedMeds = new ArrayList<>();
        exptectedMeds.add("paracetamol");
        expectedConsultation.setMeds(exptectedMeds);
        try {
            _controller.addPatient(simplePatient);
            _controller.addConsultation(expectedConsultation);
            Consultation actualConsultation = _controller.getConsByID(expectedConsultation.getConsID());
            assertEquals(expectedConsultation, actualConsultation);
        } catch (CustomException e) {
            e.printStackTrace();
        }
    }

    @Test(expected = CustomException.class)
    public void TC_EC_9_FamilyDoctor() throws CustomException {
        Patient simplePatient = new Patient();
        simplePatient.setSSN("1");
        simplePatient.setAddress("cuca macaii");
        simplePatient.setName("gigel");
        Consultation expectedConsultation = new Consultation();
        expectedConsultation.setConsID("1");
        expectedConsultation.setPatientSSN("11");
        expectedConsultation.setConsultation_date(new Date("18/03/2016"));
        expectedConsultation.setDiag("d");
        List<String> exptectedMeds = new ArrayList<>();
        exptectedMeds.add("test");
        expectedConsultation.setMeds(exptectedMeds);
        _controller.addPatient(simplePatient);
        _controller.addConsultation(expectedConsultation);
    }


    @Test(expected = CustomException.class)
    public void TC_EC_10_FamilyDoctor() throws CustomException {
        Patient simplePatient = new Patient();
        simplePatient.setSSN("1");
        simplePatient.setAddress("cuca macaii");
        simplePatient.setName("gigel");
        Consultation expectedConsultation = new Consultation();
        expectedConsultation.setConsID("1");
        expectedConsultation.setPatientSSN("");
        expectedConsultation.setConsultation_date(new Date("18/03/2016"));
        expectedConsultation.setDiag("d");
        List<String> exptectedMeds = new ArrayList<>();
        exptectedMeds.add("test");
        expectedConsultation.setMeds(exptectedMeds);
        _controller.addPatient(simplePatient);
        _controller.addConsultation(expectedConsultation);
    }


    @Test(expected = CustomException.class)
    public void TC_EC_11_FamilyDoctor() throws CustomException {
        Patient simplePatient = new Patient();
        simplePatient.setSSN("1");
        simplePatient.setAddress("cuca macaii");
        simplePatient.setName("gigel");
        Consultation expectedConsultation = new Consultation();
        Consultation existingConsultations = new Consultation();

        existingConsultations.setConsID("1");
        existingConsultations.setPatientSSN("1");
        existingConsultations.setConsultation_date(new Date("18/03/2016"));
        existingConsultations.setDiag("d");
        List<String> existingConsultationsMeds = new ArrayList<>();
        existingConsultationsMeds.add("test");
        expectedConsultation.setMeds(existingConsultationsMeds);


        expectedConsultation.setConsID("1");
        expectedConsultation.setPatientSSN("1");
        expectedConsultation.setConsultation_date(new Date("18/03/2016"));
        expectedConsultation.setDiag("d");
        List<String> exptectedMeds = new ArrayList<>();
        exptectedMeds.add("test");
        expectedConsultation.setMeds(exptectedMeds);
        _controller.addPatient(simplePatient);
        _controller.addConsultation(existingConsultations);
        _controller.addConsultation(expectedConsultation);
    }



    @Test(expected = CustomException.class)
    public void TC_EC_12_FamilyDoctor() throws CustomException {
        Patient simplePatient = new Patient();
        simplePatient.setSSN("1");
        simplePatient.setAddress("cuca macaii");
        simplePatient.setName("gigel");
        Consultation expectedConsultation = new Consultation();
        expectedConsultation.setConsID("");
        expectedConsultation.setPatientSSN("1");
        expectedConsultation.setConsultation_date(new Date("18/03/2016"));
        expectedConsultation.setDiag("d");
        List<String> exptectedMeds = new ArrayList<>();
        exptectedMeds.add("test");
        expectedConsultation.setMeds(exptectedMeds);
        _controller.addPatient(simplePatient);
        _controller.addConsultation(expectedConsultation);
    }



    @Test(expected = IllegalArgumentException.class)
    public void TC_EC_13_FamilyDoctor() throws CustomException {
        Patient simplePatient = new Patient();
        simplePatient.setSSN("1");
        simplePatient.setAddress("cuca macaii");
        simplePatient.setName("gigel");
        Consultation expectedConsultation = new Consultation();
        expectedConsultation.setConsID("1");
        expectedConsultation.setPatientSSN("1");
        expectedConsultation.setConsultation_date(new Date(""));
        expectedConsultation.setDiag("d");
        List<String> exptectedMeds = new ArrayList<>();
        exptectedMeds.add("test");
        expectedConsultation.setMeds(exptectedMeds);
        _controller.addPatient(simplePatient);
        _controller.addConsultation(expectedConsultation);
    }



    @Test(expected = IllegalArgumentException.class)
    public void TC_EC_14_FamilyDoctor() throws CustomException {
        Patient simplePatient = new Patient();
        simplePatient.setSSN("1");
        simplePatient.setAddress("cuca macaii");
        simplePatient.setName("gigel");
        Consultation expectedConsultation = new Consultation();
        expectedConsultation.setConsID("1");
        expectedConsultation.setPatientSSN("1");
        expectedConsultation.setConsultation_date(new Date("test"));
        expectedConsultation.setDiag("d");
        List<String> exptectedMeds = new ArrayList<>();
        exptectedMeds.add("test");
        expectedConsultation.setMeds(exptectedMeds);
        _controller.addPatient(simplePatient);
        _controller.addConsultation(expectedConsultation);
    }



    @Test(expected = CustomException.class)
    public void TC_EC_15_FamilyDoctor() throws CustomException {
        Patient simplePatient = new Patient();
        simplePatient.setSSN("1");
        simplePatient.setAddress("cuca macaii");
        simplePatient.setName("gigel");
        Consultation expectedConsultation = new Consultation();
        expectedConsultation.setConsID("1");
        expectedConsultation.setPatientSSN("1");
        expectedConsultation.setConsultation_date(new Date("18/03/2016"));
        expectedConsultation.setDiag("");
        List<String> exptectedMeds = new ArrayList<>();
        expectedConsultation.setMeds(exptectedMeds);
        _controller.addPatient(simplePatient);
        _controller.addConsultation(expectedConsultation);
    }



    @Test(expected = CustomException.class)
    public void TC_EC_16_FamilyDoctor() throws CustomException {
        Patient simplePatient = new Patient();
        simplePatient.setSSN("1");
        simplePatient.setAddress("cuca macaii");
        simplePatient.setName("gigel");
        Consultation expectedConsultation = new Consultation();
        expectedConsultation.setConsID("1");
        expectedConsultation.setPatientSSN("1");
        expectedConsultation.setConsultation_date(new Date("18/03/2016"));
        expectedConsultation.setDiag("d");
        List<String> exptectedMeds = new ArrayList<>();
        expectedConsultation.setMeds(exptectedMeds);
        _controller.addPatient(simplePatient);
        _controller.addConsultation(expectedConsultation);
    }

}

