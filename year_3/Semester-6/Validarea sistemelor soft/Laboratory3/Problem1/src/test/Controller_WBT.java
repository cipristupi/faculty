package test;

import controller.doctorController;
import model.Consultation;
import model.Patient;
import org.junit.Before;
import org.junit.Test;
import persistence.IRepository;
import utils.CustomException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Created by Cipri on 4/10/2016.
 */
public class Controller_WBT {
    private doctorController _controller;

    @Before
    public void TestInitialize() {
        IRepository repository = new MockRepository();
        _controller = new doctorController(repository);
    }

    @Test(expected = CustomException.class)
    public void TC_01_FamilyDoctor() throws CustomException {
        Patient expectedPatient = new Patient();
        expectedPatient.setSSN("1");
        expectedPatient.setAddress("test");
        expectedPatient.setName("");
        _controller.addPatient(expectedPatient);
    }
    @Test
    public void TC_02_FamilyDoctor() {//1,2,4
        Patient expectedPatient = new Patient();
        expectedPatient.setSSN("1");
        expectedPatient.setAddress("cuca macaii");
        expectedPatient.setName("gigel");
        try {
            _controller.addPatient(expectedPatient);
            Patient actualPatient = _controller.getPatientBySSN(expectedPatient.getSSN());
            assertEquals(expectedPatient, actualPatient);
        } catch (CustomException e) {
            e.printStackTrace();
        }
    }

    @Test//1,3,4
    public void TC_03_FamilyDoctor() throws CustomException {
        Patient simplePatient = new Patient();
        simplePatient.setSSN("1");
        simplePatient.setAddress("cuca macaii");
        simplePatient.setName("gigel");
        Consultation expectedConsultation = new Consultation();
        expectedConsultation.setConsID("1");
        expectedConsultation.setPatientSSN("1");
        expectedConsultation.setConsultation_date(new Date("18/03/2016"));
        expectedConsultation.setDiag("");
        expectedConsultation.setMeds(null);
        _controller.addPatient(simplePatient);
        _controller.addConsultation(expectedConsultation);
        Consultation actualConsultation = _controller.getConsByID(expectedConsultation.getConsID());
        assertNull(actualConsultation);
    }


    @Test(expected = CustomException.class)//1,2,6
    public void TC_04_FamilyDoctor() throws CustomException {
        Patient simplePatient = new Patient();
        simplePatient.setSSN("1");
        simplePatient.setAddress("cuca macaii");
        simplePatient.setName("gigel");
        Consultation expectedConsultation = new Consultation();
        expectedConsultation.setConsID("1");
        expectedConsultation.setPatientSSN("1");
        expectedConsultation.setConsultation_date(new Date("18/03/2016"));
        expectedConsultation.setDiag("");
        List<String> exptectedMeds = new ArrayList<>();
        exptectedMeds.add("test");
        expectedConsultation.setMeds(exptectedMeds);
        _controller.addPatient(simplePatient);
        _controller.addConsultation(expectedConsultation);
        Consultation actualConsultation = _controller.getConsByID(expectedConsultation.getConsID());
        assertEquals(expectedConsultation,actualConsultation);
    }


    @Test(expected = CustomException.class)
    public void TC_05_FamilyDoctor() throws CustomException {
        Patient simplePatient = new Patient();
        simplePatient.setSSN("1");
        simplePatient.setAddress("cuca macaii");
        simplePatient.setName("gigel");
        Consultation expectedConsultation = new Consultation();
        expectedConsultation.setConsID("1");
        expectedConsultation.setPatientSSN("1");
        expectedConsultation.setConsultation_date(new Date("18/03/2016"));
        expectedConsultation.setDiag("d");
        List<String> exptectedMeds = new ArrayList<>();
        expectedConsultation.setMeds(exptectedMeds);
        _controller.addPatient(simplePatient);
        _controller.addConsultation(expectedConsultation);
    }

    @Test
    public void TC_06_FamilyDoctor() throws CustomException {
        Patient simplePatient = new Patient();
        simplePatient.setSSN("1");
        simplePatient.setAddress("cuca macaii");
        simplePatient.setName("gigel");
        Consultation expectedConsultation = new Consultation();
        expectedConsultation.setConsID("1");
        expectedConsultation.setPatientSSN("1");
        expectedConsultation.setConsultation_date(new Date("18/03/2016"));
        expectedConsultation.setDiag("test");
        List<String> exptectedMeds = new ArrayList<>();
        exptectedMeds.add("test");
        expectedConsultation.setMeds(exptectedMeds);
        _controller.addPatient(simplePatient);
        _controller.addConsultation(expectedConsultation);
        String givenDisease = "random";
        List<Patient> actualList = _controller.getPatientsWithDisease("givenDisease");
        assertEquals(0,actualList.size());
    }


    @Test
    public void TC_07_FamilyDoctor() throws CustomException {
        Patient simplePatient = new Patient();
        simplePatient.setSSN("1");
        simplePatient.setAddress("cuca macaii");
        simplePatient.setName("gigel");
        Consultation expectedConsultation = new Consultation();
        expectedConsultation.setConsID("1");
        expectedConsultation.setPatientSSN("1");
        expectedConsultation.setConsultation_date(new Date("18/03/2016"));
        expectedConsultation.setDiag("test");
        List<String> exptectedMeds = new ArrayList<>();
        exptectedMeds.add("test");
        expectedConsultation.setMeds(exptectedMeds);

        Patient simplePatient2 = new Patient();
        simplePatient2.setSSN("2");
        simplePatient2.setAddress("cluj");
        simplePatient2.setName("aurel");

        Consultation expectedConsultation2 = new Consultation();
        expectedConsultation2.setConsID("2");
        expectedConsultation2.setPatientSSN("2");
        expectedConsultation2.setConsultation_date(new Date("18/03/2016"));
        expectedConsultation2.setDiag("test");
        List<String> exptectedMeds2 = new ArrayList<>();
        exptectedMeds2.add("test");
        expectedConsultation2.setMeds(exptectedMeds2);

        _controller.addPatient(simplePatient);
        _controller.addConsultation(expectedConsultation);

        _controller.addPatient(simplePatient2);
        _controller.addConsultation(expectedConsultation2);
        String givenDisease = "test";
        List<Patient> actualList = _controller.getPatientsWithDisease(givenDisease);
        assertEquals(2,actualList.size());
    }
}
