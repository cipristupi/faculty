/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vaannila;

import controller.doctorController;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Patient;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import persistence.Repository;
import utils.CustomException;

/**
 *
 * @author Cipri
 */
public class PatientAction extends org.apache.struts.action.Action {
   /* forward name="success" path="" */
    private final static String SUCCESS = "success";
    private final static String FAILURE = "failure";
    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        PatientForm patientForm = (PatientForm) form;
        String errorMessage = "";
        Repository rep = new Repository("pat.txt", "cons.txt");
        doctorController ctr = new doctorController(rep);
        Patient patient = new Patient(patientForm.getName(),patientForm.getSsn() ,patientForm.getAddress());
        try {
            ctr.addPatient(patient);
            System.out.println("Patient added");
            return mapping.findForward(SUCCESS);
        } catch (CustomException exception) {
            System.out.println(exception.getMessage());
            errorMessage = exception.getMessage();
        }
        patientForm.setError(errorMessage);
        return mapping.findForward(FAILURE);
    }
}
