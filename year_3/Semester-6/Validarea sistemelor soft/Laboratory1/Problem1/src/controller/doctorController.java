package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import persistence.Repository;

import model.Consultation;
import model.Patient;
import utils.CustomException;

import javax.print.DocFlavor;
import javax.xml.bind.ValidationException;

public class doctorController {

    private List<Patient> PatientList;
    private List<Consultation> ConsultationList;
    private Repository rep;

    /**
     * Constructors
     */

    public doctorController(Repository rep) {
        this.rep = rep;
        this.PatientList = rep.getPatientList();
        this.ConsultationList = rep.getConsultationList();
        // Get list from file in order to avoid duplicates.
    }

    /**
     * Getters
     */
    public List<Patient> getPatientList() {
        return PatientList;
    }

    public List<Consultation> getConsultationList() {
        return ConsultationList;
    }

    public void setConsulationList(List<Consultation> consultationList) {
        ConsultationList = consultationList;
    }

    public Patient getPatientBySSN(String SSN) {
        for (int i = 0; i < PatientList.size(); i++) {
            if (PatientList.get(i).getSSN().equals(SSN))
                return PatientList.get(i);
        }
        return null;
    }

    public int getConsByID(String ID) {
        for (int i = 0; i < ConsultationList.size(); i++) {
            if (ConsultationList.get(i).getConsID().compareTo(ID) == 0) {
                return i;
            }
        }

        return -1;
    }

    public Repository getRepository() {
        return rep;
    }


    //new version
    public void addPatient(Patient p) throws CustomException {
        ArrayList<String> errorMessages = new ArrayList<String>();
        if (validatePatient(p, errorMessages) == true) {
            PatientList.add(p);
            try {
                rep.savePatientToFile(PatientList);
            } catch (IOException e) {
                throw new CustomException("Add patient failed.");
            }
        } else {
            throw new CustomException(String.join(", ", errorMessages));
        }
    }

    //new version
    public boolean validatePatient(Patient p, List<String> errorMessages) {
        boolean valid = true;

        if (p.getName() == null || p.getName().isEmpty()) {
            valid = false;
            errorMessages.add("Name must be not empty");
        }
        if (p.getAddress() == null || p.getAddress().isEmpty()) {
            valid = false;
            errorMessages.add("Address must be not empty");
        }
        if (p.getSSN() == null || p.getSSN().isEmpty()) {
            errorMessages.add("SSN must be not empty");
            valid = false;
        }

        if (!p.getSSN().matches(("[0-9]*"))) {
            valid = false;
            errorMessages.add("SSN must be a number");

        }
        if (!p.getName().matches("^[A-Za-z ']*$")) {
            errorMessages.add("Name must contains only letters");
            valid = false;
        }
        if (getPatientBySSN(p.getSSN()) != null) {
            valid = false;
            errorMessages.add("SSN must be unique");
        }
        return valid;
    }


    //new version
    public void addConsultation(Consultation c) throws CustomException {
        if (c.getMeds() == null)
            return;
        ArrayList<String> errorMessages = new ArrayList<String>();
        if (validateConsultation(c, errorMessages) == true) {
            ConsultationList.add(c);
            try {
                rep.saveConsultationToFile(ConsultationList);
                Patient p = new Patient();
                p = this.getPatientBySSN(c.getPatientSSN());
                p.setConsNum(p.getConsNum() + 1);
                rep.savePatientToFile(PatientList);
            } catch (IOException e) {
                throw new CustomException("Add Consultation failed.");
            }
        } else {
            throw new CustomException(String.join(", ", errorMessages));
        }

    }

    //new version
    public boolean validateConsultation(Consultation c, List<String> errorMessages) {
        boolean valid = true;

        if (c.getConsID() == null || c.getConsID().isEmpty()) {
            valid = false;
            errorMessages.add("Consultation ID must be not empty");
        }
        if (c.getPatientSSN() == null || c.getPatientSSN().isEmpty()) {
            valid = false;
            errorMessages.add("Patient SSN must be not empty");
        }
        if (c.getDiag() == null || c.getDiag().isEmpty()) {
            errorMessages.add("Diagnostic must be not empty");
            valid = false;
        }
        if (c.getMeds().size() == 0) {
            valid = false;
            errorMessages.add("At least one med must be added");
        }
        if (this.getPatientBySSN(c.getPatientSSN()) == null) {
            errorMessages.add("Patient not exists");
            valid = false;
        }
        if (this.getConsByID(c.getConsID()) != -1) {
            errorMessages.add("Consultation ID must be unique");
            valid = false;
        }
        return valid;
    }


    public List<Patient> getPatientsWithDisease(String disease) {
        List<Consultation> c = this.getConsultationList();
        List<Patient> p = new ArrayList<Patient>();
        if (disease != null) {
            boolean patientNotExists ;
            List<Patient> patients = getAllPatients();

            for (int i = 0; i < c.size(); i++) {
                patientNotExists = false;
                if (c.get(i).getDiag().toLowerCase().contains(disease.toLowerCase())) // so that it is case insensitive
                {
                    for (int j = 0; j < p.size(); j++) // verify patient was not already added
                    {
                        if (p.get(j).getSSN().equals(c.get(i).getPatientSSN())) {
                            patientNotExists = true;
                            break;
                        }
                    }

                    if (!patientNotExists) {
                        Patient patient = this.getPatientBySSN(c.get(i).getPatientSSN());
                        p.add(patient); // get Patient by SSN
                    }

                }
            }

            // Sort the list

            Patient paux = new Patient();

            for (int i = 0; i < p.size()-1; i++)
                for (int j = i + 1; j < p.size(); j++)
                    if (p.get(j).getConsNum() > p.get(i).getConsNum()) {
                        paux = p.get(j);
                        p.set(j, p.get(i));
                        p.set(i, paux);
                    }
        }
        return p;
    }

    public ArrayList<Patient> getAllPatients() {
        return (ArrayList<Patient>) rep.getPatientList();
    }

    public ArrayList<Consultation> getAllConsultations() {
        return (ArrayList<Consultation>) rep.getConsultationList();
    }

}
