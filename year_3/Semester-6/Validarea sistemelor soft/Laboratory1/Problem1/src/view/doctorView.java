package view;

import controller.doctorController;
import model.Consultation;
import model.Patient;
import utils.CustomException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Cipri on 3/14/2016.
 */
public class doctorView {
    private doctorController ctr;

    public doctorView(doctorController controller) {
        ctr = controller;
    }

    public void EntryPoint() {
        readMenu();
    }

    private void readMenu() {
        Scanner kb = new Scanner(System.in);
        int cmd = 42;
        do {
            mainMenuPrint();
            System.out.println("Command: ");
            cmd = kb.nextInt();
            switch (cmd) {
                case 0:
                    break;
                case 1: {
                    addPatient();
                    break;
                }
                case 2: {
                    addConsultation();
                    break;
                }
                case 3: {
                    searchByDisease();
                    break;
                }
                case 4: {
                    printAllPatients();
                    break;
                }
                case 5: {
                    printAllConsultations();
                    break;
                }
            }

        } while (cmd != 0);
    }

    private void searchByDisease() {
        Scanner kb = new Scanner(System.in);
        System.out.println("\n");
        System.out.println("Disease: ");
        String disease = kb.nextLine();
        List<Patient> patients = ctr.getPatientsWithDisease(disease);
        printPatientList(patients);
    }

    private void mainMenuPrint() {
        System.out.println("Menu: ");
        System.out.println("0 - to exit");
        System.out.println("1 - to enter a patient");
        System.out.println("2 - to enter a consultation");
        System.out.println("3 - search by disease");
        System.out.println("4 - print all patients");
        System.out.println("5 - print all consultations");
        System.out.println("---------------------------------");
    }

    private void addPatient() {
        System.out.println("---------------Add patient------------------");
        Scanner kb = new Scanner(System.in);
        System.out.println("\n");
        System.out.println("Patient name: ");
        String patientName = kb.nextLine();


        System.out.println("\n");
        System.out.println("Patient SSN: ");
        String patientSSN = kb.nextLine();

        System.out.println("\n");
        System.out.println("Patient address: ");
        String patientaddress = kb.nextLine();

        Patient patient = new Patient(patientName, patientSSN, patientaddress);
        try {
            ctr.addPatient(patient);
            System.out.println("Patient added");
        } catch (CustomException exception) {
            System.out.println(exception.getMessage());
        }
    }


    private void addConsultation() {
        System.out.println("----------------Add consultation-----------------");
        Scanner kb = new Scanner(System.in);
        System.out.println("\n");
        System.out.println("Consultation ID: ");
        String consultationId = kb.nextLine();

        System.out.println("\n");
        System.out.println("Patient SSN: ");
        String patientSSN = kb.nextLine();


        System.out.println("\n");
        System.out.println("Consultation date: ");
        String consultationDateString = kb.nextLine();
        String dateFormat = "dd/MM/yyyy";
        Date consultationDate = null;
        try {
            consultationDate = new SimpleDateFormat(dateFormat).parse(consultationDateString);
        } catch (ParseException e) {
            System.out.println("Invalid date. Format for date is Day/Month/Year/ . Example 16/03/2016");
            return;
        }

        System.out.println("\n");
        System.out.println("Diagnostic: ");
        String diagnostic = kb.nextLine();

        List<String> meds = readMeds();

        Consultation consultation = new Consultation();
        consultation.setDiag(diagnostic);
        consultation.setConsultation_date(consultationDate);
        consultation.setPatientSSN(patientSSN);
        consultation.setConsID(consultationId);
        consultation.setMeds(meds);
        try {
            ctr.addConsultation(consultation);
            System.out.println("Consultation added");
        } catch (CustomException exception) {
            System.out.println(exception.getMessage());
        }
    }

    private List<String> readMeds() {
        ArrayList<String> drugs = new ArrayList<>();
        Scanner kb = new Scanner(System.in);
        System.out.println("---------------------------------");
        int cmd;
        do {
            printAddDrugMenu();
            System.out.println("Add drug command: ");
            cmd = kb.nextInt();
            switch (cmd) {
                case 0:
                    break;
                case 1: {
                    String drug = readMed();
                    drugs.add(drug);
                    break;
                }
            }
        } while (cmd != 0);
        return drugs;
    }

    private String readMed() {
        Scanner kb = new Scanner(System.in);
        System.out.println("\n");
        System.out.println("Prescription drug: ");
        String drug = kb.nextLine();
        return drug;
    }


    private void printAllPatients() {
        ArrayList<Patient> patients = ctr.getAllPatients();
        for (Patient p : patients) {
            System.out.println("\n");
            System.out.println(p.toString());
        }
    }

    private void printAllConsultations() {
        ArrayList<Consultation> consultations = ctr.getAllConsultations();
        for (Consultation c : consultations) {
            System.out.println("\n");
            System.out.println(c.toString());
        }
    }

    private void printAddDrugMenu() {
        System.out.println("-----------------Add drug----------------");
        System.out.println("0 - to stop adding drugs");
        System.out.println("1 - to add drug");
    }

    private void printPatientList(List<Patient> patients)
    {
        for (Patient p : patients) {
            System.out.println("\n");
            System.out.println(p.toString());
        }
    }

}
