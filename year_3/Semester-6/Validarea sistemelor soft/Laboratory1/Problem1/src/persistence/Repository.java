package persistence;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import model.Consultation;
import model.Patient;


public class Repository {

    private String patients; // list of patients
    private String consultations; // list of consultation


    public Repository(String patients, String consultations) {
        this.patients = patients;
        this.consultations = consultations;
    }

    //new version
    public void cleanFiles(String fileName) {
        FileWriter fw;
        try {
            fw = new FileWriter(fileName);
            PrintWriter out = new PrintWriter(fw);
            out.print("");
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public String[] getPatientsFromFile() throws IOException {
        int n = 0;
        BufferedReader in = new BufferedReader(new FileReader(patients));
        while ((in.readLine()) != null) {
            n++;
        }
        in.close();

        String[] la = new String[n];
        String s = new String();
        int i = 0;
        in = new BufferedReader(new FileReader(patients));
        while ((s = in.readLine()) != null) {
            la[i] = s;
            i++;
        }
        in.close();
        return la;
    }

    public String[] getConsultationsFromFile() throws IOException {
        int n = 0;
        BufferedReader in = new BufferedReader(new FileReader(consultations));
        while ((in.readLine()) != null) {
            n++;
        }
        in.close();

        String[] la = new String[n];
        String s = new String();
        int i = 0;
        in = new BufferedReader(new FileReader(consultations));
        while ((s = in.readLine()) != null) {
            la[i] = s;
            i++;
        }
        in.close();
        return la;
    }

    public List<Patient> getPatientList() {
        List<Patient> lp = new ArrayList<Patient>();
        try {
            String[] tokens = getPatientsFromFile();

            String tok = new String();
            String[] pat;
            int i = 0;
            while (i < tokens.length) {
                tok = tokens[i];
                pat = tok.split(",");
                lp.add(new Patient(pat[0], pat[1], pat[2], pat[3]));
                i = i + 1;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return lp;
    }

    public List<Consultation> getConsultationList() {
        List<Consultation> lp = new ArrayList<Consultation>();
        try {
            String[] tokens = getConsultationsFromFile();

            String tok = new String();
            String[] cons;
            String[] meds;
            int i = 0;
            while (i < tokens.length) {
                List<String> med = new ArrayList<String>();
                tok = tokens[i];
                cons = tok.split(",");

                String dateFormat = "dd/MM/yyyy";
                Date consultationDate = null;
                try {
                    consultationDate = new SimpleDateFormat(dateFormat).parse(cons[4]);
                } catch (ParseException e) {
                }

                Consultation c = new Consultation(cons[0], cons[1], cons[2], med, consultationDate);
                meds = cons[3].split("\\+");
                for (int j = 0; j < meds.length; j++) {
                    c.getMeds().add(meds[j]);
                }
                lp.add(c);
                i = i + 1;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return lp;
    }


    //Improve to save file
    //new version
    public void savePatientToFile(List<Patient> patientsList) throws IOException {
        cleanFiles(patients);
        FileWriter fw = new FileWriter(patients);
        PrintWriter out = new PrintWriter(fw);
        for (int i = 0; i < patientsList.size(); i++)
            out.println(patientsList.get(i).toString());
        out.close();
    }

    //Improve to save file
    //new version
    public void saveConsultationToFile(List<Consultation> consultationList) throws IOException {
        cleanFiles(consultations);
        FileWriter fw = new FileWriter(consultations);
        PrintWriter out = new PrintWriter(fw);
        for (int i = 0; i < consultationList.size(); i++)
            out.println(consultationList.get(i).toString());
        out.close();
    }
}

