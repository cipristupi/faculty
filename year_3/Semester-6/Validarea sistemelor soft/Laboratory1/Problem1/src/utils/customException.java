package utils;

/**
 * Created by Cipri on 3/16/2016.
 */
public class CustomException extends Exception {
    public CustomException(String message) {
        super(message);
    }
}
