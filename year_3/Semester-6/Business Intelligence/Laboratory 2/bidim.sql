use [BusinessIntelligence]

--drop table FactOlympicAthlets
--drop table DimensionDate
--drop table DimensionAthlete
--drop table DimensionAge
--drop table DimensionCountry
--drop table DimensionSport
--drop table DimensionMedal

create table DimensionAthlete(
		AthleteKey int identity primary key,
		FirstName nvarchar(50),
		LastName nvarchar(50));

create table DimensionAge(
		AgeKey int identity primary key,
		Age tinyint not null,
		AgeCategory nvarchar(15));

create table DimensionCountry(
		CountryKey int identity primary key,
		CountryName nvarchar(100));
		
create table DimensionDate(
		DataKey int identity primary key,
		Data date not null,
		DDay tinyint,
		DayOfWeek nvarchar(15),
		DMonth tinyint,
		Season nvarchar(15),
		DYear smallint);

create table DimensionSport(
		SportKey int identity primary key,
		Name nvarchar(50))

create table DimensionMedal(
		MedalKey int identity primary key,
		Medaltype int)

insert into DimensionMedal values (1) -- gold
insert into DimensionMedal values (2) -- silver
insert into DimensionMedal values (3) -- bronze

create table FactOlympicAthlets(
		FactKey int identity primary key,
		AthleteKey int foreign key references DimensionAthlete(athleteKey),
		AgeKey int foreign key references DimensionAge(ageKey),
		CountryKey int foreign key references DimensionCountry(countryKey),
		DataKey int foreign key references DimensionDate(dataKey),
		SportKey int foreign key references DimensionSport(sportKey),
		MedalKey int foreign key references DimensionMedal(medalKey),
		NoOfMedals int)