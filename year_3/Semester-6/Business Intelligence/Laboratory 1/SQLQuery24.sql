USE [Organizations]
GO

INSERT INTO [dbo].[Countries]
SELECT ct2.Code ,ct2.Name,city.Name , ct2.Region,ct2.Population,c.Code FROM Country2 ct2
JOIN City city ON city.ID = ct2.Capital
JOIN Continents c ON c.Name = ct2.Continent
     --      ([Code]
     --      ,[Name]
     --      ,[Capital]
     --      ,[Area]
     --      ,[Population]
     --      ,[ContinentCode])
     --VALUES
     --      (<Code, nvarchar(50),>
     --      ,<Name, nvarchar(50),>
     --      ,<Capital, nvarchar(50),>
     --      ,<Area, nvarchar(50),>
     --      ,<Population, int,>
     --      ,<ContinentCode, nvarchar(3),>)
GO


