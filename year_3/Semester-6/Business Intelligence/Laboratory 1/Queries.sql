---List all the countries which are members of NATO.
SELECT c.Name,o.Name FROM OrganizationCountry og
JOIN Countries c ON og.CountryCode = c.Code
JOIN Organizations o ON og.OrganizationCode = o.Code
WHERE o.Code = 'Nato'

--List all the countries which are members of organizations founded before 1980
SELECT c.Name,o.Name FROM OrganizationCountry og
JOIN Countries c ON og.CountryCode = c.Code
JOIN Organizations o ON og.OrganizationCode = o.Code
WHERE o.FoundedDate < '01-01-1980'


--List all the countries which are members of only one organization
SELECT c.Name FROM Countries c
WHERE (SELECT COUNT(*) FROM OrganizationCountry WHERE CountryCode = c.Code) = 1


--List all the capitals which are headquarter of no organization
SELECT c.Capital FROM Countries c WHERE c.Capital NOT IN (SELECT o.Headquarter FROM Organizations o)

--List the population of each continent
SELECT c.Name, SUM(CONVERT(bigint, ct.Population))   FROM Continents c
LEFT JOIN Countries ct ON ct.ContinentCode = c.Code
GROUP By c.Name

--Count the countries of each continent
SELECT c.Name, Count(ct.Name)   FROM Continents c
LEFT JOIN Countries ct ON ct.ContinentCode = c.Code
GROUP By c.Name