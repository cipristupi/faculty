﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearningPlatform.Models.BusinessModels
{
    public class RoleBusiness
    {
        public string Name { get; set; }
        public string Id { get; set; }
    }
}
