﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearningPlatform.Models.BusinessModels
{
    public class StaffToDepartamentBusiness
    {
        public int Id { get; set; }
        public string UniversityStaffId { get; set; }
        public int DepartmentId { get; set; }
        public string TeacherName { get; set; }
        public string DepartmentName { get; set; }
    }
}
