﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearningPlatform.Models.BusinessModels
{
    public class TeacherCourseBusiness
    {
        public CourseBusiness CourseDetails { get; set; }
        public List<LectureBusiness> Lectures { get; set; }

        public string CoordonaterId { get; set; }
    }
}
