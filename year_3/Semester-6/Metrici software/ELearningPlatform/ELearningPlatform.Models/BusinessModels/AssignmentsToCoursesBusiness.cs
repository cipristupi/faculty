﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearningPlatform.Models.BusinessModels
{
    public class AssignmentsToCoursesBusiness
    {
        public int Id { get; set; }
        public int CourseId { get; set; }
        public int AssignmentId { get; set; }
    }
}
