﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearningPlatform.Models.BusinessModels
{
    public class QuestionBusiness
    {
        public int Id { get; set; }
        public string Question { get; set; }
        public string Description { get; set; }
        public int Points { get; set; }
        public int TypeId { get; set; }
    }
}
