﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearningPlatform.Models.BusinessModels
{
    public class SyllabusBusiness
    {

        public int Id { get; set; }
        public string Name { get; set; }
        public string SyllabusContent { get; set; }
        public int CourseId { get; set; }
        public string CourseName { get; set; }
    }
}
