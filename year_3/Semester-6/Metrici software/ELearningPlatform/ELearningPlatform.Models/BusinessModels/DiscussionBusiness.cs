﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearningPlatform.Models.BusinessModels
{
    public class DiscussionBusiness
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Subject { get; set; }
        public int? GradeValueId { get; set; }
        public int GradeSchemaId { get; set; }
        public int ParentId { get; set; }
        public int ParentType { get; set; }

        public string SchemaName { get; set; }
        public string SchemaValue { get; set; }
        public List<CommentBusiness> Comments { get; set; } 
    }
}
