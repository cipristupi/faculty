﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearningPlatform.Models.BusinessModels
{
    public class BoolQuestionBusiness
    {
        public int QuestionId { get; set; }
        public bool Answer { get; set; }
    }
}
