﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearningPlatform.Models.BusinessModels
{
    public class AssignmentBusiness
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime DeadlineDate { get; set; }
        public string Type { get; set; }
        public int GradeSchemaId { get; set; }
        public int AssignmentsCategoryId { get; set; }
    }
}
