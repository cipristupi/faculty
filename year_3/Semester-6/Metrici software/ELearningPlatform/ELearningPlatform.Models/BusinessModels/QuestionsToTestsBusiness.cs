﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearningPlatform.Models.BusinessModels
{
    public class QuestionsToTestsBusiness
    {
        public int Id { get; set; }
        public int TestId { get; set; }
        public int QuestionId { get; set; }
    }
}
