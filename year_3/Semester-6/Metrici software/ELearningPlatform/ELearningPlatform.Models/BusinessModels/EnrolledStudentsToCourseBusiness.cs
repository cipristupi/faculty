﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearningPlatform.Models.BusinessModels
{
    public class EnrolledStudentsToCourseBusiness
    {
        public int Id { get; set; }
        public int CourseId { get; set; }
        public string StudentId { get; set; }
    }
}
