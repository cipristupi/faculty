﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearningPlatform.Models.BusinessModels
{
    public class AssignmentSettingBusiness
    {
        public int Id { get; set; }
        public int AssignmentId { get; set; }
        public bool PhisicalDelivery { get; set; }
        public bool NoDelivery { get; set; }
        public bool UploadFile { get; set; }
        public bool WithLink { get; set; }
        public bool WithText { get; set; }
        public bool GroupAssignment { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
