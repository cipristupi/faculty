﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearningPlatform.Models.BusinessModels
{
    public class GradeValueBusiness
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public int GradeSchemaId { get; set; }
    }
}
