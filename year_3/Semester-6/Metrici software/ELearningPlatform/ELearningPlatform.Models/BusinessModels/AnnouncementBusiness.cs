﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearningPlatform.Models.BusinessModels
{
    public class AnnouncementBusiness
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string AnnouncementContent { get; set; }
        public DateTime DateCreated { get; set; }
        public int? ParentId { get; set; }
        public string UserId { get; set; }
        public int? ParentType { get; set; }
        public string UserName { get; set; }
    }
}
