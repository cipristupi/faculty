﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearningPlatform.Models.BusinessModels
{
    public class TestSettingBusiness
    {
        public int Id { get; set; }
        public int TestId { get; set; }
        public bool RandomOrderQuestions { get; set; }
        public bool TimeLimit { get; set; }
        public Nullable<System.DateTime> TimeLimitValue { get; set; }
        public bool CanRepetTest { get; set; }
        public Nullable<int> NumberOfRepetitions { get; set; }
        public bool LastGrade { get; set; }
        public bool HigherGrade { get; set; }
        public bool CanSeeAnswers { get; set; }
        public bool GoodAnswersIntervalForSeen { get; set; }
        public bool OneQuestionAtAnyMoment { get; set; }
        public bool TestHasPassword { get; set; }
        public string TestPassword { get; set; }
    }
}
