﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearningPlatform.Models.BusinessModels
{
    public class PageSectionBusiness
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string SectionContent { get; set; }
        public int PageId { get; set; }
    }
}
