﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearningPlatform.Models.BusinessModels
{
    public class CommentBusiness
    {
        public int Id { get; set; }
        public System.DateTime DateCreated { get; set; }
        public string CommentContent { get; set; }
        public string UserId { get; set; }
        public int ParentId { get; set; }
        public string UserFullName { get; set; }
        public int ParentType { get; set; }
    }
}
