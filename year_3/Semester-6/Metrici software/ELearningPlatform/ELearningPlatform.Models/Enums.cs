﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearningPlatform.Models
{
    public enum UserRolesEnum
    {
        Admin,
        Teacher,
        Student
    }

    public enum ParentTypes
    {
        Course=0,
        Discussion=1,
        Announcements=2
    }
}
