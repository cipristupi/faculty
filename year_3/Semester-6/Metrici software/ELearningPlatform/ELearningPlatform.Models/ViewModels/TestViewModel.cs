﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace ELearningPlatform.Models.ViewModels
{
    public class TestViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        [Display(Name = "Start Date")]
        public DateTime StartDate { get; set; }
        [Display(Name = "End Date")]
        public DateTime? EndDate { get; set; }

        public DateTime? DeadLine { get; set; }
        public string Type { get; set; }
        public int CourseId { get; set; }
        public List<SelectListItem> AvailableCourses { get; set; }

        public List<SelectListItem> Courses { get; set; }

        [Display(Name = "Course Name")]
        public string CourseName { get; set; }
    }
}
