﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearningPlatform.Models.ViewModels
{
    public class StudentsToWorkingGroupViewModel
    {
        public int Id { get; set; }
        public string StudentId { get; set; }
        public int WorkingGroupId { get; set; }
    }
}
