﻿using System.ComponentModel.DataAnnotations;

namespace ELearningPlatform.Models.ViewModels
{
    public class StudentViewModel :UserViewModel
    {
        [Required]
        public string UniqueNumber { get; set; }
    }
}
