﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ELearningPlatform.Models.ViewModels
{
    public class QuestionViewModel
    {
        public int Id { get; set; }
        public string Question { get; set; }
        public string Description { get; set; }
        public int Points { get; set; }
        public int TypeId { get; set; }
        public List<SelectListItem> Types { get; set; }
    }
}
