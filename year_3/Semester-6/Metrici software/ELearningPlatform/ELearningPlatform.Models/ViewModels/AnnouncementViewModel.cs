﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace ELearningPlatform.Models.ViewModels
{
    public class AnnouncementViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        [AllowHtml]
        [Display(Name = "Announcement Content")]
        public string AnnouncementContent { get; set; }
        [Display(Name="Creation Date")]
        public DateTime DateCreated { get; set; }
        public int? ParentId { get; set; }
        public string UserId { get; set; }
        public int? ParentType { get; set; }
        [Display(Name = "Username")]
        public string UserName { get; set; }
        public string ParentName { get; set; }
    }
}
