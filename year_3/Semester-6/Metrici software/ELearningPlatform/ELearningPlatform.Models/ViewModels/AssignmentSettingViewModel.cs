﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace ELearningPlatform.Models.ViewModels
{
    public class AssignmentSettingViewModel
    {
        public int Id { get; set; }
        public int AssignmentId { get; set; }
        public bool PhisicalDelivery { get; set; }
        public bool NoDelivery { get; set; }
        public bool UploadFile { get; set; }
        public bool WithLink { get; set; }
        public bool WithText { get; set; }
        public bool GroupAssignment { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public int SelectedAssignment { get; set; }
        public List<SelectListItem> AvailableAssignments { get; set; }
    }
}
