﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ELearningPlatform.Models.ViewModels
{
    public class AssignmentsToCoursesViewModel
    {
        public int Id { get; set; }
        public int CourseId { get; set; }
        public int AssignmentId { get; set; }
        public List<SelectListItem> Courses { get; set; }
        public List<SelectListItem> Assignments { get; set; }
    }
}
