﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearningPlatform.Models.ViewModels
{
    public class TeacherCourseViewModel
    {
        public CourseViewModel CourseDetails { get; set; }
        public List<LectureViewModel> Lectures { get; set; } 

        public string CoordonaterId { get; set; }
    }
}
