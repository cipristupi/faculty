﻿using System.ComponentModel.DataAnnotations;

namespace ELearningPlatform.Models.ViewModels
{
    public class UniversityStaffViewModel : UserViewModel
    {
        [Required]
        public int DepartmentId { get; set; }
    }
}
