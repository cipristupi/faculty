﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearningPlatform.Models.ViewModels
{
    public class JqueryAutocompleteItem
    {
        public string Id { get; set; }
        public string Value { get; set; }
        public string Label { get; set; }
    }
}
