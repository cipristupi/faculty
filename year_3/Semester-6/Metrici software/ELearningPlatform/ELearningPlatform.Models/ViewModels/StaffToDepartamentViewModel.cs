﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace ELearningPlatform.Models.ViewModels
{
    public class StaffToDepartamentViewModel
    {
        public int Id { get; set; }
        public string UniversityStaffId { get; set; }
        public int DepartmentId { get; set; }

        [Display(Name="Teacher Name")]
        public string TeacherName { get; set; }
        [Display(Name = "Department Name")]
        public string DepartmentName { get; set; }
        public List<SelectListItem> UniversityStaff { get; set; }
        public List<SelectListItem> Teachers { get; set; }
    }
}
