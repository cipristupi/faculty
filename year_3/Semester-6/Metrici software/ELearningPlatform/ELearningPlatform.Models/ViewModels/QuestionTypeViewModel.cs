﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearningPlatform.Models.ViewModels
{
    public class QuestionTypeViewModel
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
}
