﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ELearningPlatform.Models.ViewModels
{
    public class SyllabusViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        [AllowHtml]
        [Display(Name = "Syllabus Content")]
        public string SyllabusContent { get; set; }
        public int CourseId { get; set; }
        public List<SelectListItem> Courses { get; set; }
        [Display(Name="Course Name")]
        public string CourseName { get; set; }
    }
}
