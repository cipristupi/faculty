﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace ELearningPlatform.Models.ViewModels
{
    public class CommentViewModel
    {
        public int Id { get; set; }
        public DateTime DateCreated { get; set; }

        [AllowHtml]
        public string CommentContent { get; set; }
        public string UserId { get; set; }
        public int ParentId { get; set; }
        public string UserFullName { get; set; }
        public int ParentType { get; set; }
    }
}
