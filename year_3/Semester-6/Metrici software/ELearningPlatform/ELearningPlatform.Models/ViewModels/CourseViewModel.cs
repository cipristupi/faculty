﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using ELearningPlatform.Models.BusinessModels;

namespace ELearningPlatform.Models.ViewModels
{
    public class CourseViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        [Display(Name = "Start date")]
        public DateTime StartDate { get; set; }

        [Display(Name = "End date")]
        public DateTime EndDate { get; set; }

        [AllowHtml]
        public string Description { get; set; }
        public int DepartmentId { get; set; }
        public string CoordonaterId { get; set; }
        [Display(Name = "Coordonater Name")]
        public string CoordonaterName { get; set; }
        [Display(Name = "Department Name")]
        public string DepartmentName { get; set; }
        public List<SelectListItem> Coordonaters { get; set; }
        public List<SelectListItem> Departments { get; set; }
        public List<LectureViewModel> Lectures { get; set; }
        public List<DiscussionViewModel> Discussions { get; set; }
        public List<AssignmentViewModel> Assignments { get; set; }
        public List<SyllabusViewModel> Syllabus { get; set; }
        public List<AnnouncementViewModel> Announcements { get; set; }
        public List<TestViewModel> Tests { get; set; }
    }
}
