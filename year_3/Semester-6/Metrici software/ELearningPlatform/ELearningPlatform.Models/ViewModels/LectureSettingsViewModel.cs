﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearningPlatform.Models.ViewModels
{
    public class LectureSettingsViewModel
    {
        public int Id { get; set; }
        public bool IsAvailableAfterDate { get; set; }

        public bool AvailableAfterDate { get; set; }

        public int LectureId { get; set; }
    }
}
