﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ELearningPlatform.Models.ViewModels
{
    public class QuestionsToTestsViewModel
    {
        public int Id { get; set; }
        public int TestId { get; set; }
        public int QuestionId { get; set; }

        public List<SelectListItem> Tests { get; set; }
        public List<SelectListItem> Questions { get; set; }
    }
}
