﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ELearningPlatform.Models.ViewModels
{
    public class AssignmentViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime DeadlineDate { get; set; }
        public string Type { get; set; }
        public int GradeSchemaId { get; set; }
        public int AssignmentsCategoryId { get; set; }

        public int SelectedCategory { get; set; }
        public List<SelectListItem> AvailableCategories { get; set; }

        public int SelectedGradeSchema { get; set; }
        public List<SelectListItem> AvailableGradeSchemas { get; set; }
    }
}
