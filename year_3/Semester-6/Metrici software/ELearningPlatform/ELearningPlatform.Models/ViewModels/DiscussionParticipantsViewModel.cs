﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearningPlatform.Models.ViewModels
{
    public class DiscussionParticipantsViewModel
    {
        public int Id { get; set; }
        
        public int UserId { get; set; }
        public int DiscussionId { get; set; }
    }
}

