﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace ELearningPlatform.Models.ViewModels
{
    public class EnrolledStudentsToCourseViewModel
    {
        public int Id { get; set; }
        public int CourseId { get; set; }
        public string CourseName { get; set; }
        [Display(Name ="Student")]
        public string StudentId { get; set; }

        public List<SelectListItem> Students { get; set; }
        public List<SelectListItem> EnrolledStudents { get; set; }
    }
}
