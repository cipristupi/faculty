﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ELearningPlatform.Models.ViewModels
{
    public class DiscussionViewModel
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [AllowHtml]
        public string Description { get; set; }
        [Required]
        public string Subject { get; set; }
        public int? GradeValueId { get; set; }

        [Required]
        public int GradeSchemaId { get; set; }
        public int ParentId { get; set; }
        public int ParentType { get; set; }

        public List<SelectListItem> GradeValues { get; set; }
        public List<SelectListItem> GradeSchemas { get; set; }

        public List<CommentViewModel> Comments { get; set; }

        public string SchemaName { get; set; }
        public string SchemaValue { get; set; }


        public CommentViewModel Comment { get; set; }
    }
}
