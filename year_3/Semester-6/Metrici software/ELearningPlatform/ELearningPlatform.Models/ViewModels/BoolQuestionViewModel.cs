﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ELearningPlatform.Models.ViewModels
{
    public class BoolQuestionViewModel
    {
        public int QuestionId { get; set; }
        public bool Answer { get; set; }
        public List<SelectListItem> Questions { get; set; }
    }
}
