﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace ELearningPlatform.Models.ViewModels
{
    public class PageViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int ParentId { get; set; }
        public List<PageSectionViewModel> PageSections { get; set; }

        public List<SelectListItem> Lectures { get; set; } 
    }
}
