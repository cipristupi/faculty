﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace ELearningPlatform.Models.ViewModels
{
    public class LectureViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        [AllowHtml]
        public string Description { get; set; }
        [Display(Name = "Start date")]
        public DateTime StartDate { get; set; }
        [Display(Name = "End date")]
        public DateTime? EndDate { get; set; }
        public int CourseId { get; set; }
        [Display(Name = "Course name")]
        public string CourseName { get; set; }

        public List<SelectListItem> Courses { get; set; } 
        public LectureSettingsViewModel LectureSettings { get; set; } 
        public List<PageViewModel> Pages { get; set; }
    }
}
