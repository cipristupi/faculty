﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace ELearningPlatform.Models.ViewModels
{
    public class GradeValueViewModel
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public int GradeSchemaId { get; set; }

        public int SelectedGradeSchema { get; set; }
        public List<SelectListItem> AvailableGradeSchemas { get; set; }
    }
}
