﻿using System.Collections.Generic;

namespace ELearningPlatform.Models
{
    public class DataTableResponse
    {
        public DataTableResponse()
        {
            sEcho = 1;
            iTotalRecords = "0";
            iTotalDisplayRecords = "0";
        }

        public int sEcho { get; set; }
        public string iTotalRecords { get; set; }
        public string iTotalDisplayRecords { get; set; }
        public List<List<string>> aaData { get; set; }
    }
}
