﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ELearningPlatform.DataLayer.Edmx;

namespace ELearningPlatform.DataLayer.Repository
{
    public class UsersRepository : BaseRepository<AspNetUsers>
    {
        public IQueryable<AspNetUsers> GetAllTeachers()
        {
            return _context.AspNetUsers.Include("AspNetRoles");
        }

        public IQueryable<AspNetUsers> GetAllStudents()
        {
            return _context.AspNetUsers.Include("AspNetRoles");
        }

        public string GetFullName(string id)
        {
            var user = _context.AspNetUsers.FirstOrDefault(x => x.Id == id);
            string fullName = string.Empty;
            if (user != null)
            {
                fullName = string.Format("{0} {1}", user.FirstName, user.LastName);
            }
            return fullName;
        }
    }
}
