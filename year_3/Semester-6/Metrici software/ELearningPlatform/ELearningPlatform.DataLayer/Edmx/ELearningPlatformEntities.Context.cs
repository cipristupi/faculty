﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ELearningPlatform.DataLayer.Edmx
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class ELearningPlatformEntities : DbContext
    {
        public ELearningPlatformEntities()
            : base("name=ELearningPlatformEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<C__MigrationHistory> C__MigrationHistory { get; set; }
        public virtual DbSet<Announcements> Announcements { get; set; }
        public virtual DbSet<AspNetRoles> AspNetRoles { get; set; }
        public virtual DbSet<AspNetUserClaims> AspNetUserClaims { get; set; }
        public virtual DbSet<AspNetUserLogins> AspNetUserLogins { get; set; }
        public virtual DbSet<AspNetUsers> AspNetUsers { get; set; }
        public virtual DbSet<Assignments> Assignments { get; set; }
        public virtual DbSet<AssignmentsCategories> AssignmentsCategories { get; set; }
        public virtual DbSet<AssignmentsSettings> AssignmentsSettings { get; set; }
        public virtual DbSet<AssignmentsToCourses> AssignmentsToCourses { get; set; }
        public virtual DbSet<AssociationQuestions> AssociationQuestions { get; set; }
        public virtual DbSet<AssociationsAnswers> AssociationsAnswers { get; set; }
        public virtual DbSet<BoolQuestions> BoolQuestions { get; set; }
        public virtual DbSet<Comments> Comments { get; set; }
        public virtual DbSet<Courses> Courses { get; set; }
        public virtual DbSet<Departments> Departments { get; set; }
        public virtual DbSet<DiscussionParticipants> DiscussionParticipants { get; set; }
        public virtual DbSet<Discussions> Discussions { get; set; }
        public virtual DbSet<EnrolledStudentsToCourses> EnrolledStudentsToCourses { get; set; }
        public virtual DbSet<Files> Files { get; set; }
        public virtual DbSet<FillEmptySpacesAnswers> FillEmptySpacesAnswers { get; set; }
        public virtual DbSet<FillEmptySpacesQuestions> FillEmptySpacesQuestions { get; set; }
        public virtual DbSet<GradeSchemas> GradeSchemas { get; set; }
        public virtual DbSet<GradeValues> GradeValues { get; set; }
        public virtual DbSet<Lectures> Lectures { get; set; }
        public virtual DbSet<LectureSettings> LectureSettings { get; set; }
        public virtual DbSet<MissingTexstQuestionsAnswers> MissingTexstQuestionsAnswers { get; set; }
        public virtual DbSet<MissingTextQuestions> MissingTextQuestions { get; set; }
        public virtual DbSet<Pages> Pages { get; set; }
        public virtual DbSet<PageSections> PageSections { get; set; }
        public virtual DbSet<Questions> Questions { get; set; }
        public virtual DbSet<QuestionsToTests> QuestionsToTests { get; set; }
        public virtual DbSet<QuestionTypes> QuestionTypes { get; set; }
        public virtual DbSet<StaffToDepartaments> StaffToDepartaments { get; set; }
        public virtual DbSet<StudentAssignments> StudentAssignments { get; set; }
        public virtual DbSet<StudentAssociationsAnswers> StudentAssociationsAnswers { get; set; }
        public virtual DbSet<StudentBoolQuestions> StudentBoolQuestions { get; set; }
        public virtual DbSet<StudentFillEmptySpaceAnswers> StudentFillEmptySpaceAnswers { get; set; }
        public virtual DbSet<StudentMissingTexstQuestionsAnswers> StudentMissingTexstQuestionsAnswers { get; set; }
        public virtual DbSet<Students> Students { get; set; }
        public virtual DbSet<StudentsToTests> StudentsToTests { get; set; }
        public virtual DbSet<StudentsToWorkingGroups> StudentsToWorkingGroups { get; set; }
        public virtual DbSet<StudentToTestAnswers> StudentToTestAnswers { get; set; }
        public virtual DbSet<Syllabus> Syllabus { get; set; }
        public virtual DbSet<sysdiagrams> sysdiagrams { get; set; }
        public virtual DbSet<Tests> Tests { get; set; }
        public virtual DbSet<TestSettings> TestSettings { get; set; }
        public virtual DbSet<UniversityStaff> UniversityStaff { get; set; }
        public virtual DbSet<WorkingGroups> WorkingGroups { get; set; }
    }
}
