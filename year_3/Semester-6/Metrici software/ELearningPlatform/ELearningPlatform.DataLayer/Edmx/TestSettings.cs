//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ELearningPlatform.DataLayer.Edmx
{
    using System;
    using System.Collections.Generic;
    
    public partial class TestSettings
    {
        public int Id { get; set; }
        public int TestId { get; set; }
        public bool RandomOrderQuestions { get; set; }
        public bool TimeLimit { get; set; }
        public Nullable<System.DateTime> TimeLimitValue { get; set; }
        public bool CanRepetTest { get; set; }
        public Nullable<int> NumberOfRepetitions { get; set; }
        public bool LastGrade { get; set; }
        public bool HigherGrade { get; set; }
        public bool CanSeeAnswers { get; set; }
        public bool GoodAnswersIntervalForSeen { get; set; }
        public bool OneQuestionAtAnyMoment { get; set; }
        public bool TestHasPassword { get; set; }
        public string TestPassword { get; set; }
    
        public virtual Tests Tests { get; set; }
    }
}
