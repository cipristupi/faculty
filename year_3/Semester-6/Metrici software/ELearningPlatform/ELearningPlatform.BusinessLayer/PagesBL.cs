﻿using ELearningPlatform.DataLayer.Edmx;
using ELearningPlatform.DataLayer.Repository;
using ELearningPlatform.Models.BusinessModels;
using System.Collections.Generic;
using System.Linq;

namespace ELearningPlatform.BusinessLayer
{
    public class PagesBl : IBusinessLayer<PageBusiness>
    {
        readonly PageRepository _pageRepository;
        private readonly PageSectionsBl _pageSectionBl;

        public PagesBl()
        {
            _pageRepository = new PageRepository();
            _pageSectionBl =new PageSectionsBl();
        }
        public void Add(PageBusiness entity)
        {
            var pages = AutoMapper.Mapper.Map<Pages>(entity);
            _pageRepository.Add(pages);
        }

        public int AddAndReturnsId(PageBusiness entity)
        {
            var pages = AutoMapper.Mapper.Map<Pages>(entity);
            _pageRepository.Add(pages);
            return pages.Id;
        }

        public void Remove(object id)
        {
            RemovePageSections(id);
            _pageRepository.Delete(id);
        }

        private void RemovePageSections(object id)
        {
            var pageSections = _pageSectionBl.GetAllByPage((int) id);
            foreach (var pageSectionBusiness in pageSections)
            {
                _pageSectionBl.Remove(pageSectionBusiness.Id);
            }
        }

        public void Update(PageBusiness entity)
        {
            var pages = AutoMapper.Mapper.Map<Pages>(entity);
            _pageRepository.Update(pages, entity.Id);
        }

        public IEnumerable<PageBusiness> GetAll()
        {
            return _pageRepository.GetAll().Select(AutoMapper.Mapper.Map<PageBusiness>).ToList();
        }

        public IEnumerable<PageBusiness> GetAllByParentId(int id)
        {
            return GetAll().Where(x => x.ParentId == id);
        }

        public PageBusiness GetById(int id)
        {
            return GetAll().FirstOrDefault(x => x.Id == id);
        }
    }
}
