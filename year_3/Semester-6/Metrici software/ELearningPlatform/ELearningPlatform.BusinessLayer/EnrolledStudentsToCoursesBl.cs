﻿using AutoMapper;
using ELearningPlatform.DataLayer.Edmx;
using ELearningPlatform.DataLayer.Repository;
using ELearningPlatform.Models.BusinessModels;
using System.Collections.Generic;
using System.Linq;

namespace ELearningPlatform.BusinessLayer
{
    public class EnrolledStudentsToCoursesBl : IBusinessLayer<EnrolledStudentsToCourseBusiness>
    {
        private readonly EnrolledStudentsToCoursesRepository _repository;

        public EnrolledStudentsToCoursesBl()
        {
            _repository = new EnrolledStudentsToCoursesRepository();
        }
        public void Add(EnrolledStudentsToCourseBusiness entity)
        {
            var entityDb = Mapper.Map<EnrolledStudentsToCourses>(entity);
            _repository.Add(entityDb);
        }

        public void Remove(object id)
        {
            _repository.Delete(id);
        }

        public void Update(EnrolledStudentsToCourseBusiness entity)
        {
            var entityDb = Mapper.Map<EnrolledStudentsToCourses>(entity);
            _repository.Update(entityDb, entityDb.Id);
        }

        public IEnumerable<EnrolledStudentsToCourseBusiness> GetAll()
        {
            return _repository.GetAll().ToList().Select(Mapper.Map<EnrolledStudentsToCourseBusiness>).ToList();
        }

        public EnrolledStudentsToCourseBusiness GetById(int id)
        {
            return GetAll().FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<EnrolledStudentsToCourseBusiness>  GetAllByStudentId(string id)
        {
            return GetAll().Where(x => x.StudentId == id);
        }

        public IEnumerable<EnrolledStudentsToCourseBusiness> GetAllByCourseId(int id)
        {
            return GetAll().Where(x => x.CourseId == id);
        }
    }
}
