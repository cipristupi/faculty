﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using ELearningPlatform.DataLayer.Edmx;
using ELearningPlatform.DataLayer.Repository;
using ELearningPlatform.Models.BusinessModels;

namespace ELearningPlatform.BusinessLayer
{
    public class CoursesManagementBl : IBusinessLayer<CourseBusiness>
    {
        private readonly CoursesRepository _repository;

        public CoursesManagementBl()
        {
            _repository = new CoursesRepository();
        }
        public void Add(CourseBusiness entity)
        {
            var entityDb = Mapper.Map<Courses>(entity);
            _repository.Add(entityDb);
        }

        public void Remove(object id)
        {
            _repository.Delete(id);
        }

        public void Update(CourseBusiness entity)
        {
            var entityDb = Mapper.Map<Courses>(entity);
            _repository.Update(entityDb, entityDb.Id);
        }

        public IEnumerable<CourseBusiness> GetAll()
        {
            return _repository.GetAll().ToList().Select(Mapper.Map<CourseBusiness>).ToList();
        }

        public CourseBusiness GetById(int id)
        {
            return GetAll().FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<CourseBusiness> GetAllByTeacherId(string id)
        {
            return GetAll().Where(x => x.CoordonaterId == id);
        }

        public IEnumerable<CourseBusiness> GetAllForStudent(string id)
        {
            EnrolledStudentsToCoursesBl enrolledStudentsToCoursesBl = new EnrolledStudentsToCoursesBl();
            List<int> studentEnrolledCoursesIds =
                enrolledStudentsToCoursesBl.GetAllByStudentId(id).Select(x => x.CourseId).ToList();
            return GetAll().Where(x => studentEnrolledCoursesIds.Contains(x.Id)).ToList();
        }
    }
}
