﻿using AutoMapper;
using ELearningPlatform.DataLayer.Edmx;
using ELearningPlatform.DataLayer.Repository;
using ELearningPlatform.Models.BusinessModels;
using System.Collections.Generic;
using System.Linq;
using ELearningPlatform.Models;

namespace ELearningPlatform.BusinessLayer
{
    public class DiscussionsManagementBl : IBusinessLayer<DiscussionBusiness>
    {
        private readonly DiscussionsRepository _repository;

        public DiscussionsManagementBl()
        {
            _repository = new DiscussionsRepository();
        }
        public void Add(DiscussionBusiness entity)
        {
            var entityDb = Mapper.Map<Discussions>(entity);
            _repository.Add(entityDb);
        }

        public void Remove(object id)
        {
            _repository.Delete(id);
        }

        public void Update(DiscussionBusiness entity)
        {
            var entityDb = Mapper.Map<Discussions>(entity);
            _repository.Update(entityDb, entityDb.Id);
        }

        public IEnumerable<DiscussionBusiness> GetAll()
        {
            GradeSchemasManagementBl gradeSchemasManagementBl = new GradeSchemasManagementBl();
            GradeValuesManagementBl gradeValuesManagementBl = new GradeValuesManagementBl();
            var result = _repository.GetAll().ToList().Select(Mapper.Map<DiscussionBusiness>).ToList();
            for (int i = 0; i < result.Count; i++)
            {
                result[i].SchemaName = gradeSchemasManagementBl.GetById(result[i].GradeSchemaId).Name;
                if (result[i].GradeValueId.HasValue)
                {
                    result[i].SchemaValue = gradeValuesManagementBl.GetById(result[i].GradeValueId.Value).Value;
                }
            }
            return result;
        }

        public DiscussionBusiness GetById(int id)
        {
            return GetAll().FirstOrDefault(x => x.Id == id);
        }

        public DiscussionBusiness GetByIdWithComments(int id)
        {
            return GetAllWithComments().FirstOrDefault(x => x.Id == id);
        }


        public IEnumerable<DiscussionBusiness> GetByParentId(int parentId,int parentType)
        {
            return GetAll().Where(x => x.ParentId == parentId && x.ParentType == parentType);
        }

        public IEnumerable<DiscussionBusiness> GetAllWithComments()
        {
            CommentsManagementBl commentsManagementBl = new CommentsManagementBl();
            List<DiscussionBusiness> discussionBusinesses = new List<DiscussionBusiness>();
            foreach (var discussion in GetAll())
            {
                discussion.Comments = commentsManagementBl.GetAllByParentId(discussion.Id).ToList();
                discussionBusinesses.Add(discussion);
            }
            return discussionBusinesses;
        }

        public IEnumerable<DiscussionBusiness> GetWithCommentsByParentId(int parentId)
        {
            int parentType = (int)ParentTypes.Discussion;
            return GetAllWithComments().Where(x => x.ParentId == parentId && x.ParentType == parentType);
        }
    }
}

