﻿using AutoMapper;
using ELearningPlatform.DataLayer.Edmx;
using ELearningPlatform.DataLayer.Repository;
using ELearningPlatform.Models.BusinessModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearningPlatform.BusinessLayer
{
    public class QuestionsManagementBl : IBusinessLayer<QuestionBusiness>
    {
        private readonly QuestionsRepository _repository;

        public QuestionsManagementBl()
        {
            _repository = new QuestionsRepository();
        }

        public void Add(QuestionBusiness entity)
        {
            var entityDb = Mapper.Map<Questions>(entity);
            _repository.Add(entityDb);
        }

        public IEnumerable<QuestionBusiness> GetAll()
        {
            return _repository.GetAll().ToList().Select(Mapper.Map<QuestionBusiness>).ToList();
        }

        public void Remove(object id)
        {
            _repository.Delete(id);
        }

        public void Update(QuestionBusiness entity)
        {
            var entityDb = Mapper.Map<Questions>(entity);
            _repository.Update(entityDb, entityDb.Id);
        }

        public QuestionBusiness GetById(int id)
        {
            return GetAll().FirstOrDefault(x => x.Id == id);
        }
    }
}
