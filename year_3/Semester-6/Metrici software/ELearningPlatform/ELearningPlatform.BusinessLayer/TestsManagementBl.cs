﻿using AutoMapper;
using ELearningPlatform.DataLayer.Edmx;
using ELearningPlatform.DataLayer.Repository;
using ELearningPlatform.Models.BusinessModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearningPlatform.BusinessLayer
{
    public class TestsManagementBl : IBusinessLayer<TestBusiness>
    {
        readonly TestsRepository _repository;

        public TestsManagementBl()
        {
            _repository = new TestsRepository();
        }

        public void Add(TestBusiness entity)
        {
            var entityDb = Mapper.Map<Tests>(entity);
            _repository.Add(entityDb);
        }

        public IEnumerable<TestBusiness> GetAll()
        {
            return _repository.GetAll().Select(t => new TestBusiness()
            {
                Id = t.Id,
                CourseId = t.CourseId,
                DeadLine = t.DeadLine,
                Description = t.Description,
                EndDate = t.EndDate,
                Name = t.Name,
                CourseName = t.Courses.Name
            }).ToList();
        }

        public void Remove(object id)
        {
            _repository.Delete(id);
        }

        public void Update(TestBusiness entity)
        {
            var entityDb = Mapper.Map<Tests>(entity);
            _repository.Update(entityDb, entityDb.Id);
        }

        public TestBusiness GetById(int id)
        {
            return GetAll().FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<TestBusiness> GetAllTestByTeacherId(string teacherId)
        {
            var courseBl = new CoursesManagementBl();
            var teacherCourses = courseBl.GetAllByTeacherId(teacherId);
            var tests =
                GetAll()
                    .Where(x => teacherCourses.Select(c => c.Id).Contains(x.CourseId));
            return tests;
        }

        public IEnumerable<TestBusiness> GetAllTestsByStudentId(string studentId)
        {
            var courseBl = new CoursesManagementBl();
            var studentCourses = courseBl.GetAllForStudent(studentId);
            var tests =
                GetAll()
                    .Where(x => studentCourses.Select(c => c.Id).Contains(x.CourseId));
            return tests;
        }

        public IEnumerable<TestBusiness> GetAllByCourseId(int courseId)
        {
            var tests =
                GetAll()
                    .Where(x => x.CourseId == courseId);
            return tests;
        }
    }
}
