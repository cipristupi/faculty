﻿using AutoMapper;
using ELearningPlatform.DataLayer.Edmx;
using ELearningPlatform.DataLayer.Repository;
using ELearningPlatform.Models.BusinessModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearningPlatform.BusinessLayer
{
    public class CommentsManagementBl : IBusinessLayer<CommentBusiness>
    {
        private readonly CommentsRepository _repository;

        public CommentsManagementBl()
        {
            _repository = new CommentsRepository();
        }
        public void Add(CommentBusiness entity)
        {
            var entityDb = Mapper.Map<Comments>(entity);
            _repository.Add(entityDb);
        }

        public void Remove(object id)
        {
            _repository.Delete(id);
        }

        public void Update(CommentBusiness entity)
        {
            var entityDb = Mapper.Map<Comments>(entity);
            _repository.Update(entityDb, entityDb.Id);
        }

        public IEnumerable<CommentBusiness> GetAll()
        {
            UsersManagementBl usersManagementBl = new UsersManagementBl();
            List<CommentBusiness> comments = new List<CommentBusiness>();
            foreach (var comment in _repository.GetAll().ToList().Select(Mapper.Map<CommentBusiness>).ToList())
            {
                comment.UserFullName = usersManagementBl.GetFullName(comment.UserId);
                comments.Add(comment);
            }
            return comments;
        }

        public CommentBusiness GetById(int id)
        {
            return GetAll().FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<CommentBusiness> GetAllByParentId(int parentId)
        {
            return GetAll().Where(x => x.ParentId == parentId);
        } 
    }
}
