﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using ELearningPlatform.DataLayer.Edmx;
using ELearningPlatform.DataLayer.Repository;
using ELearningPlatform.Models.BusinessModels;

namespace ELearningPlatform.BusinessLayer
{
    public class LectureBl : IBusinessLayer<LectureBusiness>
    {
        LectureRepository _lectureRepository;
        LectureSettingsRepository _lectureSettingsRepository;
        PageRepository _pageRepository;
        public LectureBl()
        {
            _lectureRepository = new LectureRepository();
            _lectureSettingsRepository = new LectureSettingsRepository();
            _pageRepository = new PageRepository();
        }
        public void Add(LectureBusiness entity)
        {
            var lectures = AutoMapper.Mapper.Map<Lectures>(entity);
            _lectureRepository.Add(lectures);

        }

        public void Remove(object id)
        {
            _lectureRepository.Delete(id);
        }

        public void Update(LectureBusiness entity)
        {
            var lectures = AutoMapper.Mapper.Map<Lectures>(entity);
            _lectureRepository.Update(lectures, entity.Id);
        }

        public IEnumerable<LectureBusiness> GetAll()
        {
            return _lectureRepository.GetAll().Select(AutoMapper.Mapper.Map<LectureBusiness>).ToList();
        }

        public IEnumerable<LectureBusiness> GetAllWithCourseName()
        {
            var lectures = _lectureRepository.GetAll();
            List<LectureBusiness> lecturesBusiness = new List<LectureBusiness>();
            foreach (var lecture in lectures)
            {
                var lectureBusiness = AutoMapper.Mapper.Map<LectureBusiness>(lecture);
                lectureBusiness.CourseName = lecture.Courses.Name;
                lecturesBusiness.Add(lectureBusiness);
            }
            return lecturesBusiness;
        }

        public IEnumerable<LectureBusiness> GetAllByCourseId(int courseId)
        {
            return GetAllWithCourseName().Where(x=>x.CourseId == courseId);
        }

        public IEnumerable<LectureBusiness> GetAllByCourseIdWithPages(int courseId)
        {
            return GetAllWithPages().Where(x => x.CourseId == courseId);
        }

        public LectureBusiness GetById(int lectureId)
        {
            return GetAllWithCourseName().FirstOrDefault(x => x.Id == lectureId);
        }


        public IEnumerable<LectureBusiness> GetAllWithPages()
        {
            var lectures = _lectureRepository.GetAll();
            List<LectureBusiness> lecturesBusiness = new List<LectureBusiness>();
            foreach (var lecture in lectures)
            {
                var lectureBusiness = Mapper.Map<LectureBusiness>(lecture);
                lectureBusiness.Pages = lecture.Pages.Select(Mapper.Map<PageBusiness>).ToList();
                lecturesBusiness.Add(lectureBusiness);
            }
            return lecturesBusiness;
        }
    }
}
