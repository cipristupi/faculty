﻿using AutoMapper;
using ELearningPlatform.DataLayer.Edmx;
using ELearningPlatform.DataLayer.Repository;
using ELearningPlatform.Models.BusinessModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearningPlatform.BusinessLayer
{
    public class QuestionTypeManagementBl : IBusinessLayer<QuestionTypeBusiness>
    {
        QuestionTypesRepository _repository;

        public QuestionTypeManagementBl()
        {
            _repository = new QuestionTypesRepository();
        }
        public void Add(QuestionTypeBusiness entity)
        {
            var entityDb = Mapper.Map<QuestionTypes>(entity);
            _repository.Add(entityDb);
        }

        public IEnumerable<QuestionTypeBusiness> GetAll()
        {
            return _repository.GetAll().ToList().Select(Mapper.Map<QuestionTypeBusiness>).ToList();
        }

        public void Remove(object id)
        {
            _repository.Delete(id);
        }

        public void Update(QuestionTypeBusiness entity)
        {
            var entityDb = Mapper.Map<QuestionTypes>(entity);
            _repository.Update(entityDb, entityDb.Id);
        }

        public QuestionTypeBusiness GetById(int id)
        {
            return GetAll().FirstOrDefault(x => x.Id == id);
        }
    }
}
