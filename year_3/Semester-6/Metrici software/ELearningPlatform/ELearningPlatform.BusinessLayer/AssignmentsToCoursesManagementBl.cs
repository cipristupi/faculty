﻿using AutoMapper;
using ELearningPlatform.DataLayer.Edmx;
using ELearningPlatform.DataLayer.Repository;
using ELearningPlatform.Models.BusinessModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearningPlatform.BusinessLayer
{
    public class AssignmentsToCoursesManagementBl : IBusinessLayer<AssignmentsToCoursesBusiness>
    {
        private readonly AssignmentsToCoursesRepository _repository;

        public AssignmentsToCoursesManagementBl()
        {
            _repository = new AssignmentsToCoursesRepository();
        }
        public void Add(AssignmentsToCoursesBusiness entity)
        {
            var entityDb = Mapper.Map<AssignmentsToCourses>(entity);
            _repository.Add(entityDb);
        }

        public void Remove(object id)
        {
            _repository.Delete(id);
        }

        public void Update(AssignmentsToCoursesBusiness entity)
        {
            var entityDb = Mapper.Map<AssignmentsToCourses>(entity);
            _repository.Update(entityDb, entityDb.Id);
        }

        public IEnumerable<AssignmentsToCoursesBusiness> GetAll()
        {
            return _repository.GetAll().ToList().Select(Mapper.Map<AssignmentsToCoursesBusiness>).ToList();
        }

        public AssignmentsToCoursesBusiness GetById(int id)
        {
            return GetAll().FirstOrDefault(x => x.Id == id);
        }
    }
}
