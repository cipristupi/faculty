﻿using AutoMapper;
using ELearningPlatform.DataLayer.Edmx;
using ELearningPlatform.DataLayer.Repository;
using ELearningPlatform.Models.BusinessModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearningPlatform.BusinessLayer
{
    public class StudentsToWorkingGroupsBl : IBusinessLayer<StudentsToWorkingGroupBusiness>
    {
        private readonly StudentsToWorkingGroupsRepository _repository;

        public StudentsToWorkingGroupsBl()
        {
            _repository = new StudentsToWorkingGroupsRepository();
        }
        public void Add(StudentsToWorkingGroupBusiness entity)
        {
            var entityDb = Mapper.Map<StudentsToWorkingGroups>(entity);
            _repository.Add(entityDb);
        }

        public void Remove(object id)
        {
            _repository.Delete(id);
        }

        public void Update(StudentsToWorkingGroupBusiness entity)
        {
            var entityDb = Mapper.Map<StudentsToWorkingGroups>(entity);
            _repository.Update(entityDb, entityDb.Id);
        }

        public IEnumerable<StudentsToWorkingGroupBusiness> GetAll()
        {
            return _repository.GetAll().ToList().Select(Mapper.Map<StudentsToWorkingGroupBusiness>).ToList();
        }

        public StudentsToWorkingGroupBusiness GetById(int id)
        {
            return GetAll().FirstOrDefault(x => x.Id == id);
        }
    }
}
