﻿using AutoMapper;
using ELearningPlatform.DataLayer.Edmx;
using ELearningPlatform.DataLayer.Repository;
using ELearningPlatform.Models.BusinessModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearningPlatform.BusinessLayer
{
    public class GradeValuesManagementBl:IBusinessLayer<GradeValueBusiness>
    {
        private readonly GradeValuesRepository _repository;

        public GradeValuesManagementBl()
        {
            _repository = new GradeValuesRepository();
        }
        public void Add(GradeValueBusiness entity)
        {
            var entityDb = Mapper.Map<GradeValues>(entity);
            _repository.Add(entityDb);
        }

        public void Remove(object id)
        {
            _repository.Delete(id);
        }

        public void Update(GradeValueBusiness entity)
        {
            var entityDb = Mapper.Map<GradeValues>(entity);
            _repository.Update(entityDb, entityDb.Id);
        }

        public IEnumerable<GradeValueBusiness> GetAll()
        {
            return _repository.GetAll().ToList().Select(Mapper.Map<GradeValueBusiness>).ToList();
        }

        public GradeValueBusiness GetById(int id)
        {
            return GetAll().FirstOrDefault(x => x.Id == id);
        }
    }
}
