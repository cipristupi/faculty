﻿using AutoMapper;
using ELearningPlatform.DataLayer.Edmx;
using ELearningPlatform.DataLayer.Repository;
using ELearningPlatform.Models.BusinessModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearningPlatform.BusinessLayer
{
    public class AssignmentsManagementBl : IBusinessLayer<AssignmentBusiness>
    {
        private readonly AssignmentsRepository _repository;

        public AssignmentsManagementBl()
        {
            _repository = new AssignmentsRepository();
        }
        public void Add(AssignmentBusiness entity)
        {
            var entityDb = Mapper.Map<Assignments>(entity);
            _repository.Add(entityDb);
        }

        public void Remove(object id)
        {
            _repository.Delete(id);
        }

        public void Update(AssignmentBusiness entity)
        {
            var entityDb = Mapper.Map<Assignments>(entity);
            _repository.Update(entityDb, entityDb.Id);
        }

        public IEnumerable<AssignmentBusiness> GetAll()
        {
            return _repository.GetAll().ToList().Select(Mapper.Map<AssignmentBusiness>).ToList();
        }

        public AssignmentBusiness GetById(int id)
        {
            return GetAll().FirstOrDefault(x => x.Id == id);
        }
    }
}
