﻿using AutoMapper;
using ELearningPlatform.DataLayer.Edmx;
using ELearningPlatform.DataLayer.Repository;
using ELearningPlatform.Models.BusinessModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearningPlatform.BusinessLayer
{
    public class WorkingGroupsManagementBl : IBusinessLayer<WorkingGroupBusiness>
    {
        private readonly WorkingGroupsRepository _repository;

        public WorkingGroupsManagementBl()
        {
            _repository = new WorkingGroupsRepository();
        }
        public void Add(WorkingGroupBusiness entity)
        {
            var entityDb = Mapper.Map<WorkingGroups>(entity);
            _repository.Add(entityDb);
        }

        public void Remove(object id)
        {
            _repository.Delete(id);
        }

        public void Update(WorkingGroupBusiness entity)
        {
            var entityDb = Mapper.Map<WorkingGroups>(entity);
            _repository.Update(entityDb, entityDb.Id);
        }

        public IEnumerable<WorkingGroupBusiness> GetAll()
        {
            return _repository.GetAll().ToList().Select(Mapper.Map<WorkingGroupBusiness>).ToList();
        }

        public WorkingGroupBusiness GetById(int id)
        {
            return GetAll().FirstOrDefault(x => x.Id == id);
        }
    }
}
