﻿using AutoMapper;
using ELearningPlatform.DataLayer.Edmx;
using ELearningPlatform.DataLayer.Repository;
using ELearningPlatform.Models.BusinessModels;
using System.Collections.Generic;
using System.Linq;

namespace ELearningPlatform.BusinessLayer
{
    public class BoolQuestionsManagementBl : IBusinessLayer<BoolQuestionBusiness>
    {
        BoolQuestionsRepository _repository;

        public BoolQuestionsManagementBl()
        {
            _repository = new BoolQuestionsRepository();
        }

        public void Add(BoolQuestionBusiness entity)
        {
            var entityDb = Mapper.Map<BoolQuestions>(entity);
            _repository.Add(entityDb);
        }

        public IEnumerable<BoolQuestionBusiness> GetAll()
        {
            return _repository.GetAll().ToList().Select(Mapper.Map<BoolQuestionBusiness>).ToList();
        }

        public void Remove(object id)
        {
            _repository.Delete(id);
        }

        public void Update(BoolQuestionBusiness entity)
        {
            var entityDb = Mapper.Map<BoolQuestions>(entity);
            _repository.Update(entityDb, entityDb.QuestionId);
        }

        public BoolQuestionBusiness GetById(int id)
        {
            return GetAll().FirstOrDefault(x => x.QuestionId == id);
        }
    }
}
