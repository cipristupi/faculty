﻿using System;
using System.Collections.Generic;
using ELearningPlatform.Models.BusinessModels;
using ELearningPlatform.DataLayer.Repository;
using ELearningPlatform.DataLayer.Edmx;
using AutoMapper;
using System.Linq;
using ELearningPlatform.DataLayer;

namespace ELearningPlatform.BusinessLayer
{
    public class TestSettingsBl : IBusinessLayer<TestSettingBusiness>
    {
        private readonly TestSettingsRepository _repository;

        public TestSettingsBl()
        {
            _repository = new TestSettingsRepository();
        }

        public void Add(TestSettingBusiness entity)
        {
            var entityDb = Mapper.Map<TestSettings>(entity);
            _repository.Add(entityDb);
        }

        public IEnumerable<TestSettingBusiness> GetAll()
        {
            return _repository.GetAll().ToList().Select(Mapper.Map<TestSettingBusiness>).ToList();
        }

        public void Remove(object id)
        {
            _repository.Delete(id);
        }

        public void Update(TestSettingBusiness entity)
        {
            var entityDb = Mapper.Map<TestSettings>(entity);
            _repository.Update(entityDb, entityDb.Id);
        }

        public TestSettingBusiness GetById(int id)
        {
            return GetAll().FirstOrDefault(x => x.Id == id);
        }
    }
}
