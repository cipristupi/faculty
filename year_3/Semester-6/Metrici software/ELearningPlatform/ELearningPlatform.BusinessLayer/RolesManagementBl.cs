﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using ELearningPlatform.DataLayer.Edmx;
using ELearningPlatform.DataLayer.Repository;
using ELearningPlatform.Models.BusinessModels;

namespace ELearningPlatform.BusinessLayer
{
    public class RolesManagementBl: IBusinessLayer<RoleBusiness>
    { 
        private readonly RolesRepository _repository;
        public RolesManagementBl(RolesRepository repository)
        {
            _repository = repository;
        }

        public RolesManagementBl()
        {
            _repository = new RolesRepository();
        }
        public void Add(RoleBusiness model)
        {
            var role = Mapper.Map<AspNetRoles>(model);
            _repository.Add(role);
        }

        public void Remove(object id)
        {
            _repository.Delete(id);
        }

        public void Update(RoleBusiness model)
        {
            var role = Mapper.Map<AspNetRoles>(model);
            _repository.Update(role, 0);
        }

        public IEnumerable<RoleBusiness> GetAll()
        {
            return _repository.GetAll().ToList().Select(role => Mapper.Map<RoleBusiness>(role)).ToList();
        }

        public RoleBusiness GetById(string id)
        {
            return GetAll().FirstOrDefault(x => x.Id == id);
        }
    }
}
