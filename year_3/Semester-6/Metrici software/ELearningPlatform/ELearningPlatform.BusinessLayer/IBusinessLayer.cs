﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearningPlatform.BusinessLayer
{
    interface IBusinessLayer <T> where T:class
    {
        void Add(T entity);

        void Remove(object entity);

        void Update(T entity);

        IEnumerable<T> GetAll();
    }
}
