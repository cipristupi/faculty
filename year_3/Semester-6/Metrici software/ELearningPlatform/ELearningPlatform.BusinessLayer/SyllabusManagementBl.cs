﻿using AutoMapper;
using ELearningPlatform.DataLayer.Edmx;
using ELearningPlatform.DataLayer.Repository;
using ELearningPlatform.Models.BusinessModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearningPlatform.BusinessLayer
{
    public class SyllabusManagementBl : IBusinessLayer<SyllabusBusiness>
    {
        private readonly SyllabusRepository _repository;

        public SyllabusManagementBl()
        {
            _repository = new SyllabusRepository();
        }

        public void Add(SyllabusBusiness entity)
        {
            var entityDb = Mapper.Map<Syllabus>(entity);
            _repository.Add(entityDb);
        }

        public void Remove(object id)
        {
            _repository.Delete(id);
        }

        public void Update(SyllabusBusiness entity)
        {
            var entityDb = Mapper.Map<Syllabus>(entity);
            _repository.Update(entityDb, entityDb.Id);
        }

        public IEnumerable<SyllabusBusiness> GetAll()
        {
            return _repository.GetAll()
                    .Select(x => new SyllabusBusiness()
                    {
                        CourseId = x.CourseId,
                        CourseName = x.Courses.Name,
                        Id = x.Id,
                        Name = x.Name,
                        SyllabusContent = x.SyllabusContent
                    }).ToList();
        }

        public SyllabusBusiness GetById(int id)
        {
            return GetAll().FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<SyllabusBusiness> GetAllByUserId(string userId)
        {
            CoursesManagementBl coursesManagementBl = new CoursesManagementBl();
            var courses = coursesManagementBl.GetAllByTeacherId(userId).Select(x => x.Id);
            return
                _repository.GetAll()
                    .Where(x => courses.Contains(x.CourseId))
                    .Select(x => new SyllabusBusiness()
                    {
                        CourseId = x.CourseId,
                        CourseName = x.Courses.Name,
                        Id = x.Id,
                        Name = x.Name,
                        SyllabusContent = x.SyllabusContent
                    }).ToList();
        }

        public IEnumerable<SyllabusBusiness> GetAllByCourseId(int courseId)
        {
            return
                _repository.GetAll()
                    .Where(x => x.CourseId == courseId)
                    .Select(x => new SyllabusBusiness()
                    {
                        CourseId = x.CourseId,
                        CourseName = x.Courses.Name,
                        Id = x.Id,
                        Name = x.Name,
                        SyllabusContent = x.SyllabusContent
                    }).ToList();
        }
    }
}
