﻿using AutoMapper;
using ELearningPlatform.DataLayer.Edmx;
using ELearningPlatform.DataLayer.Repository;
using ELearningPlatform.Models.BusinessModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearningPlatform.BusinessLayer
{
    public class AssignmentsSettingsManagementBl:IBusinessLayer<AssignmentSettingBusiness>
    {
        private readonly AssignmentsSettingsRepository _repository;

        public AssignmentsSettingsManagementBl()
        {
            _repository = new AssignmentsSettingsRepository();
        }
        public void Add(AssignmentSettingBusiness entity)
        {
            var entityDb = Mapper.Map<AssignmentsSettings>(entity);
            _repository.Add(entityDb);
        }

        public void Remove(object id)
        {
            _repository.Delete(id);
        }

        public void Update(AssignmentSettingBusiness entity)
        {
            var entityDb = Mapper.Map<AssignmentsSettings>(entity);
            _repository.Update(entityDb, entityDb.Id);
        }

        public IEnumerable<AssignmentSettingBusiness> GetAll()
        {
            return _repository.GetAll().ToList().Select(Mapper.Map<AssignmentSettingBusiness>).ToList();
        }

        public AssignmentSettingBusiness GetById(int id)
        {
            return GetAll().FirstOrDefault(x => x.Id == id);
        }
    }
}
