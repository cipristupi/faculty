﻿using AutoMapper;
using ELearningPlatform.DataLayer.Edmx;
using ELearningPlatform.DataLayer.Repository;
using ELearningPlatform.Models.BusinessModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearningPlatform.BusinessLayer
{
    public class AssignmentsCategoriesManagementBl: IBusinessLayer<AssignmentCategoryBusiness>
    {
        private readonly AssignmentsCategoriesRepository _repository;

        public AssignmentsCategoriesManagementBl()
        {
            _repository = new AssignmentsCategoriesRepository();
        }
        public void Add(AssignmentCategoryBusiness entity)
        {
            var entityDb = Mapper.Map<AssignmentsCategories>(entity);
            _repository.Add(entityDb);
        }

        public void Remove(object id)
        {
            _repository.Delete(id);
        }

        public void Update(AssignmentCategoryBusiness entity)
        {
            var entityDb = Mapper.Map<AssignmentsCategories>(entity);
            _repository.Update(entityDb, entityDb.Id);
        }

        public IEnumerable<AssignmentCategoryBusiness> GetAll()
        {
            return _repository.GetAll().ToList().Select(Mapper.Map<AssignmentCategoryBusiness>).ToList();
        }

        public AssignmentCategoryBusiness GetById(int id)
        {
            return GetAll().FirstOrDefault(x => x.Id == id);
        }
    }
}
