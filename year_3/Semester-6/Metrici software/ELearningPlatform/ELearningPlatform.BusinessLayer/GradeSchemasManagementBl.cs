﻿using AutoMapper;
using ELearningPlatform.DataLayer.Edmx;
using ELearningPlatform.DataLayer.Repository;
using ELearningPlatform.Models.BusinessModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearningPlatform.BusinessLayer
{
    public class GradeSchemasManagementBl : IBusinessLayer<GradeSchemaBusiness>
    {
        private readonly GradeSchemasRepository _repository;

        public GradeSchemasManagementBl()
        {
            _repository = new GradeSchemasRepository();
        }
        public void Add(GradeSchemaBusiness entity)
        {
            var entityDb = Mapper.Map<GradeSchemas>(entity);
            _repository.Add(entityDb);
        }

        public void Remove(object id)
        {
            _repository.Delete(id);
        }

        public void Update(GradeSchemaBusiness entity)
        {
            var entityDb = Mapper.Map<GradeSchemas>(entity);
            _repository.Update(entityDb, entityDb.Id);
        }

        public IEnumerable<GradeSchemaBusiness> GetAll()
        {
            return _repository.GetAll().ToList().Select(Mapper.Map<GradeSchemaBusiness>).ToList();
        }

        public GradeSchemaBusiness GetById(int id)
        {
            return GetAll().FirstOrDefault(x => x.Id == id);
        }
    }
}
