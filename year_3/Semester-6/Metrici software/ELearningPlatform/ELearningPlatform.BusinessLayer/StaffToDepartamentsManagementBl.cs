﻿using AutoMapper;
using ELearningPlatform.DataLayer.Edmx;
using ELearningPlatform.DataLayer.Repository;
using ELearningPlatform.Models.BusinessModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper.Internal;

namespace ELearningPlatform.BusinessLayer
{
    public class StaffToDepartamentsManagementBl : IBusinessLayer<StaffToDepartamentBusiness>
    {
        private readonly StaffToDepartamentsRepository _repository;

        public StaffToDepartamentsManagementBl()
        {
            _repository = new StaffToDepartamentsRepository();
        }
        public void Add(StaffToDepartamentBusiness entity)
        {
            var entityDb = Mapper.Map<StaffToDepartaments>(entity);
            _repository.Add(entityDb);
        }

        public void Remove(object id)
        {
            _repository.Delete(id);
        }

        public void Update(StaffToDepartamentBusiness entity)
        {
            var entityDb = Mapper.Map<StaffToDepartaments>(entity);
            _repository.Update(entityDb, entityDb.Id);
        }

        public IEnumerable<StaffToDepartamentBusiness> GetAll()
        {
            return _repository.GetAll().Select(x => new StaffToDepartamentBusiness()
            {
                DepartmentId = x.DepartmentId,
                Id = x.Id,
                UniversityStaffId = x.UniversityStaffId,
                TeacherName = x.UniversityStaff.AspNetUsers.FirstName + x.UniversityStaff.AspNetUsers.LastName,
                DepartmentName = x.Departments.Name
            });
        }

        public StaffToDepartamentBusiness GetById(int id)
        {
            return GetAll().FirstOrDefault(x => x.Id == id);
        }
    }
}
