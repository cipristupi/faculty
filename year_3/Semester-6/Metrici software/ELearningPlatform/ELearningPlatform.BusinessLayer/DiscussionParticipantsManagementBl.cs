﻿using AutoMapper;
using ELearningPlatform.DataLayer.Edmx;
using ELearningPlatform.DataLayer.Repository;
using ELearningPlatform.Models.BusinessModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearningPlatform.BusinessLayer
{
    public class DiscussionParticipantsManagementBl:IBusinessLayer<DiscussionParticipantsBusiness>
    {
        private readonly DiscussionParticipantsRepository _repository;
        public DiscussionParticipantsManagementBl()
         {
            _repository = new DiscussionParticipantsRepository();
        }

        public void Add(DiscussionParticipantsBusiness entity)
        {
            var entityDb = Mapper.Map<DiscussionParticipants>(entity);
            _repository.Add(entityDb);
        }

        public void Remove(object id)
        {
            _repository.Delete(id);
        }

        public void Update(DiscussionParticipantsBusiness entity)
        {
            var entityDb = Mapper.Map<DiscussionParticipants>(entity);
            _repository.Update(entityDb, entityDb.Id);
        }

        public IEnumerable<DiscussionParticipantsBusiness> GetAll()
        {
            return _repository.GetAll().ToList().Select(Mapper.Map<DiscussionParticipantsBusiness>).ToList();
        }
        public DiscussionParticipantsBusiness GetById(int id)
        {
            return GetAll().FirstOrDefault(x => x.Id == id);
        }
    }
}
