﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using ELearningPlatform.DataLayer.Repository;
using ELearningPlatform.Models;
using ELearningPlatform.Models.BusinessModels;

namespace ELearningPlatform.BusinessLayer
{
    public class UsersManagementBl
    {
        private readonly UsersRepository _repository;

        public UsersManagementBl(UsersRepository repository)
        {
            _repository = repository;
        }

        public UsersManagementBl()
        {
            _repository = new UsersRepository();
        }

        public IEnumerable<UserBusiness> GetAllTeachers()
        {
            return
                _repository.GetAllTeachers()
                    .Where(
                        t =>
                            t.IsSuspended == false &&
                            t.AspNetRoles.Count(x => x.Name == UserRolesEnum.Teacher.ToString()) != 0)
                    .Select(Mapper.Map<UserBusiness>)
                    .AsEnumerable();
        }


        public UserBusiness GetById(string id)
        {
            return Mapper.Map<UserBusiness>(_repository.GetAll().FirstOrDefault(u => u.Id == id));
        }

        public string GetFullName(string id)
        {
            return _repository.GetFullName(id);
        }

        public IEnumerable<UserBusiness> GetAllStudents()
        {
            return
                _repository.GetAllStudents()
                    .Where(
                        t =>
                            t.IsSuspended == false &&
                            t.AspNetRoles.Count(x => x.Name == UserRolesEnum.Student.ToString()) != 0)
                    .Select(Mapper.Map<UserBusiness>)
                    .AsEnumerable();
        }
    }
}
