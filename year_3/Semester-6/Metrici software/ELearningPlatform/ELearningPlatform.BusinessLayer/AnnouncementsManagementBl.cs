﻿using AutoMapper;
using ELearningPlatform.DataLayer.Edmx;
using ELearningPlatform.DataLayer.Repository;
using ELearningPlatform.Models.BusinessModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ELearningPlatform.Models;

namespace ELearningPlatform.BusinessLayer
{
    public class AnnouncementsManagementBl : IBusinessLayer<AnnouncementBusiness>
    {
        private readonly AnnouncementsRepository _repository;

        public AnnouncementsManagementBl()
        {
            _repository = new AnnouncementsRepository();
        }

        public void Add(AnnouncementBusiness entity)
        {
            var entityDb = Mapper.Map<Announcements>(entity);
            _repository.Add(entityDb);
        }

        public void Remove(object id)
        {
            _repository.Delete(id);
        }

        public void Update(AnnouncementBusiness entity)
        {
            var entityDb = Mapper.Map<Announcements>(entity);
            _repository.Update(entityDb, entityDb.Id);
        }

        public IEnumerable<AnnouncementBusiness> GetAll()
        {
            return _repository.GetAll().Select(x => new AnnouncementBusiness
            {
                Id = x.Id,
                ParentId = x.ParentId,
                ParentType = x.ParentType,
                AnnouncementContent = x.AnnouncementContent,
                Name = x.Name,
                UserId = x.UserId,
                DateCreated = x.DateCreated,
                UserName = x.AspNetUsers.UserName,
            }).ToList();
        }

        public AnnouncementBusiness GetById(int id)
        {
            return GetAll().FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<AnnouncementBusiness> GetAllGeneraleAnnouncement()
        {
            return
                GetAll().Where(x => x.ParentType == null);
        }

        public IEnumerable<AnnouncementBusiness> GetAllAnnouncementRelatedToCourses(string userId)
        {
            //var enrolledStudentsToCoursesBl = new EnrolledStudentsToCoursesBl();
            IEnumerable<int?> studentsCourse = GetAllByStudentId(userId).Select(x => x.CourseId).ToList() as IEnumerable<int?>;
            //enrolledStudentsToCoursesBl.GetAllByStudentId(userId).Select(x => x.CourseId).ToList() as
            //        IEnumerable<int?>;
            var announcementsRelatedToCourses = studentsCourse != null
                ? GetAll().Where(x => studentsCourse.Contains(x.ParentId) && x.ParentType == (int) ParentTypes.Course)
                : new List<AnnouncementBusiness>();
            return announcementsRelatedToCourses;
        }

        public IEnumerable<AnnouncementBusiness> GetAllAnnouncementsByCourseId(int id)
        {
            return
               GetAll().Where(x => x.ParentType == (int?)ParentTypes.Course && x.ParentId == id);
        }

        public IEnumerable<EnrolledStudentsToCourseBusiness> GetAllByStudentId(string id)
        {
            EnrolledStudentsToCoursesRepository repository = new EnrolledStudentsToCoursesRepository();
            return repository.GetAll().Select(Mapper.Map<EnrolledStudentsToCourseBusiness>).Where(x => x.StudentId == id);
        }
    }
}
