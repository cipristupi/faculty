﻿using AutoMapper;
using ELearningPlatform.DataLayer.Edmx;
using ELearningPlatform.DataLayer.Repository;
using ELearningPlatform.Models.BusinessModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearningPlatform.BusinessLayer
{
    public class PageSectionsBl : IBusinessLayer<PageSectionBusiness>
    {
        private readonly PageSectionsRepository _repository;

        public PageSectionsBl()
        {
            _repository = new PageSectionsRepository();
        }

        public void Add(PageSectionBusiness entity)
        {
            var entityDb = Mapper.Map<PageSections>(entity);
            _repository.Add(entityDb);
        }

        public IEnumerable<PageSectionBusiness> GetAll()
        {
            return _repository.GetAll().ToList().Select(Mapper.Map<PageSectionBusiness>).ToList();
        }

        public void Remove(object id)
        {
            _repository.Delete(id);
        }

        public void Update(PageSectionBusiness entity)
        {
            var entityDb = Mapper.Map<PageSections>(entity);
            _repository.Update(entityDb, entityDb.Id);
        }

        public PageSectionBusiness GetById(int id)
        {
            return GetAll().FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<PageSectionBusiness> GetAllByPage(int pageid)
        {
            return _repository.GetAll().Where(x=>x.PageId==pageid).Select(Mapper.Map<PageSectionBusiness>).ToList();
        }

        public void AddPages(List<PageSectionBusiness> pages, int pageId)
        {
            foreach (var pageSectionBusiness in pages)
            {
                pageSectionBusiness.PageId = pageId;
                Add(pageSectionBusiness);
            }
        }

        public void UpdatePages(List<PageSectionBusiness> pageSectionsBusiness, int pageId)
        {
            RemoveAllPageSectionForPage(pageId);
            foreach (var pageSectionBusiness in pageSectionsBusiness)
            {
                pageSectionBusiness.PageId = pageId;
                Add(pageSectionBusiness);
            }
        }

        private void RemoveAllPageSectionForPage(int pageId)
        {
            var pages = GetAllByPage(pageId).Select(x=>x.Id);
            foreach (var page in pages)
            {
                Remove(page);
            }
        }
    }
}
