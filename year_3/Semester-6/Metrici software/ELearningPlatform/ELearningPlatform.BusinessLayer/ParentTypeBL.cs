﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ELearningPlatform.Models;

namespace ELearningPlatform.BusinessLayer
{
    public class ParentTypeBl
    {
        public ParentTypeBl()
        {

        }

        public string GetNameForParent(int parentId, int parentType)
        {
            string parentName = string.Empty;
            switch (parentType)
            {
                case (int)ParentTypes.Announcements:
                    {
                        var announcementBl = new AnnouncementsManagementBl();
                        var announcement = announcementBl.GetById(parentId);
                        if (announcement != null)
                        {
                            parentName = announcement.Name;
                        }
                        break;
                    }
                case (int)ParentTypes.Discussion:
                    {
                        var discussionBl = new DiscussionsManagementBl();
                        var discussion = discussionBl.GetById(parentId);
                        if (discussion != null)
                        {
                            parentName = discussion.Name;
                        }
                        break;
                    }
                case (int)ParentTypes.Course:
                    {
                        var courseBl = new CoursesManagementBl();
                        var course = courseBl.GetById(parentId);
                        if (course != null)
                        {
                            parentName = course.Name;
                        }
                        break;
                    }
            }
            return parentName;
        }
    }
}
