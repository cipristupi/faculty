﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using ELearningPlatform.DataLayer.Edmx;
using ELearningPlatform.DataLayer.Repository;
using ELearningPlatform.Models.BusinessModels;

namespace ELearningPlatform.BusinessLayer
{
    public class DepartmentsManagementBl : IBusinessLayer<DepartmentBusiness>
    {
        private readonly DepartmentsRepository _repository;

        public DepartmentsManagementBl()
        {
                _repository =new DepartmentsRepository();
        }
        public void Add(DepartmentBusiness entity)
        {
            var entityDb = Mapper.Map<Departments>(entity);
            _repository.Add(entityDb);
        }

        public void Remove(object id)
        {
            _repository.Delete(id);
        }

        public void Update(DepartmentBusiness entity)
        {
            var entityDb = Mapper.Map<Departments>(entity);
            _repository.Update(entityDb,entityDb.Id);
        }

        public IEnumerable<DepartmentBusiness> GetAll()
        {
            return _repository.GetAll().ToList().Select(Mapper.Map<DepartmentBusiness>).ToList();
        }

        public DepartmentBusiness GetById(int id)
        {
            return GetAll().FirstOrDefault(x => x.Id == id);
        }
    }
}
