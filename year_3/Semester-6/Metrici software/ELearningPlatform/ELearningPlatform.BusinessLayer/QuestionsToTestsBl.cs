﻿using AutoMapper;
using ELearningPlatform.DataLayer.Edmx;
using ELearningPlatform.DataLayer.Repository;
using ELearningPlatform.Models.BusinessModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearningPlatform.BusinessLayer
{
    public class QuestionsToTestsBl : IBusinessLayer<QuestionsToTestsBusiness>
    {
        readonly QuestionsToTestsRepository _repository;

        public QuestionsToTestsBl()
        {
            _repository = new QuestionsToTestsRepository();
        }

        public void Add(QuestionsToTestsBusiness entity)
        {
            var entityDb = Mapper.Map<QuestionsToTests>(entity);
            _repository.Add(entityDb);
        }

        public IEnumerable<QuestionsToTestsBusiness> GetAll()
        {
            return _repository.GetAll().ToList().Select(Mapper.Map<QuestionsToTestsBusiness>).ToList();
        }

        public void Remove(object id)
        {
            _repository.Delete(id);
        }

        public void Update(QuestionsToTestsBusiness model)
        {
            var entityDb = Mapper.Map<QuestionsToTests>(model);
            _repository.Update(entityDb, entityDb.Id);
        }

        public QuestionsToTestsBusiness GetById(int id)
        {
            return GetAll().FirstOrDefault(x => x.Id == id);
        }
    }
}
