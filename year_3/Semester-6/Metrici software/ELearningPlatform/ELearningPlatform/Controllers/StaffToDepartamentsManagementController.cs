﻿using AutoMapper;
using ELearningPlatform.BusinessLayer;
using ELearningPlatform.Controllers.Shared;
using ELearningPlatform.Models.BusinessModels;
using ELearningPlatform.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace ELearningPlatform.Controllers
{
    public class StaffToDepartamentsManagementController : BaseController
    {
        private readonly StaffToDepartamentsManagementBl _staffToDepartamentsManagementBl = new StaffToDepartamentsManagementBl();
        private readonly DepartmentsManagementBl _departmentsManagementBl = new DepartmentsManagementBl();
        public ActionResult StaffToDepartamentsIndex()
        {
            List<StaffToDepartamentViewModel> staffToDepartaments = new List<StaffToDepartamentViewModel>();
            foreach (var item in _staffToDepartamentsManagementBl.GetAll())
            {
                staffToDepartaments.Add(Mapper.Map<StaffToDepartamentViewModel>(item));
            }
            return View("StaffToDepartamentsIndex", staffToDepartaments);
        }

        public ActionResult StaffToDepartamentsCreate(int? departmentId)
        {
            var model = new StaffToDepartamentViewModel();
            if (departmentId.HasValue)
            {
                model.DepartmentId = departmentId.Value;
                model.DepartmentName = _departmentsManagementBl.GetById(departmentId.Value).Name;
                model.UniversityStaff = GetUniversityStaff(departmentId.Value);
                model.Teachers = GetTeachers(departmentId.Value);
            }
            else
            {
                model.UniversityStaff = GetUniversityStaff(0);
                model.Teachers = GetTeachers(0);
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult StaffToDepartamentsCreate(StaffToDepartamentViewModel model)
        {
            var staffToDepartamentsBusiness = new StaffToDepartamentBusiness()
            {
                UniversityStaffId = model.UniversityStaffId,
                DepartmentId = model.DepartmentId,
            };
            _staffToDepartamentsManagementBl.Add(staffToDepartamentsBusiness);
            return RedirectToAction("StaffToDepartamentsCreate", new { departmentId = model.DepartmentId });
        }


        public ActionResult StaffToDepartamentsEdit(int id)
        {
            var staffToDepartamentsViewModel = Mapper.Map<StaffToDepartamentViewModel>(_staffToDepartamentsManagementBl.GetById(id));
            return View(staffToDepartamentsViewModel);
        }

        [HttpPost]
        public ActionResult StaffToDepartamentsEdit(int id, StaffToDepartamentViewModel model)
        {
            var asToCrBusiness = Mapper.Map<StaffToDepartamentBusiness>(model);
            _staffToDepartamentsManagementBl.Update(asToCrBusiness);
            return RedirectToAction("StaffToDepartamentsIndex");
        }


        public ActionResult Delete(int id, int departmentId)
        {
            _staffToDepartamentsManagementBl.Remove(id);
            return RedirectToAction("StaffToDepartamentsCreate", new { departmentId = departmentId });
        }


        private List<SelectListItem> GetUniversityStaff(int departmentId)
        {
            var userManagementBl = new UsersManagementBl();
            var universityStaffForDepartment =
                _staffToDepartamentsManagementBl.GetAll().Where(x => x.DepartmentId == departmentId).AsEnumerable();
            var allTeachers = userManagementBl.GetAllTeachers();
            var teachersFromDepartment = allTeachers
                     .Where(x => universityStaffForDepartment.Select(y => y.UniversityStaffId).Contains(x.Id));
            var items = teachersFromDepartment.Select(t => new SelectListItem()
            {
                Text = string.Format("{0} {1}", t.FirstName, t.LastName),
                Value = universityStaffForDepartment.FirstOrDefault(
                    u => u.UniversityStaffId == t.Id && u.DepartmentId == departmentId)?.Id.ToString()
            }).ToList();

            return items;
        }

        private List<SelectListItem> GetTeachers(int departmentId)
        {
            var userManagementBl = new UsersManagementBl();
            var universityStaffForDepartment =
                _staffToDepartamentsManagementBl.GetAll().Where(x => x.DepartmentId == departmentId);
            var items =
                userManagementBl.GetAllTeachers()
                    .Where(x => !universityStaffForDepartment.Select(y => y.UniversityStaffId).Contains(x.Id))
                    .Select(t => new SelectListItem()
                    {
                        Text = string.Format("{0} {1}", t.FirstName, t.LastName),
                        Value = t.Id
                    });

            return items.ToList();
        }
    }
}
