﻿using AutoMapper;
using ELearningPlatform.BusinessLayer;
using ELearningPlatform.Controllers.Shared;
using ELearningPlatform.Models.BusinessModels;
using ELearningPlatform.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ELearningPlatform.Controllers
{
    public class StudentsToWorkingGroupsController : BaseController
    {
        private StudentsToWorkingGroupsBl _studentsToWorkingGroupsManagementBl = new StudentsToWorkingGroupsBl();

        public ActionResult StudentsToWorkingGroupsIndex()
        {
            List<StudentsToWorkingGroupViewModel> studentsToWorkingGroups = new List<StudentsToWorkingGroupViewModel>();
            foreach (var item in _studentsToWorkingGroupsManagementBl.GetAll())
            {
                studentsToWorkingGroups.Add(Mapper.Map<StudentsToWorkingGroupViewModel>(item));
            }
            return View("StudentsToWorkingGroupsIndex", studentsToWorkingGroups);
        }

        public ActionResult StudentsToWorkingGroupsCreate()
        {
            return View();
        }

        [HttpPost]
        public ActionResult StudentsToWorkingGroupsCreate(StudentsToWorkingGroupViewModel model)
        {
            var studentsToWorkingGroupsBusiness = new StudentsToWorkingGroupBusiness()
            {
                StudentId = model.StudentId,
                WorkingGroupId = model.WorkingGroupId
            };
            _studentsToWorkingGroupsManagementBl.Add(studentsToWorkingGroupsBusiness);
            return RedirectToAction("StudentsToWorkingGroupsIndex");
        }


        public ActionResult StudentsToWorkingGroupsEdit(int id)
        {
            var StudentsToWorkingGroupViewModel = Mapper.Map<StaffToDepartamentViewModel>(_studentsToWorkingGroupsManagementBl.GetById(id));
            return View(StudentsToWorkingGroupViewModel);
        }

        [HttpPost]
        public ActionResult StudentsToWorkingGroupsEdit(int id, StaffToDepartamentViewModel model)
        {
            var business = Mapper.Map<StudentsToWorkingGroupBusiness>(model);
            _studentsToWorkingGroupsManagementBl.Update(business);
            return RedirectToAction("StudentsToWorkingGroupsIndex");
        }


        public ActionResult Delete(int id)
        {
            _studentsToWorkingGroupsManagementBl.Remove(id);
            return RedirectToAction("StudentsToWorkingGroupsIndex");
        }
    }
}
