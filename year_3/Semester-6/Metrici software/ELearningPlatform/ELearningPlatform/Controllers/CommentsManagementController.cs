﻿using AutoMapper;
using ELearningPlatform.BusinessLayer;
using ELearningPlatform.Controllers.Shared;
using ELearningPlatform.Models.BusinessModels;
using ELearningPlatform.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ELearningPlatform.Controllers
{
    public class CommentsManagementController : BaseController
    {
        private readonly CommentsManagementBl _commentsManagementBl = new CommentsManagementBl();

        public ActionResult CommentsIndex()
        {
            List<CommentViewModel> comments = new List<CommentViewModel>();
            foreach (var item in _commentsManagementBl.GetAll())
            {
                comments.Add(Mapper.Map<CommentViewModel>(item));
            }
            return View("CommentsIndex", comments);
        }


        public ActionResult CommentsCreate(int? parentId,int? parentType)
        {
            CommentViewModel model = new CommentViewModel();
            if (parentId.HasValue)
            {
                model.ParentId = parentId.Value;
            }
            if (parentType.HasValue)
            {
                model.ParentType = parentType.Value;
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult CommentsCreate(CommentViewModel model)
        {
            if (ModelState.IsValid)
            {
                CommentBusiness commentBusiness = Mapper.Map<CommentBusiness>(model);
                commentBusiness.CommentContent = Server.HtmlEncode(commentBusiness.CommentContent);
                commentBusiness.DateCreated = DateTime.Now;
                commentBusiness.UserId = LoggedUser.Id;
                _commentsManagementBl.Add(commentBusiness);
                ViewBag.Succes = true;
                return View(model);
            }
            ViewBag.ModelState = ModelState;
            return View(model);
        }


        public ActionResult CommentsEdit(int id)
        {
            var commentViewModel = Mapper.Map<CommentViewModel>(_commentsManagementBl.GetById(id));
            return View(commentViewModel);
        }

        [HttpPost]
        public ActionResult CommentsEdit(int id, CommentViewModel model)
        {
            if (ModelState.IsValid)
            {
                CommentBusiness commentBusiness = Mapper.Map<CommentBusiness>(model);
                commentBusiness.CommentContent = Server.HtmlEncode(commentBusiness.CommentContent);
                _commentsManagementBl.Update(commentBusiness);
                ViewBag.Succes = true;
                return View(model);
            }
            ViewBag.ModelState = ModelState;
            return View(model);
        }

        public ActionResult Delete(int id)
        {
            _commentsManagementBl.Remove(id);
            return RedirectToAction("CommentsIndex");
        }
    }
}
