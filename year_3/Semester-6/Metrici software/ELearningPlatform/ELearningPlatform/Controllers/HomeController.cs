﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ELearningPlatform.Controllers.Shared;
using ELearningPlatform.Models;

namespace ELearningPlatform.Controllers
{
    [Authorize]
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            if (UserRoles.Contains(UserRolesEnum.Admin))
                return RedirectToAction("AdminDashboardIndex", "Dashboards");
            if (UserRoles.Contains(UserRolesEnum.Teacher))
                return RedirectToAction("TeacherDashboardIndex", "Dashboards");
            if (UserRoles.Contains(UserRolesEnum.Student))
                return RedirectToAction("StudentDashboardIndex", "Dashboards");
            throw new HttpException(404, "");
        }
    }
}