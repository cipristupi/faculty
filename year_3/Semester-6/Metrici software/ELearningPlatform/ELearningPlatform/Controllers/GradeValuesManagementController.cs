﻿using AutoMapper;
using ELearningPlatform.BusinessLayer;
using ELearningPlatform.Controllers.Shared;
using ELearningPlatform.Models.BusinessModels;
using ELearningPlatform.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ELearningPlatform.Controllers
{
    public class GradeValuesManagementController : BaseController
    {
        private GradeValuesManagementBl _gradeValuesManagementBl = new GradeValuesManagementBl();
        private readonly GradeSchemasManagementBl _gradesSchemaBl = new GradeSchemasManagementBl();

        // GET: GradeValuesManagement
        public ActionResult GradeValuesIndex()
        {
            List<GradeValueViewModel> gradeValues = new List<GradeValueViewModel>();
            foreach (var item in _gradeValuesManagementBl.GetAll())
            {
                gradeValues.Add(Mapper.Map<GradeValueViewModel>(item));
            }
            List<GradeSchemaViewModel> gradeSchemas = new List<GradeSchemaViewModel>();
            foreach (var item in _gradesSchemaBl.GetAll())
            {
                gradeSchemas.Add(Mapper.Map<GradeSchemaViewModel>(item));
            }
            Tuple<List<GradeValueViewModel>, List<GradeSchemaViewModel>> tuple = 
                new Tuple<List<GradeValueViewModel>, List<GradeSchemaViewModel>>(gradeValues, gradeSchemas);
            return View("GradeValuesIndex", tuple);
        }


        // GET: GradeValuesManagement/Create
        public ActionResult GradeValuesCreate()
        {
            GradeValueViewModel model = new GradeValueViewModel();
            model.AvailableGradeSchemas = GetGradesSchemaAsSelectList();
            return View(model);
        }

        // POST: GradeValuesManagement/Create
        [HttpPost]
        public ActionResult GradeValuesCreate(GradeValueViewModel model)
        {
            var gradeValueBusiness = new GradeValueBusiness()
            {
                Value = model.Value,
                GradeSchemaId = model.SelectedGradeSchema,
            };
            _gradeValuesManagementBl.Add(gradeValueBusiness);
            return RedirectToAction("GradeValuesIndex");
        }

        // GET: GradeValuesManagement/Edit/5
        public ActionResult GradeValuesEdit(int id)
        {
            var gradeValueViewModel = Mapper.Map<GradeValueViewModel>(_gradeValuesManagementBl.GetById(id));
            gradeValueViewModel.AvailableGradeSchemas = GetGradesSchemaAsSelectList();
            gradeValueViewModel.SelectedGradeSchema = gradeValueViewModel.GradeSchemaId;
            return View(gradeValueViewModel);
        }

        // POST: GradeValuesManagement/Edit/5
        [HttpPost]
        public ActionResult GradeValuesEdit(int id, GradeValueViewModel model)
        {
            var gradeValueBusiness = Mapper.Map<GradeValueBusiness>(model);
            gradeValueBusiness.GradeSchemaId = model.SelectedGradeSchema;
            _gradeValuesManagementBl.Update(gradeValueBusiness);
            return RedirectToAction("GradeValuesIndex");
        }

        // POST: GradeValuesManagement/Delete/5
        public ActionResult GradeValuesDelete(int id)
        {
            _gradeValuesManagementBl.Remove(id);
            return RedirectToAction("GradeValuesIndex");
        }


        private List<SelectListItem> GetGradesSchemaAsSelectList()
        {
            return _gradesSchemaBl.GetAll().Select(x => new SelectListItem()
            {
                Value = x.Id.ToString(),
                Text = x.Name
            }).ToList();
        }
    }
}
