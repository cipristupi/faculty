﻿using System.Web.Mvc;
using ELearningPlatform.Controllers.Shared;
using ELearningPlatform.BusinessLayer;
using ELearningPlatform.Models.BusinessModels;
using ELearningPlatform.Models.ViewModels;
using AutoMapper;
using System.Linq;

namespace ELearningPlatform.Controllers
{
    public class PageSectionsController : BaseController
    {
        readonly PageSectionsBl _pageSectionBl = new PageSectionsBl();

        public ActionResult PageSectionsIndex()
        {
            var pageSections = _pageSectionBl.GetAll().Select(Mapper.Map<PageSectionViewModel>).ToList();
            return View("PageSectionsIndex", pageSections);
        }

        public ActionResult PageSectionsCreate()
        {
            PageSectionViewModel model = new PageSectionViewModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult PageSectionsCreate(PageSectionViewModel model)
        {
            var pageSectionsBusiness = new PageSectionBusiness()
            {
                Name = model.Name,
                SectionContent = model.SectionContent,
                PageId = model.PageId
            };
            _pageSectionBl.Add(pageSectionsBusiness);
            return RedirectToAction("PageSectionsIndex");
        }

        public ActionResult PageSectionsEdit(int id)
        {
            PageSectionViewModel model = new PageSectionViewModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult PageSectionsEdit(int id, PageSectionViewModel model)
        {
            var pageSectionsBusiness = Mapper.Map<PageSectionBusiness>(model);
            pageSectionsBusiness.Name = model.Name;
            pageSectionsBusiness.SectionContent = model.SectionContent;
            pageSectionsBusiness.PageId = model.PageId;
            _pageSectionBl.Update(pageSectionsBusiness);
            return RedirectToAction("PageSectionsIndex");
        }

        public ActionResult PageSectionsDelete(int id)
        {
            _pageSectionBl.Remove(id);
            return RedirectToAction("PageSectionsIndex");

        }
    }
}
