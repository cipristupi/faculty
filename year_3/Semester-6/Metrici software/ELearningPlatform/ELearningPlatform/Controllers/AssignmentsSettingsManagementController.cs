﻿using AutoMapper;
using ELearningPlatform.BusinessLayer;
using ELearningPlatform.Controllers.Shared;
using ELearningPlatform.Models.BusinessModels;
using ELearningPlatform.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ELearningPlatform.Controllers
{
    public class AssignmentsSettingsManagementController : BaseController
    {
        private AssignmentsSettingsManagementBl _assignmentsSettingsManagementBl = new AssignmentsSettingsManagementBl();
        private readonly AssignmentsManagementBl _assignmentsBl = new AssignmentsManagementBl();

        // GET: AssignmentsSettingsManagement
        public ActionResult AssignmentsSettingsIndex()
        {
            List<AssignmentSettingViewModel> assignmentsSettings = new List<AssignmentSettingViewModel>();
            foreach (var item in _assignmentsSettingsManagementBl.GetAll())
            {
                assignmentsSettings.Add(Mapper.Map<AssignmentSettingViewModel>(item));
            }
            List<AssignmentViewModel> assignments = new List<AssignmentViewModel>();
            foreach (var item in _assignmentsBl.GetAll())
            {
                assignments.Add(Mapper.Map<AssignmentViewModel>(item));
            }
            Tuple<List<AssignmentSettingViewModel>, List<AssignmentViewModel>> tuple =
                new Tuple<List<AssignmentSettingViewModel>, List<AssignmentViewModel>>(assignmentsSettings, assignments);
            return View("AssignmentsSettingsIndex", tuple);
        }

        // GET: AssignmentsSettingsManagement/Create
        public ActionResult AssignmentsSettingsCreate()
        {
            AssignmentSettingViewModel model = new AssignmentSettingViewModel();
            model.AvailableAssignments = GetAssignmentsAsSelectList();
            return View(model);
        }

        // POST: AssignmentsSettingsManagement/Create
        [HttpPost]
        public ActionResult AssignmentsSettingsCreate(AssignmentSettingViewModel model)
        {
            var assignmentSettingBusiness = new AssignmentSettingBusiness()
            {
                AssignmentId = model.SelectedAssignment,
                WithLink = model.WithLink,
                WithText = model.WithText,
                PhisicalDelivery = model.PhisicalDelivery,
                NoDelivery = model.NoDelivery,
                GroupAssignment = model.GroupAssignment,
                UploadFile = model.UploadFile,
                StartDate = model.StartDate,
                EndDate = model.EndDate
            };
            _assignmentsSettingsManagementBl.Add(assignmentSettingBusiness);
            return RedirectToAction("AssignmentsSettingsIndex");
        }

        // GET: AssignmentsSettingsManagement/Edit/5
        public ActionResult AssignmentsSettingsEdit(int id)
        {
            var assignmentSettingViewModel = Mapper.Map<AssignmentSettingViewModel>(_assignmentsSettingsManagementBl.GetById(id));
            assignmentSettingViewModel.AvailableAssignments = GetAssignmentsAsSelectList();
            assignmentSettingViewModel.SelectedAssignment = assignmentSettingViewModel.AssignmentId;
            return View(assignmentSettingViewModel);
        }

        // POST: AssignmentsSettingsManagement/Edit/5
        [HttpPost]
        public ActionResult AssignmentsSettingsEdit(int id, AssignmentSettingViewModel model)
        {
            var assignmentSettingBusiness = Mapper.Map<AssignmentSettingBusiness>(model);
            assignmentSettingBusiness.AssignmentId = model.SelectedAssignment;
            _assignmentsSettingsManagementBl.Update(assignmentSettingBusiness);
            return RedirectToAction("AssignmentsSettingsIndex");
        }

        // POST: AssignmentsSettingsManagement/Delete/5
        public ActionResult AssignmentsSettingsDelete(int id)
        {
            _assignmentsSettingsManagementBl.Remove(id);
            return RedirectToAction("AssignmentsSettingsIndex");
        }

        private List<SelectListItem> GetAssignmentsAsSelectList()
        {
            return _assignmentsBl.GetAll().Select(x => new SelectListItem()
            {
                Value = x.Id.ToString(),
                Text = x.Name
            }).ToList();
        }
    }
}
