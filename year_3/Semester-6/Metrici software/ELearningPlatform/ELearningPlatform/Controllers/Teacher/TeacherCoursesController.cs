﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ELearningPlatform.BusinessLayer;
using ELearningPlatform.Controllers.Shared;
using ELearningPlatform.Models.ViewModels;

namespace ELearningPlatform.Controllers.Teacher
{
    public class TeacherCoursesController : BaseController
    {
        private CoursesManagementBl _coursesManagementBl = new CoursesManagementBl();
        private readonly DepartmentsManagementBl _departmentsManagementBl = new DepartmentsManagementBl();
        public ActionResult TeacherCourseIndex()
        {
            return View();
        }

        public ActionResult Details(int id)
        {
            return View();
        }

        public ActionResult TeacherCourseCreate()
        {
            TeacherCourseViewModel model = new TeacherCourseViewModel()
            {
                CoordonaterId = LoggedUser.Id,
                CourseDetails = new CourseViewModel(),
                Lectures = new List<LectureViewModel>()
            };
            PopulateOptions(model);
            return View("TeacherCourseCreate", model);
        }

        [HttpPost]
        public ActionResult TeacherCourseCreate(TeacherCourseViewModel model)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Edit(int id)
        {
            return View();
        }

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Delete(int id)
        {
            return View();
        }

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        private void PopulateOptions(TeacherCourseViewModel model)
        {
            model.CourseDetails.Departments = _departmentsManagementBl.GetAll().Select(d => new SelectListItem() { Text = d.Name, Value = d.Id.ToString() }).ToList();
        }
    }
}
