﻿using AutoMapper;
using ELearningPlatform.BusinessLayer;
using ELearningPlatform.Controllers.Shared;
using ELearningPlatform.Models.BusinessModels;
using ELearningPlatform.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;

namespace ELearningPlatform.Controllers
{
    public class EnrolledStudentsToCoursesController : BaseController
    {
        private readonly EnrolledStudentsToCoursesBl _enrolledStudentsToCoursesBlManagementBl =
            new EnrolledStudentsToCoursesBl();
        readonly CoursesManagementBl _courseBl = new CoursesManagementBl();

        public ActionResult EnrolledStudentsToCoursesIndex()
        {
            var enrolledStudentsToCourses =
                _enrolledStudentsToCoursesBlManagementBl.GetAll()
                    .Select(Mapper.Map<EnrolledStudentsToCourseViewModel>)
                    .ToList();
            return View("EnrolledStudentsToCoursesIndex", enrolledStudentsToCourses);
        }

        public ActionResult EnrolledStudentsToCoursesCreate(int? courseId)
        {
            var model = new EnrolledStudentsToCourseViewModel();
            if (courseId.HasValue)
            {
                model.CourseId = courseId.Value;
                model.CourseName = _courseBl.GetById(courseId.Value).Name;
                model.Students = GetNotEnrolledStudents(courseId.Value);
                model.EnrolledStudents = GetEnrolledStudents(courseId.Value);
            }
            else
            {
                model.Students = GetNotEnrolledStudents(0);
                model.EnrolledStudents = GetEnrolledStudents(0);
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult EnrolledStudentsToCoursesCreate(EnrolledStudentsToCourseViewModel model)
        {
            var enrolledStudentsToCoursesBusiness = new EnrolledStudentsToCourseBusiness()
            {
                StudentId = model.StudentId,
                CourseId = model.CourseId
            };
            _enrolledStudentsToCoursesBlManagementBl.Add(enrolledStudentsToCoursesBusiness);
            return RedirectToAction("EnrolledStudentsToCoursesCreate",new {courseId = model.CourseId });
        }


        public ActionResult EnrolledStudentsToCoursesEdit(int id)
        {
            var enrolledStudentsToCourseViewModel =
                Mapper.Map<EnrolledStudentsToCourseViewModel>(_enrolledStudentsToCoursesBlManagementBl.GetById(id));
            return View(enrolledStudentsToCourseViewModel);
        }

        [HttpPost]
        public ActionResult EnrolledStudentsToCoursesEdit(int id, EnrolledStudentsToCourseViewModel model)
        {
            var business = Mapper.Map<EnrolledStudentsToCourseBusiness>(model);
            _enrolledStudentsToCoursesBlManagementBl.Update(business);
            return RedirectToAction("EnrolledStudentsToCoursesIndex");
        }


        public ActionResult Delete(int id,int courseId)
        {
            _enrolledStudentsToCoursesBlManagementBl.Remove(id);
            return RedirectToAction("EnrolledStudentsToCoursesCreate", new { courseId = courseId });
        }

        [HttpGet]
        public JsonResult GetStudents(int courseId,string term)
        {
            UsersManagementBl usersManagementBl = new UsersManagementBl();
            var allStudents = usersManagementBl.GetAllStudents();
            var enrolledStudents = _enrolledStudentsToCoursesBlManagementBl.GetAllByCourseId(courseId).Select(x=>x.StudentId);
            Func<UserBusiness, bool> whereClause = x => !enrolledStudents.Contains(x.Id);
            if (!string.IsNullOrWhiteSpace(term))
            {
                whereClause =
                    x => !enrolledStudents.Contains(x.Id) && (x.FirstName.Contains(term) || x.LastName.Contains(term));
            }
            var notEnrolledStudents = allStudents.Where(whereClause);
            var items = notEnrolledStudents.Select(x => new JqueryAutocompleteItem()
            {
                Id = x.Id,
                Value = string.Format("{0} {1}", x.FirstName, x.LastName),
                Label = string.Format("{0} {1}", x.FirstName, x.LastName)
            }).ToList();
            return Json(items, JsonRequestBehavior.AllowGet);
        }

        private List<SelectListItem> GetNotEnrolledStudents(int courseId)
        {
            UsersManagementBl usersManagementBl = new UsersManagementBl();
            var allStudents = usersManagementBl.GetAllStudents();
            var enrolledStudents = _enrolledStudentsToCoursesBlManagementBl.GetAllByCourseId(courseId).Select(x => x.StudentId);
            Func<UserBusiness, bool> whereClause = x => !enrolledStudents.Contains(x.Id);
            var notEnrolledStudents = allStudents.Where(whereClause);
            var items = notEnrolledStudents.Select(x => new SelectListItem()
            {
                Value = x.Id,
                Text = string.Format("{0} {1}", x.FirstName, x.LastName)
            }).ToList();
            return items;
        }

        private List<SelectListItem> GetEnrolledStudents(int courseId)
        {
            UsersManagementBl usersManagementBl = new UsersManagementBl();
            var allStudents = usersManagementBl.GetAllStudents();
            var enrolledStudents = _enrolledStudentsToCoursesBlManagementBl.GetAllByCourseId(courseId);
            var enrolledStudentsData = allStudents.Where(x=>enrolledStudents.Select(y=>y.StudentId).Contains(x.Id));
            var items = enrolledStudentsData.Select(x => new SelectListItem()
            {
                Value = enrolledStudents.FirstOrDefault(y=>y.StudentId == x.Id && y.CourseId == courseId)?.Id.ToString(),
                Text = string.Format("{0} {1}", x.FirstName, x.LastName)
            }).ToList();
            return items;
        }
    }
}
