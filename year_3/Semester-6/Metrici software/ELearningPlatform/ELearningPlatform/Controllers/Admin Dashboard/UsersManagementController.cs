﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using AutoMapper;
using ELearningPlatform.DataLayer.Edmx;
using ELearningPlatform.DataLayer.Repository;
using ELearningPlatform.Models;
using ELearningPlatform.Models.ViewModels;
using Microsoft.AspNet.Identity;
using ELearningPlatform.Controllers.Shared;
using Microsoft.AspNet.Identity.EntityFramework;
using System.IO;

namespace ELearningPlatform.Controllers.Admin_Dashboard
{
    public class UsersManagementController : BaseController
    {
        private readonly UniversityStaffRepository _universityStaffRepository = new UniversityStaffRepository();
        private const string ERRORTAG = "ErrorMessage";
        public ActionResult UsersIndex()
        {
            return View("UsersIndex");
        }

        public ActionResult UserCreate()
        {
            UserViewModel model = new UserViewModel();
            model.RolesList = GetRolesAsSelectListItem();
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> UserCreate(UserViewModel model)
        {
            if (string.IsNullOrEmpty(model.Password))
            {
                ModelState.AddModelError(ERRORTAG, "Password is required.");
            }
            if (ModelState.IsValid)
            {

                var user = new ApplicationUser
                {
                    UserName = model.Email,
                    Email = model.Email,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    DateCreated = DateTime.UtcNow.ToString(CultureInfo.InvariantCulture),
                    IsSuspended = model.IsSuspended
                };
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    var role = GetRole(model.Role);
                    await UserManager.AddToRoleAsync(user.Id, role);
                    if (role == UserRolesEnum.Teacher.ToString())
                    {
                        _universityStaffRepository.Add(new UniversityStaff() { Id = user.Id });
                    }
                    return RedirectToAction("UsersIndex");
                }
            }
            ViewBag.ModelState = ModelState;
            model.RolesList = GetRolesAsSelectListItem();
            return View("UserCreate", model);
        }

        public ActionResult UserEdit(string id)
        {
            ApplicationUser aspNetUser = UserManager.FindById(id);
            UserViewModel userViewModel = Mapper.Map<UserViewModel>(aspNetUser);
            userViewModel.RolesList = GetRolesAsSelectListItem();
            return View("UserEdit", userViewModel);
        }

        [HttpPost]
        public async Task<ActionResult> UserEdit(string id, UserViewModel model)
        {
            //if (ModelState.IsValid)
            //{
            //    ApplicationUser applicationUser = Mapper.Map<ApplicationUser>(model);
            //    IList<string> role = UserManager.GetRoles(applicationUser.Id);
            //    foreach (string r in role)
            //    {
            //        UserManager.RemoveFromRole(applicationUser.Id, r);
            //    }
            //    await UserManager.AddToRoleAsync(applicationUser.Id, model.Role);
            //    await UserManager.UpdateAsync(applicationUser);
            //    ViewBag.Succes = true;
            //    model.RolesList = GetRolesAsSelectListItem();
            //    return View("UserEdit", model);
            //}
            //ViewBag.ModelState = ModelState;
            //model.RolesList = GetRolesAsSelectListItem();
            //return View("UserEdit", model);

            UserManager<ApplicationUser> userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
            if (string.IsNullOrEmpty(model.Password))
            {
                ModelState.Remove("Password");
            }
            if (ModelState.IsValid)
            {
                ApplicationUser applicationUser = await UserManager.FindByIdAsync(model.Id);
                applicationUser.FirstName = model.FirstName;
                applicationUser.LastName = model.LastName;
                applicationUser.IsSuspended = model.IsSuspended;
                applicationUser.Email = model.Email;

                IList<string> rolesList = UserManager.GetRoles(applicationUser.Id);
                foreach (string r in rolesList)
                {
                    UserManager.RemoveFromRole(applicationUser.Id, r);
                }
                var role = GetRole(model.Role);
                await UserManager.AddToRoleAsync(applicationUser.Id, role);
                ViewBag.Succes = true;
                model.RolesList = GetRolesAsSelectListItem();
                return View("UserEdit", model);
            }
            ViewBag.ModelState = ModelState;
            model.RolesList = GetRolesAsSelectListItem();
            return View("UserEdit", model);
        }

        public ActionResult Delete(string id)
        {
            ApplicationUser applicationUser = UserManager.FindById(id);
            UserManager.Delete(applicationUser);
            return RedirectToAction("UsersIndex");
        }

        public ActionResult StudentCreate()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> StudentCreate(StudentViewModel model)
        {
            StudentsRepository studentsRepository = new StudentsRepository();
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser
                {
                    UserName = model.Email,
                    Email = model.Email,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    DateCreated = DateTime.UtcNow.ToString(CultureInfo.InvariantCulture),
                    IsSuspended = model.IsSuspended
                };
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    await UserManager.AddToRoleAsync(user.Id, UserRolesEnum.Student.ToString());
                    Students student = new Students()
                    {
                        Id = user.Id,
                        UniqueNumber = model.UniqueNumber
                    };
                    studentsRepository.Add(student);
                    return RedirectToAction("UsersIndex");
                }
            }
            return View("StudentCreate", model);
        }

        public string GetRoleById(string roleId)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            return db.Roles.Where(x => x.Id == roleId).Select(r => r.Name).FirstOrDefault();
        }

        [HttpGet]
        public JsonResult GetUsers(DataTableRequest request)
        {
            DataTableResponse response = new DataTableResponse()
            {
                aaData = new List<List<string>>()
            };
            List<ApplicationUser> users = UserManager.Users.ToList();
            foreach (ApplicationUser user in users)
            {
                string userRoleId = user.Roles.First().RoleId;
                string roleName = GetRoleById(userRoleId);
                response.aaData.Add(new List<string>()
                {
                    string.Format("{0} {1}",user.FirstName , user.LastName),
                    user.UserName,
                    roleName,
                    user.IsSuspended ? "Suspended" : "Active" ,
                    Convert.ToDateTime(user.DateCreated).ToShortDateString(),
                    user.Id
                });
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }


        private string GetRole(string id)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            return db.Roles.Where(x => x.Id == id).Select(r => r.Name).FirstOrDefault();
        }
        private List<string> GetRoles()
        {
            ApplicationDbContext db = new ApplicationDbContext();
            return db.Roles.Where(x => x.Name != "Student").Select(r => r.Name).ToList();
        }
        private List<SelectListItem> GetRolesAsSelectListItem()
        {
            ApplicationDbContext db = new ApplicationDbContext();
            return db.Roles.Where(x => x.Name != "Student").Select(r => new SelectListItem()
            {
                Text = r.Name,
                Value = r.Id
            }).ToList();
        }

        public async Task<ActionResult> StudentsCreate()
        {
            UserViewModel model;
            var filepath = @"C:\Users\Cipri\Desktop\randomNames.txt";
            string[] lines = System.IO.File.ReadAllLines(filepath);
            StudentsRepository studentsRepository = new StudentsRepository();
            foreach (var item in lines)
            {
                Random random = new Random(Guid.NewGuid().GetHashCode());

                model = new UserViewModel()
                {
                    Email = string.Format("{0}.{1}@ubbcluj.ro", item.Split(' ')[0].ToLower(), item.Split(' ')[1].ToLower()),
                    FirstName = item.Split(' ')[0],
                    LastName = item.Split(' ')[1],
                    IsSuspended = false,
                    Password = "google",
                    Role = "feb3444a-e995-401e-8996-a64734c4e24a"
                };
                var user = new ApplicationUser
                {
                    UserName = model.Email,
                    Email = model.Email,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    DateCreated = DateTime.UtcNow.ToString(CultureInfo.InvariantCulture),
                    IsSuspended = model.IsSuspended
                };
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    var role = GetRole(model.Role);
                    await UserManager.AddToRoleAsync(user.Id, role);
                    if (role == UserRolesEnum.Teacher.ToString())
                    {
                        _universityStaffRepository.Add(new UniversityStaff() { Id = user.Id });
                    }
                }
            }
            return RedirectToAction("UsersIndex");
        }
    }
}