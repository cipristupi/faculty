﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using ELearningPlatform.BusinessLayer;
using ELearningPlatform.Controllers.Shared;
using ELearningPlatform.Models.BusinessModels;
using ELearningPlatform.Models.ViewModels;

namespace ELearningPlatform.Controllers.Admin_Dashboard
{
    public class RolesManagementController :BaseController
    {
        readonly RolesManagementBl _rolesManagementBl = new RolesManagementBl();
        public ActionResult RolesIndex()
        {

            List<RoleViewModel> roles = new List<RoleViewModel>();
            foreach (var item in _rolesManagementBl.GetAll())
            {
                roles.Add(Mapper.Map<RoleViewModel>(item));
            }
            return View("RolesIndex", roles);
        }


        // GET: RolesManagement/Create
        public ActionResult RoleCreate()
        {
            return View("RoleCreate");
        }

        // POST: RolesManagement/Create
        [HttpPost]
        public ActionResult RoleCreate(RoleViewModel model)
        {
            _rolesManagementBl.Add(new RoleBusiness()
            {
                Id = Guid.NewGuid().ToString(),
                Name = model.Name
            });

            return RedirectToAction("RolesIndex");

        }

        // GET: RolesManagement/Edit/5
        public ActionResult RoleEdit(string id)
        {
            var roleViewModel = Mapper.Map<RoleViewModel>(_rolesManagementBl.GetById(id));
            return View(roleViewModel);
        }

        // POST: RolesManagement/Edit/5
        [HttpPost]
        public ActionResult RoleEdit(string id, RoleViewModel model)
        {
            var roleBusiness = Mapper.Map<RoleBusiness>(model);
            _rolesManagementBl.Update(roleBusiness);
            return RedirectToAction("RolesIndex");
        }

        
        public ActionResult RoleDelete(string id)
        {
            _rolesManagementBl.Remove(id);
            return RedirectToAction("RolesIndex");
        }
    }
}