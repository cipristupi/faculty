﻿using System.Web.Mvc;
using ELearningPlatform.Controllers.Shared;

namespace ELearningPlatform.Controllers.Admin_Dashboard
{
    public class AdminDashboardController : BaseController
    {
        // GET: AdminDashboard
        public ActionResult AdminDashboardIndex()
        {
            return View("AdminDashboardIndex");
        }
    }
}