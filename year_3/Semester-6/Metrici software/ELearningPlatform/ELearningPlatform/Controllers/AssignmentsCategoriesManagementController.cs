﻿using AutoMapper;
using ELearningPlatform.BusinessLayer;
using ELearningPlatform.Controllers.Shared;
using ELearningPlatform.Models.BusinessModels;
using ELearningPlatform.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ELearningPlatform.Controllers
{
    public class AssignmentsCategoriesManagementController : BaseController
    {
        private AssignmentsCategoriesManagementBl _assignmentsCategoriesManagementBl = new AssignmentsCategoriesManagementBl();
        // GET: AssignmentsCategoriesManagement
        public ActionResult AssignmentsCategoriesIndex()
        {
            List<AssignmentCategoryViewModel> assignmentsCategories = new List<AssignmentCategoryViewModel>();
            foreach (var item in _assignmentsCategoriesManagementBl.GetAll())
            {
                assignmentsCategories.Add(Mapper.Map<AssignmentCategoryViewModel>(item));
            }
            return View("AssignmentsCategoriesIndex", assignmentsCategories);
        }

        // GET: AssignmentsCategoriesManagement/Create
        public ActionResult AssignmentsCategoriesCreate()
        {
            return View();
        }

        // POST: AssignmentsCategoriesManagement/Create
        [HttpPost]
        public ActionResult AssignmentsCategoriesCreate(AssignmentViewModel model)
        {
            var assignmentCategoryBusiness = new AssignmentCategoryBusiness()
            {
                Name = model.Name,
                Description = model.Description
            };
            _assignmentsCategoriesManagementBl.Add(assignmentCategoryBusiness);
            return RedirectToAction("AssignmentsCategoriesIndex");
        }

        // GET: AssignmentsCategoriesManagement/Edit/5
        public ActionResult AssignmentsCategoriesEdit(int id)
        {
            var assignmentCategoryViewModel = Mapper.Map<AssignmentCategoryViewModel>(_assignmentsCategoriesManagementBl.GetById(id));
            return View(assignmentCategoryViewModel);
        }

        // POST: AssignmentsCategoriesManagement/Edit/5
        [HttpPost]
        public ActionResult AssignmentsCategoriesEdit(int id, AssignmentCategoryViewModel model)
        {
            var assignmentCategoryBusiness = Mapper.Map<AssignmentCategoryBusiness>(model);
            _assignmentsCategoriesManagementBl.Update(assignmentCategoryBusiness);
            return RedirectToAction("AssignmentsCategoriesIndex");
        }

        // POST: AssignmentsCategoriesManagement/Delete/5
        public ActionResult AssignmentsCategoriesDelete(int id)
        {
            _assignmentsCategoriesManagementBl.Remove(id);
            return RedirectToAction("AssignmentsCategoriesIndex");
        }
    }
}
