﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using ELearningPlatform.BusinessLayer;
using ELearningPlatform.Controllers.Shared;
using ELearningPlatform.Models;
using ELearningPlatform.Models.BusinessModels;
using ELearningPlatform.Models.ViewModels;

namespace ELearningPlatform.Controllers
{
    public class LecturesManagementController : BaseController
    {
        readonly LectureBl _lectureBl;
        readonly PagesBl _pagesBl;
        readonly CoursesManagementBl _courseBl;

        public LecturesManagementController()
        {
            _lectureBl = new LectureBl();
            _pagesBl = new PagesBl();
            _courseBl = new CoursesManagementBl();
        }

        public ActionResult LecturesIndex()
        {
            List<LectureViewModel> lectures = _lectureBl.GetAllWithCourseName().Select(Mapper.Map<LectureViewModel>).ToList();
            return View(lectures);
        }

        public ActionResult LecturesCreate(int? courseId)
        {
            LectureViewModel model = new LectureViewModel();
            if (courseId.HasValue)
            {
                model.CourseId = courseId.Value;
                model.CourseName = _courseBl.GetById(courseId.Value).Name;
            }
            else
            {
                PopulateModel(model);
            }
            model.StartDate=DateTime.Now;
            return View(model);
        }

        [HttpPost]
        public ActionResult LecturesCreate(LectureViewModel model)
        {
            if(ModelState.IsValid)
            {
                var lectureBusiness = Mapper.Map<LectureBusiness>(model);
                lectureBusiness.Description = Server.HtmlEncode(model.Description);
                _lectureBl.Add(lectureBusiness);
                ViewBag.Succes = true;
                PopulateModel(model);
                model.CourseName = _courseBl.GetById(model.CourseId).Name;
                return View(model);
            }
            PopulateModel(model);
            ViewBag.ModelState = ModelState;
            model.CourseName = _courseBl.GetById(model.CourseId).Name;
            return View(model);
        }

        public ActionResult LecturesEdit(int id)
        {
            var lectureViewModel = Mapper.Map<LectureViewModel>(_lectureBl.GetById(id));
            lectureViewModel.Pages = _pagesBl.GetAllByParentId(lectureViewModel.Id).Select(Mapper.Map<PageViewModel>).ToList();
            lectureViewModel.Description = Server.HtmlDecode(lectureViewModel.Description);
            PopulateModel(lectureViewModel);
            return View(lectureViewModel);
        }

        [HttpPost]
        public ActionResult LecturesEdit(LectureViewModel model)
        {
            if (ModelState.IsValid)
            {
                var lectureBusiness = Mapper.Map<LectureBusiness>(model);
                lectureBusiness.Description = Server.HtmlEncode(model.Description);
                _lectureBl.Update(lectureBusiness);
                ViewBag.Succes = true;

                model.Pages = _pagesBl.GetAllByParentId(model.Id).Select(Mapper.Map<PageViewModel>).ToList();
                model.Description = Server.HtmlDecode(model.Description);
                PopulateModel(model);
                model.CourseName = _courseBl.GetById(model.CourseId).Name;
                return View(model);
            }
            model.Pages = _pagesBl.GetAllByParentId(model.Id).Select(Mapper.Map<PageViewModel>).ToList();
            model.Description = Server.HtmlDecode(model.Description);
            PopulateModel(model);
            ViewBag.ModelState = ModelState;
            model.CourseName = _courseBl.GetById(model.CourseId).Name;
            return View(model);
        }

        public ActionResult LecturesDelete(int id)
        {
            _lectureBl.Remove(id);
            return RedirectToAction("LecturesIndex");
        }

        public ActionResult LecturesDetails(int id)
        {
            var lectureViewModel = Mapper.Map<LectureViewModel>(_lectureBl.GetById(id));
            lectureViewModel.Pages = _pagesBl.GetAllByParentId(lectureViewModel.Id).Select(Mapper.Map<PageViewModel>).ToList();
            lectureViewModel.Description = Server.HtmlDecode(lectureViewModel.Description);
            PopulateModel(lectureViewModel);
            return View(lectureViewModel);
        }

        [HttpGet]
        public JsonResult GetLectures(DataTableRequest request)
        {
            DataTableResponse response = new DataTableResponse()
            {
                aaData = new List<List<string>>()
            };
            var lectureViewModels = _lectureBl.GetAllWithCourseName().ToList();
            foreach (var item in lectureViewModels)
            {
                string endDate = item.EndDate != null ? ((DateTime)item.EndDate).ToShortDateString() : string.Empty;
                response.aaData.Add(new List<string>()
                {
                    item.Name,
                   item.StartDate.ToShortDateString(),
                   endDate,
                   item.CourseName,
                    item.Id.ToString()
                });
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        private void PopulateModel(LectureViewModel model)
        {
            model.Courses = _courseBl.GetAll().Select(x => new SelectListItem()
            {
                Value = x.Id.ToString(),
                Text = x.Name
            }).ToList();
        }

    }
}
