﻿using AutoMapper;
using ELearningPlatform.BusinessLayer;
using ELearningPlatform.Controllers.Shared;
using ELearningPlatform.Models.BusinessModels;
using ELearningPlatform.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ELearningPlatform.Controllers
{
    public class GradeSchemasManagementController : BaseController
    {
        private GradeSchemasManagementBl _gradeValuesManagementBl = new GradeSchemasManagementBl();
        // GET: GradeSchemasManagement
        public ActionResult GradeSchemasIndex()
        {
            List<GradeSchemaViewModel> gradeValues = new List<GradeSchemaViewModel>();
            foreach (var item in _gradeValuesManagementBl.GetAll())
            {
                gradeValues.Add(Mapper.Map<GradeSchemaViewModel>(item));
            }
            return View("GradeSchemasIndex", gradeValues);
        }


        // GET: GradeSchemasManagement/Create
        public ActionResult GradeSchemasCreate()
        {
            return View();
        }

        // POST: GradeSchemasManagement/Create
        [HttpPost]
        public ActionResult GradeSchemasCreate(GradeSchemaViewModel model)
        {
            var gradeValueBusiness = new GradeSchemaBusiness()
            {
                Name = model.Name,
                Description = model.Description
            };
            _gradeValuesManagementBl.Add(gradeValueBusiness);
            return RedirectToAction("GradeSchemasIndex");
        }

        // GET: GradeSchemasManagement/Edit/5
        public ActionResult GradeSchemasEdit(int id)
        {
            var gradeValueViewModel = Mapper.Map<GradeSchemaViewModel>(_gradeValuesManagementBl.GetById(id));
            return View(gradeValueViewModel);
        }

        // POST: GradeSchemasManagement/Edit/5
        [HttpPost]
        public ActionResult GradeSchemasEdit(int id, GradeSchemaViewModel model)
        {
            var gradeValueBusiness = Mapper.Map<GradeSchemaBusiness>(model);
            _gradeValuesManagementBl.Update(gradeValueBusiness);
            return RedirectToAction("GradeSchemasIndex");
        }

        // POST: GradeSchemasManagement/Delete/5
        public ActionResult GradeSchemasDelete(int id)
        {
            _gradeValuesManagementBl.Remove(id);
            return RedirectToAction("GradeSchemasIndex");
        }
    }
}
