﻿using AutoMapper;
using ELearningPlatform.BusinessLayer;
using ELearningPlatform.Controllers.Shared;
using ELearningPlatform.Models.BusinessModels;
using ELearningPlatform.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ELearningPlatform.Controllers
{
    public class AssignmentsToCoursesManagementController : BaseController
    {
        private readonly AssignmentsToCoursesManagementBl _assignmentsToCoursesManagementBl = new AssignmentsToCoursesManagementBl();
        private readonly CoursesManagementBl _coursesManagementBl = new CoursesManagementBl();
        private readonly AssignmentsManagementBl _assignmentsManagementBl = new AssignmentsManagementBl();

        public ActionResult AssignmentsToCoursesIndex()
        {
            List<AssignmentsToCoursesViewModel> assignmentsToCourses = new List<AssignmentsToCoursesViewModel>();
            foreach (var item in _assignmentsToCoursesManagementBl.GetAll())
            {
                assignmentsToCourses.Add(Mapper.Map<AssignmentsToCoursesViewModel>(item));
            }
            return View("AssignmentsToCoursesIndex", assignmentsToCourses);
        }

        // GET: AssignmentsToCoursesManagement/Create
        public ActionResult AssignmentsToCoursesCreate()
        {
            AssignmentsToCoursesViewModel model = new AssignmentsToCoursesViewModel();
            PopulateOptions(model);
            return View(model);
        }

        // POST: AssignmentsToCoursesManagement/Create
        [HttpPost]
        public ActionResult AssignmentsToCoursesCreate(AssignmentsToCoursesViewModel model)
        {
            var assignmentsToCoursesBusiness = new AssignmentsToCoursesBusiness()
            {
                CourseId = model.CourseId,
                AssignmentId = model.AssignmentId
            };
            _assignmentsToCoursesManagementBl.Add(assignmentsToCoursesBusiness);
            return RedirectToAction("AssignmentsToCoursesIndex");
        }

        // GET: AssignmentsToCoursesManagement/Edit/5
        public ActionResult AssignmentsToCoursesEdit(int id)
        {
            var assignementsToCoursesViewModel = Mapper.Map<AssignmentsToCoursesViewModel>(_assignmentsToCoursesManagementBl.GetById(id));
            PopulateOptions(assignementsToCoursesViewModel);
            return View(assignementsToCoursesViewModel);
        }

        // POST: AssignmentsToCoursesManagement/Edit/5
        [HttpPost]
        public ActionResult AssignmentsToCoursesEdit(int id, AssignmentsToCoursesViewModel model)
        {
            var asToCrBusiness = Mapper.Map<AssignmentsToCoursesBusiness>(model);
            asToCrBusiness.AssignmentId = model.AssignmentId;
            asToCrBusiness.CourseId = model.CourseId;
            _assignmentsToCoursesManagementBl.Update(asToCrBusiness);
            return RedirectToAction("AssignmentsToCoursesIndex");
        }

        // GET: AssignmentsToCoursesManagement/Delete/5
        public ActionResult AssignmentsToCoursesDelete(int id)
        {
            _assignmentsToCoursesManagementBl.Remove(id);
            return RedirectToAction("AssignmentsToCoursesIndex");
        }

        private void PopulateOptions(AssignmentsToCoursesViewModel model)
        {
            model.Courses = _coursesManagementBl.GetAll().Select(c =>
             new SelectListItem()
             {
                 Text = c.Name,
                 Value = c.Id.ToString()
             }).ToList();
            model.Assignments = _assignmentsManagementBl.GetAll().Select(d => new SelectListItem() { Text = d.Name, Value = d.Id.ToString() }).ToList();
        }
    }
}
