﻿using AutoMapper;
using ELearningPlatform.BusinessLayer;
using ELearningPlatform.Models.BusinessModels;
using ELearningPlatform.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ELearningPlatform.Controllers
{
    public class QuestionTypeManagementController : Controller
    {
        QuestionTypeManagementBl _questionTypeBl = new QuestionTypeManagementBl();

        public ActionResult QuestionTypesIndex()
        {
            var questionTypes = _questionTypeBl.GetAll().Select(item => Mapper.Map<QuestionTypeViewModel>(item)).ToList();
            return View("QuestionTypesIndex", questionTypes);
        }

        public ActionResult QuestionTypesCreate()
        {
            return View();
        }

        [HttpPost]
        public ActionResult QuestionTypesCreate(QuestionTypeViewModel model)
        {
            var questionTypeBusiness = new QuestionTypeBusiness()
            {
                Value = model.Value
            };
            _questionTypeBl.Add(questionTypeBusiness);
            return RedirectToAction("QuestionTypesIndex");
        }

        public ActionResult QuestionTypesEdit(int id)
        {
            var questionTypeViewModel = Mapper.Map<QuestionTypeViewModel>(_questionTypeBl.GetById(id));
            return View(questionTypeViewModel);
        }

        [HttpPost]
        public ActionResult QuestionTypesEdit(int id, QuestionTypeViewModel model)
        {
            var questionTypeBusiness = Mapper.Map<QuestionTypeBusiness>(model);
            _questionTypeBl.Update(questionTypeBusiness);
            return RedirectToAction("QuestionTypesIndex");
        }

        public ActionResult QuestionTypesDelete(int id)
        {
            _questionTypeBl.Remove(id);
            return RedirectToAction("QuestionTypesIndex");
        }
    }
}
