﻿using AutoMapper;
using ELearningPlatform.BusinessLayer;
using ELearningPlatform.Models.BusinessModels;
using ELearningPlatform.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ELearningPlatform.Controllers
{
    public class BoolQuestionsManagementController : Controller
    {
        BoolQuestionsManagementBl _boolQuestionsBl = new BoolQuestionsManagementBl();
        QuestionsManagementBl _questionsBl = new QuestionsManagementBl();
        
        public ActionResult BoolQuestionsIndex()
        {
            var boolQuestions = _boolQuestionsBl.GetAll().Select(item => Mapper.Map<BoolQuestionViewModel>(item)).ToList();
            return View("BoolQuestionsIndex", boolQuestions);
        }
       
        public ActionResult BoolQuestionsCreate()
        {
            BoolQuestionViewModel boolQuestions = new BoolQuestionViewModel();
            PopulateOptions(boolQuestions);
            return View(boolQuestions);
        }

        [HttpPost]
        public ActionResult BoolQuestionsCreate(BoolQuestionViewModel model)
        {
            var boolQuestionBusiness = new BoolQuestionBusiness()
            {
                QuestionId = model.QuestionId,
                Answer = model.Answer
            };
            _boolQuestionsBl.Add(boolQuestionBusiness);
            return RedirectToAction("BoolQuestionsIndex");
        }

        public ActionResult BoolQuestionsEdit(int id)
        {
            var boolQuestionViewModel = Mapper.Map<BoolQuestionViewModel>(_boolQuestionsBl.GetById(id));
            PopulateOptions(boolQuestionViewModel);
            return View(boolQuestionViewModel);
        }

        [HttpPost]
        public ActionResult BoolQuestionsEdit(int id, BoolQuestionViewModel model)
        {
            var boolQuestionBusiness = Mapper.Map<BoolQuestionBusiness>(model);
            _boolQuestionsBl.Update(boolQuestionBusiness);
            return RedirectToAction("BoolQuestionsIndex");
        }

        public ActionResult BoolQuestionsDelete(int id)
        {
            _boolQuestionsBl.Remove(id);
            return RedirectToAction("BoolQuestionsIndex");
        }

        void PopulateOptions(BoolQuestionViewModel model)
        {
            model.Questions = _questionsBl.GetAll().Select(d => new SelectListItem() { Text = d.Question, Value = d.Id.ToString() }).ToList();
        }
    }
}
