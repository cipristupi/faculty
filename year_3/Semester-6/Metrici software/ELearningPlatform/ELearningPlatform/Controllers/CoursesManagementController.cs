﻿using System;
using AutoMapper;
using ELearningPlatform.BusinessLayer;
using ELearningPlatform.Controllers.Shared;
using ELearningPlatform.Models.BusinessModels;
using ELearningPlatform.Models.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ELearningPlatform.DataLayer.Repository;
using ELearningPlatform.Models;

namespace ELearningPlatform.Controllers
{
    public class CoursesManagementController : BaseController
    {
        private readonly CoursesManagementBl _coursesManagementBl = new CoursesManagementBl();
        private readonly DepartmentsManagementBl _departmentsManagementBl = new DepartmentsManagementBl();
        private readonly SyllabusManagementBl _syllabusManagementBl = new SyllabusManagementBl();
        public ActionResult CoursesIndex()
        {
            var courses = _coursesManagementBl.GetAll().Select(item => Mapper.Map<CourseViewModel>(item)).ToList();
            return View("CoursesIndex", courses);
        }

        [HttpGet]
        public JsonResult GetCourses(DataTableRequest request)
        {
            DataTableResponse response = new DataTableResponse()
            {
                aaData = new List<List<string>>()
            };
            string coordonaterFullName = string.Empty;
            string departmentName = string.Empty;
            foreach (var item in GetCoursesByRole())
            {
                var coordonater = GetById(item.CoordonaterId);
                var department = _departmentsManagementBl.GetById(item.DepartmentId);
                if (coordonater != null)
                {
                    coordonaterFullName = string.Format("{0} {1}", coordonater.FirstName, coordonater.LastName);
                }
                if (department != null)
                {
                    departmentName = department.Name;
                }
                response.aaData.Add(new List<string>()
                {
                    item.Name,
                    item.StartDate.ToShortDateString(),
                    item.EndDate.ToShortDateString(),
                    departmentName,
                    coordonaterFullName,
                    item.Id.ToString()
                });
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        #region Courses 
        public ActionResult CoursesCreate()
        {
            CourseViewModel model = new CourseViewModel();
            PopulateOptions(model);
            if (UserRoles.Contains(UserRolesEnum.Teacher))
            {
                model.CoordonaterId = LoggedUser.Id;
            }
            model.StartDate = DateTime.Now;
            model.EndDate = DateTime.Now;
            return View(model);
        }

        [HttpPost]
        public ActionResult CoursesCreate(CourseViewModel model)
        {

            var courseBusiness = Mapper.Map<CourseBusiness>(model);
            courseBusiness.Description = Server.HtmlEncode(model.Description);
            _coursesManagementBl.Add(courseBusiness);
            return RedirectToAction("CoursesIndex");
        }

        public ActionResult CoursesEdit(int id)
        {
            var courseViewModel = Mapper.Map<CourseViewModel>(_coursesManagementBl.GetById(id));
            PopulateOptions(courseViewModel);
            courseViewModel.Description = Server.HtmlDecode(courseViewModel.Description);
            return View(courseViewModel);
        }

        [HttpPost]
        public ActionResult CoursesEdit(int id, CourseViewModel model)
        {
            if (ModelState.IsValid)
            {
                var courseBusiness = Mapper.Map<CourseBusiness>(model);
                courseBusiness.Description = Server.HtmlEncode(model.Description);
                _coursesManagementBl.Update(courseBusiness);
                ViewBag.Succes = true;
                PopulateOptions(model);
                return View(model);
            }
            PopulateOptions(model);
            ViewBag.ModelState = ModelState;
            return View(model);
        }

        public ActionResult CoursesDetails(int id)
        {
            var courseViewModel = Mapper.Map<CourseViewModel>(_coursesManagementBl.GetById(id));
            PopulateOptions(courseViewModel);
            courseViewModel.Description = Server.HtmlDecode(courseViewModel.Description);
            var selectListItem =
                courseViewModel.Coordonaters.FirstOrDefault(x => x.Value == courseViewModel.CoordonaterId);
            if (selectListItem != null)
                courseViewModel.CoordonaterName = selectListItem.Text;
            var firstOrDefault = courseViewModel.Departments.FirstOrDefault(x => x.Value == courseViewModel.DepartmentId.ToString());
            if (firstOrDefault != null)
                courseViewModel.DepartmentName = firstOrDefault.Text;
            PopulateLectures(courseViewModel);
            PopulateTests(courseViewModel);
            PopulateAnnouncements(courseViewModel);
            PopulateDiscussions(new DiscussionsManagementBl(), courseViewModel);
            courseViewModel.Syllabus = _syllabusManagementBl.GetAllByCourseId(id).Select(Mapper.Map<SyllabusViewModel>).ToList();
            return View(courseViewModel);
        }

        public ActionResult CoursesDelete(int id)
        {
            _coursesManagementBl.Remove(id);
            return RedirectToAction("CoursesIndex");
        }
        #endregion

        #region Populate Related Entities

        public ActionResult CoursesDiscussions(int id)
        {
            DiscussionsManagementBl discussionsManagementBl = new DiscussionsManagementBl();
            var model = Mapper.Map<CourseViewModel>(_coursesManagementBl.GetById(id));
            PopulateDiscussions(discussionsManagementBl, model);
            return View(model);
        }

        public ActionResult CoursesLectures(int id)
        {
            var model = Mapper.Map<CourseViewModel>(_coursesManagementBl.GetById(id));
            PopulateLectures(model);
            return View(model);
        }

        public ActionResult CoursesTests(int id)
        {
            var model = Mapper.Map<CourseViewModel>(_coursesManagementBl.GetById(id));
            PopulateTests(model);
            return View(model);
        }

        public ActionResult CoursesAnnouncements(int id)
        {
            var model = Mapper.Map<CourseViewModel>(_coursesManagementBl.GetById(id));
            PopulateAnnouncements(model);
            return View(model);
        }

        #endregion

        #region Privates
        private List<CourseViewModel> GetCoursesByRole()
        {
            var courses = _coursesManagementBl.GetAll();
            if (UserRoles.Contains(UserRolesEnum.Teacher))
            {
                courses = _coursesManagementBl.GetAllByTeacherId(LoggedUser.Id);
            }
            if (UserRoles.Contains(UserRolesEnum.Student))
            {
                courses = _coursesManagementBl.GetAllForStudent(LoggedUser.Id);
            }
            return courses.Select(Mapper.Map<CourseViewModel>).ToList();
        }
        void PopulateOptions(CourseViewModel model)
        {
            UsersRepository userRepository = new UsersRepository();
            var teachers = userRepository.GetAllTeachers()
                    .Where(
                        t =>
                            t.IsSuspended == false &&
                            t.AspNetRoles.Count(x => x.Name == UserRolesEnum.Teacher.ToString()) != 0)
                    .Select(Mapper.Map<UserBusiness>)
                    .AsEnumerable();
            model.Coordonaters =
                teachers.Select(
                    t => new SelectListItem() { Text = string.Format("{0} {1}", t.FirstName, t.LastName), Value = t.Id })
                    .ToList();
            model.Departments = _departmentsManagementBl.GetAll().Select(d => new SelectListItem() { Text = d.Name, Value = d.Id.ToString() }).ToList();
        }

        private void PopulateDiscussions(DiscussionsManagementBl discussionsManagementBl, CourseViewModel model)
        {
            model.Discussions =
                discussionsManagementBl.GetByParentId(model.Id, (int)ParentTypes.Course)
                    .Select(Mapper.Map<DiscussionViewModel>)
                    .ToList();
        }

        private void PopulateTests(CourseViewModel model)
        {
            TestsManagementBl testsManagementBl = new TestsManagementBl();
            var tests = testsManagementBl.GetAllByCourseId(model.Id).Select(Mapper.Map<TestViewModel>).ToList();
            model.Tests = tests;
        }

        private void PopulateLectures(CourseViewModel model)
        {
            LectureBl lectureBl = new LectureBl();
            var lectures = lectureBl.GetAllByCourseIdWithPages(model.Id).Select(Mapper.Map<LectureViewModel>).ToList();
            for (int i = 0; i < lectures.Count; i++)
            {
                lectures[i].Description = Server.HtmlDecode(lectures[i].Description);
            }
            model.Lectures = lectures;
        }

        private void PopulateAnnouncements(CourseViewModel model)
        {
            AnnouncementsManagementBl announcementsManagementBl = new AnnouncementsManagementBl();
            var announcements = announcementsManagementBl.GetAllAnnouncementsByCourseId(model.Id).Select(Mapper.Map<AnnouncementViewModel>).ToList();
            model.Announcements = announcements;
        }


        private UserBusiness GetById(string id)
        {
            UsersRepository repository = new UsersRepository();
            return Mapper.Map<UserBusiness>(repository.GetAll().FirstOrDefault(u => u.Id == id));
        }
        #endregion
    }
}