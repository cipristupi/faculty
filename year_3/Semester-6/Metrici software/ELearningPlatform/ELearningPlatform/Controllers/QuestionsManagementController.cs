﻿using AutoMapper;
using ELearningPlatform.BusinessLayer;
using ELearningPlatform.Models.BusinessModels;
using ELearningPlatform.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ELearningPlatform.Controllers
{
    public class QuestionsManagementController : Controller
    {
        QuestionsManagementBl _questionsBl = new QuestionsManagementBl();
        QuestionTypeManagementBl _questionTypeBl = new QuestionTypeManagementBl();

        public ActionResult QuestionsIndex()
        {
            var questions = _questionsBl.GetAll().Select(item => Mapper.Map<QuestionViewModel>(item)).ToList();
            return View("QuestionsIndex", questions);
        }

        public ActionResult QuestionsCreate()
        {
            QuestionViewModel model = new QuestionViewModel();
            PopulateOptions(model);
            return View(model);
        }

        [HttpPost]
        public ActionResult QuestionsCreate(QuestionViewModel model)
        {
            var questionBusiness = new QuestionBusiness()
            {
                Question = model.Question,
                Points = model.Points,
                Description = model.Description,
                TypeId = model.TypeId
            };
            _questionsBl.Add(questionBusiness);
            return RedirectToAction("QuestionsIndex");
        }

        public ActionResult QuestionsEdit(int id)
        {
            var questionViewModel = Mapper.Map<QuestionViewModel>(_questionsBl.GetById(id));
            PopulateOptions(questionViewModel);
            return View(questionViewModel);
        }

        [HttpPost]
        public ActionResult QuestionsEdit(int id, QuestionViewModel model)
        {
            var questionBusiness = Mapper.Map<QuestionBusiness>(model);
            _questionsBl.Update(questionBusiness);
            return RedirectToAction("QuestionsIndex");
        }

        public ActionResult QuestionsDelete(int id)
        {
            _questionsBl.Remove(id);
            return RedirectToAction("QuestionsIndex");
        }

        void PopulateOptions(QuestionViewModel model)
        {
            model.Types = _questionTypeBl.GetAll().Select(d => new SelectListItem() { Text = d.Value, Value = d.Id.ToString() }).ToList();
        }

    }
}
