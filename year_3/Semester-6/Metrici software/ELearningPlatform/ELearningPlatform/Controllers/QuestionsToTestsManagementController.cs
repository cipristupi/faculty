﻿using AutoMapper;
using ELearningPlatform.BusinessLayer;
using ELearningPlatform.Models.BusinessModels;
using ELearningPlatform.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ELearningPlatform.Controllers
{
    public class QuestionsToTestsManagementController : Controller
    {
        QuestionsToTestsBl _questionsToTestsBl = new QuestionsToTestsBl();
        TestsManagementBl _testsBl = new TestsManagementBl();
        QuestionsManagementBl _questionsBl = new QuestionsManagementBl();

        public ActionResult QuestionsToTestsIndex()
        {
            var questionsToTests = _questionsToTestsBl.GetAll().Select(item => Mapper.Map<QuestionsToTestsViewModel>(item)).ToList();
            return View("QuestionsToTestsIndex", questionsToTests);
        }

        
        public ActionResult QuestionsToTestsCreate()
        {
            QuestionsToTestsViewModel model = new QuestionsToTestsViewModel();
            PopulateOptions(model);
            return View(model);
        }

        [HttpPost]
        public ActionResult QuestionsToTestsCreate(QuestionsToTestsViewModel model)
        {
            var questionsToTestsBusiness = new QuestionsToTestsBusiness()
            {
                QuestionId = model.QuestionId,
                TestId = model.TestId
            };
            _questionsToTestsBl.Add(questionsToTestsBusiness);
            return RedirectToAction("QuestionsToTestsIndex");
        }

        public ActionResult QuestionsToTestsEdit(int id)
        {
            var questionsToTestsViewModel = Mapper.Map<QuestionsToTestsViewModel>(_questionsToTestsBl.GetById(id));
            PopulateOptions(questionsToTestsViewModel);
            return View(questionsToTestsViewModel);
        }

        [HttpPost]
        public ActionResult QuestionsToTestsEdit(int id, QuestionsToTestsViewModel model)
        {
            var questionsToTestsBusiness = Mapper.Map<QuestionsToTestsBusiness>(model);
            _questionsToTestsBl.Update(questionsToTestsBusiness);
            return RedirectToAction("QuestionsToTestsIndex");
        }

        // GET: QuestionsToTestsManagement/Delete/5
        public ActionResult QuestionsToTestsDelete(int id)
        {
            _questionsToTestsBl.Remove(id);
            return RedirectToAction("QuestionsToTestsIndex");
        }

        void PopulateOptions(QuestionsToTestsViewModel model)
        {
            model.Tests = _testsBl.GetAll().Select(d => new SelectListItem() { Text = d.Name, Value = d.Id.ToString() }).ToList();
            model.Questions = _questionsBl.GetAll().Select(d => new SelectListItem() { Text = d.Question, Value = d.Id.ToString() }).ToList();
        }

    }
}
