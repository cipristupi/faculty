﻿using AutoMapper;
using ELearningPlatform.BusinessLayer;
using ELearningPlatform.Controllers.Shared;
using ELearningPlatform.Models.BusinessModels;
using ELearningPlatform.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ELearningPlatform.Controllers
{
    public class AssignmentsManagementController : BaseController
    {
        private readonly AssignmentsManagementBl _assignmentsManagementBl = new AssignmentsManagementBl();
        private readonly GradeSchemasManagementBl _gradesSchemaBl = new GradeSchemasManagementBl();
        private readonly AssignmentsCategoriesManagementBl _assignmentsCategoriesBl = new AssignmentsCategoriesManagementBl();

        public ActionResult AssignmentsIndex()
        {
            var assignments = _assignmentsManagementBl.GetAll().Select(Mapper.Map<AssignmentViewModel>).ToList();
            var gradeSchemas = _gradesSchemaBl.GetAll().Select(Mapper.Map<GradeSchemaViewModel>).ToList();
            var assignmentsCategories = _assignmentsCategoriesBl.GetAll().Select(Mapper.Map<AssignmentCategoryViewModel>).ToList();
            Tuple<List<AssignmentViewModel>, List<GradeSchemaViewModel>, List<AssignmentCategoryViewModel>> tuple = 
                new Tuple<List<AssignmentViewModel>, List<GradeSchemaViewModel>, List<AssignmentCategoryViewModel>>(assignments, gradeSchemas, assignmentsCategories);
            return View("AssignmentsIndex", tuple);
        }

        public ActionResult AssignmentsCreate()
        {
            AssignmentViewModel model = new AssignmentViewModel
            {
                AvailableGradeSchemas = GetGradesSchemaAsSelectList(),
                AvailableCategories = GetCategoriesAsSelectList()
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult AssignmentsCreate(AssignmentViewModel model)
        {
            var assignmentBusiness = new AssignmentBusiness()
            {
                Name = model.Name,
                Description = model.Description,
                Type = model.Type,
                DeadlineDate = model.DeadlineDate,
                AssignmentsCategoryId = model.SelectedCategory,
                GradeSchemaId = model.SelectedGradeSchema
            };
            _assignmentsManagementBl.Add(assignmentBusiness);
            return RedirectToAction("AssignmentsIndex");
        }

        public ActionResult AssignmentsEdit(int id)
        {
            var assignmentViewModel = Mapper.Map<AssignmentViewModel>(_assignmentsManagementBl.GetById(id));
            assignmentViewModel.AvailableGradeSchemas = GetGradesSchemaAsSelectList();
            assignmentViewModel.SelectedGradeSchema = assignmentViewModel.GradeSchemaId;
            assignmentViewModel.AvailableCategories = GetCategoriesAsSelectList();
            assignmentViewModel.SelectedCategory = assignmentViewModel.AssignmentsCategoryId;
            return View(assignmentViewModel);
        }

        [HttpPost]
        public ActionResult AssignmentsEdit(int id, AssignmentViewModel model)
        {
            var assignmentBusiness = Mapper.Map<AssignmentBusiness>(model);
            assignmentBusiness.GradeSchemaId = model.SelectedGradeSchema;
            assignmentBusiness.AssignmentsCategoryId = model.SelectedCategory;
            _assignmentsManagementBl.Update(assignmentBusiness);
            return RedirectToAction("AssignmentsIndex");
        }

        public ActionResult AssignmentsDelete(int id)
        {
            _assignmentsManagementBl.Remove(id);
            return RedirectToAction("AssignmentsIndex");
        }

        private List<SelectListItem> GetGradesSchemaAsSelectList()
        {
            return _gradesSchemaBl.GetAll().Select(x => new SelectListItem()
            {
                Value = x.Id.ToString(),
                Text = x.Name
            }).ToList();
        }

        private List<SelectListItem> GetCategoriesAsSelectList()
        {
            return _assignmentsCategoriesBl.GetAll().Select(x => new SelectListItem()
            {
                Value = x.Id.ToString(),
                Text = x.Name
            }).ToList();
        }
    }
}
