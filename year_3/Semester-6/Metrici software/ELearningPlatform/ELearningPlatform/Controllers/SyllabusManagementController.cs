﻿using AutoMapper;
using ELearningPlatform.BusinessLayer;
using ELearningPlatform.Controllers.Shared;
using ELearningPlatform.Models.BusinessModels;
using ELearningPlatform.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ELearningPlatform.Models;

namespace ELearningPlatform.Controllers
{
    public class SyllabusManagementController : BaseController
    {
        private readonly SyllabusManagementBl _syllabusManagementBl = new SyllabusManagementBl();
        private readonly CoursesManagementBl _coursesManagementBl = new CoursesManagementBl();
        public ActionResult SyllabusIndex()
        {
            List<SyllabusViewModel> syllabus = GetSyllabusByRole();
            return View("SyllabusIndex", syllabus);
        }

        public ActionResult SyllabusCreate(int? courseId)
        {
            SyllabusViewModel model = new SyllabusViewModel();
            if (courseId.HasValue)
            {
                model.CourseId = courseId.Value;
                model.CourseName = _coursesManagementBl.GetById(courseId.Value).Name;
            }
            PopulateOptions(model);
            return View(model);
        }

        [HttpPost]
        public ActionResult SyllabusCreate(SyllabusViewModel model)
        {
            if (ModelState.IsValid)
            {
                var syllabusBusiness = Mapper.Map<SyllabusBusiness>(model);
                syllabusBusiness.SyllabusContent = Server.HtmlEncode(model.SyllabusContent);
                _syllabusManagementBl.Add(syllabusBusiness);
                PopulateOptions(model);
                model.CourseName = _coursesManagementBl.GetById(model.CourseId).Name;
                return View(model);
            }
            ViewBag.ModelState = ModelState;
            PopulateOptions(model);
            model.CourseName = _coursesManagementBl.GetById(model.CourseId).Name;
            return View(model);
        }

        public ActionResult SyllabusEdit(int id)
        {
            var syllabusViewModel = Mapper.Map<SyllabusViewModel>(_syllabusManagementBl.GetById(id));
            PopulateOptions(syllabusViewModel);
            syllabusViewModel.SyllabusContent = Server.HtmlDecode(syllabusViewModel.SyllabusContent);
            return View(syllabusViewModel);
        }

        [HttpPost]
        public ActionResult SyllabusEdit(int id, SyllabusViewModel model)
        {
            if(ModelState.IsValid)
            {

                var syllabusBusiness = Mapper.Map<SyllabusBusiness>(model);
                syllabusBusiness.Name = model.Name;
                syllabusBusiness.SyllabusContent = Server.HtmlEncode(model.SyllabusContent);
                syllabusBusiness.CourseId = model.CourseId;
                _syllabusManagementBl.Update(syllabusBusiness);
                PopulateOptions(model);
                model.CourseName = _coursesManagementBl.GetById(model.CourseId).Name;
                return View(model);
            }
            ViewBag.ModelState = ModelState;
            PopulateOptions(model);
            model.CourseName = _coursesManagementBl.GetById(model.CourseId).Name;
            return View(model);
        }

        public ActionResult SyllabusDelete(int id)
        {
            _syllabusManagementBl.Remove(id);
            return RedirectToAction("SyllabusIndex");
        }

        public JsonResult GetSyllabus()
        {
            DataTableResponse response = new DataTableResponse()
            {
                aaData = new List<List<string>>()
            };
            foreach (var item in _syllabusManagementBl.GetAll())
            {
                response.aaData.Add(new List<string>()
                {
                    item.Name,
                    item.Id.ToString()
                });
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SyllabusDetails(int id)
        {
            var syllabusViewModel = Mapper.Map<SyllabusViewModel>(_syllabusManagementBl.GetById(id));
            PopulateOptions(syllabusViewModel);
            syllabusViewModel.SyllabusContent = Server.HtmlDecode(syllabusViewModel.SyllabusContent);
            return View(syllabusViewModel);
        }

        private void PopulateOptions(SyllabusViewModel model)
        {
            model.Courses = _coursesManagementBl.GetAll().Select(c =>
            new SelectListItem()
            {
                Text = c.Name,
                Value = c.Id.ToString()
            }).ToList();
        }

        private List<SyllabusViewModel> GetSyllabusByRole()
        {
            var syllabus = _syllabusManagementBl.GetAll();
            if (UserRoles.Contains(UserRolesEnum.Teacher))
            {
                syllabus = _syllabusManagementBl.GetAllByUserId(LoggedUser.Id);
            }
            
            return syllabus.Select(Mapper.Map<SyllabusViewModel>).ToList();
        } 
    }
}
