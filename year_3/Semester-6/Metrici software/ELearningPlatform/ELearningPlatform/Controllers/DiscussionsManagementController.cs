﻿using System;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using ELearningPlatform.BusinessLayer;
using ELearningPlatform.Controllers.Shared;
using ELearningPlatform.Models.BusinessModels;
using ELearningPlatform.Models.ViewModels;
using System.Collections.Generic;
using System.Linq;
using ELearningPlatform.Models;

namespace ELearningPlatform.Controllers
{
    public class DiscussionsManagementController : BaseController
    {
        readonly DiscussionsManagementBl _discussionsManagementBl;
        readonly CommentsManagementBl _commentsManagementBl = new CommentsManagementBl();

        public DiscussionsManagementController()
        {
            _discussionsManagementBl = new DiscussionsManagementBl();
        }
        public ActionResult DiscussionsIndex()
        {
            List<DiscussionViewModel> discussions =
                _discussionsManagementBl.GetAll().Select(item => Mapper.Map<DiscussionViewModel>(item)).ToList();
            return View("DiscussionsIndex", discussions);
        }
        public ActionResult DiscussionsCreate(int? parentId, int? parentType)
        {
            DiscussionViewModel model = new DiscussionViewModel();
            if (parentId.HasValue)
            {
                model.ParentId = parentId.Value;
            }
            if (parentType.HasValue)
            {
                model.ParentType = parentType.Value;
            }
            PopulateModel(model);
            return View(model);
        }
        [HttpPost]
        public ActionResult DiscussionsCreate(DiscussionViewModel model)
        {
            if (ModelState.IsValid)
            {
                DiscussionBusiness discussionBusiness = Mapper.Map<DiscussionBusiness>(model);
                discussionBusiness.Description = Server.HtmlEncode(discussionBusiness.Description);
                _discussionsManagementBl.Add(discussionBusiness);
                ViewBag.Succes = true;
                PopulateModel(model);
                return View(model);
            }
            ViewBag.ModelState = ModelState;
            return View(model);
        }
        public ActionResult DiscussionsEdit(int id)
        {
            var discussionViewModel = Mapper.Map<DiscussionViewModel>(_discussionsManagementBl.GetById(id));
            discussionViewModel.Description = Server.HtmlDecode(discussionViewModel.Description);
            PopulateModel(discussionViewModel);
            return View(discussionViewModel);
        }
        [HttpPost]
        public ActionResult DiscussionsEdit(DiscussionViewModel model)
        {
            if (ModelState.IsValid)
            {
                var discussionBusiness = Mapper.Map<DiscussionBusiness>(model);
                discussionBusiness.Description = Server.HtmlEncode(discussionBusiness.Description);
                _discussionsManagementBl.Update(discussionBusiness);
                ViewBag.Succes = true;
                PopulateModel(model);
                return View(model);
            }
            PopulateModel(model);
            ViewBag.ModelState = ModelState;
            return View(model);
        }

        public ActionResult DiscussionsDelete(int id)
        {
            _discussionsManagementBl.Remove(id);
            //Response.Redirect(Request.RawUrl);
            return RedirectToAction("DiscussionsIndex");
        }

        public ActionResult DiscussionsDetails(int id)
        {
            var discussionViewModel = Mapper.Map<DiscussionViewModel>(_discussionsManagementBl.GetByIdWithComments(id));
            discussionViewModel.Description = Server.HtmlDecode(discussionViewModel.Description);
            return View(discussionViewModel);
        }

        [HttpPost]
        public ActionResult DiscussionsDetails(DiscussionViewModel model)
        {
            CommentBusiness commentBusiness = Mapper.Map<CommentBusiness>(model.Comment);
            commentBusiness.CommentContent = Server.HtmlEncode(commentBusiness.CommentContent);
            commentBusiness.DateCreated = DateTime.Now;
            commentBusiness.UserId = LoggedUser.Id;
            commentBusiness.ParentId = model.Id;
            commentBusiness.ParentType = (int)ParentTypes.Discussion;
            _commentsManagementBl.Add(commentBusiness);

            return RedirectToAction("DiscussionsDetails", new { id = model.Id });
        }

        void PopulateModel(DiscussionViewModel model)
        {
            GradeSchemasManagementBl gradeSchemasManagementBl = new GradeSchemasManagementBl();
            GradeValuesManagementBl gradeValuesManagementBl = new GradeValuesManagementBl();
            model.GradeSchemas = gradeSchemasManagementBl.GetAll().Select(x => new SelectListItem()
            {
                Value = x.Id.ToString(),
                Text = x.Name
            }).ToList();
            model.GradeValues = new List<SelectListItem>
            {
                new SelectListItem()
                {
                    Value = string.Empty,
                    Text = string.Empty
                }
            };
            model.GradeValues.AddRange(gradeValuesManagementBl.GetAll().Select(x => new SelectListItem()
            {
                Value = x.Id.ToString(),
                Text = x.Value
            }).ToList());
        }

    }
}