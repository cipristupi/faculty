﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using ELearningPlatform.BusinessLayer;
using ELearningPlatform.Controllers.Shared;
using ELearningPlatform.Models.ViewModels;

namespace ELearningPlatform.Controllers
{
    public class DashboardsController : BaseController
    {
        private readonly AnnouncementsManagementBl _announcementsManagementBl;

        public DashboardsController()
        {
            _announcementsManagementBl = new AnnouncementsManagementBl();
        }

        public ActionResult AdminDashboardIndex()
        {
            var announcements = _announcementsManagementBl.GetAll().Select(Mapper.Map<AnnouncementViewModel>);
            return View("AdminDashboardIndex", announcements);
        }

        public ActionResult TeacherDashboardIndex()
        {
            var announcements =
                _announcementsManagementBl.GetAllGeneraleAnnouncement().Select(Mapper.Map<AnnouncementViewModel>);
            return View("TeacherDashboardIndex", announcements);
        }

        public ActionResult StudentDashboardIndex()
        {
            var announcementsRelatedToCourses =
                _announcementsManagementBl.GetAllAnnouncementRelatedToCourses(LoggedUser.Id)
                    .Select(Mapper.Map<AnnouncementViewModel>);
            var generalAnnouncements =
                _announcementsManagementBl.GetAllGeneraleAnnouncement().Select(Mapper.Map<AnnouncementViewModel>);
            var announcements = new List<AnnouncementViewModel>();
            announcements.AddRange(announcementsRelatedToCourses);
            announcements.AddRange(generalAnnouncements);
            return View("StudentDashboardIndex", announcements);
        }
    }
}