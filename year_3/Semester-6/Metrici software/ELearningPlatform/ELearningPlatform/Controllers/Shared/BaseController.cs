﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using ELearningPlatform.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace ELearningPlatform.Controllers.Shared
{
    public class BaseController : RootController
    {
        protected ApplicationUser LoggedUser { get; private set; }

        protected List<UserRolesEnum> UserRoles { get; set; }

        protected override void Initialize(RequestContext requestContext)
        {

            base.Initialize(requestContext);
            LoggedUser = UserManager.FindById(User.Identity.GetUserId());
            UserRoles = new List<UserRolesEnum>();

            List<string> userRoles = new List<string>();
            if (LoggedUser != null)
            {
                userRoles = UserManager.GetRolesAsync(LoggedUser.Id).Result.ToList();
                foreach (string userRoleName in userRoles)
                {
                    UserRoles.Add((UserRolesEnum)Enum.Parse(typeof(UserRolesEnum), userRoleName));
                }
            }
            ViewBag.CurrentRole = userRoles.Count != 0 ? userRoles[0] : "";
            ViewBag.UserRoles = UserRoles;
            ViewBag.LoggedUser = LoggedUser;
        }
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            ViewBag.SkipLayout = false;
            ViewBag.Succes = false;
        }
    }
}