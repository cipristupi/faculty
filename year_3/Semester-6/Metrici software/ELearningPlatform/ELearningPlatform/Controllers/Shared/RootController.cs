﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ELearningPlatform.Models;
using Microsoft.AspNet.Identity.Owin;

namespace ELearningPlatform.Controllers.Shared
{
    public class RootController : Controller
    {
        private ApplicationUserManager m_userManager;
        private readonly ApplicationRoleManager m_roleManager;

        private ApplicationSignInManager m_signInManager;

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return m_signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set { m_signInManager = value; }
        }

        protected ApplicationUserManager UserManager
        {
            get
            {
                return m_userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                m_userManager = value;
            }
        }

        protected ApplicationRoleManager RoleManager
        {
            get
            {
                return m_roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            }
        }

        public RootController(ApplicationRoleManager roleManager)
        {
            m_roleManager = roleManager;
        }

        protected RootController()
        {

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && UserManager != null)
            {
                UserManager.Dispose();
                UserManager = null;
            }
            base.Dispose(disposing);
        }
    }
}