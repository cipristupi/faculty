﻿using AutoMapper;
using ELearningPlatform.BusinessLayer;
using ELearningPlatform.Controllers.Shared;
using ELearningPlatform.Models.BusinessModels;
using ELearningPlatform.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ELearningPlatform.Models;

namespace ELearningPlatform.Controllers
{
    public class AnnouncementsManagementController : BaseController
    {
        private readonly AnnouncementsManagementBl _announcementsManagementBl = new AnnouncementsManagementBl();

        public ActionResult AnnouncementsIndex()
        {
            var announcements = new List<AnnouncementViewModel>();
            if (UserRoles.Contains(UserRolesEnum.Teacher))
            {
                announcements = _announcementsManagementBl.GetAllGeneraleAnnouncement().Select(Mapper.Map<AnnouncementViewModel>).ToList();
            }
            if (UserRoles.Contains(UserRolesEnum.Student))
            {
                var announcementsRelatedToCourses =
                _announcementsManagementBl.GetAllAnnouncementRelatedToCourses(LoggedUser.Id)
                    .Select(Mapper.Map<AnnouncementViewModel>);
                var generalAnnouncements =
                    _announcementsManagementBl.GetAllGeneraleAnnouncement().Select(Mapper.Map<AnnouncementViewModel>);
                announcements.AddRange(announcementsRelatedToCourses);
                announcements.AddRange(generalAnnouncements);
            }
            if (UserRoles.Contains(UserRolesEnum.Admin))
            {
                announcements = _announcementsManagementBl.GetAll().Select(Mapper.Map<AnnouncementViewModel>).ToList();
            }
            return View("AnnouncementsIndex", announcements);
        }


        public ActionResult AnnouncementsCreate(int? parentId, int? parentType)
        {
            var model = new AnnouncementViewModel();
            if (parentId.HasValue)
            {
                model.ParentId = parentId.Value;
            }
            if (parentType.HasValue)
            {
                model.ParentType = parentType.Value;
            }
            return View(model);
        }


        [HttpPost]
        public ActionResult AnnouncementsCreate(AnnouncementViewModel model)
        {
            if (ModelState.IsValid)
            {
                var announcementBusiness = new AnnouncementBusiness()
                {
                    Name = model.Name,
                    AnnouncementContent = Server.HtmlEncode(model.AnnouncementContent),
                    DateCreated = DateTime.Now,
                    ParentId = model.ParentId,
                    UserId = LoggedUser.Id,
                    ParentType = model.ParentType
                };
                _announcementsManagementBl.Add(announcementBusiness);
                ViewBag.Succes = true;
                return View(model);
            }
            ViewBag.ModelState = ModelState;
            return View(model);
        }


        public ActionResult AnnouncementsEdit(int id)
        {
            var announcementViewModel = Mapper.Map<AnnouncementViewModel>(_announcementsManagementBl.GetById(id));
            announcementViewModel.AnnouncementContent = Server.HtmlDecode(announcementViewModel.AnnouncementContent);
            return View(announcementViewModel);
        }


        [HttpPost]
        public ActionResult AnnouncementsEdit(int id, AnnouncementViewModel model)
        {
            if (ModelState.IsValid)
            {
                var announcementBusiness = Mapper.Map<AnnouncementBusiness>(model);
                announcementBusiness.AnnouncementContent = Server.HtmlEncode(announcementBusiness.AnnouncementContent);
                _announcementsManagementBl.Update(announcementBusiness);
                ViewBag.Succes = true;
                return View(model);
            }
            ViewBag.ModelState = ModelState;
            return View(model);
        }

        public ActionResult Delete(int id)
        {
            _announcementsManagementBl.Remove(id);
            return RedirectToAction("AnnouncementsIndex");
        }

        public ActionResult AnnouncementDetails(int id)
        {
            var announcementViewModel = Mapper.Map<AnnouncementViewModel>(_announcementsManagementBl.GetById(id));
            announcementViewModel.AnnouncementContent = Server.HtmlDecode(announcementViewModel.AnnouncementContent);
            return View(announcementViewModel);
        }
    }
}
