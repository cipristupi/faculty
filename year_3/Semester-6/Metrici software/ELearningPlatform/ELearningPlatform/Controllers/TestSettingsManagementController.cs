﻿using AutoMapper;
using ELearningPlatform.BusinessLayer;
using ELearningPlatform.Models.BusinessModels;
using ELearningPlatform.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ELearningPlatform.Controllers
{
    public class TestSettingsManagementController : Controller
    {
        TestSettingsBl _testSettingsBl = new TestSettingsBl();
        TestsManagementBl _testsBl = new TestsManagementBl();

        public ActionResult TestSettingsIndex()
        {
            var testSettings = _testSettingsBl.GetAll().Select(item => Mapper.Map<TestSettingsViewModel>(item)).ToList();
            return View("TestSettingsIndex", testSettings);
        }

        public ActionResult TestSettingsCreate()
        {
            TestSettingsViewModel model = new TestSettingsViewModel();
            PopulateOptions(model);
            return View(model);
        }

        [HttpPost]
        public ActionResult TestSettingsCreate(TestSettingsViewModel model)
        {
            var testSettingsBusiness = new TestSettingBusiness()
            {
                CanRepetTest = model.CanRepetTest,
                GoodAnswersIntervalForSeen = model.GoodAnswersIntervalForSeen,
                CanSeeAnswers = model.CanSeeAnswers,
                HigherGrade = model.HigherGrade,
                LastGrade = model.LastGrade,
                OneQuestionAtAnyMoment = model.OneQuestionAtAnyMoment,
                NumberOfRepetitions = model.NumberOfRepetitions,
                RandomOrderQuestions = model.RandomOrderQuestions,
                TestHasPassword = model.TestHasPassword,
                TestId = model.TestId,
                TestPassword = model.TestPassword,
                TimeLimit = model.TimeLimit,
                TimeLimitValue = model.TimeLimitValue
            };
            _testSettingsBl.Add(testSettingsBusiness);
            return RedirectToAction("TestSettingsIndex");
        }

        public ActionResult TestSettingsEdit(int id)
        {
            var testSettingViewModel = Mapper.Map<TestSettingsViewModel>(_testSettingsBl.GetById(id));
            PopulateOptions(testSettingViewModel);
            return View(testSettingViewModel);
        }

        [HttpPost]
        public ActionResult TestSettingsEdit(int id, TestSettingsViewModel model)
        {
            var testSettingBusiness = Mapper.Map<TestSettingBusiness>(model);
            _testSettingsBl.Update(testSettingBusiness);
            return RedirectToAction("TestSettingsIndex");
        }

        public ActionResult TestSettingsDelete(int id)
        {
            _testSettingsBl.Remove(id);
            return RedirectToAction("TestSettingsIndex");
        }

        void PopulateOptions(TestSettingsViewModel model)
        {
            model.Tests = _testsBl.GetAll().Select(d => new SelectListItem() { Text = d.Name, Value = d.Id.ToString() }).ToList();
        }
    }
}
