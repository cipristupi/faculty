﻿using AutoMapper;
using ELearningPlatform.BusinessLayer;
using ELearningPlatform.Controllers.Shared;
using ELearningPlatform.Models.BusinessModels;
using ELearningPlatform.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ELearningPlatform.Controllers
{
    public class WorkingGroupsManagementController : BaseController
    {
        private WorkingGroupsManagementBl _workingGroupsManagementBl = new WorkingGroupsManagementBl();

        public ActionResult WorkingGroupsIndex()
        {
            List<WorkingGroupViewModel> _workingGroups = new List<WorkingGroupViewModel>();
            foreach (var item in _workingGroupsManagementBl.GetAll())
            {
                _workingGroups.Add(Mapper.Map<WorkingGroupViewModel>(item));
            }
            return View("WorkingGroupsIndex", _workingGroups);
        }


        public ActionResult WorkingGroupsCreate()
        {
            return View();
        }


        [HttpPost]
        public ActionResult WorkingGroupsCreate(WorkingGroupViewModel model)
        {
            var announcementBusiness = new WorkingGroupBusiness()
            {
                Name = model.Name,
                Description = model.Description,
                CourseId = model.CourseId
            };
            _workingGroupsManagementBl.Add(announcementBusiness);
            return RedirectToAction("WorkingGroupsIndex");
        }


        public ActionResult WorkingGroupsEdit(int id)
        {
            var workingGroupsViewModel = Mapper.Map<WorkingGroupViewModel>(_workingGroupsManagementBl.GetById(id));
            return View(workingGroupsViewModel);
        }


        [HttpPost]
        public ActionResult WorkingGroupsEdit(int id, WorkingGroupViewModel model)
        {
            var workingGroupsBusiness = Mapper.Map<WorkingGroupBusiness>(model);
            _workingGroupsManagementBl.Update(workingGroupsBusiness);
            return RedirectToAction("WorkingGroupsIndex");
        }

        public ActionResult Delete(int id)
        {
            _workingGroupsManagementBl.Remove(id);
            return RedirectToAction("WorkingGroupsIndex");
        }
    }
}
