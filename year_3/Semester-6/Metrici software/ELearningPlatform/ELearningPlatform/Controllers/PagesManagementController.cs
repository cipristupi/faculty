﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ELearningPlatform.Models.ViewModels;
using ELearningPlatform.BusinessLayer;
using AutoMapper;
using ELearningPlatform.Controllers.Shared;
using ELearningPlatform.Models;
using ELearningPlatform.Models.BusinessModels;

namespace ELearningPlatform.Controllers
{
    public class PagesManagementController : BaseController
    {
        readonly PagesBl _pagesBl = new PagesBl();
        readonly PageSectionsBl _pageSectionsBl = new PageSectionsBl();
        readonly LectureBl _lectureBl = new LectureBl();

        public ActionResult PagesIndex()
        {
            var pages = _pagesBl.GetAll().Select(Mapper.Map<PageViewModel>).ToList();
            return View("PagesIndex", pages);
        }

        public ActionResult PagesCreate(int? id)
        {
            PageViewModel model = new PageViewModel();
            if (id.HasValue)
            {
                model.ParentId = id.Value;
            }
            else
            {
                model.Lectures = _lectureBl.GetAll().Select(x => new SelectListItem() {Text = x.Name, Value = x.Id.ToString()}).ToList();
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult PagesCreate(PageViewModel model)
        {
            var pageBusiness = new PageBusiness()
            {
                Name = model.Name,
                ParentId = model.ParentId
            };
            int id = _pagesBl.AddAndReturnsId(pageBusiness);
            var pageSectionsBusiness = model.PageSections.Select(Mapper.Map<PageSectionBusiness>).ToList();
            for (int index = 0; index < pageSectionsBusiness.Count; index++)
            {
                var pageSection = pageSectionsBusiness[index];
                pageSectionsBusiness[index].SectionContent = Server.HtmlEncode(pageSection.SectionContent);
            }
            _pageSectionsBl.AddPages(pageSectionsBusiness, id);
            var url = Url.Action("PagesIndex");
            return JavaScript("window.location = '" + url + "'");
        }

        public ActionResult PagesEdit(int id)
        {
            var model = Mapper.Map<PageViewModel>(_pagesBl.GetById(id));
            model.PageSections = new List<PageSectionViewModel>();
            foreach (var pageSectionBusiness in _pageSectionsBl.GetAllByPage(id))
            {
                var pageSection = Mapper.Map<PageSectionViewModel>(pageSectionBusiness);
                pageSection.SectionContent = Server.HtmlDecode(pageSection.SectionContent);
                model.PageSections.Add(pageSection);
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult PagesEdit(PageViewModel model)
        {
            var pageBusiness = Mapper.Map<PageBusiness>(model);
            _pagesBl.Update(pageBusiness);
            var pageSectionsBusiness = model.PageSections.Select(Mapper.Map<PageSectionBusiness>).ToList();
            for (int index = 0; index < pageSectionsBusiness.Count; index++)
            {
                var pageSection = pageSectionsBusiness[index];
                pageSectionsBusiness[index].SectionContent = Server.HtmlEncode(pageSection.SectionContent);
            }
            _pageSectionsBl.UpdatePages(pageSectionsBusiness, model.Id);
            return RedirectToAction("PagesIndex");
        }

        public ActionResult PagesDelete(int id)
        {
            _pagesBl.Remove(id);
            return RedirectToAction("PagesIndex");
        }


        public ActionResult PagesDetails(int id)
        {
            var model = Mapper.Map<PageViewModel>(_pagesBl.GetById(id));
            model.PageSections = new List<PageSectionViewModel>();
            foreach (var pageSectionBusiness in _pageSectionsBl.GetAllByPage(id))
            {
                var pageSection = Mapper.Map<PageSectionViewModel>(pageSectionBusiness);
                pageSection.SectionContent = Server.HtmlDecode(pageSection.SectionContent);
                model.PageSections.Add(pageSection);
            }
            return View(model);
        }
        [HttpGet]
        public JsonResult GetPages(DataTableRequest request)
        {
            DataTableResponse response = new DataTableResponse()
            {
                aaData = new List<List<string>>()
            };
            var pages = _pagesBl.GetAll().Select(Mapper.Map<PageViewModel>).ToList();
            foreach (var item in pages)
            {
                var pagesSectionCount = _pageSectionsBl.GetAllByPage(item.Id).Count();
                response.aaData.Add(new List<string>()
                {
                    item.Name,
                    pagesSectionCount.ToString(),
                    item.Id.ToString()
                });
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}
