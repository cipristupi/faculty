﻿using System;
using System.Collections.Generic;
using AutoMapper;
using ELearningPlatform.BusinessLayer;
using ELearningPlatform.Models.BusinessModels;
using ELearningPlatform.Models.ViewModels;
using System.Linq;
using System.Web.Mvc;
using ELearningPlatform.Controllers.Shared;
using ELearningPlatform.Models;

namespace ELearningPlatform.Controllers
{
    public class TestsManagementController : BaseController
    {
        readonly TestsManagementBl _testManagementBl = new TestsManagementBl();
        readonly CoursesManagementBl _courseManagementBl = new CoursesManagementBl();

        public ActionResult TestsIndex()
        {
            var tests = new List<TestViewModel>();
            if (UserRoles.Contains(UserRolesEnum.Teacher))
            {
                tests =
                    _testManagementBl.GetAllTestByTeacherId(LoggedUser.Id).Select(Mapper.Map<TestViewModel>).ToList();
            }
            if (UserRoles.Contains(UserRolesEnum.Student))
            {
                tests =
                    _testManagementBl.GetAllTestsByStudentId(LoggedUser.Id).Select(Mapper.Map<TestViewModel>).ToList();
            }
            if (UserRoles.Contains(UserRolesEnum.Admin))
            {
                tests = _testManagementBl.GetAll().Select(Mapper.Map<TestViewModel>).ToList();
            }
            return View("TestsIndex", tests);
        }

        public ActionResult TestsCreate(int? courseId)
        {
            TestViewModel model = new TestViewModel();
            if (courseId.HasValue)
            {
                model.CourseId = courseId.Value;
                model.CourseName = _courseManagementBl.GetById(courseId.Value).Name;
            }
            model.StartDate = DateTime.Now;
            PopulateOptions(model);
            return View(model);
        }

        [HttpPost]
        public ActionResult TestsCreate(TestViewModel model)
        {
            if (ModelState.IsValid)
            {
                var testBusiness = new TestBusiness()
                {
                    Name = model.Name,
                    Description = model.Description,
                    DeadLine = model.DeadLine,
                    Type = "test",
                    StartDate = model.StartDate,
                    EndDate = model.EndDate,
                    CourseId = model.CourseId
                };
                _testManagementBl.Add(testBusiness);
                ViewBag.Succes = true;
                model.CourseName = _courseManagementBl.GetById(model.CourseId).Name;
                PopulateOptions(model);
                return View(model);
            }
            model.CourseName = _courseManagementBl.GetById(model.CourseId).Name;
            ViewBag.ModelState = ModelState;
            PopulateOptions(model);
            return View(model);
        }

        public ActionResult TestsEdit(int id)
        {
            var testViewModel = Mapper.Map<TestViewModel>(_testManagementBl.GetById(id));
            PopulateOptions(testViewModel);
            return View(testViewModel);
        }

        [HttpPost]
        public ActionResult TestsEdit(int id, TestViewModel model)
        {
            if (ModelState.IsValid)
            {
                var testBusiness = Mapper.Map<TestBusiness>(model);
                testBusiness.Type = "test";
                _testManagementBl.Update(testBusiness);
                ViewBag.Succes = true;
                PopulateOptions(model);
                return View(model);
            }
            ViewBag.ModelState = ModelState;
            PopulateOptions(model);
            return View(model);
        }

        public ActionResult TestsDelete(int id)
        {
            _testManagementBl.Remove(id);
            return RedirectToAction("TestsIndex");
        }

        void PopulateOptions(TestViewModel model)
        {
            model.AvailableCourses =
                _courseManagementBl.GetAll()
                    .Select(d => new SelectListItem {Text = d.Name, Value = d.Id.ToString()})
                    .ToList();
            model.Courses = _courseManagementBl.GetAll().Select(c =>
                new SelectListItem()
                {
                    Text = c.Name,
                    Value = c.Id.ToString()
                }).ToList();
        }

        public ActionResult TestDetails(int id)
        {
            var testViewModel = Mapper.Map<TestViewModel>(_testManagementBl.GetById(id));
            PopulateOptions(testViewModel);
            return View(testViewModel);
        }
    }
}
