﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using ELearningPlatform.BusinessLayer;
using ELearningPlatform.Controllers.Shared;
using ELearningPlatform.Models;
using ELearningPlatform.Models.BusinessModels;
using ELearningPlatform.Models.ViewModels;

namespace ELearningPlatform.Controllers
{
    public class DepartmentsManagementController : BaseController
    {
        private readonly DepartmentsManagementBl _departmentsManagementBl = new DepartmentsManagementBl();
        public ActionResult DepartmentsIndex()
        {
            return View("DepartmentsIndex");
        }

        public ActionResult DepartmentsCreate()
        {
            return View();
        }

        [HttpPost]
        public ActionResult DepartmentsCreate(DepartmentViewModel model)
        {
            if (ModelState.IsValid)
            {
                var departmentBusiness = Mapper.Map<DepartmentBusiness>(model);
                departmentBusiness.Description = Server.HtmlEncode(departmentBusiness.Description);
                _departmentsManagementBl.Add(departmentBusiness);
                return RedirectToAction("DepartmentsIndex");
            }
            return View(model);
        }

        public ActionResult DepartmentsEdit(int id)
        {
            var departmentViewModel = Mapper.Map<DepartmentViewModel>(_departmentsManagementBl.GetById(id));
            departmentViewModel.Description = Server.HtmlDecode(departmentViewModel.Description);
            return View(departmentViewModel);
        }

        [HttpPost]
        public ActionResult DepartmentsEdit(DepartmentViewModel model)
        {
            if (ModelState.IsValid)
            {
                var departmentBusiness = Mapper.Map<DepartmentBusiness>(model);
                departmentBusiness.Description = Server.HtmlEncode(departmentBusiness.Description);
                _departmentsManagementBl.Update(departmentBusiness);
                ViewBag.Succes = true;
                return View(model);
            }
            ViewBag.ModelState = ModelState;
            return View(model);
        }


        public ActionResult Delete(int id)
        {
            _departmentsManagementBl.Remove(id);
            return RedirectToAction("DepartmentsIndex");
        }

        [HttpGet]
        public JsonResult GetDepatments(DataTableRequest request)
        {
            DataTableResponse response = new DataTableResponse()
            {
                aaData = new List<List<string>>()
            };
            foreach (var item in _departmentsManagementBl.GetAll())
            {
                response.aaData.Add(new List<string>()
                {
                    item.Name,
                    item.Description,
                    item.Id.ToString()
                });
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}
