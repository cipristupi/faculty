﻿using System.Web;
using System.Web.Mvc;
using AutoMapper;
using ELearningPlatform.BusinessLayer;
using ELearningPlatform.Controllers.Shared;
using ELearningPlatform.Models.BusinessModels;
using ELearningPlatform.Models.ViewModels;
using System.Collections.Generic;


namespace ELearningPlatform.Controllers
{
    public class DiscussionParticipantsManagementController : BaseController
    {
        private DiscussionParticipantsManagementBl _discussionParticipantsManagementBl = new DiscussionParticipantsManagementBl();

        public ActionResult DiscussionParticipantsIndex()
        {
            List<DiscussionParticipantsViewModel> discussionParticipants = new List<DiscussionParticipantsViewModel>();
            foreach (var item in _discussionParticipantsManagementBl.GetAll())
            {
                discussionParticipants.Add(Mapper.Map<DiscussionParticipantsViewModel>(item));
            }
            return View("DiscussionParticipantsIndex", discussionParticipants);
        }
        public ActionResult DiscussionParticipantsCreate()
        {
            return View();
        }
        [HttpPost]
        public ActionResult DiscussionParticipantsCreate(DiscussionParticipantsViewModel model)
        {
            var discussionParticipantsBusiness = new DiscussionParticipantsBusiness()
            {
                UserId = model.UserId,
                DiscussionId = model.DiscussionId
            };
            _discussionParticipantsManagementBl.Add(discussionParticipantsBusiness);
            return RedirectToAction("DiscussionParticipantsIndex");
        }
        // GET: DepartmentsManager/Edit/5
        public ActionResult DiscussionParticipantsEdit(int id)
        {
            var discussionParticipantsViewModel = Mapper.Map<DiscussionParticipantsViewModel>(_discussionParticipantsManagementBl.GetById(id));
            return View(discussionParticipantsViewModel);
        }
        // POST: DepartmentsManager/Edit/5
        [HttpPost]
        public ActionResult DiscussionParticipantsEdit(int id, DiscussionParticipantsViewModel model)
        {
            var discussionParticipantsBusiness = Mapper.Map<DiscussionParticipantsBusiness>(model);
            _discussionParticipantsManagementBl.Update(discussionParticipantsBusiness);
            return RedirectToAction("DiscussionParticipantsIndex");
        }

        // POST: DepartmentsManager/Delete/5

        public ActionResult DiscussionParticipantsDelete(int id)
        {
            _discussionParticipantsManagementBl.Remove(id);
            return RedirectToAction("DiscussionParticipantsIndex");
        }

    }
}