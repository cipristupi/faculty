﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ELearningPlatform.Utils
{
    public class Menu
    {
        private List<MenuItem> menuItems;
        public Menu()
        {
            menuItems = new List<MenuItem>();
        }

        private void InitializeMenu()
        {
            menuItems.Add(new MenuItem()
            {
                ControllerName = "AdminDashboard",
                ActionName = "AdminDashboardIndex",
                DisplayText = "Dashboard"
            });
        }


        public List<MenuItem> MenuList
        {
            get { return menuItems; }
        }
    }
}