﻿using AutoMapper;
using ELearningPlatform.DataLayer.Edmx;
using ELearningPlatform.Models;
using ELearningPlatform.Models.BusinessModels;
using ELearningPlatform.Models.ViewModels;

namespace ELearningPlatform.Utils
{
    public class AutoMapperConfiguration
    {
        public static void Initialize()
        {
            //User
            Mapper.CreateMap<UserViewModel, ApplicationUser>().ReverseMap();
            Mapper.CreateMap<UserBusiness, ApplicationUser>().ReverseMap();
            Mapper.CreateMap<UserBusiness, UserViewModel>().ReverseMap();

            Mapper.CreateMap<UserViewModel, AspNetUsers>().ReverseMap();
            Mapper.CreateMap<UserBusiness, AspNetUsers>().ReverseMap();
            //Role
            Mapper.CreateMap<RoleViewModel, RoleBusiness>().ReverseMap();
            Mapper.CreateMap<RoleBusiness, AspNetRoles>().ReverseMap();
            //Department
            Mapper.CreateMap<DepartmentViewModel, DepartmentBusiness>().ReverseMap();
            Mapper.CreateMap<DepartmentBusiness, Departments>().ReverseMap();

            //Discussion
            Mapper.CreateMap<DiscussionViewModel, DiscussionBusiness>().ReverseMap();
            Mapper.CreateMap<DiscussionBusiness, Discussions>().ReverseMap();

            //DiscussionParticipants
            Mapper.CreateMap<DiscussionParticipantsViewModel, DiscussionParticipantsBusiness>().ReverseMap();
            Mapper.CreateMap<DiscussionParticipantsBusiness, DiscussionParticipants>().ReverseMap();
            //Assignment
            Mapper.CreateMap<AssignmentViewModel, AssignmentBusiness>().ReverseMap();
            Mapper.CreateMap<AssignmentBusiness, Assignments>().ReverseMap();
            //Assignment Category
            Mapper.CreateMap<AssignmentCategoryViewModel, AssignmentCategoryBusiness>().ReverseMap();
            Mapper.CreateMap<AssignmentCategoryBusiness, AssignmentsCategories>().ReverseMap();
            //Assignment Setting
            Mapper.CreateMap<AssignmentSettingViewModel, AssignmentSettingBusiness>().ReverseMap();
            Mapper.CreateMap<AssignmentSettingBusiness, AssignmentsSettings>().ReverseMap();
            //Grade Schema
            Mapper.CreateMap<GradeSchemaViewModel, GradeSchemaBusiness>().ReverseMap();
            Mapper.CreateMap<GradeSchemaBusiness, GradeSchemas>().ReverseMap();
            //Grade Value
            Mapper.CreateMap<GradeValueViewModel, GradeValueBusiness>().ReverseMap();
            Mapper.CreateMap<GradeValueBusiness, GradeValues>().ReverseMap();
            //Course
            Mapper.CreateMap<CourseViewModel, CourseBusiness>().ReverseMap();
            Mapper.CreateMap<CourseBusiness, Courses>().ReverseMap();
            //AssignmentsToCourses
            Mapper.CreateMap<AssignmentsToCoursesViewModel, AssignmentsToCoursesBusiness>().ReverseMap();
            Mapper.CreateMap<AssignmentsToCoursesBusiness, AssignmentsToCourses>().ReverseMap();
            //Syllabus
            Mapper.CreateMap<SyllabusViewModel, SyllabusBusiness>().ReverseMap();
            Mapper.CreateMap<SyllabusBusiness, Syllabus>().ReverseMap();

            //Lectures
            Mapper.CreateMap<LectureViewModel, LectureBusiness>().ReverseMap();
            Mapper.CreateMap<Lectures,LectureBusiness>().ForMember(l => l.LectureSettings, opt => opt.Ignore());
            Mapper.CreateMap<LectureBusiness,Lectures>();

            //Lectures Settings
            Mapper.CreateMap<LectureSettingsViewModel, LectureSettingsBusiness>().ReverseMap();
            Mapper.CreateMap<LectureSettingsBusiness, LectureSettings>();
            Mapper.CreateMap<LectureSettings, LectureSettingsBusiness>();

            //Pages
            Mapper.CreateMap<PageViewModel, PageBusiness>().ReverseMap();
            Mapper.CreateMap<PageBusiness, Pages>().ReverseMap();

            //Page Sections
            Mapper.CreateMap<PageSectionViewModel, PageSectionBusiness>().ReverseMap();
            Mapper.CreateMap<PageSectionBusiness, PageSections>().ReverseMap();

            //Tests
            Mapper.CreateMap<TestViewModel, TestBusiness>().ReverseMap();
            Mapper.CreateMap<TestBusiness, Tests>().ReverseMap();

            //Test Settings
            Mapper.CreateMap<TestSettingsViewModel, TestSettingBusiness>().ReverseMap();
            Mapper.CreateMap<TestSettingBusiness, TestSettings>().ReverseMap();

            //Questions
            Mapper.CreateMap<QuestionViewModel, QuestionBusiness>().ReverseMap();
            Mapper.CreateMap<QuestionBusiness, Questions>().ReverseMap();

            //Question Types
            Mapper.CreateMap<QuestionTypeViewModel, QuestionTypeBusiness>().ReverseMap();
            Mapper.CreateMap<QuestionTypeBusiness, QuestionTypes>().ReverseMap();

            //Bool Questions
            Mapper.CreateMap<BoolQuestionViewModel, BoolQuestionBusiness>().ReverseMap();
            Mapper.CreateMap<BoolQuestionBusiness, BoolQuestions>().ReverseMap();

            //Questions to Tests
            Mapper.CreateMap<QuestionsToTestsViewModel, QuestionsToTestsBusiness>().ReverseMap();
            Mapper.CreateMap<QuestionsToTestsBusiness, QuestionsToTests>().ReverseMap();

            //StaffToDepartaments
            Mapper.CreateMap<StaffToDepartamentViewModel, StaffToDepartamentBusiness>().ReverseMap();
            Mapper.CreateMap<StaffToDepartamentBusiness, StaffToDepartaments>().ReverseMap();
           
            Mapper.CreateMap<StudentsToWorkingGroupViewModel, StudentsToWorkingGroupBusiness>().ReverseMap();
            Mapper.CreateMap<StudentsToWorkingGroupBusiness, StudentsToWorkingGroups>().ReverseMap();

            Mapper.CreateMap<WorkingGroupViewModel, WorkingGroupBusiness>().ReverseMap();
            Mapper.CreateMap<WorkingGroupBusiness, WorkingGroups>().ReverseMap();

            Mapper.CreateMap<EnrolledStudentsToCourseViewModel, EnrolledStudentsToCourseBusiness>().ReverseMap();
            Mapper.CreateMap<EnrolledStudentsToCourseBusiness, EnrolledStudentsToCourses>().ReverseMap();

            Mapper.CreateMap<AnnouncementViewModel, AnnouncementBusiness>().ReverseMap();
            Mapper.CreateMap<AnnouncementBusiness, Announcements>().ReverseMap();

            Mapper.CreateMap<CommentViewModel, CommentBusiness>().ReverseMap();
            Mapper.CreateMap<CommentBusiness, Comments>().ReverseMap();

        }
    }
}
