﻿var Courses = (function () {
    var privates = {};
    privates.bind = function () {

    };
    privates.createRow = function (data, type, row) {
        var menuDivider = '<li class="divider"></li>';
        var editLink = '<li><a href="/CoursesManagement/CoursesEdit/' + data + '">Edit</a></li>';
        var deleteLink = '<li><a href="/CoursesManagement/CoursesDelete/' + data + '">Delete</a></li>';
        var viewLectures = '<li><a href="/CoursesManagement/CoursesLectures/' + data + '" target="_blank">Lectures</a></li>';
        var viewDiscussions = '<li><a href="/CoursesManagement/CoursesDiscussions/' + data + '" target="_blank">Discussions</a></li>';
        var viewTests = '<li><a href="/CoursesManagement/CoursesTests/' + data + '" target="_blank">Tests</a></li>';
        var viewAnnouncements = '<li><a href="/CoursesManagement/CoursesAnnouncements/' + data + '" target="_blank">Announcements</a></li>';

        var createLectures = '<li><a href="/LecturesManagement/LecturesCreate?courseId=' + data + '" target="_blank">Add Lectures</a></li>';
        var createDiscussions = '<li><a href="/DiscussionsManagement/DiscussionsCreate?parentId=' + data + '&parentType=0" target="_blank">Add Discussion</a></li>';
        var createSyllabus = '<li><a href="/SyllabusManagement/SyllabusCreate?courseId=' + data + '" target="_blank">Add Syllabus</a></li>';
        var createAnnouncement = '<li><a href="/AnnouncementsManagement/AnnouncementsCreate?parentId=' + data + '&parentType=0" target="_blank">Add Announcement</a></li>';
        var createTest = '<li><a href="/TestsManagement/TestsCreate?courseId=' + data + '" target="_blank">Add Test</a></li>';
        var manageEnrolledStudents = '<li><a href="/EnrolledStudentsToCourses/EnrolledStudentsToCoursesCreate?courseId=' + data + '" target="_blank">Enrolled Students</a></li>';
        var dropDownId = 'dropdown' + data;
       
        if (window.CurrentRole !== "Admin") {
            deleteLink = '';
        }
        if (window.CurrentRole === "Student") {
            editLink = '';
            deleteLink = '';
            createLectures = '';
            createDiscussions = '';
            createSyllabus = '';
            manageEnrolledStudents = '';
            createAnnouncement = '';
            createTest = '';
        }

        var lectureSubMenu = menuDivider + viewLectures + createLectures;
        var discussionSubMenu = menuDivider + viewDiscussions + createDiscussions + menuDivider;
        var testSubMenu = viewTests + createTest + menuDivider;
        var announcementSubMenu = viewAnnouncements + createAnnouncement + menuDivider;
        var dropDownItems = editLink + deleteLink + lectureSubMenu + discussionSubMenu + testSubMenu+announcementSubMenu+ createSyllabus + manageEnrolledStudents;
        var cell = '<div class="dropdown"><button class="btn btn-default dropdown-toggle" type="button" id="'
            + dropDownId + '"aria-haspopup="true" aria-expanded="true" data-toggle="dropdown"> Actions <span class="caret"></span></button>'
            + '<ul class="dropdown-menu" role="menu" aria-labelledby="' + dropDownId + '">' + dropDownItems + '</ul></div>';
        return cell;
    };
    privates.createLink = function (data, type, row) {
        return '<a href="/CoursesManagement/CoursesDetails/' + row[5] + '" target="_blank">' + data + '</a>';
    };
    privates.initTable = function () {
        $('#coursesTable').DataTable({
            "ajax": '/CoursesManagement/GetCourses',
            "columnDefs": [
                {
                    "render": function (data, type, row) {
                        return privates.createRow(data, type, row);
                    },
                    "targets": 5
                },
                {
                    "render": function (data, type, row) {
                        return privates.createLink(data, type, row);
                    },
                    "targets": 0
                }
            ]
        });
    };
    privates.datePickersInit = function () {
        jQuery('#StartDate').datetimepicker({
            timepicker: false
        });
        jQuery('#EndDate').datetimepicker({
            timepicker: false
        });
    };
    privates.tinyMCEInit = function () {
        tinymce.init({
            selector: '#courseDescription',
            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table contextmenu paste code'
            ],
            toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image'
        });
    };
    return {
        init: function (initTable, initDatePickers, initTinyMCE) {
            if (initTable === true) {
                privates.initTable();
            }
            if (initDatePickers === true) {
                privates.datePickersInit();
            }
            if (initTinyMCE === true) {
                privates.tinyMCEInit();
            }
        }
    }
})();