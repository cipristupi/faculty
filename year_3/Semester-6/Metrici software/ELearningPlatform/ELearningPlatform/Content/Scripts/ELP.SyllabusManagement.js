﻿var SyllabusManagement = (function () {
    var privates = {};
    privates.bind = function() {

    };
    privates.createRow = function(data, type, row) {
        var editLink = '<a href="/SyllabusManagement/SyllabusEdit/' + data + '">Edit</a>';
        var deleteLink = '<a href="/SyllabusManagement/SyllabusDelete/' + data + '">Delete</a>';
        var cell = '<p class="actionsCell">' + editLink + deleteLink + '</p>';
        return cell;
    };
    privates.initTable = function() {
        $('#syllabusTable').DataTable({
            "ajax": '/SyllabusManagement/GetSyllabus',
            "columnDefs": [
                {
                    "render": function (data, type, row) {
                        return privates.createRow(data, type, row);
                    },
                    "targets": 5
                },
                { "visible": true, "targets": [5] }
            ]
        });
    };
    return {
        init: function () {

        }
    }
})();