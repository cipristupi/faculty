﻿var AssignmentsManagement = (function () {
    var privates = {};
    privates.bind = function () {

    };
    privates.createRow = function (data, type, row) {
        var editLink = '<a href="/DepartmentsManagement/DepartmentsEdit/' + data + '">Edit</a>';
        var deleteLink = '<a href="/DepartmentsManagement/DepartmentsDelete/' + data + '">Delete</a>';
        var cell = '<p class="actionsCell">' + editLink + deleteLink + '</p>';
        return cell;
    };
    privates.initTable = function () {
        //$('#departmentsTable').DataTable({
        //    "ajax": '/DepartmentsManagement/GetDepatments',
        //    "columnDefs": [
        //        {
        //            "render": function (data, type, row) {
        //                return privates.createRow(data, type, row);
        //            },
        //            "targets": 2
        //        },
        //        { "visible": true, "targets": [2] }
        //    ]
        //});
    };
    return {
        init: function () {
            privates.initTable();
            $('#deadlineDate').datetimepicker();
        }
    }
})();