﻿var DepartmentsManagement = (function () {
    var privates = {};
    privates.bind = function () {

    };
    privates.createRow = function (data, type, row) {
        var dropDownId = 'dropdown' + data;
        var editLink = '<li><a href="/DepartmentsManagement/DepartmentsEdit/' + data + '">Edit</a></li>';
        var deleteLink = '<li><a href="/DepartmentsManagement/Delete/' + data + '">Delete</a></li>';
        var staffManagement = '<li><a href="/StaffToDepartamentsManagement/StaffToDepartamentsCreate?departmentId=' + data + '">Staff</a></li>';
        var cell = '<div class="dropdown"><button class="btn btn-default dropdown-toggle" type="button" id="'
        + dropDownId + '"aria-haspopup="true" aria-expanded="true" data-toggle="dropdown"> Actions <span class="caret"></span></button>'
        + '<ul class="dropdown-menu" aria-labelledby="' + dropDownId + '">' + editLink + deleteLink +staffManagement+ '</ul></div>';
        return cell;
    };
    privates.initTable = function () {
        $('#departmentsTable').DataTable({
            "ajax": '/DepartmentsManagement/GetDepatments',
            "columnDefs": [
                {
                    "render": function (data, type, row) {
                        return privates.createRow(data, type, row);
                    },
                    "targets": 2
                },
                { "visible": true, "targets": [2] }
            ]
        });
    };
    return {
        init: function () {
            privates.initTable();
        }
    }
})();