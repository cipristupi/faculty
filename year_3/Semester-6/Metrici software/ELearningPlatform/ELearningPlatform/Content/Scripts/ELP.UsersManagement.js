﻿if (ELearningPlatform.UsersManagement == undefined) {
    ELearningPlatform.UsersManagement = {};
}
ELearningPlatform.UsersManagement = function() {
    var self = this;
    self.init();
    self.bind();
}
ELearningPlatform.UsersManagement.prototype.init = function () {
    var self = this;
    $('#usersTable').DataTable({
        "ajax": '/UsersManagement/GetUsers',
        "columnDefs": [
            {
                "render": function (data, type, row) {
                    return self.createRow(data, type, row);
                },
                "targets": 5
            },
            { "visible": true, "targets": [5] }
        ]
    });
}

ELearningPlatform.UsersManagement.prototype.bind = function() {
    
}

ELearningPlatform.UsersManagement.prototype.createRow = function (data, type, row) {
    var editLink = '<li><a href="/UsersManagement/UserEdit/' + data + '">Edit</a></li>';
    var deleteLink = '<li><a href="/UsersManagement/Delete/' + data + '">Delete</a></li>';
    var dropDownId = 'dropdown' + data;
    var cell = '<div class="dropdown"><button class="btn btn-default dropdown-toggle" type="button" id="'
    + dropDownId + '"aria-haspopup="true" aria-expanded="true" data-toggle="dropdown"> Actions <span class="caret"></span></button>'
    + '<ul class="dropdown-menu" aria-labelledby="' + dropDownId + '">' + editLink + deleteLink + '</ul></div>';
    return cell;
}
