﻿var AnnouncementManagement = (function() {
    var privates = {};
    privates.bind = function() {
    };

    privates.tinyMCEInit = function(target) {
        tinymce.init({
            selector: '#announcementContent',
            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table contextmenu paste code'
            ],
            toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image'
        });

    };
    return {
        init: function(initTinyMce) {
            if (initTinyMce === true) {
                privates.tinyMCEInit();
            }
            $('#DateCreated').datetimepicker({
                timepicker: false
            });
            privates.bind();
        }
    }
})();