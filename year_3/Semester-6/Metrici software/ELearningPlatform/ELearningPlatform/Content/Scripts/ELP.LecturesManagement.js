﻿var LecturesManagement = (function () {
    var privates = {};
    privates.bind = function () {

    };
    privates.createRow = function (data, type, row) {
        var editLink = '<li><a href="/LecturesManagement/LecturesEdit/' + data + '">Edit</a></li>';
        var deleteLink = '<li><a href="/LecturesManagement/LecturessDelete/' + data + '">Delete</a></li>';
        var addPages = '<li><a href="/PagesManagement/PagesCreate/' + data + '" target="_blank">Add Page</a></li>';
        var pagesDetails = '<li><a href="/PagesManagement/PagesDetails/' + data + '" target="_blank">Pages</a></li>';

        var dropDownId = 'dropdown' + data;
        if (window.CurrentRole !== "Admin") {
            deleteLink = '';
            pagesDetails = '';
        }

        var dropDownItems = editLink + deleteLink + addPages + pagesDetails;
        var cell = '<div class="dropdown"><button class="btn btn-default dropdown-toggle" type="button" id="'
        + dropDownId + '"aria-haspopup="true" aria-expanded="true" data-toggle="dropdown"> Actions <span class="caret"></span></button>'
        + '<ul class="dropdown-menu" aria-labelledby="' + dropDownId + '">' + dropDownItems + '</ul></div>';
        return cell;
    };
    privates.createLink = function(data, type, row) {
        return '<a href="/LecturesManagement/LecturesDetails/' + row[4] + '">' + data + '</a>';
    };
    privates.initTable = function () {
        $('#lecturesTable').DataTable({
            "ajax": '/LecturesManagement/GetLectures',
            "columnDefs": [
                {
                    "render": function (data, type, row) {
                        return privates.createRow(data, type, row);
                    },
                    "targets": 4
                },
                {
                    "render": function (data, type, row) {
                        return privates.createLink(data, type, row);
                    },
                    "targets": 0
                }
            ]
        });
    };
    privates.datePickersInit = function () {
        jQuery('#StartDate').datetimepicker({
            timepicker: false
        });
        jQuery('#EndDate').datetimepicker({
            timepicker: false
        });
    };
    privates.tinyMCEInit = function () {
        tinymce.init({
            selector: '#lectureDescription',
             plugins: [
    'advlist autolink lists link image charmap print preview anchor',
    'searchreplace visualblocks code fullscreen',
    'insertdatetime media table contextmenu paste code'
            ],
            toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image'
        });
    };
    return {
        init: function (initTable, initDatePickers, initTinyMce) {
            if (initTable === true) {
                privates.initTable();
            }
            if (initDatePickers === true) {
                privates.datePickersInit();
            }
            if (initTinyMce === true) {
                privates.tinyMCEInit();
            }
        }
    }
})();