﻿var DiscussionsManagement = (function() {
    var privates = {};
    privates.bind = function() {
    };

    privates.tinyMCEInit = function(target) {
        tinymce.init({
            selector: '#commentContent',
            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table contextmenu paste code'
            ],
            toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image'
        });

    };
    return {
        init: function(initTinyMce, initTable) {
            if (initTinyMce === true) {
                privates.tinyMCEInit();
            }
            if (initTable === true) {
                privates.initTable();
            }
            privates.bind();
        }
    }
})();