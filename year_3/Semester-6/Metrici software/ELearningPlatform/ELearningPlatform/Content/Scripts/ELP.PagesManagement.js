﻿var PagesManagement = (function () {
    var privates = {};
    privates.bind = function () {
        var removeRowFunction = function () {
            jQuery(this).closest('.section').remove();
        };

        jQuery('#addSection').off('click').on('click', function() {
            var templateInitial = jQuery('.template').html();
            var template = jQuery(templateInitial).clone();
            var guid = Shared.createGuid();
            var textareaId = 'textarea' + guid;
            jQuery(template).find('.templateLink').attr('href', guid);
            jQuery(template).find('.templateTarget').attr('id', guid);
            jQuery(template).find('.templateLink').removeClass('templateLink');
            jQuery(template).find('.templateTarget').removeClass('templateTarget');
            jQuery(template).find('textarea').attr('id', textareaId);

            jQuery('.page-sections').append(template);
            privates.tinyMCEInit('#' + textareaId);
            window.tinymce.dom.Event.domLoaded = true;
            jQuery('.removeSection').off('click').on('click', removeRowFunction);
        });
       
        jQuery('.removeSection').off('click').on('click', removeRowFunction);


        jQuery('#savePage').off('click').on('click', function () {
            tinyMCE.triggerSave(false, true);

            var model = {};
            model.Name = jQuery('#Name').val();
            model.ParentId = jQuery('#ParentId').val();
            model.PageSections = [];
            //todo support for multimple tinymce editors
            var pageSection = {};
            pageSection.Name = jQuery('#sectionName').val();
            pageSection.SectionContent = jQuery('#sectionContent').val();
            model.PageSections.push(pageSection);


            var jsonModel = JSON.stringify(model);
            jQuery.ajax({
                type: "POST",
                url: "/PagesManagement/PagesCreate",
                data: jsonModel,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    console.log("success");
                }
            });
        });

        jQuery('#saveEditPage').off('click').on('click', function () {
            tinyMCE.triggerSave(false, true);

            var model = {};
            model.Name = jQuery('#Name').val();
            model.ParentId = jQuery('#ParentId').val();
            model.Id = jQuery('#Id').val();
            model.PageSections = [];
            //todo support for multimple tinymce editors
            var pageSection = {};
            pageSection.Name = jQuery('#sectionName').val();
            pageSection.SectionContent = jQuery('#sectionContent').val();
            model.PageSections.push(pageSection);


            var jsonModel = JSON.stringify(model);
            jQuery.ajax({
                type: "POST",
                url: "/PagesManagement/PagesEdit",
                data: jsonModel,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    console.log("success");
                }
            });
        });
    };

    privates.createRow = function (data, type, row) {
        var editLink = '<li><a href="/PagesManagement/PagesEdit/' + data + '">Edit</a></li>';
        var deleteLink = '<li><a href="/PagesManagement/PagesDelete/' + data + '">Delete</a></li>';
        var dropDownId = 'dropdown' + data;
        if (window.CurrentRole !== "Admin") {
            deleteLink = '';
        }
        var cell = '<div class="dropdown"><button class="btn btn-default dropdown-toggle" type="button" id="'
        + dropDownId + '"aria-haspopup="true" aria-expanded="true" data-toggle="dropdown"> Actions <span class="caret"></span></button>'
        + '<ul class="dropdown-menu" aria-labelledby="' + dropDownId + '">' + editLink + deleteLink + '</ul></div>';
        return cell;
    };
    privates.initTable = function () {
        $('#pagesTable').DataTable({
            "ajax": '/PagesManagement/GetPages',
            "columnDefs": [
                {
                    "render": function (data, type, row) {
                        return privates.createRow(data, type, row);
                    },
                    "targets": 2
                },
                { "visible": true, "targets": [2] }
            ]
        });
    };
    privates.tinyMCEInit = function (target) {
        tinymce.init({
            selector: '#sectionContent',
            plugins: [
    'advlist autolink lists link image charmap print preview anchor',
    'searchreplace visualblocks code fullscreen',
    'insertdatetime media table contextmenu paste code'
            ],
            toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image'
        });
        
    };
    return {
        init: function (initTinyMce, initTable) {
            if (initTinyMce === true) {
                privates.tinyMCEInit();
            }
            if (initTable === true) {
                privates.initTable();
            }
            privates.bind();
        }
    }
})();