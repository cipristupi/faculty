﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ELearningPlatform.Startup))]
namespace ELearningPlatform
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
