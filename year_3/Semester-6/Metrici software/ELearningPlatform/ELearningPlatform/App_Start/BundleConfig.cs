﻿using System.Web.Optimization;

namespace ELearningPlatform
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            #region Shared

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Content/Scripts/Shared/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                "~/Content/Scripts/Shared/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                "~/Content/Scripts/Shared/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                "~/Content/Scripts/Shared/bootstrap.js",
                "~/Content/Scripts/Shared/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/Styles/Shared/bootstrap.css"
                ));

            bundles.Add(new StyleBundle("~/LayoutStyles").Include(
                "~/Content/Styles/ELearningPlatform.Layout.css",
                "~/Content/Theme/bower_components/metisMenu/dist/metisMenu.min.css",
                "~/Content/Theme/bower_components/font-awesome/css/font-awesome.min.css",
                "~/Content/Theme/dist/css/timeline.css",
                "~/Content/Theme/bower_components/morrisjs/morris.css",
                "~/Content/Theme/bower_components/datatables/media/css/jquery.dataTables.css",
                "~/Content/Theme/bower_components/datatables/media/css/jquery.dataTables_themeoller.css",
                "~/Content/Styles/DateTimePicker/jquery.datetimepicker.css",
                "~/Content/Scripts/jquery-ui-1.11.4/jquery-ui.css"
                ));

            bundles.Add(new ScriptBundle("~/LayoutScripts").Include(
                "~/Content/Theme/bower_components/metisMenu/dist/metisMenu.min.js",
                "~/Content/Theme/dist/js/sb-admin-2.js",
                "~/Content/Theme/bower_components/morrisjs/morris.min.js",
                "~/Content/Theme/bower_components/raphael/raphael-min.js",
                "~/Content/Theme/bower_components/datatables/media/js/jquery.dataTables.js",
                "~/Content/Scripts/DateTimePicker/jquery.datetimepicker.js",
                "~/Content/Styles/tinymce/js/tinymce/tinymce.min.js",
                "~/Content/Scripts/ELP.Shared.js",
                "~/Content/Scripts/jquery-ui-1.11.4/jquery-ui.js"
                ));

            #endregion

            #region User Management

            bundles.Add(new ScriptBundle("~/UsersManagementScripts").Include(
                "~/Content/Scripts/ELP.UsersManagement.js"
                ));

            bundles.Add(new StyleBundle("~/UsersManagementStyles").Include(
                "~/Content/Styles/ELearningPlatform.UsersManagement.css"
                ));

            #endregion

            #region Departments Management

            bundles.Add(new ScriptBundle("~/DepartmentsManagementScripts").Include(
                "~/Content/Scripts/ELP.DepartmentsManagement.js"
                ));

            bundles.Add(new StyleBundle("~/DepartmentsManagementStyles").Include(
                "~/Content/Styles/ELearningPlatform.DepartmentsManagement.css"
                ));

            #endregion

            #region Assignments Management

            bundles.Add(new ScriptBundle("~/AssignmentsManagementScripts").Include(
                "~/Content/Scripts/ELP.AssignmentsManagement.js"
                ));

            bundles.Add(new StyleBundle("~/AssignmentsManagementStyles").Include(
                "~/Content/Styles/ELearningPlatform.AssignmentsManagement.css"
                ));

            #endregion

            #region Courses Management

            bundles.Add(new ScriptBundle("~/CoursesManagementScripts").Include(
                "~/Content/Scripts/ELP.Courses.js"
                ));

            bundles.Add(new StyleBundle("~/CoursesManagementStyles").Include(
                "~/Content/Styles/ELearningPlatform.CoursesManagement.css"
                ));

            #endregion

            #region Lecture Management

            bundles.Add(new ScriptBundle("~/LecturesManagementScripts").Include(
                "~/Content/Scripts/ELP.LecturesManagement.js"
                ));

            bundles.Add(new StyleBundle("~/LecturesManagementStyles").Include(
                "~/Content/Styles/ELearningPlatform.CoursesManagement.css"
                ));

            #endregion

            #region Page Management

            bundles.Add(new ScriptBundle("~/PagesManagementScripts").Include(
                "~/Content/Scripts/ELP.PagesManagement.js"
                ));

            bundles.Add(new StyleBundle("~/PagesManagementStyles").Include(
                "~/Content/Styles/ELearningPlatform.PagesManagement.css"
                ));

            #endregion

            #region Discussions Management

            bundles.Add(new ScriptBundle("~/DiscussionsManagementScripts").Include(
                "~/Content/Scripts/ELP.DiscussionsManagement.js"
                ));

            bundles.Add(new StyleBundle("~/DiscussionsManagementStyles").Include(
                "~/Content/Styles/ELearningPlatform.DiscussionsManagement.css"
                ));

            #endregion
        }
    }
}
