﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HospitalPharmacy.Models;
using PharmacyServiceN;

namespace HospitalPharmacyClient
{
    public partial class DoctorForm : Form
    {
        private IPharmacyService _pharmacyService;
        private User loggedUser;
        public DoctorForm()
        {
            InitializeComponent();
        }

        public DoctorForm(User user)
        {
            loggedUser = user;
            InitializeComponent();
            MyActiveForm.CurrentForm = this;
        }

        private void DoctorForm_Load(object sender, EventArgs e)
        {
            MyActiveForm.CurrentForm = this;
            CreateConnetionToService();
            createOrder_status_cmbbox.DataSource = Enum.GetValues(typeof(OrderStatus));
            updateOrder_status_cmbbox.DataSource = Enum.GetValues(typeof(OrderStatus));
        }
        public void Notify(object message)
        {
            MessageBox.Show("Notification");


            if (message.ToString() == "UpdateOrder")
            {
                CreateConnetionToService();
                orders_dataGridView.DataSource = null;
                orders_dataGridView.DataSource = _pharmacyService.GetAllOrders();
            }

            if (message.ToString() == "UpdateMedicine")
            {
                CreateConnetionToService();
                medicines_dataGridView.DataSource = null;
                medicines_dataGridView.DataSource = _pharmacyService.GetAllMedicines();
            }
        }

        public void NotifyServiceCallback_ClientNotified(object sender, ClientNotifiedEventArgs e)
        {
            MessageBox.Show("Notification");
        }

     
        private void CreateConnetionToService()
        {
            ChannelFactory<IPharmacyService> factory = null;
            try
            {
                NotifyServiceCallback notifyServiceCallback = new NotifyServiceCallback();
                notifyServiceCallback.ClientNotified += NotifyServiceCallback_ClientNotified;
                InstanceContext site = new InstanceContext(notifyServiceCallback);
                var binding = new NetTcpBinding(); //unsecureNetTcpBinding
                EndpointAddress address = new EndpointAddress("net.tcp://localhost:1235/PharmacyService");
                DuplexChannelFactory<IPharmacyService> cf = new DuplexChannelFactory<IPharmacyService>(site, binding);
                _pharmacyService = cf.CreateChannel(address);
            }
            catch (CommunicationException communicationException)
            {
                MessageBox.Show(communicationException.ToString());
                if (factory != null)
                {
                    factory.Abort();
                }
            }
            catch (TimeoutException timeoutException)
            {
                MessageBox.Show(timeoutException.ToString());
                if (factory != null)
                {
                    factory.Abort();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                if (factory != null)
                {
                    factory.Abort();
                }
            }
        }

        #region Users






        #endregion

        #region Medicine

        private void loadMedicines_bttn_Click(object sender, EventArgs e)
        {
            medicines_dataGridView.DataSource = null;
            medicines_dataGridView.DataSource = _pharmacyService.GetAllMedicines();
        }


        #endregion

        #region Orders
        private void createOrder_bttn_Click(object sender, EventArgs e)
        {
            string medicineId = createOrder_medicineId_txtbox.Text;
            string medicineName = createOrder_medicineName_txtbox.Text;
            string userName = createOrder_userName_txtbox.Text;
            string quantity = createOrder_quantity_txtbox.Text;
            OrderStatus orderStatus;
            Enum.TryParse<OrderStatus>(createOrder_status_cmbbox.SelectedValue.ToString(), out orderStatus);
            if (string.IsNullOrEmpty(medicineId) || string.IsNullOrEmpty(medicineName) || string.IsNullOrEmpty(userName) ||
                string.IsNullOrEmpty(quantity))
            {
                MessageBox.Show("Please fill all fields.");
                return;
            }
            else
            {
                var user = _pharmacyService.GetAllUsers().FirstOrDefault(x => x.Username == userName);
                if (user == null)
                {
                    MessageBox.Show("User doesnt exists");
                    return;
                }
                Order newOrder = new Order()
                {
                    MedicineId = int.Parse(medicineId),
                    MedicineName = medicineName,
                    Quantity = int.Parse(quantity),
                    Status = orderStatus,
                    UserId = user.Id
                };

                try
                {
                    var order = _pharmacyService.AddOrder(newOrder);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }

        private void updateOrder_bttn_Click(object sender, EventArgs e)
        {
            string medicineId = updateOrder_medicineId_txtbox.Text;
            string medicineName = updateOrder_medicineName_txtbox.Text;
            string userName = updateOrder_userName_txtbox.Text;
            string quantity = updateOrder_quantity_txtbox.Text;
            OrderStatus orderStatus;
            Enum.TryParse(updateOrder_status_cmbbox.SelectedValue.ToString(), out orderStatus);
            if (string.IsNullOrEmpty(medicineId) || string.IsNullOrEmpty(medicineName) || string.IsNullOrEmpty(userName) ||
                string.IsNullOrEmpty(quantity))
            {
                MessageBox.Show("Please fill all fields.");
                return;
            }
            else
            {
                var user = _pharmacyService.GetAllUsers().FirstOrDefault(x => x.Username == userName);
                if (user == null)
                {
                    MessageBox.Show("User doesnt exists");
                    return;
                }
                Order newOrder = new Order()
                {
                    MedicineId = int.Parse(medicineId),
                    MedicineName = medicineName,
                    Quantity = int.Parse(quantity),
                    Status = orderStatus,
                    UserId = user.Id
                };

                try
                {
                    var order = _pharmacyService.UpdateOrder(newOrder);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }

        private void LoadOrderForUpdate(int orderId)
        {
            //CreateConnetionToService();
            try
            {
                var order = _pharmacyService.GetOrderById(orderId);
                if (order != null)
                {
                    updateOrder_medicineId_txtbox.Text = order.MedicineId.ToString();
                    updateOrder_medicineName_txtbox.Text = order.MedicineName;
                    updateOrder_quantity_txtbox.Text = order.Quantity.ToString();
                    updateOrder_status_cmbbox.SelectedItem = order.Status;
                    var user = _pharmacyService.GetUserById(order.UserId);
                    updateOrder_userName_txtbox.Text = user.Username.ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void loadOrders_bttn_Click(object sender, EventArgs e)
        {
            orders_dataGridView.DataSource = null;
            orders_dataGridView.DataSource = _pharmacyService.GetAllOrders();
        }

        private void orders_dataGridView_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                var selectedRow = orders_dataGridView.SelectedRows;
                int orderId = int.Parse(selectedRow[0].Cells[0].Value.ToString());
                LoadOrderForUpdate(orderId);
            }
            catch (Exception)
            {

            }
        }
        #endregion
    }
}
