﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HospitalPharmacy.Models;
using PharmacyServiceN;

namespace HospitalPharmacyClient
{
    public partial class LoginForm : Form
    {
        private IPharmacyService _pharmacyDataService;

        public LoginForm()
        {
            InitializeComponent();
        }

        private void login_bttn_Click(object sender, EventArgs e)
        {
            CreateConnetionToService();
            string username = username_txtbox.Text;
            string password = password_txtbox.Text;
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
            {
                MessageBox.Show("Please fill all fields");
            }
            else
            {
                User user = new User()
                {
                    Password = password,
                    Username = username
                };
                var serviceUser = _pharmacyDataService.Login(user);
                if (serviceUser == null)
                {
                    MessageBox.Show("Invalid user or password");
                    return;
                }
                switch (serviceUser.UserType)
                {
                    case UserType.Admin:
                        AdminForm adminForm = new AdminForm(serviceUser);
                        this.Hide();
                        adminForm.Show();
                        break;
                    case UserType.Doctor:
                        DoctorForm doctorForm = new DoctorForm(serviceUser);
                        this.Hide();
                        doctorForm.Show();
                        break;
                    case UserType.Pharmacist:
                        PharmacistForm pharmacistForm = new PharmacistForm(serviceUser);
                        this.Hide();
                        pharmacistForm.Show();
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }


        private void CreateConnetionToService()
        {
            ChannelFactory<IPharmacyService> factory = null;
            try
            {
                NotifyServiceCallback notifyServiceCallback = new NotifyServiceCallback();
                InstanceContext site = new InstanceContext(notifyServiceCallback);
                var binding = new NetTcpBinding(); //unsecureNetTcpBinding
                EndpointAddress address = new EndpointAddress("net.tcp://localhost:1235/PharmacyService");
                DuplexChannelFactory<IPharmacyService> cf = new DuplexChannelFactory<IPharmacyService>(site, binding);
                _pharmacyDataService = cf.CreateChannel(address);
            }
            catch (CommunicationException communicationException)
            {
                MessageBox.Show(communicationException.ToString());
                if (factory != null)
                {
                    factory.Abort();
                }
            }
            catch (TimeoutException timeoutException)
            {
                MessageBox.Show(timeoutException.ToString());
                if (factory != null)
                {
                    factory.Abort();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                if (factory != null)
                {
                    factory.Abort();
                }
            }
        }
    }

}