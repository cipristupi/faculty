﻿namespace HospitalPharmacyClient
{
    partial class PharmacistForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.loadMedicines_bttn = new System.Windows.Forms.Button();
            this.medicines_dataGridView = new System.Windows.Forms.DataGridView();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.updateMedicine_id_txtbox = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.updateMedicine_bttn = new System.Windows.Forms.Button();
            this.updateMedicine_name_txtbox = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.createMedicine_bttn = new System.Windows.Forms.Button();
            this.createMedicine_name_txtbox = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.updateOrder_quantity_txtbox = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.updateOrder_bttn = new System.Windows.Forms.Button();
            this.updateOrder_status_cmbbox = new System.Windows.Forms.ComboBox();
            this.updateOrder_userName_txtbox = new System.Windows.Forms.TextBox();
            this.updateOrder_medicineName_txtbox = new System.Windows.Forms.TextBox();
            this.updateOrder_medicineId_txtbox = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.loadOrders_bttn = new System.Windows.Forms.Button();
            this.orders_dataGridView = new System.Windows.Forms.DataGridView();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.createOrder_quantity_txtbox = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.createOrder_bttn = new System.Windows.Forms.Button();
            this.createOrder_status_cmbbox = new System.Windows.Forms.ComboBox();
            this.createOrder_userName_txtbox = new System.Windows.Forms.TextBox();
            this.createOrder_medicineName_txtbox = new System.Windows.Forms.TextBox();
            this.createOrder_medicineId_txtbox = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.medicines_dataGridView)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.orders_dataGridView)).BeginInit();
            this.groupBox9.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1005, 545);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox4);
            this.tabPage2.Controls.Add(this.groupBox5);
            this.tabPage2.Controls.Add(this.groupBox6);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(997, 519);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Medicines";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.loadMedicines_bttn);
            this.groupBox4.Controls.Add(this.medicines_dataGridView);
            this.groupBox4.Location = new System.Drawing.Point(275, 19);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(699, 495);
            this.groupBox4.TabIndex = 5;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Medicines";
            // 
            // loadMedicines_bttn
            // 
            this.loadMedicines_bttn.Location = new System.Drawing.Point(15, 443);
            this.loadMedicines_bttn.Name = "loadMedicines_bttn";
            this.loadMedicines_bttn.Size = new System.Drawing.Size(138, 39);
            this.loadMedicines_bttn.TabIndex = 1;
            this.loadMedicines_bttn.Text = "Load Users";
            this.loadMedicines_bttn.UseVisualStyleBackColor = true;
            this.loadMedicines_bttn.Click += new System.EventHandler(this.loadMedicines_bttn_Click);
            // 
            // medicines_dataGridView
            // 
            this.medicines_dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.medicines_dataGridView.Location = new System.Drawing.Point(15, 19);
            this.medicines_dataGridView.Name = "medicines_dataGridView";
            this.medicines_dataGridView.ReadOnly = true;
            this.medicines_dataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.medicines_dataGridView.Size = new System.Drawing.Size(678, 407);
            this.medicines_dataGridView.TabIndex = 0;
            this.medicines_dataGridView.SelectionChanged += new System.EventHandler(this.medicines_dataGridView_SelectionChanged);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.updateMedicine_id_txtbox);
            this.groupBox5.Controls.Add(this.label10);
            this.groupBox5.Controls.Add(this.updateMedicine_bttn);
            this.groupBox5.Controls.Add(this.updateMedicine_name_txtbox);
            this.groupBox5.Controls.Add(this.label14);
            this.groupBox5.Location = new System.Drawing.Point(8, 175);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(209, 135);
            this.groupBox5.TabIndex = 4;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Update Medicine";
            // 
            // updateMedicine_id_txtbox
            // 
            this.updateMedicine_id_txtbox.Location = new System.Drawing.Point(91, 25);
            this.updateMedicine_id_txtbox.Name = "updateMedicine_id_txtbox";
            this.updateMedicine_id_txtbox.ReadOnly = true;
            this.updateMedicine_id_txtbox.Size = new System.Drawing.Size(100, 20);
            this.updateMedicine_id_txtbox.TabIndex = 19;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(7, 25);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(62, 13);
            this.label10.TabIndex = 18;
            this.label10.Text = "Medicine Id";
            // 
            // updateMedicine_bttn
            // 
            this.updateMedicine_bttn.Location = new System.Drawing.Point(6, 79);
            this.updateMedicine_bttn.Name = "updateMedicine_bttn";
            this.updateMedicine_bttn.Size = new System.Drawing.Size(121, 37);
            this.updateMedicine_bttn.TabIndex = 17;
            this.updateMedicine_bttn.Text = "Update";
            this.updateMedicine_bttn.UseVisualStyleBackColor = true;
            this.updateMedicine_bttn.Click += new System.EventHandler(this.updateMedicine_bttn_Click);
            // 
            // updateMedicine_name_txtbox
            // 
            this.updateMedicine_name_txtbox.Location = new System.Drawing.Point(91, 53);
            this.updateMedicine_name_txtbox.Name = "updateMedicine_name_txtbox";
            this.updateMedicine_name_txtbox.Size = new System.Drawing.Size(100, 20);
            this.updateMedicine_name_txtbox.TabIndex = 13;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(7, 53);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(35, 13);
            this.label14.TabIndex = 9;
            this.label14.Text = "Name";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.createMedicine_bttn);
            this.groupBox6.Controls.Add(this.createMedicine_name_txtbox);
            this.groupBox6.Controls.Add(this.label17);
            this.groupBox6.Location = new System.Drawing.Point(8, 19);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(191, 131);
            this.groupBox6.TabIndex = 3;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Create Medicine";
            // 
            // createMedicine_bttn
            // 
            this.createMedicine_bttn.Location = new System.Drawing.Point(10, 69);
            this.createMedicine_bttn.Name = "createMedicine_bttn";
            this.createMedicine_bttn.Size = new System.Drawing.Size(121, 37);
            this.createMedicine_bttn.TabIndex = 18;
            this.createMedicine_bttn.Text = "Create";
            this.createMedicine_bttn.UseVisualStyleBackColor = true;
            this.createMedicine_bttn.Click += new System.EventHandler(this.createMedicine_bttn_Click);
            // 
            // createMedicine_name_txtbox
            // 
            this.createMedicine_name_txtbox.Location = new System.Drawing.Point(65, 25);
            this.createMedicine_name_txtbox.Name = "createMedicine_name_txtbox";
            this.createMedicine_name_txtbox.Size = new System.Drawing.Size(100, 20);
            this.createMedicine_name_txtbox.TabIndex = 6;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(7, 28);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(35, 13);
            this.label17.TabIndex = 1;
            this.label17.Text = "Name";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.groupBox8);
            this.tabPage3.Controls.Add(this.groupBox7);
            this.tabPage3.Controls.Add(this.groupBox9);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1072, 519);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Orders";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.updateOrder_quantity_txtbox);
            this.groupBox8.Controls.Add(this.label11);
            this.groupBox8.Controls.Add(this.updateOrder_bttn);
            this.groupBox8.Controls.Add(this.updateOrder_status_cmbbox);
            this.groupBox8.Controls.Add(this.updateOrder_userName_txtbox);
            this.groupBox8.Controls.Add(this.updateOrder_medicineName_txtbox);
            this.groupBox8.Controls.Add(this.updateOrder_medicineId_txtbox);
            this.groupBox8.Controls.Add(this.label12);
            this.groupBox8.Controls.Add(this.label13);
            this.groupBox8.Controls.Add(this.label15);
            this.groupBox8.Controls.Add(this.label16);
            this.groupBox8.Location = new System.Drawing.Point(8, 292);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(249, 258);
            this.groupBox8.TabIndex = 4;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Update Order";
            // 
            // updateOrder_quantity_txtbox
            // 
            this.updateOrder_quantity_txtbox.Location = new System.Drawing.Point(100, 125);
            this.updateOrder_quantity_txtbox.Name = "updateOrder_quantity_txtbox";
            this.updateOrder_quantity_txtbox.Size = new System.Drawing.Size(100, 20);
            this.updateOrder_quantity_txtbox.TabIndex = 20;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(11, 128);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(46, 13);
            this.label11.TabIndex = 19;
            this.label11.Text = "Quantity";
            // 
            // updateOrder_bttn
            // 
            this.updateOrder_bttn.Location = new System.Drawing.Point(64, 203);
            this.updateOrder_bttn.Name = "updateOrder_bttn";
            this.updateOrder_bttn.Size = new System.Drawing.Size(121, 37);
            this.updateOrder_bttn.TabIndex = 18;
            this.updateOrder_bttn.Text = "Update";
            this.updateOrder_bttn.UseVisualStyleBackColor = true;
            this.updateOrder_bttn.Click += new System.EventHandler(this.updateOrder_bttn_Click);
            // 
            // updateOrder_status_cmbbox
            // 
            this.updateOrder_status_cmbbox.FormattingEnabled = true;
            this.updateOrder_status_cmbbox.Location = new System.Drawing.Point(100, 157);
            this.updateOrder_status_cmbbox.Name = "updateOrder_status_cmbbox";
            this.updateOrder_status_cmbbox.Size = new System.Drawing.Size(121, 21);
            this.updateOrder_status_cmbbox.TabIndex = 8;
            // 
            // updateOrder_userName_txtbox
            // 
            this.updateOrder_userName_txtbox.Location = new System.Drawing.Point(100, 92);
            this.updateOrder_userName_txtbox.Name = "updateOrder_userName_txtbox";
            this.updateOrder_userName_txtbox.Size = new System.Drawing.Size(100, 20);
            this.updateOrder_userName_txtbox.TabIndex = 7;
            // 
            // updateOrder_medicineName_txtbox
            // 
            this.updateOrder_medicineName_txtbox.Location = new System.Drawing.Point(100, 59);
            this.updateOrder_medicineName_txtbox.Name = "updateOrder_medicineName_txtbox";
            this.updateOrder_medicineName_txtbox.Size = new System.Drawing.Size(100, 20);
            this.updateOrder_medicineName_txtbox.TabIndex = 6;
            // 
            // updateOrder_medicineId_txtbox
            // 
            this.updateOrder_medicineId_txtbox.Location = new System.Drawing.Point(100, 26);
            this.updateOrder_medicineId_txtbox.Name = "updateOrder_medicineId_txtbox";
            this.updateOrder_medicineId_txtbox.Size = new System.Drawing.Size(100, 20);
            this.updateOrder_medicineId_txtbox.TabIndex = 5;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(11, 160);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(37, 13);
            this.label12.TabIndex = 4;
            this.label12.Text = "Status";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(11, 95);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(60, 13);
            this.label13.TabIndex = 2;
            this.label13.Text = "User Name";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(11, 62);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(81, 13);
            this.label15.TabIndex = 1;
            this.label15.Text = "Medicine Name";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(11, 29);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(62, 13);
            this.label16.TabIndex = 0;
            this.label16.Text = "Medicine Id";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.loadOrders_bttn);
            this.groupBox7.Controls.Add(this.orders_dataGridView);
            this.groupBox7.Location = new System.Drawing.Point(275, 16);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(699, 495);
            this.groupBox7.TabIndex = 5;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Orders";
            // 
            // loadOrders_bttn
            // 
            this.loadOrders_bttn.Location = new System.Drawing.Point(15, 443);
            this.loadOrders_bttn.Name = "loadOrders_bttn";
            this.loadOrders_bttn.Size = new System.Drawing.Size(138, 39);
            this.loadOrders_bttn.TabIndex = 1;
            this.loadOrders_bttn.Text = "Load Orders";
            this.loadOrders_bttn.UseVisualStyleBackColor = true;
            this.loadOrders_bttn.Click += new System.EventHandler(this.loadOrders_bttn_Click);
            // 
            // orders_dataGridView
            // 
            this.orders_dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.orders_dataGridView.Location = new System.Drawing.Point(15, 19);
            this.orders_dataGridView.Name = "orders_dataGridView";
            this.orders_dataGridView.ReadOnly = true;
            this.orders_dataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.orders_dataGridView.Size = new System.Drawing.Size(678, 407);
            this.orders_dataGridView.TabIndex = 0;
            this.orders_dataGridView.SelectionChanged += new System.EventHandler(this.orders_dataGridView_SelectionChanged);
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.createOrder_quantity_txtbox);
            this.groupBox9.Controls.Add(this.label22);
            this.groupBox9.Controls.Add(this.createOrder_bttn);
            this.groupBox9.Controls.Add(this.createOrder_status_cmbbox);
            this.groupBox9.Controls.Add(this.createOrder_userName_txtbox);
            this.groupBox9.Controls.Add(this.createOrder_medicineName_txtbox);
            this.groupBox9.Controls.Add(this.createOrder_medicineId_txtbox);
            this.groupBox9.Controls.Add(this.label18);
            this.groupBox9.Controls.Add(this.label19);
            this.groupBox9.Controls.Add(this.label20);
            this.groupBox9.Controls.Add(this.label21);
            this.groupBox9.Location = new System.Drawing.Point(8, 16);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(249, 258);
            this.groupBox9.TabIndex = 3;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Create Order";
            // 
            // createOrder_quantity_txtbox
            // 
            this.createOrder_quantity_txtbox.Location = new System.Drawing.Point(100, 125);
            this.createOrder_quantity_txtbox.Name = "createOrder_quantity_txtbox";
            this.createOrder_quantity_txtbox.Size = new System.Drawing.Size(100, 20);
            this.createOrder_quantity_txtbox.TabIndex = 20;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(11, 128);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(46, 13);
            this.label22.TabIndex = 19;
            this.label22.Text = "Quantity";
            // 
            // createOrder_bttn
            // 
            this.createOrder_bttn.Location = new System.Drawing.Point(64, 198);
            this.createOrder_bttn.Name = "createOrder_bttn";
            this.createOrder_bttn.Size = new System.Drawing.Size(121, 37);
            this.createOrder_bttn.TabIndex = 18;
            this.createOrder_bttn.Text = "Create";
            this.createOrder_bttn.UseVisualStyleBackColor = true;
            this.createOrder_bttn.Click += new System.EventHandler(this.createOrder_bttn_Click);
            // 
            // createOrder_status_cmbbox
            // 
            this.createOrder_status_cmbbox.FormattingEnabled = true;
            this.createOrder_status_cmbbox.Location = new System.Drawing.Point(100, 157);
            this.createOrder_status_cmbbox.Name = "createOrder_status_cmbbox";
            this.createOrder_status_cmbbox.Size = new System.Drawing.Size(121, 21);
            this.createOrder_status_cmbbox.TabIndex = 8;
            // 
            // createOrder_userName_txtbox
            // 
            this.createOrder_userName_txtbox.Location = new System.Drawing.Point(100, 92);
            this.createOrder_userName_txtbox.Name = "createOrder_userName_txtbox";
            this.createOrder_userName_txtbox.Size = new System.Drawing.Size(100, 20);
            this.createOrder_userName_txtbox.TabIndex = 7;
            // 
            // createOrder_medicineName_txtbox
            // 
            this.createOrder_medicineName_txtbox.Location = new System.Drawing.Point(100, 59);
            this.createOrder_medicineName_txtbox.Name = "createOrder_medicineName_txtbox";
            this.createOrder_medicineName_txtbox.Size = new System.Drawing.Size(100, 20);
            this.createOrder_medicineName_txtbox.TabIndex = 6;
            // 
            // createOrder_medicineId_txtbox
            // 
            this.createOrder_medicineId_txtbox.Location = new System.Drawing.Point(100, 26);
            this.createOrder_medicineId_txtbox.Name = "createOrder_medicineId_txtbox";
            this.createOrder_medicineId_txtbox.Size = new System.Drawing.Size(100, 20);
            this.createOrder_medicineId_txtbox.TabIndex = 5;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(11, 160);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(37, 13);
            this.label18.TabIndex = 4;
            this.label18.Text = "Status";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(11, 95);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(60, 13);
            this.label19.TabIndex = 2;
            this.label19.Text = "User Name";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(11, 62);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(81, 13);
            this.label20.TabIndex = 1;
            this.label20.Text = "Medicine Name";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(11, 29);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(62, 13);
            this.label21.TabIndex = 0;
            this.label21.Text = "Medicine Id";
            // 
            // PharmacistForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1005, 545);
            this.Controls.Add(this.tabControl1);
            this.Name = "PharmacistForm";
            this.Text = "PharmacistForm";
            this.Load += new System.EventHandler(this.PharmacistForm_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.medicines_dataGridView)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.orders_dataGridView)).EndInit();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button loadMedicines_bttn;
        private System.Windows.Forms.DataGridView medicines_dataGridView;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox updateMedicine_id_txtbox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button updateMedicine_bttn;
        private System.Windows.Forms.TextBox updateMedicine_name_txtbox;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button createMedicine_bttn;
        private System.Windows.Forms.TextBox createMedicine_name_txtbox;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.TextBox updateOrder_quantity_txtbox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button updateOrder_bttn;
        private System.Windows.Forms.ComboBox updateOrder_status_cmbbox;
        private System.Windows.Forms.TextBox updateOrder_userName_txtbox;
        private System.Windows.Forms.TextBox updateOrder_medicineName_txtbox;
        private System.Windows.Forms.TextBox updateOrder_medicineId_txtbox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Button loadOrders_bttn;
        private System.Windows.Forms.DataGridView orders_dataGridView;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.TextBox createOrder_quantity_txtbox;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Button createOrder_bttn;
        private System.Windows.Forms.ComboBox createOrder_status_cmbbox;
        private System.Windows.Forms.TextBox createOrder_userName_txtbox;
        private System.Windows.Forms.TextBox createOrder_medicineName_txtbox;
        private System.Windows.Forms.TextBox createOrder_medicineId_txtbox;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
    }
}