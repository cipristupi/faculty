﻿using System;
using System.Linq;
using System.ServiceModel;
using System.Windows.Forms;
using HospitalPharmacy.Models;
using PharmacyServiceN;

namespace HospitalPharmacyClient
{
    public partial class PharmacistForm : Form
    {
        private User loggedUser;
        private IPharmacyService _pharmacyService;

        public PharmacistForm()
        {
            InitializeComponent();
        }

        public PharmacistForm(User user)
        {
            loggedUser = user;
            CreateConnetionToService();
        }


        public void Notify(object message)
        {
            MessageBox.Show("Notification");
           
            if (message.ToString() == "UpdateOrder")
            {
                CreateConnetionToService();
                orders_dataGridView.DataSource = null;
                orders_dataGridView.DataSource = _pharmacyService.GetAllOrders();
            }

            if (message.ToString() == "UpdateMedicine")
            {
                CreateConnetionToService();
                medicines_dataGridView.DataSource = null;
                medicines_dataGridView.DataSource = _pharmacyService.GetAllMedicines();
            }
        }
        private void CreateConnetionToService()
        {
            ChannelFactory<IPharmacyService> factory = null;
            try
            {
                NotifyServiceCallback notifyServiceCallback = new NotifyServiceCallback();
                InstanceContext site = new InstanceContext(notifyServiceCallback);
                var binding = new NetTcpBinding(); //unsecureNetTcpBinding
                EndpointAddress address = new EndpointAddress("net.tcp://localhost:1235/PharmacyService");
                DuplexChannelFactory<IPharmacyService> cf = new DuplexChannelFactory<IPharmacyService>(site, binding);
                _pharmacyService = cf.CreateChannel(address);
            }
            catch (CommunicationException communicationException)
            {
                MessageBox.Show(communicationException.ToString());
                if (factory != null)
                {
                    factory.Abort();
                }
            }
            catch (TimeoutException timeoutException)
            {
                MessageBox.Show(timeoutException.ToString());
                if (factory != null)
                {
                    factory.Abort();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                if (factory != null)
                {
                    factory.Abort();
                }
            }
        }

        #region Medicine

        private void loadMedicines_bttn_Click(object sender, EventArgs e)
        {
            medicines_dataGridView.DataSource = null;
            medicines_dataGridView.DataSource = _pharmacyService.GetAllMedicines();
        }

        private void createMedicine_bttn_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(createMedicine_name_txtbox.Text))
            {
                MessageBox.Show("Please fill all fields.");
            }
            else
            {
                Medicine newMedicine = new Medicine()
                {
                    Name = createMedicine_name_txtbox.Text
                };
                try
                {
                    var medicine = _pharmacyService.AddMedicine(newMedicine);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }

        private void updateMedicine_bttn_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(createMedicine_name_txtbox.Text))
            {
                MessageBox.Show("Please fill all fields.");
            }
            else
            {
                Medicine medicine = new Medicine()
                {
                    Id = int.Parse(updateMedicine_id_txtbox.Text),
                    Name = updateMedicine_name_txtbox.Text
                };
                try
                {
                    var updatedMedicine = _pharmacyService.AddMedicine(medicine);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }

        private void LoadMedicineForUpdate(int medicineId)
        {
            //CreateConnetionToService();
            try
            {
                var medicine = _pharmacyService.GetMedicineById(medicineId);
                if (medicine != null)
                {
                    updateMedicine_name_txtbox.Text = medicine.Name;
                    updateMedicine_id_txtbox.Text = medicine.Id.ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void medicines_dataGridView_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                var selectedRow = medicines_dataGridView.SelectedRows;
                int medicineId = int.Parse(selectedRow[0].Cells[0].Value.ToString());
                LoadMedicineForUpdate(medicineId);
            }
            catch (Exception)
            {

            }
        }
        #endregion

        #region Orders
        private void createOrder_bttn_Click(object sender, EventArgs e)
        {
            string medicineId = createOrder_medicineId_txtbox.Text;
            string medicineName = createOrder_medicineName_txtbox.Text;
            string userName = createOrder_userName_txtbox.Text;
            string quantity = createOrder_quantity_txtbox.Text;
            OrderStatus orderStatus;
            Enum.TryParse<OrderStatus>(createOrder_status_cmbbox.SelectedValue.ToString(), out orderStatus);
            if (string.IsNullOrEmpty(medicineId) || string.IsNullOrEmpty(medicineName) || string.IsNullOrEmpty(userName) ||
                string.IsNullOrEmpty(quantity))
            {
                MessageBox.Show("Please fill all fields.");
                return;
            }
            else
            {
                var user = _pharmacyService.GetAllUsers().FirstOrDefault(x => x.Username == userName);
                if (user == null)
                {
                    MessageBox.Show("User doesnt exists");
                    return;
                }
                Order newOrder = new Order()
                {
                    MedicineId = int.Parse(medicineId),
                    MedicineName = medicineName,
                    Quantity = int.Parse(quantity),
                    Status = orderStatus,
                    UserId = user.Id
                };

                try
                {
                    var order = _pharmacyService.AddOrder(newOrder);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }

        private void updateOrder_bttn_Click(object sender, EventArgs e)
        {
            string medicineId = updateOrder_medicineId_txtbox.Text;
            string medicineName = updateOrder_medicineName_txtbox.Text;
            string userName = updateOrder_userName_txtbox.Text;
            string quantity = updateOrder_quantity_txtbox.Text;
            OrderStatus orderStatus;
            Enum.TryParse(updateOrder_status_cmbbox.SelectedValue.ToString(), out orderStatus);
            if (string.IsNullOrEmpty(medicineId) || string.IsNullOrEmpty(medicineName) || string.IsNullOrEmpty(userName) ||
                string.IsNullOrEmpty(quantity))
            {
                MessageBox.Show("Please fill all fields.");
                return;
            }
            else
            {
                var user = _pharmacyService.GetAllUsers().FirstOrDefault(x => x.Username == userName);
                if (user == null)
                {
                    MessageBox.Show("User doesnt exists");
                    return;
                }
                Order newOrder = new Order()
                {
                    MedicineId = int.Parse(medicineId),
                    MedicineName = medicineName,
                    Quantity = int.Parse(quantity),
                    Status = orderStatus,
                    UserId = user.Id
                };

                try
                {
                    var order = _pharmacyService.UpdateOrder(newOrder);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }

        private void LoadOrderForUpdate(int orderId)
        {
            //CreateConnetionToService();
            try
            {
                var order = _pharmacyService.GetOrderById(orderId);
                if (order != null)
                {
                    updateOrder_medicineId_txtbox.Text = order.MedicineId.ToString();
                    updateOrder_medicineName_txtbox.Text = order.MedicineName;
                    updateOrder_quantity_txtbox.Text = order.Quantity.ToString();
                    updateOrder_status_cmbbox.SelectedItem = order.Status;
                    var user = _pharmacyService.GetUserById(order.UserId);
                    updateOrder_userName_txtbox.Text = user.Username.ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void loadOrders_bttn_Click(object sender, EventArgs e)
        {
            orders_dataGridView.DataSource = null;
            orders_dataGridView.DataSource = _pharmacyService.GetAllOrders();
        }

        private void orders_dataGridView_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                var selectedRow = orders_dataGridView.SelectedRows;
                int orderId = int.Parse(selectedRow[0].Cells[0].Value.ToString());
                LoadOrderForUpdate(orderId);
            }
            catch (Exception)
            {

            }
        }
        #endregion

        private void PharmacistForm_Load(object sender, EventArgs e)
        {
            CreateConnetionToService();
            createOrder_status_cmbbox.DataSource = Enum.GetValues(typeof(OrderStatus));
            updateOrder_status_cmbbox.DataSource = Enum.GetValues(typeof(OrderStatus));
            MyActiveForm.CurrentForm = this;
        }
    }
}
