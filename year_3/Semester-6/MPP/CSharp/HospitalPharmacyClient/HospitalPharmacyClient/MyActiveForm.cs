﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HospitalPharmacyClient
{
    public static class MyActiveForm
    {
        public static Form CurrentForm { get; set; }
    }
}
