﻿using System.Collections.Generic;
using System.Data.SQLite;
using System.Runtime.Serialization;

namespace HospitalPharmacy.Models
{
    [DataContract]
    public class Medicine
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        public List<string> Validate()
        {
            List<string> errorMessage = new List<string>();
            if (string.IsNullOrEmpty(Name))
            {
                errorMessage.Add("Name is empty");
            }
            return errorMessage;
        }

        public static Medicine ReadDB(SQLiteDataReader reader)
        {
            Medicine medicine = new Medicine
            {
                Id = reader.GetInt32(0),
                Name = reader.GetString(1)
            };
            return medicine;
        }
    }
}
