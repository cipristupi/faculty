﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace HospitalPharmacy.Models
{
    [DataContract]
    public class User
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Username { get; set; }

        [DataMember]
        public string Password { get; set; }

        [DataMember]
        public string Section { get; set; }

        [DataMember]
        public UserType UserType { get; set; }

        [DataMember]
        public Guid? ClientGuid { get; set; }

        public List<string> Validate()
        {
            List<string> errorMessages = new List<string>();
            if (string.IsNullOrEmpty(Username))
            {
                errorMessages.Add("Username is empty");
            }
            if (string.IsNullOrEmpty(Password))
            {
                errorMessages.Add("Password is empty");
            }
            if (string.IsNullOrEmpty(Section))
            {
                errorMessages.Add("Section is empty");
            }

            return errorMessages;
        }

        public static User ReadDB(SQLiteDataReader reader)
        {
            User user = new User();
            user.Id = reader.GetInt32(0);
            user.Username = reader.GetString(1);
            user.Password = reader.GetString(2);
            user.Section = reader.GetString(3);
            user.UserType = (Models.UserType)reader.GetInt32(4);
            return user;
        }
    }
}
