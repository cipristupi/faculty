﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using HospitalPharmacy.Models;
using HospitalPharmacy.Utils;

namespace HospitalPharmacy.Repository
{
    public class MedicineRepository : IRepository<Medicine>,IFileOperations<Medicine>
    {
        private readonly string _filePath;
        private List<Medicine> _medicines;
        private XmlHelper<Medicine> _xmlHelper; 
        public MedicineRepository(string filePath)
        {
            _filePath = filePath;
            _medicines = ReadFromFile(_filePath).ToList();
            _xmlHelper = new XmlHelper<Medicine>();
        }
        public void Add(Medicine model)
        {
            _medicines.Add(model);
            SaveToFile(_medicines);
        }

        public void Update(Medicine model)
        {
            var obj = _medicines.FirstOrDefault(x => x.Id == model.Id);
            if (obj == null) return;
            obj.Id = model.Id;
            obj.Name = model.Name;
            SaveToFile(_medicines);
        }

        public IEnumerable<Medicine> GetAll()
        {
            return _medicines;
        }

        public void Delete(Medicine model)
        {
            _medicines.RemoveAll(x => x.Id == model.Id);
            SaveToFile(_medicines);
        }


        public IEnumerable<Medicine> ReadFromFile(string filePath)
        {
            List<Medicine> medicines = new List<Medicine>();
            XmlDocument doc = new XmlDocument();
            doc.Load(filePath);
            var usersTags = doc.GetElementsByTagName(EntityProcessor.GetObjectName(new Medicine()));
            _xmlHelper = new XmlHelper<Medicine>();
            foreach (var user in usersTags)
            {
                medicines.Add(_xmlHelper.GetObjectFromXmlElement(user as XmlElement));
            }
            return medicines;
        }

        public void SaveToFile(IEnumerable<Medicine> elementsList)
        {
            XmlDocument xmlDoc = new XmlDocument();
            XmlDeclaration xmlDec = xmlDoc.CreateXmlDeclaration("1.0", "utf-8", string.Empty);
            xmlDoc.PrependChild(xmlDec);
            XmlElement elemRoot = xmlDoc.CreateElement("Medicines");
            xmlDoc.AppendChild(elemRoot);
            foreach (var user in _medicines)
            {
                _xmlHelper.AddNodeToXmlDocument(xmlDoc, elemRoot, user);
            }
            xmlDoc.PreserveWhitespace = true;
            xmlDoc.Save(_filePath);
        }
    }
}
