﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Diagnostics;
using HospitalPharmacy.Models;

namespace HospitalPharmacy.Repository
{
    public class SqliteMedicineRepository :IRepository<Medicine>
    {
        SQLiteConnection m_dbConnection;
        SQLiteCommand command;
        private string connString = "Data Source=Pharmacy.sqlite;Version=3;datetimeformat=CurrentCulture";
        public void Add(Medicine model)
        {
            try
            {
                m_dbConnection = new SQLiteConnection(connString);
                m_dbConnection.Open();
                var insertQuery =
                    $"INSERT INTO Medicines (Name) VALUES (\"{model.Name}\")";
                command = new SQLiteCommand(insertQuery, m_dbConnection);
                var c = command.ExecuteNonQuery();
                m_dbConnection.Close();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }
        }

        public void Update(Medicine model)
        {
            try
            {
                m_dbConnection = new SQLiteConnection(connString);
                m_dbConnection.Open();
                string updateQuery = $"UPDATE Medicines SET  Name  = \"{model.Name}\" WHERE Id  ={model.Id}";
                command = new SQLiteCommand(updateQuery, m_dbConnection);
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }
        }

        public IEnumerable<Medicine> GetAll()
        {
            string selectQuery = "SELECT * FROM Medicines";
            List<Medicine> medicines = new List<Medicine>();
            try
            {
                m_dbConnection = new SQLiteConnection(connString);
                m_dbConnection.Open();
                command = new SQLiteCommand(selectQuery, m_dbConnection);
                SQLiteDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    medicines.Add(Medicine.ReadDB(reader));
                }
            }
            catch (Exception ex)
            {
                Debug.Write(ex.ToString());
            }
            return medicines;
        }

        public void Delete(Medicine model)
        {
            try
            {
                m_dbConnection = new SQLiteConnection(connString);
                m_dbConnection.Open();
                var deleteQuery = $"DELETE FROM Medicines WHERE Id={model.Id}";
                command = new SQLiteCommand(deleteQuery, m_dbConnection);
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }
        }
    }
}
