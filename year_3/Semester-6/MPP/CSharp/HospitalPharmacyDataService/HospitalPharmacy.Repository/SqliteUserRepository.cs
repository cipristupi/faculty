﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HospitalPharmacy.Models;

namespace HospitalPharmacy.Repository
{
    public class SqliteUserRepository : IRepository<User>
    {
        SQLiteConnection m_dbConnection;
        SQLiteCommand command;
        private string connString = "Data Source=Pharmacy.sqlite;Version=3;datetimeformat=CurrentCulture";
        public void Add(User model)
        {
            try
            {
                m_dbConnection = new SQLiteConnection(connString);
                m_dbConnection.Open();
                var insertQuery =
                    $"INSERT INTO Users (Username,Password,Section,UserType) VALUES (\"{model.Username}\",\"{model.Password}\",\"{model.Section}\",\"{(int)model.UserType}\")";
                command = new SQLiteCommand(insertQuery, m_dbConnection);
                var c = command.ExecuteNonQuery();
                m_dbConnection.Close();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }
        }

        public void Update(User model)
        {
            try
            {
                m_dbConnection = new SQLiteConnection(connString);
                m_dbConnection.Open();
                string updateQuery = $"UPDATE Users  SET  Username  = \"{model.Username}\",  Password  = \"{model.Password}\",  Section  = \"{model.Section}\",  UserType  =\"{(int)model.UserType}\" WHERE Id  ={model.Id}";
                command = new SQLiteCommand(updateQuery, m_dbConnection);
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }
        }

        public IEnumerable<User> GetAll()
        {
            string selectQuery = "SELECT * FROM Users";
            List<User> users = new List<User>();
            try
            {
                m_dbConnection = new SQLiteConnection(connString);
                m_dbConnection.Open();
                command = new SQLiteCommand(selectQuery, m_dbConnection);
                SQLiteDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    users.Add(User.ReadDB(reader));
                }
            }
            catch (Exception ex)
            {
                Debug.Write(ex.ToString());
            }
            return users;
        }

        public void Delete(User model)
        {
            try
            {
                m_dbConnection = new SQLiteConnection(connString);
                m_dbConnection.Open();
                var deleteQuery = $"DELETE FROM Users WHERE Id={model.Id}";
                command = new SQLiteCommand(deleteQuery, m_dbConnection);
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }
        }
    }
}
