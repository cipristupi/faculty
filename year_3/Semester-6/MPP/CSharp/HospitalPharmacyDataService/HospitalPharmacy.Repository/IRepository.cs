﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HospitalPharmacy.Repository
{
    public interface IRepository<T> where T : class
    {
        void Add(T model);
        void Update(T model);
        IEnumerable<T> GetAll();
        void Delete(T model);
    }
}
