﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using HospitalPharmacy.Models;
using HospitalPharmacy.Utils;

namespace HospitalPharmacy.Repository
{
    public class OrderRepository : IRepository<Order>,IFileOperations<Order>
    {
        private readonly string _filePath;
        private List<Order> _orders;
        private XmlHelper<Order> _xmlHelper; 
        public OrderRepository(string filePath)
        {
            _filePath = filePath;
            _orders = ReadFromFile(_filePath).ToList();
            _xmlHelper = new XmlHelper<Order>();
        }
        public void Add(Order model)
        {
            _orders.Add(model);
            SaveToFile(_orders);
        }

        public void Update(Order model)
        {
            var obj = _orders.FirstOrDefault(x => x.Id == model.Id);
            if (obj == null) return;
            obj.Id = model.Id;
            obj.MedicineId = model.MedicineId;
            obj.MedicineName = model.MedicineName;
            obj.Quantity = model.Quantity;
            obj.Status = model.Status;
            obj.UserId = model.UserId;
            SaveToFile(_orders);
        }

        public IEnumerable<Order> GetAll()
        {
            return _orders;
        }

        public void Delete(Order model)
        {
            _orders.RemoveAll(x => x.Id == model.Id);
            SaveToFile(_orders);
        }


        public IEnumerable<Order> ReadFromFile(string filePath)
        {
            List<Order> orders = new List<Order>();
            XmlDocument doc = new XmlDocument();
            doc.Load(filePath);
            var usersTags = doc.GetElementsByTagName(EntityProcessor.GetObjectName(new Order()));
            _xmlHelper = new XmlHelper<Order>();
            foreach (var user in usersTags)
            {
                orders.Add(_xmlHelper.GetObjectFromXmlElement(user as XmlElement));
            }
            return orders;
        }

        public void SaveToFile(IEnumerable<Order> elementsList)
        {
            XmlDocument xmlDoc = new XmlDocument();
            XmlDeclaration xmlDec = xmlDoc.CreateXmlDeclaration("1.0", "utf-8", string.Empty);
            xmlDoc.PrependChild(xmlDec);
            XmlElement elemRoot = xmlDoc.CreateElement("Orders");
            xmlDoc.AppendChild(elemRoot);
            foreach (var user in _orders)
            {
                _xmlHelper.AddNodeToXmlDocument(xmlDoc, elemRoot, user);
            }
            xmlDoc.PreserveWhitespace = true;
            xmlDoc.Save(_filePath);
        }
    }
}
