﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HospitalPharmacy.Models;

namespace HospitalPharmacy.Repository
{
    public class SqliteOrderRepository : IRepository<Order>
    {

        SQLiteConnection m_dbConnection;
        SQLiteCommand command;
        private string connString = "Data Source=Pharmacy.sqlite;Version=3;datetimeformat=CurrentCulture";
        public void Add(Order model)
        {
            try
            {
                m_dbConnection = new SQLiteConnection(connString);
                m_dbConnection.Open();
                var insertQuery =
                    $"INSERT INTO Orders (MedicineName,MedicineId,Quantity,Status,UserId) " +
                    $"VALUES (\"{model.MedicineName}\",{model.MedicineId},{model.Quantity},{(int)model.Status},{model.UserId})";
                command = new SQLiteCommand(insertQuery, m_dbConnection);
                var c = command.ExecuteNonQuery();
                m_dbConnection.Close();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }
        }

        public void Update(Order model)
        {
            try
            {
                m_dbConnection = new SQLiteConnection(connString);
                m_dbConnection.Open();
                string updateQuery =
                    $"UPDATE Orders  SET  MedicineName  = \"{model.MedicineName}\"," +
                    $"  MedicineId  = \"{model.MedicineId}\",  Quantity  = {model.Quantity}," +
                    $"  Status  ={(int) model.Status}, UserId =\"{model.UserId}\" WHERE Id  ={model.Id}";
                command = new SQLiteCommand(updateQuery, m_dbConnection);
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }
        }

        public IEnumerable<Order> GetAll()
        {
            string selectQuery = "SELECT * FROM Orders";
            List<Order> orders = new List<Order>();
            try
            {
                m_dbConnection = new SQLiteConnection(connString);
                m_dbConnection.Open();
                command = new SQLiteCommand(selectQuery, m_dbConnection);
                SQLiteDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    orders.Add(Order.ReadDB(reader));
                }
            }
            catch (Exception ex)
            {
                Debug.Write(ex.ToString());
            }
            return orders;
        }

        public void Delete(Order model)
        {
            try
            {
                m_dbConnection = new SQLiteConnection(connString);
                m_dbConnection.Open();
                var deleteQuery = $"DELETE FROM Orders WHERE Id={model.Id}";
                command = new SQLiteCommand(deleteQuery, m_dbConnection);
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }
        }
    }
}
