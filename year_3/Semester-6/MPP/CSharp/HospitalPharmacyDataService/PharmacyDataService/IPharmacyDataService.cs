﻿using System.Collections.Generic;
using System.ServiceModel;
using HospitalPharmacy.Models;

namespace PharmacyDataServiceN
{
    [ServiceContract]
    public interface IPharmacyDataService
    {
        [OperationContract]
        User CreateUser(User user);

        [OperationContract]
        User UpdateUser(User user);

        [OperationContract]
        IEnumerable<User> GetAllUsers();

        [OperationContract]
        Order AddOrder(Order order);

        [OperationContract]
        Order UpdateOrder(Order order);

        [OperationContract]
        IEnumerable<Order> GetAllOrders();

        [OperationContract]
        Medicine AddMedicine(Medicine medicine);

        [OperationContract]
        Medicine UpdateMedicine(Medicine medicine);

        [OperationContract]
        IEnumerable<Medicine> GetAllMedicines();
    }
}
