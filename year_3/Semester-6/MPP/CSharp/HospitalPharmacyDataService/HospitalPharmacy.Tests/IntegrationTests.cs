﻿using System;
using System.Xml;
using HospitalPharmacy.Models;
using HospitalPharmacy.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HospitalPharmacy.Tests
{
    [TestClass]
    public class IntegrationTests
    {
        [TestMethod]
        public void EntityProcessor_GetPropertiesAndValues_UserClass_AllPropertiesAreReturned()
        {
            //Arrange
            User user = new User()
            {
                Id = 1,
                Password = "password",
                Section = "section",
                Username = "username",
                UserType = UserType.Admin
            };
            //Act
            var properties = EntityProcessor.GetPropertiesAndValues(user);
            //Assert
            Assert.AreEqual(5, properties.Count);
            Assert.IsTrue(properties.ContainsKey(nameof(User.Id)));
            Assert.IsTrue(properties.ContainsKey(nameof(User.Password)));
            Assert.IsTrue(properties.ContainsKey(nameof(User.Section)));
            Assert.IsTrue(properties.ContainsKey(nameof(User.Username)));
            Assert.IsTrue(properties.ContainsKey(nameof(User.UserType)));
        }

        [TestMethod]
        public void EntityProccessor_AddNodeToXmlDocument_UserClass_AllNodesAreValid()
        {
            //Arrange
            User user = new User()
            {
                Id = 1,
                Password = "password",
                Section = "section",
                Username = "username",
                UserType =UserType.Admin
            };
            XmlDocument xmlDoc = new XmlDocument();
            XmlDeclaration xmlDec = xmlDoc.CreateXmlDeclaration("1.0", "utf-8", string.Empty);
            xmlDoc.PrependChild(xmlDec);
            XmlElement elemRoot = xmlDoc.CreateElement("Users");
            xmlDoc.AppendChild(elemRoot);
            XmlHelper<User> xmlHelper = new XmlHelper<User>();
            //Act
            xmlHelper.AddNodeToXmlDocument(xmlDoc, elemRoot, user);

            //Assert

        }


        [TestMethod]
        public void XmlHelper_GetObjectFromXmlElement_UserModel_Ok()
        {
            //Arrange
            User user = new User()
            {
                Id = 1,
                Password = "password",
                Section = "section",
                Username = "username",
                UserType = UserType.Admin
            };
            XmlDocument xmlDoc = new XmlDocument();
            XmlDeclaration xmlDec = xmlDoc.CreateXmlDeclaration("1.0", "utf-8", string.Empty);
            xmlDoc.PrependChild(xmlDec);
            XmlElement elemRoot = xmlDoc.CreateElement("Users");
            xmlDoc.AppendChild(elemRoot);
            XmlHelper<User> xmlHelper = new XmlHelper<User>();
            xmlHelper.AddNodeToXmlDocument(xmlDoc, elemRoot, user);

            //Act
            XmlHelper<User> xmlHelperUser = new XmlHelper<User>();
            var okObject = xmlHelperUser.GetObjectFromXmlElement(elemRoot.FirstChild as XmlElement);

        }


        [TestMethod]
        public void EntityProccessor_AddNodeToXmlDocument_ServiceConfigClass_AllNodesAreValid()
        {
            //Arrange
            ServiceConfig serviceConfig = new ServiceConfig()
            {
                Protocol = "TCP",
                Port = "1234",
                ServiceName = "PharmacyDataService"
            };
            XmlDocument xmlDoc = new XmlDocument();
            XmlDeclaration xmlDec = xmlDoc.CreateXmlDeclaration("1.0", "utf-8", string.Empty);
            xmlDoc.PrependChild(xmlDec);
            XmlElement elemRoot = xmlDoc.CreateElement("Configuration");
            xmlDoc.AppendChild(elemRoot);
            XmlHelper<ServiceConfig> xmlHelper = new XmlHelper<ServiceConfig>();
            //Act
            xmlHelper.AddNodeToXmlDocument(xmlDoc, elemRoot, serviceConfig);

            //Assert

        }
    }
}
