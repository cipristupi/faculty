﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Schema;

namespace HospitalPharmacy.Utils
{
    public static class EntityProcessor
    {
        public static Dictionary<string, object> GetPropertiesAndValues(object myObject)
        {
            Dictionary<string, object> propertiesDictionary = new Dictionary<string, object>();

            Type myType = myObject.GetType();
            IList<PropertyInfo> props = new List<PropertyInfo>(myType.GetProperties());

            foreach (PropertyInfo prop in props)
            {
                object propValue = prop.GetValue(myObject, null);
                propertiesDictionary.Add(prop.Name, propValue);
            }
            return propertiesDictionary;
        }

        public static List<string> GetProperties(object myObject)
        {
            return myObject.GetType().GetProperties().Select(x => x.Name).ToList();
        }

        public static Dictionary<string, Type> GetPropertiesAndTypes(object myObject)
        {
            return myObject.GetType()
                .GetProperties()
                .ToDictionary(property => property.Name, property => property.PropertyType);
        }

        public static string GetObjectName(object myObject)
        {
            Type myType = myObject.GetType();
            return myType.Name;
        }

        public static void SetPropertyValue(object myObject, string propertyName, object propertyValue,
            string propertyTypeString)
        {
            var property = myObject.GetType().GetProperty(propertyName);
            Type propertyType = Type.GetType(propertyTypeString);
            if (propertyType != null)
            {
                var rightType = Convert.ChangeType(propertyValue, propertyType);
                property.SetValue(myObject, rightType, null);
            }
        }
    }
}
