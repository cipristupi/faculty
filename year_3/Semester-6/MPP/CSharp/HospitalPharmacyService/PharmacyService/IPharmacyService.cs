﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ServiceModel;
using HospitalPharmacy.Models;

namespace PharmacyServiceN
{
    [ServiceContract(CallbackContract = typeof(INotifyServiceCallback), SessionMode = SessionMode.Allowed)]
    public interface IPharmacyService
    {
        [OperationContract]
        User Login(User user);

        [OperationContract]
        bool Logout(User user);

        [OperationContract]
        User CreateUser(User user);

        [OperationContract]
        User UpdateUser(User user);

        [OperationContract]
        User GetUserById(int id);

        [OperationContract]
        IEnumerable<User> GetAllUsers();

        [OperationContract]
        Order AddOrder(Order order);

        [OperationContract]
        Order UpdateOrder(Order order);

        [OperationContract]
        Order GetOrderById(int id);

        [OperationContract]
        IEnumerable<Order> GetAllOrders();

        [OperationContract]
        Medicine AddMedicine(Medicine medicine);

        [OperationContract]
        Medicine UpdateMedicine(Medicine medicine);

        [OperationContract]
        Medicine GetMedicineById(int id);

        [OperationContract]
        IEnumerable<Medicine> GetAllMedicines();
    }


    /// <summary>
    /// The callback contract to be implemented by the client
    /// application.
    /// </summary>
    public interface INotifyServiceCallback
    {
        /// <summary>
        /// Implemented by the client so that the server may call
        /// this when it receives a message to be broadcasted.
        /// </summary>
        /// <param name="message">
        /// The message to broadcast.
        /// </param>
        [OperationContract(IsOneWay = true)]
        void HandleMessage(string message);
    }
}
