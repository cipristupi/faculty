﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Threading;
using HospitalPharmacy.Models;
using PharmacyDataServiceN;

namespace PharmacyServiceN
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single,
      ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class PharmacyService : IPharmacyService
    {
        private readonly IPharmacyDataService _pharmacyDataService;

        public PharmacyService()
        {
            ChannelFactory<IPharmacyDataService> factory = null;
            try
            {
                var binding = new NetTcpBinding();//unsecureNetTcpBinding
                EndpointAddress address = new EndpointAddress("net.tcp://localhost:1234/PharmacyDataService");
                factory = new ChannelFactory<IPharmacyDataService>(binding, address);
                _pharmacyDataService = factory.CreateChannel();
            }
            catch (CommunicationException)
            {
                if (factory != null)
                    factory.Abort();

            }
            catch (TimeoutException)
            {
                if (factory != null)
                    factory.Abort();
            }
            catch (Exception ex)
            {
                if (factory != null)
                    factory.Abort();
                Console.WriteLine(ex.Message);
            }
        }

        public User Login(User user)
        {
            var users = GetAllUsers().FirstOrDefault(x => x.Password == user.Password && x.Username == user.Username);
            if (users != null)
            {
                Subscribe(user);
                return users;
            }
            FaultException fe = new FaultException("Invalid user");
            throw fe;
        }

      

        public bool Logout(User user)
        {
            throw new NotImplementedException();
        }

        public User CreateUser(User user)
        {
            var users = GetAllUsers().Where(x => x.Username == user.Username);
            if (users.Any())
            {
                throw new FaultException("User already exits");
            }
            if (user.Validate().Count() != 0)
            {
                throw new FaultException(string.Join(",", user.Validate()));
            }
            BroadcastMessage("UpdateUser");
            user.Id = GetUserId();
            return _pharmacyDataService.CreateUser(user);
        }

        public User UpdateUser(User user)
        {
            if (user.Validate().Count() != 0)
            {
                throw new FaultException(string.Join(",", user.Validate()));
            }
            var users = GetAllUsers().FirstOrDefault(x => x.Id == user.Id);
            if (users == null)
            {
                throw new FaultException("User doesn't exists");
            }
            BroadcastMessage("UpdateUser");
            return _pharmacyDataService.UpdateUser(user);
        }

        public User GetUserById(int id)
        {
            return GetAllUsers().FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<User> GetAllUsers()
        {
            return _pharmacyDataService.GetAllUsers();
        }


        public Order AddOrder(Order order)
        {
            if (order.Validate().Count() != 0)
            {
                throw new FaultException(string.Join(",", order.Validate()));
            }
            if (GetUserById(order.UserId) == null)
            {
                throw new FaultException("User doesn't exits");
            }
            if (GetMedicineById(order.MedicineId) == null)
            {
                throw new FaultException("Medicine doesn't exits");
            }
            order.Id = GetOrderId();
            BroadcastMessage("UpdateOrder");
            return _pharmacyDataService.AddOrder(order);
        }

        public Order UpdateOrder(Order order)
        {
            if (order.Validate().Count() != 0)
            {
                throw new FaultException(string.Join(",", order.Validate()));
            }
            if (GetUserById(order.UserId) == null)
            {
                throw new FaultException("User doesn't exits");
            }
            if (GetMedicineById(order.MedicineId) == null)
            {
                throw new FaultException("Medicine doesn't exits");
            }
            if (GetOrderById(order.Id) != null)
            {
                throw new FaultException("Order doesn't exits");
            }
            BroadcastMessage("UpdateOrder");
            return _pharmacyDataService.UpdateOrder(order);
        }

        public Order GetOrderById(int id)
        {
            return GetAllOrders().FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<Order> GetAllOrders()
        {
            return _pharmacyDataService.GetAllOrders();
        }

        public Medicine AddMedicine(Medicine medicine)
        {
            if (medicine.Validate().Count() != 0)
            {
                throw new FaultException(string.Join(",", medicine.Validate()));
            }
            medicine.Id = GetMedicineId();
            BroadcastMessage("UpdateMedicine");
            return _pharmacyDataService.AddMedicine(medicine);
        }

        public Medicine UpdateMedicine(Medicine medicine)
        {
            if (medicine.Validate().Count() != 0)
            {
                throw new FaultException(string.Join(",", medicine.Validate()));
            }
            BroadcastMessage("UpdateMedicine");
            return _pharmacyDataService.UpdateMedicine(medicine);
        }

        public Medicine GetMedicineById(int id)
        {
            return GetAllMedicines().FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<Medicine> GetAllMedicines()
        {
            return _pharmacyDataService.GetAllMedicines();
        }



        private ConcurrentDictionary<Guid, INotifyServiceCallback> _clients =
          new ConcurrentDictionary<Guid, INotifyServiceCallback>();

        private void Subscribe(User user)
        {
            INotifyServiceCallback callback =
                OperationContext.Current.GetCallbackChannel<INotifyServiceCallback>();

            Guid clientId = Guid.NewGuid();

            if (callback != null)
            {
                lock (_clients)
                {
                    _clients.TryAdd(clientId, callback);
                    user.ClientGuid = clientId;
                }
            }

        }

        private int GetUserId()
        {
            var lastUser = _pharmacyDataService.GetAllUsers().OrderByDescending(x => x.Id).FirstOrDefault();
            int lastUserId = (int)DateTime.Now.Ticks;
            if (lastUser != null)
            {
                lastUserId = lastUser.Id;
            }
            int userId = lastUserId + 1;
            return userId;
        }


        private int GetOrderId()
        {
            var lastOrder = _pharmacyDataService.GetAllOrders().OrderByDescending(x => x.Id).FirstOrDefault();
            int lastOrderId = (int)DateTime.Now.Ticks;
            if (lastOrder != null)
            {
                lastOrderId = lastOrder.Id;
            }
            int orderId = lastOrderId + 1;
            return orderId;
        }
        private int GetMedicineId()
        {
            var lastMedicine = _pharmacyDataService.GetAllMedicines().OrderByDescending(x => x.Id).FirstOrDefault();
            int lastMedicineId = (int)DateTime.Now.Ticks;
            if (lastMedicine != null)
            {
                lastMedicineId = lastMedicine.Id;
            }
            int medicineId = lastMedicineId + 1;
            return medicineId;
        }


        /// <summary>
        /// Notifies the clients of messages.
        /// </summary>
        /// <param name="clientId">Identifies the client that sent the message.</param>
        /// <param name="message">The message to be sent to all connected clients.</param>
        private void BroadcastMessage(string message)
        {
            // Call each client's callback method
            ThreadPool.QueueUserWorkItem
            (
                delegate
                {
                    lock (_clients)
                    {
                        List<Guid> disconnectedClientGuids = new List<Guid>();

                        foreach (KeyValuePair<Guid, INotifyServiceCallback> client in _clients)
                        {
                            try
                            {
                                client.Value.HandleMessage(message);
                            }
                            catch (Exception)
                            {
                                // TODO: Better to catch specific exception types.                     

                                // If a timeout exception occurred, it means that the server
                                // can't connect to the client. It might be because of a network
                                // error, or the client was closed  prematurely due to an exception or
                                // and was unable to unregister from the server. In any case, we 
                                // must remove the client from the list of clients.

                                // Another type of exception that might occur is that the communication
                                // object is aborted, or is closed.

                                // Mark the key for deletion. We will delete the client after the 
                                // for-loop because using foreach construct makes the clients collection
                                // non-modifiable while in the loop.
                                disconnectedClientGuids.Add(client.Key);
                            }
                        }

                        foreach (Guid clientGuid in disconnectedClientGuids)
                        {
                            INotifyServiceCallback value;
                            _clients.TryRemove(clientGuid, out value);
                        }
                    }
                }
            );
        }


        private void CheckServiceState()
        {
            //if(_pharmacyDataService)
        }
    }
}
