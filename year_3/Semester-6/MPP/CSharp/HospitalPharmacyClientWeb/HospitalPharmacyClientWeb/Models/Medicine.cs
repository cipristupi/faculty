﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using HospitalPharmacy.Models;

namespace HospitalPharmacyClientWeb.Models
{
    [DataContract]
    public class Medicine
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        [Required]
        public string Name { get; set; }

        public List<string> Validate()
        {
            List<string> errorMessage = new List<string>();
            if (string.IsNullOrEmpty(Name))
            {
                errorMessage.Add("Name is empty");
            }
            return errorMessage;
        }

        public static explicit operator HospitalPharmacy.Models.Medicine(Medicine v)
        {
            HospitalPharmacy.Models.Medicine medicine = new HospitalPharmacy.Models.Medicine();
            medicine.Id = v.Id;
            medicine.Name = v.Name;
            return medicine;
        }

        public static explicit operator Medicine(HospitalPharmacy.Models.Medicine v)
        {
            Medicine medicine = new Medicine
            {
                Id = v.Id,
                Name = v.Name
            };
            return medicine;
        }
    }
}
