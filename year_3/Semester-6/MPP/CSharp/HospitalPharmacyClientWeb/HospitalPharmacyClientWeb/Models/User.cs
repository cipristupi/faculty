﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using HospitalPharmacy.Models;

namespace HospitalPharmacyClientWeb.Models
{
    [DataContract]
    public class User
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        [Required]
        public string Username { get; set; }

        [DataMember]
        [Required]
        public string Password { get; set; }

        [DataMember]
        [Required]
        public string Section { get; set; }

        [DataMember]
        [Required]
        public UserType UserType { get; set; }

        [DataMember]
        public Guid? ClientGuid { get; set; }

        public List<string> Validate()
        {
            List<string> errorMessages = new List<string>();
            if (string.IsNullOrEmpty(Username))
            {
                errorMessages.Add("Username is empty");
            }
            if (string.IsNullOrEmpty(Password))
            {
                errorMessages.Add("Password is empty");
            }
            if (string.IsNullOrEmpty(Section))
            {
                errorMessages.Add("Section is empty");
            }
           
            return errorMessages;
        }

        public static explicit operator HospitalPharmacy.Models.User(User v)
        {
            HospitalPharmacy.Models.User user = new HospitalPharmacy.Models.User
            {
                Id = v.Id,
                ClientGuid = v.ClientGuid,
                Password = v.Password
            };
            int enumValue = (int) v.UserType;
            user.UserType = (HospitalPharmacy.Models.UserType)Enum.ToObject(typeof(HospitalPharmacy.Models.UserType), enumValue);
            user.Username = v.Username;
            user.Section = v.Section;
            return user;
        }

        public static explicit operator User(HospitalPharmacy.Models.User v)
        {
            User user = new User
            {
                Id = v.Id,
                ClientGuid = v.ClientGuid,
                Password = v.Password
            };
            int enumValue = (int)v.UserType;
            user.UserType = (UserType)Enum.ToObject(typeof(UserType), enumValue);
            user.Username = v.Username;
            user.Section = v.Section;
            return user;
        }
    }
}
