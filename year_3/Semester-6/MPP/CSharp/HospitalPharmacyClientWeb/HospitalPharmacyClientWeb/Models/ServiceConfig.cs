﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HospitalPharmacy.Models
{
    public class ServiceConfig
    {
        public string Protocol { get; set; }
        public string Port { get; set; }
        public string ServiceName { get; set; }
    }
}
