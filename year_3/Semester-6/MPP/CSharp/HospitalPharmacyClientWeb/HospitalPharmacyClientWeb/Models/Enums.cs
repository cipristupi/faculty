﻿namespace HospitalPharmacyClientWeb.Models
{
    public enum UserType
    {
        Admin,
        Doctor,
        Pharmacist
    }

    public enum OrderStatus
    {
        Open,
        Delivered,
        NotPossible
    }
}
