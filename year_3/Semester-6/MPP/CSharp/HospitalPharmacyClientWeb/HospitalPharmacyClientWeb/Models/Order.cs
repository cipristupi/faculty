﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using HospitalPharmacy.Models;

namespace HospitalPharmacyClientWeb.Models
{
    [DataContract]
    public class Order
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        [Required]
        public string MedicineName { get; set; }

        [DataMember]
        [Required]
        public int MedicineId { get; set; }

        [DataMember]
        [Required]
        public int Quantity { get; set; }

        [DataMember]
        [Required]
        public OrderStatus Status { get; set; }

        [DataMember]
        [Required]
        public int UserId { get; set; }

        public List<string> Validate()
        {
            List<string> errorMessages = new List<string>();
            if (string.IsNullOrEmpty(MedicineName))
            {
                errorMessages.Add("Medicine name is empty");
            }
            return errorMessages;
        }

        public static explicit operator Order(HospitalPharmacy.Models.Order v)
        {
            Order order = new Order
            {
                Id = v.Id,
                MedicineId = v.MedicineId,
                Quantity = v.Quantity,
                MedicineName = v.MedicineName,
                UserId = v.UserId
            };

            int enumValue = (int)v.Status;
            order.Status = (OrderStatus)Enum.ToObject(typeof(OrderStatus), enumValue);

            return order;
        }

        public static explicit operator HospitalPharmacy.Models.Order(Order v)
        {
            HospitalPharmacy.Models.Order order = new HospitalPharmacy.Models.Order();
            order.Id = v.Id;
            order.MedicineId = v.MedicineId;
            order.Quantity = v.Quantity;
            order.MedicineName = v.MedicineName;
            order.UserId = v.UserId;

            int enumValue = (int)v.Status;
            order.Status = (HospitalPharmacy.Models.OrderStatus)Enum.ToObject(typeof(HospitalPharmacy.Models.OrderStatus), enumValue);

            return order;
        }
    }
}
