﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Web;
using System.Web.Mvc;
using HospitalPharmacyClient;
using HospitalPharmacyClientWeb.Models;
using PharmacyServiceN;

namespace HospitalPharmacyClientWeb.Controllers
{
    public class OrderController : Controller
    {
        private IPharmacyService _pharmacyService;
        private void CreateConnetionToService()
        {
            ChannelFactory<IPharmacyService> factory = null;
            try
            {
                NotifyServiceCallback notifyServiceCallback = new NotifyServiceCallback();
                InstanceContext site = new InstanceContext(notifyServiceCallback);
                var binding = new NetTcpBinding(); //unsecureNetTcpBinding
                EndpointAddress address = new EndpointAddress("net.tcp://localhost:1235/PharmacyService");
                DuplexChannelFactory<IPharmacyService> cf = new DuplexChannelFactory<IPharmacyService>(site, binding);
                _pharmacyService = cf.CreateChannel(address);
            }
            catch (CommunicationException communicationException)
            {
                if (factory != null)
                {
                    factory.Abort();
                }
            }
            catch (TimeoutException timeoutException)
            {
                if (factory != null)
                {
                    factory.Abort();
                }
            }
            catch (Exception ex)
            {
                if (factory != null)
                {
                    factory.Abort();
                }
            }
        }
        // GET: Medicine
        public ActionResult Index()
        {
            CreateConnetionToService();
            var ordersService = _pharmacyService.GetAllOrders();
            List<Order> orders = new List<Order>();
            foreach (var item in ordersService)
            {
                orders.Add((Order) item);
            }
            return View(orders);
        }


        // GET: Medicine/Create
        public ActionResult Create()
        {
            return View(new Order());
        }

        // POST: Medicine/Create
        [HttpPost]
        public ActionResult Create(Order model)
        {
            CreateConnetionToService();
            try
            {
                if (ModelState.IsValid)
                {
                    //HospitalPharmacy.Models.User user = (HospitalPharmacy.Models.User) model;
                    _pharmacyService.AddOrder((HospitalPharmacy.Models.Order)model);
                    return RedirectToAction("Index");
                }

                return View(model);
            }
            catch
            {
                return View(model);
            }
        }

        // GET: Medicine/Edit/5
        public ActionResult Edit(int id)
        {
            CreateConnetionToService();
            Order user = (Order)_pharmacyService.GetOrderById(id);
            return View(user);
        }

        // POST: Medicine/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Order model)
        {
            CreateConnetionToService();
            try
            {
                if (ModelState.IsValid)
                {
                    //HospitalPharmacy.Models.User user = (HospitalPharmacy.Models.User) model;
                    _pharmacyService.UpdateOrder((HospitalPharmacy.Models.Order)model);
                    return RedirectToAction("Index");
                }
                return View(model);
            }
            catch
            {
                return View(model);
            }
        }

    }
}
