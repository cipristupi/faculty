﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.ServiceModel;
using System.Web;
using System.Web.Mvc;
using HospitalPharmacyClient;
using HospitalPharmacyClientWeb.Models;
using PharmacyServiceN;

namespace HospitalPharmacyClientWeb.Controllers
{
    public class UserController : Controller
    {
        private IPharmacyService _pharmacyService;
        private void CreateConnetionToService()
        {
            ChannelFactory<IPharmacyService> factory = null;
            try
            {
                NotifyServiceCallback notifyServiceCallback = new NotifyServiceCallback();
                InstanceContext site = new InstanceContext(notifyServiceCallback);
                var binding = new NetTcpBinding(); //unsecureNetTcpBinding
                EndpointAddress address = new EndpointAddress("net.tcp://localhost:1235/PharmacyService");
                DuplexChannelFactory<IPharmacyService> cf = new DuplexChannelFactory<IPharmacyService>(site, binding);
                _pharmacyService = cf.CreateChannel(address);
            }
            catch (CommunicationException communicationException)
            {
                //MessageBox.Show(communicationException.ToString());
                if (factory != null)
                {
                    factory.Abort();
                }
            }
            catch (TimeoutException timeoutException)
            {
                //MessageBox.Show(timeoutException.ToString());
                if (factory != null)
                {
                    factory.Abort();
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
                if (factory != null)
                {
                    factory.Abort();
                }
            }
        }
        // GET: User
        public ActionResult Index()
        {
            CreateConnetionToService();
            var usersService = _pharmacyService.GetAllUsers();
            List<User> users= new List<User>();
            foreach (var u in usersService)
            {
                users.Add((User)u);
            }
            return View(users);
        }

        // GET: User/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: User/Create
        public ActionResult Create()
        {
            CreateConnetionToService();
            return View(new User());
        }

        // POST: User/Create
        [HttpPost]
        public ActionResult Create(User model)
        {
            CreateConnetionToService();
            try
            {
                if (ModelState.IsValid)
                {
                    //HospitalPharmacy.Models.User user = (HospitalPharmacy.Models.User) model;
                    _pharmacyService.CreateUser((HospitalPharmacy.Models.User)model);
                    return RedirectToAction("Index");
                }

                return View(model);
            }
            catch(Exception ex)
            {
                return View(model);
            }
        }

        // GET: User/Edit/5
        public ActionResult Edit(int id)
        {
            CreateConnetionToService();
            User user = (User)_pharmacyService.GetUserById(id);
            return View(user);
        }

        // POST: User/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, User model)
        {
            CreateConnetionToService();
            try
            {
                if (ModelState.IsValid)
                {
                    //HospitalPharmacy.Models.User user = (HospitalPharmacy.Models.User) model;
                    _pharmacyService.UpdateUser((HospitalPharmacy.Models.User)model);
                    return RedirectToAction("Index");
                }
                return View(model);
            }
            catch
            {
                return View(model);
            }
        }

        // GET: User/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: User/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
