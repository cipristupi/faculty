﻿using System;

using PharmacyServiceN;

namespace HospitalPharmacyClient
{
    /// <summary>
    /// Implementation of the callback contract.
    /// </summary>
    public class NotifyServiceCallback : INotifyServiceCallback
    {
        public event ClientNotifiedEventHandler ClientNotified;
       

        /// <summary>
        /// Notifies the client of the message by raising an event.
        /// </summary>
        /// <param name="message">Message from the server.</param>
        void INotifyServiceCallback.HandleMessage(string message)
        {
            if (ClientNotified != null)
            {
                ClientNotified(this, new ClientNotifiedEventArgs(message));
            }
            //Form activeForm = MyActiveForm.CurrentForm;
            //if (activeForm != null)
            //{
            //    if (activeForm is AdminForm)
            //    {
            //        ((AdminForm)activeForm).Notify(message);
            //    }
            //    if (activeForm is DoctorForm)
            //    {
            //        ((DoctorForm)activeForm).Notify(message);
            //    }
            //    if (activeForm is PharmacistForm)
            //    {
            //        ((PharmacistForm)activeForm).Notify(message);
            //    }
            //}
        }
    }

    public delegate void ClientNotifiedEventHandler(object sender, ClientNotifiedEventArgs e);

    public class ClientNotifiedEventArgs : EventArgs
    {
        private readonly string message;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="message">Message from server.</param>
        public ClientNotifiedEventArgs(string message)
        {
            this.message = message;
        }

        /// <summary>
        /// Gets the message.
        /// </summary>
        public string Message
        {
            get { return message; }
        }
    }

}
