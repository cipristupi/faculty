﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Web;
using System.Web.Mvc;
using HospitalPharmacyClient;
using HospitalPharmacyClientWeb.Models;
using PharmacyServiceN;

namespace HospitalPharmacyClientWeb.Controllers
{
    public class MedicineController : Controller
    {
        private IPharmacyService _pharmacyService;
        private void CreateConnetionToService()
        {
            ChannelFactory<IPharmacyService> factory = null;
            try
            {
                NotifyServiceCallback notifyServiceCallback = new NotifyServiceCallback();
                InstanceContext site = new InstanceContext(notifyServiceCallback);
                var binding = new NetTcpBinding(); //unsecureNetTcpBinding
                EndpointAddress address = new EndpointAddress("net.tcp://localhost:1235/PharmacyService");
                DuplexChannelFactory<IPharmacyService> cf = new DuplexChannelFactory<IPharmacyService>(site, binding);
                _pharmacyService = cf.CreateChannel(address);
            }
            catch (CommunicationException communicationException)
            {
                //MessageBox.Show(communicationException.ToString());
                if (factory != null)
                {
                    factory.Abort();
                }
            }
            catch (TimeoutException timeoutException)
            {
                //MessageBox.Show(timeoutException.ToString());
                if (factory != null)
                {
                    factory.Abort();
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
                if (factory != null)
                {
                    factory.Abort();
                }
            }
        }
        // GET: Medicine
        public ActionResult Index()
        {
            CreateConnetionToService();
            var medicineSerivce = _pharmacyService.GetAllMedicines();

            List<Medicine> medicines = medicineSerivce.Select(u => (Medicine) u).ToList();
            return View(medicines);
        }

    
        // GET: Medicine/Create
        public ActionResult Create()
        {
            return View(new Medicine());
        }

        // POST: Medicine/Create
        [HttpPost]
        public ActionResult Create(Medicine model)
        {
            CreateConnetionToService();
            try
            {
                if (ModelState.IsValid)
                {
                    //HospitalPharmacy.Models.User user = (HospitalPharmacy.Models.User) model;
                    _pharmacyService.AddMedicine((HospitalPharmacy.Models.Medicine)model);
                    return RedirectToAction("Index");
                }

                return View(model);
            }
            catch
            {
                return View(model);
            }
        }

        // GET: Medicine/Edit/5
        public ActionResult Edit(int id)
        {
            CreateConnetionToService();
            Medicine user = (Medicine)_pharmacyService.GetMedicineById(id);
            return View(user);
        }

        // POST: Medicine/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Medicine model)
        {
            CreateConnetionToService();
            try
            {
                if (ModelState.IsValid)
                {
                    //HospitalPharmacy.Models.User user = (HospitalPharmacy.Models.User) model;
                    _pharmacyService.UpdateMedicine((HospitalPharmacy.Models.Medicine)model);
                    return RedirectToAction("Index");
                }
                return View(model);
            }
            catch(Exception ex)
            {
                return View(model);
            }
        }

    }
}
