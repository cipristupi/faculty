﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(HospitalPharmacyClientWeb.Startup))]
namespace HospitalPharmacyClientWeb
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
