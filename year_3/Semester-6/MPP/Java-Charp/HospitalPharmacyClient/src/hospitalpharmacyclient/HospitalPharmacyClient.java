/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hospitalpharmacyclient;

import UI.LoginForm;
import Utils.ReflectionHelper;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;
import static models.Enums.UserType.Admin;
import models.ServiceConfig;

/**
 *
 * @author Cipri
 */
public class HospitalPharmacyClient {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            ReflectionHelper reflectionHelper = new ReflectionHelper();
            
            ServiceConfig serviceConfig = reflectionHelper.GetServiceConfig("ServiceConfig.xml");
            
            LoginForm form1 = new LoginForm(serviceConfig);
            form1.setVisible(true);
            Scanner s = new Scanner(System.in);
            System.out.println(s.nextInt());
            /*SwingUtilities.invokeLater(new Runnable() {
            public void run() {
            Admin form1 = new Admin(serviceConfig);
            form1.setVisible(true);
            }
            });*/
        } catch (NoSuchMethodException ex) {
            Logger.getLogger(HospitalPharmacyClient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(HospitalPharmacyClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
