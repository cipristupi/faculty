/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import java.rmi.RemoteException;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JPanel;

import Ui.Admin;
import Ui.Doctor;
import Ui.Pharmacist;
import models.Medicine;
import models.Order;
import models.User;
import pharmacyService.IObserverServices;
import pharmacyService.PharmacyServiceException;
import java.rmi.server.UnicastRemoteObject;
import pharmacyService.*;

/**
 *
 * @author Cipri
 */
public class RmiController extends UnicastRemoteObject implements IController, IObserverServices {

    JFrame currentForm;
    IPharmacyService _service;
    String formType;

    public RmiController(JFrame form) throws RemoteException {
        currentForm = form;
    }
    public RmiController(JFrame form, IPharmacyService service) throws RemoteException {
        _service = service;
        currentForm = form;
    }

    public RmiController(JFrame form, String formType, IPharmacyService service) throws RemoteException {
        _service = service;
        currentForm = form;
        this.formType = formType;
    }

    @Override
    public User Login(User user) throws PharmacyServiceException {
        User loggedUser = _service.Login(user);
        return loggedUser ;
    }

    @Override
    public boolean Logout(User user) throws PharmacyServiceException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public User CreateUser(User user) throws PharmacyServiceException {
        User createdUser = _service.CreateUser(user);
        return createdUser;
    }

    @Override
    public User UpdateUser(User user) throws PharmacyServiceException {
        User createdUser = _service.UpdateUser(user);
        return createdUser;
    }

    @Override
    public User GetUserById(int id) throws PharmacyServiceException {
        User createdUser = _service.GetUserById(id);
        return createdUser;
    }

    @Override
    public List<User> GetAllUsers() throws PharmacyServiceException {
        return _service.GetAllUsers();
    }

    @Override
    public Order AddOrder(Order order) throws PharmacyServiceException {
        return _service.AddOrder(order);
    }

    @Override
    public Order UpdateOrder(Order order) throws PharmacyServiceException {
        return _service.UpdateOrder(order);
    }

    @Override
    public Order GetOrderById(int id) throws PharmacyServiceException {
        return _service.GetOrderById(id);
    }

    @Override
    public List<Order> GetAllOrders() throws PharmacyServiceException {
        return _service.GetAllOrders();
    }

    @Override
    public Medicine AddMedicine(Medicine medicine) throws PharmacyServiceException {
        return _service.AddMedicine(medicine);
    }

    @Override
    public Medicine UpdateMedicine(Medicine medicine) throws PharmacyServiceException {
        return _service.UpdateMedicine(medicine);
    }

    @Override
    public Medicine GetMedicineById(int id) throws PharmacyServiceException {
        return _service.GetMedicineById(id);
    }

    @Override
    public List<Medicine> GetAllMedicines() throws PharmacyServiceException {
        return _service.GetAllMedicines();
    }

    @Override
    public void RegisterClient(User loggedUser) {
        _service.RegisterClient(loggedUser, this);
    }

    @Override
    public void notify(String message) throws PharmacyServiceException, RemoteException {
        if (formType == "Admin") {
            ((Admin) currentForm).Notify(message);
        }
        if (formType == "Pharmacist") {
            ((Pharmacist) currentForm).Notify(message);
        }
        if (formType == "Doctor") {
            ((Doctor) currentForm).Notify(message);
        }
    }

}
