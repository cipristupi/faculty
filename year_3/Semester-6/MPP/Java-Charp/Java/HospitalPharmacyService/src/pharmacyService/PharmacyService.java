package pharmacyService;

import models.Medicine;
import models.Order;
import models.User;
import repository.MedicineRepository;
import repository.OrderRepository;
import repository.UserRepository;

import java.rmi.RemoteException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Cipri on 4/5/2016.
 */
public class PharmacyService implements IPharmacyService {

    UserRepository userRepository;
    OrderRepository orderRepository;
    MedicineRepository medicineRepository;
    private Map<String, IObserverServices> loggedClients;

    public PharmacyService(UserRepository userRepo, OrderRepository orderRepo, MedicineRepository medicineRepo) {
        userRepository = userRepo;
        orderRepository = orderRepo;
        medicineRepository = medicineRepo;
        loggedClients = new ConcurrentHashMap<>();
    }

    @Override
    public synchronized User Login(User user) throws PharmacyServiceException {
        User loggedUser = userRepository.GetByUserName(user.Username);
        if (loggedUser != null) {
            return loggedUser;
        }
        throw new PharmacyServiceException("Invalid user");
    }


    @Override
    public synchronized boolean Logout(User user) {
        return false;
    }

    @Override
    public synchronized User CreateUser(User user) throws PharmacyServiceException {
        User users = userRepository.GetByUserName(user.Username);
        if (users != null) {
            throw new PharmacyServiceException("User already exits");
        }

        if (!user.Validate().isEmpty()) {
            throw new PharmacyServiceException(String.join(",", user.Validate()));
        }
        userRepository.Add(user);
        BroadcastMessage("UpdateUser");
        return userRepository.GetByUserName(user.Username);
    }


    @Override
    public synchronized User UpdateUser(User user) throws PharmacyServiceException {
        if (!user.Validate().isEmpty()) {
            throw new PharmacyServiceException(String.join(",", user.Validate()));
        }
        User users = userRepository.GetById(user.Id);
        if (users == null) {
            throw new PharmacyServiceException("User doesn't exists");
        }
        BroadcastMessage("UpdateUser");
        userRepository.Update(user);
        return userRepository.GetById(user.Id);
    }

    @Override
    public synchronized User GetUserById(int id) {
        return userRepository.GetById(id);
    }

    @Override
    public synchronized List<User> GetAllUsers() {
        return userRepository.GetAll();
    }

    @Override
    public synchronized Order AddOrder(Order order) throws PharmacyServiceException {
        if (!order.Validate().isEmpty()) {
            throw new PharmacyServiceException(String.join(",", order.Validate()));
        }
        if (GetUserById(order.UserId) == null) {
            throw new PharmacyServiceException("User doesn't exits");
        }
        if (GetMedicineById(order.MedicineId) == null) {
            throw new PharmacyServiceException("Medicine doesn't exits");
        }
        BroadcastMessage("UpdateOrder");
        orderRepository.Add(order);
        return order;
    }

    @Override
    public synchronized Order UpdateOrder(Order order) throws PharmacyServiceException {
        if (!order.Validate().isEmpty()) {
            throw new PharmacyServiceException(String.join(",", order.Validate()));
        }
        if (GetUserById(order.UserId) == null) {
            throw new PharmacyServiceException("User doesn't exits");
        }
        if (GetMedicineById(order.MedicineId) == null) {
            throw new PharmacyServiceException("Medicine doesn't exits");
        }
        if (GetOrderById(order.Id) != null) {
            throw new PharmacyServiceException("Order doesn't exits");
        }
        BroadcastMessage("UpdateOrder");
        orderRepository.Update(order);
        return order;
    }

    @Override
    public synchronized Order GetOrderById(int id) {
        return orderRepository.GetById(id);
    }

    @Override
    public synchronized List<Order> GetAllOrders() {
        return orderRepository.GetAll();
    }

    @Override
    public synchronized Medicine AddMedicine(Medicine medicine) {
         medicineRepository.Add(medicine);
        return medicine;
    }

    @Override
    public synchronized Medicine UpdateMedicine(Medicine medicine) throws PharmacyServiceException {
        if (!medicine.Validate().isEmpty()) {
            throw new PharmacyServiceException(String.join(",", medicine.Validate()));
        }
        if (GetMedicineById(medicine.Id) != null) {
            throw new PharmacyServiceException("Medicine already exits");
        }
        BroadcastMessage("UpdateMedicine");
        medicineRepository.Add(medicine);
        return medicine;
    }

    @Override
    public synchronized Medicine GetMedicineById(int id) {
        return medicineRepository.GetById(id);
    }

    @Override
    public synchronized List<Medicine> GetAllMedicines() {
        return medicineRepository.GetAll();
    }

    @Override
    public void RegisterClient(User user,IObserverServices client) {
        try {
            Subscribe(user, client);
        } catch (PharmacyServiceException e) {
            e.printStackTrace();
        }
    }

    private void Subscribe(User user, IObserverServices client) throws PharmacyServiceException {
        String userId = Integer.toString(user.Id);
        if (loggedClients.get(userId) != null)
            throw new PharmacyServiceException("User already logged in.");
        loggedClients.put(userId, client);
    }

    private void BroadcastMessage(String message) {
        ExecutorService executor = Executors.newFixedThreadPool(loggedClients.keySet().size());
        for (String i : loggedClients.keySet()) {
            IObserverServices client = loggedClients.get(i);
            if (client != null)
                executor.execute(() -> {
                    try {
                        System.out.println("Notifying [" + i + "] client.");
                        client.notify(message);
                    } catch (PharmacyServiceException | RemoteException e) {
                        System.out.println("Error notifying client " + e);
                    }
                });
        }

    }

}
