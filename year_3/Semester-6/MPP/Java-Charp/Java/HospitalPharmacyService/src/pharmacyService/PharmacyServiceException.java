package pharmacyService;

/**
 * Created by Cipri on 4/5/2016.
 */
public class PharmacyServiceException extends Exception {
    public PharmacyServiceException() {

    }
    public PharmacyServiceException(String message) {
        super(message);
    }
    public PharmacyServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
