package models;

import javax.xml.ws.Service;

/**
 * Created by Cipri on 4/4/2016.
 */
public class ServiceConfig {

    public String Protocol;
    public String Port;
    public String ServiceName ;

    public ServiceConfig(String protocol, String port, String serviceName){
        this.Protocol = protocol;
        this.Port = port;
        this.ServiceName = serviceName;
    }
}
