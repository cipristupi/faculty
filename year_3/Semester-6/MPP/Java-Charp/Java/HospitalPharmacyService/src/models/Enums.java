package models;

/**
 * Created by Cipri on 4/4/2016.
 */
public class Enums {

    public enum UserType
    {
        Admin,
        Doctor,
        Pharmacist
    }

    public enum OrderStatus
    {
        Open,
        Delivered,
        NotPossible
    }
}
