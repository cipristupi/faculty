package models;

import helpers.Helper;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Cipri on 4/4/2016.
 */
public class Medicine implements Serializable {
    public int Id;

    public String Name;

    public Medicine(int id, String name)
    {
        this.Id = id;
        this.Name = name;
    }
    public Medicine(){

    }
    public List<String> Validate()
    {
        Helper helper = new Helper();
        List<String> errorMessage = new ArrayList<>();
        if (helper.isNullOrEmpty(Name))
        {
            errorMessage.add("Name is empty");
        }
        return errorMessage;
    }
}
