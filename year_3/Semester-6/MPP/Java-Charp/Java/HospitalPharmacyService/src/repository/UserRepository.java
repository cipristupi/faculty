package repository;

import models.Enums;
import models.User;

import java.util.List;
import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Logger;

/**
 * Created by Cipri on 4/4/2016.
 */
public class UserRepository implements IRepository<User>{
    String filePath = null;
    public UserRepository(){
        filePath = System.getProperty("user.dir") + "\\src\\Repository\\Pharmacy.sqlite";
    }
    @Override
    public void Add(User model) {
        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + filePath);
            c.setAutoCommit(false);
            stmt = c.createStatement();
            String sql = "INSERT INTO Users (Username,Password,Section,UserType) VALUES ('"+ model.Username+"','"+model.Password+"','"+ model.Section+"',"+ model.UserType+");";
            stmt.executeUpdate(sql);
            stmt.close();
            c.commit();
            c.close();
            // logger.info("[Add:] " + p);
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            // logger.info("[Error Add:] " + e.getClass().getName() + ": " + e.getMessage());
        }
    }

    @Override
    public void Update(User model) {
        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + filePath);
            c.setAutoCommit(false);
            stmt = c.createStatement();
            String sql = String.format("UPDATE Users SET Username = '%1$s',Password = '%2$s',Section = '%3$s' , UserType = %4$s WHERE Id = %5$s;", model.Username, model.Password,model.Section,model.UserType,model.Id);
            stmt.executeUpdate(sql);
            c.commit();
            stmt.close();
            c.close();
            // logger.info("[Update:] " + p);
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            // logger.info("[Error Update:] " + e.getClass().getName() + ": " + e.getMessage());
        }
    }

    @Override
    public List<User> GetAll() {
        ArrayList<User> result = new ArrayList<>();
        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + filePath);
            c.setAutoCommit(false);
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM Users;");
            while (rs.next()) {
                int id = rs.getInt("Id");
                String username = rs.getString("Username");
                String password = rs.getString("Password");
                String section = rs.getString("Section");
                int userType = rs.getInt("UserType");
                result.add(new User(id,username,password,section,userType));
            }
            rs.close();
            stmt.close();
            c.close();
            // logger.info("[GetAll] ");
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            // logger.info("[Error GetAll:] " + e.getClass().getName() + ": " + e.getMessage());
        }
        return result;
    }


    @Override
    public void Delete(User model) {
        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + filePath);
            c.setAutoCommit(false);
            stmt = c.createStatement();
            String sql = String.format("DELETE FROM Users WHERE Id =%1$s;", model.Id);
            stmt.executeUpdate(sql);
            c.commit();
            stmt.close();
            c.close();
            // logger.info("[Delete:] " + code);
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            //  logger.info("[Error Delete:] " + e.getClass().getName() + ": " + e.getMessage());
        }
    }

    @Override
    public User GetById(int id) {
        Connection c = null;
        Statement stmt = null;
        User p = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + filePath);
            c.setAutoCommit(false);
            String sql = String.format("SELECT * FROM Users WHERE Id =%1$s;",id);
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                int idUser = rs.getInt("Id");
                String username = rs.getString("Username");
                String password = rs.getString("Password");
                String section = rs.getString("Section");
                int userType = rs.getInt("UserType");
                p = new User(idUser,username,password,section,userType);
            }
            rs.close();
            stmt.close();
            c.close();
            // logger.info("[GetByCode] ");
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            // logger.info("[Error GetByCode:] " + e.getClass().getName() + ": " + e.getMessage());
        }
        return p;
    }


    public User GetByUserName(String userName) {
        Connection c = null;
        Statement stmt = null;
        User p = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + filePath);
            c.setAutoCommit(false);
            String sql = String.format("SELECT * FROM Users WHERE Username = '%1$s';",userName);
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                int idUser = rs.getInt("Id");
                String username = rs.getString("Username");
                String password = rs.getString("Password");
                String section = rs.getString("Section");
                int userType = rs.getInt("UserType");
                p = new User(idUser,username,password,section,userType);
            }
            rs.close();
            stmt.close();
            c.close();
            // logger.info("[GetByCode] ");
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            // logger.info("[Error GetByCode:] " + e.getClass().getName() + ": " + e.getMessage());
        }
        return p;
    }
}
