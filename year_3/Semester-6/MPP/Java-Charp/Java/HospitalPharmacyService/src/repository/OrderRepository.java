package repository;

import models.Enums;
import models.Order;
import models.User;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Cipri on 4/4/2016.
 */
public class OrderRepository implements IRepository<Order> {
    String filePath = null;

    public OrderRepository() {
        filePath = System.getProperty("user.dir") + "\\src\\Repository\\Pharmacy.sqlite";
    }

    @Override
    public void Add(Order model) {
        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + filePath);
            c.setAutoCommit(false);
            stmt = c.createStatement();
            String sql = "INSERT INTO Orders (MedicineName,MedicineId,Quantity,Status,UserId) VALUES ('" + model.MedicineName + "'," + model.MedicineId + "," + model.Quantity + "," + model.Status + "," + model.UserId + ");";
            stmt.executeUpdate(sql);
            stmt.close();
            c.commit();
            c.close();
            // logger.info("[Add:] " + p);
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            // logger.info("[Error Add:] " + e.getClass().getName() + ": " + e.getMessage());
        }
    }

    @Override
    public void Update(Order model) {
        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + filePath);
            c.setAutoCommit(false);
            stmt = c.createStatement();
            String sql = String.format("UPDATE Orders SET MedicineName = '%1$s',MedicineId = %2$s,Quantity = %3$s , Status = %4$s , UserId =%5$s WHERE Id = %4$s;", model.MedicineName, model.MedicineId, model.Quantity, model.Status, model.UserId, model.Id);
            stmt.executeUpdate(sql);
            c.commit();
            stmt.close();
            c.close();
            // logger.info("[Update:] " + p);
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            // logger.info("[Error Update:] " + e.getClass().getName() + ": " + e.getMessage());
        }
    }

    @Override
    public List<Order> GetAll() {

        ArrayList<Order> result = new ArrayList<>();
        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + filePath);
            c.setAutoCommit(false);
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM Users;");
            while (rs.next()) {
                int id = rs.getInt("Id");
                String medicineName = rs.getString("MedicineName");
                int medicineId = rs.getInt("MedicineId");
                int quantity = rs.getInt("Quantity");
                int status = rs.getInt("Status");
                int userId = rs.getInt("UserId");
                result.add(new Order(id, medicineName, medicineId, quantity,status, userId));
            }
            rs.close();
            stmt.close();
            c.close();
            // logger.info("[GetAll] ");
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            // logger.info("[Error GetAll:] " + e.getClass().getName() + ": " + e.getMessage());
        }
        return result;
    }

    @Override
    public void Delete(Order model) {
        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + filePath);
            c.setAutoCommit(false);
            stmt = c.createStatement();
            String sql = String.format("DELETE FROM Orders WHERE Id =%1$s;", model.Id);
            stmt.executeUpdate(sql);
            c.commit();
            stmt.close();
            c.close();
            // logger.info("[Delete:] " + code);
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            //  logger.info("[Error Delete:] " + e.getClass().getName() + ": " + e.getMessage());
        }
    }

    @Override
    public Order GetById(int id) {

        Connection c = null;
        Statement stmt = null;
        Order p = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + filePath);
            c.setAutoCommit(false);
            String sql = String.format("SELECT * FROM Users WHERE Id =%1$s;", id);
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                int idOrder = rs.getInt("Id");
                String medicineName = rs.getString("MedicineName");
                int medicineId = rs.getInt("MedicineId");
                int quantity = rs.getInt("Quantity");
                int status = rs.getInt("Status");
                int userId = rs.getInt("UserId");
                p = new Order(idOrder, medicineName, medicineId, quantity, status, userId);
            }
            rs.close();
            stmt.close();
            c.close();
            // logger.info("[GetByCode] ");
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            // logger.info("[Error GetByCode:] " + e.getClass().getName() + ": " + e.getMessage());
        }
        return p;
    }
}
