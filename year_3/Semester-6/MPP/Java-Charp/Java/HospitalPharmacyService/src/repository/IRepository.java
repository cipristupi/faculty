package repository;

import java.util.List;

/**
 * Created by Cipri on 4/4/2016.
 */
public interface IRepository<T> {
    void Add(T model);
    void Update(T model);
    List<T> GetAll();
    void Delete(T model);
    T GetById(int id);
}
