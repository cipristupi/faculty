package repository;

import models.Medicine;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Cipri on 4/4/2016.
 */
public class MedicineRepository  implements IRepository<Medicine>{
    String filePath = null;

    public MedicineRepository(){
        filePath = System.getProperty("user.dir") + "\\src\\Repository\\Pharmacy.sqlite";
    }
    @Override
    public void Add(Medicine model) {
        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + filePath);
            c.setAutoCommit(false);
            stmt = c.createStatement();
            String sql = "INSERT INTO Medicines ('Name') VALUES ('"+ model.Name+"');";
            stmt.executeUpdate(sql);
            stmt.close();
            c.commit();
            c.close();
            // logger.info("[Add:] " + p);
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            // logger.info("[Error Add:] " + e.getClass().getName() + ": " + e.getMessage());
        }
    }

    @Override
    public void Update(Medicine model) {
        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + filePath);
            c.setAutoCommit(false);
            stmt = c.createStatement();
            String sql = String.format("UPDATE Medicines SET Name = '%1$s' WHERE Id = %4$s;", model.Name, model.Id);
            stmt.executeUpdate(sql);
            c.commit();
            stmt.close();
            c.close();
            // logger.info("[Update:] " + p);
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            // logger.info("[Error Update:] " + e.getClass().getName() + ": " + e.getMessage());
        }
    }

    @Override
    public List<Medicine> GetAll() {
        ArrayList<Medicine> result = new ArrayList<>();
        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + filePath);
            c.setAutoCommit(false);
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM Medicines;");
            while (rs.next()) {
                int id = rs.getInt("Id");
                String name = rs.getString("Name");
                result.add(new Medicine(id,name));
            }
            rs.close();
            stmt.close();
            c.close();
            // logger.info("[GetAll] ");
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            // logger.info("[Error GetAll:] " + e.getClass().getName() + ": " + e.getMessage());
        }
        return result;
    }

    @Override
    public void Delete(Medicine model) {
        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + filePath);
            c.setAutoCommit(false);
            stmt = c.createStatement();
            String sql = String.format("DELETE FROM Medicines WHERE Id =%1$s;", model.Id);
            stmt.executeUpdate(sql);
            c.commit();
            stmt.close();
            c.close();
            // logger.info("[Delete:] " + code);
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            //  logger.info("[Error Delete:] " + e.getClass().getName() + ": " + e.getMessage());
        }
    }

    @Override
    public Medicine GetById(int id) {
        Connection c = null;
        Statement stmt = null;
        Medicine p = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + filePath);
            c.setAutoCommit(false);
            String sql = String.format("SELECT * FROM Medicines WHERE Id =%1$s;",id);
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                int idUser = rs.getInt("Id");
                String name = rs.getString("Name");
                p = new Medicine(id,name);
            }
            rs.close();
            stmt.close();
            c.close();
            // logger.info("[GetByCode] ");
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            // logger.info("[Error GetByCode:] " + e.getClass().getName() + ": " + e.getMessage());
        }
        return p;
    }
}
