package helpers;


import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static sun.reflect.misc.ReflectUtil.forName;

/**
 * Created by Cipri on 4/4/2016.
 */
public class EntityProcessor {

    public List<Field> GetProperties(Object myObject) {
        Class<?> c = myObject.getClass();
        Field[] fields = c.getDeclaredFields();
        List<Field> fieldsToReturn = new ArrayList<Field>();
        for (Field field : fields) {
            try {
                fieldsToReturn.add(field);
            } catch (IllegalArgumentException e1) {
            }
        }
        return fieldsToReturn;
    }

    public static Map<String, Object> GetPropertiesAndValues(Object obj) {
        Class<?> c = obj.getClass();
        Field[] fields = c.getDeclaredFields();
        Map<String, Object> temp = new HashMap<String, Object>();

        for (Field field : fields) {
            try {
                temp.put(field.getName().toString(), field.get(obj));
            } catch (IllegalArgumentException e1) {
            } catch (IllegalAccessException e1) {
            }
        }

        return temp;
    }
}
