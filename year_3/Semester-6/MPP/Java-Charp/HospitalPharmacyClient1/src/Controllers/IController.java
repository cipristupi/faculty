/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import java.util.List;
import models.Medicine;
import models.Order;
import models.User;
import pharmacyService.PharmacyServiceException;

/**
 *
 * @author Cipri
 */
public interface IController {

    User Login(User user) throws PharmacyServiceException;

    boolean Logout(User user) throws PharmacyServiceException;

    User CreateUser(User user) throws PharmacyServiceException;

    User UpdateUser(User user) throws PharmacyServiceException;

    User GetUserById(int id) throws PharmacyServiceException;

    List<User> GetAllUsers() throws PharmacyServiceException;

    Order AddOrder(Order order) throws PharmacyServiceException;

    Order UpdateOrder(Order order) throws PharmacyServiceException;

    Order GetOrderById(int id) throws PharmacyServiceException;

    List<Order> GetAllOrders() throws PharmacyServiceException;

    Medicine AddMedicine(Medicine medicine) throws PharmacyServiceException;

    Medicine UpdateMedicine(Medicine medicine) throws PharmacyServiceException;

    Medicine GetMedicineById(int id) throws PharmacyServiceException;

    List<Medicine> GetAllMedicines() throws PharmacyServiceException;
}
