/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import java.util.List;
import javax.swing.JFrame;
import javax.swing.JPanel;
import static models.Enums.UserType.Admin;
import models.Medicine;
import models.Order;
import models.User;
import pharmacyService.PharmacyServiceException;

/**
 *
 * @author Cipri
 */
public class RmiController implements IController {

    JFrame currentForm;

    public RmiController(JFrame form) {
        currentForm = form;
    }

    public RmiController(JFrame form, String formType) {
        if (formType == "Admin") {
            currentForm = (Admin) form;
        }
    }

    @Override
    public User Login(User user) throws PharmacyServiceException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean Logout(User user) throws PharmacyServiceException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public User CreateUser(User user) throws PharmacyServiceException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public User UpdateUser(User user) throws PharmacyServiceException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public User GetUserById(int id) throws PharmacyServiceException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<User> GetAllUsers() throws PharmacyServiceException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Order AddOrder(Order order) throws PharmacyServiceException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Order UpdateOrder(Order order) throws PharmacyServiceException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Order GetOrderById(int id) throws PharmacyServiceException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Order> GetAllOrders() throws PharmacyServiceException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Medicine AddMedicine(Medicine medicine) throws PharmacyServiceException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Medicine UpdateMedicine(Medicine medicine) throws PharmacyServiceException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Medicine GetMedicineById(int id) throws PharmacyServiceException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Medicine> GetAllMedicines() throws PharmacyServiceException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
