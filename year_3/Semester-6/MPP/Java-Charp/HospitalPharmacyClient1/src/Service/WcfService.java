/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Service;

/**
 *
 * @author Cipri
 */
public class WcfService {

    public static String getAllUsers() {
        org.tempuri.PharmacyService service = new org.tempuri.PharmacyService();
        org.tempuri.IPharmacyService port = service.getBasicHttpBindingIPharmacyService();
        return port.getAllUsers();
    }

    public  static String createUser(java.lang.String user) {
        org.tempuri.PharmacyService service = new org.tempuri.PharmacyService();
        org.tempuri.IPharmacyService port = service.getBasicHttpBindingIPharmacyService();
        return port.createUser(user);
    }

    public static String login(java.lang.String user) {
        org.tempuri.PharmacyService service = new org.tempuri.PharmacyService();
        org.tempuri.IPharmacyService port = service.getBasicHttpBindingIPharmacyService();
        return port.login(user);
    }

    public  static String addMedicine(java.lang.String medicine) {
        org.tempuri.PharmacyService service = new org.tempuri.PharmacyService();
        org.tempuri.IPharmacyService port = service.getBasicHttpBindingIPharmacyService();
        return port.addMedicine(medicine);
    }

    public  static String addOrder(java.lang.String order) {
        org.tempuri.PharmacyService service = new org.tempuri.PharmacyService();
        org.tempuri.IPharmacyService port = service.getBasicHttpBindingIPharmacyService();
        return port.addOrder(order);
    }

    public  static String getAllMedicines() {
        org.tempuri.PharmacyService service = new org.tempuri.PharmacyService();
        org.tempuri.IPharmacyService port = service.getBasicHttpBindingIPharmacyService();
        return port.getAllMedicines();
    }

    public  static String getAllOrders() {
        org.tempuri.PharmacyService service = new org.tempuri.PharmacyService();
        org.tempuri.IPharmacyService port = service.getBasicHttpBindingIPharmacyService();
        return port.getAllOrders();
    }

    public  static String getMedicineById(java.lang.Integer id) {
        org.tempuri.PharmacyService service = new org.tempuri.PharmacyService();
        org.tempuri.IPharmacyService port = service.getBasicHttpBindingIPharmacyService();
        return port.getMedicineById(id);
    }

    public  static String getOrderById(java.lang.Integer id) {
        org.tempuri.PharmacyService service = new org.tempuri.PharmacyService();
        org.tempuri.IPharmacyService port = service.getBasicHttpBindingIPharmacyService();
        return port.getOrderById(id);
    }

    public  static String getUserById(java.lang.Integer id) {
        org.tempuri.PharmacyService service = new org.tempuri.PharmacyService();
        org.tempuri.IPharmacyService port = service.getBasicHttpBindingIPharmacyService();
        return port.getUserById(id);
    }

    public  static String updateMedicine(java.lang.String medicine) {
        org.tempuri.PharmacyService service = new org.tempuri.PharmacyService();
        org.tempuri.IPharmacyService port = service.getBasicHttpBindingIPharmacyService();
        return port.updateMedicine(medicine);
    }

    public  static String updateOrder(java.lang.String order) {
        org.tempuri.PharmacyService service = new org.tempuri.PharmacyService();
        org.tempuri.IPharmacyService port = service.getBasicHttpBindingIPharmacyService();
        return port.updateOrder(order);
    }

    public  static String updateUser(java.lang.String user) {
        org.tempuri.PharmacyService service = new org.tempuri.PharmacyService();
        org.tempuri.IPharmacyService port = service.getBasicHttpBindingIPharmacyService();
        return port.updateUser(user);
    }
    
    
    
}
