package pharmacyService;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by Cipri on 4/5/2016.
 */
public interface IObserverServices extends Remote {
    void notify(String message) throws PharmacyServiceException, RemoteException;
}
