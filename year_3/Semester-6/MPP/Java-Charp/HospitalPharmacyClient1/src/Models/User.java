package models;


import helpers.Helper;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Cipri on 4/4/2016.
 */
public class User implements Serializable{
    public int Id;

    public String Username;

    public String Password;

    public String Section;

    public int UserType;
    
    public String ClientGuid;

    public User(){
        
    }
     public User(int id, String username,String password,String section, int userType)
     {
         this.Id =id;
         this.Username=username;
         this.Password=password;
         this.Section=section;
         this.UserType=userType;
     }

    public List<String> Validate()
    {
        Helper helper = new Helper();
        List<String> errorMessages = new ArrayList<String>();
        if (helper.isNullOrEmpty(Username))
        {
            errorMessages.add("Username is empty");
        }
        if (helper.isNullOrEmpty(Password))
        {
            errorMessages.add("Password is empty");
        }
        if (helper.isNullOrEmpty(Section))
        {
            errorMessages.add("Section is empty");
        }
        return errorMessages;
    }
}
