﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HospitalPharmacy.Repository
{
    interface IFileOperations<T> where T:class
    {
        IEnumerable<T> ReadFromFile(string filePath);
        void SaveToFile(IEnumerable<T> elementsList);
    }
}
