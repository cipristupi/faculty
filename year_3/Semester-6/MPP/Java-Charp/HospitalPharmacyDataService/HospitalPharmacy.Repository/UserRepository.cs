﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using HospitalPharmacy.Models;
using HospitalPharmacy.Utils;

namespace HospitalPharmacy.Repository
{
    public class UserRepository : IRepository<User>,IFileOperations<User>
    {
        private readonly string _filePath;
        private List<User> _users;
        private XmlHelper<User> _xmlHelper; 
        public UserRepository(string filePath)
        {
            _filePath = filePath;
            _users = ReadFromFile(_filePath).ToList();
            _xmlHelper = new XmlHelper<User>();
        }
        public void Add(User model)
        {
            _users.Add(model);
            SaveToFile(_users);
        }

        public void Update(User model)
        {
            var obj = _users.FirstOrDefault(x => x.Id == model.Id);
            if (obj == null) return;
            obj.Password = model.Password;
            obj.Section = model.Section;
            obj.UserType = model.UserType;
            obj.Username = model.Username;
            SaveToFile(_users);
        }

        public IEnumerable<User> GetAll()
        {
            return _users;
        }

        public void Delete(User model)
        {
            _users.RemoveAll(x => x.Id == model.Id);
            SaveToFile(_users);
        }


        public IEnumerable<User> ReadFromFile(string filePath)
        {
            List<User> userList = new List<User>();
            XmlDocument doc = new XmlDocument();
            doc.Load(filePath);
            var usersTags = doc.GetElementsByTagName(EntityProcessor.GetObjectName(new User()));
            var xmlHelper = new XmlHelper<User>();
            foreach (var user in usersTags)
            {
                userList.Add(xmlHelper.GetObjectFromXmlElement(user as XmlElement));
            }
            return userList;
        }

        public void SaveToFile(IEnumerable<User> elementsList)
        {
            XmlDocument xmlDoc = new XmlDocument();
            XmlDeclaration xmlDec = xmlDoc.CreateXmlDeclaration("1.0", "utf-8", string.Empty);
            xmlDoc.PrependChild(xmlDec);
            XmlElement elemRoot = xmlDoc.CreateElement("Users");
            xmlDoc.AppendChild(elemRoot);
            XmlHelper<User> xmlHelper = new XmlHelper<User>();
            foreach (var user in _users)
            {
                xmlHelper.AddNodeToXmlDocument(xmlDoc, elemRoot, user);
            }
            xmlDoc.PreserveWhitespace = true;
            xmlDoc.Save(_filePath);
        }
    }
}
