﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using HospitalPharmacy.Models;
using HospitalPharmacy.Repository;

namespace PharmacyDataServiceN
{
    public class PharmacyDataService : IPharmacyDataService
    {
        private readonly IRepository<User> userRepository; 
        private readonly IRepository<Medicine> medicineRepository; 
        private readonly IRepository<Order> orderRepository; 
        public PharmacyDataService()
        {
            //userRepository=new UserRepository("Users.xml");
            userRepository=new SqliteUserRepository();
            //medicineRepository=new MedicineRepository("Medicines.xml");
            medicineRepository=new SqliteMedicineRepository();
            //orderRepository=new OrderRepository("Orders.xml");
            orderRepository=new SqliteOrderRepository();
        }
        public User CreateUser(User user)
        {
            userRepository.Add(user);
            return user;
        }

        public User UpdateUser(User user)
        {
            userRepository.Update(user);
            return user;
        }

        public IEnumerable<User> GetAllUsers()
        {
            return userRepository.GetAll();
        }

        public Order AddOrder(Order order)
        {
            orderRepository.Add(order);
            return order;
        }

        public Order UpdateOrder(Order order)
        {
            orderRepository.Update(order);
            return order;
        }

        public IEnumerable<Order> GetAllOrders()
        {
            return orderRepository.GetAll();
        }

        public Medicine AddMedicine(Medicine medicine)
        {
           medicineRepository.Add(medicine);
            return medicine;
        }

        public Medicine UpdateMedicine(Medicine medicine)
        {
            medicineRepository.Update(medicine);
            return medicine;
        }

        public IEnumerable<Medicine> GetAllMedicines()
        {
            return medicineRepository.GetAll();
        }
    }
}
