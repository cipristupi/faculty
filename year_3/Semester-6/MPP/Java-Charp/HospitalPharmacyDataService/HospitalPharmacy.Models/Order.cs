﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace HospitalPharmacy.Models
{
    [DataContract]
    public class Order
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string MedicineName { get; set; }

        [DataMember]
        public int MedicineId { get; set; }

        [DataMember]
        public int Quantity { get; set; }

        [DataMember]
        public OrderStatus Status { get; set; }

        [DataMember]
        public int UserId { get; set; }

        public List<string> Validate()
        {
            List<string> errorMessages = new List<string>();
            if (string.IsNullOrEmpty(MedicineName))
            {
                errorMessages.Add("Medicine name is empty");
            }
            return errorMessages;
        }

        public static Order ReadDB(SQLiteDataReader reader)
        {
            Order order = new Order();
            order.Id = reader.GetInt32(0);
            order.MedicineName = reader.GetString(1);
            order.MedicineId = reader.GetInt32(2);
            order.Quantity = reader.GetInt32(3);
            order.Status = (OrderStatus)reader.GetInt32(4);
            order.UserId = reader.GetInt32(5);
            return order;
        }
    }
}
