﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace HospitalPharmacy.Utils
{
    public class XmlHelper<T> where T: class 
    {
        public void AddNodeToXmlDocument(XmlDocument document, XmlElement rootElement, object myObject)
        {
            XmlElement myObjectElement = document.CreateElement(EntityProcessor.GetObjectName(myObject));
            foreach (var objectProperty in EntityProcessor.GetPropertiesAndValues(myObject))
            {
                var elem = document.CreateElement(objectProperty.Key);
                if (objectProperty.Value != null)
                {
                    elem.SetAttribute("type", objectProperty.Value.GetType().FullName);
                    elem.InnerText = objectProperty.Value.ToString();
                }
                myObjectElement.AppendChild(elem);
            }
            rootElement.AppendChild(myObjectElement);
        }

        public T GetObjectFromXmlElement(XmlElement xmlElement)
        {
            T myObject = Activator.CreateInstance<T>();
            var childNodes = xmlElement.ChildNodes;
            foreach (XmlElement childNode in childNodes)
            {
                var propertyName = childNode.Name;
                object propertyValue = childNode.InnerText;
                var propertyType = childNode.GetAttribute("type");
                EntityProcessor.SetPropertyValue(myObject,propertyName,propertyValue,propertyType);
            }
            return myObject;
        }
    }
}
