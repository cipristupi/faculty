﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using HospitalPharmacy.Models;
using HospitalPharmacy.Utils;
using PharmacyServiceN;

namespace HospitalPharmacyService
{
    class Program
    {
        static ServiceHost m_svcHost = null;
        static void Main(string[] args)
        {
            ServiceConfig serviceConfig = ReadServiceConfiguration();

            string strBinding = serviceConfig.Protocol.ToUpper();
            bool bSuccess = ((strBinding == "TCP") || (strBinding == "HTTP"));
            if (bSuccess == false)
            {
                Console.WriteLine("\nBinding argument is invalid, should be either TCP or HTTP)");
                return;
            }
            int nPort = 0;
            bSuccess = int.TryParse(serviceConfig.Port, out nPort);
            if (bSuccess == false)
            {
                Console.WriteLine("\nPort number must be a numeric value");
                return;
            }

            bool bindingTCP = (strBinding == "TCP");
            if (bindingTCP) StartTCPService(nPort, serviceConfig.ServiceName); else StartHTTPService(nPort, serviceConfig.ServiceName);
            if (m_svcHost != null)
            {
                Console.WriteLine("\nPress any key to close the Service");
                Console.ReadKey();
                StopService();
            }
            Console.ReadLine();
        }

        private static void StartTCPService(int nPort, string serviceName)
        {
            string strAdr = "net.tcp://localhost:" + nPort + $"/{serviceName}/";
            try
            {
                Uri adrbase = new Uri(strAdr);
                m_svcHost = new ServiceHost(typeof(PharmacyServiceN.PharmacyService), adrbase);

                ServiceMetadataBehavior mBehave = new ServiceMetadataBehavior();
                m_svcHost.Description.Behaviors.Add(mBehave);
                m_svcHost.AddServiceEndpoint(typeof(IMetadataExchange), MetadataExchangeBindings.CreateMexTcpBinding(), "mex");

                NetTcpBinding tcpb = new NetTcpBinding();
                m_svcHost.AddServiceEndpoint(typeof(IPharmacyService), tcpb, strAdr);
                m_svcHost.Open();
                Console.WriteLine("\n\nService is Running as >> " + strAdr);
            }
            catch (Exception eX)
            {
                m_svcHost = null;
                Console.WriteLine("Service can not be started as >> [" + strAdr + "] \n\nError Message [" + eX.Message + "]");
            }
        }

        private static void StartHTTPService(int nPort, string serviceName)
        {
            string strAdr = "http://localhost:" + nPort + $"/{serviceName}";
            try
            {
                Uri adrbase = new Uri(strAdr);
                m_svcHost = new ServiceHost(typeof(PharmacyServiceN.PharmacyService), adrbase);

                ServiceMetadataBehavior mBehave = new ServiceMetadataBehavior();
                mBehave.HttpGetEnabled = true;
                m_svcHost.Description.Behaviors.Add(mBehave);
                m_svcHost.AddServiceEndpoint(typeof(IMetadataExchange), MetadataExchangeBindings.CreateMexHttpBinding(), "mex");


                BasicHttpBinding httpb = new BasicHttpBinding();
                httpb.OpenTimeout = TimeSpan.FromMinutes(3);
                httpb.SendTimeout = TimeSpan.FromMinutes(3);
                httpb.ReceiveTimeout = TimeSpan.FromMinutes(10);
                m_svcHost.AddServiceEndpoint(typeof(IPharmacyService), httpb, strAdr);
                m_svcHost.Open();
                Console.WriteLine("\n\nService is Running as >> " + strAdr);
            }
            catch (Exception eX)
            {
                m_svcHost = null;
                Console.WriteLine("Service can not be started as >> [" + strAdr + "] \n\nError Message [" + eX.Message + "]");
            }
        }

        private static ServiceConfig ReadServiceConfiguration()
        {
            List<ServiceConfig> userList = new List<ServiceConfig>();
            XmlDocument doc = new XmlDocument();
            doc.Load("ServiceConfig.xml");
            var elementsByTagName = doc.GetElementsByTagName(EntityProcessor.GetObjectName(new ServiceConfig()));
            XmlHelper<ServiceConfig> _xmlHelper = new XmlHelper<ServiceConfig>();
            foreach (var serviceConfiguration in elementsByTagName)
            {
                userList.Add(_xmlHelper.GetObjectFromXmlElement(serviceConfiguration as XmlElement));
            }
            return userList.FirstOrDefault();
        }

        private static void StopService()
        {
            if (m_svcHost != null)
            {
                m_svcHost.Close();
                m_svcHost = null;
            }
        }
    }
}
