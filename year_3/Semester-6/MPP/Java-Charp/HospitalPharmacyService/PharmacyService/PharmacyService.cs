﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Threading;
using HospitalPharmacy.Models;
using Newtonsoft.Json;
using PharmacyDataServiceN;

namespace PharmacyServiceN
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single,
      ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class PharmacyService : IPharmacyService
    {
        private IPharmacyDataService _pharmacyDataService;

        public PharmacyService()
        {
            ConnectToDataService();
        }

        private void ConnectToDataService()
        {
            ChannelFactory<IPharmacyDataService> factory = null;
            try
            {
                var binding = new NetTcpBinding(); //unsecureNetTcpBinding
                EndpointAddress address = new EndpointAddress("net.tcp://localhost:1234/PharmacyDataService");
                factory = new ChannelFactory<IPharmacyDataService>(binding, address);
                _pharmacyDataService = factory.CreateChannel();
            }
            catch (CommunicationException)
            {
                if (factory != null)
                    factory.Abort();
            }
            catch (TimeoutException)
            {
                if (factory != null)
                    factory.Abort();
            }
            catch (Exception ex)
            {
                if (factory != null)
                    factory.Abort();
                Console.WriteLine(ex.Message);
            }
        }

        public string Login(string userJson)
        {
            ConnectToDataService();
            User user = JsonConvert.DeserializeObject<User>(userJson);
            var users = _pharmacyDataService.GetAllUsers().FirstOrDefault(x => x.Password == user.Password && x.Username == user.Username);
            if (users != null)
            {
                //Subscribe(user);
                return JsonConvert.SerializeObject(users);
            }
            return JsonConvert.SerializeObject("Invalid user");
        }



        public bool Logout(User user)
        {
            throw new NotImplementedException();
        }

        public string CreateUser(string userJson)
        {
            ConnectToDataService();
            User user = JsonConvert.DeserializeObject<User>(userJson);
            var users = _pharmacyDataService.GetAllUsers().Where(x => x.Username == user.Username);
            if (users.Any())
            {
                throw new FaultException("User already exits");
            }
            if (user.Validate().Count() != 0)
            {
                throw new FaultException(string.Join(",", user.Validate()));
            }
            //BroadcastMessage("UpdateUser");
            user.Id = GetUserId();
            return JsonConvert.SerializeObject(_pharmacyDataService.CreateUser(user));
        }

        public string UpdateUser(string userJson)
        {
            ConnectToDataService();
            User user = JsonConvert.DeserializeObject<User>(userJson);
            if (user.Validate().Count() != 0)
            {
                throw new FaultException(string.Join(",", user.Validate()));
            }
            var users = _pharmacyDataService.GetAllUsers().FirstOrDefault(x => x.Id == user.Id);
            if (users == null)
            {
                throw new FaultException("User doesn't exists");
            }
            //BroadcastMessage("UpdateUser");
            //return _pharmacyDataService.UpdateUser(user);
            return JsonConvert.SerializeObject(_pharmacyDataService.UpdateUser(user));
        }

        public string GetUserById(int id)
        {
            ConnectToDataService();
            var user = _pharmacyDataService.GetAllUsers().FirstOrDefault(x => x.Id == id);
            return JsonConvert.SerializeObject(user);
        }

        public string GetAllUsers()
        {
            ConnectToDataService();
            var users= _pharmacyDataService.GetAllUsers();
            return JsonConvert.SerializeObject(users);
        }

        public string AddOrder(string orderJson)
        {
            ConnectToDataService();
            Order order = JsonConvert.DeserializeObject<Order>(orderJson);
            if (order.Validate().Count() != 0)
            {
                throw new FaultException(string.Join(",", order.Validate()));
            }
            if (GetUserById(order.UserId) == null)
            {
                throw new FaultException("User doesn't exits");
            }
            if (GetMedicineById(order.MedicineId) == null)
            {
                throw new FaultException("Medicine doesn't exits");
            }
            order.Id = GetOrderId();
            return JsonConvert.SerializeObject(_pharmacyDataService.AddOrder(order));
        }

        public string UpdateOrder(string orderJson)
        {
            ConnectToDataService();
            Order order = JsonConvert.DeserializeObject<Order>(orderJson);
            if (order.Validate().Count() != 0)
            {
                throw new FaultException(string.Join(",", order.Validate()));
            }
            if (GetUserById(order.UserId) == null)
            {
                throw new FaultException("User doesn't exits");
            }
            if (GetMedicineById(order.MedicineId) == null)
            {
                throw new FaultException("Medicine doesn't exits");
            }
            if (GetOrderById(order.Id) != null)
            {
                throw new FaultException("Order doesn't exits");
            }
            BroadcastMessage("UpdateOrder");
            //return _pharmacyDataService.UpdateOrder(order);
            return JsonConvert.SerializeObject(_pharmacyDataService.UpdateOrder(order));
        }

        public string GetOrderById(int id)
        {
            ConnectToDataService();
            var order = _pharmacyDataService.GetAllOrders().FirstOrDefault(x => x.Id == id);
            return JsonConvert.SerializeObject(order);
        }

        public string GetAllOrders()
        {
            ConnectToDataService();
            return JsonConvert.SerializeObject(_pharmacyDataService.GetAllOrders());
        }

        public string AddMedicine(string medicineJson)
        {
            ConnectToDataService();
            Medicine medicine = JsonConvert.DeserializeObject<Medicine>(medicineJson);
            if (medicine.Validate().Count() != 0)
            {
                throw new FaultException(string.Join(",", medicine.Validate()));
            }
            //if (GetMedicineById(medicine.Id) != null)
            //{
            //    throw new FaultException("Medicine already exits");
            //}
            medicine.Id = GetMedicineId();
            BroadcastMessage("UpdateMedicine");
            var medicine2 = _pharmacyDataService.AddMedicine(medicine);
            return JsonConvert.SerializeObject(medicine2);
        }

        public string UpdateMedicine(string medicineJson)
        {
            ConnectToDataService();
            Medicine medicine = JsonConvert.DeserializeObject<Medicine>(medicineJson);
            if (medicine.Validate().Count() != 0)
            {
                throw new FaultException(string.Join(",", medicine.Validate()));
            }
            if (GetMedicineById(medicine.Id) != null)
            {
                throw new FaultException("Medicine already exits");
            }
            BroadcastMessage("UpdateMedicine");
            return JsonConvert.SerializeObject(_pharmacyDataService.UpdateMedicine(medicine));
        }

        public string GetMedicineById(int id)
        {
            ConnectToDataService();
            var medicine = _pharmacyDataService.GetAllMedicines().FirstOrDefault(x => x.Id == id);
            return JsonConvert.SerializeObject(medicine);
        }

        public string GetAllMedicines()
        {
            ConnectToDataService();
            return JsonConvert.SerializeObject(_pharmacyDataService.GetAllMedicines());
        }

        private ConcurrentDictionary<Guid, INotifyServiceCallback> _clients =
          new ConcurrentDictionary<Guid, INotifyServiceCallback>();

        private void Subscribe(User user)
        {
            INotifyServiceCallback callback =
                OperationContext.Current.GetCallbackChannel<INotifyServiceCallback>();

            Guid clientId = Guid.NewGuid();

            if (callback != null)
            {
                lock (_clients)
                {
                    _clients.TryAdd(clientId, callback);
                    user.ClientGuid = clientId;
                }
            }

        }

        private int GetUserId()
        {
            var lastUser = _pharmacyDataService.GetAllUsers().OrderByDescending(x => x.Id).FirstOrDefault();
            int lastUserId = (int)DateTime.Now.Ticks;
            if (lastUser != null)
            {
                lastUserId = lastUser.Id;
            }
            int userId = lastUserId + 1;
            return userId;
        }


        private int GetOrderId()
        {
            var lastOrder = _pharmacyDataService.GetAllOrders().OrderByDescending(x => x.Id).FirstOrDefault();
            int lastOrderId = (int)DateTime.Now.Ticks;
            if (lastOrder != null)
            {
                lastOrderId = lastOrder.Id;
            }
            int orderId = lastOrderId + 1;
            return orderId;
        }
        private int GetMedicineId()
        {
            var lastMedicine = _pharmacyDataService.GetAllMedicines().OrderByDescending(x => x.Id).FirstOrDefault();
            int lastMedicineId = (int)DateTime.Now.Ticks;
            if (lastMedicine != null)
            {
                lastMedicineId = lastMedicine.Id;
            }
            int medicineId = lastMedicineId + 1;
            return medicineId;
        }


        /// <summary>
        /// Notifies the clients of messages.
        /// </summary>
        /// <param name="clientId">Identifies the client that sent the message.</param>
        /// <param name="message">The message to be sent to all connected clients.</param>
        private void BroadcastMessage(string message)
        {
            //// Call each client's callback method
            //ThreadPool.QueueUserWorkItem
            //(
            //    delegate
            //    {
            //        lock (_clients)
            //        {
            //            List<Guid> disconnectedClientGuids = new List<Guid>();

            //            foreach (KeyValuePair<Guid, INotifyServiceCallback> client in _clients)
            //            {
            //                try
            //                {
            //                    client.Value.HandleMessage(message);
            //                }
            //                catch (Exception)
            //                {
            //                    // TODO: Better to catch specific exception types.                     

            //                    // If a timeout exception occurred, it means that the server
            //                    // can't connect to the client. It might be because of a network
            //                    // error, or the client was closed  prematurely due to an exception or
            //                    // and was unable to unregister from the server. In any case, we 
            //                    // must remove the client from the list of clients.

            //                    // Another type of exception that might occur is that the communication
            //                    // object is aborted, or is closed.

            //                    // Mark the key for deletion. We will delete the client after the 
            //                    // for-loop because using foreach construct makes the clients collection
            //                    // non-modifiable while in the loop.
            //                    disconnectedClientGuids.Add(client.Key);
            //                }
            //            }

            //            foreach (Guid clientGuid in disconnectedClientGuids)
            //            {
            //                INotifyServiceCallback value;
            //                _clients.TryRemove(clientGuid, out value);
            //            }
            //        }
            //    }
            //);
        }


        private void CheckServiceState()
        {
            //if(_pharmacyDataService)
        }
    }
}
