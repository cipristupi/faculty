﻿using System.ServiceModel;
using HospitalPharmacy.Models;

namespace PharmacyServiceN
{
    [ServiceContract]
    public interface IPharmacyService
    {
        [OperationContract]
        string Login(string user);

        [OperationContract]
        bool Logout(User user);

        [OperationContract]
        string CreateUser(string user);

        [OperationContract]
        string UpdateUser(string user);

        [OperationContract]
        string GetUserById(int id);

        [OperationContract]
        string GetAllUsers();

        [OperationContract]
        string AddOrder(string order);

        [OperationContract]
        string UpdateOrder(string order);

        [OperationContract]
        string GetOrderById(int id);

        [OperationContract]
        string GetAllOrders();

        [OperationContract]
        string AddMedicine(string medicine);

        [OperationContract]
        string UpdateMedicine(string medicine);

        [OperationContract]
        string GetMedicineById(int id);

        [OperationContract]
        string GetAllMedicines();
    }


    /// <summary>
    /// The callback contract to be implemented by the client
    /// application.
    /// </summary>
    public interface INotifyServiceCallback
    {
        /// <summary>
        /// Implemented by the client so that the server may call
        /// this when it receives a message to be broadcasted.
        /// </summary>
        /// <param name="message">
        /// The message to broadcast.
        /// </param>
        [OperationContract(IsOneWay = true)]
        void HandleMessage(string message);
    }
}
