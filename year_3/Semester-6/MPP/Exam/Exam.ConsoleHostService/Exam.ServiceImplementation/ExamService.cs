﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Threading;
using System.Xml;
using Exam.Models;
using Exam.Repository;
using HospitalPharmacy.Utils;

namespace Exam.ServiceImplementation
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single,
      ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class ExamService : IExamService
    {
        public ExamService()
        {
            _serviceConfig = ReadServiceConfiguration();
        }
        ServiceConfig ReadServiceConfiguration()
        {
            List<ServiceConfig> userList = new List<ServiceConfig>();
            XmlDocument doc = new XmlDocument();
            doc.Load("ServiceConfig.xml");
            var elementsByTagName = doc.GetElementsByTagName(EntityProcessor.GetObjectName(new ServiceConfig()));
            XmlHelper<ServiceConfig> _xmlHelper = new XmlHelper<ServiceConfig>();
            foreach (var serviceConfiguration in elementsByTagName)
            {
                userList.Add(_xmlHelper.GetObjectFromXmlElement(serviceConfiguration as XmlElement));
            }
            return userList.FirstOrDefault();
        }

        public ExamService(ServiceConfig serviceConfig)
        {
            _serviceConfig = serviceConfig;
        }
        public User Login(User user)
        {
            var users = GetAllUsers().FirstOrDefault(x => x.Password == user.Password && x.Username == user.Username);
            if (users != null)
            {
                Subscribe(user);
                return users;
            }
            FaultException fe = new FaultException("Invalid user");
            throw fe;
        }

        public bool CreateUser(User user)
        {
            UserRepository userRepository = new UserRepository(_serviceConfig.ConnectionString);
            BroadcastMessage("Update");
            return userRepository.Add(user);
        }

        public bool UpdateUser(User user)
        {
            UserRepository userRepository = new UserRepository(_serviceConfig.ConnectionString);
            BroadcastMessage("Update");
            return userRepository.Update(user);
        }

        public IEnumerable<User> GetAllUsers()
        {
            UserRepository userRepository = new UserRepository(_serviceConfig.ConnectionString);
            return userRepository.GetAll();
        }

        public bool CreateParticipant(Participant participant)
        {
            ParticipantRepository participantRepository = new ParticipantRepository(_serviceConfig.ConnectionString);
            BroadcastMessage("Update");
            return participantRepository.Add(participant);
        }

        public bool UpdateParticipant(Participant participant)
        {
            ParticipantRepository participantRepository = new ParticipantRepository(_serviceConfig.ConnectionString);
            BroadcastMessage("Update");
            return participantRepository.Update(participant);
        }

        public IEnumerable<Participant> GetAllParticipants()
        {
            ParticipantRepository participantRepository = new ParticipantRepository(_serviceConfig.ConnectionString);
            return participantRepository.GetAll();
        }

        public bool CreateCheckpoint(Checkpoint checkpoint)
        {
            CheckpointRepository checkpointRepository = new CheckpointRepository(_serviceConfig.ConnectionString);
            BroadcastMessage("Update");
            return checkpointRepository.Add(checkpoint);
        }

        public bool UpdateCheckpoint(Checkpoint checkpoint)
        {
            CheckpointRepository checkpointRepository = new CheckpointRepository(_serviceConfig.ConnectionString);
            BroadcastMessage("Update");
            return checkpointRepository.Update(checkpoint);
        }

        public IEnumerable<Checkpoint> GetAllCheckpoints()
        {
            CheckpointRepository checkpointRepository = new CheckpointRepository(_serviceConfig.ConnectionString);
            BroadcastMessage("Update");
            return checkpointRepository.GetAll();
        }

        public bool CreateParticipantCheckpoint(ParticipantCheckpoint participantCheckpointcheckpoint)
        {
            ParticipantCheckpointRepository checkpointRepository = new ParticipantCheckpointRepository(_serviceConfig.ConnectionString);
            BroadcastMessage("Update");
            return checkpointRepository.Add(participantCheckpointcheckpoint);
        }

        public bool UpdateParticipantCheckpoint(ParticipantCheckpoint participantCheckpointcheckpoint)
        {
            ParticipantCheckpointRepository checkpointRepository = new ParticipantCheckpointRepository(_serviceConfig.ConnectionString);
            BroadcastMessage("Update");
            return checkpointRepository.Update(participantCheckpointcheckpoint);
        }

        public IEnumerable<ParticipantCheckpoint> GetAllParticipantCheckpoints()
        {
            ParticipantCheckpointRepository checkpointRepository = new ParticipantCheckpointRepository(_serviceConfig.ConnectionString);
            return checkpointRepository.GetAll();
        }

        public List<Participant> GetParticipantsByCheckpointId(int? checkpointId)
        {
            ParticipantRepository participantRepository = new ParticipantRepository(_serviceConfig.ConnectionString);
            ParticipantCheckpointRepository participantCheckpointRepository = new ParticipantCheckpointRepository(_serviceConfig.ConnectionString);

            var participansId =
                participantCheckpointRepository.GetAll()
                    .Where(x => x.CheckpointId == checkpointId)
                    .Select(x => x.ParticipantId);
            return participantRepository.GetAll().Where(x => participansId.Contains(x.Id)).ToList();
        }

        public IEnumerable<Checkpoint> GetCheckpointsForUser(int userId)
        {
            CheckpointRepository checkpointRepository = new CheckpointRepository(_serviceConfig.ConnectionString);
            return checkpointRepository.GetAll().Where(x => x.UserId == userId);
        }

        public IEnumerable<Participant> GetParticipantsCheckedByCheckointId(int? checkpointId, bool firstCheckpoint = false)
        {
            ParticipantRepository participantRepository = new ParticipantRepository(_serviceConfig.ConnectionString);
            ParticipantCheckpointRepository participantCheckpointRepository = new ParticipantCheckpointRepository(_serviceConfig.ConnectionString);

            IEnumerable<int> participansId;
            if (firstCheckpoint == false)
            {

                participansId = participantCheckpointRepository.GetAll()
                    .Where(x => x.CheckpointId == checkpointId && x.Checked == true)
                    .Select(x => x.ParticipantId);
            }
            else
            {
                participansId = participantCheckpointRepository.GetAll()
                   .Where(x => x.CheckpointId == checkpointId)
                   .Select(x => x.ParticipantId);
            }
            return participantRepository.GetAll().Where(x => participansId.Contains(x.Id)).ToList();
        }

        public void MarkAsChecked(int participantId, int? checkpointId)
        {
            ParticipantCheckpointRepository participantCheckpointRepository = new ParticipantCheckpointRepository(_serviceConfig.ConnectionString);
            var participantCheckpoint =
                participantCheckpointRepository.GetAll()
                    .FirstOrDefault(x => x.ParticipantId == participantId && x.CheckpointId == checkpointId);
            if (participantCheckpoint != null)
            {
                participantCheckpoint.Checked = true;
                participantCheckpoint.CheckInTime = DateTime.Now;
                participantCheckpointRepository.Update(participantCheckpoint);
            }
            BroadcastMessage("Update");
        }

        public IEnumerable<ParticipantWithTime> GetParticipantsWithTimeCheckedByCheckpointId(int? checkpointId)
        {
            ParticipantRepository participantRepository = new ParticipantRepository(_serviceConfig.ConnectionString);
            ParticipantCheckpointRepository participantCheckpointRepository = new ParticipantCheckpointRepository(_serviceConfig.ConnectionString);

            var participansId =
                participantCheckpointRepository.GetAll()
                    .Where(x => x.CheckpointId == checkpointId && x.Checked == true)
                    .Select(x => x.ParticipantId);
            var participants = participantRepository.GetAll().Where(x => participansId.Contains(x.Id)).ToList();
            List<ParticipantWithTime> participantWithTimes = new List<ParticipantWithTime>();
            foreach (Participant participant in participants)
            {
                var participantCheckPoint =
                    participantCheckpointRepository.GetAll().FirstOrDefault(x => x.ParticipantId == participant.Id);
                if (participantCheckPoint != null)
                {
                    participantWithTimes.Add(new ParticipantWithTime()
                    {
                        Id = participant.Id,
                        Name = participant.Name,
                        TimeOfCheckIn = (DateTime)participantCheckPoint.CheckInTime
                    });
                }
            }
            return participantWithTimes;
        }

        private ConcurrentDictionary<Guid, INotifyServiceCallback> _clients =
  new ConcurrentDictionary<Guid, INotifyServiceCallback>();

        private ServiceConfig _serviceConfig;

        private void Subscribe(User user)
        {
            INotifyServiceCallback callback =
                OperationContext.Current.GetCallbackChannel<INotifyServiceCallback>();

            Guid clientId = Guid.NewGuid();

            if (callback != null)
            {
                lock (_clients)
                {
                    _clients.TryAdd(clientId, callback);
                    user.ClientGuid = clientId;
                }
            }

        }


        /// <summary>
        /// Notifies the clients of messages.
        /// </summary>
        /// <param name="clientId">Identifies the client that sent the message.</param>
        /// <param name="message">The message to be sent to all connected clients.</param>
        private void BroadcastMessage(string message)
        {
            // Call each client's callback method
            ThreadPool.QueueUserWorkItem
            (
                delegate
                {
                    lock (_clients)
                    {
                        List<Guid> disconnectedClientGuids = new List<Guid>();

                        foreach (KeyValuePair<Guid, INotifyServiceCallback> client in _clients)
                        {
                            try
                            {
                                client.Value.HandleMessage(message);
                            }
                            catch (Exception)
                            {
                                // TODO: Better to catch specific exception types.                     

                                // If a timeout exception occurred, it means that the server
                                // can't connect to the client. It might be because of a network
                                // error, or the client was closed  prematurely due to an exception or
                                // and was unable to unregister from the server. In any case, we 
                                // must remove the client from the list of clients.

                                // Another type of exception that might occur is that the communication
                                // object is aborted, or is closed.

                                // Mark the key for deletion. We will delete the client after the 
                                // for-loop because using foreach construct makes the clients collection
                                // non-modifiable while in the loop.
                                disconnectedClientGuids.Add(client.Key);
                            }
                        }

                        foreach (Guid clientGuid in disconnectedClientGuids)
                        {
                            INotifyServiceCallback value;
                            _clients.TryRemove(clientGuid, out value);
                        }
                    }
                }
            );
        }

    }
}
