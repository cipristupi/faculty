﻿using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;
using Exam.Models;

namespace Exam.ServiceImplementation
{
    [ServiceContract(CallbackContract = typeof(INotifyServiceCallback), SessionMode = SessionMode.Allowed)]
    public interface IExamService
    {
        [OperationContract]
        User Login(User user);

        [OperationContract]
        bool CreateUser(User user);

        [OperationContract]
        bool UpdateUser(User user);

        [OperationContract]
        IEnumerable<User> GetAllUsers();

        [OperationContract]
        bool CreateParticipant(Participant participant);

        [OperationContract]
        bool UpdateParticipant(Participant participant);

        [OperationContract]
        IEnumerable<Participant> GetAllParticipants();

        [OperationContract]
        bool CreateCheckpoint(Checkpoint checkpoint);

        [OperationContract]
        bool UpdateCheckpoint(Checkpoint checkpoint);

        [OperationContract]
        IEnumerable<Checkpoint> GetAllCheckpoints();


        [OperationContract]
        bool CreateParticipantCheckpoint(ParticipantCheckpoint participantCheckpointcheckpoint);

        [OperationContract]
        bool UpdateParticipantCheckpoint(ParticipantCheckpoint participantCheckpointcheckpoint);

        [OperationContract]
        IEnumerable<ParticipantCheckpoint> GetAllParticipantCheckpoints();


        [OperationContract]
        List<Participant> GetParticipantsByCheckpointId(int? checkpointId);

        [OperationContract]
        IEnumerable<Checkpoint> GetCheckpointsForUser(int userId);

        [OperationContract]
        IEnumerable<Participant> GetParticipantsCheckedByCheckointId(int? checkpointId, bool firstCheckpoint = false);

        [OperationContract]
        void MarkAsChecked(int participantId, int? checkpointId);

        [OperationContract]
        IEnumerable<ParticipantWithTime> GetParticipantsWithTimeCheckedByCheckpointId(int? checkpointId);

    }
    /// <summary>
    /// The callback contract to be implemented by the client
    /// application.
    /// </summary>
    public interface INotifyServiceCallback
    {
        /// <summary>
        /// Implemented by the client so that the server may call
        /// this when it receives a message to be broadcasted.
        /// </summary>
        /// <param name="message">
        /// The message to broadcast.
        /// </param>
        [OperationContract(IsOneWay = true)]
        void HandleMessage(string message);
    }
}
