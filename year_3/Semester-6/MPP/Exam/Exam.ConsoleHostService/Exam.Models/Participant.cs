﻿using System.Data;
using System.Runtime.Serialization;

namespace Exam.Models
{
    [DataContract]
    public class Participant
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }

        public static Participant Read(IDataReader reader)
        {
            Participant participant = new Participant
            {
                Id = reader.GetInt32(0),
                Name = reader.GetString(1)
            };
            return participant;
        }
    }
}
