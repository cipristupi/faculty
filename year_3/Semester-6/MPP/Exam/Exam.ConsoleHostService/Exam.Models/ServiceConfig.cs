﻿namespace Exam.Models
{
    public class ServiceConfig
    {
        public string Protocol { get; set; }
        public string Port { get; set; }
        public string ServiceName { get; set; }

        public string ConnectionString { get; set; }
    }
}
