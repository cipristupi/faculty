﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Exam.Models
{
    [DataContract]
    public class ParticipantCheckpoint
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int ParticipantId { get; set; }
        [DataMember]
        public int CheckpointId { get; set; }
        [DataMember]
        public bool Checked { get; set; }
        [DataMember]
        public DateTime? CheckInTime { get; set; }

        public static ParticipantCheckpoint Read(IDataReader reader)
        {
            ParticipantCheckpoint participantCheckpoint = new ParticipantCheckpoint
            {
                Id = reader.GetInt32(0),
                ParticipantId = reader.GetInt32(1),
                CheckpointId = reader.GetInt32(2),
                Checked = reader.GetBoolean(3),
            };
            if (!reader.IsDBNull(4))
            {
                participantCheckpoint.CheckInTime = reader.GetDateTime(4);
            }
            return participantCheckpoint;
        }
    }
}
