﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Exam.Models
{
    [DataContract]
    public class Checkpoint
    {
        [DataMember]
        public int Id { get; set; }
        
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int? PreviousCheckpointId { get; set; }

        public static Checkpoint Read(IDataReader reader)
        {
            Checkpoint checkpoint = new Checkpoint
            {
                Id = reader.GetInt32(0),
                Name = reader.GetString(1),
                UserId = reader.GetInt32(2),
                //PreviousCheckpointId = reader.GetInt32(3)
            };
            if (!reader.IsDBNull(3))
            {
                checkpoint.PreviousCheckpointId = reader.GetInt32(3);
            }
            return checkpoint;
        }
    }
}
