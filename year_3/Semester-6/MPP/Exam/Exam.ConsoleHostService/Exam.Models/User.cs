﻿using System;
using System.Data;
using System.Runtime.Serialization;

namespace Exam.Models
{
    [DataContract]
    public class User
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Username { get; set; }
        [DataMember]
        public string Password { get; set; }
        [DataMember]
        public int Role { get; set; }

        [DataMember]
        public Guid ClientGuid { get; set; }

        public static User Read(IDataReader reader)
        {
            User user = new User
            {
                Id = reader.GetInt32(0),
                Username = reader.GetString(1),
                Password = reader.GetString(2),
                Role = reader.GetInt32(3)
            };
            return user;
        }
    }
}
