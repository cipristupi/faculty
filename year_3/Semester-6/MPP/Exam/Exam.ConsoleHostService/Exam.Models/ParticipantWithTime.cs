﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Exam.Models
{
    [DataContract]
    public class ParticipantWithTime : Participant
    {
        [DataMember]
        public DateTime TimeOfCheckIn { get; set; }
    }
}
