﻿using System.Collections.Generic;

namespace Exam.Repository
{
    interface IFileOperations<T> where T:class
    {
        IEnumerable<T> ReadFromFile(string filePath);
        void SaveToFile(IEnumerable<T> elementsList);
    }
}
