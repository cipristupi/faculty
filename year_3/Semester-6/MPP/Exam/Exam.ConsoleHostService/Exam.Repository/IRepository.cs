﻿using System.Collections.Generic;

namespace Exam.Repository
{
    public interface IRepository<T> where T : class
    {
        bool Add(T model);
        bool Update(T model);
        IEnumerable<T> GetAll();
        bool Delete(T model);
    }
}
