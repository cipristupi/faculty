﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Exam.Models;

namespace Exam.Repository
{
    public class ParticipantCheckpointRepository : IRepository<ParticipantCheckpoint>
    {
        string _connectionString;
        SqlCommand _cmd;
        SqlConnection _conn;
        SqlDataReader _reader;

        public ParticipantCheckpointRepository(string connectionString)
        {
            _connectionString = connectionString;
        }
        public bool Add(ParticipantCheckpoint model)
        {
            _conn = new SqlConnection(_connectionString);
            try
            {
                _conn.Open();
                _cmd = new SqlCommand("INSERT INTO [dbo].[Participants_CheckPoints] ([ParticipantId],[CheckpointId],[Checked],[CheckInTime])" +
                                     " VALUES (@participantId,@checkpointId,@checked,@checkInTimes)", _conn);
                _cmd.Parameters.Add(new SqlParameter("@participantId", model.ParticipantId));
                _cmd.Parameters.Add(new SqlParameter("@checkpointId", model.CheckpointId));
                _cmd.Parameters.Add(new SqlParameter("@checked", model.Checked));
                _cmd.Parameters.Add(new SqlParameter("@checkInTimes", model.CheckInTime));
                _cmd.ExecuteNonQuery();
                _conn.Close();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public bool Update(ParticipantCheckpoint model)
        {
            _conn = new SqlConnection(_connectionString);
            try
            {
                _conn.Open();
                _cmd = new SqlCommand("UPDATE Participants_CheckPoints SET" +
                                      " ParticipantId = @participantId," +
                                      " CheckpointId =@checkpointId," +
                                      " Checked =@checked," +
                                      " CheckInTime =@checkInTime WHERE Id =@participants_CheckPointsId", _conn);
                _cmd.Parameters.Add(new SqlParameter("@participantId", model.ParticipantId));
                _cmd.Parameters.Add(new SqlParameter("@checkpointId", model.CheckpointId));
                _cmd.Parameters.Add(new SqlParameter("@checked", model.Checked));
                _cmd.Parameters.Add(new SqlParameter("@checkInTime", model.CheckInTime));
                _cmd.Parameters.Add(new SqlParameter("@participants_CheckPointsId", model.Id));
                _cmd.ExecuteNonQuery();
                _conn.Close();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public IEnumerable<ParticipantCheckpoint> GetAll()
        {
            List<ParticipantCheckpoint> participantCheckpoints = new List<ParticipantCheckpoint>();
            _conn = new SqlConnection(_connectionString);
            try
            {
                _conn.Open();
                _cmd = new SqlCommand("SELECT Id , ParticipantId , CheckpointId, Checked, CheckInTime FROM [dbo].[Participants_CheckPoints] ORDER BY CheckInTime DESC", _conn);
                _reader = _cmd.ExecuteReader();
                while (_reader.Read())
                {
                    participantCheckpoints.Add(ParticipantCheckpoint.Read(_reader));
                }
                _conn.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return participantCheckpoints;
        }

        public bool Delete(ParticipantCheckpoint model)
        {
            throw new NotImplementedException();
        }
    }
}
