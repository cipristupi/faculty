﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Exam.Models;

namespace Exam.Repository
{
    public class CheckpointRepository : IRepository<Checkpoint>
    {
        string _connectionString;
        SqlCommand _cmd;
        SqlConnection _conn;
        SqlDataReader _reader;

        public CheckpointRepository(string connectionString)
        {
            _connectionString = connectionString;
        }
        public bool Add(Checkpoint model)
        {
            _conn = new SqlConnection(_connectionString);
            try
            {
                _conn.Open();
                _cmd = new SqlCommand("INSERT INTO [dbo].[Checkpoints] ([Name],[UserId],[PreviousCheckpointId])" +
                                     " VALUES (@name,@userId,@previousCheckpointId)", _conn);
                _cmd.Parameters.Add(new SqlParameter("@name", model.Name));
                _cmd.Parameters.Add(new SqlParameter("@userId", model.UserId));
                _cmd.Parameters.Add(new SqlParameter("@previousCheckpointId", model.PreviousCheckpointId));
                _cmd.ExecuteNonQuery();
                _conn.Close();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public bool Update(Checkpoint model)
        {
            _conn = new SqlConnection(_connectionString);
            try
            {
                _conn.Open();
                _cmd = new SqlCommand("UPDATE Checkpoint SET Name = @name, UserId =@userId, PreviousCheckpointId =@previousCheckpointId WHERE Id =@checkpointId", _conn);
                _cmd.Parameters.Add(new SqlParameter("@name", model.Name));
                _cmd.Parameters.Add(new SqlParameter("@userId", model.UserId));
                _cmd.Parameters.Add(new SqlParameter("@previousCheckpointId", model.PreviousCheckpointId));
                _cmd.Parameters.Add(new SqlParameter("@checkpointId", model.Id));
                _cmd.ExecuteNonQuery();
                _conn.Close();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public IEnumerable<Checkpoint> GetAll()
        {
            List<Checkpoint> checkpoints = new List<Checkpoint>();
            _conn = new SqlConnection(_connectionString);
            try
            {
                _conn.Open();
                _cmd = new SqlCommand("SELECT Id , Name, UserId, PreviousCheckpointId FROM [dbo].[Checkpoints] ", _conn);
                _reader = _cmd.ExecuteReader();
                while (_reader.Read())
                {
                    checkpoints.Add(Checkpoint.Read(_reader));
                }
                _conn.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return checkpoints;
        }

        public bool Delete(Checkpoint model)
        {
            throw new NotImplementedException();
        }
    }
}
