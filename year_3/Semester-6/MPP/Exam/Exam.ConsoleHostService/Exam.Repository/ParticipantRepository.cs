﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Exam.Models;

namespace Exam.Repository
{
    public class ParticipantRepository : IRepository<Participant>
    {
        string _connectionString;
        SqlCommand _cmd;
        SqlConnection _conn;
        SqlDataReader _reader;
        public ParticipantRepository(string connectionString)
        {
            _connectionString = connectionString;
        }
        public bool Add(Participant model)
        {
            _conn = new SqlConnection(_connectionString);
            try
            {
                _conn.Open();
                _cmd = new SqlCommand("INSERT INTO [dbo].[Participants] ([Name])" +
                                     " VALUES (@name)", _conn);
                _cmd.Parameters.Add(new SqlParameter("@name", model.Name));
                _cmd.ExecuteNonQuery();
                _conn.Close();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public bool Update(Participant model)
        {
            _conn = new SqlConnection(_connectionString);
            try
            {
                _conn.Open();
                _cmd = new SqlCommand("UPDATE Participants SET Name =@name WHERE Id =@participantId", _conn);
                _cmd.Parameters.Add(new SqlParameter("@name", model.Name));
                _cmd.Parameters.Add(new SqlParameter("@participantId", model.Id));
                _cmd.ExecuteNonQuery();
                _conn.Close();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public IEnumerable<Participant> GetAll()
        {
            List<Participant> participants = new List<Participant>();
            _conn = new SqlConnection(_connectionString);
            try
            {
                _conn.Open();
                _cmd = new SqlCommand("SELECT Id , Name FROM [dbo].[Participants] ", _conn);
                _reader = _cmd.ExecuteReader();
                while (_reader.Read())
                {
                    participants.Add(Participant.Read(_reader));
                }
                _conn.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return participants;
        }

        public bool Delete(Participant model)
        {
            throw new NotImplementedException();
        }
    }
}
