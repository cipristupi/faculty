﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Exam.Models;

namespace Exam.Repository
{
    public class UserRepository : IRepository<User>
    {
        string _connectionString;
        SqlCommand _cmd;
        SqlConnection _conn;
        SqlDataReader _reader;

        public UserRepository(string connectionString)
        {
            _connectionString = connectionString;
        }
        public bool Add(User model)
        {
            _conn = new SqlConnection(_connectionString);
            try
            {
                _conn.Open();
                _cmd = new SqlCommand("INSERT INTO [dbo].[Users] ([Username],[Password],[Role])" +
                                     " VALUES (@username,@password,@roleId)", _conn);
                _cmd.Parameters.Add(new SqlParameter("@username", model.Username));
                _cmd.Parameters.Add(new SqlParameter("@password", model.Password));
                _cmd.Parameters.Add(new SqlParameter("@roleId", model.Role));
                _cmd.ExecuteNonQuery();
                _conn.Close();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public bool Update(User model)
        {
            _conn = new SqlConnection(_connectionString);
            try
            {
                _conn.Open();
                _cmd = new SqlCommand("UPDATE Users SET Username =@username, Password =@password, Role =@roleId WHERE Id =@userId", _conn);
                _cmd.Parameters.Add(new SqlParameter("@username", model.Username));
                _cmd.Parameters.Add(new SqlParameter("@password", model.Password));
                _cmd.Parameters.Add(new SqlParameter("@roleId", model.Role));
                _cmd.Parameters.Add(new SqlParameter("@userId", model.Id));
                _cmd.ExecuteNonQuery();
                _conn.Close();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public IEnumerable<User> GetAll()
        {
            List<User> userList = new List<User>();
            _conn = new SqlConnection(_connectionString);
            try
            {
                _conn.Open();
                _cmd = new SqlCommand("SELECT Id , Username, Password, Role FROM [dbo].[Users] ", _conn);
                _reader = _cmd.ExecuteReader();
                while (_reader.Read())
                {
                    userList.Add(User.Read(_reader));
                }
                _conn.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return userList;
        }

        public bool Delete(User model)
        {
            throw new NotImplementedException();
        }
    }
}
