﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Exam.Models;
using Exam.ServiceImplementation;
using HospitalPharmacy.Utils;

namespace Exam.ConsoleHostService
{
    class Program
    {
        static ServiceHost m_svcHost = null;

        static void Main(string[] args)
        {
            //if (args.Count() != 2)
            //{
            //    Console.WriteLine("Insufficient arguments supplied");
            //    Console.WriteLine("Service Host must be started as <Executable Name> <TCP/HTTP> <Port#>");
            //    Console.WriteLine("Argument 1 >> Specifying the Binding, which binding will be used to Host (Supported values are either HTTP or TCP) without quotes");
            //    Console.WriteLine("Argument 2 >> Port Number a numeric value, the port you want to use");
            //    Console.WriteLine("\nExamples");
            //    Console.WriteLine("<Executable Name> TCP 9001");
            //    Console.WriteLine("<Executable Name> TCP 8001");
            //    Console.WriteLine("<Executable Name> HTTP 8001");
            //    Console.WriteLine("<Executable Name> HTTP 9001");

            //    return;
            //}
            ServiceConfig serviceConfig = ReadServiceConfiguration();

            string strBinding = serviceConfig.Protocol.ToUpper();
            bool bSuccess = ((strBinding == "TCP") || (strBinding == "HTTP"));
            if (bSuccess == false)
            {
                Console.WriteLine("\nBinding argument is invalid, should be either TCP or HTTP)");
                return;
            }
            int nPort = 0;
            bSuccess = int.TryParse(serviceConfig.Port, out nPort);
            if (bSuccess == false)
            {
                Console.WriteLine("\nPort number must be a numeric value");
                return;
            }

            bool bindingTCP = (strBinding == "TCP");
            if (bindingTCP) StartTCPService(nPort, serviceConfig.ServiceName); else StartHTTPService(nPort, serviceConfig.ServiceName);
            if (m_svcHost != null)
            {
                Console.WriteLine("\nPress any key to close the Service");
                Console.ReadKey();
                StopService();
            }
            Console.ReadLine();
        }

        private static void StartTCPService(int nPort, string serviceName)
        {
            string strAdr = "net.tcp://localhost:" + nPort + $"/{serviceName}/";
            try
            {
                Uri adrbase = new Uri(strAdr);
                m_svcHost = new ServiceHost(typeof(ExamService), adrbase);

                ServiceMetadataBehavior mBehave = new ServiceMetadataBehavior();
                m_svcHost.Description.Behaviors.Add(mBehave);
                m_svcHost.AddServiceEndpoint(typeof(IMetadataExchange), MetadataExchangeBindings.CreateMexTcpBinding(), "mex");

                NetTcpBinding tcpb = new NetTcpBinding();
                m_svcHost.AddServiceEndpoint(typeof(IExamService), tcpb, strAdr);
                m_svcHost.Open();
                Console.WriteLine("\n\nService is Running as >> " + strAdr);
            }
            catch (Exception eX)
            {
                m_svcHost = null;
                Console.WriteLine("Service can not be started as >> [" + strAdr + "] \n\nError Message [" + eX.Message + "]");
            }
        }

        private static void StartHTTPService(int nPort, string serviceName)
        {
            string strAdr = "http://localhost:" + nPort.ToString() + $"/{serviceName}";
            try
            {
                Uri adrbase = new Uri(strAdr);
                m_svcHost = new ServiceHost(typeof(ExamService), adrbase);

                ServiceMetadataBehavior mBehave = new ServiceMetadataBehavior();
                m_svcHost.Description.Behaviors.Add(mBehave);
                m_svcHost.AddServiceEndpoint(typeof(IMetadataExchange), MetadataExchangeBindings.CreateMexHttpBinding(), "mex");

                BasicHttpBinding httpb = new BasicHttpBinding();
                m_svcHost.AddServiceEndpoint(typeof(IExamService), httpb, strAdr);
                m_svcHost.Open();
                Console.WriteLine("\n\nService is Running as >> " + strAdr);
            }
            catch (Exception eX)
            {
                m_svcHost = null;
                Console.WriteLine("Service can not be started as >> [" + strAdr + "] \n\nError Message [" + eX.Message + "]");
            }
        }

        private static ServiceConfig ReadServiceConfiguration()
        {
            List<ServiceConfig> userList = new List<ServiceConfig>();
            XmlDocument doc = new XmlDocument();
            doc.Load("ServiceConfig.xml");
            var elementsByTagName = doc.GetElementsByTagName(EntityProcessor.GetObjectName(new ServiceConfig()));
            XmlHelper<ServiceConfig> _xmlHelper = new XmlHelper<ServiceConfig>();
            foreach (var serviceConfiguration in elementsByTagName)
            {
                userList.Add(_xmlHelper.GetObjectFromXmlElement(serviceConfiguration as XmlElement));
            }
            return userList.FirstOrDefault();
        }

        private static void StopService()
        {
            if (m_svcHost != null)
            {
                m_svcHost.Close();
                m_svcHost = null;
            }
        }
    }
}
