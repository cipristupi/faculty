﻿namespace Exam.Client
{
    partial class Organizer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.participantsGrid = new System.Windows.Forms.DataGridView();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.checkedParticipantsGrid = new System.Windows.Forms.DataGridView();
            this.button3 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.participantsGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkedParticipantsGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // participantsGrid
            // 
            this.participantsGrid.AllowUserToAddRows = false;
            this.participantsGrid.AllowUserToDeleteRows = false;
            this.participantsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.participantsGrid.Location = new System.Drawing.Point(13, 49);
            this.participantsGrid.Name = "participantsGrid";
            this.participantsGrid.ReadOnly = true;
            this.participantsGrid.Size = new System.Drawing.Size(381, 263);
            this.participantsGrid.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(13, 343);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(123, 55);
            this.button1.TabIndex = 1;
            this.button1.Text = "Load Participants";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(156, 343);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(139, 55);
            this.button2.TabIndex = 2;
            this.button2.Text = "Checked ";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // checkedParticipantsGrid
            // 
            this.checkedParticipantsGrid.AllowUserToAddRows = false;
            this.checkedParticipantsGrid.AllowUserToDeleteRows = false;
            this.checkedParticipantsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.checkedParticipantsGrid.Location = new System.Drawing.Point(462, 49);
            this.checkedParticipantsGrid.Name = "checkedParticipantsGrid";
            this.checkedParticipantsGrid.ReadOnly = true;
            this.checkedParticipantsGrid.Size = new System.Drawing.Size(381, 263);
            this.checkedParticipantsGrid.TabIndex = 3;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(462, 343);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(117, 55);
            this.button3.TabIndex = 4;
            this.button3.Text = "Load Checked Participants";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 25);
            this.label1.TabIndex = 5;
            this.label1.Text = "label1";
            // 
            // Organizer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(909, 451);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.checkedParticipantsGrid);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.participantsGrid);
            this.Name = "Organizer";
            this.Text = "Organizer";
            this.Load += new System.EventHandler(this.Organizer_Load);
            ((System.ComponentModel.ISupportInitialize)(this.participantsGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkedParticipantsGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView participantsGrid;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataGridView checkedParticipantsGrid;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label1;
    }
}