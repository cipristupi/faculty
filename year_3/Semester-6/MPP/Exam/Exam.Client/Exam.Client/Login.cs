﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Exam.Models;
using Exam.ServiceImplementation;

namespace Exam.Client
{
    public partial class Login : Form
    {
        private IExamService _examService;
        public Login()
        {
            InitializeComponent();
        }

        private void login_bttn_Click(object sender, EventArgs e)
        {
            CreateConnetionToService();
            string username = username_txtbox.Text;
            string password = password_txtbox.Text;
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
            {
                MessageBox.Show("Please fill all fields");
            }
            else
            {
                User user = new User()
                {
                    Password = password,
                    Username = username
                };
                var serviceUser = _examService.Login(user);
                if (serviceUser == null)
                {
                    MessageBox.Show("Invalid user or password");
                    return;
                }
                switch (serviceUser.Role)
                {
                    case 1:
                        Admin adminForm = new Admin(serviceUser);
                        this.Hide();
                        adminForm.Show();
                        break;
                    case 2:
                        Organizer doctorForm = new Organizer(serviceUser);
                        this.Hide();
                        doctorForm.Show();
                        break;
                    
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        private void CreateConnetionToService()
        {
            ChannelFactory<IExamService> factory = null;
            try
            {
                NotifyServiceCallback notifyServiceCallback = new NotifyServiceCallback();
                InstanceContext site = new InstanceContext(notifyServiceCallback);
                var binding = new NetTcpBinding(); //unsecureNetTcpBinding
                EndpointAddress address = new EndpointAddress("net.tcp://localhost:1235/ExamService");
                DuplexChannelFactory<IExamService> cf = new DuplexChannelFactory<IExamService>(site, binding);
                _examService = cf.CreateChannel(address);
            }
            catch (CommunicationException communicationException)
            {
                MessageBox.Show(communicationException.ToString());
                if (factory != null)
                {
                    factory.Abort();
                }
            }
            catch (TimeoutException timeoutException)
            {
                MessageBox.Show(timeoutException.ToString());
                if (factory != null)
                {
                    factory.Abort();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                if (factory != null)
                {
                    factory.Abort();
                }
            }
        }
    }
}
