﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Exam.Models;
using Exam.ServiceImplementation;

namespace Exam.Client
{
    public partial class Organizer : Form
    {
        private User loggedUser;
        private IExamService _examService;
        public delegate void ClientNotifiedEventHandler(object sender, ClientNotifiedEventArgs e);
        private User _loggedUser;
        public Organizer()
        {
            InitializeComponent();
        }

        public Organizer(User loggedUser)
        {
            _loggedUser = loggedUser;
            InitializeComponent();
        }


        //Load Participants
        private void button1_Click(object sender, EventArgs e)
        {
            LoadParticipantsPrevious();
        }

        private void LoadParticipantsPrevious()
        {
            var checkpoints = _examService.GetCheckpointsForUser(_loggedUser.Id);
            var oneCheckpoint = checkpoints.FirstOrDefault();
            if (oneCheckpoint != null)
            {
                var checkpointId = oneCheckpoint.PreviousCheckpointId == null
                    ? oneCheckpoint.Id
                    : oneCheckpoint.PreviousCheckpointId;
                var firstCheckpoint = oneCheckpoint.PreviousCheckpointId == null;
                var currentCheckedParticipats = _examService.GetParticipantsCheckedByCheckointId(oneCheckpoint.Id, firstCheckpoint).Select(x => x.Id);

                //IEnumerable<Participant> participantsFromPreviousCheckpoint;
                //if (!firstCheckpoint)
                //{
                //    participantsFromPreviousCheckpoint = _examService.GetParticipantsCheckedByCheckointId(checkpointId,
                //        firstCheckpoint)
                //        .Where(x => !currentCheckedParticipats.Contains(x.Id));
                //}
                //else
                //{
                //    participantsFromPreviousCheckpoint =
                //    _examService.GetParticipantsCheckedByCheckointId(checkpointId, firstCheckpoint);
                //}

                var participantsFromPreviousCheckpoint =
                    _examService.GetParticipantsCheckedByCheckointId(checkpointId, firstCheckpoint);

                participantsGrid.DataSource = null;
                participantsGrid.DataSource = participantsFromPreviousCheckpoint;
            }
        }

        private void CreateConnetionToService()
        {
            ChannelFactory<IExamService> factory = null;
            try
            {
                NotifyServiceCallback notifyServiceCallback = new NotifyServiceCallback();
                InstanceContext site = new InstanceContext(notifyServiceCallback);
                var binding = new NetTcpBinding(); //unsecureNetTcpBinding
                EndpointAddress address = new EndpointAddress("net.tcp://localhost:1235/ExamService");
                DuplexChannelFactory<IExamService> cf = new DuplexChannelFactory<IExamService>(site, binding);
                _examService = cf.CreateChannel(address);
            }
            catch (CommunicationException communicationException)
            {
                MessageBox.Show(communicationException.ToString());
                if (factory != null)
                {
                    factory.Abort();
                }
            }
            catch (TimeoutException timeoutException)
            {
                MessageBox.Show(timeoutException.ToString());
                if (factory != null)
                {
                    factory.Abort();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                if (factory != null)
                {
                    factory.Abort();
                }
            }
        }

        private void Organizer_Load(object sender, EventArgs e)
        {
            MyActiveForm.CurrentForm = this;
            if (_loggedUser != null)
            {
                label1.Text = _loggedUser.Username;
            }
            CreateConnetionToService();
        }

        //Checked person with time
        private void button3_Click(object sender, EventArgs e)
        {
            LoadCheckedInParticipantsWithTime();
        }

        private void LoadCheckedInParticipantsWithTime()
        {
            var checkpoints = _examService.GetCheckpointsForUser(_loggedUser.Id);
            var oneCheckpoint = checkpoints.FirstOrDefault();
            if (oneCheckpoint != null)
            {
                var participantsFromPreviousCheckpoint =
                    _examService.GetParticipantsWithTimeCheckedByCheckpointId(oneCheckpoint.Id);
                checkedParticipantsGrid.DataSource = null;
                checkedParticipantsGrid.DataSource = participantsFromPreviousCheckpoint;
            }
        }

        //MArk as checked
        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                var selectedRow = participantsGrid.SelectedRows;
                int participantId = int.Parse(selectedRow[0].Cells[0].Value.ToString());
                var checkpoints = _examService.GetCheckpointsForUser(_loggedUser.Id);
                var oneCheckpoint = checkpoints.FirstOrDefault();
                if (oneCheckpoint != null)
                {
                    _examService.MarkAsChecked(participantId, oneCheckpoint.Id);
                }
                LoadParticipantsPrevious();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.ToString());
            }
        }


        public void Notify(object message)
        {
            if (message.ToString() == "Update")
            {
                CreateConnetionToService();
                LoadParticipantsPrevious();
                LoadCheckedInParticipantsWithTime();
            }
        }
    }
}
