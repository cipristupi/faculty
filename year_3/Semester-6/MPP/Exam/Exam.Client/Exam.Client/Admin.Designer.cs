﻿namespace Exam.Client
{
    partial class Admin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.updateOrder_quantity_txtbox = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.updateOrder_bttn = new System.Windows.Forms.Button();
            this.updateOrder_status_cmbbox = new System.Windows.Forms.ComboBox();
            this.updateOrder_userName_txtbox = new System.Windows.Forms.TextBox();
            this.updateOrder_medicineName_txtbox = new System.Windows.Forms.TextBox();
            this.updateOrder_medicineId_txtbox = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.loadOrders_bttn = new System.Windows.Forms.Button();
            this.orders_dataGridView = new System.Windows.Forms.DataGridView();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.createOrder_quantity_txtbox = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.createOrder_bttn = new System.Windows.Forms.Button();
            this.createOrder_status_cmbbox = new System.Windows.Forms.ComboBox();
            this.createOrder_userName_txtbox = new System.Windows.Forms.TextBox();
            this.createOrder_medicineName_txtbox = new System.Windows.Forms.TextBox();
            this.createOrder_medicineId_txtbox = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.loadMedicines_bttn = new System.Windows.Forms.Button();
            this.organiser_dataGridView = new System.Windows.Forms.DataGridView();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.updateOrganiser_id_txtbox = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.updateOrganiser_bttn = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.createOrganiser_bttn = new System.Windows.Forms.Button();
            this.createOrganiser_name_txtbox = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.loadUsers_bttn = new System.Windows.Forms.Button();
            this.users_dataGridView = new System.Windows.Forms.DataGridView();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.updateUser_userId_txtbox = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.updateUser_bttn = new System.Windows.Forms.Button();
            this.updateUser_userType_cmbbox = new System.Windows.Forms.ComboBox();
            this.updateUser_password_txtbox = new System.Windows.Forms.TextBox();
            this.updateUser_username_txtbox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.createUser_bttn = new System.Windows.Forms.Button();
            this.createUser_userType_cmbbox = new System.Windows.Forms.ComboBox();
            this.createUser_password_txtbox = new System.Windows.Forms.TextBox();
            this.createUser_username_txtbox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage3.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.orders_dataGridView)).BeginInit();
            this.groupBox9.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.organiser_dataGridView)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.users_dataGridView)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.groupBox8);
            this.tabPage3.Controls.Add(this.groupBox7);
            this.tabPage3.Controls.Add(this.groupBox9);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(924, 465);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Orders";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.updateOrder_quantity_txtbox);
            this.groupBox8.Controls.Add(this.label11);
            this.groupBox8.Controls.Add(this.updateOrder_bttn);
            this.groupBox8.Controls.Add(this.updateOrder_status_cmbbox);
            this.groupBox8.Controls.Add(this.updateOrder_userName_txtbox);
            this.groupBox8.Controls.Add(this.updateOrder_medicineName_txtbox);
            this.groupBox8.Controls.Add(this.updateOrder_medicineId_txtbox);
            this.groupBox8.Controls.Add(this.label12);
            this.groupBox8.Controls.Add(this.label13);
            this.groupBox8.Controls.Add(this.label15);
            this.groupBox8.Controls.Add(this.label16);
            this.groupBox8.Location = new System.Drawing.Point(8, 292);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(249, 258);
            this.groupBox8.TabIndex = 4;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Update Order";
            // 
            // updateOrder_quantity_txtbox
            // 
            this.updateOrder_quantity_txtbox.Location = new System.Drawing.Point(100, 125);
            this.updateOrder_quantity_txtbox.Name = "updateOrder_quantity_txtbox";
            this.updateOrder_quantity_txtbox.Size = new System.Drawing.Size(100, 20);
            this.updateOrder_quantity_txtbox.TabIndex = 20;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(11, 128);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(46, 13);
            this.label11.TabIndex = 19;
            this.label11.Text = "Quantity";
            // 
            // updateOrder_bttn
            // 
            this.updateOrder_bttn.Location = new System.Drawing.Point(64, 203);
            this.updateOrder_bttn.Name = "updateOrder_bttn";
            this.updateOrder_bttn.Size = new System.Drawing.Size(121, 37);
            this.updateOrder_bttn.TabIndex = 18;
            this.updateOrder_bttn.Text = "Update";
            this.updateOrder_bttn.UseVisualStyleBackColor = true;
            // 
            // updateOrder_status_cmbbox
            // 
            this.updateOrder_status_cmbbox.FormattingEnabled = true;
            this.updateOrder_status_cmbbox.Location = new System.Drawing.Point(100, 157);
            this.updateOrder_status_cmbbox.Name = "updateOrder_status_cmbbox";
            this.updateOrder_status_cmbbox.Size = new System.Drawing.Size(121, 21);
            this.updateOrder_status_cmbbox.TabIndex = 8;
            // 
            // updateOrder_userName_txtbox
            // 
            this.updateOrder_userName_txtbox.Location = new System.Drawing.Point(100, 92);
            this.updateOrder_userName_txtbox.Name = "updateOrder_userName_txtbox";
            this.updateOrder_userName_txtbox.Size = new System.Drawing.Size(100, 20);
            this.updateOrder_userName_txtbox.TabIndex = 7;
            // 
            // updateOrder_medicineName_txtbox
            // 
            this.updateOrder_medicineName_txtbox.Location = new System.Drawing.Point(100, 59);
            this.updateOrder_medicineName_txtbox.Name = "updateOrder_medicineName_txtbox";
            this.updateOrder_medicineName_txtbox.Size = new System.Drawing.Size(100, 20);
            this.updateOrder_medicineName_txtbox.TabIndex = 6;
            // 
            // updateOrder_medicineId_txtbox
            // 
            this.updateOrder_medicineId_txtbox.Location = new System.Drawing.Point(100, 26);
            this.updateOrder_medicineId_txtbox.Name = "updateOrder_medicineId_txtbox";
            this.updateOrder_medicineId_txtbox.Size = new System.Drawing.Size(100, 20);
            this.updateOrder_medicineId_txtbox.TabIndex = 5;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(11, 160);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(37, 13);
            this.label12.TabIndex = 4;
            this.label12.Text = "Status";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(11, 95);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(60, 13);
            this.label13.TabIndex = 2;
            this.label13.Text = "User Name";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(11, 62);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(81, 13);
            this.label15.TabIndex = 1;
            this.label15.Text = "Medicine Name";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(11, 29);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(62, 13);
            this.label16.TabIndex = 0;
            this.label16.Text = "Medicine Id";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.loadOrders_bttn);
            this.groupBox7.Controls.Add(this.orders_dataGridView);
            this.groupBox7.Location = new System.Drawing.Point(275, 16);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(699, 495);
            this.groupBox7.TabIndex = 5;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Orders";
            // 
            // loadOrders_bttn
            // 
            this.loadOrders_bttn.Location = new System.Drawing.Point(15, 443);
            this.loadOrders_bttn.Name = "loadOrders_bttn";
            this.loadOrders_bttn.Size = new System.Drawing.Size(138, 39);
            this.loadOrders_bttn.TabIndex = 1;
            this.loadOrders_bttn.Text = "Load Orders";
            this.loadOrders_bttn.UseVisualStyleBackColor = true;
            // 
            // orders_dataGridView
            // 
            this.orders_dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.orders_dataGridView.Location = new System.Drawing.Point(15, 19);
            this.orders_dataGridView.Name = "orders_dataGridView";
            this.orders_dataGridView.ReadOnly = true;
            this.orders_dataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.orders_dataGridView.Size = new System.Drawing.Size(678, 407);
            this.orders_dataGridView.TabIndex = 0;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.createOrder_quantity_txtbox);
            this.groupBox9.Controls.Add(this.label22);
            this.groupBox9.Controls.Add(this.createOrder_bttn);
            this.groupBox9.Controls.Add(this.createOrder_status_cmbbox);
            this.groupBox9.Controls.Add(this.createOrder_userName_txtbox);
            this.groupBox9.Controls.Add(this.createOrder_medicineName_txtbox);
            this.groupBox9.Controls.Add(this.createOrder_medicineId_txtbox);
            this.groupBox9.Controls.Add(this.label18);
            this.groupBox9.Controls.Add(this.label19);
            this.groupBox9.Controls.Add(this.label20);
            this.groupBox9.Controls.Add(this.label21);
            this.groupBox9.Location = new System.Drawing.Point(8, 16);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(249, 258);
            this.groupBox9.TabIndex = 3;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Create Order";
            // 
            // createOrder_quantity_txtbox
            // 
            this.createOrder_quantity_txtbox.Location = new System.Drawing.Point(100, 125);
            this.createOrder_quantity_txtbox.Name = "createOrder_quantity_txtbox";
            this.createOrder_quantity_txtbox.Size = new System.Drawing.Size(100, 20);
            this.createOrder_quantity_txtbox.TabIndex = 20;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(11, 128);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(46, 13);
            this.label22.TabIndex = 19;
            this.label22.Text = "Quantity";
            // 
            // createOrder_bttn
            // 
            this.createOrder_bttn.Location = new System.Drawing.Point(64, 198);
            this.createOrder_bttn.Name = "createOrder_bttn";
            this.createOrder_bttn.Size = new System.Drawing.Size(121, 37);
            this.createOrder_bttn.TabIndex = 18;
            this.createOrder_bttn.Text = "Create";
            this.createOrder_bttn.UseVisualStyleBackColor = true;
            // 
            // createOrder_status_cmbbox
            // 
            this.createOrder_status_cmbbox.FormattingEnabled = true;
            this.createOrder_status_cmbbox.Location = new System.Drawing.Point(100, 157);
            this.createOrder_status_cmbbox.Name = "createOrder_status_cmbbox";
            this.createOrder_status_cmbbox.Size = new System.Drawing.Size(121, 21);
            this.createOrder_status_cmbbox.TabIndex = 8;
            // 
            // createOrder_userName_txtbox
            // 
            this.createOrder_userName_txtbox.Location = new System.Drawing.Point(100, 92);
            this.createOrder_userName_txtbox.Name = "createOrder_userName_txtbox";
            this.createOrder_userName_txtbox.Size = new System.Drawing.Size(100, 20);
            this.createOrder_userName_txtbox.TabIndex = 7;
            // 
            // createOrder_medicineName_txtbox
            // 
            this.createOrder_medicineName_txtbox.Location = new System.Drawing.Point(100, 59);
            this.createOrder_medicineName_txtbox.Name = "createOrder_medicineName_txtbox";
            this.createOrder_medicineName_txtbox.Size = new System.Drawing.Size(100, 20);
            this.createOrder_medicineName_txtbox.TabIndex = 6;
            // 
            // createOrder_medicineId_txtbox
            // 
            this.createOrder_medicineId_txtbox.Location = new System.Drawing.Point(100, 26);
            this.createOrder_medicineId_txtbox.Name = "createOrder_medicineId_txtbox";
            this.createOrder_medicineId_txtbox.Size = new System.Drawing.Size(100, 20);
            this.createOrder_medicineId_txtbox.TabIndex = 5;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(11, 160);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(37, 13);
            this.label18.TabIndex = 4;
            this.label18.Text = "Status";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(11, 95);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(60, 13);
            this.label19.TabIndex = 2;
            this.label19.Text = "User Name";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(11, 62);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(81, 13);
            this.label20.TabIndex = 1;
            this.label20.Text = "Medicine Name";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(11, 29);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(62, 13);
            this.label21.TabIndex = 0;
            this.label21.Text = "Medicine Id";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox4);
            this.tabPage2.Controls.Add(this.groupBox5);
            this.tabPage2.Controls.Add(this.groupBox6);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(924, 465);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Participant";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.loadMedicines_bttn);
            this.groupBox4.Controls.Add(this.organiser_dataGridView);
            this.groupBox4.Location = new System.Drawing.Point(275, 19);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(699, 495);
            this.groupBox4.TabIndex = 5;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Participant";
            // 
            // loadMedicines_bttn
            // 
            this.loadMedicines_bttn.Location = new System.Drawing.Point(15, 399);
            this.loadMedicines_bttn.Name = "loadMedicines_bttn";
            this.loadMedicines_bttn.Size = new System.Drawing.Size(138, 39);
            this.loadMedicines_bttn.TabIndex = 1;
            this.loadMedicines_bttn.Text = "Load Participants";
            this.loadMedicines_bttn.UseVisualStyleBackColor = true;
            this.loadMedicines_bttn.Click += new System.EventHandler(this.loadMedicines_bttn_Click);
            // 
            // organiser_dataGridView
            // 
            this.organiser_dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.organiser_dataGridView.Location = new System.Drawing.Point(15, 19);
            this.organiser_dataGridView.Name = "organiser_dataGridView";
            this.organiser_dataGridView.ReadOnly = true;
            this.organiser_dataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.organiser_dataGridView.Size = new System.Drawing.Size(678, 371);
            this.organiser_dataGridView.TabIndex = 0;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.textBox1);
            this.groupBox5.Controls.Add(this.label3);
            this.groupBox5.Controls.Add(this.updateOrganiser_id_txtbox);
            this.groupBox5.Controls.Add(this.label10);
            this.groupBox5.Controls.Add(this.updateOrganiser_bttn);
            this.groupBox5.Location = new System.Drawing.Point(8, 175);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(209, 135);
            this.groupBox5.TabIndex = 4;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Update Participant";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(80, 57);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 21;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(22, 60);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 20;
            this.label3.Text = "Name";
            // 
            // updateOrganiser_id_txtbox
            // 
            this.updateOrganiser_id_txtbox.Location = new System.Drawing.Point(91, 25);
            this.updateOrganiser_id_txtbox.Name = "updateOrganiser_id_txtbox";
            this.updateOrganiser_id_txtbox.ReadOnly = true;
            this.updateOrganiser_id_txtbox.Size = new System.Drawing.Size(100, 20);
            this.updateOrganiser_id_txtbox.TabIndex = 19;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(7, 25);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(64, 13);
            this.label10.TabIndex = 18;
            this.label10.Text = "Organiser Id";
            // 
            // updateOrganiser_bttn
            // 
            this.updateOrganiser_bttn.Location = new System.Drawing.Point(44, 92);
            this.updateOrganiser_bttn.Name = "updateOrganiser_bttn";
            this.updateOrganiser_bttn.Size = new System.Drawing.Size(121, 37);
            this.updateOrganiser_bttn.TabIndex = 17;
            this.updateOrganiser_bttn.Text = "Update";
            this.updateOrganiser_bttn.UseVisualStyleBackColor = true;
            this.updateOrganiser_bttn.Click += new System.EventHandler(this.updateOrganiser_bttn_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.createOrganiser_bttn);
            this.groupBox6.Controls.Add(this.createOrganiser_name_txtbox);
            this.groupBox6.Controls.Add(this.label17);
            this.groupBox6.Location = new System.Drawing.Point(8, 19);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(191, 131);
            this.groupBox6.TabIndex = 3;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Create Participant";
            // 
            // createOrganiser_bttn
            // 
            this.createOrganiser_bttn.Location = new System.Drawing.Point(25, 75);
            this.createOrganiser_bttn.Name = "createOrganiser_bttn";
            this.createOrganiser_bttn.Size = new System.Drawing.Size(121, 37);
            this.createOrganiser_bttn.TabIndex = 18;
            this.createOrganiser_bttn.Text = "Create";
            this.createOrganiser_bttn.UseVisualStyleBackColor = true;
            this.createOrganiser_bttn.Click += new System.EventHandler(this.createOrganiser_bttn_Click);
            // 
            // createOrganiser_name_txtbox
            // 
            this.createOrganiser_name_txtbox.Location = new System.Drawing.Point(65, 25);
            this.createOrganiser_name_txtbox.Name = "createOrganiser_name_txtbox";
            this.createOrganiser_name_txtbox.Size = new System.Drawing.Size(100, 20);
            this.createOrganiser_name_txtbox.TabIndex = 6;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(7, 28);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(35, 13);
            this.label17.TabIndex = 1;
            this.label17.Text = "Name";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(924, 465);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Users";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.loadUsers_bttn);
            this.groupBox3.Controls.Add(this.users_dataGridView);
            this.groupBox3.Location = new System.Drawing.Point(290, 23);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(699, 495);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Users";
            // 
            // loadUsers_bttn
            // 
            this.loadUsers_bttn.Location = new System.Drawing.Point(15, 388);
            this.loadUsers_bttn.Name = "loadUsers_bttn";
            this.loadUsers_bttn.Size = new System.Drawing.Size(138, 39);
            this.loadUsers_bttn.TabIndex = 1;
            this.loadUsers_bttn.Text = "Load Users";
            this.loadUsers_bttn.UseVisualStyleBackColor = true;
            this.loadUsers_bttn.Click += new System.EventHandler(this.loadUsers_bttn_Click);
            // 
            // users_dataGridView
            // 
            this.users_dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.users_dataGridView.Location = new System.Drawing.Point(15, 19);
            this.users_dataGridView.Name = "users_dataGridView";
            this.users_dataGridView.ReadOnly = true;
            this.users_dataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.users_dataGridView.Size = new System.Drawing.Size(678, 354);
            this.users_dataGridView.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.updateUser_userId_txtbox);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.updateUser_bttn);
            this.groupBox2.Controls.Add(this.updateUser_userType_cmbbox);
            this.groupBox2.Controls.Add(this.updateUser_password_txtbox);
            this.groupBox2.Controls.Add(this.updateUser_username_txtbox);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Location = new System.Drawing.Point(23, 239);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(249, 217);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Update User";
            // 
            // updateUser_userId_txtbox
            // 
            this.updateUser_userId_txtbox.Location = new System.Drawing.Point(64, 22);
            this.updateUser_userId_txtbox.Name = "updateUser_userId_txtbox";
            this.updateUser_userId_txtbox.ReadOnly = true;
            this.updateUser_userId_txtbox.Size = new System.Drawing.Size(100, 20);
            this.updateUser_userId_txtbox.TabIndex = 19;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(7, 25);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 13);
            this.label9.TabIndex = 18;
            this.label9.Text = "User Id";
            // 
            // updateUser_bttn
            // 
            this.updateUser_bttn.Location = new System.Drawing.Point(64, 174);
            this.updateUser_bttn.Name = "updateUser_bttn";
            this.updateUser_bttn.Size = new System.Drawing.Size(121, 37);
            this.updateUser_bttn.TabIndex = 17;
            this.updateUser_bttn.Text = "Update";
            this.updateUser_bttn.UseVisualStyleBackColor = true;
            this.updateUser_bttn.Click += new System.EventHandler(this.updateUser_bttn_Click);
            // 
            // updateUser_userType_cmbbox
            // 
            this.updateUser_userType_cmbbox.FormattingEnabled = true;
            this.updateUser_userType_cmbbox.Items.AddRange(new object[] {
            "Admin",
            "Organiser"});
            this.updateUser_userType_cmbbox.Location = new System.Drawing.Point(67, 123);
            this.updateUser_userType_cmbbox.Name = "updateUser_userType_cmbbox";
            this.updateUser_userType_cmbbox.Size = new System.Drawing.Size(121, 21);
            this.updateUser_userType_cmbbox.TabIndex = 16;
            // 
            // updateUser_password_txtbox
            // 
            this.updateUser_password_txtbox.Location = new System.Drawing.Point(65, 82);
            this.updateUser_password_txtbox.Name = "updateUser_password_txtbox";
            this.updateUser_password_txtbox.Size = new System.Drawing.Size(100, 20);
            this.updateUser_password_txtbox.TabIndex = 14;
            // 
            // updateUser_username_txtbox
            // 
            this.updateUser_username_txtbox.Location = new System.Drawing.Point(64, 50);
            this.updateUser_username_txtbox.Name = "updateUser_username_txtbox";
            this.updateUser_username_txtbox.Size = new System.Drawing.Size(100, 20);
            this.updateUser_username_txtbox.TabIndex = 13;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 123);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "User type";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 85);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "Password";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 53);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(55, 13);
            this.label8.TabIndex = 9;
            this.label8.Text = "Username";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.createUser_bttn);
            this.groupBox1.Controls.Add(this.createUser_userType_cmbbox);
            this.groupBox1.Controls.Add(this.createUser_password_txtbox);
            this.groupBox1.Controls.Add(this.createUser_username_txtbox);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(23, 23);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(249, 210);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Create User";
            // 
            // createUser_bttn
            // 
            this.createUser_bttn.Location = new System.Drawing.Point(54, 132);
            this.createUser_bttn.Name = "createUser_bttn";
            this.createUser_bttn.Size = new System.Drawing.Size(121, 37);
            this.createUser_bttn.TabIndex = 18;
            this.createUser_bttn.Text = "Create";
            this.createUser_bttn.UseVisualStyleBackColor = true;
            this.createUser_bttn.Click += new System.EventHandler(this.createUser_bttn_Click);
            // 
            // createUser_userType_cmbbox
            // 
            this.createUser_userType_cmbbox.FormattingEnabled = true;
            this.createUser_userType_cmbbox.Items.AddRange(new object[] {
            "Admin",
            "Organiser"});
            this.createUser_userType_cmbbox.Location = new System.Drawing.Point(67, 92);
            this.createUser_userType_cmbbox.Name = "createUser_userType_cmbbox";
            this.createUser_userType_cmbbox.Size = new System.Drawing.Size(98, 21);
            this.createUser_userType_cmbbox.TabIndex = 8;
            // 
            // createUser_password_txtbox
            // 
            this.createUser_password_txtbox.Location = new System.Drawing.Point(65, 58);
            this.createUser_password_txtbox.Name = "createUser_password_txtbox";
            this.createUser_password_txtbox.Size = new System.Drawing.Size(100, 20);
            this.createUser_password_txtbox.TabIndex = 6;
            // 
            // createUser_username_txtbox
            // 
            this.createUser_username_txtbox.Location = new System.Drawing.Point(65, 29);
            this.createUser_username_txtbox.Name = "createUser_username_txtbox";
            this.createUser_username_txtbox.Size = new System.Drawing.Size(100, 20);
            this.createUser_username_txtbox.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 92);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "User type";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Password";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Username";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(932, 491);
            this.tabControl1.TabIndex = 1;
            // 
            // Admin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(932, 491);
            this.Controls.Add(this.tabControl1);
            this.Name = "Admin";
            this.Text = "Admin";
            this.Load += new System.EventHandler(this.Admin_Load);
            this.tabPage3.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.orders_dataGridView)).EndInit();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.organiser_dataGridView)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.users_dataGridView)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.TextBox updateOrder_quantity_txtbox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button updateOrder_bttn;
        private System.Windows.Forms.ComboBox updateOrder_status_cmbbox;
        private System.Windows.Forms.TextBox updateOrder_userName_txtbox;
        private System.Windows.Forms.TextBox updateOrder_medicineName_txtbox;
        private System.Windows.Forms.TextBox updateOrder_medicineId_txtbox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Button loadOrders_bttn;
        private System.Windows.Forms.DataGridView orders_dataGridView;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.TextBox createOrder_quantity_txtbox;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Button createOrder_bttn;
        private System.Windows.Forms.ComboBox createOrder_status_cmbbox;
        private System.Windows.Forms.TextBox createOrder_userName_txtbox;
        private System.Windows.Forms.TextBox createOrder_medicineName_txtbox;
        private System.Windows.Forms.TextBox createOrder_medicineId_txtbox;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button loadMedicines_bttn;
        private System.Windows.Forms.DataGridView organiser_dataGridView;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox updateOrganiser_id_txtbox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button updateOrganiser_bttn;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button createOrganiser_bttn;
        private System.Windows.Forms.TextBox createOrganiser_name_txtbox;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button loadUsers_bttn;
        private System.Windows.Forms.DataGridView users_dataGridView;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox updateUser_userId_txtbox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button updateUser_bttn;
        private System.Windows.Forms.ComboBox updateUser_userType_cmbbox;
        private System.Windows.Forms.TextBox updateUser_password_txtbox;
        private System.Windows.Forms.TextBox updateUser_username_txtbox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button createUser_bttn;
        private System.Windows.Forms.ComboBox createUser_userType_cmbbox;
        private System.Windows.Forms.TextBox createUser_password_txtbox;
        private System.Windows.Forms.TextBox createUser_username_txtbox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl tabControl1;
    }
}