﻿using System.Windows.Forms;

namespace Exam.Client
{
    public static class MyActiveForm
    {
        public static Form CurrentForm { get; set; }
    }
}
