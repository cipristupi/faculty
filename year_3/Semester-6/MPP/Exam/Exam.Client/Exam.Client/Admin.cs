﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Exam.Models;
using Exam.ServiceImplementation;

namespace Exam.Client
{
    public partial class Admin : Form
    {
        private User loggedUser;
        private IExamService _examService;
        private User _loggedUser;

        public delegate void ClientNotifiedEventHandler(object sender, ClientNotifiedEventArgs e);
        public Admin()
        {
            InitializeComponent();
        }

        public Admin(User loggedUser)
        {
            _loggedUser = loggedUser;
            InitializeComponent();
        }
        private void CreateConnetionToService()
        {
            ChannelFactory<IExamService> factory = null;
            try
            {
                NotifyServiceCallback notifyServiceCallback = new NotifyServiceCallback();
                InstanceContext site = new InstanceContext(notifyServiceCallback);
                var binding = new NetTcpBinding(); //unsecureNetTcpBinding
                EndpointAddress address = new EndpointAddress("net.tcp://localhost:1235/ExamService");
                DuplexChannelFactory<IExamService> cf = new DuplexChannelFactory<IExamService>(site, binding);
                _examService = cf.CreateChannel(address);
            }
            catch (CommunicationException communicationException)
            {
                MessageBox.Show(communicationException.ToString());
                if (factory != null)
                {
                    factory.Abort();
                }
            }
            catch (TimeoutException timeoutException)
            {
                MessageBox.Show(timeoutException.ToString());
                if (factory != null)
                {
                    factory.Abort();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                if (factory != null)
                {
                    factory.Abort();
                }
            }
        }
        public void Notify(object message)
        {
            MessageBox.Show("Notification");
            if (message.ToString() == "UpdateUser")
            {
                CreateConnetionToService();
                users_dataGridView.DataSource = null;
                users_dataGridView.DataSource = _examService.GetAllUsers();
            }

           
        }
        private void createUser_bttn_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(createUser_password_txtbox.Text)  ||
    string.IsNullOrEmpty(createUser_username_txtbox.Text))
            {
                MessageBox.Show("Please fill all fields");
            }
            else
            {
                int role = createUser_userType_cmbbox.SelectedIndex + 1;
                User newUser = new User()
                {
                    Username = createUser_username_txtbox.Text,
                    Password = createUser_password_txtbox.Text,
                    Role = role
                };
                //CreateConnetionToService();
                try
                {
                    var user = _examService.CreateUser(newUser);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }

        private void Admin_Load(object sender, EventArgs e)
        {
            MyActiveForm.CurrentForm = this;
            if (_loggedUser != null)
            {
                label1.Text = _loggedUser.Username;
            }
            CreateConnetionToService();
        }

        private void updateUser_bttn_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(updateUser_password_txtbox.Text) ||
               string.IsNullOrEmpty(updateUser_username_txtbox.Text))
            {
                MessageBox.Show("Please fill all fields");
            }
            else
            {
                int role = createUser_userType_cmbbox.SelectedIndex + 1;
                User newUser = new User()
                {
                    Username = createUser_username_txtbox.Text,
                    Password = createUser_password_txtbox.Text,
                    Role = role
                };
                try
                {
                    var user = _examService.UpdateUser(newUser);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }

        private void loadUsers_bttn_Click(object sender, EventArgs e)
        {
            users_dataGridView.DataSource = null;
            users_dataGridView.DataSource = _examService.GetAllUsers();
        }

        private void updateOrganiser_bttn_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textBox1.Text))
            {
                MessageBox.Show("Please fill all fields.");
            }
            else
            {
                Participant participant = new Participant()
                {
                    Id = int.Parse(updateOrganiser_id_txtbox.Text),
                    Name = createOrganiser_name_txtbox.Text
                };
                try
                {
                    var medicine = _examService.UpdateParticipant(participant);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }

        private void createOrganiser_bttn_Click(object sender, EventArgs e)
        {
            if(string.IsNullOrEmpty(createOrganiser_name_txtbox.Text))
            {
                MessageBox.Show("Please fill all fields.");
            }
            else
            {
                Participant participant = new Participant()
                {
                    Name = createOrganiser_name_txtbox.Text
                };
                try
                {
                    var medicine = _examService.CreateParticipant(participant);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }

        private void loadMedicines_bttn_Click(object sender, EventArgs e)
        {
            organiser_dataGridView.DataSource = null;
            organiser_dataGridView.DataSource = _examService.GetAllParticipants();
        }
    }
}
