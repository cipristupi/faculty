package repository;

import java.util.List;

/**
 * Created by Cipri on 4/4/2016.
 */
public interface IFileOperations<T> {
    List<T> ReadFromFile(String filePath);
    void SaveToFile(List<T> elementsList);
}
