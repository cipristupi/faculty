package helpers;

/**
 * Created by Cipri on 4/4/2016.
 */
public class Helper {

    public static boolean isNullOrEmpty(String s) {
        return s == null || s.length() == 0;
    }
}
