package pharmacyService;

import models.Medicine;
import models.Order;
import models.User;

import java.util.List;

/**
 * Created by Cipri on 4/5/2016.
 */
public interface IPharmacyService {
    User Login(User user) throws PharmacyServiceException;

    boolean Logout(User user) throws PharmacyServiceException;

    User CreateUser(User user) throws PharmacyServiceException;

    User UpdateUser(User user) throws PharmacyServiceException;

    User GetUserById(int id) throws PharmacyServiceException;

    List<User> GetAllUsers() throws PharmacyServiceException;

    Order AddOrder(Order order) throws PharmacyServiceException;

    Order UpdateOrder(Order order) throws PharmacyServiceException;

    Order GetOrderById(int id) throws PharmacyServiceException;

    List<Order> GetAllOrders() throws PharmacyServiceException;

    Medicine AddMedicine(Medicine medicine) throws PharmacyServiceException;

    Medicine UpdateMedicine(Medicine medicine) throws PharmacyServiceException;

    Medicine GetMedicineById(int id) throws PharmacyServiceException;

    List<Medicine> GetAllMedicines() throws PharmacyServiceException;
    void RegisterClient(User user,IObserverServices client);
}
