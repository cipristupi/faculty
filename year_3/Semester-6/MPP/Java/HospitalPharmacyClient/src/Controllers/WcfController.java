/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controllers;

//import Service.WcfService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JPanel;
import static models.Enums.UserType.Admin;
import models.Medicine;
import models.Order;
import models.User;
import pharmacyService.PharmacyServiceException;


/**
 *
 * @author Cipri
 */

public class WcfController implements IController {

    JFrame currentForm;
    WcfService wcfService;

    public WcfController(JFrame form) {
        wcfService = new WcfService();
        currentForm = form;
    }

    public WcfController(JFrame form, String formType) {
        wcfService = new WcfService();
        if (formType == "Admin") {
            currentForm = (Admin) form;
        }
    }

    public void Notify(String message) {

    }

    @Override
    public User Login(User user) throws PharmacyServiceException {
        Gson g = new Gson();
        String userJson = g.toJson(user);
        String userServiceJson = wcfService.login(userJson);
        User loggedUser = g.fromJson(userServiceJson, User.class);
        return loggedUser;
    }

    @Override
    public boolean Logout(User user) throws PharmacyServiceException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public User CreateUser(User user) throws PharmacyServiceException {
        Gson g = new Gson();
        String userJson = g.toJson(user);
        String userServiceJson = wcfService.createUser(userJson);
        User loggedUser = g.fromJson(userServiceJson, User.class);
        return loggedUser;
    }

    @Override
    public User UpdateUser(User user) throws PharmacyServiceException {
        Gson g = new Gson();
        String userJson = g.toJson(user);
        String userServiceJson = wcfService.updateUser(userJson);
        User loggedUser = g.fromJson(userServiceJson, User.class);
        return loggedUser;
    }

    @Override
    public User GetUserById(int id) throws PharmacyServiceException {
        Gson g = new Gson();
        String userServiceJson = wcfService.getUserById(id);
        User user = g.fromJson(userServiceJson, User.class);
        return user;
    }

    @Override
    public List<User> GetAllUsers() throws PharmacyServiceException {
        String usersJson = wcfService.getAllUsers();
        Gson g = new Gson();
        Type arrayType = new TypeToken<ArrayList<User>>() {
        }.getType();
        ArrayList<User> users = null;
        try {
            users = g.fromJson(usersJson, arrayType);
        } catch (Exception e) {

        }
        return users;
    }

    @Override
    public Order AddOrder(Order order) throws PharmacyServiceException {
        Gson g = new Gson();
        String orderJson = g.toJson(order);
        String orderServiceJson = wcfService.addOrder(orderJson);
        return g.fromJson(orderServiceJson, Order.class);
    }

    @Override
    public Order UpdateOrder(Order order) throws PharmacyServiceException {
        Gson g = new Gson();
        String orderJson = g.toJson(order);
        String orderServiceJson = wcfService.updateOrder(orderJson);
        return g.fromJson(orderServiceJson, Order.class);
    }

    @Override
    public Order GetOrderById(int id) throws PharmacyServiceException {
        Gson g = new Gson();
        String orderServiceJson = wcfService.getOrderById(id);
        return g.fromJson(orderServiceJson, Order.class);
    }

    @Override
    public List<Order> GetAllOrders() throws PharmacyServiceException {
        String ordersJson = wcfService.getAllOrders();
        Gson g = new Gson();
        Type arrayType = new TypeToken<ArrayList<Order>>() {
        }.getType();
        ArrayList<Order> orders = null;
        try {
            orders = g.fromJson(ordersJson, arrayType);
        } catch (Exception e) {

        }
        return orders;
    }

    @Override
    public Medicine AddMedicine(Medicine medicine) throws PharmacyServiceException {
        Gson g = new Gson();
        String medicineJson = g.toJson(medicine);
        String medicineServiceJson = wcfService.addMedicine(medicineJson);
        return g.fromJson(medicineServiceJson, Medicine.class);
    }

    @Override
    public Medicine UpdateMedicine(Medicine medicine) throws PharmacyServiceException {
        Gson g = new Gson();
        String medicineJson = g.toJson(medicine);
        String medicineServiceJson = wcfService.updateMedicine(medicineJson);
        return g.fromJson(medicineServiceJson, Medicine.class);
    }

    @Override
    public Medicine GetMedicineById(int id) throws PharmacyServiceException {
        Gson g = new Gson();
        String medicineServiceJson = wcfService.getMedicineById(id);
        return  g.fromJson(medicineServiceJson, Medicine.class);
    }

    @Override
    public List<Medicine> GetAllMedicines() throws PharmacyServiceException {
        String medicinesJson = wcfService.getAllMedicines();
        Gson g = new Gson();
        Type arrayType = new TypeToken<ArrayList<Medicine>>() {
        }.getType();
        ArrayList<Medicine> medicines = null;
        try {
            medicines = g.fromJson(medicinesJson, arrayType);
        } catch (Exception e) {

        }
        return medicines;
    }
}
