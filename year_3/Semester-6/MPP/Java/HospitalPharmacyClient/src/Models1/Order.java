package models1;

import helpers.Helper;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Cipri on 4/4/2016.
 */
public class Order implements Serializable {

    public int Id;

    public String MedicineName;

    public int MedicineId;

    public int Quantity;

    public int Status; //look in enums for ints values

    public int UserId;

    public Order(int id, String medicineName,int medicineId,int quantity, int status,int userId)
    {
        this.Id = id;
        this.MedicineName = medicineName;
        this.Quantity = quantity;
        this.Status = status;
        this.UserId = userId;
        this.MedicineId = medicineId;
    }

    public Order() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<String> Validate()
    {
        Helper helper = new Helper();
        List<String> errorMessages = new ArrayList<>();
        if (helper.isNullOrEmpty(MedicineName))
        {
            errorMessages.add("Medicine name is empty");
        }
        return errorMessages;
    }
}
