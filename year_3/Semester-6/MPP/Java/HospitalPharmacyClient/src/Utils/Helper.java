package helpers;

/**
 * Created by Cipri on 4/4/2016.
 */
public class Helper {

    public static boolean isNullOrEmpty(String s) {
        return s == null || s.length() == 0;
    }
    
    public static String getUserType(int userType) {
       if(userType == 0)
       {
           return "Admin";
       }
       if(userType == 1)
       {
           return "Doctor";
       }
       if(userType == 2)
       {
           return "Pharmacist";
       }
       return "";
    }
    
     public static String getOrderType(int userType) {
       if(userType == 0)
       {
           return "Open";
       }
       if(userType == 1)
       {
           return "Delivered";
       }
       if(userType == 2)
       {
           return "NotPossible";
       }
       return "";
    }
}
