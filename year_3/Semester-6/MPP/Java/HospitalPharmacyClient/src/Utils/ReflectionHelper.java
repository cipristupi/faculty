/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import models.ServiceConfig;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author Cipri
 */
public class ReflectionHelper {

    public ServiceConfig GetServiceConfig(String xmlPath) throws NoSuchMethodException, InstantiationException {
        final ServiceConfig v;
        try {
            return ServiceConfig.class.getConstructor(String.class).newInstance(getServiceTypeFromXml(xmlPath));
        } catch (IllegalAccessException ex) {
            Logger.getLogger(ReflectionHelper.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalArgumentException ex) {
            Logger.getLogger(ReflectionHelper.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvocationTargetException ex) {
            Logger.getLogger(ReflectionHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private String getServiceTypeFromXml(String xmlFile) {
        String serviceType ="";
        try {
            File inputFile = new File(xmlFile);
            DocumentBuilderFactory dbFactory
                    = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(inputFile);
            doc.getDocumentElement().normalize();
            NodeList elementsByTagName = doc.getElementsByTagName("ServiceLanguage");
            //serviceType = elementsByTagName.item(0).getTextContent();
            serviceType = elementsByTagName.item(0).getFirstChild().getTextContent();
        } catch (SAXException ex) {
            Logger.getLogger(ReflectionHelper.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ReflectionHelper.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(ReflectionHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return serviceType;
    }
}
