function tq=trapezium_quadrature_double_integrale(f,a,b,c,d)
apb = (a+b)/2; %a+b
cpd= (c+d)/2; %c+d
tq = (((b-a)*(d-c))/16)*(f(a,c)+f(a,d)+f(b,c)+(f(b,d)+2*f(apb,c)+2*f(apb,d)+2*f(a,cpd)+2*f(b,cpd)+4*f(apb,cpd)));
end