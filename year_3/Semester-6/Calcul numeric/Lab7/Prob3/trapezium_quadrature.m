function tq=trapezium_quadrature(f,a,b)
tq = ((b-a)/2)*(f(a)+f(b));
end