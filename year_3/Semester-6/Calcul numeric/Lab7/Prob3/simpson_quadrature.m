function result=simpson_quadrature(f,a,b)
result = ((b-a)/6)*(f(a)+4*f((a+b)/2) + f(b));
end