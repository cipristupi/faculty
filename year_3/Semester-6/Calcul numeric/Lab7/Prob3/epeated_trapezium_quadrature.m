function tq=epeated_trapezium_quadrature(f,a,b,n,x)
%f2= ((x(k)-x(k-1))/2)*(f(x(k-1)+f(x))-((x(k)-x(k-1))^3)/12);
tq = ((b-a)/(2*n))*(f(a)+f(b)+2*sum(f(x(2:n))));
end