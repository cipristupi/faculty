%a) Approximate the integral

r=110;
x=75;
c=(60*r)/(r^2-x^2);
f = @(fi)sqrt((1-((x/r)^2)*sin(fi)));
x2=linspace(0,2*pi);
a = 0;
b=2*pi;
n=length(x2)-1;
trapeziumQuadrature = epeated_trapezium_quadrature(f,a,b,n,x2)*c
