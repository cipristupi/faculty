%a) Approximate the integral

f =@(x) 2./(1+x.^2);
a=0;
b=1;
trapeziumQuadrature = trapezium_quadrature(f,a,b)
simpsonQuadrature = simpson_quadrature(f,a,b)
x = [0 0 1 1];
y = [0 f(0) f(1) 0];
%fill(x,y,'g');
%hold on;
x2 = 0:0.1:1;
y2 = f(x2);
plot(x,y,'blue');
hold on;
plot(x2,y2,'red');