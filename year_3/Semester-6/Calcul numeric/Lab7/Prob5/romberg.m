function r = romberg(f,a,b,error)
   h = b-a;
   qt0 = (h/2)*(f(a)+f(b));
   %qt1 = (h/4)*qt0 + h*(f(a + (h/2)));
   qt1 = (h/4)*(f(a)+2*f(a + (h/2)) + f(b));
   k=2;
   while (qt1 - qt0)>=error 
        qt2 = (1/2)*qt1 + (h/(2^k)) * sum(f(a+((2*(1:2^(k-1))-1)/(2^k))*h));
        qt0 = qt1;
        qt1 = qt2;
        k=k+1;
   end
   r= qt1;
end