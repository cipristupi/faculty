% Considering h = 0:25; a = 1; ai = a + ih; i = 0; 4; and f(x) =
% sqrt(x.^2+x)construct the finite differences table

a=1+(0:4)*0.25;
f=sqrt(a.^2+a);
tb = dif_finite(f)