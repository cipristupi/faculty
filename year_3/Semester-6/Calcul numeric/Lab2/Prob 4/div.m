function z=div(a,h,n,f)
 x=a+[0:4]*h; 
 y=f(x);
 y=y';
 z=zeros(n+1);
 z(:,1)=y;
 for i=2:n+1
  z(1:n-i+1,i) = diff(z(1:n-i+2,i-1));
 end
 