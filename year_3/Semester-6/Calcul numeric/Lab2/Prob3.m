x=-1:0.01:3;
f = exp(x);
x0=0;
plot(x,f,'r')
hold on;
for n=1:6
    pol=zeros(size(x));
    for k=0:n
        pol=pol+(((x-x0).^k)/factorial(k));
    end
    plot(x,pol);
end
