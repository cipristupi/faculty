t=-1:0.01:1;
T1 = cos(acos(t));
T2 = cos(2*acos(t));
T3 = cos(3*acos(t));

plot(t,T1,'c',t,T2,'b',t,T3,'g');