%. To investigate the relationship between yield of potatoes, y; and level of
%fertilizer, x; an experimenter divided a field into 5 plots of equal size and applied
%differing amounts of fertilizer to each. The recorded data are given in the table
%(in pounds).

x = [ 1 2 3 4 5];
y = [22 23 25 30 28];
%t= 2.5;
%r=newton_interpolation(x,y,t)
t = 1:0.1:5;
r=newton_interpolation(x,y,t);
plot(t,r,x,y,'*');


