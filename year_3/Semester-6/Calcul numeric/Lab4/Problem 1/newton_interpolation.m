%comment
%x = vector of points
%f = values of an function in points from x
%t = where we whan to calculate interpolation value
%difdiv = diferente divizate
function y=newton_interpolation(x,f,t)
lt = length(t);
y=zeros(1,lt);
td= div_dif(x,f);
for i=1:lt
    d=t(i)-x;
    lnd = length(d);
    y(i) =[1, cumprod(d(1:lnd-1))]*td(1,:)';
end

