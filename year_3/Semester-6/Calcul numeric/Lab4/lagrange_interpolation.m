%xi = points
%fi= function values in points from x
%x where we want to calculate the interpolator
function result = lagrange_interpolation(xi,fi,x)
m=length(xi);
A=zeros(size(xi));
for i=1:m
    A(i)=1/prod([xi(1:i-1),xi(i+1:m)]-xi(i));
end
y=zeros(size(x));
for j=1:length(y)
    y(j) =  sum(A.*fi./(x(j)-xi))/sum(A./(x(j)-xi));
end
result =y;
