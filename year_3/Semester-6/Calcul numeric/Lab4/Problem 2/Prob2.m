%. Consider the function f : [0; 6] ! R; f(x) = e
%sin x and 13 equidistant interpolation
%points. Plot the interpolation points, the function f and the Newton
%intepolation polynomial.


x = linspace(0,6,13);
y = exp(sin(x));
t = 0:0.01:6;
r=newton_interpolation(x,y,t);
plot(t,r,x,y,'*');


