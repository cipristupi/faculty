%3. Approximate 115 with precision " = 103; using Aitken�s algorithm

x= [81 100 121];
y= [9 10 11];
t=115;
a = aitken(x,y,t);
abs(a-sqrt(115))