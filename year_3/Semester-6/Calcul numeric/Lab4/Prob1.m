%. To investigate the relationship between yield of potatoes, y; and level of
%fertilizer, x; an experimenter divided a field into 5 plots of equal size and applied
%differing amounts of fertilizer to each. The recorded data are given in the table
%(in pounds).

dif_finite(5)