xi=[1930 1940 1950 1960 1970 1980];
fi = [123203 131669 150697 179323 203212 226505];
x= [1955 1995];

x2=linspace(min(xi),max(xi),100);
y= lagrange_interpolation(xi,fi,x2);
plot(x2,y,xi,fi,'*');