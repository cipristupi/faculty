f = @(x) 1./(4+sin(20*x));
n = 10;
l = linspace(0,pi,n+1);
s = repeated_simpson(f,0,pi,n,l)