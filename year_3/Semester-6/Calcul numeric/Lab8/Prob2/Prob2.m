f=@(x) (100./(x.^2)).*(sin(10./x));
error = 1e-4;
a = 1;
b = 3;
p = [1:0.1:3];
r = adaptivequad(a,b,error,f)

n = 100;
l = linspace(a,b,n+1);
s = repeated_simpson(f,a,b,n,l)
plot(p,f(p));