function rs = repeated_simpson(f,a,b,n,x)
    rs = ((b-a)/(6*n))*...
    (f(a)+f(b)+4*(sum(f((x(1:n)+x(2:n+1))/2)))+2*sum(f(x(2:n))));
end