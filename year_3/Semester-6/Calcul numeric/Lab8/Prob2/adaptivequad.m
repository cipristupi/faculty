function I=adaptivequad(a,b,er,f)
    I1=simpson_quadrature(f,a,b);
    I2=simpson_quadrature(f,a,(a+b)/2)+simpson_quadrature(f,(a+b)/2,b);

    if abs(I1-I2)<15*er
           I=I2;
           return
    else
            I=adaptivequad(a,(a+b)/2,(er/2),f)+adaptivequad((a+b)/2,b,(er/2),f);
    end

end