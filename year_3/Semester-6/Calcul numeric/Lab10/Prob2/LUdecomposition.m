%Gauss�s method with partial pivoting
function x = LUdecomposition(A,n,b)
    %E = [A | b];
    A = [A,b];
    for p=1:n-1
        [absV,q] = max(abs(A(p:n,p)));
        q = q+p-1;
        %A(q,p)  = abs(absV);
        if A(q,p) == 0
            Disp('Message')
            return;
        end
        if q ~= p
            T = A(p,:);
            A(p,:) = A(q,:);
            A(q,:) = T;
        end
        
        for j = p+1:n
            m = A(j,p)/A(p,p);
            T(j) = m;
            A(j,:) = A(j,:) - m*A(p,:);
        end
    end

    if A(n,n) == 0
        disp('No unique solution');
        return
    end
    %x(n) = A(n,n+1)/A(n,n);
    %for i = n - 1:-1:1
    %    sumax = 0;
    %    for j = i+1:n
    %        sumax = sumax + A(i,j)*x(j);
    %    end    
    %   x(i) = (A(i,n+1) - sumax)/A(i,i);
    %end
    I = eye(n);
    M = zeros(n);
    M = I - T*E;
    x=M;
end