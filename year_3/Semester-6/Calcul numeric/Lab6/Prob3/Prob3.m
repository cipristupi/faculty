%3. Find the least squares polynomial of 4th degree that �t the data given
%by the vectors x = 3
%: 0:4 : 3 and y = sin(x): Plot the points and the least
%squares polynomial in the same �gure. (Use polyf it and polyval:)
xx=-3:0.01:3;
x=-3:0.4:3;
y=sin(x);
r=polyfit(x,y,4)
plot(x,y,'*',xx,polyval(r,xx))