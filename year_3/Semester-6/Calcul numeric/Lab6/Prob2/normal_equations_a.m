function [a b]=normal_equations_a(x,f)
m=length(x);
xf=zeros(m);
xf = sum(x.*f);
sumx = sum(x);
sumf = sum(f);
sumpowx2 = sum(x.^2); % sum(x^2)
sumxpow2 = (sum(x))^2; %sum(x)^2

a = ((m*xf)-(sumx*sumf))/((m*sumpowx2)-sumxpow2)
b = ((sumpowx2*sumf)-(xf*sumx))/(m*sumpowx2-sumxpow2)
end