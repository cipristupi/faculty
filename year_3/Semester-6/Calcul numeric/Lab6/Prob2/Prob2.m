%2. The vapor pressure P of the water (in bars) as a function of temperature
%T (in C) is:

t = [0 10 20 30 40 60 80 100];
p = [0.0061 0.0123 0.0234 0.0424 0.0738 0.1992 0.4736 1.0133];

[a b] = normal_equations_a(t,p);

e=polyval([a b],45);
%t = 1:0.1:7;
%plyfit = polyfit(a,b,t)
%plot(x,f,'*',t,polyfit([a b],t))
r1 = polyfit(t,p,1);
r2 = polyfit(t,p,2);

e1=polyval(r1,45);
e2=polyval(r2,45);
error = e1-e2
l = 1:0.1:100;
plot(t,p,'*',l,polyval(r1,l),l,polyval(r2,l))