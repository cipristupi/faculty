%The following table list the temperatures of a room recorded during
%the time interval [1 : 00; 7 : 00]: Find the best liniar least squares function
%'(x) = ax + b that approximates the table, using the normal equations. Use
%your result to predict the temperature of the room at 8 : 00: Find the minimum
%value E(a; b); for the obtained a and b: In the same �gure, plot the points (Time,
%Temperature) and the least squares function.

x =[1 2 3 4 5 6 7];%time
f = [13 15 20 14 15 13 10];%temperature

[a b] = normal_equations_a(x,f);

e=polyval([a b],8);
t = 1:0.1:7;
plot(x,f,'*',t,polyval([a b],t))
