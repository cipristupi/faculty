%Consider 10 random points in the plane [0; 3][0; 5] using Matlab function
%ginput: Plot the points and the least squares polynomial of 2nd degree that best
%�ts these points.


[x,y]=ginput(10)
x=x*3;
y=y*5;
r=polyfit(x,y,2)
xx = min(x):0.01:max(x)
plot(x,y,'*',xx,polyval(r,xx))