%comment
%x = vector of points
%f = values of an function in points from x
%t = where we whan to calculate interpolation value
%difdiv = diferente divizate
function y=newton_interpolation(x,f,d,t)
lt = length(t);
y=zeros(1,lt);
td= double_nodes(x,f,d);

z=zeros(1,2*length(x));
z(1:2:2*length(x)-1)=x; %de la 1 cu pasul 2 pana la xn 
z(2:2:2*length(x))=x;

for i=1:lt
    d2=t(i)-z;
    lnd = length(d2);
    y(i) =[1, cumprod(d2(1:lnd-1))]*td(1,:)';
end
end