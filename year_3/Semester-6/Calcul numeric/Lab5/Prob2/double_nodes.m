function tb = double_nodes(x,y,ydifferential)
    xn=length(x);

    z=zeros(1,2*xn);
    f=zeros(1,2*xn);
    z(1:2:2*xn-1)=x; %de la 1 cu pasul 2 pana la xn 
    z(2:2:2*xn)=x;
    
    f(1:2:2*xn-1)=y; %de la 1 cu pasul 2 pana la 2*xn 
    f(2:2:2*xn)=y;
    
    tb=zeros(2*xn);
    tb(:,1) = f'; %prima coloana din matrice
    tb(1:2:2*xn-1,2)= ydifferential';%pe coloana 2 pozitile impare punem derivata   
    tb(2:2:2*xn-2,2)= diff(y')./diff(x');%pe coloana 2 pozitiile pare puten valorile lui x
    %for j=3:2*xn
    %   tb(1:2*xn-j+1,j)=diff(tb(1:2*xn-j+2,j-1))./(z(j:2*xn)-z(1:2*xn-j+1))';
    %end
end