x = linspace(-5,5,15);
y=2*cos(2*x);
yd = sin(2*x);
%a = double_nodes(x,y,yd);
t=-5:0.01:5;
b = newton_interpolation(x,yd,y,t);
plot(t,b,x,yd,'*');