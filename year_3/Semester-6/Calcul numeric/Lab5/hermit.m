function result=hermit(x,y,f)
xn = length(x);
yn = length(y);
zdouble = zeros(1,xn*2);

for i=1:xn:2
    zdouble(i) = xn(i-1)
    zdouble(i-1) = xn(i-1)

n=length(f);
tb=zeros(n);
tb(:,1)=f';
    
    for j=2:n
       tb(1:n-j+1,j)=diff(tb(1:n-j+2,j-1))./(x(j:n)-x(1:n-j+1))';
    end
end