x = 0:0.01:1;

l1 = [1 0];
l2 = [3/2 0 -1/2];
l3 = [5/2 0 -3/2 0];
l4 = [35/8 -15/4 0 3/8];

y1 = polyval(l1,x);
y2 = polyval(l2,x);
y3 = polyval(l3,x);
y4 = polyval(l4,x);

subplot(2,2,1)
plot(x,y1)

subplot(2,2,2)
plot(x,y2)

subplot(2,2,3)
plot(x,y3)

subplot(2,2,4)
plot(x,y4)