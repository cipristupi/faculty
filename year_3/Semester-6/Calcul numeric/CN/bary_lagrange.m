function li = bary_lagrange(xi,fi,x)
    m = length(xi);
    A = zeros(size(xi));
    
    for i=1:m
        A(i) = 1/prod([xi(1:i-1),xi(i+1:m)]-xi(i));
    end
    li = zeros(size(x));
    for j=1:length(li)
        li(j) = sum(A.*fi./(x(j)-xi))/sum(A./(x(j)-xi));
    end
end