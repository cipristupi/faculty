function tb = hdouble(x,y,yd)
    m = length(x);
    
    z = zeros(1, 2*m);  
    z(1:2:2*m-1) = x;
    z(2:2:2*m) = x;
    
    f = zeros(1, 2*m);
    f(1:2:2*m-1) = y;
    f(2:2:2*m) = y;
    
    tb = zeros(2*m);
    tb(:,1) = f';
    tb(1:2:2*m-1,2) = yd'; %pe coloana 2 punem din 2 in 2, pe pozitiile impare
                           %yd' este transpunerea lui y pe coloane
    tb(2:2:2*m-2,2) = diff(y')./diff(x');
    
    for j=3:2*m
        tb(1:2*m-j+1,j)=diff(tb(1:2*m-j+2,j-1))./(z(j:2*m)-z(1:2*m-j+1))';
    end
end