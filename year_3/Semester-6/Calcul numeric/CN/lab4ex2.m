x = linspace(0,6,13);
f = exp(sin(x));
t = 0:0.1:6;
r = newton(x,f,t);
plot(t,r,x,f,'*');