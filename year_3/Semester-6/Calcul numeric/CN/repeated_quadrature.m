function rqf = repeated_quadrature(f,a,b,n,x)
    rqf = ((b-a)/(2*n))*(f(a)+f(b)+2*sum(f(x(2:n))));
end