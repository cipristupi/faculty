function tr = trapezium(f,a,b)
    tr = [(b-a)/2] * [f(a)+f(b)];
end