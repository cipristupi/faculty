x = [1 2 3 4 5 6 7];
f = [13 15 20 14 15 13 10];

[a,b] = least_square(x,f)
res = polyval([a,b],8);
t= 1:0.1:7;
plot(x,f,'*',t,polyval([a,b],t));