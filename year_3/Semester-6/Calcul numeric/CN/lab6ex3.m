x = -3:0.4:3;
t = -3:0.4:3;
y = sin(x);

p = polyfit(x,y,4);
plot(x,y,'*',t,polyval(p,t));