xi = 0:0.5:10;
fi = (1+cos(pi*xi))./(1+xi);
x = linspace(0, 10, 500);
y = bary_lagrange(xi, fi, x)
plot(x,y,xi,fi,'*')