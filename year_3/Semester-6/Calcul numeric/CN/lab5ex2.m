x = linspace(-5,5,15);
t = -5:0.1:5;
f = sin(2*x);
d = 2*cos(2*x);
h = hermite(x,f,d,t)
plot(t,h,x,f,'*');
