function y = newton(x, f, t)
    lt = length(t);
    lx = length(x);
    y = zeros(1,lt);
    tb = dif_div(x,f);
    
    for i=1:lt
       d = t(i)-x; 
       lnd = length(d);
       y(i) = [1, cumprod(d(1:lnd-1))]*tb(1,:)';
    end