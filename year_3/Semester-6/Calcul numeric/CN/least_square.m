function [a,b] = least_square(x,f)
    m = length(x);
    a = (m*sum(x.*f)-sum(x)*sum(f))/(m*sum(x.^2)-(sum(x))^2);
    b = (sum(x.^2)*sum(f)-sum(x.*f)*sum(x))/(m*sum(x.^2)-(sum(x)^2));
end