x = -1:0.01:3;
x0 = 0;
y = exp(x);
plot(x,y,'r');
hold on;

for n=1:6
    p=zeros(size(x));
    for k=0:n
        p=p+((x-x0).^k)/factorial(k);
    end
    plot(x,p);
end