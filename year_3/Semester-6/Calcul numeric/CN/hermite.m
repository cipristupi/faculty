function y = hermite(x,f,yd,t)
    lt = length(t);
    lx = length(x);
    y = zeros(1,lt);
    tb = hdouble(x,f,yd);
    z = zeros(1,2*length(x));
    z(1:2:2*lx-1) = x;
    z(2:2:2*lx) = x;
    
    for i=1:lt
       d = t(i)-z; 
       lnd = length(d);
       y(i) = [1, cumprod(d(1:lnd-1))]*tb(1,:)';
    end
end