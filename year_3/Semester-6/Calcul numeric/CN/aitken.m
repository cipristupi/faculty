function a = aitken(x,y,t)
    n=length(x);
    P=zeros(n);
    P(1,:)=y;
    
    for j=1:n-1
      for i=j+1:n
        P(j+1,i)=(P(j,i)*(t-x(j))-P(j,j)*(t-x(i)))/(x(i)-x(j));
      end
    end
    
    a=P(n,n);
    for i=1:n
      disp([x(i) P(1:i,i)']);
    end
end