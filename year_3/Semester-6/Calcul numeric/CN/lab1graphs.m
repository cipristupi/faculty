x = 0:0.01:1;
y = exp(10*x.*(x-1)).*sin(12*pi*x);
%plot(x,y);

%a = input('a=');
%b = input('b=');
t = 0:0.01:10*pi;
%x = (a+b).*cos(t)-b.*cos((a/b+1)*t);
%y = (a+b).*sin(t)-b.*sin((a/b+1)*t);
%plot(x,y);

x = 0:0.01:2*pi;
f1 = cos(x)
f2 = sin(x)
f3 = cos(2*x)
%plot(x,f1,'c',x,f2,'g', x,f3,'b');

x = 0:50;
y = zeros(1,51);
y(1:2:51) = x(1:2:51)/2;
y(2:2:50) = 3*x(2:2:50)+1;
%plot(x,y);

%plot(fft(eye(17)))

g = 1;
for i=1:5
    g = 1 + 1/g;
end

fprintf('g=%i',g)


[X,Y] = meshgrid(-2:0.01:2,-4:0.01:4);
g = exp(-((X-1/2).^2+(Y-1/2).^2));
mesh(X,Y,g);
