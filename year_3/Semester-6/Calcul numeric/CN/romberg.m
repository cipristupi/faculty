function r = romberg(f,a,b)
    r =((b-a)/2)*(f(a)+f(b));
end