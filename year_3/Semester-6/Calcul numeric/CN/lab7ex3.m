r =110;
x = 75;
f = @(t) sqrt(1-(x/r)^2*sin(t));
l = linspace(0,2*pi);
n = length(l)-1;
q = repeated_quadrature(f,0,2*pi,n,l);
h = 60*r/(r^2-x^2)*q