f = @(x) 2./(1+x.^2);
trapezium(f,0,1)
simpson(f,0,1)

v1 = [0 0 1 1];
v2 = [0 f(0) f(1) 0];
x1 = 0:0.1:1;

plot(x1, f(x1),'r');
hold on;
plot(v1, v2, 'b');
