﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RegularGrammarAndFiniteAutomata;

namespace RegularGrammerAndFiniteAutomata.Tests
{
    [TestClass]
    public class RegularGrammerTests
    {
        public RegularGrammerValidator RegularGrammerValidator;

        [TestInitialize]
        public void Initialize()
        {
            RegularGrammerValidator = new RegularGrammerValidator();
        }

        #region RegularGrammer_ValidateNonTerminals
        [TestMethod]
        public void RegularGrammer_ValidateNonTerminals_ValidNonTerminals()
        {

            //Arrange
            string nonTerminals = "A,B,C";
            //Act
            bool result = RegularGrammerValidator.ValidateNonTerminals(nonTerminals);
            //Assert
            Assert.IsTrue(result);
        }


        [TestMethod]
        public void RegularGrammer_ValidateNonTerminals_InValidNonTerminals()
        {

            //Arrange
            string nonTerminals = "A,B,C,a";
            //Act
            bool result = RegularGrammerValidator.ValidateNonTerminals(nonTerminals);
            //Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void RegularGrammer_ValidateNonTerminals_ContainsNotAllowedCharacters_InValidNonTerminals()
        {

            //Arrange
            string nonTerminals = "A,B,C,1,1,2";
            //Act
            bool result = RegularGrammerValidator.ValidateNonTerminals(nonTerminals);
            //Assert
            Assert.IsFalse(result);
        }
        #endregion


        #region RegularGrammer_ValidateTerminals

        [TestMethod]
        public void RegularGrammer_ValidateTerminals_ValidTerminals()
        {

            //Arrange
            string terminals = "a,b,c,1,2,3";
            //Act
            bool result = RegularGrammerValidator.ValidateTerminals(terminals);
            //Assert
            Assert.IsTrue(result);
        }


        [TestMethod]
        public void RegularGrammer_ValidateTerminals_InValidTerminals()
        {

            //Arrange
            string nonTerminals = "1,2,a,A,#";
            //Act
            bool result = RegularGrammerValidator.ValidateNonTerminals(nonTerminals);
            //Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void RegularGrammer_ValidateTerminals_ContainsNotAllowedCharacters_InValidTerminals()
        {

            //Arrange
            string nonTerminals = "a,b,c,1,1,2,##";
            //Act
            bool result = RegularGrammerValidator.ValidateTerminals(nonTerminals);
            //Assert
            Assert.IsFalse(result);
        }
        #endregion


        #region RegularGrammer_ValidateStartingSymbol

        [TestMethod]
        public void RegularGrammer_ValidateStartingSymbol_ValidStartingSymbol()
        {
            //Arrange
            Grammer grammer = new Grammer()
            {
                NonTerminals = "A,B,C",
                Products = string.Empty,
                StartingSymbol = "A",
                Terminals = "a,b,c"
            };
            //Act
            bool result = RegularGrammerValidator.ValidateStartingSymbol(grammer);
            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void RegularGrammer_ValidateStartingSymbol_InValidStartingSymbol()
        {
            //Arrange
            Grammer grammer = new Grammer()
            {
                NonTerminals = "A,B,C",
                Products = string.Empty,
                StartingSymbol = "Z",
                Terminals = "a,b,c"
            };
            //Act
            bool result = RegularGrammerValidator.ValidateStartingSymbol(grammer);
            //Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void RegularGrammer_ValidateStartingSymbol_MultipleStartingSymbol_InValidStartingSymbol()
        {
            //Arrange
            Grammer grammer = new Grammer()
            {
                NonTerminals = "A,B,C",
                Products = string.Empty,
                StartingSymbol = "Ana",
                Terminals = "a,b,c"
            };
            //Act
            bool result = RegularGrammerValidator.ValidateStartingSymbol(grammer);
            //Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void RegularGrammer_ValidateStartingSymbol_Epsilon_InValidStartingSymbol()
        {
            //Arrange
            Grammer grammer = new Grammer()
            {
                NonTerminals = "A,B,C",
                Products = string.Empty,
                StartingSymbol = "epsilon",
                Terminals = "a,b,c"
            };
            //Act
            bool result = RegularGrammerValidator.ValidateStartingSymbol(grammer);
            //Assert
            Assert.IsFalse(result);
        }
        #endregion


        #region RegularGrammer_GetProducts
        [TestMethod]
        public void RegularGrammer_GetProducts_ValidLength()
        {
            //Arrange
            string nonTerminal = "A";
            string products = "A-aA|bA,B-ba|aC";
            //Act
            var productsList = RegularGrammerValidator.GetProducts(products);
            //Assert
            Assert.AreEqual(2, productsList.Count);
            Assert.AreEqual("A", productsList[0].NonTerminal);
            Assert.AreEqual(2, productsList[0].Combinations.Count);

            Assert.AreEqual("B", productsList[1].NonTerminal);
            Assert.AreEqual(2, productsList[1].Combinations.Count);
        }
        #endregion

        #region RegularGrammer_ValidProducts
        [TestMethod]
        public void RegularGrammer_ValidProducts_Valid()
        {
            //Arrange
            Grammer grammer = new Grammer()
            {
                NonTerminals = "A,B,C",
                Products = "A-aA|a|aB,B-bA|bB",
                StartingSymbol = "A",
                Terminals = "a,b,c"
            };

            //Act
            bool result = RegularGrammerValidator.ValidateProducts(grammer);

            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void RegularGrammer_ValidProducts_OneCombinationWithEpsilonNotIncludedInOtherCombination_Valid()
        {
            //Arrange
            Grammer grammer = new Grammer()
            {
                NonTerminals = "A,B,C",
                Products = "A-aA|a|aB,B-bA|bB,C-epsilon",
                StartingSymbol = "C",
                Terminals = "a,b,c"
            };

            //Act
            bool result = RegularGrammerValidator.ValidateProducts(grammer);

            //Assert
            Assert.IsTrue(result);
        }


        [TestMethod]
        public void RegularGrammer_ValidProducts_CombinationWithEpsilonIncludedInOtherCombination_InValid()
        {
            //Arrange
            Grammer grammer = new Grammer()
            {
                NonTerminals = "A,B,C",
                Products = "A-aA|a|aB|aC,B-bA|bB,C-epsilon",
                StartingSymbol = "A",
                Terminals = "a,b,c"
            };

            //Act
            bool result = RegularGrammerValidator.ValidateProducts(grammer);

            //Assert
            Assert.IsFalse(result);
        }



        [TestMethod]
        public void RegularGrammer_ValidProducts_CombinationWithNonExistingTerminal_InValid()
        {
            //Arrange
            Grammer grammer = new Grammer()
            {
                NonTerminals = "A,B,C",
                Products = "A-aA|a|aB,B-bA|bB,C-epsilon|ac|cB",
                StartingSymbol = "A",
                Terminals = "a,b"
            };

            //Act
            bool result = RegularGrammerValidator.ValidateProducts(grammer);
            //Assert
            Assert.IsFalse(result);
        }



        [TestMethod]
        public void RegularGrammer_ValidProducts_CombinationWithNonExistingNonTerminal_InValid()
        {

            //Arrange
            Grammer grammer = new Grammer()
            {
                NonTerminals = "A,B",
                Products = "A-aA|a|aB|aC,B-bA|bB,C-epsilon|ac|cB",
                StartingSymbol = "A",
                Terminals = "a,b,c"
            };

            //Act
            bool result = RegularGrammerValidator.ValidateProducts(grammer);

            //Assert
            Assert.IsFalse(result);
        }


        [TestMethod]
        public void RegularGrammer_ValidProducts_CombinationWithNonExistingNonTerminalAndTerminals_InValid()
        {

            //Arrange
            Grammer grammer = new Grammer()
            {
                NonTerminals = "A,B",
                Products = "A-aA|a|aB|aC,B-bA|bB,C-epsilon|ac|cB",
                StartingSymbol = "A",
                Terminals = "a,b"
            };

            //Act
            bool result = RegularGrammerValidator.ValidateProducts(grammer);

            //Assert
            Assert.IsFalse(result);
        }


        [TestMethod]
        public void RegularGrammer_ValidProducts_CombinationWithEpsilonAndTerminal_InValid()
        {

            //Arrange
            Grammer grammer = new Grammer()
            {
                NonTerminals = "A,B",
                Products = "A-aA|a|aB|aC,B-bA|bB,C-epsilonc|ac|cB",
                StartingSymbol = "A",
                Terminals = "a,b"
            };

            //Act
            bool result = RegularGrammerValidator.ValidateProducts(grammer);

            //Assert
            Assert.IsFalse(result);
        }


        [TestMethod]
        public void RegularGrammer_ValidProducts_CombinationWithEpsilonAndNonTerminal_InValid()
        {

            //Arrange
            Grammer grammer = new Grammer()
            {
                NonTerminals = "A,B",
                Products = "A-aA|a|aB|aC,B-bA|bB,C-epsilonA|ac|cB",
                StartingSymbol = "A",
                Terminals = "a,b"
            };

            //Act
            bool result = RegularGrammerValidator.ValidateProducts(grammer);

            //Assert
            Assert.IsFalse(result);
        }
        #endregion


        #region RegularGrammer_ValideRegularGrammer

        [TestMethod]
        public void RegularGrammer_ValideRegularGrammer_AllSetsValid_Valid()
        {
            //Arrange
            Grammer grammer = new Grammer()
            {
                NonTerminals = "A,B,C",
                Products = "A-aA|a|aB,B-bA|bB,C-epsilon|ac|cB",
                StartingSymbol = "C",
                Terminals = "a,b,c"
            };

            //Act
            bool result = RegularGrammerValidator.ValidateRegularGrammer(grammer);

            //Assert
            Assert.IsTrue(result);
        }


        [TestMethod]
        public void RegularGrammer_ValideRegularGrammer_InvalidNonTerminals_InValid()
        {
            //Arrange
            Grammer grammer = new Grammer()
            {
                NonTerminals = "A,B,a",
                Products = "A-aA|a|aB,B-bA|bB,C-epsilon|ac|cB",
                StartingSymbol = "A",
                Terminals = "a,b"
            };

            //Act
            bool result = RegularGrammerValidator.ValidateRegularGrammer(grammer);

            //Assert
            Assert.IsFalse(result);
        }


        [TestMethod]
        public void RegularGrammer_ValideRegularGrammer_InvalidTerminals_InValid()
        {
            //Arrange
            Grammer grammer = new Grammer()
            {
                NonTerminals = "A,B,a",
                Products = "A-aA|a|aB,B-bA|bB,C-epsilon|ac|cB",
                StartingSymbol = "A",
                Terminals = "a,b,ana,$"
            };

            //Act
            bool result = RegularGrammerValidator.ValidateRegularGrammer(grammer);

            //Assert
            Assert.IsFalse(result);
        }


        [TestMethod]
        public void RegularGrammer_ValideRegularGrammer_InvalidStartingSymbol_InValid()
        {
            //Arrange
            Grammer grammer = new Grammer()
            {
                NonTerminals = "A,B,a",
                Products = "A-aA|a|aB,B-bA|bB,C-epsilon|ac|cB",
                StartingSymbol = "epsilon",
                Terminals = "a,b"
            };

            //Act
            bool result = RegularGrammerValidator.ValidateRegularGrammer(grammer);

            //Assert
            Assert.IsFalse(result);
        }


        [TestMethod]
        public void RegularGrammer_ValideRegularGrammer_InvalidCombination_InValid()
        {
            //Arrange
            Grammer grammer = new Grammer()
            {
                NonTerminals = "A,B,a",
                Products = "A-aA|a|aB|aC,B-bA|bB,C-epsilonA|ac|cB",
                StartingSymbol = "A",
                Terminals = "a,b"
            };

            //Act
            bool result = RegularGrammerValidator.ValidateRegularGrammer(grammer);

            //Assert
            Assert.IsFalse(result);
        }
        #endregion
    }
}
