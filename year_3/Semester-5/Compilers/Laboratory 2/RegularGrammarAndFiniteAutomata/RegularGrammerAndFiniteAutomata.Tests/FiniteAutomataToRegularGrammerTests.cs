﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RegularGrammarAndFiniteAutomata;

namespace RegularGrammerAndFiniteAutomata.Tests
{
    [TestClass]
    public class FiniteAutomataToRegularGrammerTests
    {
        private const string EPSILON = "epsilon";
        private readonly RegularGrammerValidator regularGrammerValidator =new RegularGrammerValidator();
        [TestMethod]
        public void FiniteAutomataToRegularGrammer_ConvertFiniteAutomataToRegularGrammer_Valid()
        {
            FiniteAutomataToRegularGrammer finiteAutomataToRegularGrammer = new FiniteAutomataToRegularGrammer();
            //Arrange
            FiniteAutomata automata = new FiniteAutomata
            {
                FiniteSetOfStates = "A,B,C,D,E,F,G,H,I",
                Alphabet = "a,b",
                InitialState = "A",
                SetOfFinalStates = "A,B,C,D,E,F",
                TransitionFunctionResults = InitializeFunctions()
            };
            //Act
            Grammer grammer = finiteAutomataToRegularGrammer.ConvertFiniteAutomataToRegularGrammer(automata);
            //Assert
            Assert.AreEqual(automata.FiniteSetOfStates, grammer.NonTerminals);
            Assert.AreEqual(automata.Alphabet, grammer.Terminals);
            Assert.AreEqual(automata.InitialState, grammer.StartingSymbol);

            List<string> nonTerminals = regularGrammerValidator.GetElements(automata.FiniteSetOfStates, ',');
            foreach (string nonTerminal in nonTerminals)
            {
                ProductItem expected =null;
                ProductItem actual = grammer.ProductsItems.FirstOrDefault(x => x.NonTerminal == nonTerminal);
                switch (nonTerminal)
                {
                    case "A":
                        expected = ProductsItemsA();
                        break;
                    case "B":
                        expected = ProductsItemsB();
                        break;
                    case "C":
                        expected = ProductsItemsC();
                        break;
                    case "D":
                        expected = ProductsItemsD();
                        break;
                    case "E":
                        expected = ProductsItemsE();
                        break;
                    case "F":
                        expected = ProductsItemsF();
                        break;
                    case "G":
                        expected = ProductsItemsG();
                        break;
                    case "H":
                        expected = ProductsItemsH();
                        break;
                    case "I":
                        expected = ProductsItemsI();
                        break;
                }
                ValidateCombinations(expected, actual);
            }

        }

        public void ValidateCombinations(ProductItem expected,ProductItem actual)
        {
            Assert.AreEqual(expected.NonTerminal, actual.NonTerminal);
            Assert.AreEqual(expected.Combinations.Count, actual.Combinations.Count);
            foreach (string expectedCombination in expected.Combinations)
            {
                Assert.IsTrue(actual.Combinations.Contains(expectedCombination),string.Format("Expected: {0}, Actual: {1}, Expected Combination {2},Actual Combinations: {3}",true,false,expectedCombination,string.Join(",",actual.Combinations)));
            }
        }




        private ProductItem ProductsItemsA()
        {
            List<string> combinations = new List<string>()
            {
                "bA","aB","a","b","epsilon"
            };
            ProductItem item = new ProductItem
            {
                NonTerminal = "A",
                Combinations = combinations
            };
            return item;
        }

        private ProductItem ProductsItemsB()
        {
            List<string> combinations = new List<string>()
            {
                "bC","bG","b"
            };
            ProductItem item = new ProductItem
            {
                NonTerminal = "B",
                Combinations = combinations
            };
            return item;
        }
        private ProductItem ProductsItemsC()
        {
            List<string> combinations = new List<string>()
            {
                "bD","b"
            };
            ProductItem item = new ProductItem
            {
                NonTerminal = "C",
                Combinations = combinations
            };
            return item;
        }
        private ProductItem ProductsItemsD()
        {
            List<string> combinations = new List<string>()
            {
                "bE","b"
            };
            ProductItem item = new ProductItem
            {
                NonTerminal = "D",
                Combinations = combinations
            };
            return item;
        }
        private ProductItem ProductsItemsE()
        {
            List<string> combinations = new List<string>()
            {
                "bF","b"
            };
            ProductItem item = new ProductItem
            {
                NonTerminal = "E",
                Combinations = combinations
            };
            return item;
        }
        private ProductItem ProductsItemsF()
        {
            List<string> combinations = new List<string>()
            {
                "aB","a"
            };
            ProductItem item = new ProductItem
            {
                NonTerminal = "F",
                Combinations = combinations
            };
            return item;
        }
        private ProductItem ProductsItemsG()
        {
            List<string> combinations = new List<string>()
            {
                "bH"
            };
            ProductItem item = new ProductItem
            {
                NonTerminal = "G",
                Combinations = combinations
            };
            return item;
        }
        private ProductItem ProductsItemsH()
        {
            List<string> combinations = new List<string>()
            {
                "bI"
            };
            ProductItem item = new ProductItem
            {
                NonTerminal = "H",
                Combinations = combinations
            };
            return item;
        }
        private ProductItem ProductsItemsI()
        {
            List<string> combinations = new List<string>()
            {
                "bB","b"
            };
            ProductItem item = new ProductItem
            {
                NonTerminal = "I",
                Combinations = combinations
            };
            return item;
        }
        public List<Function> InitializeFunctions()
        {
            List<Function> functions = new List<Function>
            {
                new Function("A", "a", "B"),
                new Function("A", "a", "a"),
                new Function("A", "b", "A"),
                new Function("A", "b", "b"),

                new Function("B", "b", "C"),
                new Function("B", "b", "G"),
                new Function("B", "b", "b"),

                new Function("C", "b", "D"),
                new Function("C", "b", "b"),

                new Function("D", "b", "E"),
                new Function("D", "b", "b"),

                new Function("E", "b", "F"),
                new Function("E", "b", "b"),

                new Function("F", "a", "B"),
                new Function("F", "a", "a"),

                new Function("G", "b", "H"),
                new Function("G", "b", "b"),

                new Function("H", "b", "I"),
                new Function("H", "b", "b"),

                new Function("I", "b", "B"),
                new Function("I", "b", "b")
            };
            return functions;
        }
    }
}
