﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RegularGrammarAndFiniteAutomata;

namespace RegularGrammerAndFiniteAutomata.Tests
{
    [TestClass]
    public class RegularGrammerToFiniteAutomataTests
    {
        private const string EPSILON = "epsilon";
        [TestMethod]
        public void RegularGrammerToFiniteAutomata_ConvertRegularGrammerToFiniteAutomata_Valid()
        {
            RegularGrammerToFiniteAutomata regularGrammerToFiniteAutomata = new RegularGrammerToFiniteAutomata();
            //Arrange
            Grammer grammer = new Grammer();
            grammer.StartingSymbol = "S";
            grammer.NonTerminals = "S,A";
            grammer.Terminals = "a,b";
            grammer.Products = "S-epsilon|aA,A-aA|bA|a|b";
            //Act
            FiniteAutomata finiteAutomata = regularGrammerToFiniteAutomata.ConvertRegularGrammerToFiniteAutomata(grammer);
            //Assert
            Assert.AreEqual("S,A,K", finiteAutomata.FiniteSetOfStates);
            Assert.AreEqual(grammer.Terminals, finiteAutomata.Alphabet);
            Assert.AreEqual(grammer.StartingSymbol, finiteAutomata.InitialState);
            List<Function> expectedFunctions = ExpectedFunctions();
            Assert.AreEqual(expectedFunctions.Count, finiteAutomata.TransitionFunctionResults.Count);
            foreach (Function expectedFunction in expectedFunctions)
            {
                Function function =
                    finiteAutomata.TransitionFunctionResults.Where(
                        x => x.NonTerminal == expectedFunction.NonTerminal &&
                             x.Terminal == expectedFunction.Terminal &&
                             x.FinalState == expectedFunction.FinalState
                             ).FirstOrDefault();
                Assert.IsNotNull(function);
                Assert.AreEqual(expectedFunction.NonTerminal, function.NonTerminal);
                Assert.AreEqual(expectedFunction.Terminal, function.Terminal);
                Assert.AreEqual(expectedFunction.FinalState, function.FinalState);
            }
        }

        public List<Function> ExpectedFunctions()
        {
            List<Function> expectedFunctions = new List<Function>();
            expectedFunctions.Add(new Function("S", "a", "A"));
            expectedFunctions.Add(new Function("A", "a", "A"));
            expectedFunctions.Add(new Function("A", "a", "K"));
            expectedFunctions.Add(new Function("A", "b", "A"));
            expectedFunctions.Add(new Function("A", "b", "K"));
            return expectedFunctions;
        }
    }
}
