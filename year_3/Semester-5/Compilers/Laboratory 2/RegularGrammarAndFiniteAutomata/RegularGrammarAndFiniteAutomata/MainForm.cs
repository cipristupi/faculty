﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace RegularGrammarAndFiniteAutomata
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        #region Grammer
        private void loadFileRGBttn_Click(object sender, EventArgs e)
        {
            outputRTxtbox.Clear();
            openFileDialog1.DefaultExt = "*.txt";
            DialogResult result = openFileDialog1.ShowDialog();
            string[] lines = { };
            if (result == DialogResult.OK) // Test result.
            {
                lines = File.ReadAllLines(openFileDialog1.FileName);
            }
            StringBuilder builder = new StringBuilder();
            foreach (string line in lines)
            {
                builder.AppendLine(line);
            }

            inputRTxtBox.Text = builder.ToString();
        }

        private void readBttn_Click(object sender, EventArgs e)
        {
            List<string> errorMessages = new List<string>();
            if (ValidInputGrammer(errorMessages))
            {
                string nonterminals = string.Format("N={0}", nonTerminalsTxtBox.Text);
                string terminals = string.Format("E={0}", terminalsTxtBox.Text);
                string startingSymbol = string.Format("S={0}", startingSymbolTxtBox.Text);
                string products = string.Format("P={0}", productsTxtBox.Text);
                inputRTxtBox.Text = string.Format("{0}\n{1}\n{2}\n{3}", nonterminals, terminals, startingSymbol,
                    products);
            }
            else
            {
                string errorMessage = string.Join("\n", errorMessages);
                MessageBox.Show(errorMessage, "Validation summary", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private bool ValidInputGrammer(List<string> errorMessages)
        {
            bool valid = true;
            if (string.IsNullOrEmpty(nonTerminalsTxtBox.Text))
            {
                errorMessages.Add("Non-Terminals cannot be empty.");
                valid = false;
            }
            if (string.IsNullOrEmpty(terminalsTxtBox.Text))
            {
                errorMessages.Add("Terminals cannot be empty.");
                valid = false;
            }

            if (string.IsNullOrEmpty(productsTxtBox.Text))
            {
                errorMessages.Add("Products cannot be empty.");
                valid = false;
            }
            if (string.IsNullOrEmpty(startingSymbolTxtBox.Text))
            {
                errorMessages.Add("Starting Symbol cannot be empty.");
                valid = false;
            }
            return valid;
        }

        private void convertToFiniteAutomataBttn_Click(object sender, EventArgs e)
        {
            outputRTxtbox.Clear();
            if (!string.IsNullOrEmpty(inputRTxtBox.Text))
            {
                string regularGrammerAsString = inputRTxtBox.Text;
                Grammer grammer = GetGrammerFromString(regularGrammerAsString);
                RegularGrammerToFiniteAutomata regularGrammerToFiniteAutomata = new RegularGrammerToFiniteAutomata();
                FiniteAutomata finiteAutomata =
                    regularGrammerToFiniteAutomata.ConvertRegularGrammerToFiniteAutomata(grammer);
                outputRTxtbox.Text = finiteAutomata.ToString();
            }
            else
            {
                MessageBox.Show("Please load file or add a grammer using inputs");
            }
        }


        private Grammer GetGrammerFromString(string str)
        {
            string[] lines = str.Split(Environment.NewLine.ToCharArray());
            Grammer grammer = new Grammer();
            for (int i = 0; i < lines.Length; i++)
            {
                string line = lines[i];
                if (line.Length != 0)
                {
                    if (line[0] == 'N')
                    {
                        grammer.NonTerminals = line.Split('=')[1];
                    }
                    if (line[0] == 'E')
                    {
                        grammer.Terminals = line.Split('=')[1];
                    }
                    if (line[0] == 'S')
                    {
                        grammer.StartingSymbol = line.Split('=')[1];
                    }
                    if (line[0] == 'P')
                    {
                        grammer.Products = line.Split('=')[1];
                    }
                }
            }
            return grammer;
        }

        private void regularGrammerBttn_Click(object sender, EventArgs e)
        {
            if (inputRTxtBox.TextLength != 0)
            {
                outputRTxtbox.Clear();
                string regularGrammerAsString = inputRTxtBox.Text;
                Grammer grammer = GetGrammerFromString(regularGrammerAsString);
                RegularGrammerValidator validator = new RegularGrammerValidator();
                outputRTxtbox.Text = validator.ValidateRegularGrammer(grammer) ? "Regular Grammer" : "Not Regular Grammer";
            }
            else
            {
                MessageBox.Show("Please load file or add a grammer using inputs");
            }
        }

        private void getProductsForNonTerminalBttn_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(getProductsForNonTerminalTxtbox.Text))
            {
                outputRTxtbox.Clear();
                string regularGrammerAsString = inputRTxtBox.Text;
                Grammer grammer = GetGrammerFromString(regularGrammerAsString);
                RegularGrammerValidator validator = new RegularGrammerValidator();
                outputRTxtbox.Text = validator.GetProductsForNonTerminal(getProductsForNonTerminalTxtbox.Text,
                    grammer.Products);
            }
            else
            {
                MessageBox.Show("Please fill Get products for Non-Terminal");
            }
        }

        #endregion

        #region FiniteAutomata

        private void loadFileBttn_Click(object sender, EventArgs e)
        {
            openFileDialog1.DefaultExt = "*.txt";
            DialogResult result = openFileDialog1.ShowDialog();
            string[] lines = { };
            if (result == DialogResult.OK) // Test result.
            {
                lines = File.ReadAllLines(openFileDialog1.FileName);
            }
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < lines.Length; i++)
            {
                stringBuilder.AppendLine(lines[i]);
            }
            inputRichTextBox.Text = stringBuilder.ToString();
        }

        private FiniteAutomata GetFiniteAutomataFromString(string str)
        {
            string[] lines = str.Split(Environment.NewLine.ToCharArray());
            FiniteAutomata finiteAutomata = new FiniteAutomata();
            List<Function> transitionFunctionResults = new List<Function>();
            for (int i = 0; i < lines.Length; i++)
            {
                string line = lines[i];
                if (line.Length != 0)
                {
                    if (line[0] == 'Q')
                    {
                        finiteAutomata.FiniteSetOfStates = line.Split('=')[1];
                    }
                    if (line[0] == 'E')
                    {
                        finiteAutomata.Alphabet = line.Split('=')[1];
                    }
                    if (line.Contains("q0"))
                    {
                        finiteAutomata.InitialState = line.Split('=')[1];
                    }
                    if (line[0] == 'F')
                    {
                        finiteAutomata.SetOfFinalStates = line.Split('=')[1];
                    }
                    if (line[0] == 'd')
                    {

                        Function function = new Function();
                        string betweenParactesis = line.Split('(', ')')[1];
                        function.NonTerminal = betweenParactesis.Split(',')[0];
                        function.Terminal = betweenParactesis.Split(',')[1];
                        function.FinalState = line.Split('=')[1];
                        transitionFunctionResults.Add(function);
                    }
                }
            }
            finiteAutomata.TransitionFunctionResults = transitionFunctionResults;
            return finiteAutomata;
        }

        #endregion

        private void readInputBttn_Click(object sender, EventArgs e)
        {
            List<string> errorMessages = new List<string>();
            if (ValidInputFiniteAutomata(errorMessages))
            {
                string finiteSetOfStates = string.Format("Q={0}", finiteSetOfStatesTxtbox.Text);
                string alphabet = string.Format("E={0}", alphabetTxtBox.Text);
                string initialState = string.Format("q0={0}", initialStateTxtbox.Text);
                string setOfFinalStates = string.Format("F={0}", setOfFinalStatesTxtBox.Text);
                string transitionFunction = transitionFunctionsRTxtBox.Text;
                inputRichTextBox.Text = string.Format("{0}\n{1}\n{2}\n{3}\n{4}", finiteSetOfStates, alphabet, initialState,
                    setOfFinalStates, transitionFunction);
            }
            else
            {
                string errorMessage = string.Join("\n", errorMessages);
                MessageBox.Show(errorMessage, "Validation summary", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private bool ValidInputFiniteAutomata(List<string> errorMessages)
        {
            bool valid = true;
            if (string.IsNullOrEmpty(finiteSetOfStatesTxtbox.Text))
            {
                errorMessages.Add("Finit set of states cannot be empty.");
                valid = false;
            }
            if (string.IsNullOrEmpty(alphabetTxtBox.Text))
            {
                errorMessages.Add("Alphabet cannot be empty.");
                valid = false;
            }

            if (string.IsNullOrEmpty(initialStateTxtbox.Text))
            {
                errorMessages.Add("Initial state be empty.");
                valid = false;
            }
            if (string.IsNullOrEmpty(transitionFunctionsRTxtBox.Text))
            {
                errorMessages.Add("Set of final states cannot be empty.");
                valid = false;
            }
            return valid;
        }

        private void convertToGrammerBttn_Click(object sender, EventArgs e)
        {
            if (inputRichTextBox.TextLength != 0)
            {
                string finiteAutomataString = inputRichTextBox.Text.Trim(' ');
                FiniteAutomata finiteAutomata = GetFiniteAutomataFromString(finiteAutomataString);
                FiniteAutomataToRegularGrammer finiteAutomataToRegularGrammer = new FiniteAutomataToRegularGrammer();
                Grammer grammer = finiteAutomataToRegularGrammer.ConvertFiniteAutomataToRegularGrammer(finiteAutomata);
                ouputRichTextBox.Text = grammer.ToString();
            }
            else
            {
                MessageBox.Show("Please load file or add a finite automana using inputs");
            }
        }
    }
}
