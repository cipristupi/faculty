﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RegularGrammarAndFiniteAutomata
{
    public class FiniteAutomata
    {
        public string FiniteSetOfStates { get; set; } //Q

        public string Alphabet { get; set; } //

        public string InitialState { get; set; }
        public string SetOfFinalStates { get; set; }

        public List<Function> TransitionFunctionResults { get; set; }


        public override string ToString()
        {
            StringBuilder finiteAutomataString = new StringBuilder();
            finiteAutomataString.AppendLine(string.Format("Q={0}", this.FiniteSetOfStates));
            finiteAutomataString.AppendLine(string.Format("E={0}", this.Alphabet));
            finiteAutomataString.AppendLine(string.Format("q0={0}", this.InitialState));
            finiteAutomataString.AppendLine(string.Format("F={0}", this.SetOfFinalStates));

            foreach (Function item in TransitionFunctionResults)
            {
                finiteAutomataString.AppendLine(string.Format("d({0},{1})={2}", item.NonTerminal, item.Terminal,item.FinalState));
            }
            return finiteAutomataString.ToString();
        }
    }

    public class Function
    {
        public string NonTerminal { get; set; }
        public string Terminal { get; set; }
        public string FinalState { get; set; }

        public Function()
        {
                
        }
        public Function(string nonTerminal,string terminal,string finalState)
        {
            NonTerminal = nonTerminal;
            Terminal = terminal;
            FinalState = finalState;
        }
    }
}
