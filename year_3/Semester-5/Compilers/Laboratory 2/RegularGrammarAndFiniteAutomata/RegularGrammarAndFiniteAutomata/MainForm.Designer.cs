﻿namespace RegularGrammarAndFiniteAutomata
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.regularGrammerBttn = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.readBttn = new System.Windows.Forms.Button();
            this.getProductsForNonTerminalTxtbox = new System.Windows.Forms.TextBox();
            this.startingSymbolTxtBox = new System.Windows.Forms.TextBox();
            this.productsTxtBox = new System.Windows.Forms.TextBox();
            this.terminalsTxtBox = new System.Windows.Forms.TextBox();
            this.nonTerminalsTxtBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.outputRTxtbox = new System.Windows.Forms.RichTextBox();
            this.inputRTxtBox = new System.Windows.Forms.RichTextBox();
            this.loadFileRGBttn = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.convertToGrammerBttn = new System.Windows.Forms.Button();
            this.readInputBttn = new System.Windows.Forms.Button();
            this.initialStateTxtbox = new System.Windows.Forms.TextBox();
            this.alphabetTxtBox = new System.Windows.Forms.TextBox();
            this.finiteSetOfStatesTxtbox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.ouputRichTextBox = new System.Windows.Forms.RichTextBox();
            this.inputRichTextBox = new System.Windows.Forms.RichTextBox();
            this.loadFileBttn = new System.Windows.Forms.Button();
            this.getProductsForNonTerminalBttn = new System.Windows.Forms.Button();
            this.transitionFunctionsRTxtBox = new System.Windows.Forms.RichTextBox();
            this.setOfFinalStatesTxtBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(898, 452);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.getProductsForNonTerminalBttn);
            this.tabPage1.Controls.Add(this.regularGrammerBttn);
            this.tabPage1.Controls.Add(this.button2);
            this.tabPage1.Controls.Add(this.readBttn);
            this.tabPage1.Controls.Add(this.getProductsForNonTerminalTxtbox);
            this.tabPage1.Controls.Add(this.startingSymbolTxtBox);
            this.tabPage1.Controls.Add(this.productsTxtBox);
            this.tabPage1.Controls.Add(this.terminalsTxtBox);
            this.tabPage1.Controls.Add(this.nonTerminalsTxtBox);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.outputRTxtbox);
            this.tabPage1.Controls.Add(this.inputRTxtBox);
            this.tabPage1.Controls.Add(this.loadFileRGBttn);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(890, 426);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Grammer";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // regularGrammerBttn
            // 
            this.regularGrammerBttn.Location = new System.Drawing.Point(408, 368);
            this.regularGrammerBttn.Name = "regularGrammerBttn";
            this.regularGrammerBttn.Size = new System.Drawing.Size(105, 36);
            this.regularGrammerBttn.TabIndex = 23;
            this.regularGrammerBttn.Text = "Regular Grammer";
            this.regularGrammerBttn.UseVisualStyleBackColor = true;
            this.regularGrammerBttn.Click += new System.EventHandler(this.regularGrammerBttn_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(283, 368);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(105, 36);
            this.button2.TabIndex = 22;
            this.button2.Text = "Convert to Finite Automata";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.convertToFiniteAutomataBttn_Click);
            // 
            // readBttn
            // 
            this.readBttn.Location = new System.Drawing.Point(146, 368);
            this.readBttn.Name = "readBttn";
            this.readBttn.Size = new System.Drawing.Size(105, 36);
            this.readBttn.TabIndex = 21;
            this.readBttn.Text = "Read Inputs";
            this.readBttn.UseVisualStyleBackColor = true;
            this.readBttn.Click += new System.EventHandler(this.readBttn_Click);
            // 
            // getProductsForNonTerminalTxtbox
            // 
            this.getProductsForNonTerminalTxtbox.Location = new System.Drawing.Point(11, 321);
            this.getProductsForNonTerminalTxtbox.Name = "getProductsForNonTerminalTxtbox";
            this.getProductsForNonTerminalTxtbox.Size = new System.Drawing.Size(248, 20);
            this.getProductsForNonTerminalTxtbox.TabIndex = 20;
            // 
            // startingSymbolTxtBox
            // 
            this.startingSymbolTxtBox.Location = new System.Drawing.Point(11, 242);
            this.startingSymbolTxtBox.Name = "startingSymbolTxtBox";
            this.startingSymbolTxtBox.Size = new System.Drawing.Size(248, 20);
            this.startingSymbolTxtBox.TabIndex = 19;
            // 
            // productsTxtBox
            // 
            this.productsTxtBox.Location = new System.Drawing.Point(11, 182);
            this.productsTxtBox.Name = "productsTxtBox";
            this.productsTxtBox.Size = new System.Drawing.Size(248, 20);
            this.productsTxtBox.TabIndex = 18;
            // 
            // terminalsTxtBox
            // 
            this.terminalsTxtBox.Location = new System.Drawing.Point(11, 122);
            this.terminalsTxtBox.Name = "terminalsTxtBox";
            this.terminalsTxtBox.Size = new System.Drawing.Size(248, 20);
            this.terminalsTxtBox.TabIndex = 17;
            // 
            // nonTerminalsTxtBox
            // 
            this.nonTerminalsTxtBox.Location = new System.Drawing.Point(11, 56);
            this.nonTerminalsTxtBox.Name = "nonTerminalsTxtBox";
            this.nonTerminalsTxtBox.Size = new System.Drawing.Size(248, 20);
            this.nonTerminalsTxtBox.TabIndex = 16;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(8, 145);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 17);
            this.label5.TabIndex = 15;
            this.label5.Text = "Products";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(8, 285);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(201, 17);
            this.label4.TabIndex = 14;
            this.label4.Text = "Get products for Non-Terminal";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(8, 205);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(107, 17);
            this.label3.TabIndex = 13;
            this.label3.Text = "Starting Symbol";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(8, 85);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 17);
            this.label2.TabIndex = 12;
            this.label2.Text = "Terminals";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 17);
            this.label1.TabIndex = 11;
            this.label1.Text = "Non-terminals";
            // 
            // outputRTxtbox
            // 
            this.outputRTxtbox.Location = new System.Drawing.Point(581, 24);
            this.outputRTxtbox.Name = "outputRTxtbox";
            this.outputRTxtbox.Size = new System.Drawing.Size(292, 329);
            this.outputRTxtbox.TabIndex = 10;
            this.outputRTxtbox.Text = "";
            // 
            // inputRTxtBox
            // 
            this.inputRTxtBox.Location = new System.Drawing.Point(283, 25);
            this.inputRTxtBox.Name = "inputRTxtBox";
            this.inputRTxtBox.Size = new System.Drawing.Size(292, 328);
            this.inputRTxtBox.TabIndex = 9;
            this.inputRTxtBox.Text = "";
            // 
            // loadFileRGBttn
            // 
            this.loadFileRGBttn.Location = new System.Drawing.Point(11, 368);
            this.loadFileRGBttn.Name = "loadFileRGBttn";
            this.loadFileRGBttn.Size = new System.Drawing.Size(105, 36);
            this.loadFileRGBttn.TabIndex = 0;
            this.loadFileRGBttn.Text = "Load File";
            this.loadFileRGBttn.UseVisualStyleBackColor = true;
            this.loadFileRGBttn.Click += new System.EventHandler(this.loadFileRGBttn_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Controls.Add(this.setOfFinalStatesTxtBox);
            this.tabPage2.Controls.Add(this.transitionFunctionsRTxtBox);
            this.tabPage2.Controls.Add(this.convertToGrammerBttn);
            this.tabPage2.Controls.Add(this.readInputBttn);
            this.tabPage2.Controls.Add(this.initialStateTxtbox);
            this.tabPage2.Controls.Add(this.alphabetTxtBox);
            this.tabPage2.Controls.Add(this.finiteSetOfStatesTxtbox);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Controls.Add(this.label9);
            this.tabPage2.Controls.Add(this.label10);
            this.tabPage2.Controls.Add(this.ouputRichTextBox);
            this.tabPage2.Controls.Add(this.inputRichTextBox);
            this.tabPage2.Controls.Add(this.loadFileBttn);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(890, 426);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Finite Automata";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // convertToGrammerBttn
            // 
            this.convertToGrammerBttn.Location = new System.Drawing.Point(283, 364);
            this.convertToGrammerBttn.Name = "convertToGrammerBttn";
            this.convertToGrammerBttn.Size = new System.Drawing.Size(105, 36);
            this.convertToGrammerBttn.TabIndex = 38;
            this.convertToGrammerBttn.Text = "Convert to Grammer";
            this.convertToGrammerBttn.UseVisualStyleBackColor = true;
            this.convertToGrammerBttn.Click += new System.EventHandler(this.convertToGrammerBttn_Click);
            // 
            // readInputBttn
            // 
            this.readInputBttn.Location = new System.Drawing.Point(146, 364);
            this.readInputBttn.Name = "readInputBttn";
            this.readInputBttn.Size = new System.Drawing.Size(105, 36);
            this.readInputBttn.TabIndex = 37;
            this.readInputBttn.Text = "Read Inputs";
            this.readInputBttn.UseVisualStyleBackColor = true;
            this.readInputBttn.Click += new System.EventHandler(this.readInputBttn_Click);
            // 
            // initialStateTxtbox
            // 
            this.initialStateTxtbox.Location = new System.Drawing.Point(8, 127);
            this.initialStateTxtbox.Name = "initialStateTxtbox";
            this.initialStateTxtbox.Size = new System.Drawing.Size(248, 20);
            this.initialStateTxtbox.TabIndex = 34;
            // 
            // alphabetTxtBox
            // 
            this.alphabetTxtBox.Location = new System.Drawing.Point(8, 84);
            this.alphabetTxtBox.Name = "alphabetTxtBox";
            this.alphabetTxtBox.Size = new System.Drawing.Size(248, 20);
            this.alphabetTxtBox.TabIndex = 33;
            // 
            // finiteSetOfStatesTxtbox
            // 
            this.finiteSetOfStatesTxtbox.Location = new System.Drawing.Point(8, 41);
            this.finiteSetOfStatesTxtbox.Name = "finiteSetOfStatesTxtbox";
            this.finiteSetOfStatesTxtbox.Size = new System.Drawing.Size(248, 20);
            this.finiteSetOfStatesTxtbox.TabIndex = 32;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(8, 107);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 17);
            this.label6.TabIndex = 31;
            this.label6.Text = "Initial State";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(8, 150);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(126, 17);
            this.label8.TabIndex = 29;
            this.label8.Text = "Set Of Final States";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(8, 64);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(64, 17);
            this.label9.TabIndex = 28;
            this.label9.Text = "Alphabet";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(8, 21);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(140, 17);
            this.label10.TabIndex = 27;
            this.label10.Text = "A Finite Set of States";
            // 
            // ouputRichTextBox
            // 
            this.ouputRichTextBox.Location = new System.Drawing.Point(581, 20);
            this.ouputRichTextBox.Name = "ouputRichTextBox";
            this.ouputRichTextBox.Size = new System.Drawing.Size(292, 329);
            this.ouputRichTextBox.TabIndex = 26;
            this.ouputRichTextBox.Text = "";
            // 
            // inputRichTextBox
            // 
            this.inputRichTextBox.Location = new System.Drawing.Point(283, 21);
            this.inputRichTextBox.Name = "inputRichTextBox";
            this.inputRichTextBox.Size = new System.Drawing.Size(292, 328);
            this.inputRichTextBox.TabIndex = 25;
            this.inputRichTextBox.Text = "";
            // 
            // loadFileBttn
            // 
            this.loadFileBttn.Location = new System.Drawing.Point(11, 364);
            this.loadFileBttn.Name = "loadFileBttn";
            this.loadFileBttn.Size = new System.Drawing.Size(105, 36);
            this.loadFileBttn.TabIndex = 24;
            this.loadFileBttn.Text = "Load File";
            this.loadFileBttn.UseVisualStyleBackColor = true;
            this.loadFileBttn.Click += new System.EventHandler(this.loadFileBttn_Click);
            // 
            // getProductsForNonTerminalBttn
            // 
            this.getProductsForNonTerminalBttn.Location = new System.Drawing.Point(531, 368);
            this.getProductsForNonTerminalBttn.Name = "getProductsForNonTerminalBttn";
            this.getProductsForNonTerminalBttn.Size = new System.Drawing.Size(105, 36);
            this.getProductsForNonTerminalBttn.TabIndex = 24;
            this.getProductsForNonTerminalBttn.Text = "Get Products For Non-Terminal";
            this.getProductsForNonTerminalBttn.UseVisualStyleBackColor = true;
            this.getProductsForNonTerminalBttn.Click += new System.EventHandler(this.getProductsForNonTerminalBttn_Click);
            // 
            // transitionFunctionsRTxtBox
            // 
            this.transitionFunctionsRTxtBox.Location = new System.Drawing.Point(11, 224);
            this.transitionFunctionsRTxtBox.Name = "transitionFunctionsRTxtBox";
            this.transitionFunctionsRTxtBox.Size = new System.Drawing.Size(248, 125);
            this.transitionFunctionsRTxtBox.TabIndex = 39;
            this.transitionFunctionsRTxtBox.Text = "";
            // 
            // setOfFinalStatesTxtBox
            // 
            this.setOfFinalStatesTxtBox.Location = new System.Drawing.Point(8, 170);
            this.setOfFinalStatesTxtBox.Name = "setOfFinalStatesTxtBox";
            this.setOfFinalStatesTxtBox.Size = new System.Drawing.Size(248, 20);
            this.setOfFinalStatesTxtBox.TabIndex = 40;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(8, 204);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(136, 17);
            this.label7.TabIndex = 41;
            this.label7.Text = "Transition Functions";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(898, 452);
            this.Controls.Add(this.tabControl1);
            this.Name = "MainForm";
            this.Text = "Form1";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button loadFileRGBttn;
        private System.Windows.Forms.RichTextBox inputRTxtBox;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button readBttn;
        private System.Windows.Forms.TextBox getProductsForNonTerminalTxtbox;
        private System.Windows.Forms.TextBox startingSymbolTxtBox;
        private System.Windows.Forms.TextBox productsTxtBox;
        private System.Windows.Forms.TextBox terminalsTxtBox;
        private System.Windows.Forms.TextBox nonTerminalsTxtBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RichTextBox outputRTxtbox;
        private System.Windows.Forms.Button regularGrammerBttn;
        private System.Windows.Forms.Button getProductsForNonTerminalBttn;
        private System.Windows.Forms.Button convertToGrammerBttn;
        private System.Windows.Forms.Button readInputBttn;
        private System.Windows.Forms.TextBox initialStateTxtbox;
        private System.Windows.Forms.TextBox alphabetTxtBox;
        private System.Windows.Forms.TextBox finiteSetOfStatesTxtbox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.RichTextBox ouputRichTextBox;
        private System.Windows.Forms.RichTextBox inputRichTextBox;
        private System.Windows.Forms.Button loadFileBttn;
        private System.Windows.Forms.RichTextBox transitionFunctionsRTxtBox;
        private System.Windows.Forms.TextBox setOfFinalStatesTxtBox;
        private System.Windows.Forms.Label label7;
    }
}

