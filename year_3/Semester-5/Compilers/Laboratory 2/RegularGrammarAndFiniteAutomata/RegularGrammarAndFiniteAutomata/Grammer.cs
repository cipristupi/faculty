﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RegularGrammarAndFiniteAutomata
{
    public class Grammer
    {
        public string NonTerminals { get; set; } // A,B,C
        public string Terminals { get; set; } // a,b,c
        public string Products { get; set; } // A-aA|bA,B-epsilon|bB
        public string StartingSymbol { get; set; }

        public List<ProductItem> ProductsItems { get; set; }

        public override string ToString()
        {
            StringBuilder regularGrammerBuilder = new StringBuilder();
            regularGrammerBuilder.AppendLine(string.Format("N={0}", this.NonTerminals));
            regularGrammerBuilder.AppendLine(string.Format("E={0}", this.Terminals));
            regularGrammerBuilder.AppendLine(string.Format("S={0}", this.StartingSymbol));
            regularGrammerBuilder.Append(string.Format("P={0}  ",Products));
            foreach (ProductItem productsItem in ProductsItems)
            {
                regularGrammerBuilder.Append(string.Format(",   {0}-{1}",productsItem.NonTerminal,string.Join(",",productsItem.Combinations)));
            }
            return regularGrammerBuilder.ToString();
        }
    }

    public class ProductItem
    {
        public string NonTerminal { get; set; }
        public List<string> Combinations { get; set; } 
    }
}
