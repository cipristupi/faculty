﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace RegularGrammarAndFiniteAutomata
{
    public class RegularGrammerValidator
    {
        private const char Comma = ',';
        private const char OrizontalBar = '|';
        private const string EPSILON = "epsilon";
        public bool ValidateRegularGrammer(Grammer grammer)
        {
            return ValidateNonTerminals(grammer.NonTerminals) &&
                   ValidateStartingSymbol(grammer) &&
                   ValidateTerminals(grammer.Terminals) &&
                   ValidateProducts(grammer);
        }

        //Tested
        public bool ValidateNonTerminals(string nonTerminalsLine)
        {
            var nonTerminals = GetElements(nonTerminalsLine, Comma);
            return nonTerminals.All(nt => Regex.IsMatch(nt, "[A-Z]") && nt.Length == 1);
        }
        //Tested
        public bool ValidateTerminals(string terminalsLine)
        {
            Regex rg = new Regex(@"^[a-z0-9]*$");
            var terminals = GetElements(terminalsLine, Comma);
            return terminals.All(t => rg.IsMatch(t));
        }

        public bool ValidateProducts(Grammer grammer)
        {
            var nonTerminals = GetElements(grammer.NonTerminals, Comma);
            var terminals = GetElements(grammer.Terminals, Comma);
            var products = GetProducts(grammer.Products);
            string startingSymbol = grammer.StartingSymbol;
            bool containsEpsilon = false;
            string nonTerminalWhichContainsEpsilon = null;
            foreach (var productItem in products)
            {
                foreach (var combination in productItem.Combinations)
                {
                    if (CheckIfContainsEpsilon(combination))
                    {
                        nonTerminalWhichContainsEpsilon = productItem.NonTerminal;
                        containsEpsilon = true;
                    }
                    else
                    {
                        if (!ValidCombination(combination, nonTerminals, terminals))
                        {
                            return false;
                        }
                    }
                }
               
            }
            if (containsEpsilon)
            {
                if (nonTerminalWhichContainsEpsilon == startingSymbol)//If nonterminal which contains epsilon is starting symbol i check that other combination doesn't contain this nonterminal
                {
                    foreach (ProductItem item in products.Where(x => x.NonTerminal != nonTerminalWhichContainsEpsilon))//Here a get all combinations of nonterminals different that which contains epsilon
                    {
                        //here validate combinations to not contain the nonTerminal which contains epsilon because is invalid
                        if (CombinationContainNonTerminal(item.Combinations, nonTerminalWhichContainsEpsilon))
                        {
                            return false;
                        }
                    }
                }
                else
                {
                    return false;//A nonterminal which is not symbol table can not contain epsilon in combinations
                }

            }
            return true;
        }

        public bool CombinationContainNonTerminal(List<string> combinations, string nonTerminal)
        {
            return combinations.Any(combination => combination.Contains(nonTerminal));
        }


        public bool ValidCombination(string combination, List<string> nonTerminals, List<string> terminals)
        {

            var combinationAsCharArray = combination.ToCharArray();//get each element of combination as array
            var upperCaseLetters = combinationAsCharArray.Where(char.IsUpper).ToList(); //check for uppercase letters
            char existingUpperCaseLetter = '\0';
            if (upperCaseLetters.Count() > 1) return false;//check if are multiple nonterminals in combination
            if (upperCaseLetters.Count != 0)
            {
                existingUpperCaseLetter = upperCaseLetters[0];
                if (!nonTerminals.Contains(upperCaseLetters[0].ToString()))
                    return false; //check if upper letter is a nonterminal;
            }
            foreach (var source in combinationAsCharArray.Where(c => c != existingUpperCaseLetter).ToList())//here get only terminals from combination
            {
                if (!terminals.Contains(source.ToString()))//check if terminals from combination are in terminal list provided by Grammer
                {
                    return false;
                }
            }
            return true;
        }

        private bool CheckIfContainsEpsilon(string combination)
        {
            return combination.Contains(EPSILON) && combination.Length == EPSILON.Length;
        }


        //Tested
        /// <summary>
        /// Starting symbol must be a NonTerminal
        /// </summary>
        /// <param name="grammer"></param>
        /// <returns></returns>
        public bool ValidateStartingSymbol(Grammer grammer)
        {
            var nonTerminals = GetElements(grammer.NonTerminals, Comma);
            if (grammer.StartingSymbol.Length > 1)
            {
                return false;
            }
            if (!nonTerminals.Contains(grammer.StartingSymbol))
            {
                return false;
            }
            if (grammer.StartingSymbol.Contains(EPSILON))
            {
                return false;
            }
            return true;
        }

        public string GetProductsForNonTerminal(string nonTerminal, string products)
        {
            ProductItem item = GetProducts(products).FirstOrDefault(x => x.NonTerminal == nonTerminal);
            return string.Join(" , ", item.Combinations);
        }

        public List<string> GetElements(string line, char separator)
        {
            return new List<string>(line.Split(separator));
        }


        //Tested
        public List<ProductItem> GetProducts(string line)
        {
            var productsString = line.Split(',');
            ProductItem item;
            List<ProductItem> productsItemsList = new List<ProductItem>();
            foreach (var s in productsString)
            {
                var productStructure = s.Split('-');
                item = new ProductItem()
                {
                    NonTerminal = productStructure[0],
                    Combinations = new List<string>(productStructure[1].Split('|'))
                };
                productsItemsList.Add(item);
            }
            return productsItemsList;
        }
    }
}
