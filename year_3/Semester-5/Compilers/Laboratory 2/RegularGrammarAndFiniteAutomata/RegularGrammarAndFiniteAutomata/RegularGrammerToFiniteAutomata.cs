﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RegularGrammarAndFiniteAutomata
{
    public class RegularGrammerToFiniteAutomata
    {
        private const string K = "K";
        private const string EPSILON = "epsilon";
        private const char Comma = ',';
        private RegularGrammerValidator _regularGrammerValidator;

        public RegularGrammerToFiniteAutomata()
        {
            _regularGrammerValidator = new RegularGrammerValidator();
        }

        public FiniteAutomata ConvertRegularGrammerToFiniteAutomata(Grammer grammer)
        {
            FiniteAutomata automata = new FiniteAutomata
            {
                FiniteSetOfStates = grammer.NonTerminals + "," + K,
                Alphabet = grammer.Terminals,
                InitialState = grammer.StartingSymbol,
                SetOfFinalStates = K
            };
            if (StartingSymbolCombinationContainsEpsilon(grammer))
            {
                automata.SetOfFinalStates = string.Format("{0},{1}", automata.SetOfFinalStates, grammer.StartingSymbol);
            }
            automata.TransitionFunctionResults = GetTransitionFunctions(grammer);
            return automata;
        }

        public bool StartingSymbolCombinationContainsEpsilon(Grammer grammer)
        {
            var products = _regularGrammerValidator.GetProducts(grammer.Products);
            var productStartingSymbol = products.FirstOrDefault(x => x.NonTerminal == grammer.StartingSymbol);
            return productStartingSymbol.Combinations.Contains(EPSILON);
        }

        public List<Function> GetTransitionFunctions(Grammer grammer)
        {
            List<ProductItem> products = _regularGrammerValidator.GetProducts(grammer.Products);
            List<Function> functions = new List<Function>();
            Function functionResult;
            foreach (var product in products)
            {
                foreach (var combination in product.Combinations)
                {
                    functionResult = new Function { NonTerminal = product.NonTerminal };
                    var terminalsFromCombination = combination.ToCharArray().Where(char.IsLower).ToArray();
                    var nonTerminalsFromCombination = combination.ToCharArray().Where(char.IsUpper).ToArray();
                    functionResult.Terminal = new string(terminalsFromCombination);
                    if (nonTerminalsFromCombination.Length == 1)
                    {
                        functionResult.FinalState = new string(nonTerminalsFromCombination);
                    }
                    else
                    {
                        functionResult.FinalState = K;
                    }
                    if (combination != EPSILON)
                    {
                        functions.Add(functionResult);
                    }
                }
            }
            return functions;
        }

        public bool CheckTerminalAsCombinationWithOutNonTerminal(string terminal, List<string> combinationList)
        {
            foreach (var combination in combinationList)
            {
                if (combination.ToCharArray().Where(char.IsUpper).ToList().Count == 0)
                {
                    if (combination.Contains(terminal))
                    {
                        return true;
                    }
                }
            }
            return false;
        }


    }
}
