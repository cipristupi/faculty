﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RegularGrammarAndFiniteAutomata
{
    public class FiniteAutomataToRegularGrammer
    {
        private const string K = "K";
        private const string EPSILON = "epsilon";
        private const char Comma = ',';
        private RegularGrammerValidator _regularGrammerValidator;

        public FiniteAutomataToRegularGrammer()
        {
            _regularGrammerValidator = new RegularGrammerValidator();
        }

        public Grammer ConvertFiniteAutomataToRegularGrammer(FiniteAutomata finiteAutomata)
        {
            Grammer grammer = new Grammer();
            grammer.StartingSymbol = finiteAutomata.InitialState;
            grammer.NonTerminals = finiteAutomata.FiniteSetOfStates;
            grammer.Terminals = finiteAutomata.Alphabet;
            grammer.ProductsItems = GetProductsFromFiniteAutomata(finiteAutomata);
            return grammer;
        }

        public List<ProductItem> GetProductsFromFiniteAutomata(FiniteAutomata finiteAutomata)
        {
            List<ProductItem> productsItems = new List<ProductItem>();
            List<string> terminalStateList = new List<string>(finiteAutomata.SetOfFinalStates.Split(Comma));
            var distinctNonTerminalsFormTransitionFunctions =
                finiteAutomata.TransitionFunctionResults.Select(x => x.NonTerminal).Distinct().ToList();
            ProductItem item;
            List<string> listOfProductionsString = new List<string>();
            foreach (var nonterminal in distinctNonTerminalsFormTransitionFunctions)
            {
                item = new ProductItem { NonTerminal = nonterminal };
                var allFunctionsHavingTheGivenNonTerminal =
                    finiteAutomata.TransitionFunctionResults.Where(x => x.NonTerminal == nonterminal).ToList();
                if (finiteAutomata.SetOfFinalStates.Contains(nonterminal) && nonterminal == finiteAutomata.InitialState)
                {
                    listOfProductionsString.Add(EPSILON);
                }
                foreach (var function in allFunctionsHavingTheGivenNonTerminal)
                {
                    if (function.Terminal != function.FinalState)
                    {
                        listOfProductionsString.Add(string.Format("{0}{1}", function.Terminal, function.FinalState));
                    }
                    if (terminalStateList.Contains(function.FinalState))
                    {
                        listOfProductionsString.Add(function.Terminal);
                    }
                }
                item.Combinations = listOfProductionsString.Distinct().ToList();
                productsItems.Add(item);
                listOfProductionsString.Clear();
            }
            return productsItems;
        }

    }
}
