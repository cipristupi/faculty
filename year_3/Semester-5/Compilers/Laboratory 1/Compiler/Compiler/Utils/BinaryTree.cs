﻿using System.Collections.Generic;
using System.Linq;

namespace Compiler.Utils
{
    public class BinaryTree
    {

        Node _root;
        int _currentId;
        readonly List<Node> _nodes;
        public BinaryTree()
        {
            _currentId = 0;
            _nodes = new List<Node>();
        }
        /// <summary>
        /// Add new node to tree and returns the position of new node
        /// </summary>
        /// <param name="value">Value to be added to tree</param>
        /// <returns>Position of new node</returns>
        public int AddNode(string value)
        {
            var newNode = new Node
            {
                Id = _currentId,
                Value = value,
                LeftChild = -1,
                RightChild = -1,
                Parent = -1
            };
            if (!IsValueExisting(value))
            {
                if (_root == null)
                {
                    _root = newNode;
                    _nodes.Add(newNode);
                    _currentId++;
                }
                else
                {
                    var parent = GetParentForNewNode(_root, value);
                    if (IsRightChild(parent.Value, newNode.Value))
                    {
                        parent.RightChild = newNode.Id;
                    }
                    else
                    {
                        parent.LeftChild = newNode.Id;
                    }
                    newNode.Parent = parent.Id;
                    _nodes.Add(newNode);
                    _currentId++;
                }
                return newNode.Id;
            }
            return -2;
        }

        Node GetParentForNewNode(Node currentNode, string value)
        {
            if (IsLeaf(currentNode))
            {
                return currentNode;
            }
            if (string.Compare(currentNode.Value, value) > 0)
            {
                if (currentNode.LeftChild != -1)
                {
                    currentNode = GetParentForNewNode(GetNodeById(currentNode.LeftChild), value);
                }
                //return currentNode;
            }
            else
            {
                if (string.Compare(currentNode.Value, value) < 0)
                {
                    if (currentNode.RightChild != -1)
                    {
                        currentNode = GetParentForNewNode(GetNodeById(currentNode.RightChild), value);
                    }
                    //return currentNode;
                }
            }
            return currentNode;
        }

        bool IsRightChild(string parentValue, string childValue) => string.Compare(parentValue, childValue) < 0;


        Node GetNodeById(int id) => _nodes.FirstOrDefault(x => x.Id == id);


        /// <summary>
        /// Check if a node is leaf or not
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        bool IsLeaf(Node node) => node.LeftChild == -1 && node.RightChild == -1;

        bool IsPartialLeaf(Node node) => node.LeftChild != -1 || node.RightChild != -1;


        /// <summary>
        /// Check if given value exists as node
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool IsValueExisting(string value) => _nodes.Any(x => x.Value == value);

        /// <summary>
        /// Return all nodes
        /// </summary>
        /// <returns></returns>
        public List<Node> GetAll() => _nodes;
    }
}