﻿namespace Compiler.Utils
{
    public class Node
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public int LeftChild { get; set; }
        public int RightChild { get; set; }
        public int Parent { get; set; }
    }
}