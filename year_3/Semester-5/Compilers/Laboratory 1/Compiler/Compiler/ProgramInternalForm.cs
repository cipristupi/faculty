﻿using System;
using System.Collections.Generic;

namespace Compiler
{
    public class ProgramInternalForm
    {
        List<KeyValuePair<int, int>> _programInternalFormKeyValuePairs;

        public ProgramInternalForm()
        {
              _programInternalFormKeyValuePairs = new List<KeyValuePair<int, int>>();
        }

        public void Add(int tokenCode, int symbolTableId)
        {
            if (symbolTableId != -2)
            {
                _programInternalFormKeyValuePairs.Add(new KeyValuePair<int, int>(tokenCode, symbolTableId));
            }
        }

        public void RemoveByTokenCode(int tokenCode)
        {
            throw new NotImplementedException();
        }

        public void RemoveBySymbolTableId(int symbolTableId)
        {
            throw new NotImplementedException();
        }

        public List<KeyValuePair<int, int>> GetAll()
        {
            return _programInternalFormKeyValuePairs;
        }
    }
}