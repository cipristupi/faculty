﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Compiler.Utils;

namespace Compiler
{
    public class SymbolTable
    {
        private BinaryTree _binaryTree;
        public SymbolTable()
        {
            _binaryTree = new BinaryTree();
        }
        public int Add(string value)
        {
            return _binaryTree.AddNode(value);
        }

        public List<SymbolTableRow> GetSymbolTableRows()
        {
            return _binaryTree.GetAll().Select(SymbolTableRow.GetRowFromNode).ToList();
        }

        public SymbolTableRow GetSymbolTableRowById(int id)
        {
            var node = _binaryTree.GetAll().FirstOrDefault(x => x.Id == id);
            return node != null ? SymbolTableRow.GetRowFromNode(node) : null;
        }

        public IEnumerable<Node> GetTreeNodes()
        {
            return _binaryTree.GetAll();
        }
    }
}