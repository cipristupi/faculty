﻿using System;
using System.Collections.Generic;

namespace Compiler
{
    public static class Enums
    {
        public static Dictionary<string, int> CodifiedTokens
        {
            get
            {
                var dictionary = new Dictionary<string, int>
                {
                    {"identifier", 0},
                    {"constant", 1},
                    {"program", 2},
                    {"array", 3},
                    {"of", 4},
                    {"var", 5},
                    {"integer", 6},
                    {"real", 7},
                    {"bool", 8},
                    {"begin", 9},
                    {"end", 10},
                    {"read", 11},
                    {"write", 12},
                    {"do", 13},
                    {"if", 14},
                    {"then", 15},
                    {"else", 16},
                    {"string", 17},
                    {":", 18},
                    {";", 19},
                    {",", 20},
                    {".", 21},
                    {"+", 22},
                    {"*", 23},
                    {"(", 24},
                    {")", 25},
                    {"[", 26},
                    {"]", 27},
                    {"-", 28},
                    {"<", 29},
                    {">", 30},
                    {"=", 31},
                    {":=", 32},
                    {"while", 33},
                    {"{", 34},
                    {"}", 35},
                    {"/", 36},
                    {"_", 37},
                    {"char",38 }
                };
                return dictionary;
            }
        }


        public static Dictionary<string, int> ReservedWords
        {
            get
            {
                var dictionary = new Dictionary<string, int>
                {
                    {"identifier", 0},
                    {"constant", 1},
                    {"program", 2},
                    {"array", 3},
                    {"of", 4},
                    {"var", 5},
                    {"integer", 6},
                    {"real", 7},
                    {"bool", 8},
                    {"begin", 9},
                    {"end", 10},
                    {"read", 11},
                    {"write", 12},
                    {"do", 13},
                    {"if", 14},
                    {"then", 15},
                    {"else", 16},
                    {"string", 17},
                    {"while", 33},
                     {"char",38 }
                };
                return dictionary;
            }
        }

        public static Dictionary<string, int> SpecialCharacters
        {
            get
            {
                var dictionary = new Dictionary<string, int>
                {
                    {":", 18},
                    {";", 19},
                    {",", 20},
                    {".", 21},
                    {"+", 22},
                    {"*", 23},
                    {"(", 24},
                    {")", 25},
                    {"[", 26},
                    {"]", 27},
                    {"-", 28},
                    {"<", 29},
                    {">", 30},
                    {"=", 31},
                    {":=", 32},
                    {"{", 34},
                    {"}", 35},
                    {"/", 36},
                    {"_", 37}
                };

                return dictionary;
            }
        }
    }
}
