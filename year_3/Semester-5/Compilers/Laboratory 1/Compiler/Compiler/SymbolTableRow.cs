﻿using Compiler.Utils;

namespace Compiler
{
    public class SymbolTableRow
    {
        public int Id { get; set; }
        public string Value { get; set; }


        public static SymbolTableRow GetRowFromNode(Node node)
        {
            return new SymbolTableRow()
            {
                Id = node.Id,
                Value = node.Value
            };
        }
    }
}