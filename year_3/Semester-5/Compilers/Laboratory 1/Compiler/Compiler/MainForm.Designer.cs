﻿namespace Compiler
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.loadProgramBttn = new System.Windows.Forms.Button();
            this.analyseProgramBttn = new System.Windows.Forms.Button();
            this.programPathTxtbox = new System.Windows.Forms.TextBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.SuspendLayout();
            // 
            // loadProgramBttn
            // 
            this.loadProgramBttn.Location = new System.Drawing.Point(13, 13);
            this.loadProgramBttn.Name = "loadProgramBttn";
            this.loadProgramBttn.Size = new System.Drawing.Size(137, 36);
            this.loadProgramBttn.TabIndex = 0;
            this.loadProgramBttn.Text = "Load Program ";
            this.loadProgramBttn.UseVisualStyleBackColor = true;
            this.loadProgramBttn.Click += new System.EventHandler(this.loadProgramBttn_Click);
            // 
            // analyseProgramBttn
            // 
            this.analyseProgramBttn.Location = new System.Drawing.Point(13, 69);
            this.analyseProgramBttn.Name = "analyseProgramBttn";
            this.analyseProgramBttn.Size = new System.Drawing.Size(137, 36);
            this.analyseProgramBttn.TabIndex = 1;
            this.analyseProgramBttn.Text = "Analyze Program ";
            this.analyseProgramBttn.UseVisualStyleBackColor = true;
            this.analyseProgramBttn.Click += new System.EventHandler(this.analyseProgramBttn_Click);
            // 
            // programPathTxtbox
            // 
            this.programPathTxtbox.Location = new System.Drawing.Point(168, 13);
            this.programPathTxtbox.Name = "programPathTxtbox";
            this.programPathTxtbox.ReadOnly = true;
            this.programPathTxtbox.Size = new System.Drawing.Size(449, 20);
            this.programPathTxtbox.TabIndex = 2;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.saveFileDialog1_FileOk);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(629, 127);
            this.Controls.Add(this.programPathTxtbox);
            this.Controls.Add(this.analyseProgramBttn);
            this.Controls.Add(this.loadProgramBttn);
            this.Name = "MainForm";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button loadProgramBttn;
        private System.Windows.Forms.Button analyseProgramBttn;
        private System.Windows.Forms.TextBox programPathTxtbox;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
    }
}

