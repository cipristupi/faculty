﻿using System.Collections.Generic;

namespace Compiler.ScannerImplementation
{
    public class ScannerResult
    {
        public ProgramInternalForm ProgramInternalFormResult { get; set; }
        public SymbolTable IdentifiersSymbolTable { get; set; }
        public SymbolTable ConstantsSymbolTable { get; set; }

        public List<KeyValuePair<string,int>> LexicalError { get; set; } 
    }
}
