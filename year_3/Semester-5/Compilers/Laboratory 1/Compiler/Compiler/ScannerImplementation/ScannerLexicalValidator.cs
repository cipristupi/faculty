﻿using System.Linq;
using System.Text.RegularExpressions;

namespace Compiler.ScannerImplementation
{
    public static class ScannerLexicalValidator
    {
        private const char SINGLEQUOTE = '\'';
        private const char QUOTE = '\"';
        private const char UNDERLINE = '_';

        public static bool ContainsSpecialCharacters(string token)
        {
            bool containsSpecialCharacter = false;
            foreach (var s in Enums.SpecialCharacters.Select(x => x.Key))
            {
                if (token.Contains(s))
                {
                    containsSpecialCharacter = true;
                }
            }
            return containsSpecialCharacter;
        }

        public static bool StartWithUnderline(string token) => token[0] == '_';

        public static bool StringContainsOnlyLettersAndNumber(string token)
        {
            //TODO regex for number | numbers | letter | letters | (letters && number)
            Regex r = new Regex("^[a-zA-Z0-9]*$");
            return r.IsMatch(token);
        }

        public static bool IsReservedWord(string token)
        {
            return Enums.ReservedWords.ContainsKey(token);
        }

        public static bool IsSpecialCharacter(string token)
        {
            return Enums.SpecialCharacters.ContainsKey(token);
        }

        public static bool IsValidIdentifier(string token)
        {
            if (token.Length > 250)
            {
                return false;
            }
            if (token[0] == UNDERLINE)
            {
                var tokenWithotUndeline = token.Remove(0, 1);
                return StringContainsOnlyLettersAndNumber(tokenWithotUndeline);
            }
            int number;
            if(int.TryParse(token[0].ToString(),out number))
            {
                return false;
            }
            return StringContainsOnlyLettersAndNumber(token);
        }

        public static bool IsValidConstant(string token)
        {
            int intResult;
            float floatResult;
            string tokenWithoutQuoates;
            if (int.TryParse(token, out intResult))//validate int constants
            {
                return true;
            }
            if (float.TryParse(token, out floatResult))//validate float constants
            {
                return false;
            }
            if (token[0] == SINGLEQUOTE)//validate for char constant
            {
                if (!token.EndsWith(SINGLEQUOTE.ToString()))
                {
                    return false;
                }
                tokenWithoutQuoates = ScannerHelpers.RemoveFirstAndLastCharacter(token, SINGLEQUOTE);
                if (tokenWithoutQuoates.Length > 1)
                {
                    return false;
                }
                return StringContainsOnlyLettersAndNumber(tokenWithoutQuoates);
            }
            if (token[0] == QUOTE)
            {
                if (!token.EndsWith(QUOTE.ToString())) return false;
                tokenWithoutQuoates = ScannerHelpers.RemoveFirstAndLastCharacter(token, SINGLEQUOTE);
                return StringContainsOnlyLettersAndNumber(tokenWithoutQuoates);
            }
            return false;
        }
    }
}
