﻿using System.Collections.Generic;
using System.IO;

namespace Compiler.ScannerImplementation
{
    public class Scanner
    {
        private string _programPath;

        public Scanner(string path)
        {
            _programPath = path;
        }

        public ScannerResult AnalyzeProgram(bool singleSymbolTable = false)
        {
            List<string> lexicalError = new List<string>();
            SymbolTable identifiersSymbolTable =new SymbolTable();
            SymbolTable constantsSymbolTable = new SymbolTable();
            ProgramInternalForm programInternalForm =new ProgramInternalForm();
            List<KeyValuePair<string,int>> lexicalErrorResult = new List<KeyValuePair<string, int>>();
            var lines = File.ReadAllLines(_programPath);
            for (int i = 0; i < lines.Length; i++)
            {
                string line = lines[i];
                var tokensFromLine = ScannerHelpers.GetTokensFromLine(line);
                if (singleSymbolTable)
                {
                    ClassifyTokensSingleSymbolTable(tokensFromLine, programInternalForm, identifiersSymbolTable,
                        lexicalError);
                }
                else
                {
                    ClassifyTokensDoubleSymbolTable(tokensFromLine, programInternalForm, identifiersSymbolTable,
                        constantsSymbolTable, lexicalError);
                }
                foreach (var error in lexicalError)
                {
                    lexicalErrorResult.Add(new KeyValuePair<string, int>(error, i));
                }
                lexicalError.Clear();
            }

            return new ScannerResult()
            {
               ProgramInternalFormResult = programInternalForm,
               IdentifiersSymbolTable = identifiersSymbolTable,
               ConstantsSymbolTable = constantsSymbolTable,
               LexicalError = lexicalErrorResult
            };
        }

        public void ClassifyTokensDoubleSymbolTable(List<string> tokens,
            ProgramInternalForm programInternalForm, SymbolTable identifierSymbolTable, SymbolTable constantsSymbolTable, List<string> lexicalError)
        {
            int tokenSymbolTableId;
            foreach (string token in tokens)
            {
                if (ScannerLexicalValidator.IsReservedWord(token))
                {
                    programInternalForm.Add(ScannerHelpers.GetCodeForToken(token), -1);
                }
                else
                {
                    if (ScannerLexicalValidator.IsSpecialCharacter(token))
                    {
                        programInternalForm.Add(ScannerHelpers.GetCodeForToken(token), -1);
                    }
                    else
                    {
                        if (ScannerLexicalValidator.IsValidIdentifier(token))
                        {
                            tokenSymbolTableId = identifierSymbolTable.Add(token);
                            programInternalForm.Add(ScannerHelpers.GetCodeForToken("identifier"), tokenSymbolTableId);
                        }
                        else
                        {
                            if (ScannerLexicalValidator.IsValidConstant(token))
                            {
                                tokenSymbolTableId = constantsSymbolTable.Add(token);
                                programInternalForm.Add(ScannerHelpers.GetCodeForToken("constant"), tokenSymbolTableId);
                            }
                            else
                            {
                                lexicalError.Add(token);
                            }
                        }
                    }
                }
            }
        }

        public void ClassifyTokensSingleSymbolTable(List<string> tokens,
            ProgramInternalForm programInternalForm, SymbolTable symbolTable, List<string> syntaxError)
        {
            int tokenSymbolTableId;
            foreach (string token in tokens)
            {
                if (ScannerLexicalValidator.IsReservedWord(token))
                {
                    programInternalForm.Add(ScannerHelpers.GetCodeForToken(token), -1);
                }
                else
                {
                    if (ScannerLexicalValidator.IsSpecialCharacter(token))
                    {
                        programInternalForm.Add(ScannerHelpers.GetCodeForToken(token), -1);
                    }
                    else
                    {
                        if (ScannerLexicalValidator.IsValidIdentifier(token))
                        {
                            tokenSymbolTableId = symbolTable.Add(token);
                            programInternalForm.Add(ScannerHelpers.GetCodeForToken("identifier"), tokenSymbolTableId);
                        }
                        else
                        {
                            if (ScannerLexicalValidator.IsValidConstant(token))
                            {
                                tokenSymbolTableId = symbolTable.Add(token);
                                programInternalForm.Add(ScannerHelpers.GetCodeForToken("constant"), tokenSymbolTableId);
                            }
                            else
                            {
                                syntaxError.Add(token);
                            }
                        }
                    }
                }
            }
        }
    }
}