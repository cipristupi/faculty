﻿using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Compiler.ScannerImplementation
{
    public static class ScannerHelpers
    {

        public static List<string> GetTokensFromLine(string line)
        {
            var tokens = new List<string>(line.Split(' '));
            int lastIndexForToken = tokens.Count - 1;
            var lastToken = tokens[lastIndexForToken];
            if (lastToken.EndsWith(";") && lastToken != ";")
            {
                tokens[lastIndexForToken] = tokens[lastIndexForToken].Remove(lastToken.LastIndexOf(";"), 1);
                tokens.Add(";");
            }
            for (int i = 0; i < tokens.Count; i++)
            {
                tokens[i] = Regex.Replace(tokens[i], @"\t|\n", "");
            }
            return tokens;
        }

        public static string RemoveFirstAndLastCharacter(string token, char character)
        {
            var newToken = token;
            if (newToken[0] == character)
            {
                newToken = newToken.Remove(0, 1);
            }
            if (token.EndsWith(character.ToString()))
            {
                var lastPosition = newToken.LastIndexOf(character);
                newToken = newToken.Remove(lastPosition, 1);
            }
            return newToken;
        }

        public static int GetCodeForToken(string token) => Enums.CodifiedTokens[token];
    }
}
