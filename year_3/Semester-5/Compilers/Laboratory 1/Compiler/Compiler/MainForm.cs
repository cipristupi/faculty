﻿using System;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Windows.Forms;
using Compiler.ScannerImplementation;

namespace Compiler
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void loadProgramBttn_Click(object sender, EventArgs e)
        {
            openFileDialog1.DefaultExt = "*.txt";
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK) // Test result.
            {
                programPathTxtbox.Text = openFileDialog1.FileName;
            }
            Console.WriteLine(result); // <-- For debugging use.
        }
        ScannerResult scannerResult;

        private void analyseProgramBttn_Click(object sender, EventArgs e)
        {
            if (programPathTxtbox.Text.Length != 0)
            {
                var scanner = new Scanner(programPathTxtbox.Text);
                scannerResult = scanner.AnalyzeProgram();
                saveFileDialog1.ShowDialog();
            }
            else
            {
                MessageBox.Show("Please Load Program.");
            }
        }

        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            string name = saveFileDialog1.FileName;
            if (scannerResult != null)
            {
                File.WriteAllText(name, GetStringFromScannerResult());
            }
        }

        private string GetStringFromScannerResult()
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendLine("Program Internal Form");
            string row;
            row = string.Format("{0,-20} {1,-20}", "Token Code", "Symbol Table Id");
            stringBuilder.AppendLine(row);
            foreach (var programInternalFormEntry in scannerResult.ProgramInternalFormResult.GetAll())
            {
                row = string.Format("{0,-20} {1,-20}", programInternalFormEntry.Key, programInternalFormEntry.Value);
                stringBuilder.AppendLine(row);
            }
            stringBuilder.AppendLine();
            stringBuilder.AppendLine("Identifiers Symbol Table");
            row = string.Format("{0,-20} {1,-20} {2,-20} {3,-20}", "Symbol Table Id", "Value","Left Child","Right Child");
            stringBuilder.AppendLine(row);
            foreach (var identifier in scannerResult.IdentifiersSymbolTable.GetTreeNodes())
            {
                row = string.Format("{0,-20} {1,-20} {2,-20} {3,-20}", identifier.Id, identifier.Value,identifier.LeftChild,identifier.RightChild);
                stringBuilder.AppendLine(row);
            }

            stringBuilder.AppendLine();
            stringBuilder.AppendLine("Constants Symbol Table");
            row = string.Format("{0,-20} {1,-20} {2,-20} {3,-20}", "Symbol Table Id", "Value", "Left Child", "Right Child");
            stringBuilder.AppendLine(row);
            foreach (var constant in scannerResult.ConstantsSymbolTable.GetTreeNodes())
            {
                row = string.Format("{0,-20} {1,-20} {2,-20} {3,-20}", constant.Id, constant.Value, constant.LeftChild, constant.RightChild);
                stringBuilder.AppendLine(row);
            }


            stringBuilder.AppendLine();
            stringBuilder.AppendLine("Lexical Errors");
            row = string.Format("{0,-20} {1,-20} ", "Error", "Line");
            stringBuilder.AppendLine(row);
            foreach (var error in scannerResult.LexicalError)
            {
                row = string.Format("{0,-20} {1,-20} ", error.Key,error.Value);
                stringBuilder.AppendLine(row);
            }
            return stringBuilder.ToString();
        }
    }
}
