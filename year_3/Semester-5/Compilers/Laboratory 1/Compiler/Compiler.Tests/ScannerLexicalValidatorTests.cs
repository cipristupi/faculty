﻿using Compiler.ScannerImplementation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Compiler.Tests
{
    [TestClass]
    public class ScannerLexicalValidatorTests
    {
        #region Contains Special Characters

        [TestMethod]
        public void ScannerLexicalValidator_ContainsSpecialCharacters_Contains()
        {
            //Arrange
            string token = "token+token";
            //Act
            bool result = ScannerLexicalValidator.ContainsSpecialCharacters(token);
            //Assert
            Assert.IsTrue(result);
        }
        [TestMethod]
        public void ScannerLexicalValidator_ContainsSpecialCharacters_NoContains()
        {

            //Arrange
            string token = "token";
            //Act
            bool result = ScannerLexicalValidator.ContainsSpecialCharacters(token);
            //Assert
            Assert.IsFalse(result);
        }
        #endregion

        #region StartWithUnderline

        [TestMethod]
        public void ScannerLexicalValidator_StartWithUnderline_IsStartingWithUnderline()
        {
            //Arrange
            string token = "_tokenhere";
            //Act
            bool result = ScannerLexicalValidator.StartWithUnderline(token);
            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void ScannerLexicalValidator_StartWithUnderline_IsNotStartingWithUnderline()
        {
            //Arrange
            string token = "tokenhere";
            //Act
            bool result = ScannerLexicalValidator.StartWithUnderline(token);
            //Assert
            Assert.IsFalse(result);
        }
        #endregion

        #region StringContainsOnlyLettersAndNumber

        [TestMethod]
        public void ScannerLexicalValidator_StringContainsOnlyLettersAndNumber_True()
        {
            //Arrange
            string token = "token1token1";
            //Act
            bool result = ScannerLexicalValidator.StringContainsOnlyLettersAndNumber(token);

            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void ScannerLexicalValidator_StringContainsOnlyLettersAndNumber_False()
        {
            //Arrange
            string token = "token1+token2";
            //Act
            bool result = ScannerLexicalValidator.StringContainsOnlyLettersAndNumber(token);

            //Assert
            Assert.IsFalse(result);
        }
        #endregion

        #region IsReservedWord
        [TestMethod]
        public void ScannerLexicalValidator_IsReservedWord_True()
        {
            //Arrange
            string token = "bool";
            //Act
            bool result = ScannerLexicalValidator.IsReservedWord(token);

            //Assert
            Assert.IsTrue(result);
        }
        [TestMethod]
        public void ScannerLexicalValidator_IsReservedWord_False()
        {
            //Arrange
            string token = "token1+token2";
            //Act
            bool result = ScannerLexicalValidator.IsReservedWord(token);

            //Assert
            Assert.IsFalse(result);
        }
        #endregion

        #region IsSpecialCharacter
        [TestMethod]
        public void ScannerLexicalValidator_IsSpecialCharacter_True()
        {
            //Arrange
            string token = "[";
            //Act
            bool result = ScannerLexicalValidator.IsReservedWord(token);

            //Assert
            Assert.IsFalse(result);
        }
        [TestMethod]
        public void ScannerLexicalValidator_IsSpecialCharacter_False()
        {
            //Arrange
            string token = "token1+token2";
            //Act
            bool result = ScannerLexicalValidator.IsReservedWord(token);

            //Assert
            Assert.IsFalse(result);
        }
        #endregion

        #region IsValidIdentifier

        [TestMethod]
        public void ScannerLexicalValidator_IsValidIdentifier_StartWithUndescore_Valid()
        {
            //Arrange
            string token = "_token";
            //Act
            bool result = ScannerLexicalValidator.IsValidIdentifier(token);
            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void ScannerLexicalValidator_IsValidIdentifier_StartWithNumber_InValid()
        {
            //Arrange
            string token = "123token";
            //Act
            bool result = ScannerLexicalValidator.IsValidIdentifier(token);
            //Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void ScannerLexicalValidator_IsValidIdentifier_ContainsSpecialCharacters_InValid()
        {
            //Arrange
            string token = "_to+ken+";
            //Act
            bool result = ScannerLexicalValidator.IsValidIdentifier(token);
            //Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void ScannerLexicalValidator_IsValidIdentifier_EndsWithUnderscor_InValid()
        {
            //Arrange
            string token = "token123token_";
            //Act
            bool result = ScannerLexicalValidator.IsValidIdentifier(token);
            //Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void ScannerLexicalValidator_IsValidIdentifier_ToLongIdentifier_InValid()
        {
            //Arrange
            string token = "loremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremlorem";
            //Act
            bool result = ScannerLexicalValidator.IsValidIdentifier(token);
            //Assert
            Assert.IsFalse(result);
        }
        #endregion

        #region IsValidConstant

        [TestMethod]
        public void ScannerLexicalValidator_IsValidConstant_IntegerConstant_Valid()
        {
            //Arrange
            string token = "10";
            //Act
            bool result = ScannerLexicalValidator.IsValidConstant(token);
            //Assert

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void ScannerLexicalValidator_IsValidConstant_IntegerConstantStartWithPlus_Valid()
        {
            //Arrange
            string token = "+10";
            //Act
            bool result = ScannerLexicalValidator.IsValidConstant(token);
            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void ScannerLexicalValidator_IsValidConstant_IntegerConstantStartWithMinus_Valid()
        {
            //Arrange
            string token = "-10";
            //Act
            bool result = ScannerLexicalValidator.IsValidConstant(token);
            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void ScannerLexicalValidator_IsValidConstant_RealConstant_InValid()
        {
            //Arrange
            string token = "10.5";
            //Act
            bool result = ScannerLexicalValidator.IsValidConstant(token);
            //Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void ScannerLexicalValidator_IsValidConstant_SingleNumberChar_Valid()
        {
            //Arrange
            string token = "\'1\'";
            //Act
            bool result = ScannerLexicalValidator.IsValidConstant(token);
            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void ScannerLexicalValidator_IsValidConstant_SingleLetterChar_Valid()
        {
            //Arrange
            string token = "\'a\'";
            //Act
            bool result = ScannerLexicalValidator.IsValidConstant(token);
            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void ScannerLexicalValidator_IsValidConstant_SpecialCharacterChar_InValid()
        {
            //Arrange
            string token = "\'+\'";
            //Act
            bool result = ScannerLexicalValidator.IsValidConstant(token);
            //Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void ScannerLexicalValidator_IsValidConstant_MultipleLettersChar_InValid()
        {
            //Arrange
            string token = "\'aa\'";
            //Act
            bool result = ScannerLexicalValidator.IsValidConstant(token);
            //Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void ScannerLexicalValidator_IsValidConstant_MultipleNumberChar_InValid()
        {
            //Arrange
            string token = "\'11\'";
            //Act
            bool result = ScannerLexicalValidator.IsValidConstant(token);
            //Assert
            Assert.IsFalse(result);
        }
        [TestMethod]
        public void ScannerLexicalValidator_IsValidConstant_CharStartWithSingleQuoteButNoFinalQuote_InValid()
        {
            //Arrange
            string token = "\'a";
            //Act
            bool result = ScannerLexicalValidator.IsValidConstant(token);
            //Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void ScannerLexicalValidator_IsValidConstant_StringContainingSpecialChar_InValid()
        {
            //Arrange
            string token = "\"+\"";
            //Act
            bool result = ScannerLexicalValidator.IsValidConstant(token);
            //Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void ScannerLexicalValidator_IsValidConstant_StringStartWithQuoteButNoFinalQuote_Valid()
        {
            //Arrange
            string token = "\"token";
            //Act
            bool result = ScannerLexicalValidator.IsValidConstant(token);
            //Assert
            Assert.IsFalse(result);
        }
        #endregion
    }
}
