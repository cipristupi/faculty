﻿using Compiler.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Compiler.Tests
{
    [TestClass]
    public class BinaryTreeTests
    {
        [TestMethod]
        public void BinarySearchTree_AddNode_OneNode_ValidRightLeftChild()
        {
            //Arrange
            var binaryTree = new BinaryTree();
            //Act
            int id = binaryTree.AddNode("ana");
            var allNodes = binaryTree.GetAll();
            //Assert
            var node = allNodes[0];
            Assert.AreEqual(0, id);
            Assert.AreEqual(1, allNodes.Count);
            Assert.AreEqual("ana", node.Value);
            Assert.AreEqual(0, node.Id);
            Assert.AreEqual(-1, node.RightChild);
            Assert.AreEqual(-1, node.Parent);
            Assert.AreEqual(-1, node.LeftChild);
        }

        [TestMethod]
        public void BinarySearchTree_AddNode_ThreeNodes()
        {
            //Arrange
            var binaryTree = new BinaryTree();
            //Act
            string rootNodeValue = "node", leftNodeValue = "aaa", rightNodeValue = "zzzz";
            binaryTree.AddNode(rootNodeValue);
            binaryTree.AddNode(leftNodeValue);
            binaryTree.AddNode(rightNodeValue);
            var allNodes = binaryTree.GetAll();
            //Assert
            var rootNode = allNodes[0];
            var leftNode = allNodes[1];
            var rightNode = allNodes[2];
            Assert.AreEqual(allNodes.Count, 3);
            Assert.AreEqual(rootNode.Value, rootNodeValue);
            Assert.AreEqual(rootNode.Id, 0);
            Assert.AreEqual(rootNode.RightChild, 2);
            Assert.AreEqual(rootNode.Parent, -1);
            Assert.AreEqual(rootNode.LeftChild, 1);


            Assert.AreEqual(leftNode.Value, leftNodeValue);
            Assert.AreEqual(leftNode.Id, 1);
            Assert.AreEqual(leftNode.RightChild, -1);
            Assert.AreEqual(leftNode.Parent, 0);
            Assert.AreEqual(leftNode.LeftChild, -1);

            Assert.AreEqual(rightNode.Value, rightNodeValue);
            Assert.AreEqual(rightNode.Id, 2);
            Assert.AreEqual(rightNode.RightChild, -1);
            Assert.AreEqual(rightNode.Parent, 0);
            Assert.AreEqual(rightNode.LeftChild, -1);

        }
    }
}