﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using Compiler.ScannerImplementation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Compiler.Tests
{
    [TestClass]
    public class ScannerHelpersTests
    {
        private const char SINGLEQUOTE = '\'';
        private const char QUOTE = '\"';
        [TestMethod]
        public void ScannerHelpers_GetTokensFromLine_ReturnAllTokens()
        {
            //Arrange
            string line = "var bool x ;";
            //Act
            var result = ScannerHelpers.GetTokensFromLine(line);
            //Assert
            Assert.AreEqual(4,result.Count);
            Assert.AreEqual("var",result[0]);
            Assert.AreEqual("bool", result[1]);
            Assert.AreEqual("x",result[2]);
            Assert.AreEqual(";",result[3]);
        }
       [TestMethod]
        public void ScannerHelpers_RemoveFirstAndLastCharacter_SingleQuoteCharacterToBeRemove()
        {
            //Arrange
            string token = "\'token\'";
            //Act
            var result = ScannerHelpers.RemoveFirstAndLastCharacter(token, SINGLEQUOTE);
            //Assert
            Assert.AreEqual("token", result);
           
        }


        [TestMethod]
        public void ScannerHelpers_RemoveFirstAndLastCharacter_QuoteCharacterToBeRemove()
        {
            //Arrange
            string token = "\"token\"";
            //Act
            var result = ScannerHelpers.RemoveFirstAndLastCharacter(token, QUOTE);
            //Assert
            Assert.AreEqual("token", result);
        }


        [TestMethod]
        public void ScannerHelpers_GetCodeForToken_ValidToken()
        {
            //Arrange
            string token = "bool";
            //Act
            var result = ScannerHelpers.GetCodeForToken(token);
            //Assert
            Assert.AreEqual(8, result);
        }

        [TestMethod]
        [ExpectedException(typeof (KeyNotFoundException))]
        public void ScannerHelpers_GetCodeForToken_InValidToken()
        {
            //Arrange
            string token = "token";
            //Act
            var result = ScannerHelpers.GetCodeForToken(token);
           
        }
    }
}
