﻿using System.Windows.Forms;

namespace Parser
{
    public class ParseResult
    {
        public ParseMessages Message { get; set; }
        public string ProductionsString { get; set; }
    }
}