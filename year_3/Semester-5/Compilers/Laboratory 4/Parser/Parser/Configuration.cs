﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parser
{
    public class Configuration
    {
        public int State { get; set; }
        public int CurrentSymbolPosition { get; set; } // i
        public Stack<WorkItem> AlfaStack { get; set; }  //Working Stack
        public Stack<string> BetaStack { get; set; } //Input Stack

        public Configuration()
        {
            AlfaStack = new Stack<WorkItem>();
            BetaStack = new Stack<string>();
        }
        public Configuration(int state, int currentSymbolPosition, WorkItem initialWorkItem, string initialInputStackItem)
        {
            State = state;
            CurrentSymbolPosition = currentSymbolPosition;
            AlfaStack = new Stack<WorkItem>();
            AlfaStack.Push(initialWorkItem);
            BetaStack = new Stack<string>();
            BetaStack.Push(initialInputStackItem);
        }

        public void AddAlfaItem(string value, int index)
        {
            AlfaStack.Push(new WorkItem()
            {
                Value = value,
                Index = index
            });
        }

        public override string ToString()
        {
            string state = string.Empty;
            string alphaStack = string.Empty;
            string betaStack = string.Empty;

            foreach (WorkItem wi in AlfaStack)
            {
                if (wi.Value != "epsilon")
                {
                    if (char.IsUpper(wi.Value[0]))
                        alphaStack = string.Format("{0}{1}", wi.Value, wi.Index) + alphaStack;
                    else
                        alphaStack = string.Format("{0}", wi.Value) + alphaStack;
                }
                if (alphaStack.Length == 0)
                    alphaStack = "epslion";
            }

            if (BetaStack.Count == 0)
                betaStack = "epsilon";
            else
                betaStack = string.Format("{0}", BetaStack.Aggregate((i, j) => i + j));

            string enumName = Enum.GetName(typeof(States), State);
            switch (enumName)//De ce ai comparat State cu string?
            {
                case "NormalState":
                    state = "q"; break;
                case "BackState":
                    state = "b"; break;
                case "ErrorState":
                    state = "e"; break;
                case "FinalState":
                    state = "f"; break;
            }

            string configuration = string.Format("({0}, {1}, {2}, {3}) |-",
                state, CurrentSymbolPosition.ToString(), alphaStack, betaStack);
            return configuration;
        }

    }
}
