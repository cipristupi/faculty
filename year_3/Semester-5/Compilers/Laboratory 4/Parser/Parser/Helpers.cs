﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Parser
{
    static class Helpers
    {
        public static bool CheckIfWorkingItemIsNonTerminal(WorkItem workItem)
        {
            return Regex.IsMatch(workItem.Value, "[A-Z]") && workItem.Value.Length == 1;
        }

        public static bool CheckIfWorkingItemIsTerminal(WorkItem workItem)
        {
            Regex rg = new Regex(@"^[a-z0-9]*$");
            return rg.IsMatch(workItem.Value);
        }

        public static bool CheckIfInputItemIsNonTerminal(string inputItem)
        {
            return Regex.IsMatch(inputItem, "[A-Z]");
        }

        public static int GetIndexForProduction(Grammer grammer, string nonTerminal,string production)
        {
            ProductItem productionForNonTerminal = grammer.Products.FirstOrDefault(x => x.NonTerminal == nonTerminal.ToString());
            for (int i = 0; i < productionForNonTerminal.Combinations.Count; i++)
            {
                var currencCombination = productionForNonTerminal.Combinations[i];
                if (currencCombination == production)
                {
                    return i;
                }
            }
            return 0;
        }

        public static string GetProductionByIndex(Grammer grammer, string nonTerminal, int index)
        {
            ProductItem productionForNonTerminal = grammer.Products.FirstOrDefault(x => x.NonTerminal == nonTerminal);
            for (int i = 0; i < productionForNonTerminal.Combinations.Count; i++)
            {
                var currencCombination = productionForNonTerminal.Combinations[i];
                if (i == index)
                {
                    return currencCombination;
                }
            }
            return string.Empty;
        }
    }
}
