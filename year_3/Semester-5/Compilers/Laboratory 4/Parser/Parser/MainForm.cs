﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Parser
{
    public partial class MainForm : Form
    {
        private OpenFileDialog ofd;
        private Grammer grammar;
        private string sequence;
        public MainForm()
        {
            InitializeComponent();
        }

        private void GetGrammarBttn_Click(object sender, EventArgs e)
        {
            ofd = new OpenFileDialog();
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                string filePath = ofd.SafeFileName;
                if (Path.GetExtension(filePath) == ".txt")
                {
                    try
                    {
                        string[] lines = System.IO.File.ReadAllLines(filePath);
                        CreateGrammar(lines);
                        GrammarTxtBox.Text = grammar.ToString();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error reading file: " + ex.ToString());
                        //throw;
                    }
                    finally
                    {
                        ofd.Dispose();
                    }
                }
            }
        }

        private void CreateGrammar(string[] lines)
        {
            grammar = new Grammer();
            grammar.StartingSymbol = lines[0];
            grammar.NonTerminals = lines[1].Split(' ').ToList();
            grammar.Terminals = lines[2].Split(' ').ToList();
            grammar.Products = new List<ProductItem>();

            for(int i = 3; i < lines.Count(); i++)
            {
                var splittedProduction = lines[i].Split(new string[] { "->" }, StringSplitOptions.None);
                ProductItem pi = new ProductItem()
                {
                    NonTerminal = splittedProduction[0],
                    Combinations = splittedProduction[1].Split('|').ToList()
                };
                grammar.Products.Add(pi);
            }
        }

        private void ParsingBttn_Click(object sender, EventArgs e)
        {
            ParsingEngine parsingEngine = new ParsingEngine();
            var result = parsingEngine.ParserRecursiveDescendent(grammar, sequence);
            MessageBox.Show(result.Message.ToString());
            ParsingTxtBox.Text = parsingEngine.ParsingSteps;
            ProductionStringTxtBox.Text = result.ProductionsString;
        }

        private void GetSequenceBttn_Click(object sender, EventArgs e)
        {
            ofd = new OpenFileDialog();
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                string filePath = ofd.SafeFileName;
                if (Path.GetExtension(filePath) == ".txt")
                {
                    try
                    {
                        string[] lines = System.IO.File.ReadAllLines(filePath);
                        foreach(string line in lines)
                        {
                            string[] parts = line.Split(' ');
                            sequence += parts[0];
                        }
                        SequenceLabel.Text = "Sequence: " + sequence;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error reading file: " + ex.ToString());
                        //throw;
                    }
                    finally
                    {
                        ofd.Dispose();
                    }
                }
            }
        }
    }
}
