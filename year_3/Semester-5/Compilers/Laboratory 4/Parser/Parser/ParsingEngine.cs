﻿using System;
using System.Text;

namespace Parser
{
    public class ParsingEngine
    {
        public string ParsingSteps = string.Empty;
        public ParseResult ParserRecursiveDescendent(Grammer grammer, string sequence)
        {
            WorkItem initialWorkItem = new WorkItem()
            {
                Value = "epsilon",
                Index = 0
            };
            int sequenceLength = sequence.Length;
            Configuration configuration = new Configuration(
                (int)States.NormalState,
                0,
                initialWorkItem,
                grammer.StartingSymbol);

            ParsingSteps += configuration.ToString();

            while (configuration.State != (int)States.FinalState && configuration.State != (int)States.ErrorState)
            {
                if (configuration.State == (int)States.NormalState)// if the state is normal, we work with stack beta
                {
                    if (configuration.BetaStack.Count == 0 &&
                        configuration.CurrentSymbolPosition == sequenceLength)//FINAL CONFIGURATION CONDITIONS ARE MET
                    {
                        configuration.State = (int)States.FinalState;
                        ParsingSteps += configuration.ToString();
                    }
                    else
                    {
                        string top = configuration.BetaStack.Peek();
                        if (Helpers.CheckIfInputItemIsNonTerminal(top[0].ToString()))//EXPAND
                        {
                            //var nonTerminal = top[0];
                            var productionIndex = Helpers.GetIndexForProduction(grammer, top[0].ToString(), string.Empty);//get first production index
                            configuration.AddAlfaItem(top[0].ToString(), productionIndex);
                            configuration.BetaStack.Pop();
                            if (top.Length != 1)
                                configuration.BetaStack.Push(top.Remove(0, 1));
                            configuration.BetaStack.Push(Helpers.GetProductionByIndex(grammer, top[0].ToString(), productionIndex));
                            ParsingSteps += configuration.ToString();
                        }
                        else
                        {
                            if (configuration.CurrentSymbolPosition < sequence.Length)
                            {
                                if (top[0] == sequence[configuration.CurrentSymbolPosition])
                                {
                                    configuration.CurrentSymbolPosition++;
                                    configuration.AddAlfaItem(top[0].ToString(), 0);
                                    configuration.BetaStack.Pop();
                                    if (top.Length != 1)
                                        configuration.BetaStack.Push(top.Remove(0, 1));
                                    ParsingSteps += configuration.ToString();
                                }
                                else {
                                    configuration.State = (int)States.BackState;
                                    ParsingSteps += configuration.ToString();
                                }
                            }
                            else
                            {
                                configuration.State = (int)States.BackState;
                                ParsingSteps += configuration.ToString();
                            }
                        }
                    }
                }
                else
                {
                    if (configuration.State == (int)States.BackState)
                    {
                        if (Helpers.CheckIfWorkingItemIsTerminal(configuration.AlfaStack.Peek()))//BACK
                        {
                            configuration.CurrentSymbolPosition--;
                            var terminal = configuration.AlfaStack.Pop();
                            if (configuration.BetaStack.Count == 1)
                            {
                                configuration.BetaStack.Push(terminal.Value);
                                ParsingSteps += configuration.ToString();
                            }
                            else
                            {
                                string production = terminal.Value + configuration.BetaStack.Peek();
                                configuration.BetaStack.Pop();
                                configuration.BetaStack.Push(production);
                                ParsingSteps += configuration.ToString();
                            }
                        }
                        else
                        {
                            var workingItem = configuration.AlfaStack.Peek();
                            var nextIndexWorkingItem = workingItem.Index + 1;
                            var nextProductionForCurrentWorkingItem = Helpers.GetProductionByIndex(grammer,
                                workingItem.Value, nextIndexWorkingItem);
                            if (nextProductionForCurrentWorkingItem != string.Empty)//'ANOTHER TRY'
                            {
                                configuration.State = (int)States.NormalState;
                                configuration.AlfaStack.Pop();
                                configuration.AddAlfaItem(workingItem.Value, nextIndexWorkingItem);
                                configuration.BetaStack.Pop();
                                configuration.BetaStack.Push(nextProductionForCurrentWorkingItem);
                                ParsingSteps += configuration.ToString();
                            }
                            else
                            {
                                if (configuration.CurrentSymbolPosition == 0 && workingItem.Value == grammer.StartingSymbol)
                                {
                                    configuration.State = (int)States.ErrorState;
                                    ParsingSteps += configuration.ToString();
                                }
                                else//ANOTHER TRY
                                {
                                    var nonTerminal = configuration.AlfaStack.Pop();
                                    string production = Helpers.GetProductionByIndex(grammer, nonTerminal.Value, nonTerminal.Index);
                                    string betaProduction = configuration.BetaStack.Peek();
                                    if (betaProduction[0].ToString() == production)
                                    {
                                        betaProduction = betaProduction.Remove(0, 1);
                                        configuration.BetaStack.Pop();
                                        configuration.BetaStack.Push(nonTerminal.Value + betaProduction);
                                        ParsingSteps += configuration.ToString();
                                    }
                                    else
                                    {
                                        string newProduction = production + configuration.BetaStack.Peek();
                                        configuration.BetaStack.Pop();
                                        configuration.BetaStack.Push(newProduction);
                                        ParsingSteps += configuration.ToString();
                                    }
                                }
                            }
                        }
                    }
                }
            }

            ParseResult parseResult = new ParseResult();
            if (configuration.State == (int)States.ErrorState)
            {
                parseResult.Message = ParseMessages.Error;
            }
            else
            {
                parseResult.Message = ParseMessages.SequenceAccepted;
                parseResult.ProductionsString = ConstructProductionsString(configuration);
            }
            //return new ParseResult();
            return parseResult;
        }

        private string ConstructProductionsString(Configuration configuration)
        {
            string prodString = string.Empty;
            while (configuration.AlfaStack.Count != 0)
            {
                if (Helpers.CheckIfWorkingItemIsNonTerminal(configuration.AlfaStack.Peek()))
                {
                    var item = configuration.AlfaStack.Peek();
                    if (char.IsUpper(item.Value[0]))
                        prodString = item.Index.ToString() + prodString;
                }
                configuration.AlfaStack.Pop();
            }
            return prodString;
        }
    }
}
