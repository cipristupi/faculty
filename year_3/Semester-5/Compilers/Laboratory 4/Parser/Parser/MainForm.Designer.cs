﻿namespace Parser
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GetGrammarBttn = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.GrammarTxtBox = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.ParsingTxtBox = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.ProductionStringTxtBox = new System.Windows.Forms.TextBox();
            this.SequenceLabel = new System.Windows.Forms.Label();
            this.ParsingBttn = new System.Windows.Forms.Button();
            this.GetSequenceBttn = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // GetGrammarBttn
            // 
            this.GetGrammarBttn.Location = new System.Drawing.Point(156, 13);
            this.GetGrammarBttn.Name = "GetGrammarBttn";
            this.GetGrammarBttn.Size = new System.Drawing.Size(120, 23);
            this.GetGrammarBttn.TabIndex = 0;
            this.GetGrammarBttn.Text = "Get Grammar";
            this.GetGrammarBttn.UseVisualStyleBackColor = true;
            this.GetGrammarBttn.Click += new System.EventHandler(this.GetGrammarBttn_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.GrammarTxtBox);
            this.groupBox1.Location = new System.Drawing.Point(30, 57);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(261, 321);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Grammar";
            // 
            // GrammarTxtBox
            // 
            this.GrammarTxtBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.GrammarTxtBox.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrammarTxtBox.Location = new System.Drawing.Point(6, 19);
            this.GrammarTxtBox.Multiline = true;
            this.GrammarTxtBox.Name = "GrammarTxtBox";
            this.GrammarTxtBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.GrammarTxtBox.Size = new System.Drawing.Size(249, 294);
            this.GrammarTxtBox.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.ParsingTxtBox);
            this.groupBox2.Location = new System.Drawing.Point(297, 57);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(433, 321);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Parsing Steps";
            // 
            // ParsingTxtBox
            // 
            this.ParsingTxtBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ParsingTxtBox.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ParsingTxtBox.Location = new System.Drawing.Point(6, 19);
            this.ParsingTxtBox.Multiline = true;
            this.ParsingTxtBox.Name = "ParsingTxtBox";
            this.ParsingTxtBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.ParsingTxtBox.Size = new System.Drawing.Size(421, 294);
            this.ParsingTxtBox.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.ProductionStringTxtBox);
            this.groupBox3.Location = new System.Drawing.Point(30, 384);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(700, 58);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "String of Productions";
            // 
            // ProductionStringTxtBox
            // 
            this.ProductionStringTxtBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ProductionStringTxtBox.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ProductionStringTxtBox.Location = new System.Drawing.Point(6, 19);
            this.ProductionStringTxtBox.Multiline = true;
            this.ProductionStringTxtBox.Name = "ProductionStringTxtBox";
            this.ProductionStringTxtBox.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.ProductionStringTxtBox.Size = new System.Drawing.Size(688, 33);
            this.ProductionStringTxtBox.TabIndex = 0;
            // 
            // SequenceLabel
            // 
            this.SequenceLabel.AutoSize = true;
            this.SequenceLabel.Location = new System.Drawing.Point(471, 18);
            this.SequenceLabel.Name = "SequenceLabel";
            this.SequenceLabel.Size = new System.Drawing.Size(59, 13);
            this.SequenceLabel.TabIndex = 4;
            this.SequenceLabel.Text = "Sequence:";
            // 
            // ParsingBttn
            // 
            this.ParsingBttn.Location = new System.Drawing.Point(282, 13);
            this.ParsingBttn.Name = "ParsingBttn";
            this.ParsingBttn.Size = new System.Drawing.Size(183, 23);
            this.ParsingBttn.TabIndex = 5;
            this.ParsingBttn.Text = "Start Parsing";
            this.ParsingBttn.UseVisualStyleBackColor = true;
            this.ParsingBttn.Click += new System.EventHandler(this.ParsingBttn_Click);
            // 
            // GetSequenceBttn
            // 
            this.GetSequenceBttn.Location = new System.Drawing.Point(30, 13);
            this.GetSequenceBttn.Name = "GetSequenceBttn";
            this.GetSequenceBttn.Size = new System.Drawing.Size(120, 23);
            this.GetSequenceBttn.TabIndex = 6;
            this.GetSequenceBttn.Text = "Get Sequence";
            this.GetSequenceBttn.UseVisualStyleBackColor = true;
            this.GetSequenceBttn.Click += new System.EventHandler(this.GetSequenceBttn_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(759, 454);
            this.Controls.Add(this.GetSequenceBttn);
            this.Controls.Add(this.ParsingBttn);
            this.Controls.Add(this.SequenceLabel);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.GetGrammarBttn);
            this.Name = "MainForm";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button GetGrammarBttn;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox GrammarTxtBox;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox ParsingTxtBox;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox ProductionStringTxtBox;
        private System.Windows.Forms.Label SequenceLabel;
        private System.Windows.Forms.Button ParsingBttn;
        private System.Windows.Forms.Button GetSequenceBttn;
    }
}

