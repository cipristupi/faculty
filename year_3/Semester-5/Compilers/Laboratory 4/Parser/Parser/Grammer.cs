﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Parser
{
    public class Grammer
    {
        public List<string> NonTerminals { get; set; } 
        public List<string> Terminals { get; set; } 
        public List<ProductItem> Products { get; set; } 
        public string StartingSymbol { get; set; }

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendFormat("S = {0}{1}", StartingSymbol, Environment.NewLine);
            stringBuilder.AppendFormat("N = {{{0}}}{1}", NonTerminals.Aggregate((i, j) => i + " " + j), Environment.NewLine);
            stringBuilder.AppendFormat("E = {{{0}}}{1}", Terminals.Aggregate((i, j) => i + " " + j), Environment.NewLine);
            stringBuilder.Append("P: ");
            foreach(ProductItem pi in Products)
            {
                stringBuilder.AppendFormat("{0} -> {1}{2}", pi.NonTerminal, pi.Combinations.Aggregate((i,j) => i + " | " + j), Environment.NewLine);
            }
            return stringBuilder.ToString();
        }
    }

    public class ProductItem
    {
        public string NonTerminal { get; set; }
        public List<string> Combinations { get; set; } 
    }
}