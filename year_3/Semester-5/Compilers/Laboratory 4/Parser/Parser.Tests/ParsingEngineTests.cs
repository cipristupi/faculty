﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Parser.Tests
{
    [TestClass]
    public class ParsingEngineTests
    {
        [TestMethod]
        public void ParsingEngine_ParserRecursiveDescendent_ValidGrammer_IReallyDontKnow()
        {
            //Arrange
            Grammer grammer = BookGrammerInit();
            string sequence = "aacbc";
            ParsingEngine parsingEngine = new ParsingEngine();
            //Act
            var result = parsingEngine.ParserRecursiveDescendent(grammer, sequence);
            //Assert
        }

        private Grammer BookGrammerInit()
        {
            Grammer grammer = new Grammer
            {
                StartingSymbol = "S",
                NonTerminals = new List<string>()
                {
                    "S"
                },
                Terminals = new List<string>()
                {
                    "a",
                    "b",
                    "c"
                }
            };
            List<string> nonTerminalSCombinations = new List<string>()
            {
                "aSbS","aS","c"
            };
            grammer.Products = new List<ProductItem>()
            {
                new ProductItem()
                {
                    Combinations = nonTerminalSCombinations,
                    NonTerminal = "S"
                }
            };
            return grammer;
        }
    }
}
