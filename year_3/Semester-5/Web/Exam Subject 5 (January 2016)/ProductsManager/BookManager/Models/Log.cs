﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookManager.Models
{
    public class Log
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public string IPAddress { get; set; }
        public string EventType { get; set; }
        public string Event { get; set; }
        public int IDProduct { get; set; }
    }
}