﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using BookManager.Models;

namespace BookManager.Controllers
{
    public class ProductsController : ApiController
    {
        readonly string _connectionString = @"Data Source =.\; Initial Catalog=ProductsManager; Integrated Security=true";
        SqlConnection _sqlConnection;
        SqlCommand _sqlCommand;
        SqlDataReader _dataReader;


        [ActionName("GetAllProducts")]
        [HttpGet]
        public IEnumerable<Product> GetAllProducts()
        {
            string selectAllBooksQuery = "SELECT Id,Name,Type,Price,Description FROM Products";
            List<Product> books = new List<Product>();
            try
            {
                _sqlConnection = new SqlConnection(_connectionString);
                _sqlConnection.Open();
                _sqlCommand = new SqlCommand(selectAllBooksQuery, _sqlConnection);
                _dataReader = _sqlCommand.ExecuteReader();
                while (_dataReader.Read())
                {
                    books.Add(Product.Read(_dataReader));
                }
            }
            catch (Exception ex) { }
            finally
            {
                _sqlConnection.Close();
            }
            return books;
        }

        [ActionName("CreateProduct")]
        [HttpPost]
        public HttpResponseMessage CreateProduct([FromBody]Product model)
        {
            string insertProductQuery =
                "INSERT INTO [dbo].[Products] ([Name],[Type],[Price],[Description]) VALUES(@name,@type,@price,@description)";
            try
            {
                _sqlConnection = new SqlConnection(_connectionString);
                _sqlConnection.Open();
                _sqlCommand = new SqlCommand(insertProductQuery, _sqlConnection);

                _sqlCommand.Parameters.Add(new SqlParameter("@name", model.Name));
                _sqlCommand.Parameters.Add(new SqlParameter("@type", model.Type));
                _sqlCommand.Parameters.Add(new SqlParameter("@price", model.Price));
                _sqlCommand.Parameters.Add(new SqlParameter("@description", model.Description));
                _sqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, ex.ToString());
            }
            finally
            {
                _sqlConnection.Close();
            }
            return Request.CreateResponse<Product>(HttpStatusCode.OK, model);
        }

        [ActionName("SearchProducts")]
        [HttpGet]
        public IEnumerable<Product> SearchProducts(string value, string columns)
        {
            string selectAllProductsQuery = "SELECT Id,Name,Type,Price,Description FROM Products";
            string whereClause = " WHERE ";
            string decodedValue = HttpUtility.HtmlDecode(value);
            var columnsList = columns.Split(',');
            for (int i = 0; i < columnsList.Length; i++)
            {
                var column = columnsList[i];
                if (i != columnsList.Length - 1)
                {
                    whereClause += column + " LIKE '%" + decodedValue + "%' OR ";
                }
                else
                {
                    whereClause += column + " LIKE '%" + decodedValue + "%'";
                }
            }
            selectAllProductsQuery = selectAllProductsQuery + whereClause;
            List<Product> books = new List<Product>();
            try
            {
                _sqlConnection = new SqlConnection(_connectionString);
                _sqlConnection.Open();
                _sqlCommand = new SqlCommand(selectAllProductsQuery, _sqlConnection);
                _dataReader = _sqlCommand.ExecuteReader();
                while (_dataReader.Read())
                {
                    books.Add(Product.Read(_dataReader));
                }
            }
            catch (Exception ex)
            {
                var exeption = ex;
            }
            finally
            {
                _sqlConnection.Close();
            }
            return books;
        }


        [ActionName("CreateLog")]
        [HttpPost]
        public HttpResponseMessage CreateLog([FromBody]Log model)
        {
            string insertProductQuery =
                "INSERT INTO [dbo].[Logs]([Date],[IPAddress],[EventType],[Event],[IDProduct]) VALUES (@Date,@IPAddress,@EventType,@Event,@IDProduct)";
            try
            {
                _sqlConnection = new SqlConnection(_connectionString);
                _sqlConnection.Open();
                _sqlCommand = new SqlCommand(insertProductQuery, _sqlConnection);

                _sqlCommand.Parameters.Add(new SqlParameter("@Date", model.Date));
                _sqlCommand.Parameters.Add(new SqlParameter("@IPAddress", model.IPAddress));
                _sqlCommand.Parameters.Add(new SqlParameter("@EventType", model.EventType));
                _sqlCommand.Parameters.Add(new SqlParameter("@Event", model.Event));
                _sqlCommand.Parameters.Add(new SqlParameter("@IDProduct", model.IDProduct));
                _sqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, ex.ToString());
            }
            finally
            {
                _sqlConnection.Close();
            }
            return Request.CreateResponse<Log>(HttpStatusCode.OK, model);
        }
    }
}