﻿var ProductsManager = (function () {
    var privates = {};
    privates.bind = function () {
        var baseUri = 'api/Products';
        var uriGetAllProducts = baseUri + "/GetAllProducts";
        var uriCreateProduct = baseUri + "/CreateProduct";
        jQuery('#loadAllProducts').off('click').on('click', function () {
            privates.loadTable();
        });
        jQuery('#addProduct').off('click').on('click', function () {
            var addWrapper = jQuery('.addProductWrapper');
            var name = jQuery(addWrapper).find('input[name="name"]').val();
            var type = jQuery(addWrapper).find('input[name="type"]').val();
            var description = jQuery(addWrapper).find('input[name="description"]').val();
            var price = jQuery(addWrapper).find('input[name="price"]').val();
            var model = {
                Name: name,
                Type: type,
                Description: description,
                Price: price
            };
            var jsonModel = JSON.stringify(model);
            jQuery.ajax({
                type: "POST",
                url: uriCreateProduct,
                contentType: "application/json",
                dataType: "json",
                data: jsonModel,
                success: function (response) {
                    privates.loadTable();
                },
                error: function (response) {
                    console.log(response);
                }
            });
        });


        jQuery('#searchInput').keyup(function () {
            console.log('keyup');
            var uri = 'api/Products';
            var uriSearchProducts = uri + "/SearchProducts";
            var searchWrapper = jQuery('.filterWrapper');
            var searchValue = jQuery(searchWrapper).find('input[name="searchValue"]').val();
            var checkedColumns = jQuery(searchWrapper).find('.searchProductProperties :checkbox:checked');
            if (checkedColumns.length !== 0) {
                var checkedColumnsValues = [];
                var value;
                for (var i = 0; i < checkedColumns.length; i++) {
                    value = jQuery(checkedColumns[i]).attr('value');
                    checkedColumnsValues.push(value);
                }
                var columnsValues = checkedColumnsValues.join(',');
                var model = {
                    'value': searchValue,
                    'columns': columnsValues
                };

                jQuery.ajax({
                    type: "GET",
                    url: uriSearchProducts,
                    data: model,
                    success: function(response) {
                        privates.buildTable(response);
                    },
                    error: function(response) {
                        console.log(response);
                    }
                });

                privates.insertLog("search", "search", "");
            }
        });

    };

    privates.insertLog = function (eventType,event,idProduct) {
        var uriCreateLog = "api/Products/CreateLog";
        var ipaddress;
        $.getJSON("https://api.ipify.org?format=jsonp&callback=?",
            function(json) {
                ipaddress = json;
            }
        );
        var model = {
            Date: new Date().toLocaleString(),
            EventType: eventType,
            Event: event,
            IDProduct: idProduct,
            IPAddress:ipaddress
            };
        var jsonModel = JSON.stringify(model);
        jQuery.ajax({
            type: "POST",
            url: uriCreateLog,
            contentType: "application/json",
            dataType: "json",
            data: jsonModel,
            success: function (response) {
               // privates.loadTable();
            },
            error: function (response) {
                console.log(response);
            }
        });
    };

    privates.calculatePopulare = function(currentSearchResult) {
        if (typeof (Storage) !== "undefined") {
            var newPopularSearch = [];
            var currentPopularSearch = localStorage.getItem("popularSearch");
            var popularSearchEntry = {
            
            };
            for (var i = 0; i < currentSearchResult.length; i++) {//luam cele noi 
                var product = currentSearchResult[i];
                var popularProduct = privates.getElementById(product.ID, currentPopularSearch);
                if (popularProduct != undefined) { //nu exista deja in browser;
                    popularSearchEntry.ID = product.ID;
                    popularSearchEntry.Product = product;
                    popularSearchEntry.ResultsNumber = popularProduct.ResultsNumber ++;
                } else {
                    popularSearchEntry.ID = product.ID;
                    popularSearchEntry.Product = product;
                    popularSearchEntry.ResultsNumber = 1;
                }
                newPopularSearch.push(popularSearchEntry);
            }
            
            for (var j = 0; j < currentPopularSearch.length; j++) {
                var currentPopularSearchProduct = currentPopularSearch[j];
                var x = privates.getElementById(currentPopularSearchProduct.ID, newPopularSearch);
                if (x === undefined) {
                    newPopularSearch.push(currentPopularSearchProduct);
                }
            }
            // Store
            localStorage.setItem("popularSearch", newPopularSearch);
            // Retrieve
        } else {
            document.getElementById("result").innerHTML = "Sorry, your browser does not support Web Storage...";
        }
    };

    privates.displayPopular = function() {
        if (typeof (Storage) !== "undefined") {
            var currentPopularSearch = localStorage.getItem("popularSearch");
            var sortedResult = currentPopularSearch.sort(function(a, b) {
                return a.ResultsNumber > b.ResultsNumber;
            });
            var firstFiveResults = sortedResult.slice(0, 5);
            privates.buildPopularTable(firstFiveResults);
        }
    };
    privates.getElementById = function (id, popularSearch) {
        var element = undefined;
        for (var i = 0; i < popularSearch.length; i++) {
            if (popularSearch[i].id === id) {
                element = popularSearch[i];
                break;
            }
        }
        return element;
    }
    privates.loadTable = function () {
        var uri = 'api/Products';
        var uriGetAllBooks = uri + "/GetAllProducts";
        $.getJSON(uriGetAllBooks)
          .done(function (data) {
              privates.buildTable(data);
          });
    };
    privates.buildTable = function (data) {
        var table = jQuery('#booksTable');
        table.find('tbody').html('');
        var rowTemplate = '<tr> <td>{0}</td> <td>{1}</td> <td>{2}</td> <td>{3}</td> <td>{4}</td> </tr>';
        for (var i = 0; i < data.length; i++) {
            var product = data[i];
            var row = rowTemplate;
            row = row.format(product.Id, product.Name, product.Type, product.Price, product.Description, product.Id);
            table.find('tbody').append(row);
        }
        privates.bind();
    };

    privates.buildPopularTable = function(data) {
        var table = jQuery('#popularTable');
        table.find('tbody').html('');
        var rowTemplate = '<tr> <td>{0}</td> <td>{1}</td> <td>{2}</td> <td>{3}</td> <td>{4}</td>  <td>{5}</td></tr>';
        for (var i = 0; i < data.length; i++) {
            var product = data[i].Product;
            var row = rowTemplate;
            row = row.format(product.Id, product.Name, product.Type, product.Price, product.Description, product.Id,data[i].ResultsNumber);
            table.find('tbody').append(row);
        }
        privates.bind();
    };
    return {
        init: function () {
            privates.bind();
        }
    }
})();