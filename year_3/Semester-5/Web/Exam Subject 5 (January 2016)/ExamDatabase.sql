USE [master]
GO
/****** Object:  Database [ProductsManager]    Script Date: 2/15/2016 10:07:00 PM ******/
CREATE DATABASE [ProductsManager]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'ProductsManager', FILENAME = N'D:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\ProductsManager.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'ProductsManager_log', FILENAME = N'D:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\ProductsManager_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [ProductsManager] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [ProductsManager].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [ProductsManager] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [ProductsManager] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [ProductsManager] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [ProductsManager] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [ProductsManager] SET ARITHABORT OFF 
GO
ALTER DATABASE [ProductsManager] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [ProductsManager] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [ProductsManager] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [ProductsManager] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [ProductsManager] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [ProductsManager] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [ProductsManager] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [ProductsManager] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [ProductsManager] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [ProductsManager] SET  DISABLE_BROKER 
GO
ALTER DATABASE [ProductsManager] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [ProductsManager] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [ProductsManager] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [ProductsManager] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [ProductsManager] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [ProductsManager] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [ProductsManager] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [ProductsManager] SET RECOVERY FULL 
GO
ALTER DATABASE [ProductsManager] SET  MULTI_USER 
GO
ALTER DATABASE [ProductsManager] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [ProductsManager] SET DB_CHAINING OFF 
GO
ALTER DATABASE [ProductsManager] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [ProductsManager] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [ProductsManager] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'ProductsManager', N'ON'
GO
USE [ProductsManager]
GO
/****** Object:  Table [dbo].[Logs]    Script Date: 2/15/2016 10:07:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Logs](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Date] [datetime] NOT NULL,
	[IPAddress] [nvarchar](50) NOT NULL,
	[EventType] [nvarchar](50) NOT NULL,
	[Event] [nvarchar](50) NOT NULL,
	[IDProduct] [int] NOT NULL,
 CONSTRAINT [PK_Logs] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Products]    Script Date: 2/15/2016 10:07:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Products](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Type] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[Price] [int] NOT NULL,
 CONSTRAINT [PK_Products] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
USE [master]
GO
ALTER DATABASE [ProductsManager] SET  READ_WRITE 
GO
