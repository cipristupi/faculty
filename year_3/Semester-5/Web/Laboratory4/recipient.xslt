<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="recipes">
    <html>
      <head>
        <link href="style.css" rel="stylesheet" type="text/css"/>
      </head>
      <body>
        <div class="recipes-wrapper">

          <xsl:for-each select="recipe">
            <div class="recipe">
              <xsl:variable name="imageSource" select="image"/>
              <img src="{$imageSource}"/>
              <div class="ingredients">

                <xsl:for-each select="ingredients/ingredient">
                  <div class="ingredient">
                    <xsl:value-of select="current()"/>
                  </div>
                </xsl:for-each>
              </div>
              <div class="steps">

                <xsl:for-each select="steps/step">
                  <div class="step">
                    <xsl:value-of select="current()"/>
                  </div>
                </xsl:for-each>
              </div>
            </div>
          </xsl:for-each>
        </div>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
