﻿var BooksManager = (function () {
    var privates = {};
    privates.bind = function () {
        var baseUri = 'api/books';
        var uriGetAllBooks = baseUri + "/GetAllBooks";
        var uriCreateBook = baseUri + "/CreateBook";
        var uriDeleteBook = baseUri + "/DeleteBook";
        var uriUpdateBook = baseUri + "/UpdateBook";
        var uriGetBookById = baseUri + "/GetBookById";
        var uriGetBookByGenre = baseUri + "/GetBooksByGenre";
        jQuery('#loadAllBooks').off('click').on('click', function () {
            privates.loadTable();
        });
        jQuery('#addBook').off('click').on('click', function () {
            var addWrapper = jQuery('.addBookWrapper');
            var title = jQuery(addWrapper).find('input[name="title"]').val();
            var author = jQuery(addWrapper).find('input[name="author"]').val();
            var description = jQuery(addWrapper).find('input[name="description"]').val();
            var numberOfPages = jQuery(addWrapper).find('input[name="numberOfPages"]').val();
            var genre = jQuery(addWrapper).find('input[name="genre"]').val();
            var model = {
                Title: title,
                Author: author,
                Description: description,
                NumberOfPages: numberOfPages,
                Genre:genre
            };
            var jsonModel = JSON.stringify(model);
            jQuery.ajax({
                type: "POST",
                url: uriCreateBook,
                contentType: "application/json",
                dataType: "json",
                data: jsonModel,
                success: function(response) {
                    privates.loadTable();
                },
                error: function(response) {
                    console.log(response);
                }
            });
        });
        jQuery('#updateBook').off('click').on('click', function () {
            var editWrapper = jQuery('.editBookWrapper');
            var title = jQuery(editWrapper).find('input[name="title"]').val();
            var author = jQuery(editWrapper).find('input[name="author"]').val();
            var description = jQuery(editWrapper).find('input[name="description"]').val();
            var numberOfPages = jQuery(editWrapper).find('input[name="numberOfPages"]').val();
            var genre = jQuery(editWrapper).find('input[name="genre"]').val();
            var id = jQuery(editWrapper).find('input[name="BookId"]').val();
            var model = {
                Title: title,
                Author: author,
                Description: description,
                NumberOfPages: numberOfPages,
                Genre:genre,
                Id:id
            };
            var jsonModel = JSON.stringify(model);
            jQuery.ajax({
                type: "POST",
                url: uriUpdateBook,
                contentType: "application/json",
                dataType: "json",
                data: jsonModel,
                success: function (response) {
                    privates.loadTable();
                },
                error: function (response) {
                    console.log(response);
                }
            });
        });
        jQuery('#searchByGenre').off('click').on('click', function () {
            var genre = jQuery('input[name="searchValue"]').val();
            var model = {Genre: genre};
            var modelJson = JSON.stringify(model);
            jQuery.ajax({
                type: "POST",
                url: uriGetBookByGenre,
                 contentType: "application/json",
                 dataType: "json",
                data: modelJson,
                success: function (response) {
                    privates.buildTable(response);
                },
                error: function (response) {
                    console.log(response);
                }
            });
        });

        jQuery('.deleteBook').off('click').on('click', function () {
            var bookId = jQuery(this).attr('bookId');
            var deleteBookUri = uriDeleteBook;
            deleteBookUri = deleteBookUri + "/" + bookId;
            jQuery.ajax({
                type: "POST",
                url: deleteBookUri,
                success: function (response) {
                    privates.loadTable();
                },
                error: function (response) {
                    console.log(response);
                }
            });
        });

        jQuery('.loadBook').off('click').on('click', function () {
            var editWrapper = jQuery('.editBookWrapper');
            var bookId = jQuery(this).attr('bookId');
            var getBookByIdUri = uriGetBookById;
            getBookByIdUri = getBookByIdUri + "/" + bookId;
            jQuery.ajax({
                type: "GET",
                url: getBookByIdUri,
                success: function (response) {
                    response = JSON.parse(response);
                    jQuery(editWrapper).find('input[name="title"]').val(response.Title);
                    jQuery(editWrapper).find('input[name="author"]').val(response.Author);
                    jQuery(editWrapper).find('input[name="description"]').val(response.Description);
                    jQuery(editWrapper).find('input[name="numberOfPages"]').val(response.NumberOfPages);
                    jQuery(editWrapper).find('input[name="BookId"]').val(response.Id);
                    jQuery(editWrapper).find('input[name="genre"]').val(response.Genre);
                },
                error: function (response) {
                    console.log(response);
                }
            });
        });
    };
    privates.loadTable = function () {
        var uri = 'api/books';
        var uriGetAllBooks = uri + "/GetAllBooks";
        $.getJSON(uriGetAllBooks)
               .done(function (data) {
                   privates.buildTable(data);
               });
    };
    privates.buildTable = function (data) {
        var table = jQuery('#booksTable');
        //clean-up existing rows;
        table.find('tbody').html('');
        var rowTemplate = '<tr> <td>{0}</td> <td>{1}</td> <td>{2}</td> <td>{3}</td> <td>{4}</td> <td>{6}</td>' +
            '<td><input class="deleteBook" type="button" value="Delete" bookId="{5}" /> <input class="loadBook" type="button" value="Edit" bookId="{5}"/></td>' +
            '</tr>';
        for (var i = 0; i < data.length; i++) {
            var book = data[i];
            var row = rowTemplate;
            row = row.format(book.Id, book.Title, book.Author, book.NumberOfPages, book.Description,book.Id,book.Genre);
            table.find('tbody').append(row);
        }
        privates.bind();
    };
    return {
        init: function () {
            privates.bind();
        }
    }

})();