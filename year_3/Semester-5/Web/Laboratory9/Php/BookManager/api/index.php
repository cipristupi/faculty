<?php
include 'db.php';
require 'Slim/Slim.php';
\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();

$app->get('/books/GetAllBooks','GetAllBooks');
$app->post('/books/CreateBook', 'CreateBook');
$app->post('/books/UpdateBook','UpdateBook');
$app->get('/books/GetBookById/:id','GetBookById');
$app->post('/books/GetBooksByGenre','GetBooksByGenre');
$app->post('/books/DeleteBook/:id','DeleteBook');

$app->run();

function GetAllBooks() {
	$sql = "SELECT * FROM books";
	try {
		$db = getDB();
		$stmt = $db->query($sql);  
		$books = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		echo json_encode($books);
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}


function CreateBook() {
	$request = \Slim\Slim::getInstance()->request();
	$book = json_decode($request->getBody());
	$sql = "INSERT INTO books (title, author, numberOfPages,description, genre) VALUES (:title, :author, :numberOfPages,:description, :genre)";
	try {
		$db = getDB();
		$stmt = $db->prepare($sql);  
		$stmt->bindParam("title", $book->Title);
		$stmt->bindParam("author", $book->Author);
		$stmt->bindParam("numberOfPages", $book->NumberOfPages);
		$stmt->bindParam("description", $book->Description);
		$stmt->bindParam("genre", $book->Genre);
		$stmt->execute();
		echo json_encode($book);
		$db = null;

	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}


function UpdateBook() {
	$request = \Slim\Slim::getInstance()->request();
	$book = json_decode($request->getBody());
	$sql = "UPDATE books SET title =:title, author=:author, numberOfPages=:numberOfPages,description = :description , genre =:genre WHERE id=:id";
	try {
		$db = getDB();
		$stmt = $db->prepare($sql);  
		$stmt->bindParam("title", $book->Title);
		$stmt->bindParam("author", $book->Author);
		$stmt->bindParam("numberOfPages", $book->NumberOfPages);
		$stmt->bindParam("description", $book->Description);
		$stmt->bindParam("genre", $book->Genre);
		$stmt->bindParam("id", $book->Id);
		$stmt->execute();
		echo json_encode($book);
		$db = null;

	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}


function GetBookById($id) {

	$sql = "SELECT Id,Title,Author,Description,NumberOfPages,Genre FROM books WHERE id=" . $id .";";
	try {
		$db = getDB();
		$stmt = $db->query($sql);  
		$book = $stmt->fetch(PDO::FETCH_ASSOC);
		$db = null;
		echo json_encode($book);
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}

function DeleteBook($id) {
	$sql = "DELETE FROM books WHERE id=" . $id .";";
	try {
		$db = getDB();
		$stmt = $db->prepare($sql);  
		$stmt->execute();
		$db = null;
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}	
}

function GetBooksByGenre() {
	$request = \Slim\Slim::getInstance()->request();
	$genre = json_decode($request->getBody());
	$sql = "SELECT * FROM books WHERE genre='" . $genre->Genre . "';";
	try {
		$db = getDB();
		$stmt = $db->query($sql);  
		$books = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		echo json_encode($books);
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}
?>