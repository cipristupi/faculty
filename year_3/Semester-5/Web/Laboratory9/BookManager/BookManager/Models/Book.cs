﻿using System.Data;

namespace BookManager.Models
{
    public class Book
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public string Description { get; set; }
        public int NumberOfPages { get; set; }
        public string Genre { get; set; }

        public static Book Read(IDataReader reader)
        {
            return new Book
            {
                Id = reader.GetInt32(0),
                Title = reader.GetString(1),
                Author = reader.GetString(2),
                NumberOfPages = reader.GetInt32(3),
                Description = reader.GetString(4),
                Genre = reader.GetString(5)
            };
        }
    }
}