﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using BookManager.Models;

namespace BookManager.Controllers
{
    public class BooksController : ApiController
    {
        readonly string _connectionString = @"Data Source =.\; Initial Catalog=BooksManager; Integrated Security=true";
        SqlConnection _sqlConnection;
        SqlCommand _sqlCommand;
        SqlDataReader _dataReader;


        [ActionName("GetAllBooks")]
        [HttpGet]
        public IEnumerable<Book> GetAllBooks()
        {
            string selectAllBooksQuery = "SELECT Id,Title,Author,NumberOfPages,Description,Genre FROM Books";
            List<Book> books=new List<Book>();
            try
            {
                _sqlConnection = new SqlConnection(_connectionString);
                _sqlConnection.Open();
                _sqlCommand = new SqlCommand(selectAllBooksQuery,_sqlConnection);
                _dataReader = _sqlCommand.ExecuteReader();
                while (_dataReader.Read())
                {
                    books.Add(Book.Read(_dataReader));
                }
            }
            catch (Exception ex)
            {
                
            }
            finally
            {
                _sqlConnection.Close();
            }
            return books;
        }

        [ActionName("GetBookById")]
        [HttpGet]
        //[Route("{id:int}")]
        public IHttpActionResult GetBook(int id)
        {
            string selectBookById = "SELECT Id,Title,Author,NumberOfPages,Description,Genre FROM Books WHERE Id = @id";
            Book book = null;
            try
            {
                _sqlConnection = new SqlConnection(_connectionString);
                _sqlConnection.Open();
                _sqlCommand = new SqlCommand(selectBookById, _sqlConnection);
                SqlParameter parameter = new SqlParameter("@id",id);
                _sqlCommand.Parameters.Add(parameter);
                _dataReader = _sqlCommand.ExecuteReader();
                _dataReader.Read();
                if (_dataReader.HasRows)
                {
                    book = Book.Read(_dataReader);
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                _sqlConnection.Close();
            }
            if (book == null)
            {
                return NotFound();
            }
            return Ok(book);
        }


        [ActionName("GetBooksByGenre")]
        [HttpGet]
        public IEnumerable<Book> GetBooksByGenre(string genre)
        {
            string selectAllBooksQuery = "SELECT Id,Title,Author,NumberOfPages,Description,Genre FROM Books WHERE Genre = @genre";
            List<Book> books = new List<Book>();
            try
            {
                _sqlConnection = new SqlConnection(_connectionString);
                _sqlConnection.Open();
                _sqlCommand = new SqlCommand(selectAllBooksQuery, _sqlConnection);
                SqlParameter parameter = new SqlParameter("@genre", genre);
                _sqlCommand.Parameters.Add(parameter);
                _dataReader = _sqlCommand.ExecuteReader();
                while (_dataReader.Read())
                {
                    books.Add(Book.Read(_dataReader));
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                _sqlConnection.Close();
            }
            return books;
        }


        [ActionName("CreateBook")]
        [HttpPost]
        public HttpResponseMessage CreateBook([FromBody]Book model)
        {
            string insertBookQuery =
                "INSERT INTO [dbo].[Books] ([Title],[Author],[NumberOfPages],[Description],[Genre]) VALUES(@title,@author,@numberOfPages,@description,@genre)";
            try
            {
                _sqlConnection = new SqlConnection(_connectionString);
                _sqlConnection.Open();
                _sqlCommand = new SqlCommand(insertBookQuery, _sqlConnection);

                _sqlCommand.Parameters.Add(new SqlParameter("@title",model.Title));
                _sqlCommand.Parameters.Add(new SqlParameter("@author", model.Author));
                _sqlCommand.Parameters.Add(new SqlParameter("@numberOfPages", model.NumberOfPages));
                _sqlCommand.Parameters.Add(new SqlParameter("@description", model.Description));
                _sqlCommand.Parameters.Add(new SqlParameter("@genre", model.Genre));
                _sqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, ex.ToString());
            }
            finally
            {
                _sqlConnection.Close();
            }
            return  Request.CreateResponse<Book>(HttpStatusCode.OK, model);
        }

        [ActionName("UpdateBook")]
        [HttpPost]
        public HttpResponseMessage UpdateBook([FromBody]Book model)
        {
            string updateBookQuery =
                "UPDATE [dbo].[Books] SET [Title] = @title ,[Author] = @author ,[NumberOfPages] = @numberOfPages ,[Description] = @description ,[Genre] = @genre WHERE Id =@id";
            try
            {
                _sqlConnection = new SqlConnection(_connectionString);
                _sqlConnection.Open();
                _sqlCommand = new SqlCommand(updateBookQuery, _sqlConnection);

                _sqlCommand.Parameters.Add(new SqlParameter("@title", model.Title));
                _sqlCommand.Parameters.Add(new SqlParameter("@author", model.Author));
                _sqlCommand.Parameters.Add(new SqlParameter("@numberOfPages", model.NumberOfPages));
                _sqlCommand.Parameters.Add(new SqlParameter("@description", model.Description));
                _sqlCommand.Parameters.Add(new SqlParameter("@id", model.Id));
                _sqlCommand.Parameters.Add(new SqlParameter("@genre", model.Genre));
                _sqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, ex.ToString());
            }
            finally
            {
                _sqlConnection.Close();
            }
            return Request.CreateResponse<Book>(HttpStatusCode.OK, model);
        }

        [ActionName("DeleteBook")]
        [HttpPost]
        public HttpResponseMessage DeleteBook(int id)
        {
            string updateBookQuery =
               "DELETE FROM [dbo].[Books] WHERE Id =@id";
            try
            {
                _sqlConnection = new SqlConnection(_connectionString);
                _sqlConnection.Open();
                _sqlCommand = new SqlCommand(updateBookQuery, _sqlConnection);
                _sqlCommand.Parameters.Add(new SqlParameter("@id", id));
                _sqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, ex.ToString());
            }
            finally
            {
                _sqlConnection.Close();
            }
            return Request.CreateResponse<int>(HttpStatusCode.OK, id);
        }

    }
}
