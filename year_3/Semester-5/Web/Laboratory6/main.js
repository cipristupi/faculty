var Sorter = (function() {
  var privates = {};
  privates.bind = function() {
    jQuery('thead').find('th').off('click').on('click', function() {
      var columnIndex = jQuery(this).index();
      var table = jQuery('#tableForSort');
      var order;
      if (jQuery(this).hasClass('asc')) {
        order = 'desc';
        jQuery(this).removeClass('asc');
        jQuery(this).addClass('desc');
      } else {
        order = 'asc';
        jQuery(this).removeClass('desc');
        jQuery(this).addClass('asc');
      }
      privates.sortTable(table, order, columnIndex);
    });

    privates.sortTable = function(table, order, columnIndex) {
      var asc = order === 'asc',
        tbody = table.find('tbody');

      tbody.find('tr').sort(function(a, b) {
        if (asc) {
          return $('td:eq(' + columnIndex + ')', a).text().localeCompare($('td:eq(' + columnIndex + ')', b).text());
        } else {
          return $('td:eq(' + columnIndex + ')', b).text().localeCompare($('td:eq(' + columnIndex + ')', a).text());
        }
      }).appendTo(tbody);
    }

    privates.moveColumn = function(table, from, to) {
      var rows = jQuery('tr', table);
      var cols;
      rows.each(function() {
        cols = jQuery(this).children('th, td');
        if (to != 0) {
          cols.eq(from).detach().insertAfter(cols.eq(to));
        }else{
          cols.eq(from).detach().insertBefore(cols.eq(to));
        }
      });
    }

    jQuery('tfoot').find('td').off('click').on('click', function() {
      var columnIndex = jQuery(this).index();
      var table = jQuery('#tableForSort');
      var fromPosition, toPosition;

      if (columnIndex + 1 == 5) {
        fromPosition = columnIndex;
        toPosition = 0;
      } else {
        fromPosition = columnIndex;
        toPosition = columnIndex + 1;
      }
      privates.moveColumn(table, fromPosition, toPosition)
    });
  };
  privates.columnNumber = 0;
  privates.rowNumber = 0;
  return {
    init: function() {
      privates.bind();
    }
  }
})();
