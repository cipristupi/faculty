var Slider = (function() {
  var privates = {};
  privates.lastImageNumber = 5;
  privates.firstImageNumber = 1;
  privates.paused = true;
  privates.currentPicture = 1;
  privates.autoSlide = false;
  privates.timeout = undefined;
  return {
    bind: function() {
      document.getElementById('start-btn').onclick = function() {
        var imageName;
        var slideTime = document.getElementsByName('slidetime')[0].value;
        if (slideTime == "") {
          slideTime = 1000;
        }
        var autoSlide = document.querySelector('#auto-slide:checked');
        if (autoSlide != undefined) {
          privates.autoSlide = true;
        } else {
          privates.autoSlide = false;
        }
        if (privates.paused) {
          document.getElementById("status").innerHTML = "Sliding";
          privates.timeout = setInterval(function() {
            if (privates.currentPicture != 6) {
              imageName = "image" + privates.currentPicture + ".jpg";
              document.getElementById("displayedImage").src = imageName;
              document.getElementById("image-no").innerHTML = "Image " + privates.currentPicture + " from 5."
              privates.currentPicture = privates.currentPicture + 1;
            }
            if (privates.currentPicture == 6) {
              if (privates.autoSlide) {
                privates.currentPicture = 1;
              } else {
                clearTimeout(privates.timeout);
                privates.paused = true;
                document.getElementById("status").innerHTML = "Stopped";
                privates.currentPicture = 1;
              }
            };
          }, slideTime);
        }
      };
      document.getElementById('pause-btn').onclick = function() {
        if (privates.paused) {
          privates.paused = true;
          document.getElementById("status").innerHTML = "Paused";
          clearTimeout(privates.timeout);
        }
      };

      document.getElementById('reset-btn').onclick = function() {
        privates.currentPicture = 1;
        imageName = "image" + privates.currentPicture + ".jpg";
        document.getElementById("displayedImage").src = imageName;
        document.getElementById("image-no").innerHTML = "Image " + privates.currentPicture + " from 5."
        privates.paused = true;
        document.getElementById("status").innerHTML = "Stopped";
        clearTimeout(privates.timeout);
      };
    }
  };
})();
