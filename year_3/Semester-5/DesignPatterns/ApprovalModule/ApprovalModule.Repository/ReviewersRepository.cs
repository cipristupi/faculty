﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApprovalModule.Repository
{
    public class ReviewersRepository : BaseRepository<Reviewer>
    {
        public Reviewer GetById(int id)
        {
            return GetAll().FirstOrDefault(x => x.Id == id);
        }

        public Reviewer GetByUserId(string id)
        {
            return GetAll().FirstOrDefault(x => x.UserId == id);
        }

        public bool CheckIfReviewExits(string userId, int boardId)
        {
            return GetAll().Count(x => x.BoardId == boardId && x.UserId == userId) > 0;
        }

        public IEnumerable<Reviewer> GetReviewersByBoardId(int id)
        {
            return GetAll().Where(x => x.BoardId == id).ToList();
        }

    }
}
