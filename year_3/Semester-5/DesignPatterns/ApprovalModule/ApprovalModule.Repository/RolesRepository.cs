﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApprovalModule.Repository
{
    public class RolesRepository : BaseRepository<AspNetRole>
    {
        public override void Update(AspNetRole entity, int id)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            var existing = _context.Set<AspNetRole>().Find(entity.Id);
            if (existing == null) return;
            _context.Entry(existing).CurrentValues.SetValues(entity);
            SaveContext();
        }

        public AspNetRole GetById(string id)
        {
            return GetAll().FirstOrDefault(x => x.Id == id);
        }
    }
}
