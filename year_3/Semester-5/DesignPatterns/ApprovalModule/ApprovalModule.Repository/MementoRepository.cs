﻿using System.Linq;

namespace ApprovalModule.Repository
{
    public class MementoRepository :BaseRepository<Memento>
    {
        public Memento GetById(int mementoId)
        {
            return GetAll().FirstOrDefault(x => x.Id == mementoId);
        }

        public Memento GetFirstByBoardId(int boardId)
        {
            return GetAll().OrderBy(x => x.DateCreated).FirstOrDefault(y => y.BoardId == boardId);
        }
    }
}
