﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApprovalModule.Repository
{
    public class BaseRepository<T> where T:class
    {
        protected readonly ApprovalModuleEntities _context;
        internal DbSet<T> _dbSet;

        public BaseRepository()
        {
            _context = new ApprovalModuleEntities();
            _dbSet = _context.Set<T>();
        }
        public virtual void Add(T entity)
        {
            _context.Set<T>().Add(entity);
            SaveContext();
        }

        public virtual void Update(T entity, int id)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            var existing = _context.Set<T>().Find(id);
            if (existing == null) return;
            _context.Entry(existing).CurrentValues.SetValues(entity);
            SaveContext();
        }

        public virtual void Delete(object id)
        {
            T entity = _dbSet.Find(id);
            if (entity != null)
            {
                if (_context.Entry(entity).State == EntityState.Detached)
                {
                    _dbSet.Attach(entity);
                }
                _dbSet.Remove(entity);
            }
            SaveContext();
        }

        public virtual IQueryable<T> GetAll()
        {
            return _context.Set<T>();
        }

        protected bool IsDetached(T entity)
        {
            return _context.Entry(entity).State == EntityState.Detached;
        }

        protected void SaveContext()
        {
            _context.SaveChanges();
        }
    }
}
