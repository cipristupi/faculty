﻿using System.Collections.Generic;
using System.Linq;

namespace ApprovalModule.Repository
{
    public class BoardRepository : BaseRepository<Board>
    {
        public Board GetById(int id)
        {
            return GetAll().FirstOrDefault(x => x.Id == id);
        }


        public string GetCurrentReviewRole(int id)
        {
            return GetById(id).CurrentReviewRole;
        }

      
    }
}
