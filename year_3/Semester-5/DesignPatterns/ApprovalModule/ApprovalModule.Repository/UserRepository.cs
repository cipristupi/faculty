﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApprovalModule.Repository
{
    class UserRepository :BaseRepository<AspNetUser>
    {
        public AspNetUser GetById(string id)
        {
            return GetAll().FirstOrDefault(x => x.Id == id);
        }

        public string GetRoleForUser(string id)
        {
            string role = string.Empty;
            var user = _context.AspNetUsers.Include("AspNetRoles").FirstOrDefault(x => x.Id == id);
            if (user != null)
            {
                var aspNetRole = user.AspNetRoles.FirstOrDefault(x => x.AspNetUsers == user);
                role = aspNetRole != null ? aspNetRole.Name : string.Empty;
            }
            return role;
        }
    }
}
