﻿using System;
using ApprovalModule.Repository;
using AutoMapper;
using Newtonsoft.Json;

namespace Utils
{
    public class MementoHandler
    {
        private readonly MementoRepository _mementoRepository;
        private readonly BoardRepository _boardRepository;
        public MementoHandler()
        {
            _mementoRepository = new MementoRepository();
            _boardRepository = new BoardRepository();
        }
        public MementoEntry CreateMemento(Board board)
        {
            Memento memento = new Memento
            {
                State = JsonConvert.SerializeObject(board),
                DateCreated = DateTime.Now,
                BoardId = board.Id
            };
            _mementoRepository.Add(memento);
            return Mapper.Map<MementoEntry>(memento);
        }

        public void RestoreMemento(MementoEntry memento)
        {
            Board board = (Board) JsonConvert.DeserializeObject(memento.State);
            _boardRepository.Update(board,board.Id);
        }

        public void RestoreMementoByMememtoId(int mementoId)
        {
            Memento memento = _mementoRepository.GetById(mementoId);
            Board board = (Board)JsonConvert.DeserializeObject(memento.State);
            _boardRepository.Update(board, board.Id);
        }

        public void RestoreMementoByBoardId(int boardId)
        {
            Memento memento = _mementoRepository.GetFirstByBoardId(boardId);
            Board board = JsonConvert.DeserializeObject<Board>(memento.State);
            _boardRepository.Update(board, board.Id);
        }
    }
}
