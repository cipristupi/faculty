﻿namespace Utils
{
    public enum ReviewStatuses
    {
        Accept,
        Reject,
        NotDecided
    } 
}
