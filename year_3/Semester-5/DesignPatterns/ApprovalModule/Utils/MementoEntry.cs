﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utils
{
    public class MementoEntry
    {
        public int Id { get; set; }
        public string State { get; set; }
    }
}
