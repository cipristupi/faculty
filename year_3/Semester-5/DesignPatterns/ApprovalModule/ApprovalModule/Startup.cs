﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ApprovalModule.Startup))]
namespace ApprovalModule
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
