﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using ApprovalModule.Models.ViewModels;
using ApprovalModule.Repository;
using AutoMapper;

namespace ApprovalModule.Controllers
{
    public class RolesManagementController : BaseController
    {
        private readonly RolesRepository _roleRepository;
        public RolesManagementController()
        {
            _roleRepository = new RolesRepository();
        }
        public ActionResult RolesIndex()
        {
            List<RoleViewModel> roles = new List<RoleViewModel>();
            foreach (var item in _roleRepository.GetAll())
            {
                roles.Add(Mapper.Map<RoleViewModel>(item));
            }
            return View("RolesIndex", roles);
        }

        public ActionResult RoleCreate()
        {
            return View("RoleCreate");
        }

        [HttpPost]
        public ActionResult RoleCreate(RoleViewModel model)
        {
            if (ModelState.IsValid)
            {
                var aspNetRole = Mapper.Map<AspNetRole>(model);
                aspNetRole.Id = Guid.NewGuid().ToString();
                _roleRepository.Add(aspNetRole);
                return RedirectToAction("RolesIndex");
            }
            ViewBag.ModelState = ModelState;
            return View("RoleCreate", model);

        }

        public ActionResult RoleEdit(string id)
        {
            var roleViewModel = Mapper.Map<RoleViewModel>(_roleRepository.GetById(id));
            return View(roleViewModel);
        }

        [HttpPost]
        public ActionResult RoleEdit(string id, RoleViewModel model)
        {
            if (ModelState.IsValid)
            {
                AspNetRole aspNetRole = Mapper.Map<AspNetRole>(model);
                _roleRepository.Update(aspNetRole,0);
                ViewBag.Succes = true;
                return View("RoleEdit", model);
            }
            ViewBag.ModelState = ModelState;
            return View(model);
        }

        public ActionResult RoleDelete(string id)
        {
            _roleRepository.Delete(id);
            return RedirectToAction("RolesIndex");
        }
    }
}
