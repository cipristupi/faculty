﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;
using ApprovalModule.Models;
using Microsoft.AspNet.Identity;

namespace ApprovalModule.Controllers
{
    public class BaseController : RootController
    {
        protected ApplicationUser LoggedUser { get; private set; }
        protected  string LoggedUserRole { get; private set; }
        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            if (!User.Identity.IsAuthenticated) return;
            LoggedUser = UserManager.FindById(User.Identity.GetUserId());
            var userRoles = UserManager.GetRolesAsync(LoggedUser.Id).Result.ToList();
            ViewBag.CurrentRole = userRoles.Count != 0 ? userRoles[0] : "";
            LoggedUserRole = userRoles.Count != 0 ? userRoles[0] : "";
            ViewBag.LoggedUser = LoggedUser;
        }
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            ViewBag.Succes = false;
        }
    }
}
