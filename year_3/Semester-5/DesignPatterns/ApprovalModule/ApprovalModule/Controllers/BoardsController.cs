﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ApprovalModule.Models.ViewModels;
using ApprovalModule.Repository;
using AutoMapper;
using Utils;

namespace ApprovalModule.Controllers
{
    public class BoardsController : BaseController
    {
        readonly BoardRepository _boardRepository;
        readonly MementoHandler _mementoHandler;
        readonly ReviewersRepository _reviewersRepository;
        const string ErrorTag = "ERROR";
        public BoardsController()
        {
            _boardRepository = new BoardRepository();
            _mementoHandler = new MementoHandler();
            _reviewersRepository = new ReviewersRepository();
        }
        public ActionResult BoardsIndex()
        {
            var boards = _boardRepository.GetAll().Select(Mapper.Map<BoardViewModel>);
            return View(boards);
        }
        public ActionResult BoardsCreate()
        {
            return View();
        }

        [HttpPost]
        public ActionResult BoardsCreate(BoardViewModel model)
        {
            if (ModelState.IsValid)
            {
                Board board = Mapper.Map<Board>(model);
                board.Owner = LoggedUser.Id;
                board.DateCreated = DateTime.Now;
                board.CurrentReviewRole = string.Empty;
                _boardRepository.Add(board);
                _mementoHandler.CreateMemento(board);
                return RedirectToAction("BoardsIndex");
            }
            ViewBag.ModelState = ModelState;
            return View(model);
        }

        public ActionResult BoardsEdit(int id)
        {
            var board = Mapper.Map<BoardViewModel>(_boardRepository.GetById(id));
            return View(board);
        }

        [HttpPost]
        public ActionResult BoardsEdit(BoardViewModel model)
        {
            if (ModelState.IsValid)
            {
                Board board = Mapper.Map<Board>(model);
                _boardRepository.Update(board, board.Id);
            }
            ViewBag.ModelState = ModelState;
            return View(model);
        }


        public ActionResult BoardsDelete(int id)
        {
            _boardRepository.Delete(id);
            return View("BoardsIndex");
        }

        public ActionResult BoardsReview(int id)
        {
            var board = Mapper.Map<BoardViewModel>(_boardRepository.GetById(id));
            PopulateReviewStatuses(board);
            return View(board);
        }

        private static void PopulateReviewStatuses(BoardViewModel board)
        {
            board.CurrentReviewer = new ReviewerViewModel { Status = new List<SelectListItem>() };
            for (int i = 0; i < 3; i++)
            {
                board.CurrentReviewer.Status.Add(new SelectListItem()
                {
                    Text = ((ReviewStatuses)i).ToString(),
                    Value = i.ToString()
                });
            }
        }

        [HttpPost]
        public ActionResult BoardsReview(BoardViewModel model)
        {
            PopulateReviewStatuses(model);
            if (!_reviewersRepository.CheckIfReviewExits(model.CurrentReviewer.UserId, model.Id))
            {
                Reviewer reviewer = Mapper.Map<Reviewer>(model.CurrentReviewer);
                reviewer.ReviewDate = DateTime.Now;
                reviewer.BoardId = model.Id;
                reviewer.UserId = LoggedUser.Id;
                reviewer.ReviewStatus = model.ReviewStatus;
                string currentReviewRole = _boardRepository.GetCurrentReviewRole(model.Id);
                if (string.IsNullOrEmpty(currentReviewRole) && LoggedUserRole != "Lead")
                {
                    ModelState.AddModelError(ErrorTag, "First must be reviewed by technical lead");
                    ViewBag.ModelState = ModelState;
                    return View(model);
                }
                if (!string.IsNullOrEmpty(currentReviewRole) && LoggedUserRole == "Lead")
                {
                    ModelState.AddModelError(ErrorTag, "Board is reviewed by technical lead");
                    ViewBag.ModelState = ModelState;
                    return View(model);
                }
                //if (CheckIfBoardHasStatusNotDecided(model.Id))
                //{
                //    ModelState.AddModelError(ErrorTag, "You can not review current board");
                //    ViewBag.ModelState = ModelState;
                //    return View(model);
                //}
                _reviewersRepository.Add(reviewer);
                UpdateBoard(model.Id, LoggedUserRole);
                if (LoggedUserRole == "Architect" && reviewer.ReviewStatus == (int)ReviewStatuses.Reject)
                {
                    _mementoHandler.RestoreMementoByBoardId(model.Id);
                }
            }
            return View(model);
        }

        private void UpdateBoard(int id, string loggedUserRole)
        {
            Board board = _boardRepository.GetById(id);
            board.CurrentReviewRole = loggedUserRole;
            _boardRepository.Update(board, id);
        }

        private bool CheckIfBoardHasStatusNotDecided(int boardId)
        {
            return
                _reviewersRepository.GetReviewersByBoardId(boardId)
                    .Count(x => x.ReviewStatus == (int)ReviewStatuses.NotDecided) > 0;
        }
    }
}
