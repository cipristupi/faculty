﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using ApprovalModule.Models;
using ApprovalModule.Models.ViewModels;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace ApprovalModule.Controllers
{
    public class UsersManagementController : BaseController
    {
        public ActionResult UsersIndex()
        {
            List<UserViewModel> users = new List<UserViewModel>();
            foreach (var user in UserManager.Users.ToList())
            {
                var userViewModel = Mapper.Map<UserViewModel>(user);
                var role = UserManager.GetRoles(userViewModel.Id);
                userViewModel.Role = role.Count != 0 ? role[0] : "";
                users.Add(userViewModel);
            }
            return View("UsersIndex", users);
        }

        public ActionResult UserCreate()
        {
            var model = new UserViewModel { RolesList = GetRolesAsSelectListItem() };
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> UserCreate(UserViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser
                {
                    UserName = model.Email,
                    Email = model.Email,
                    FirstName = model.FirstName,
                    LastName = model.LastName
                };
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    var role = GetRole(model.Role);
                    await UserManager.AddToRoleAsync(user.Id, role);
                    return RedirectToAction("UsersIndex");
                }
            }
            ViewBag.ModelState = ModelState;
            model.RolesList = GetRolesAsSelectListItem();
            return View("UserCreate", model);
        }

        public ActionResult UserEdit(string id)
        {
            var aspNetUser = UserManager.FindById(id);
            var userViewModel = Mapper.Map<UserViewModel>(aspNetUser);
            var roles = UserManager.GetRoles(userViewModel.Id);
            userViewModel.Role = roles.Count != 0 ? roles[0] : "";
            userViewModel.RolesList = GetRolesAsSelectListItem();
            return View("UserEdit", userViewModel);
        }



        [HttpPost]
        public async Task<ActionResult> UserEdit(string id, UserViewModel model)
        {
            UserManager<ApplicationUser> userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
            if (string.IsNullOrEmpty(model.Password))
            {
                ModelState.Remove("Password");
            }
            if (ModelState.IsValid)
            {
                ApplicationUser applicationUser = await UserManager.FindByIdAsync(model.Id);
                applicationUser.FirstName = model.FirstName;
                applicationUser.LastName = model.LastName;

                IList<string> rolesList = UserManager.GetRoles(applicationUser.Id);
                foreach (string r in rolesList)
                {
                    UserManager.RemoveFromRole(applicationUser.Id, r);
                }
                var role = GetRole(model.Role);
                await UserManager.AddToRoleAsync(applicationUser.Id, role);
                ViewBag.Succes = true;
                model.RolesList = GetRolesAsSelectListItem();
                return View("UserEdit", model);
            }
            ViewBag.ModelState = ModelState;
            model.RolesList = GetRolesAsSelectListItem();
            return View("UserEdit", model);
        }

        public ActionResult UserDelete(string id)
        {
            ApplicationUser aspNetUser = UserManager.FindById(id);
            var userViewModel = Mapper.Map<UserViewModel>(aspNetUser);
            return View("UserDelete", userViewModel);
        }

        public ActionResult Delete(string id, UserViewModel model)
        {
            ApplicationUser applicationUser = UserManager.FindById(id);
            UserManager.Delete(applicationUser);
            return RedirectToAction("UsersIndex");
        }

        public List<string> GetRoles()
        {
            ApplicationDbContext db = new ApplicationDbContext();
            return db.Roles.Select(r => r.Name).ToList();
        }

        private string GetRole(string id)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            return db.Roles.Where(x => x.Id == id).Select(r => r.Name).FirstOrDefault();
        }

        private List<SelectListItem> GetRolesAsSelectListItem()
        {
            ApplicationDbContext db = new ApplicationDbContext();
            return db.Roles.Select(r => new SelectListItem()
            {
                Text = r.Name,
                Value = r.Id
            }).ToList();
        }
    }
}
