﻿using ApprovalModule.Models;
using ApprovalModule.Models.ViewModels;
using ApprovalModule.Repository;
using AutoMapper;
using Utils;

namespace ApprovalModule.Helpers
{
    public class MapperConfiguration
    {
        public static void Initilize()
        {
            Mapper.CreateMap<AspNetRole, RoleViewModel>();
            Mapper.CreateMap<RoleViewModel,AspNetRole>();

            //User
            Mapper.CreateMap<UserViewModel, ApplicationUser>().ReverseMap();
            Mapper.CreateMap<ApplicationUser, UserViewModel>().ReverseMap();


            Mapper.CreateMap<Reviewer, ReviewerViewModel>().ReverseMap();
            Mapper.CreateMap<Board, BoardViewModel>().ReverseMap();

            Mapper.CreateMap<Memento, MementoEntry>().ReverseMap();
        }
    }
}
