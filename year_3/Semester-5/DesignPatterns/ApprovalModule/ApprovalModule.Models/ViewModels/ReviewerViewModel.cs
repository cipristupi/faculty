﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace ApprovalModule.Models.ViewModels
{
    public class ReviewerViewModel
    {
        public int Id { get; set; }
        public int BoardId { get; set; }
        public string UserId { get; set; }
        public DateTime ReviewDate { get; set; }
        public int ReviewStatus { get; set; }
        public List<SelectListItem> Status { get; set; }
    }
}
