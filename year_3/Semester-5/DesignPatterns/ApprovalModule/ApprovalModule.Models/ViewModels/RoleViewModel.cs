﻿using System.ComponentModel.DataAnnotations;

namespace ApprovalModule.Models.ViewModels
{
    public class RoleViewModel
    {
        [Required]
        public string Name { get; set; }
        public string Id { get; set; }
    }
}
