﻿using System;

namespace ApprovalModule.Models.ViewModels
{
    public class BoardViewModel
    {
       
        public int Id { get; set; }
        public string Description { get; set; }
        public string BoardContent { get; set; }
        public string Owner { get; set; }
        public string OwnerName { get; set; }
        public DateTime DateCreated { get; set; }
        public string CurrentReviewRole { get; set; }

        public ReviewerViewModel CurrentReviewer { get; set; }
        public bool ReadyForReview { get; set; }
        public int ReviewStatus { get; set; }
    }
}
