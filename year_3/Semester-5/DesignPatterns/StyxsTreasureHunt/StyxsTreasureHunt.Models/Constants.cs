﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StyxsTreasureHunt.Models
{
   public static class Constants
   {
       public const string Bag = "Bag";
       public const string Guardian = "Guardian";
       public const string Goblin = "Goblin";
       public const string Hobgoblin = "Hobgoblin";
       public static string BugBear = "BugBear";
   }
}
