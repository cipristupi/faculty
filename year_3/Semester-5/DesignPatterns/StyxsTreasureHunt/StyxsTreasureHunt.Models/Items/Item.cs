﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StyxsTreasureHunt.Models.Items
{
    public class Item
    {
        public int Value { get; set; }
        public string Name { get; set; }
    }
}
