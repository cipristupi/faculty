﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StyxsTreasureHunt.Models
{
    public class Guardians
    {
        public string Name { get; set; }
        public int Perception { get; set; }
    }
}
