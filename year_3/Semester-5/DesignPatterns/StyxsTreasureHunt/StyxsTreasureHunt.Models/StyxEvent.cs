﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StyxsTreasureHunt.Models.Items;

namespace StyxsTreasureHunt.Models
{
    public class StyxEvent
    {
        public List<Guardians> Guardians { get; set; }
        public int Difficulty { get; set; }
        public List<Item> Items { get; set; }
        public string Winner { get; set; }
        public string GoblinStealthValue { get; set; }
        public string GuardiansPerception { get; set; }
    }
}
