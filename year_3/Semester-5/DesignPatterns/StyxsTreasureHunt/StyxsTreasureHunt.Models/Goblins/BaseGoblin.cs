﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StyxsTreasureHunt.Models.Items;

namespace StyxsTreasureHunt.Models.Goblins
{
    public class BaseGoblin
    {
        private readonly Random _random = new Random(Guid.NewGuid().GetHashCode());

        public string Id { get; set; }
        public string Name { get; set; }
        public float Height { get; set; }
        public string SkinColor { get; set; }
        public string EyeColor { get; set; }
        public int GoldTeethNumber { get; set; }

        public int Stealth { get; set; }
        public int Treasure { get; set; }
        public int GuardsFlee { get; set; }

        public int InventorySlots { get; set; }

        public List<Item> InventoryItems { get; set; } 

        public List<StyxEvent> Events { get; set; } 

        public int BonusByType { get; set; }

        public string Type { get; set; }
        public BaseGoblin()
        {
            Id = Guid.NewGuid().ToString();
            InventorySlots = 10;
            InventoryItems =new List<Item>();
            Events = new List<StyxEvent>();
            Stealth = _random.Next(1, 10);
            Treasure = _random.Next(1, 10);
            GuardsFlee = _random.Next(1, 10);
        }
    }
}
