﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using StyxsTreasureHunt.Models;
using StyxsTreasureHunt.Models.Goblins;
using StyxsTreasureHunt.Models.Items;
using StyxsTreasureHunt.Properties;

namespace StyxsTreasureHunt
{
    public partial class GoblinDashboard : UserControl
    {
        public GoblinDashboard()
        {
            InitializeComponent();
        }

        public string ID { get; set; }
        private BaseGoblin _goblin;
        public GoblinDashboard(BaseGoblin goblin)
        {
            InitializeComponent();
            nameLabel.Text = string.Format("Name: {0}", goblin.Name);
            heightLabel.Text = string.Format("Height: {0}", goblin.Height);
            skinColorLabel.Text = string.Format("Skin Color: {0}", goblin.SkinColor);
            eyeColorLabel.Text = string.Format("Eye Color: {0}", goblin.EyeColor);
            goldTeethLabel.Text = string.Format("Gold Teeths: {0}", goblin.GoldTeethNumber);
            treasureLabel.Text = string.Format("Treasure: {0}", goblin.Treasure);
            inventorySlotsLabel.Text = string.Format("Invetory Slots: {0}//{1}", goblin.InventoryItems.Count, goblin.InventorySlots);
            guardsFleeLabel.Text = string.Format("Guards Flee: {0}", goblin.GuardsFlee);
            stealthLabel.Text = string.Format("Stealth: {0}", goblin.Stealth);
            ID = goblin.Id;
            int attributeWithoutBonus;
            if (goblin.Type == Constants.Goblin)
            {
                profilePicture.Image = Resources.ResourceManager.GetObject("goblin") as Image; 
                goblinTypeLabel.Text = "Goblin type: Goblin";
                attributeWithoutBonus = goblin.Stealth - goblin.BonusByType;
                stealthLabel.Text = string.Format("Stealth: {0} Bonus: {1}", attributeWithoutBonus,goblin.BonusByType);
            }

            if (goblin.Type == Constants.BugBear)
            {
                profilePicture.Image = Resources.ResourceManager.GetObject("bugbear") as Image;
                goblinTypeLabel.Text = "Goblin type: Bugbear";
                attributeWithoutBonus = goblin.Treasure - goblin.BonusByType;
                treasureLabel.Text = string.Format("Treasure: {0} Bonus: {1}", attributeWithoutBonus, goblin.BonusByType);
            }
            if (goblin.Type == Constants.Hobgoblin)
            {
                profilePicture.Image = Resources.ResourceManager.GetObject("hobgoblin") as Image;
                goblinTypeLabel.Text = "Goblin type: Hobgoblin";
                treasureLabel.Text = string.Format("Guards Flee: {0} Bonus: 10%", goblin.GuardsFlee);
            }
            _goblin = goblin;
            profilePicture.Refresh();
        }

        public void AddChallengeToChallengeList(StyxEvent styxEvent)
        {
            _goblin.Events.Add(styxEvent);
            int bagsFromEvent = styxEvent.Items.Count(x => x.Name == Constants.Bag);
            var itemsWithoutBags = styxEvent.Items.Where(x => x.Name != Constants.Bag).ToList();
            if (bagsFromEvent > 0)
            {
                var slotsFromBags = 8 * bagsFromEvent;
                _goblin.InventorySlots = _goblin.InventorySlots + slotsFromBags;
            }
            int itemsAvailbleAfterEvent = _goblin.InventoryItems.Count + itemsWithoutBags.Count;

            if (itemsAvailbleAfterEvent > _goblin.InventorySlots)
            {
                int itemsICanAdd = _goblin.InventorySlots - _goblin.InventoryItems.Count;
                for (int i = 0; i < itemsICanAdd; i++)
                {
                    var item = itemsWithoutBags[i];
                    _goblin.InventoryItems.Add(item);
                }
            }
            else
            {
                foreach (var item in itemsWithoutBags)
                {
                    _goblin.InventoryItems.Add(item);
                }
            }

            UpdateDashboard(styxEvent);
        }

        private void UpdateDashboard(StyxEvent styxEvent)
        {
            inventorySlotsLabel.Text = string.Format("Invetory Slots {0}/{1}", _goblin.InventoryItems.Count,
                _goblin.InventorySlots);
            inventorySlotsLabel.Refresh();
            invetoryListBox.BeginUpdate();
            foreach (var item in styxEvent.Items)
            {
                invetoryListBox.Items.Add(string.Format("{0} - Value {1}", item.Name, item.Value));
            }
            invetoryListBox.EndUpdate();
            eventsListBox.BeginUpdate();
            eventsListBox.Items.Add(string.Format("Guardians Number - {0} " +
                                                  "| Difficulty - {1} " +
                                                  "| Items Number - {2} " +
                                                  "| Gurdian Perception - {3} " +
                                                  "| Goblin stealth Value - {4} " +
                                                  "| Challenge Winner - {5} ",
                                                  styxEvent.Guardians.Count, 
                                                  styxEvent.Difficulty,
                                                  styxEvent.Items.Count,
                                                  styxEvent.GuardiansPerception,
                                                  styxEvent.GoblinStealthValue,
                                                  styxEvent.Winner));
            eventsListBox.EndUpdate();
            itemsValueLabel.Text = string.Format("Items Value: {0}", _goblin.InventoryItems.Sum(x => x.Value));
            itemsValueLabel.Refresh();
        }

        public void Winner(int winnerGoblinItemsValue)
        {
            BackgroundImage = Resources.ResourceManager.GetObject("winner") as Image;
        }
    }
}
