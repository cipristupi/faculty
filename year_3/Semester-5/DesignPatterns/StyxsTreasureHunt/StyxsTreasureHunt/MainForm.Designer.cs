﻿namespace StyxsTreasureHunt
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.runningTime = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.challengesNumberTxtBox = new System.Windows.Forms.TextBox();
            this.challengeNumberLabel = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.goblinTypeCmb = new System.Windows.Forms.ComboBox();
            this.goldTeethTxtBox = new System.Windows.Forms.TextBox();
            this.eyeColorTxtBox = new System.Windows.Forms.TextBox();
            this.skinColorTxtBox = new System.Windows.Forms.TextBox();
            this.heightTextBox = new System.Windows.Forms.TextBox();
            this.nameTxtBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.resetBttn = new System.Windows.Forms.Button();
            this.startChallengesBttn = new System.Windows.Forms.Button();
            this.addNewGoblinBttn = new System.Windows.Forms.Button();
            this.flowLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoScroll = true;
            this.flowLayoutPanel1.Controls.Add(this.panel1);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1315, 579);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.GhostWhite;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.runningTime);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.challengesNumberTxtBox);
            this.panel1.Controls.Add(this.challengeNumberLabel);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.goblinTypeCmb);
            this.panel1.Controls.Add(this.goldTeethTxtBox);
            this.panel1.Controls.Add(this.eyeColorTxtBox);
            this.panel1.Controls.Add(this.skinColorTxtBox);
            this.panel1.Controls.Add(this.heightTextBox);
            this.panel1.Controls.Add(this.nameTxtBox);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.resetBttn);
            this.panel1.Controls.Add(this.startChallengesBttn);
            this.panel1.Controls.Add(this.addNewGoblinBttn);
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1300, 103);
            this.panel1.TabIndex = 0;
            // 
            // runningTime
            // 
            this.runningTime.AutoSize = true;
            this.runningTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.runningTime.Location = new System.Drawing.Point(523, 72);
            this.runningTime.Name = "runningTime";
            this.runningTime.Size = new System.Drawing.Size(96, 17);
            this.runningTime.TabIndex = 106;
            this.runningTime.Text = "Running Time";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(222, 72);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(146, 17);
            this.label7.TabIndex = 107;
            this.label7.Text = "Number of challenges";
            // 
            // challengesNumberTxtBox
            // 
            this.challengesNumberTxtBox.Location = new System.Drawing.Point(374, 71);
            this.challengesNumberTxtBox.Name = "challengesNumberTxtBox";
            this.challengesNumberTxtBox.Size = new System.Drawing.Size(100, 20);
            this.challengesNumberTxtBox.TabIndex = 10;
            this.challengesNumberTxtBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.challengesNumberTxtBox_KeyPress);
            // 
            // challengeNumberLabel
            // 
            this.challengeNumberLabel.AutoSize = true;
            this.challengeNumberLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.challengeNumberLabel.Location = new System.Drawing.Point(18, 72);
            this.challengeNumberLabel.Name = "challengeNumberLabel";
            this.challengeNumberLabel.Size = new System.Drawing.Size(128, 17);
            this.challengeNumberLabel.TabIndex = 108;
            this.challengeNumberLabel.Text = "Challenge x From y";
            this.challengeNumberLabel.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(429, 40);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(85, 17);
            this.label6.TabIndex = 105;
            this.label6.Text = "Goblin Type";
            // 
            // goblinTypeCmb
            // 
            this.goblinTypeCmb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goblinTypeCmb.FormattingEnabled = true;
            this.goblinTypeCmb.Items.AddRange(new object[] {
            "Goblin",
            "Hobgoblin",
            "BugBear"});
            this.goblinTypeCmb.Location = new System.Drawing.Point(523, 36);
            this.goblinTypeCmb.Name = "goblinTypeCmb";
            this.goblinTypeCmb.Size = new System.Drawing.Size(121, 21);
            this.goblinTypeCmb.TabIndex = 6;
            // 
            // goldTeethTxtBox
            // 
            this.goldTeethTxtBox.Location = new System.Drawing.Point(523, 7);
            this.goldTeethTxtBox.Name = "goldTeethTxtBox";
            this.goldTeethTxtBox.Size = new System.Drawing.Size(121, 20);
            this.goldTeethTxtBox.TabIndex = 5;
            this.goldTeethTxtBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.challengesNumberTxtBox_KeyPress);
            // 
            // eyeColorTxtBox
            // 
            this.eyeColorTxtBox.Location = new System.Drawing.Point(304, 41);
            this.eyeColorTxtBox.Name = "eyeColorTxtBox";
            this.eyeColorTxtBox.Size = new System.Drawing.Size(100, 20);
            this.eyeColorTxtBox.TabIndex = 4;
            // 
            // skinColorTxtBox
            // 
            this.skinColorTxtBox.Location = new System.Drawing.Point(304, 10);
            this.skinColorTxtBox.Name = "skinColorTxtBox";
            this.skinColorTxtBox.Size = new System.Drawing.Size(100, 20);
            this.skinColorTxtBox.TabIndex = 3;
            // 
            // heightTextBox
            // 
            this.heightTextBox.Location = new System.Drawing.Point(100, 38);
            this.heightTextBox.Name = "heightTextBox";
            this.heightTextBox.Size = new System.Drawing.Size(100, 20);
            this.heightTextBox.TabIndex = 2;
            // 
            // nameTxtBox
            // 
            this.nameTxtBox.Location = new System.Drawing.Point(100, 10);
            this.nameTxtBox.Name = "nameTxtBox";
            this.nameTxtBox.Size = new System.Drawing.Size(100, 20);
            this.nameTxtBox.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(429, 11);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(79, 17);
            this.label5.TabIndex = 104;
            this.label5.Text = "Gold Teeth";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(222, 41);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 17);
            this.label4.TabIndex = 103;
            this.label4.Text = "Eye Color";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(222, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 17);
            this.label3.TabIndex = 102;
            this.label3.Text = "Skin Color";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(18, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 17);
            this.label2.TabIndex = 101;
            this.label2.Text = "Height";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(18, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 17);
            this.label1.TabIndex = 100;
            this.label1.Text = "Name";
            // 
            // resetBttn
            // 
            this.resetBttn.Location = new System.Drawing.Point(802, 7);
            this.resetBttn.Name = "resetBttn";
            this.resetBttn.Size = new System.Drawing.Size(156, 48);
            this.resetBttn.TabIndex = 8;
            this.resetBttn.Tag = "15";
            this.resetBttn.Text = "Reset";
            this.resetBttn.UseVisualStyleBackColor = true;
            this.resetBttn.Click += new System.EventHandler(this.resetBttn_Click);
            // 
            // startChallengesBttn
            // 
            this.startChallengesBttn.Location = new System.Drawing.Point(961, 7);
            this.startChallengesBttn.Name = "startChallengesBttn";
            this.startChallengesBttn.Size = new System.Drawing.Size(151, 48);
            this.startChallengesBttn.TabIndex = 9;
            this.startChallengesBttn.Tag = "16";
            this.startChallengesBttn.Text = "Start Challenges";
            this.startChallengesBttn.UseVisualStyleBackColor = true;
            this.startChallengesBttn.Click += new System.EventHandler(this.startChallengesBttn_Click);
            // 
            // addNewGoblinBttn
            // 
            this.addNewGoblinBttn.Location = new System.Drawing.Point(650, 7);
            this.addNewGoblinBttn.Name = "addNewGoblinBttn";
            this.addNewGoblinBttn.Size = new System.Drawing.Size(146, 48);
            this.addNewGoblinBttn.TabIndex = 7;
            this.addNewGoblinBttn.Tag = "";
            this.addNewGoblinBttn.Text = "Add new goblin";
            this.addNewGoblinBttn.UseVisualStyleBackColor = true;
            this.addNewGoblinBttn.Click += new System.EventHandler(this.addNewGoblinBttn_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1315, 579);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Name = "MainForm";
            this.Text = "Form1";
            this.flowLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button resetBttn;
        private System.Windows.Forms.Button startChallengesBttn;
        private System.Windows.Forms.Button addNewGoblinBttn;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox goblinTypeCmb;
        private System.Windows.Forms.TextBox goldTeethTxtBox;
        private System.Windows.Forms.TextBox eyeColorTxtBox;
        private System.Windows.Forms.TextBox skinColorTxtBox;
        private System.Windows.Forms.TextBox heightTextBox;
        private System.Windows.Forms.TextBox nameTxtBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox challengesNumberTxtBox;
        private System.Windows.Forms.Label challengeNumberLabel;
        private System.Windows.Forms.Label runningTime;
    }
}

