﻿namespace StyxsTreasureHunt
{
    partial class GoblinDashboard
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.profilePicture = new System.Windows.Forms.PictureBox();
            this.goblinTypeLabel = new System.Windows.Forms.Label();
            this.goldTeethLabel = new System.Windows.Forms.Label();
            this.eyeColorLabel = new System.Windows.Forms.Label();
            this.skinColorLabel = new System.Windows.Forms.Label();
            this.heightLabel = new System.Windows.Forms.Label();
            this.nameLabel = new System.Windows.Forms.Label();
            this.eventsListBox = new System.Windows.Forms.ListBox();
            this.invetoryListBox = new System.Windows.Forms.ListBox();
            this.guardsFleeLabel = new System.Windows.Forms.Label();
            this.treasureLabel = new System.Windows.Forms.Label();
            this.stealthLabel = new System.Windows.Forms.Label();
            this.inventorySlotsLabel = new System.Windows.Forms.Label();
            this.itemsValueLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.profilePicture)).BeginInit();
            this.SuspendLayout();
            // 
            // profilePicture
            // 
            this.profilePicture.Location = new System.Drawing.Point(4, 4);
            this.profilePicture.Name = "profilePicture";
            this.profilePicture.Size = new System.Drawing.Size(109, 100);
            this.profilePicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.profilePicture.TabIndex = 0;
            this.profilePicture.TabStop = false;
            // 
            // goblinTypeLabel
            // 
            this.goblinTypeLabel.AutoSize = true;
            this.goblinTypeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.goblinTypeLabel.Location = new System.Drawing.Point(3, 271);
            this.goblinTypeLabel.Name = "goblinTypeLabel";
            this.goblinTypeLabel.Size = new System.Drawing.Size(85, 17);
            this.goblinTypeLabel.TabIndex = 26;
            this.goblinTypeLabel.Text = "Goblin Type";
            // 
            // goldTeethLabel
            // 
            this.goldTeethLabel.AutoSize = true;
            this.goldTeethLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.goldTeethLabel.Location = new System.Drawing.Point(3, 242);
            this.goldTeethLabel.Name = "goldTeethLabel";
            this.goldTeethLabel.Size = new System.Drawing.Size(79, 17);
            this.goldTeethLabel.TabIndex = 19;
            this.goldTeethLabel.Text = "Gold Teeth";
            // 
            // eyeColorLabel
            // 
            this.eyeColorLabel.AutoSize = true;
            this.eyeColorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.eyeColorLabel.Location = new System.Drawing.Point(3, 216);
            this.eyeColorLabel.Name = "eyeColorLabel";
            this.eyeColorLabel.Size = new System.Drawing.Size(69, 17);
            this.eyeColorLabel.TabIndex = 18;
            this.eyeColorLabel.Text = "Eye Color";
            // 
            // skinColorLabel
            // 
            this.skinColorLabel.AutoSize = true;
            this.skinColorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.skinColorLabel.Location = new System.Drawing.Point(3, 185);
            this.skinColorLabel.Name = "skinColorLabel";
            this.skinColorLabel.Size = new System.Drawing.Size(72, 17);
            this.skinColorLabel.TabIndex = 17;
            this.skinColorLabel.Text = "Skin Color";
            // 
            // heightLabel
            // 
            this.heightLabel.AutoSize = true;
            this.heightLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.heightLabel.Location = new System.Drawing.Point(3, 150);
            this.heightLabel.Name = "heightLabel";
            this.heightLabel.Size = new System.Drawing.Size(49, 17);
            this.heightLabel.TabIndex = 16;
            this.heightLabel.Text = "Height";
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nameLabel.Location = new System.Drawing.Point(3, 119);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(45, 17);
            this.nameLabel.TabIndex = 15;
            this.nameLabel.Text = "Name";
            // 
            // eventsListBox
            // 
            this.eventsListBox.FormattingEnabled = true;
            this.eventsListBox.HorizontalScrollbar = true;
            this.eventsListBox.Location = new System.Drawing.Point(750, 4);
            this.eventsListBox.Name = "eventsListBox";
            this.eventsListBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.eventsListBox.Size = new System.Drawing.Size(501, 277);
            this.eventsListBox.TabIndex = 28;
            // 
            // invetoryListBox
            // 
            this.invetoryListBox.FormattingEnabled = true;
            this.invetoryListBox.Location = new System.Drawing.Point(398, 4);
            this.invetoryListBox.Name = "invetoryListBox";
            this.invetoryListBox.Size = new System.Drawing.Size(328, 277);
            this.invetoryListBox.TabIndex = 29;
            // 
            // guardsFleeLabel
            // 
            this.guardsFleeLabel.AutoSize = true;
            this.guardsFleeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guardsFleeLabel.Location = new System.Drawing.Point(170, 70);
            this.guardsFleeLabel.Name = "guardsFleeLabel";
            this.guardsFleeLabel.Size = new System.Drawing.Size(86, 17);
            this.guardsFleeLabel.TabIndex = 32;
            this.guardsFleeLabel.Text = "Guards Flee";
            // 
            // treasureLabel
            // 
            this.treasureLabel.AutoSize = true;
            this.treasureLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.treasureLabel.Location = new System.Drawing.Point(170, 35);
            this.treasureLabel.Name = "treasureLabel";
            this.treasureLabel.Size = new System.Drawing.Size(66, 17);
            this.treasureLabel.TabIndex = 31;
            this.treasureLabel.Text = "Treasure";
            // 
            // stealthLabel
            // 
            this.stealthLabel.AutoSize = true;
            this.stealthLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stealthLabel.Location = new System.Drawing.Point(170, 4);
            this.stealthLabel.Name = "stealthLabel";
            this.stealthLabel.Size = new System.Drawing.Size(52, 17);
            this.stealthLabel.TabIndex = 30;
            this.stealthLabel.Text = "Stealth";
            // 
            // inventorySlotsLabel
            // 
            this.inventorySlotsLabel.AutoSize = true;
            this.inventorySlotsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inventorySlotsLabel.Location = new System.Drawing.Point(170, 103);
            this.inventorySlotsLabel.Name = "inventorySlotsLabel";
            this.inventorySlotsLabel.Size = new System.Drawing.Size(101, 17);
            this.inventorySlotsLabel.TabIndex = 33;
            this.inventorySlotsLabel.Text = "Inventory Slots";
            // 
            // itemsValueLabel
            // 
            this.itemsValueLabel.AutoSize = true;
            this.itemsValueLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.itemsValueLabel.Location = new System.Drawing.Point(170, 150);
            this.itemsValueLabel.Name = "itemsValueLabel";
            this.itemsValueLabel.Size = new System.Drawing.Size(81, 17);
            this.itemsValueLabel.TabIndex = 34;
            this.itemsValueLabel.Text = "Items Value";
            // 
            // GoblinDashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.HighlightText;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.itemsValueLabel);
            this.Controls.Add(this.inventorySlotsLabel);
            this.Controls.Add(this.guardsFleeLabel);
            this.Controls.Add(this.treasureLabel);
            this.Controls.Add(this.stealthLabel);
            this.Controls.Add(this.invetoryListBox);
            this.Controls.Add(this.eventsListBox);
            this.Controls.Add(this.goblinTypeLabel);
            this.Controls.Add(this.goldTeethLabel);
            this.Controls.Add(this.eyeColorLabel);
            this.Controls.Add(this.skinColorLabel);
            this.Controls.Add(this.heightLabel);
            this.Controls.Add(this.nameLabel);
            this.Controls.Add(this.profilePicture);
            this.Name = "GoblinDashboard";
            this.Size = new System.Drawing.Size(1254, 295);
            ((System.ComponentModel.ISupportInitialize)(this.profilePicture)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox profilePicture;
        private System.Windows.Forms.Label goblinTypeLabel;
        private System.Windows.Forms.Label goldTeethLabel;
        private System.Windows.Forms.Label eyeColorLabel;
        private System.Windows.Forms.Label skinColorLabel;
        private System.Windows.Forms.Label heightLabel;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.ListBox eventsListBox;
        private System.Windows.Forms.ListBox invetoryListBox;
        private System.Windows.Forms.Label guardsFleeLabel;
        private System.Windows.Forms.Label treasureLabel;
        private System.Windows.Forms.Label stealthLabel;
        private System.Windows.Forms.Label inventorySlotsLabel;
        private System.Windows.Forms.Label itemsValueLabel;
    }
}
