﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using StyxsTreasureHunt.GameEngine;
using StyxsTreasureHunt.Helpers;
using StyxsTreasureHunt.Helpers.Decorator;
using StyxsTreasureHunt.Helpers.Factory;
using StyxsTreasureHunt.Models;
using StyxsTreasureHunt.Models.Goblins;

namespace StyxsTreasureHunt
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            goblinTypeCmb.SelectedIndex = 0;
        }

        private const int WAITBETWEENCHALLENGES = 500;
        private Stopwatch stopwatch;

        private List<BaseGoblin> _goblins = new List<BaseGoblin>();
        private void addNewGoblinBttn_Click(object sender, EventArgs e)
        {
            GoblinFactory factory = new GoblinFactory();

            SpecialBonusGoblin decoratedGoblin;
            List<string> errorMessages = new List<string>();
            if (ValidInput(errorMessages))
            {
                var goblinType = goblinTypeCmb.Text;
                BaseGoblin baseGoblin = null;
                switch (goblinType)
                {
                    case "BugBear":
                        baseGoblin = (BugBear)factory.GetGoblin(typeof(BugBear).Name);
                        break;
                    case "Goblin":
                        baseGoblin = (Goblin)factory.GetGoblin(typeof(Goblin).Name);
                        break;
                    case "Hobgoblin":
                        baseGoblin = (Hobgoblin)factory.GetGoblin(typeof(Hobgoblin).Name);
                        break;
                }
                baseGoblin.Name = nameTxtBox.Text;
                baseGoblin.Height = float.Parse(heightTextBox.Text);
                baseGoblin.GoldTeethNumber = int.Parse(goldTeethTxtBox.Text);
                baseGoblin.EyeColor = eyeColorTxtBox.Text;
                baseGoblin.SkinColor = skinColorTxtBox.Text;

                decoratedGoblin = new SpecialBonusGoblin(baseGoblin);
                decoratedGoblin.ApplySpecialBonus(goblinType);
                BaseGoblin goblinWithBonus = null;
                switch (goblinType)
                {
                    case "BugBear":
                        goblinWithBonus = (BugBear)decoratedGoblin.GetGoblinWithSpecialBonus();
                        goblinWithBonus.Type = Constants.BugBear;
                        break;
                    case "Goblin":
                        goblinWithBonus = (Goblin)decoratedGoblin.GetGoblinWithSpecialBonus();
                        goblinWithBonus.Type = Constants.Goblin;
                        break;
                    case "Hobgoblin":
                        goblinWithBonus = (Hobgoblin)decoratedGoblin.GetGoblinWithSpecialBonus();
                        goblinWithBonus.Type = Constants.Hobgoblin;
                        break;
                }

                GoblinDashboard dashboard = new GoblinDashboard(goblinWithBonus);
                flowLayoutPanel1.Controls.Add(dashboard);
                _goblins.Add(goblinWithBonus);
                CleanInputs();
            }
            else
            {
                string errorMessage = string.Join("\n", errorMessages);
                MessageBox.Show(errorMessage, "Validation summary", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void CleanInputs()
        {
            nameTxtBox.Clear();
            heightTextBox.Clear();
            goldTeethTxtBox.Clear();
            eyeColorTxtBox.Clear();
            skinColorTxtBox.Clear();
        }

        private bool ValidInput(List<string> errorMessages)
        {
            bool valid = true;
            if (string.IsNullOrEmpty(nameTxtBox.Text))
            {
                errorMessages.Add("Name cannot be empty.");
                valid = false;
            }
            if (string.IsNullOrEmpty(heightTextBox.Text))
            {
                errorMessages.Add("Height cannot be empty.");
                valid = false;
            }

            if (string.IsNullOrEmpty(goldTeethTxtBox.Text))
            {
                errorMessages.Add("Gold teeth cannot be empty.");
                valid = false;
            }
            if (string.IsNullOrEmpty(eyeColorTxtBox.Text))
            {
                errorMessages.Add("Eye color cannot be empty.");
                valid = false;
            }
            if (string.IsNullOrEmpty(skinColorTxtBox.Text))
            {
                errorMessages.Add("Skin color cannot be empty.");
                valid = false;
            }
            float value;
            int goldTeeth;
            if (!float.TryParse(heightTextBox.Text, out value))
            {
                errorMessages.Add("Height value must be a number.");
                valid = false;
            }

            if (!int.TryParse(goldTeethTxtBox.Text, out goldTeeth))
            {
                errorMessages.Add("Height value must be a number.");
                valid = false;
            }
            if (value > 1.5)
            {
                errorMessages.Add("Height value can not be higher than 1.5.");
                valid = false;
            }
            return valid;
        }

        private void resetBttn_Click(object sender, EventArgs e)
        {
            for (int ix = flowLayoutPanel1.Controls.Count - 1; ix >= 0; ix--)
            {
                if (flowLayoutPanel1.Controls[ix] is GoblinDashboard)
                {
                    flowLayoutPanel1.Controls[ix].Dispose();
                }
            }
            _goblins.Clear();
        }

        private GoblinDashboard GetDashboard(string goblinId)
        {
            GoblinDashboard goblinDashboard;
            for (int ix = flowLayoutPanel1.Controls.Count - 1; ix >= 0; ix--)
            {
                if (flowLayoutPanel1.Controls[ix] is GoblinDashboard)
                {
                    goblinDashboard = flowLayoutPanel1.Controls[ix] as GoblinDashboard;
                    if (goblinDashboard.ID == goblinId)
                    {
                        return goblinDashboard;
                    }
                }
            }
            return null;
        }

        private void startChallengesBttn_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(challengesNumberTxtBox.Text))
            {
                stopwatch = new Stopwatch();
                stopwatch.Start();
                ChallengeGenerator challengeGenerator = new ChallengeGenerator();
                int challengesNumber = Convert.ToInt32(challengesNumberTxtBox.Text);
                challengeNumberLabel.Visible = true;
                for (int i = 1; i <= challengesNumber; i++)
                {
                    challengeNumberLabel.Text = string.Format("Challenge {0} From {1}", i, challengesNumber);
                    challengeNumberLabel.Refresh();
                    foreach (var goblin in _goblins)
                    {
                        StyxEvent styxEvent = challengeGenerator.GenerateChallenge(goblin);
                        var dashboard = GetDashboard(goblin.Id);
                        dashboard.AddChallengeToChallengeList(styxEvent);
                    }
                }


                ChallengeWinner();
                stopwatch.Stop();
                runningTime.Text = string.Format("Running time: {0}", stopwatch.Elapsed.TotalMinutes);
            }
            else
            {
                MessageBox.Show("Please probide Challenges Number");
            }
        }

        private void ChallengeWinner()
        {
            Dictionary<string, int> goblinItemsValue = new Dictionary<string, int>();
            foreach (var goblin in _goblins)
            {
                goblinItemsValue.Add(goblin.Id, goblin.InventoryItems.Sum(x => x.Value));
            }

            string winnerGoblinId = goblinItemsValue.OrderByDescending(x => x.Value).FirstOrDefault().Key;
            var dashboard = GetDashboard(winnerGoblinId);
            dashboard.Winner(0);
        }

        private void challengesNumberTxtBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }
    }
}
