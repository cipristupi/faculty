﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StyxsTreasureHunt.Models.Goblins;

namespace StyxsTreasureHunt.Helpers.Factory
{
    public class GoblinFactory
    {
        public BaseGoblin GetGoblin(string goblinType)
        {
            switch (goblinType)
            {
                case "BugBear":
                    return new BugBear();
                case "Goblin":
                    return new Goblin();
                case "Hobgoblin":
                    return new Hobgoblin();
            }
            return null;
        }
    }
}
