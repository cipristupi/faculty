﻿using StyxsTreasureHunt.Models.Goblins;

namespace StyxsTreasureHunt.Helpers.Decorator
{
    public class SpecialBonusGoblin : Decorator
    {
        public SpecialBonusGoblin(BaseGoblin goblin) : base(goblin)
        {
        }

        public void ApplySpecialBonus(string goblinType)
        {
            int bonus;
            switch (goblinType)
            {
                case "BugBear":
                    var plus = GuardsFlee * 10 / 100;
                    _baseGoblin.GuardsFlee = GuardsFlee + plus;
                    _baseGoblin.BonusByType = plus;
                    break;
                case "Goblin":
                    _baseGoblin.Stealth = Stealth + 2;
                    _baseGoblin.BonusByType = 2;
                    break;
                case "Hobgoblin":
                    _baseGoblin.Treasure = Treasure + 1;
                    _baseGoblin.BonusByType = 1;
                    break;
            }
        }

        public BaseGoblin GetGoblinWithSpecialBonus()
        {
            return _baseGoblin;
        }
    }
}
