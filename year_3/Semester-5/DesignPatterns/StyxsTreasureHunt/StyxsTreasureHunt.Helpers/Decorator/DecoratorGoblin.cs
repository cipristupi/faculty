﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StyxsTreasureHunt.Models.Goblins;

namespace StyxsTreasureHunt.Helpers.Decorator
{
    public class Decorator : BaseGoblin
    {
        protected BaseGoblin _baseGoblin;

        public Decorator(BaseGoblin goblin)
        {
            _baseGoblin = goblin;
        }
    }
}
