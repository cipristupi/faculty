﻿using System;
using System.Collections.Generic;
using System.Linq;
using StyxsTreasureHunt.Models;
using StyxsTreasureHunt.Models.Goblins;
using StyxsTreasureHunt.Models.Items;

namespace StyxsTreasureHunt.GameEngine
{
    public class ChallengeGenerator
    {
        readonly Random _random = new Random(Guid.NewGuid().GetHashCode());
        public StyxEvent GenerateChallenge(BaseGoblin goblin)
        {
            StyxEvent styxEvent = new StyxEvent();
            var goblinStealthValue = goblin.Stealth + _random.Next(1, 20);
            var guardians = GenerateGuardians();
            var guardianAverage = guardians.Sum(x => x.Perception)/guardians.Count;
            int guardiansPerception = guardianAverage + _random.Next(1,20);
            styxEvent.Difficulty = guardians.Count;
            styxEvent.Guardians = guardians;
            styxEvent.Items = new List<Item>();
            styxEvent.Items = GenerateItems(guardians.Count);
            styxEvent.Winner = goblinStealthValue > guardiansPerception ? Constants.Goblin : Constants.Guardian;
            styxEvent.GoblinStealthValue = goblinStealthValue.ToString();
            styxEvent.GuardiansPerception = guardiansPerception.ToString();
            return styxEvent;
        }

        private List<Item> GenerateItems(int difficulty)
        {
            int numberOfItems;
            List<Item> items= new List<Item>();
            if (difficulty >= 1 && difficulty <= 3)
            {
                numberOfItems = _random.Next(1, 10);
            }
            else
            {
                numberOfItems = _random.Next(1, 3);
            }
            for (int i = 0; i < numberOfItems; i++)
            {
                int value = _random.Next(1, 1000);
                if (value > 900)
                {
                    items.Add(new Bag()
                    {
                        Value = 0,
                        Name = Constants.Bag
                    });
                }
                else
                {
                    items.Add(new Item()
                    {
                        Value = value,
                        Name = "Item "+ value
                    });
                }
            }
            return items;
        } 
        private List<Guardians> GenerateGuardians()
        {
            List<Guardians> guardians = new List<Guardians>();
            var numberOfGuardians = _random.Next(1, 5);
            for (int i = 0; i < numberOfGuardians; i++)
            {
                guardians.Add(new Guardians()
                {
                    Perception = _random.Next(1,10),
                    Name = string.Format("Guardian-{0}",i)
                });
            }
            return guardians;
        }


    }
}
