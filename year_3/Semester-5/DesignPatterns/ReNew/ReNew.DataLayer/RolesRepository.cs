﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReNew.DataLayer
{
    public class RolesRepository : BaseRepository<AspNetRoles>
    {
        public override void Update(AspNetRoles entity, int id)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            var existing = _context.Set<AspNetRoles>().Find(entity.Id);
            if (existing == null) return;
            _context.Entry(existing).CurrentValues.SetValues(entity);
            SaveContext();
        }
    }
}
