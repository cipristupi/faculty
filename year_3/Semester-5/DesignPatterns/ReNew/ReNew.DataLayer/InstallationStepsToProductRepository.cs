﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReNew.DataLayer
{
    public class InstallationStepsToProductRepository : BaseRepository<InstallationSteps>
    {
        public void DeleteByCompoundedKey(InstallationSteps step)
        {
            InstallationSteps entity = _dbSet.Find(step.ID);//FirstOrDefault(x => x.StepId == step.StepId && x.ProductId ==step.ProductId);
            if (entity != null)
            {
                if (_context.Entry(entity).State == EntityState.Detached)
                {
                    _dbSet.Attach(entity);
                }
                _dbSet.Remove(entity);
            }
            SaveContext();
        }
    }
}
