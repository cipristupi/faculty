﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ReNew.Models.View_Models;

namespace ReNew.Models.Business_Models
{
    public class ProjectBusiness 
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Color { get; set; }
        public int UnitOfMeasureID { get; set; }

        public List<ProductsBusiness> ProductsToProject { get; set; }
    }
}
