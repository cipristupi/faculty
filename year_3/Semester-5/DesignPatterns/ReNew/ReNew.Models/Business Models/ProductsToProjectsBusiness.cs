﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReNew.Models.Business_Models
{
    public class ProductsToProjectsBusiness
    {
        public int ID { get; set; }
        public int ProjectId { get; set; }
        public int ProductId { get; set; }
        public int Quantity { get; set; }
    }
}
