﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReNew.Models.Business_Models
{
    public class AdditionalItemsToProductBusiness
    {
        public int ID { get; set; }
        public int ProductId { get; set; }
        public int AdditionalProductId { get; set; }
        public int Quantity { get; set; }
    }
}
