﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReNew.Models.Business_Models
{
    public class StepsBusiness
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public int StepOrder { get; set; }
    }
}
