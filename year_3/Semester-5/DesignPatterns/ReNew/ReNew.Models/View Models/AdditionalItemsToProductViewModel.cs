﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ReNew.Models.View_Models
{
    public class AdditionalItemsToProductViewModel :BaseViewModel
    {
        public ProductsViewModel Product { get; set; }
        public List<ProductsViewModel> AdditionaItemsToProduct { get; set; } 
        public List<ProductsViewModel> AvailableItems { get; set; } 
        public List<SelectListItem> AvailableItemsSelectList { get; set; } 
    }
}
