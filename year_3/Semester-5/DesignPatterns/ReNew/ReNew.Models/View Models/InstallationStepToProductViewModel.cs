﻿namespace ReNew.Models.View_Models
{
    public class InstallationStepToProductViewModel : BaseViewModel
    {
        public int ProductId { get; set; }
        public int StepId { get; set; }
        public int StepOrder { get; set; }
    }
}