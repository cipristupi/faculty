﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace ReNew.Models.View_Models
{
    public class ProjectViewModel : BaseViewModel
    {
        public int ID { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        public string UserId { get; set; }

        public List<ProductsViewModel> ProductsToProject { get; set; }

        public List<SelectListItem> AllProducts { get; set; } 

        public string ProductsQuantityToProject { get; set; }
    }
}
