﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace ReNew.Models.View_Models
{
    public class ProductsViewModel : BaseViewModel
    {
        public int ID { get; set; }

        [Required]
        [Display(Name = "Product Name")]
        public string Name { get; set; }
        [Required]
        [Display(Name = "Product Description")]
        public string Description { get; set; }
        [Required]
        [Display(Name = "Product Color")]
        public string Color { get; set; }

        public int UnitOfMeasureID { get; set; }

        [Display(Name="Unit Of Measure")]
        public string UnitOfMeasure { get; set; }

        public int Quantity { get; set; }
        [Required]
        public float Price { get; set; }

        public List<SelectListItem> UnitOfMeasureSelectListItems { get; set; }

        public List<ProductsViewModel> AdditionalProducts { get; set; }
        public List<StepsViewModel> InstallationSteps { get; set; }
    }
}
