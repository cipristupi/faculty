﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ReNew.Models.View_Models
{
    public class InstallationStepsToProductViewModel : BaseViewModel
    {
        [Display(Name = "Product Name")]
        public string ProductName { get; set; }
        public int ProductId { get; set; }

        [Display(Name = "Available Steps")]
        public List<SelectListItem> AvailableSteps { get; set; }
        public List<StepsViewModel> StepsViewModels { get; set; }
        public List<InstallationStepToProductViewModel> InstallationStepToProductViewModels { get; set; } //Steps to be saved for current product
    }
}
