﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReNew.Models.View_Models
{
    public class RoleViewModel : BaseViewModel
    {
        [Required]
        public string Name { get; set; }
        public string Id { get; set; }
    }
}
