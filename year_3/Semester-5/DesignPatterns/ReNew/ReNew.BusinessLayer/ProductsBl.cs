﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using ReNew.DataLayer;
using ReNew.Models.Business_Models;

namespace ReNew.BusinessLayer
{
    public class ProductsBl : IBusinessLayer<ProductsBusiness>
    {
        private readonly ProductsRepository _repository;
        private readonly UnitOfMeasuresBl _unitOfMeasuresBl;
        private readonly AdditionalItemsToProductBl _additionalItemsToProductBl;
        private readonly StepsBl _stepsBl;
        private readonly InstallationStepsToProductBl _installationStepsToProductBl;

        public ProductsBl()
        {
            _repository = new ProductsRepository();
            _unitOfMeasuresBl = new UnitOfMeasuresBl();
            _additionalItemsToProductBl = new AdditionalItemsToProductBl();
            _stepsBl = new StepsBl();
            _installationStepsToProductBl = new InstallationStepsToProductBl();
        }
        public void Add(ProductsBusiness entity)
        {
            var productDB = Mapper.Map<Products>(entity);
            _repository.Add(productDB);
        }

        public void Remove(object id)
        {
            _repository.Delete(id);
        }

        public void Update(ProductsBusiness entity)
        {
            var productDb = Mapper.Map<Products>(entity);
            _repository.Update(productDb, entity.ID);
        }

        public IEnumerable<ProductsBusiness> GetAllWithAdditionalProducts()
        {
            var productsBusinesses = _repository.GetAll().Select(Mapper.Map<ProductsBusiness>).ToList();
            for (int i = 0; i < productsBusinesses.Count; i++)
            {
                var productsBusiness = productsBusinesses[i];
                productsBusinesses[i].UnitOfMeasure = _unitOfMeasuresBl.GetById(productsBusiness.UnitOfMeasureID).Name;
                productsBusinesses[i].AdditionalProducts = GetAdditionalProducts(productsBusiness.ID).ToList();
                productsBusinesses[i].InstallationSteps = GetStepsDetailsById(productsBusiness.ID);
            }
            return productsBusinesses;
        }

        public IEnumerable<ProductsBusiness> GetAll()
        {
            var productsBusinesses = _repository.GetAll().Select(Mapper.Map<ProductsBusiness>).ToList();
            for (int i = 0; i < productsBusinesses.Count; i++)
            {
                var productsBusiness = productsBusinesses[i];
                productsBusinesses[i].UnitOfMeasure = _unitOfMeasuresBl.GetById(productsBusiness.UnitOfMeasureID).Name;
            }
            return productsBusinesses;
        }

        public ProductsBusiness GetProductById(int id)
        {
            var product = _repository.GetAll().FirstOrDefault(x => x.ID == id);
            var productBusiness = Mapper.Map<ProductsBusiness>(product);
            return productBusiness;
        }

        public ProductsBusiness GetById(int id)
        {
            var product = _repository.GetAll().FirstOrDefault(x => x.ID == id);
            var stepsForProject = _installationStepsToProductBl.GetStepsByProductId(id).ToList();
            var productBusiness = Mapper.Map<ProductsBusiness>(product);
            productBusiness.InstallationSteps = GetStepsDetails(stepsForProject);
            productBusiness.AdditionalProducts = GetAdditionalProducts(id).ToList();
            return productBusiness;
        }

        public IEnumerable<ProductsBusiness> GetAdditionalProducts(int productId)
        {
            List<ProductsBusiness> products = new List<ProductsBusiness>();
            foreach (var additionalItemsToProductBusiness in _additionalItemsToProductBl.GetAllAdditionalItemsByProductId(productId))
            {
                var additionalItem = GetById(additionalItemsToProductBusiness.AdditionalProductId);
                additionalItem.Quantity = additionalItemsToProductBusiness.Quantity;
                products.Add(additionalItem);
            }
            return products;
        }

        private List<StepsBusiness> GetStepsDetails(List<InstallationStepBusiness> stepToProductList)
        {
            List<StepsBusiness> steps = new List<StepsBusiness>();
            var step = new StepsBusiness();
            foreach (var stepToProductViewModel in stepToProductList)
            {
                step = Mapper.Map<StepsBusiness>(_stepsBl.GetById(stepToProductViewModel.StepId));
                step.StepOrder = stepToProductViewModel.StepOrder;
                steps.Add(step);
            }
            return steps;
        }

        private List<StepsBusiness> GetStepsDetailsById(int productId)
        {
            var stepsForProject = _installationStepsToProductBl.GetStepsByProductId(productId).ToList();
            return  GetStepsDetails(stepsForProject);
        } 
    }
}