﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using ReNew.DataLayer;
using ReNew.Models.Business_Models;

namespace ReNew.BusinessLayer
{
    public class StepsBl : IBusinessLayer<StepsBusiness>
    {
        private readonly StepsRepository _repository;
        private readonly InstallationStepsToProductBl _installationStepsToProductBl;
        public StepsBl()
        {
            _repository = new StepsRepository();
            _installationStepsToProductBl = new InstallationStepsToProductBl();
        }
        public void Add(StepsBusiness entity)
        {
            var stepsDB = AutoMapper.Mapper.Map<Steps>(entity);
            _repository.Add(stepsDB);
        }

        public void Remove(object id)
        {
            _repository.Delete(id);
        }

        public void Update(StepsBusiness entity)
        {
            var stepsDB = AutoMapper.Mapper.Map<Steps>(entity);
            _repository.Update(stepsDB, entity.ID);
        }

        public IEnumerable<StepsBusiness> GetAll()
        {
            return _repository.GetAll().ToList().Select(Mapper.Map<StepsBusiness>).ToList();
        }


        public StepsBusiness GetById(int id)
        {
            var step = _repository.GetAll().FirstOrDefault(x => x.ID == id);
            return Mapper.Map<StepsBusiness>(step);
        }

        public bool IsInstallationStep(int id)
        {
            var installationSteps = _installationStepsToProductBl.GetAll().Count(x => x.StepId == id);
            return installationSteps == 0;
        }
    }
}
