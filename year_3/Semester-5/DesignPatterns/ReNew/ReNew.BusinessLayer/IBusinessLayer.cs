﻿using System.Collections.Generic;

namespace ReNew.BusinessLayer
{
    interface IBusinessLayer<T> where T: class
    {
        void Add(T entity);

        void Remove(object entity);

        void Update(T entity);

        IEnumerable<T> GetAll();

    }
}
