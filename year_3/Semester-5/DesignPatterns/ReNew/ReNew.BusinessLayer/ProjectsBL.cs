﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using ReNew.DataLayer;
using ReNew.Models.Business_Models;
using ReNew.Models.View_Models;

namespace ReNew.BusinessLayer
{
    public class ProjectsBl : IBusinessLayer<ProjectsBusiness>
    {
        private readonly ProductsBl _productsBl;
        private readonly ProjectsRepository _repository;
        private readonly ProductsToProjectRepository _productsToProjectRepository;

        public ProjectsBl()
        {
            _productsBl = new ProductsBl();
            _repository = new ProjectsRepository();
            _productsToProjectRepository = new ProductsToProjectRepository();
        }

        public void Add(ProjectsBusiness entity)
        {
            var entityDb = Mapper.Map<Projects>(entity);
            _repository.Add(entityDb);
            SaveProductsToProject(entity.ProductsToProject, entityDb.ID);
        }

        public void Remove(object id)
        {
            _repository.Delete(id);
        }

        public void Update(ProjectsBusiness entity)
        {
            var entityDb = Mapper.Map<Projects>(entity);
            _repository.Update(entityDb, entityDb.ID);
            SaveProductsToProject(entity.ProductsToProject,entity.ID);
        }

        public IEnumerable<ProjectsBusiness> GetAll()
        {
            var projectBusinessIDs= _repository.GetAll().Select(x=>x.ID).ToList();
            var projectBusinessWithProducts = new List<ProjectsBusiness>();

            foreach (var projectsBusinessID in projectBusinessIDs)
            {
                projectBusinessWithProducts.Add(GetById(projectsBusinessID));
            }
            return projectBusinessWithProducts;
        }

        public IEnumerable<ProjectsBusiness> GetAllByUserId(string userId)
        {
            return
                _repository.GetAll()
                    .Where(x => x.UserId == userId)
                    .ToList()
                    .Select(Mapper.Map<ProjectsBusiness>)
                    .ToList();
        }

        public ProjectsBusiness GetById(int id)
        {
            var project = _repository.GetAll().FirstOrDefault(x => x.ID == id);
            var productsToProject = GetProductsToProject(id);
            var projectBusiness = Mapper.Map<ProjectsBusiness>(project);
            projectBusiness.ProductsToProject = GetProductsForProject(productsToProject);
            return projectBusiness;
        }

        private List<ProductsBusiness> GetProductsForProject(List<ProductsToProjectsBusiness> productsToProject)
        {
            var productsForProject = productsToProject.Select(productsToProjectsBusiness => _productsBl.GetById(productsToProjectsBusiness.ProductId)).ToList();
            int productQuantity;
            for (int i = 0; i < productsForProject.Count; i++)
            {
                productQuantity =
                    productsToProject.Where(y => y.ProductId == productsForProject[i].ID).Select(x => x.Quantity).FirstOrDefault();
                productsForProject[i].Quantity = productQuantity;
            }
            return productsForProject;
        }


        private List<ProductsToProjectsBusiness> GetProductsToProject(int projectId)
        {
            return
                _productsToProjectRepository.GetAll()
                    .Where(x => x.ProjectId == projectId)
                    .ToList()
                    .Select(Mapper.Map<ProductsToProjectsBusiness>)
                    .ToList();
        }

        private void SaveProductsToProject(List<ProductsBusiness> products, int projectId)
        {
            var existingProductsToProject = _productsToProjectRepository.GetAll().Where(x => x.ProjectId == projectId).ToList();
            DeleteExistingProductsToStep(existingProductsToProject);
            foreach (var product in products)
            {
                var productToProject = new ProductsToProjects()
                {
                    ProductId = product.ID,
                    ProjectId = projectId,
                    Quantity = product.Quantity
                };
                _productsToProjectRepository.Add(productToProject);
            }
        }

        private void DeleteExistingProductsToStep(IEnumerable<ProductsToProjects> existingProductsToProject)
        {
            foreach (var productsToProjects in existingProductsToProject)
            {
                _productsToProjectRepository.Delete(productsToProjects.ID);
            }
        }
    }
}
