﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using ReNew.DataLayer;
using ReNew.Models.Business_Models;

namespace ReNew.BusinessLayer
{
    public class UsersManagementBl
    {
        private readonly UsersRepository _repository;
        public UsersManagementBl(UsersRepository repository)
        {
            _repository = repository;
        }

        public UsersManagementBl()
        {
            _repository = new UsersRepository();
        }

    }
}
