﻿using System;
using System.Collections.Generic;
using System.IO.MemoryMappedFiles;
using System.Linq;
using AutoMapper;
using ReNew.DataLayer;
using ReNew.Models.Business_Models;
using ReNew.Models.View_Models;

namespace ReNew.BusinessLayer
{
    public class RolesManagementBl : IBusinessLayer<RoleBusiness>
    {
        private readonly RolesRepository _repository;
        public RolesManagementBl(RolesRepository repository)
        {
            _repository = repository;
        }

        public RolesManagementBl()
        {
            _repository =new RolesRepository();
        }
        public void Add(RoleBusiness model)
        {
            var role = Mapper.Map<AspNetRoles>(model);
            _repository.Add(role);
        }

        public void Remove(object id)
        {
            _repository.Delete(id);
        }

        public void Update(RoleBusiness model)
        {
            var role = Mapper.Map<AspNetRoles>(model);
            _repository.Update(role,0);
        }

        public IEnumerable<RoleBusiness> GetAll()
        {
            return _repository.GetAll().ToList().Select(Mapper.Map<RoleBusiness>).ToList();
        }

        public RoleBusiness GetById(string id)
        {
            return GetAll().FirstOrDefault(x => x.Id == id);
        }
    }
}
