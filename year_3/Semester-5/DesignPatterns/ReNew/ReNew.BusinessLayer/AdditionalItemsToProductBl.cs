﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using ReNew.DataLayer;
using ReNew.Models.Business_Models;

namespace ReNew.BusinessLayer
{
    public class AdditionalItemsToProductBl :IBusinessLayer<AdditionalItemsToProductBusiness>
    {
        readonly AdditionalItemsToProductRepository _repository = new AdditionalItemsToProductRepository();
        public void Add(AdditionalItemsToProductBusiness entity)
        {
            var entityDb = Mapper.Map<AdditionalItems>(entity);
            _repository.Add(entityDb);
        }

        public void Remove(object id)
        {
            _repository.Delete(id);
        }

        public void Update(AdditionalItemsToProductBusiness entity)
        {
            var entityDb = Mapper.Map<AdditionalItems>(entity);
            _repository.Update(entityDb,entityDb.ID);
        }

        public IEnumerable<AdditionalItemsToProductBusiness> GetAll()
        {
            return _repository.GetAll().ToList().Select(Mapper.Map<AdditionalItemsToProductBusiness>).ToList();
        }


        public IEnumerable<AdditionalItemsToProductBusiness> GetAllAdditionalItemsByProductId(int productId)
        {
            return _repository.GetAll().Where(x=>x.ProductId==productId).Select(Mapper.Map<AdditionalItemsToProductBusiness>).ToList();
        }


        public AdditionalItemsToProductBusiness GetById(int id)
        {
            var additionalItem = _repository.GetAll().FirstOrDefault(x => x.ID == id);
            return Mapper.Map<AdditionalItemsToProductBusiness>(additionalItem);
        }

        public void SaveAdditionalItems(List<AdditionalItemsToProductBusiness> list,int productId)
        {
            var existingItemsForProduct = GetAll().Where(x => x.ProductId == productId).ToList();
            DeleteExistingItemsForProduct(existingItemsForProduct);
            var modelDb =new  AdditionalItems();
            foreach (var item in list)
            {
                modelDb.AdditionalProductId = item.ID;
                modelDb.ProductId = productId;
                modelDb.Quantity = item.Quantity;
                _repository.Add(modelDb);
            }
        }

        private void DeleteExistingItemsForProduct(List<AdditionalItemsToProductBusiness> existingItemsForProduct)
        {
            foreach (var item in existingItemsForProduct)
            {
                _repository.Delete(item.ID);
            }
        }
    }
}
