﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using ReNew.DataLayer;
using ReNew.Models.Business_Models;
using ReNew.Models.View_Models;

namespace ReNew.BusinessLayer
{
    public class InstallationStepsToProductBl : IBusinessLayer<InstallationStepBusiness>
    {
        private readonly InstallationStepsToProductRepository _repository;

        public InstallationStepsToProductBl()
        {
            _repository = new InstallationStepsToProductRepository();
        }
        public void Add(InstallationStepBusiness step)
        {
            _repository.Add(Mapper.Map<InstallationSteps>(step));
        }

        public void Remove(object id)
        {
            _repository.Delete(id);
        }

        public void Update(InstallationStepBusiness step)
        {
            _repository.Update(Mapper.Map<InstallationSteps>(step), step.StepId);
        }

        public IEnumerable<InstallationStepBusiness> GetAll()
        {
            return _repository.GetAll().Select(Mapper.Map<InstallationStepBusiness>).ToList();
        }

        public void SaveInstallationSteps(List<InstallationStepBusiness> steps, int productId)
        {
            var existingStepsForProduct = GetAll().Where(x => x.ProductId == productId).ToList();
            DeleteExistingSteps(existingStepsForProduct);
            foreach (var step in steps)
            {
                _repository.Add(Mapper.Map<InstallationSteps>(step));
            }
        }

        private void DeleteExistingSteps(List<InstallationStepBusiness> existingSteps)
        {
            foreach (var step in existingSteps)
            {
                _repository.DeleteByCompoundedKey(Mapper.Map<InstallationSteps>(step));
            }
        }

        public IEnumerable<InstallationStepBusiness> GetStepsByProductId(int productId)
        {
            return GetAll().Where(x => x.ProductId == productId);
        }
    }
}
