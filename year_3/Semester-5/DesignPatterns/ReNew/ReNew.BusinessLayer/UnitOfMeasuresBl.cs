﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using ReNew.DataLayer;
using ReNew.Models.Business_Models;

namespace ReNew.BusinessLayer
{
    public class UnitOfMeasuresBl : IBusinessLayer<UnitOfMeasuresBusiness>
    {
        private UnitOfMeasuresRepository _repository;
        private readonly ProductsRepository _productsRepository;
        public UnitOfMeasuresBl()
        {
            _repository = new UnitOfMeasuresRepository();
            _productsRepository = new ProductsRepository();
        }
        public void Add(UnitOfMeasuresBusiness entity)
        {
            UnitOfMeasures dbEntity = Mapper.Map<UnitOfMeasures>(entity);
            _repository.Add(dbEntity);
        }

        public void Remove(object id)
        {
            _repository.Delete(id);
        }

        public void Update(UnitOfMeasuresBusiness entity)
        {
            UnitOfMeasures dbEntity = Mapper.Map<UnitOfMeasures>(entity);
            _repository.Update(dbEntity,entity.ID);
        }

        public IEnumerable<UnitOfMeasuresBusiness> GetAll()
        {
            return _repository.GetAll().ToList().Select(Mapper.Map<UnitOfMeasuresBusiness>).ToList();
        }

        public UnitOfMeasuresBusiness GetById(int id)
        {
            var unitOfMeasure = _repository.GetAll().FirstOrDefault(x => x.ID == id);
            return Mapper.Map<UnitOfMeasuresBusiness>(unitOfMeasure);
        }

        public bool IsNotAssignedToProduct(int id)
        {
            var productsWithGivenUnitOfMeasure = _productsRepository.GetAll().Count(x => x.UnitOfMeasureID == id);
            return productsWithGivenUnitOfMeasure == 0;
        }
    }
}
