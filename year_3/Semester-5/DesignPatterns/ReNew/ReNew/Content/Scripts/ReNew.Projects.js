﻿if (ReNew.Projects == undefined) {
    ReNew.Projects = {};
}
ReNew.Projects = function () {
    var self = this;
    self.init();
    self.bind();
};
ReNew.Projects.prototype.init = function () {
    var self = this;
    self.updateTotalPrice();
}
ReNew.Projects.prototype.bind = function () {
    var self = this;

    var removeRowFunction = function() {
        jQuery(this).closest('.mainProduct').remove();
    };
    jQuery('.removeRow').off('click').on('click', removeRowFunction);
    jQuery('.modelAddProduct').off('click').on('click', function() {
        var productId = jQuery('#Products').val();
        var quantity = jQuery('#Quantity').val();
        jQuery.ajax({
            type: 'post',
            url: '/Projects/GetProductById',
            data: { id: productId,quantity:quantity },
            success: function (data) {
                $('.products').append(data);
                jQuery('.removeRow').off('click').on('click', removeRowFunction);
                self.updateTotalPrice();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {

            }
        });
    });
    jQuery('.project-form').submit(function () {
        var allMainProducts = jQuery('.mainProduct');
        var productIdQuantityList = [];
        var mainProduct;
        var productId;
        var quantity;
        for (var i = 0; i < allMainProducts.length; i++) {
            mainProduct = allMainProducts[i];
            productId = jQuery(mainProduct).attr('productid');
            quantity = jQuery(mainProduct).find('.product-quantity').attr("value");
            if (jQuery.isNumeric(productId) && jQuery.isNumeric(quantity)) {
                productIdQuantityList.push(productId + "-" + quantity);
            }
        }
        var finalProducts = productIdQuantityList.join(",");
        jQuery('#ProductsQuantityToProject').val(finalProducts);
    });
};

ReNew.Projects.prototype.updateTotalPrice =function() {
    var self = this;
    var sum = 0;
    var allPrices = jQuery('.priceAndQuantity');
    var totalCostSpan = jQuery('.totalCost');
    var priceAndQuantity;
    var price;
    var quantity;
    for (var i = 0; i < allPrices.length; i++) {
        priceAndQuantity = allPrices[i];
        price = jQuery(priceAndQuantity).find('.product-price').attr("value");
        quantity = jQuery(priceAndQuantity).find('.product-quantity').attr("value");
        if (jQuery.isNumeric(price) && jQuery.isNumeric(quantity)) {
            sum += Number(price)*Number(quantity);
        }
    }
    jQuery(totalCostSpan).text("Total cost:" + sum);
}
