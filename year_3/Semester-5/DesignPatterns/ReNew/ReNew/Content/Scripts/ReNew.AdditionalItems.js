﻿if (ReNew.AdditionalItems == undefined) {
    ReNew.AdditionalItems = {};
}
ReNew.AdditionalItems = function () {
    var self = this;
    self.init();
    self.bind();
};
ReNew.AdditionalItems.prototype.init = function () {
}
ReNew.AdditionalItems.prototype.bind = function () {

    var self = this;
    var removeRow = function () {
        jQuery(this).parent().parent().remove();
    };
    jQuery('.removeRow').off('click').on('click', removeRow);
    jQuery('#AddItem').off('click').on('click', function () {
        var itemId = jQuery('#Available_Products').val();
        var itemName = jQuery('#Available_Products option:selected').text();
        var itemQuantity = jQuery('#ItemQuantity').val();
        var itemDescription = self.getItemDescriptionById(itemId);
        var closeTd = "</td>";
        var tableRow = "<tr class='table-row' itemId='" + itemId + "'>" +
            "<td class='itemName'>" + itemName + closeTd +
            "<td class='itemDescription'>" + itemDescription + closeTd +
            "<td class='itemQuantity'>" + itemQuantity + closeTd +
            "<td><div class='removeRow btn btn-default'>Remove item</div>" + closeTd;
            jQuery('.table').find('tbody').append(tableRow);
            jQuery('.removeRow').off('click').on('click', removeRow);
    });

    jQuery('#SaveItems').off('click').on('click', function () {
        var model = window.additionalItemsToProductViewModel;
        model.AdditionaItemsToProduct = [];
        var items = jQuery('.table-row');
        var item;
        var itemId;
        var productId = model.Product.ID;
        var itemQuantity;
        for (var i = 0; i < items.length; i++) {
            item = items[i];
            itemId = jQuery(item).attr('itemid');
            itemQuantity = jQuery(item).find('.itemQuantity').text();
            model.AdditionaItemsToProduct.push({
                ID: itemId,
                Quantity: itemQuantity
            });

        }

        var jsonModel = JSON.stringify(model);
        //Ajax call
        jQuery.ajax({
            type: "POST",
            url: "",
            data: jsonModel,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log("success");
            },
            error: function (jqXHR) {
                var a = jqXHR;
                console.log("error");
            }
        });

    });
};

ReNew.AdditionalItems.prototype.getItemDescriptionById = function (id) {
    var itemDescription;
    var itemViewModel;
    for (var i = 0; i < window.availableItems.length; i++) {
        itemViewModel = window.availableItems[i];
        if (itemViewModel.ID == id) {
            itemDescription = itemViewModel.Description;
            break;
        }
    }
    return itemDescription;
};
