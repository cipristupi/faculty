﻿if (ReNew.InstallationManagement == undefined) {
    ReNew.InstallationManagement = {};
}

ReNew.InstallationManagement = function () {
    var self = this;
    self.init();
    self.bind();
};
ReNew.InstallationManagement.prototype.init = function () {

}
ReNew.InstallationManagement.prototype.bind = function () {

    var self = this;
    var removeRow = function () {
        jQuery(this).parent().parent().remove();
    };
    jQuery('.removeRow').off('click').on('click', removeRow);
    jQuery('#AddStep').off('click').on('click', function () {
        var stepId = jQuery('#Available_Steps').val();
        var stepName = jQuery('#Available_Steps option:selected').text();
        var stepOrder = jQuery('#StepOrder').val();
        var stepDescription = self.getStepDescriptionById(stepId);
        var closeTd = "</td>";
        var tableRow = "<tr class='table-row' stepId='" + stepId + "'>" +
            "<td class='stepName'>" + stepName + closeTd +
            "<td class='stepDescription'>" + stepDescription + closeTd +
            "<td class='stepOrder'>" + stepOrder + closeTd +
            "<td><div class='removeRow btn btn-default'>Remove Step</div>" + closeTd;
        if (!self.checkIfStepExists(stepOrder)) {
            jQuery('.table').find('tbody').append(tableRow);
            jQuery('.removeRow').off('click').on('click', removeRow);
        } else {
            alert('Step Order must be unique');
        }
    });

    jQuery('#SaveSteps').off('click').on('click', function () {
        var model = window.stepsToProductViewModel;
        model.InstallationStepToProductViewModels = [];
        var steps = jQuery('.table-row');
        var step;
        var stepId;
        var productId = model.ProductId;
        var stepOrder;
        for (var i = 0; i < steps.length; i++) {
            step = steps[i];
            stepId = jQuery(step).attr('stepid');
            stepOrder = jQuery(step).find('.stepOrder').text();
            model.InstallationStepToProductViewModels.push({
                ProductId: productId,
                StepId: stepId,
                StepOrder: stepOrder
            });

        }

        var jsonModel = JSON.stringify(model);

        //Ajax call
        jQuery.ajax({
            type: "POST",
            //url: "InstallationSteps/AddInstallationStepsToProduct",
            url: "",
            data: jsonModel,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                var a = result;
                console.log("success");
            },
            error: function (jqXHR) {
                var a = jqXHR;
            }
        });

    });
};

ReNew.InstallationManagement.prototype.getStepDescriptionById = function (id) {
    var stepDescription;
    var stepViewModel;
    for (var i = 0; i < window.stepsViewModel.length; i++) {
        stepViewModel = window.stepsViewModel[i];
        if (stepViewModel.ID == id) {
            stepDescription = stepViewModel.Description;
            break;
        }
    }
    return stepDescription;
};

ReNew.InstallationManagement.prototype.checkIfStepExists = function (step) {
    var stepsInTable = jQuery('.stepOrder');
    for (var i = 0; i < stepsInTable.length; i++) {
        if (jQuery(stepsInTable[i]).text() == step) {
            return true;
        }
    }
    return false;
};