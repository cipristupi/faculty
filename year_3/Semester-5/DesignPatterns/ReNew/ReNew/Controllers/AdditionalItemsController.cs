﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using ReNew.BusinessLayer;
using ReNew.Helpers;
using ReNew.Models.Business_Models;
using ReNew.Models.View_Models;

namespace ReNew.Controllers
{
    public class AdditionalItemsController : BaseController
    {
        readonly ProductsBl _productsBl = new ProductsBl();
        readonly AdditionalItemsToProductBl _additionalItemsToProductBl = new AdditionalItemsToProductBl();
        public ActionResult AddAdditionalItemsToProduct(int id)
        {
            var model =
                (AdditionalItemsToProductViewModel)ViewModelFactory.GetViewModel(typeof(AdditionalItemsToProductViewModel).Name);
            model.AdditionaItemsToProduct = GetAdditionalItemsForProduct(id);
            model.Product = Mapper.Map<ProductsViewModel>(_productsBl.GetById(id));
            model.AvailableItemsSelectList = GetProductsAsSelectList();
            model.AvailableItems = GetStepsViewModels();

            return View("AdditionalItemsToProduct", model);
        }

        private List<ProductsViewModel> GetAdditionalItemsForProduct(int productId)
        {
            var additionalItemsToProduct = _additionalItemsToProductBl.GetAllAdditionalItemsByProductId(productId);
            var productsViewModels = new List<ProductsViewModel>();
            var model = (ProductsViewModel) ViewModelFactory.GetViewModel(typeof(ProductsViewModel).Name);
            foreach (var additionalItemToProductBusiness in additionalItemsToProduct)
            {
                model =
                    Mapper.Map<ProductsViewModel>(
                        _productsBl.GetById(additionalItemToProductBusiness.AdditionalProductId));
                model.Quantity = additionalItemToProductBusiness.Quantity;
                productsViewModels.Add(model);
            }
            return productsViewModels;
        }

        private List<SelectListItem> GetProductsAsSelectList()
        {
            return _productsBl.GetAll().Select(x => new SelectListItem()
            {
                Value = x.ID.ToString(),
                Text = x.Name
            }).ToList();
        }

        private List<ProductsViewModel> GetStepsViewModels()
        {
            return _productsBl.GetAll().ToList().Select(Mapper.Map<ProductsViewModel>).ToList();
        }

        [HttpPost]
        public ActionResult AddAdditionalItemsToProduct(AdditionalItemsToProductViewModel model)
        {
            List<AdditionalItemsToProductBusiness> additionalItemsToProductBusinessList;
            additionalItemsToProductBusinessList = model.AdditionaItemsToProduct == null ? new List<AdditionalItemsToProductBusiness>() : model.AdditionaItemsToProduct.Select(Mapper.Map<AdditionalItemsToProductBusiness>).ToList();
            _additionalItemsToProductBl.SaveAdditionalItems(additionalItemsToProductBusinessList,model.Product.ID);
            return RedirectToAction("ProductsIndex", "Products");
        }
    }
}