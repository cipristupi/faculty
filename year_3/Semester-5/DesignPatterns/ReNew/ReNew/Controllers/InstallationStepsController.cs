﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using ReNew.BusinessLayer;
using ReNew.Helpers;
using ReNew.Models.Business_Models;
using ReNew.Models.View_Models;

namespace ReNew.Controllers
{
    public class InstallationStepsController : BaseController
    {
        private readonly ProductsBl _productsBl = new ProductsBl();
        private readonly StepsBl _stepsBl = new StepsBl();
        private readonly InstallationStepsToProductBl _installationStepsToProductBl = new InstallationStepsToProductBl();
        public ActionResult AddInstallationStepsToProduct(int id)
        {
            InstallationStepsToProductViewModel model = GetViewModelById(id);
            return View("StepsToProduct", model);
        }

        private InstallationStepsToProductViewModel GetViewModelById(int id)
        {
            var model =
                (InstallationStepsToProductViewModel)ViewModelFactory.GetViewModel(typeof(InstallationStepsToProductViewModel).Name);
            var productsViewModel = Mapper.Map<ProductsViewModel>(_productsBl.GetById(id));
            model.ProductId = productsViewModel.ID;
            model.ProductName = productsViewModel.Name;
            model.AvailableSteps = GetAvailableStepsAsSelectList();
            model.StepsViewModels = GetStepsViewModels();
            model.InstallationStepToProductViewModels = GetInstallationStepsForProduct(productsViewModel.ID);
            return model;
        }

        private List<SelectListItem> GetAvailableStepsAsSelectList()
        {
            return _stepsBl.GetAll().Select(x => new SelectListItem()
            {
                Value = x.ID.ToString(),
                Text = x.Name
            }).ToList();
        }

        private List<StepsViewModel> GetStepsViewModels()
        {
            return _stepsBl.GetAll().ToList().Select(Mapper.Map<StepsViewModel>).ToList();
        }

        private List<InstallationStepToProductViewModel> GetInstallationStepsForProduct(int productId)
        {
            return _installationStepsToProductBl.GetStepsByProductId(productId).Select(Mapper.Map<InstallationStepToProductViewModel>).ToList();
        }


        [HttpPost]
        public ActionResult AddInstallationStepsToProduct(InstallationStepsToProductViewModel model)
        {
            List<InstallationStepBusiness> installationStepBusinessList;
            installationStepBusinessList = model.InstallationStepToProductViewModels == null ?
                new List<InstallationStepBusiness>() :
                model.InstallationStepToProductViewModels.Select(Mapper.Map<InstallationStepBusiness>).ToList();
            _installationStepsToProductBl.SaveInstallationSteps(installationStepBusinessList, model.ProductId);
            return RedirectToAction("ProductsIndex", "Products");
        }
    }
}