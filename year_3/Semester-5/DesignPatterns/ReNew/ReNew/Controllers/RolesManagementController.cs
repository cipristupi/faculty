﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using ReNew.BusinessLayer;
using ReNew.Models.Business_Models;
using ReNew.Models.View_Models;

namespace ReNew.Controllers
{
    public class RolesManagementController : BaseController
    {
        readonly RolesManagementBl _rolesManagementBl = new RolesManagementBl();
        public ActionResult RolesIndex()
        {

            List<RoleViewModel> roles = new List<RoleViewModel>();
            foreach (var item in _rolesManagementBl.GetAll())
            {
                roles.Add(Mapper.Map<RoleViewModel>(item));
            }
            return View("RolesIndex", roles);
        }

        public ActionResult RoleCreate()
        {
            return View("RoleCreate");
        }

        [HttpPost]
        public ActionResult RoleCreate(RoleViewModel model)
        {
            if (ModelState.IsValid)
            {
                _rolesManagementBl.Add(new RoleBusiness()
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = model.Name
                });

                return RedirectToAction("RolesIndex");
            }
            ViewBag.ModelState = ModelState;
            return View("RoleCreate", model);

        }

        public ActionResult RoleEdit(string id)
        {
            var roleViewModel =Mapper.Map<RoleViewModel>( _rolesManagementBl.GetById(id));
            return View(roleViewModel);
        }

        [HttpPost]
        public ActionResult RoleEdit(string id, RoleViewModel model)
        {
            if (ModelState.IsValid)
            {
                var roleBusiness = Mapper.Map<RoleBusiness>(model);
                _rolesManagementBl.Update(roleBusiness);
                ViewBag.Succes = true;
                return View("RoleEdit",model);
            }
            ViewBag.ModelState = ModelState;
            return View(model);
        }

        public ActionResult RoleDelete(string id)
        {
            _rolesManagementBl.Remove(id);
            return RedirectToAction("RolesIndex");
        }
    }
}
