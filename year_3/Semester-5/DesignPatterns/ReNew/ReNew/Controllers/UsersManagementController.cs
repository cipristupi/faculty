﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Microsoft.AspNet.Identity;
using ReNew.DataLayer;
using ReNew.Helpers;
using ReNew.Models;
using ReNew.Models.View_Models;

namespace ReNew.Controllers
{
    public class UsersManagementController : BaseController
    {
        public ActionResult UsersIndex()
        {
            List<UserViewModel> users = new List<UserViewModel>();
            UserViewModel userViewModel;
            foreach (var user in UserManager.Users.ToList())
            {
                userViewModel = Mapper.Map<UserViewModel>(user);
                var role = UserManager.GetRoles(userViewModel.Id);
                userViewModel.Role = role.Count != 0 ? role[0] : "";
                users.Add(userViewModel);
            }
            return View("UsersIndex", users);
        }

        public ActionResult UserCreate()
        {
            var model = (UserViewModel)ViewModelFactory.GetViewModel(typeof (UserViewModel).Name);
            model.Roles = GetRoles();
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> UserCreate(UserViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser
                {
                    UserName = model.Email,
                    Email = model.Email,
                    FirstName = model.FirstName,
                    LastName = model.LastName
                };
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    await UserManager.AddToRoleAsync(user.Id, model.Role);
                    return RedirectToAction("UsersIndex");
                }
            }
            ViewBag.ModelState = ModelState;
            model.Roles = GetRoles();
            return View("UserCreate", model);
        }

        public ActionResult UserEdit(string id)
        {
            ApplicationUser aspNetUser = UserManager.FindById(id);
            UserViewModel userViewModel = Mapper.Map<UserViewModel>(aspNetUser);
            IList<string> roles = UserManager.GetRoles(userViewModel.Id);
            userViewModel.Role = roles.Count != 0 ? roles[0] : "";
            userViewModel.Roles = GetRoles();
            return View("UserEdit", userViewModel);
        }

        [HttpPost]
        public async Task<ActionResult> UserEdit(string id, UserViewModel model)
        {
            if (ModelState.IsValid)
            {
                ApplicationUser applicationUser = Mapper.Map<ApplicationUser>(model);
                IList<string> role = UserManager.GetRoles(applicationUser.Id);
                foreach (string r in role)
                {
                    UserManager.RemoveFromRole(applicationUser.Id, r);
                }
                await UserManager.AddToRoleAsync(applicationUser.Id, model.Role);
                UserManager.Update(applicationUser);
                ViewBag.Succes = true;
                model.Roles = GetRoles();
                return View("UserEdit", model);
            }
            ViewBag.ModelState = ModelState;
            model.Roles = GetRoles();
            return View("UserEdit",model);
        }

        public ActionResult UserDelete(string id)
        {
            ApplicationUser aspNetUser = UserManager.FindById(id);
            UserViewModel userViewModel = Mapper.Map<UserViewModel>(aspNetUser);
            return View("UserDelete", userViewModel);
        }

        public ActionResult Delete(string id, UserViewModel model)
        {
            ApplicationUser applicationUser = UserManager.FindById(id);
            UserManager.Delete(applicationUser);
            return RedirectToAction("UsersIndex");
        }

        public List<string> GetRoles()
        {
            ApplicationDbContext db = new ApplicationDbContext();
            return db.Roles.Select(r => r.Name).ToList();
        }
    }
}
