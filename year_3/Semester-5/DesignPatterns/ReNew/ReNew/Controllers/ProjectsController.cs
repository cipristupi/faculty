﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using ReNew.BusinessLayer;
using ReNew.DataLayer;
using ReNew.Helpers;
using ReNew.Models.Business_Models;
using ReNew.Models.View_Models;

namespace ReNew.Controllers
{
    public class ProjectsController : BaseController
    {
        private ProjectsBl _projectBl = new ProjectsBl();
        private ProductsBl _productsBl = new ProductsBl();

        public ActionResult ProjectsIndex()
        {
            var projects = _projectBl.GetAllByUserId(LoggedUser.Id).Select(Mapper.Map<ProjectViewModel>);
            return View(projects);
        }


        public ActionResult ProjectsCreate()
        {
            ProjectViewModel model = (ProjectViewModel) ViewModelFactory.GetViewModel(typeof (ProjectViewModel).Name);
            model.AllProducts = GetAsSelectListItems(_productsBl.GetAll().Select(Mapper.Map<ProductsViewModel>));
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ProjectsCreate(ProjectViewModel projects)
        {
            if (ModelState.IsValid)
            {
                projects.UserId = LoggedUser.Id;
                if (projects.ProductsQuantityToProject != null)
                {
                    projects.ProductsToProject = GetProductsFromProject(projects.ProductsQuantityToProject);
                }
                else
                {
                    projects.ProductsToProject = new List<ProductsViewModel>();
                }
                _projectBl.Add(Mapper.Map<ProjectsBusiness>(projects));
                projects.AllProducts = GetAsSelectListItems(_productsBl.GetAllWithAdditionalProducts().Select(Mapper.Map<ProductsViewModel>));
                return RedirectToAction("ProjectsIndex");
            }
            projects.AllProducts = GetAsSelectListItems(_productsBl.GetAll().Select(Mapper.Map<ProductsViewModel>));
            ViewBag.ModelState = ModelState;
            projects.ProductsToProject=new List<ProductsViewModel>();
            return View(projects);
        }

        public ActionResult ProjectsEdit(int id)
        {
            var projectBusiness = _projectBl.GetById(id);
            var projects = Mapper.Map<ProjectViewModel>(projectBusiness);
            projects.AllProducts = GetAsSelectListItems(_productsBl.GetAll().Select(Mapper.Map<ProductsViewModel>));
            return View(projects);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ProjectsEdit(ProjectViewModel projects)
        {
            if (ModelState.IsValid)
            {
                projects.UserId = LoggedUser.Id;
                if (projects.ProductsQuantityToProject != null)
                {
                    projects.ProductsToProject = GetProductsFromProject(projects.ProductsQuantityToProject);
                }
                else
                {
                    projects.ProductsToProject =new List<ProductsViewModel>();
                }
                _projectBl.Update(Mapper.Map<ProjectsBusiness>(projects));
                projects.AllProducts = GetAsSelectListItems(_productsBl.GetAllWithAdditionalProducts().Select(Mapper.Map<ProductsViewModel>));
                ViewBag.Succes = true;
                return View(projects);
            }
            projects.AllProducts = GetAsSelectListItems(_productsBl.GetAll().Select(Mapper.Map<ProductsViewModel>));
            ViewBag.ModelState = ModelState;
            return View(projects);
        }


        public ActionResult ProjectsDelete(int id)
        {
            _projectBl.Remove(id);
            return RedirectToAction("ProjectsIndex");
        }
        public ActionResult GetProductById(int id,int quantity)
        {
            ProductsViewModel model = Mapper.Map<ProductsViewModel>(_productsBl.GetById(id));
            model.Quantity = quantity;
            ViewBag.ParentID = "accordion";
            return PartialView("_ProductPartial", model);
        }
        public JsonResult GetProductDetails(int id)
        {
            var product = _productsBl.GetById(id);
            return Json(product);
        }

        private List<SelectListItem> GetAsSelectListItems(IEnumerable<ProductsViewModel> list)
        {
            List<SelectListItem> x = new List<SelectListItem>();
            foreach (var productsViewModel in list)
            {
                x.Add(new SelectListItem()
                {
                    Value = productsViewModel.ID.ToString(),
                    Text = productsViewModel.Name
                });
            }
            return x;
        }


        private List<ProductsViewModel> GetProductsFromProject(string productsString)
        {
            var products = productsString.Split(',');
            int productId;
            int productQuantity;
            List<ProductsViewModel> productsViewModels = new List<ProductsViewModel>();
            foreach (string product in products)
            {
                string productIdString = product.Split('-')[0];
                string productQuantityString = product.Split('-')[1];
                if (int.TryParse(productIdString, out productId) &&
                    int.TryParse(productQuantityString, out productQuantity))
                {
                    productsViewModels.Add(new ProductsViewModel()
                    {
                        ID = productId,
                        Quantity = productQuantity
                    });   
                }
            }
            return productsViewModels;
        }
    }
}
