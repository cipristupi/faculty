﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using ReNew.BusinessLayer;
using ReNew.Helpers;
using ReNew.Models.Business_Models;
using ReNew.Models.View_Models;

namespace ReNew.Controllers
{
    public class ProductsController : BaseController
    {
        private readonly ProductsBl _productsBl = new ProductsBl();
        private readonly UnitOfMeasuresBl _unitOfMeasuresBl = new UnitOfMeasuresBl();
        // GET: Products
        public ActionResult ProductsIndex()
        {
            List<ProductsViewModel> products = _productsBl.GetAll().Select(Mapper.Map<ProductsViewModel>).ToList();
            return View("ProductsIndex", products);
        }

        // GET: Products/Create
        public ActionResult ProductsCreate()
        {
            ProductsViewModel model = (ProductsViewModel) ViewModelFactory.GetViewModel(typeof (ProductsViewModel).Name);
            model.UnitOfMeasureSelectListItems = GetAsSelectListItems(GetUnitOfMeasure());
            return View(model);
        }

        // POST: Products/Create
        [HttpPost]
        public ActionResult ProductsCreate(ProductsViewModel model)
        {
            if (ModelState.IsValid)
            {
                _productsBl.Add(Mapper.Map<ProductsBusiness>(model));
                return RedirectToAction("ProductsIndex");
            }
            ViewBag.ModelState = ModelState;
            model.UnitOfMeasureSelectListItems = GetAsSelectListItems(GetUnitOfMeasure());
            return View(model);
        }

        public ActionResult ProductsEdit(int id)
        {
            var productsViewModel = Mapper.Map<ProductsViewModel>(_productsBl.GetProductById(id));
            productsViewModel.UnitOfMeasureSelectListItems = GetAsSelectListItems(GetUnitOfMeasure());
            return View("ProductsEdit", productsViewModel);
        }

        [HttpPost]
        public ActionResult ProductsEdit(int id, ProductsViewModel model)
        {
            if (ModelState.IsValid)
            {
                var unitOfMeasureBusiness = Mapper.Map<ProductsBusiness>(model);
                _productsBl.Update(unitOfMeasureBusiness);
                ViewBag.Succes = true;
                model.UnitOfMeasureSelectListItems = GetAsSelectListItems(GetUnitOfMeasure());
                return View("ProductsEdit", model);
            }
            ViewBag.ModelState = ModelState;
            model.UnitOfMeasureSelectListItems = GetAsSelectListItems(GetUnitOfMeasure());
            return View("ProductsEdit", model);
        }

        
        public ActionResult ProductsDelete(int id)
        {
            _productsBl.Remove(id);
            return RedirectToAction("ProductsIndex");
        }

        private List<UnitOfMeasuresViewModel> GetUnitOfMeasure()
        {
            return _unitOfMeasuresBl.GetAll().Select(Mapper.Map<UnitOfMeasuresViewModel>).ToList();
        }

        private List<SelectListItem> GetAsSelectListItems(List<UnitOfMeasuresViewModel> list)
        {
            return list.Select(unitOfMeasuresViewModel => new SelectListItem()
            {
                Value = unitOfMeasuresViewModel.ID.ToString(), Text = unitOfMeasuresViewModel.Name
            }).ToList();
        }
    }
}
