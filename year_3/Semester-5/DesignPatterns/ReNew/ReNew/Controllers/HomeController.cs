﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace ReNew.Controllers
{
    [Authorize]
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            var userRoles = UserManager.GetRolesAsync(user.Id).Result.ToList();
            if (userRoles.Contains("Admin"))
            {
                RedirectToAction("UsersIndex", "UsersManagement");
            }
            if (userRoles.Contains("User"))
            {
                RedirectToAction("ProjectsIndex", "Projects");
            }
            return View();
        }
    }
}