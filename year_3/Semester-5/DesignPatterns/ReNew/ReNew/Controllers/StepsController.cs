﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using ReNew.BusinessLayer;
using ReNew.DataLayer;
using ReNew.Helpers;
using ReNew.Models.Business_Models;
using ReNew.Models.View_Models;

namespace ReNew.Controllers
{
    public class StepsController : BaseController
    {
        private readonly StepsBl  _stepsBL = new StepsBl();
        public ActionResult StepsIndex()
        {
            var steps = _stepsBL.GetAll().ToList().Select(Mapper.Map<StepsViewModel>).ToList();
            return View(steps);
        }

        public ActionResult StepsCreate()
        {
            var step = (StepsViewModel)ViewModelFactory.GetViewModel(typeof (StepsViewModel).Name);
            return View(step);
        }

        
        [HttpPost]
        public ActionResult StepsCreate([Bind(Include = "ID,Name,Description,StepOrder")] StepsViewModel model)
        {
            if (ModelState.IsValid)
            {
                _stepsBL.Add(Mapper.Map<StepsBusiness>(model));
                return RedirectToAction("StepsIndex");
            }
            ViewBag.ModelState = ModelState;
            return View(model);
        }

        public ActionResult StepsEdit(int id)
        {
            var stepsViewModel = Mapper.Map<StepsViewModel>(_stepsBL.GetById(id));
            return View("StepsEdit", stepsViewModel);
        }

        
        [HttpPost]
        public ActionResult StepsEdit([Bind(Include = "ID,Name,Description,StepOrder")] StepsViewModel model)
        {
            if (ModelState.IsValid)
            {
                var stepsBusiness = Mapper.Map<StepsBusiness>(model);
                _stepsBL.Update(stepsBusiness);
                ViewBag.Succes = true;
                return View("StepsEdit",model);
            }
            ViewBag.ModelState = ModelState;
            return View("StepsEdit", model);
        }
    
        public ActionResult StepsDelete(int id)
        {
            if (_stepsBL.IsInstallationStep(id))
            {
                _stepsBL.Remove(id);
                return RedirectToAction("StepsIndex");
            }
            ModelState.AddModelError("Error","Step used in installation steps. Can not be deleted");
            ViewBag.ModelState = ModelState;
            var steps = _stepsBL.GetAll().ToList().Select(Mapper.Map<StepsViewModel>).ToList();
            return View("StepsIndex",steps);
        }
    }
}
