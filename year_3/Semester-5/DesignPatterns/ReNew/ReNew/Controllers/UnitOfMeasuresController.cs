﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using ReNew.BusinessLayer;
using ReNew.Helpers;
using ReNew.Models.Business_Models;
using ReNew.Models.View_Models;

namespace ReNew.Controllers
{
    public class UnitOfMeasuresController : BaseController
    {
        private readonly UnitOfMeasuresBl _unitOfMeasuresBl = new UnitOfMeasuresBl();
        public ActionResult UnitOfMeasuresIndex()
        {
            var unitOfMeasures = GetUnitsOfMeasure();
            return View("UnitOfMeasuresIndex", unitOfMeasures);
        }

        private List<UnitOfMeasuresViewModel> GetUnitsOfMeasure()
        {
            return _unitOfMeasuresBl.GetAll().ToList().Select(Mapper.Map<UnitOfMeasuresViewModel>).ToList();
        }

        public ActionResult UnitOfMeasuresCreate()
        {
            var model = (UnitOfMeasuresViewModel)ViewModelFactory.GetViewModel(typeof(UnitOfMeasuresViewModel).Name);
            return View(model);
        }

        [HttpPost]
        public ActionResult UnitOfMeasuresCreate(UnitOfMeasuresViewModel model)
        {
            if (ModelState.IsValid)
            {
                _unitOfMeasuresBl.Add(Mapper.Map<UnitOfMeasuresBusiness>(model));
                return RedirectToAction("UnitOfMeasuresIndex");
            }
            ViewBag.ModelState = ModelState;
            return View(model);
        }

        public ActionResult UnitOfMeasuresEdit(int id)
        {
            var unitOfMeasureViewModel = Mapper.Map<UnitOfMeasuresViewModel>(_unitOfMeasuresBl.GetById(id));
            return View("UnitOfMeasuresEdit", unitOfMeasureViewModel);
        }

        [HttpPost]
        public ActionResult UnitOfMeasuresEdit(int id, UnitOfMeasuresViewModel model)
        {
            if (ModelState.IsValid)
            {
                var unitOfMeasureBusiness = Mapper.Map<UnitOfMeasuresBusiness>(model);
                _unitOfMeasuresBl.Update(unitOfMeasureBusiness);
                ViewBag.Succes = true;
                return View("UnitOfMeasuresEdit", model);
            }
            ViewBag.ModelState = ModelState;
            return View("UnitOfMeasuresEdit", model);
        }

        public ActionResult UnitOfMeasuresDelete(int id)
        {
            if (_unitOfMeasuresBl.IsNotAssignedToProduct(id))
            {
                _unitOfMeasuresBl.Remove(id);
                return RedirectToAction("UnitOfMeasuresIndex");
            }
            ModelState.AddModelError("Error", "Unit of measure assigned to project. Can not be deleted");
            ViewBag.ModelState = ModelState;
            return View("UnitOfMeasuresIndex", GetUnitsOfMeasure());
        }
    }
}