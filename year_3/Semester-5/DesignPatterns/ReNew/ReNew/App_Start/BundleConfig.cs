﻿using System.Web;
using System.Web.Optimization;

namespace ReNew
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Content/Scripts/Shared/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Content/Scripts/Shared/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Content/Scripts/Shared/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Content/Scripts/Shared/bootstrap.js",
                      "~/Content/Scripts/Shared/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/Styles/Shared/bootstrap.css"
                      ));

            bundles.Add(new StyleBundle("~/LayoutStyles").Include(
                "~/Content/Styles/ReNew.Layout.css",
                "~/Content/Theme/bower_components/metisMenu/dist/metisMenu.min.css",
                "~/Content/Theme/bower_components/font-awesome/css/font-awesome.min.css",
                "~/Content/Theme/dist/css/timeline.css",
                "~/Content/Theme/bower_components/morrisjs/morris.css"

                ));

            bundles.Add(new ScriptBundle("~/LayoutScripts").Include(
                "~/Content/Theme/bower_components/metisMenu/dist/metisMenu.min.js",
                "~/Content/Theme/dist/js/sb-admin-2.js",
                "~/Content/Theme/bower_components/morrisjs/morris.min.js",
                "~/Content/Theme/bower_components/raphael/raphael-min.js"
                ));

            bundles.Add(new ScriptBundle("~/AccountStyles").Include(
                "~/Content/Styles/ReNew.Account.css"
                ));

            bundles.Add(new ScriptBundle("~/InstallationManagement").Include(
                "~/Content/Scripts/ReNew.InstallationManagement.js"
                ));

            bundles.Add(new ScriptBundle("~/AdditionalItems").Include(
                "~/Content/Scripts/ReNew.AdditionalItems.js"
                ));

            bundles.Add(new ScriptBundle("~/Projects").Include(
               "~/Content/Scripts/ReNew.Projects.js"
               ));
        }
    }
}
