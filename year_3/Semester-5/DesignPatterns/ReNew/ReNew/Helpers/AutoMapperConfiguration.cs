﻿
using System.Web.Services.Discovery;
using AutoMapper;
using ReNew.DataLayer;
using ReNew.Models;
using ReNew.Models.Business_Models;
using ReNew.Models.View_Models;
namespace ReNew.Helpers
{
    public class AutoMapperConfiguration
    {
        public static void Initialize()
        {
            //User
            Mapper.CreateMap<UserViewModel, ApplicationUser>().ReverseMap();
            Mapper.CreateMap<ApplicationUser, UserViewModel>().ReverseMap();
            //Role
            Mapper.CreateMap<RoleViewModel, RoleBusiness>().ReverseMap();
            Mapper.CreateMap<RoleBusiness, AspNetRoles>().ReverseMap();
            //Unit of Measures
            Mapper.CreateMap<UnitOfMeasures, UnitOfMeasuresBusiness>().ReverseMap();
            Mapper.CreateMap<UnitOfMeasuresBusiness, UnitOfMeasuresViewModel>().ReverseMap();

            //Steps
            Mapper.CreateMap<Steps, StepsBusiness>().ReverseMap();
            Mapper.CreateMap<StepsBusiness, StepsViewModel>().ReverseMap();

            //Products
            Mapper.CreateMap<Products, ProductsBusiness>();
            Mapper.CreateMap<ProductsBusiness,Products>();
            Mapper.CreateMap<ProductsBusiness,ProductsViewModel>().ReverseMap();


            //Installation Steps
            Mapper.CreateMap<InstallationSteps, InstallationStepBusiness>().ReverseMap();
            Mapper.CreateMap<InstallationStepBusiness, InstallationStepToProductViewModel>().ReverseMap();
            Mapper.CreateMap<InstallationSteps, StepsBusiness>().ReverseMap();

            //Additional Items To Product
            Mapper.CreateMap<AdditionalItems, AdditionalItemsToProductBusiness>().ReverseMap();
            Mapper.CreateMap<AdditionalItemsToProductBusiness, AdditionalItemsToProductViewModel>().ReverseMap();
            Mapper.CreateMap<ProductsViewModel, AdditionalItemsToProductBusiness>().ReverseMap();
            //Product To Project

            Mapper.CreateMap<ProjectViewModel, ProjectsBusiness>().ReverseMap();
            Mapper.CreateMap<ProjectsBusiness, Projects>().ReverseMap();
            //ProjectsBusiness

            Mapper.CreateMap<ProductsToProjectsBusiness, ProductsToProjects>().ReverseMap();
            Mapper.CreateMap<ProductsToProjectsBusiness, ProductsToProjectsViewModel>().ReverseMap();
        }
    }
}
