﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReNew.Models;
using ReNew.Models.View_Models;

namespace ReNew.Helpers
{
    public static class ViewModelFactory
    {
        public static BaseViewModel GetViewModel(string type)
        {
            switch (type)
            {
                case "RoleViewModel":
                    return new RoleViewModel();
                case "UserViewModel":
                    return new UserViewModel()
                    {
                        Roles = GetRoles()
                    };
                case "ProductsViewModel":
                    return new ProductsViewModel();
                case "StepsViewModel":
                    return new StepsViewModel();
                case "UnitOfMeasuresViewModel":
                    return new UnitOfMeasuresViewModel();
                case "InstallationStepsToProductViewModel":
                    return new InstallationStepsToProductViewModel();
                case "InstallationStepToProductViewModel":
                    return new InstallationStepToProductViewModel();
                case "AdditionalItemsToProductViewModel":
                    return new AdditionalItemsToProductViewModel();
                case "ProjectViewModel":
                    return new ProjectViewModel()
                    {
                        ProductsToProject = new List<ProductsViewModel>(),
                    };
            }
            return null;
        }

        private static List<string> GetRoles()
        {
            ApplicationDbContext db = new ApplicationDbContext();
            return db.Roles.Select(r => r.Name).ToList();
        }
    }
}