﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ReNew.Startup))]
namespace ReNew
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
