﻿using System;
using EDISystem.Models;
using EDISystem.Models.States;

namespace EDISystem.Observer
{
    class ShieldObserver : IObserver
    {
        readonly MainForm _mainForm;

        public ShieldObserver(MainForm form)
        {
            _mainForm = form;
        }
        public void NewStateNotify(object sender, EventArgs e)
        {
            string shieldName =string.Empty;
            State state = (State)sender;
            if (state.System.Article != ArticleType.Energy) return;
            if (state.System is Shield)
            {
                var shield = state.System as Shield;
                shieldName = shield.ShieldName;
            }
            var buttonName = $"{shieldName}Bttn";
            _mainForm.UpdateButtonColor(buttonName, state.StateName);
        }
    }
}
