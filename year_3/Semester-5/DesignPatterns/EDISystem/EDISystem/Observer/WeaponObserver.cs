﻿using System;
using EDISystem.Models;
using EDISystem.Models.States;

namespace EDISystem.Observer
{
    class WeaponObserver : IObserver
    {
        readonly MainForm _mainForm;

        public WeaponObserver(MainForm form)
        {
            _mainForm = form;
        }
        public void NewStateNotify(object sender, EventArgs e)
        {
            string weaponName = string.Empty;
            State state = (State)sender;
            if (state.System.Article != ArticleType.Heat) return;
            if (state.System is Weapon)
            {
                var shield = state.System as Weapon;
                weaponName = shield.WeaponName;
            }
            string progressBarName = $"{weaponName}ProgressBar";
            _mainForm.UpdateProgressBar(progressBarName, state.Level, state.StateName);
        }
    }
}
