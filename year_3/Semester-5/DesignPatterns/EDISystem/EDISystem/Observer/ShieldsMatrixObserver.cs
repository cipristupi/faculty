﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EDISystem.Models;
using EDISystem.Models.States;

namespace EDISystem.Observer
{
    class ShieldsMatrixObserver : IObserver
    {
        readonly MainForm _mainForm;

        public ShieldsMatrixObserver(MainForm form)
        {
            _mainForm = form;
        }
        public void NewStateNotify(object sender, EventArgs e)
        {
            State state = (State)sender;
            if (state.System.Article != ArticleType.Energy) return;
            string output = $"\n {state.System.Name} - State: {state.StateName} - {state.Level.ToString("N2")}%";
            if (state.StateName == StateName.Red)
                output += $"\n-------BOOST SHIELD {state.System.Name} !!!";
            _mainForm.WriteOnBoardConsole(output, state.StateName);
        }
    }
}
