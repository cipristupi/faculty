﻿using System;
using EDISystem.Models;
using EDISystem.Models.States;

namespace EDISystem.Observer
{
    class LifeSupportObserver : IObserver
    {
        private MainForm _mainForm;
        public LifeSupportObserver(MainForm form)
        {
            _mainForm = form;
        }
        public void NewStateNotify(object sender, EventArgs e)
        {
            State state = (State)sender;
            if (state.System.Article != ArticleType.Oxygen) return;
            string output = $"\n Oxygen Level - State: {state.StateName} - {state.Level.ToString("N2")}%";
            if (state.StateName == StateName.Red)
            {
                output += "\n EVACUATE SHIP!!! EVACUATE SHIP!!!";
            }
            _mainForm.WriteOnBoardConsole(output, state.StateName);
            _mainForm.UpdateProgressBar("OxygenProgressBar", state.Level, state.StateName);
        }
    }
}
