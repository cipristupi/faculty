﻿using System;

namespace EDISystem.Observer
{
    interface IObserver
    {
        void NewStateNotify(object sender, EventArgs e);
    }
}
