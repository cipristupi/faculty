﻿using System;
using EDISystem.Models;
using EDISystem.Models.States;

namespace EDISystem.Observer
{
    class WeaponsObserver : IObserver
    {
        readonly MainForm _mainForm;
        public WeaponsObserver(MainForm form)
        {
            _mainForm = form;
        }
        public void NewStateNotify(object sender, EventArgs e)
        {
            State state = (State)sender;
            if (state.System.Article != ArticleType.Heat) return;
            string output = $"\n {state.System.Name} - State: {state.StateName} - {state.Level.ToString("N2")}%";
            if (state.StateName == StateName.Red)
                output += $"\n-------REPAIR WEAPON {state.System.Name} !!!";
            _mainForm.WriteOnBoardConsole(output, state.StateName);
        }
    }
}
