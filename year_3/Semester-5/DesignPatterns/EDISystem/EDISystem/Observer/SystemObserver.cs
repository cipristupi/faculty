﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EDISystem.Models;
using EDISystem.Models.States;

namespace EDISystem.Observer
{
    class SystemObserver :IObserver
    {
        private readonly MainForm _mainForm;
        public SystemObserver(MainForm form)
        {
            _mainForm = form;
        }
        public void NewStateNotify(object sender, EventArgs e)
        {
            State state = (State)sender;
            if (state.System.Article != ArticleType.Power) return;
            string output = $"\n System Engine Level - State: {state.StateName} - {state.Level.ToString("N2")}%";
            _mainForm.WriteOnBoardConsole(output, state.StateName);
        }
    }
}
