﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using EDISystem.Models;
using EDISystem.Models.States;

namespace EDISystem
{
    class Presenter : EventArgs
    {
        readonly MainForm _view;
        readonly Stopwatch _stopwatch;
        TimeSpan _interval;
        private ShipSystem _systemEngine;
        public BackgroundWorker ConsumeWorker;
        public BackgroundWorker RepairWorker;
        public BackgroundWorker TakeoffWorker;
        public BackgroundWorker TakeonWorker;
        public BackgroundWorker BoostWorker;
        public TimeSpan RemainingTime;

        public event EventHandler SystemStateChanged;

        public Presenter(MainForm form)
        {
            _view = form;
            _stopwatch = new Stopwatch();
            Init_Time();
            Init_BackgroundWorker();
            CreateBaseModels();
        }

        private void Init_Time()
        {
            _interval = TimeSpan.FromSeconds(60);
            RemainingTime = new TimeSpan(0, 0, 0, 60);
        }

        private void Init_BackgroundWorker()
        {
            ConsumeWorker = new BackgroundWorker();
            RepairWorker = new BackgroundWorker();
            TakeoffWorker = new BackgroundWorker();
            TakeonWorker = new BackgroundWorker();
            BoostWorker = new BackgroundWorker();

            ConsumeWorker.DoWork += Consume_DoWork;
            ConsumeWorker.ProgressChanged += Worker_ProgressChanged;
            ConsumeWorker.RunWorkerCompleted += Worker_RunWorkerCompleted;
            ConsumeWorker.WorkerReportsProgress = true;
            ConsumeWorker.WorkerSupportsCancellation = true;

            RepairWorker.DoWork += Repair_DoWork;
            RepairWorker.ProgressChanged += Worker_ProgressChanged;
            RepairWorker.WorkerReportsProgress = true;
            RepairWorker.WorkerSupportsCancellation = true;

            TakeoffWorker.DoWork += TakeOff_DoWork;
            TakeoffWorker.ProgressChanged += Worker_ProgressChanged;
            TakeoffWorker.WorkerReportsProgress = true;
            TakeoffWorker.WorkerSupportsCancellation = true;

            TakeonWorker.DoWork += TakeOn_DoWork;
            TakeonWorker.ProgressChanged += Worker_ProgressChanged;
            TakeonWorker.WorkerReportsProgress = true;
            TakeonWorker.WorkerSupportsCancellation = true;

            BoostWorker.DoWork += Boost_DoWork;
            BoostWorker.ProgressChanged += Worker_ProgressChanged;
            BoostWorker.WorkerReportsProgress = true;
            BoostWorker.WorkerSupportsCancellation = true;
        }

        private void Boost_DoWork(object sender, DoWorkEventArgs e)
        {
            var shields = e.Argument as List<Shield>;
            if (shields != null)
            {
                Parallel.ForEach(shields, currentShield => { currentShield.State.Fullfill(55); });
            }
            BoostWorker.CancelAsync();
            e.Cancel = true;
            BoostWorker.ReportProgress(90);
        }

        private void TakeOn_DoWork(object sender, DoWorkEventArgs e)
        {
            var weapons = e.Argument as List<Weapon>;
            if (weapons != null)
            {
                Parallel.ForEach(weapons, currentWeapon =>
                {
                    currentWeapon.State.Enable();
                    string name = "W" + currentWeapon.Name;
                    _view.OnOffButton(false, "TakeOff" + name + "Bttn");
                });
            }
            TakeonWorker.CancelAsync();
            e.Cancel = true;
            TakeonWorker.ReportProgress(90);
        }

        private void TakeOff_DoWork(object sender, DoWorkEventArgs e)
        {
            List<Weapon> weapons = e.Argument as List<Weapon>;
            if (weapons != null)
            {
                Parallel.ForEach(weapons, currentWeapon =>
                {
                    currentWeapon.State.Disable();
                    string name = currentWeapon.WeaponName;
                    _view.OnOffButton(true, "TakeOff" + name + "Bttn");
                });
            }
            TakeoffWorker.CancelAsync();
            e.Cancel = true;
            TakeoffWorker.ReportProgress(90);
        }

        private void Repair_DoWork(object sender, DoWorkEventArgs e)
        {
            Weapon weapon = (Weapon)e.Argument;
            weapon.State.Fullfill(25);
            RepairWorker.CancelAsync();
            e.Cancel = true;
            RepairWorker.ReportProgress(0);
        }

        private void Worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                MessageBox.Show("BOOM !");
            }
            else if (e.Error != null)
            {
            }

            else
            {
                MessageBox.Show("Congrats");
            }
        }

        private void Worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            DisplayState();
            _view.UpdateTimeLabel(RemainingTime);
        }

        private void Consume_DoWork(object sender, DoWorkEventArgs e)
        {
            do
            {
                if (ConsumeWorker.CancellationPending)
                {
                    e.Cancel = true;
                    break;
                }
                RunEngine(e);
            } while (RemainingTime > TimeSpan.Zero);
        }

        internal void Start_Backgroundworker()
        {
            ConsumeWorker.RunWorkerAsync();
        }

        private void SomethingChanged(State state)
        {
            if (SystemStateChanged != null)
            {
                SystemStateChanged(state, Empty);
            }
        }

        internal void BoostAllShields()
        {
            WeaponSystem weaponSystem = _systemEngine.SubSystems.FirstOrDefault(s => s.Name == SystemName.Weapons) as WeaponSystem;
            ShieldMatrix shieldMatrix = _systemEngine.SubSystems.FirstOrDefault(s => s.Name == SystemName.Shield) as ShieldMatrix;

            if (weaponSystem != null) TakeoffWorker.RunWorkerAsync(weaponSystem.Weapons);
            if (shieldMatrix != null) BoostWorker.RunWorkerAsync(shieldMatrix.Shields);
            if (weaponSystem != null) TakeonWorker.RunWorkerAsync(weaponSystem.Weapons);
        }

        private void CreateBaseModels()
        {
            _systemEngine = new ShipSystem();
            var lifeSupportSystem = InitializeLifeSupportSystem();
            var shieldMatrix = InitializeShieldMatrixSystem();
            var weaponToolbar = InitializeWeaponSystem();

            for (int i = 1; i <= 3; i++)
            {
                var name = "W" + i;
                weaponToolbar.Add(new Weapon(name, ArticleType.Heat, _systemEngine));
            }

            for (int i = 1; i <= 9; i++)
            {
                var name = "Shield" + i;
                shieldMatrix.Add(new Shield(name, ArticleType.Energy, _systemEngine));
            }
            _systemEngine.Add(lifeSupportSystem);
            _systemEngine.Add(shieldMatrix);
            _systemEngine.Add(weaponToolbar);
        }

        private BaseModel InitializeWeaponSystem()
        {
            BaseModel weaponToolbar = new WeaponSystem
            {
                Article = ArticleType.Heat,
                Engine = _systemEngine
            };
            weaponToolbar.State = new GreenState()
            {
                Level = 100,
                MainSystem = _systemEngine,
                StateName = StateName.Green,
                System = weaponToolbar
            };
            return weaponToolbar;
        }

        private BaseModel InitializeShieldMatrixSystem()
        {
            BaseModel shieldMatrix = new ShieldMatrix
            {
                Article = ArticleType.Energy,
                Engine = _systemEngine
            };
            shieldMatrix.State = new GreenState()
            {
                Level = 100,
                MainSystem = _systemEngine,
                StateName = StateName.Green,
                System = shieldMatrix
            };
            return shieldMatrix;
        }

        private LifeSupportSystem InitializeLifeSupportSystem()
        {
            var lifeSupportSystem = new LifeSupportSystem
            {
                Article = ArticleType.Oxygen,
                ConsumptionRate = 1,
                Engine = _systemEngine

            };
            lifeSupportSystem.State = new GreenState
            {
                Level = 100,
                MainSystem = _systemEngine,
                StateName = StateName.Green,
                System = lifeSupportSystem
            };
            return lifeSupportSystem;
        }

        public void RunEngine(DoWorkEventArgs e)
        {
            if (ConsumeWorker.CancellationPending)
            {
                e.Cancel = true;
            }
            double oxygenLevel = _systemEngine.SubSystems.First(s => s.Article == ArticleType.Oxygen).State.Level;
            if (oxygenLevel > 0)
            {
                _stopwatch.Restart();
                ConsumeEngine();
                _stopwatch.Stop();
                RemainingTime = RemainingTime.Subtract(_stopwatch.Elapsed);
            }
            else
            {
                ConsumeWorker.CancelAsync();
            }
        }

        private void ConsumeEngine()
        {
            Parallel.ForEach(_systemEngine.SubSystems, currentSystem =>
            {
                Thread.Sleep(100);
                if (currentSystem is ShieldMatrix)
                {
                    ConsumeShields(currentSystem);
                }
                else if (currentSystem is WeaponSystem)
                {
                    ConsumeWeapons(currentSystem);
                }
                else {
                    currentSystem.State.Consume();
                }
            });
            ConsumeWorker.ReportProgress(90);
        }

        internal void RepairWeapon(string name)
        {
            string weaponName = name;
            WeaponSystem toolbar = _systemEngine.SubSystems.FirstOrDefault(s => s.Name == SystemName.Weapons) as WeaponSystem;
            if (toolbar != null)
            {
                Weapon weapon = toolbar.Weapons.FirstOrDefault(w => w.WeaponName == weaponName);
                if (weapon != null) weapon.State.Fullfill(25);
            }
            RepairWorker.ReportProgress(0);
        }

        private void ConsumeWeapons(BaseModel currentSystem)
        {
            WeaponSystem weaponToolbar = currentSystem as WeaponSystem;
            if (weaponToolbar != null)
            {
                Parallel.ForEach(weaponToolbar.Weapons, currentWeapon =>
               {
                   if (currentWeapon.State.Level > 0)
                       currentWeapon.State.Consume();
                   else
                   {
                       string name = "W" + currentWeapon.WeaponName[currentWeapon.WeaponName.Length - 1];
                       currentWeapon.State.Disable();
                       _view.OnOffButton(true, "TakeOff" + currentWeapon.WeaponName + "Bttn");
                   }
               });
            }
        }

        private void ConsumeShields(BaseModel currentSystem)
        {
            ShieldMatrix shieldMatrix = currentSystem as ShieldMatrix;
            if (shieldMatrix != null)
            {
                Parallel.ForEach(shieldMatrix.Shields, currentShield =>
                {
                    if (currentShield.State.Level > 0)
                        currentShield.State.Consume();
                    else
                    {
                        string name = "Shield" + currentShield.ShieldName[currentShield.ShieldName.Length - 1] + "Bttn";
                        _view.EnableDisableButton(false, name);
                    }
                });
            }
        }

        public void DisplayState()
        {
            StateName oxygenState = _systemEngine.SubSystems.First(s => s.Article == ArticleType.Oxygen).State.StateName;
            if (oxygenState != StateName.Red)
            {
                for (int i = 0; i < _systemEngine.SubSystems.Count; i++)
                {
                    if (_systemEngine.SubSystems[i] is ShieldMatrix)
                    {
                        ShieldMatrix shieldMatrix = _systemEngine.SubSystems[i] as ShieldMatrix;
                        foreach (Shield shield in shieldMatrix.Shields)
                        {
                            SomethingChanged(shield.State);
                        }
                    }
                    else if (_systemEngine.SubSystems[i] is WeaponSystem)
                    {
                        var toolbar = _systemEngine.SubSystems[i] as WeaponSystem;
                        foreach (var weapon in toolbar.Weapons)
                        {
                            SomethingChanged(weapon.State);
                        }
                    }
                    else
                    {
                        SomethingChanged(_systemEngine.SubSystems[i].State);
                    }
                }
            }
            else
                _view.WriteOnBoardConsole("\n EVACUATE SHIP!!! EVACUATE SHIP!!!", oxygenState);
        }

        public Weapon GetWeaponByName(string weaponName)
        {
            var weaponSystem = _systemEngine.SubSystems.FirstOrDefault(s => s.Name == SystemName.Weapons) as WeaponSystem;
            return weaponSystem?.Weapons.FirstOrDefault(w => w.WeaponName == weaponName);
        }

        internal Shield GetShieldByName(string name)
        {
            var shieldMatrix = _systemEngine.SubSystems.FirstOrDefault(s => s.Name == SystemName.Shield) as ShieldMatrix;
            return shieldMatrix?.Shields.FirstOrDefault(s => s.Name == SystemName.Shield);
        }
    }
}
