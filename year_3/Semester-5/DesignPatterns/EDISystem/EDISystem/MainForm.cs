﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using EDISystem.Models;
using EDISystem.Observer;

namespace EDISystem
{
    public partial class MainForm : Form
    {
        private readonly Presenter _presenter;

        delegate void SetTextCallback(string text, StateName state);
        delegate void SetTimeLabelCallback(TimeSpan time);


        public MainForm()
        {
            InitializeComponent();
            _presenter = new Presenter(this);
            Init_Observers();
        }

        private void Init_Observers()
        {
            IObserver lifeSupportObserver = new LifeSupportObserver(this);
            IObserver shieldMatrixObserver = new ShieldsMatrixObserver(this);
            IObserver weaponToolbarObserver = new WeaponsObserver(this);
            IObserver shieldObserver = new ShieldObserver(this);
            IObserver weaponObserver = new WeaponObserver(this);
            _presenter.SystemStateChanged += lifeSupportObserver.NewStateNotify;
            _presenter.SystemStateChanged += shieldMatrixObserver.NewStateNotify;
            _presenter.SystemStateChanged += weaponToolbarObserver.NewStateNotify;
            _presenter.SystemStateChanged += shieldObserver.NewStateNotify;
            _presenter.SystemStateChanged += weaponObserver.NewStateNotify;
        }

        public void WriteOnBoardConsole(string text, StateName state)
        {
            if (consoleTxtBox.InvokeRequired)
            {
                SetTextCallback d = WriteOnBoardConsole;
                Invoke(d, text, state);
            }
            else
            {
                consoleTxtBox.Text = text + consoleTxtBox.Text;
                switch (state)
                {
                    case StateName.Red:
                        consoleTxtBox.SelectionColor = Color.Red;
                        break;
                    case StateName.Balanced:
                        consoleTxtBox.SelectionColor = Color.Yellow;
                        break;
                    case StateName.Green:
                        consoleTxtBox.SelectionColor = Color.Lime;
                        break;
                    case StateName.Crashed:
                        consoleTxtBox.SelectionColor = Color.CadetBlue;
                        break;
                }
            }
        }

        public void UpdateTimeLabel(TimeSpan time)
        {
            if (consoleTxtBox.InvokeRequired)
            {
                SetTimeLabelCallback d = UpdateTimeLabel;
                Invoke(d, time);
            }
            else
            {
                timeLabel.Text = time.ToString();
            }
        }

        public void UpdateProgressBar(string progressBarName, double level, StateName state)
        {
            ProgressBar bar = null;
            if (level >= 0 && level <= 100)
            {
                bar = GetProgressBarName(progressBarName);
                bar.Value = Convert.ToInt32(level);
            }
            switch (state)
            {
                case StateName.Green:
                    ModifyProgressBarColor.SetState(bar, 1);
                    break;
                case StateName.Red:
                    ModifyProgressBarColor.SetState(bar, 2);
                    break;
                case StateName.Balanced:
                    ModifyProgressBarColor.SetState(bar, 3);
                    break;
                }
        }

        public void UpdateButtonColor(string buttonName, StateName state)
        {
            Button button = Controls.Find(buttonName, true).First() as Button;
            switch (state)
            {
                case StateName.Red:
                    button.BackColor = Color.Red;
                    break;
                case StateName.Balanced:
                    button.BackColor = Color.Gold;
                    break;
                case StateName.Green:
                    button.BackColor = Color.Lime;
                    break;
                case StateName.Crashed:
                    button.BackColor = Color.Black;
                    break;
            }
        }

        private void EvacuateShipBttn_Click(object sender, EventArgs e)
        {
            _presenter.ConsumeWorker.CancelAsync();
            Hide();
            Thread.CurrentThread.Join(10000);
            Application.Exit();
        }

        private void RepairWeaponBttn_Click(object sender, EventArgs e)
        {
            Button button = sender as Button;
            Weapon weapon = _presenter.GetWeaponByName("W" + button?.Name.Substring(7, 1));
            if (weapon.State.StateName == StateName.Disabled)
                _presenter.TakeoffWorker.RunWorkerAsync(new List<Weapon>() { weapon });
            _presenter.RepairWorker.RunWorkerAsync(weapon);
        }

        private void TakeOffWeaponBttn_Click(object sender, EventArgs e)
        {
            Button button = sender as Button;
            if (button == null) return;
            var weapon = _presenter.GetWeaponByName("W" + button.Name.Substring(8, 1));
            if (weapon.State.StateName == StateName.Disabled)
                _presenter.TakeonWorker.RunWorkerAsync(new List<Weapon>() { weapon });
            else
                _presenter.TakeoffWorker.RunWorkerAsync(new List<Weapon>() { weapon });
        }

        private void BoostShieldBttn_Click(object sender, EventArgs e)
        {
            Button button = sender as Button;
            if (button == null) return;
            var shield = _presenter.GetShieldByName("Shield" + button.Name.Substring(6, 1));
            _presenter.BoostWorker.RunWorkerAsync(new List<Shield>() { shield });
        }

        private void BoostShieldsBttn_Click(object sender, EventArgs e)
        {
            _presenter.BoostAllShields();
        }

        public void EnableDisableProgressBar(bool enable, string name)
        {
            SetProgressBar(enable, name);
        }

        private void SetProgressBar(bool enable, string name)
        {
            if (consoleTxtBox.InvokeRequired)
            {
                SetProgressBarCallback d = SetProgressBar;
                Invoke(d, enable, name);
            }
            else
            {
                ProgressBar bar = null;
                bar = GetProgressBarName(name);
                bar.Enabled = enable;
            }
        }

        delegate void SetProgressBarCallback(bool enable, string name);

        private ProgressBar GetProgressBarName(string progressBarName)
        {
            return Controls.Find(progressBarName, true).First() as ProgressBar;
        }

        internal void EnableDisableButton(bool enable, string name)
        {
            SetButton(enable, name);
        }

        private void SetButton(bool enable, string name)
        {
            Button button = Controls.Find(name, true).First() as Button;

            if (button != null && button.InvokeRequired)
            {
                SetButtonCallback d = SetButton;
                Invoke(d, enable, name);
            }
            else
            {
                if (enable)
                {
                    if (button != null)
                    {
                        button.Enabled = true;
                    }
                }
                else
                {
                    if (button != null)
                    {
                        button.Enabled = false;
                        button.BackColor = Color.Gray;
                    }
                }
            }
        }

        delegate void SetButtonCallback(bool enable, string name);

        internal void OnOffButton(bool takeOn, string buttonName)
        {
            SetTextButton(takeOn, buttonName);
        }

        private void SetTextButton(bool takeOn, string buttonName)
        {
            var button = Controls.Find(buttonName, true).First() as Button;

            if (button != null && button.InvokeRequired)
            {
                SetButtonCallback d = SetTextButton;
                Invoke(d, takeOn, buttonName);
            }
            else
            {
                if (button != null)
                {
                    button.Text = takeOn ? "Take On" : "Take Off";
                }
            }
        }

        private void MainForm_Shown(object sender, EventArgs e)
        {
            _presenter.Start_Backgroundworker();
        }
    }

}
