﻿namespace EDISystem
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.RepairW3Bttn = new System.Windows.Forms.Button();
            this.TakeOffW1Bttn = new System.Windows.Forms.Button();
            this.RepairW1Bttn = new System.Windows.Forms.Button();
            this.W2ProgressBar = new System.Windows.Forms.ProgressBar();
            this.BoostShieldsBttn = new System.Windows.Forms.Button();
            this.Shield2Bttn = new System.Windows.Forms.Button();
            this.Shield3Bttn = new System.Windows.Forms.Button();
            this.Shield4Bttn = new System.Windows.Forms.Button();
            this.W1ProgressBar = new System.Windows.Forms.ProgressBar();
            this.label3 = new System.Windows.Forms.Label();
            this.Shield6Bttn = new System.Windows.Forms.Button();
            this.Shield7Bttn = new System.Windows.Forms.Button();
            this.Shield8Bttn = new System.Windows.Forms.Button();
            this.Shield9Bttn = new System.Windows.Forms.Button();
            this.Shield5Bttn = new System.Windows.Forms.Button();
            this.W3ProgressBar = new System.Windows.Forms.ProgressBar();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.OxygenProgressBar = new System.Windows.Forms.ProgressBar();
            this.Shield1Bttn = new System.Windows.Forms.Button();
            this.TakeOffW2Bttn = new System.Windows.Forms.Button();
            this.RepairW2Bttn = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.TakeOffW3Bttn = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.timeLabel = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.EvacuateShipBttn = new System.Windows.Forms.Button();
            this.consoleTxtBox = new System.Windows.Forms.RichTextBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // RepairW3Bttn
            // 
            this.RepairW3Bttn.BackColor = System.Drawing.Color.LimeGreen;
            this.RepairW3Bttn.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RepairW3Bttn.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.RepairW3Bttn.Location = new System.Drawing.Point(249, 110);
            this.RepairW3Bttn.Name = "RepairW3Bttn";
            this.RepairW3Bttn.Size = new System.Drawing.Size(75, 23);
            this.RepairW3Bttn.TabIndex = 8;
            this.RepairW3Bttn.Text = "Repair";
            this.RepairW3Bttn.UseVisualStyleBackColor = false;
            this.RepairW3Bttn.Click += new System.EventHandler(this.RepairWeaponBttn_Click);
            // 
            // TakeOffW1Bttn
            // 
            this.TakeOffW1Bttn.BackColor = System.Drawing.Color.Salmon;
            this.TakeOffW1Bttn.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TakeOffW1Bttn.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.TakeOffW1Bttn.Location = new System.Drawing.Point(330, 31);
            this.TakeOffW1Bttn.Name = "TakeOffW1Bttn";
            this.TakeOffW1Bttn.Size = new System.Drawing.Size(75, 23);
            this.TakeOffW1Bttn.TabIndex = 7;
            this.TakeOffW1Bttn.Text = "Take off";
            this.TakeOffW1Bttn.UseVisualStyleBackColor = false;
            this.TakeOffW1Bttn.Click += new System.EventHandler(this.TakeOffWeaponBttn_Click);
            // 
            // RepairW1Bttn
            // 
            this.RepairW1Bttn.BackColor = System.Drawing.Color.LimeGreen;
            this.RepairW1Bttn.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RepairW1Bttn.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.RepairW1Bttn.Location = new System.Drawing.Point(249, 31);
            this.RepairW1Bttn.Name = "RepairW1Bttn";
            this.RepairW1Bttn.Size = new System.Drawing.Size(75, 23);
            this.RepairW1Bttn.TabIndex = 6;
            this.RepairW1Bttn.Text = "Repair";
            this.RepairW1Bttn.UseVisualStyleBackColor = false;
            this.RepairW1Bttn.Click += new System.EventHandler(this.RepairWeaponBttn_Click);
            // 
            // W2ProgressBar
            // 
            this.W2ProgressBar.Location = new System.Drawing.Point(70, 70);
            this.W2ProgressBar.Name = "W2ProgressBar";
            this.W2ProgressBar.Size = new System.Drawing.Size(173, 23);
            this.W2ProgressBar.TabIndex = 5;
            // 
            // BoostShieldsBttn
            // 
            this.BoostShieldsBttn.BackColor = System.Drawing.Color.Goldenrod;
            this.BoostShieldsBttn.Font = new System.Drawing.Font("Georgia", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BoostShieldsBttn.Location = new System.Drawing.Point(138, 34);
            this.BoostShieldsBttn.Name = "BoostShieldsBttn";
            this.BoostShieldsBttn.Size = new System.Drawing.Size(252, 91);
            this.BoostShieldsBttn.TabIndex = 15;
            this.BoostShieldsBttn.Text = "Boost Shields";
            this.BoostShieldsBttn.UseVisualStyleBackColor = false;
            this.BoostShieldsBttn.Click += new System.EventHandler(this.BoostShieldsBttn_Click);
            // 
            // Shield2Bttn
            // 
            this.Shield2Bttn.BackColor = System.Drawing.Color.Lime;
            this.Shield2Bttn.FlatAppearance.BorderColor = System.Drawing.Color.Lime;
            this.Shield2Bttn.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Shield2Bttn.ForeColor = System.Drawing.Color.DarkMagenta;
            this.Shield2Bttn.Location = new System.Drawing.Point(56, 34);
            this.Shield2Bttn.Name = "Shield2Bttn";
            this.Shield2Bttn.Size = new System.Drawing.Size(28, 26);
            this.Shield2Bttn.TabIndex = 8;
            this.Shield2Bttn.Text = "2";
            this.Shield2Bttn.UseVisualStyleBackColor = false;
            this.Shield2Bttn.Click += new System.EventHandler(this.BoostShieldBttn_Click);
            // 
            // Shield3Bttn
            // 
            this.Shield3Bttn.BackColor = System.Drawing.Color.Lime;
            this.Shield3Bttn.FlatAppearance.BorderColor = System.Drawing.Color.Lime;
            this.Shield3Bttn.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Shield3Bttn.ForeColor = System.Drawing.Color.DarkMagenta;
            this.Shield3Bttn.Location = new System.Drawing.Point(90, 34);
            this.Shield3Bttn.Name = "Shield3Bttn";
            this.Shield3Bttn.Size = new System.Drawing.Size(28, 26);
            this.Shield3Bttn.TabIndex = 7;
            this.Shield3Bttn.Text = "3";
            this.Shield3Bttn.UseVisualStyleBackColor = false;
            this.Shield3Bttn.Click += new System.EventHandler(this.BoostShieldBttn_Click);
            // 
            // Shield4Bttn
            // 
            this.Shield4Bttn.BackColor = System.Drawing.Color.Lime;
            this.Shield4Bttn.FlatAppearance.BorderColor = System.Drawing.Color.Lime;
            this.Shield4Bttn.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Shield4Bttn.ForeColor = System.Drawing.Color.DarkMagenta;
            this.Shield4Bttn.Location = new System.Drawing.Point(23, 66);
            this.Shield4Bttn.Name = "Shield4Bttn";
            this.Shield4Bttn.Size = new System.Drawing.Size(28, 26);
            this.Shield4Bttn.TabIndex = 6;
            this.Shield4Bttn.Text = "4";
            this.Shield4Bttn.UseVisualStyleBackColor = false;
            this.Shield4Bttn.Click += new System.EventHandler(this.BoostShieldBttn_Click);
            // 
            // W1ProgressBar
            // 
            this.W1ProgressBar.Location = new System.Drawing.Point(70, 31);
            this.W1ProgressBar.Name = "W1ProgressBar";
            this.W1ProgressBar.Size = new System.Drawing.Size(173, 23);
            this.W1ProgressBar.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(19, 110);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "W3:";
            // 
            // Shield6Bttn
            // 
            this.Shield6Bttn.BackColor = System.Drawing.Color.Lime;
            this.Shield6Bttn.FlatAppearance.BorderColor = System.Drawing.Color.Lime;
            this.Shield6Bttn.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Shield6Bttn.ForeColor = System.Drawing.Color.DarkMagenta;
            this.Shield6Bttn.Location = new System.Drawing.Point(90, 66);
            this.Shield6Bttn.Name = "Shield6Bttn";
            this.Shield6Bttn.Size = new System.Drawing.Size(28, 26);
            this.Shield6Bttn.TabIndex = 5;
            this.Shield6Bttn.Text = "6";
            this.Shield6Bttn.UseVisualStyleBackColor = false;
            this.Shield6Bttn.Click += new System.EventHandler(this.BoostShieldBttn_Click);
            // 
            // Shield7Bttn
            // 
            this.Shield7Bttn.BackColor = System.Drawing.Color.Lime;
            this.Shield7Bttn.FlatAppearance.BorderColor = System.Drawing.Color.Lime;
            this.Shield7Bttn.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Shield7Bttn.ForeColor = System.Drawing.Color.DarkMagenta;
            this.Shield7Bttn.Location = new System.Drawing.Point(23, 98);
            this.Shield7Bttn.Name = "Shield7Bttn";
            this.Shield7Bttn.Size = new System.Drawing.Size(28, 26);
            this.Shield7Bttn.TabIndex = 4;
            this.Shield7Bttn.Text = "7";
            this.Shield7Bttn.UseVisualStyleBackColor = false;
            this.Shield7Bttn.Click += new System.EventHandler(this.BoostShieldBttn_Click);
            // 
            // Shield8Bttn
            // 
            this.Shield8Bttn.BackColor = System.Drawing.Color.Lime;
            this.Shield8Bttn.FlatAppearance.BorderColor = System.Drawing.Color.Lime;
            this.Shield8Bttn.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Shield8Bttn.ForeColor = System.Drawing.Color.DarkMagenta;
            this.Shield8Bttn.Location = new System.Drawing.Point(57, 98);
            this.Shield8Bttn.Name = "Shield8Bttn";
            this.Shield8Bttn.Size = new System.Drawing.Size(28, 26);
            this.Shield8Bttn.TabIndex = 3;
            this.Shield8Bttn.Text = "8";
            this.Shield8Bttn.UseVisualStyleBackColor = false;
            this.Shield8Bttn.Click += new System.EventHandler(this.BoostShieldBttn_Click);
            // 
            // Shield9Bttn
            // 
            this.Shield9Bttn.BackColor = System.Drawing.Color.Lime;
            this.Shield9Bttn.FlatAppearance.BorderColor = System.Drawing.Color.Lime;
            this.Shield9Bttn.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Shield9Bttn.ForeColor = System.Drawing.Color.DarkMagenta;
            this.Shield9Bttn.Location = new System.Drawing.Point(90, 98);
            this.Shield9Bttn.Name = "Shield9Bttn";
            this.Shield9Bttn.Size = new System.Drawing.Size(28, 26);
            this.Shield9Bttn.TabIndex = 2;
            this.Shield9Bttn.Text = "9";
            this.Shield9Bttn.UseVisualStyleBackColor = false;
            this.Shield9Bttn.Click += new System.EventHandler(this.BoostShieldBttn_Click);
            // 
            // Shield5Bttn
            // 
            this.Shield5Bttn.BackColor = System.Drawing.Color.Lime;
            this.Shield5Bttn.FlatAppearance.BorderColor = System.Drawing.Color.Lime;
            this.Shield5Bttn.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Shield5Bttn.ForeColor = System.Drawing.Color.DarkMagenta;
            this.Shield5Bttn.Location = new System.Drawing.Point(56, 66);
            this.Shield5Bttn.Name = "Shield5Bttn";
            this.Shield5Bttn.Size = new System.Drawing.Size(28, 26);
            this.Shield5Bttn.TabIndex = 1;
            this.Shield5Bttn.Text = "5";
            this.Shield5Bttn.UseVisualStyleBackColor = false;
            this.Shield5Bttn.Click += new System.EventHandler(this.BoostShieldBttn_Click);
            // 
            // W3ProgressBar
            // 
            this.W3ProgressBar.Location = new System.Drawing.Point(70, 110);
            this.W3ProgressBar.Name = "W3ProgressBar";
            this.W3ProgressBar.Size = new System.Drawing.Size(173, 23);
            this.W3ProgressBar.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(19, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "W2:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(19, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "W1:";
            // 
            // OxygenProgressBar
            // 
            this.OxygenProgressBar.BackColor = System.Drawing.SystemColors.Control;
            this.OxygenProgressBar.Location = new System.Drawing.Point(6, 31);
            this.OxygenProgressBar.Name = "OxygenProgressBar";
            this.OxygenProgressBar.Size = new System.Drawing.Size(399, 23);
            this.OxygenProgressBar.TabIndex = 0;
            // 
            // Shield1Bttn
            // 
            this.Shield1Bttn.BackColor = System.Drawing.Color.Lime;
            this.Shield1Bttn.FlatAppearance.BorderColor = System.Drawing.Color.Lime;
            this.Shield1Bttn.Font = new System.Drawing.Font("Georgia", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Shield1Bttn.ForeColor = System.Drawing.Color.DarkMagenta;
            this.Shield1Bttn.Location = new System.Drawing.Point(22, 34);
            this.Shield1Bttn.Name = "Shield1Bttn";
            this.Shield1Bttn.Size = new System.Drawing.Size(28, 26);
            this.Shield1Bttn.TabIndex = 0;
            this.Shield1Bttn.Text = "1";
            this.Shield1Bttn.UseVisualStyleBackColor = false;
            this.Shield1Bttn.Click += new System.EventHandler(this.BoostShieldBttn_Click);
            // 
            // TakeOffW2Bttn
            // 
            this.TakeOffW2Bttn.BackColor = System.Drawing.Color.Salmon;
            this.TakeOffW2Bttn.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TakeOffW2Bttn.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.TakeOffW2Bttn.Location = new System.Drawing.Point(330, 70);
            this.TakeOffW2Bttn.Name = "TakeOffW2Bttn";
            this.TakeOffW2Bttn.Size = new System.Drawing.Size(75, 23);
            this.TakeOffW2Bttn.TabIndex = 11;
            this.TakeOffW2Bttn.Text = "Take off";
            this.TakeOffW2Bttn.UseVisualStyleBackColor = false;
            this.TakeOffW2Bttn.Click += new System.EventHandler(this.TakeOffWeaponBttn_Click);
            // 
            // RepairW2Bttn
            // 
            this.RepairW2Bttn.BackColor = System.Drawing.Color.LimeGreen;
            this.RepairW2Bttn.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RepairW2Bttn.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.RepairW2Bttn.Location = new System.Drawing.Point(249, 70);
            this.RepairW2Bttn.Name = "RepairW2Bttn";
            this.RepairW2Bttn.Size = new System.Drawing.Size(75, 23);
            this.RepairW2Bttn.TabIndex = 10;
            this.RepairW2Bttn.Text = "Repair";
            this.RepairW2Bttn.UseVisualStyleBackColor = false;
            this.RepairW2Bttn.Click += new System.EventHandler(this.RepairWeaponBttn_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.BoostShieldsBttn);
            this.groupBox3.Controls.Add(this.Shield2Bttn);
            this.groupBox3.Controls.Add(this.Shield3Bttn);
            this.groupBox3.Controls.Add(this.Shield4Bttn);
            this.groupBox3.Controls.Add(this.Shield6Bttn);
            this.groupBox3.Controls.Add(this.Shield7Bttn);
            this.groupBox3.Controls.Add(this.Shield8Bttn);
            this.groupBox3.Controls.Add(this.Shield9Bttn);
            this.groupBox3.Controls.Add(this.Shield5Bttn);
            this.groupBox3.Controls.Add(this.Shield1Bttn);
            this.groupBox3.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.ForeColor = System.Drawing.Color.Black;
            this.groupBox3.Location = new System.Drawing.Point(12, 360);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(411, 144);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Shield System";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.TakeOffW2Bttn);
            this.groupBox2.Controls.Add(this.RepairW2Bttn);
            this.groupBox2.Controls.Add(this.TakeOffW3Bttn);
            this.groupBox2.Controls.Add(this.RepairW3Bttn);
            this.groupBox2.Controls.Add(this.TakeOffW1Bttn);
            this.groupBox2.Controls.Add(this.RepairW1Bttn);
            this.groupBox2.Controls.Add(this.W2ProgressBar);
            this.groupBox2.Controls.Add(this.W3ProgressBar);
            this.groupBox2.Controls.Add(this.W1ProgressBar);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.Black;
            this.groupBox2.Location = new System.Drawing.Point(12, 196);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(411, 148);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Weapon System";
            // 
            // TakeOffW3Bttn
            // 
            this.TakeOffW3Bttn.BackColor = System.Drawing.Color.Salmon;
            this.TakeOffW3Bttn.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TakeOffW3Bttn.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.TakeOffW3Bttn.Location = new System.Drawing.Point(330, 110);
            this.TakeOffW3Bttn.Name = "TakeOffW3Bttn";
            this.TakeOffW3Bttn.Size = new System.Drawing.Size(75, 23);
            this.TakeOffW3Bttn.TabIndex = 9;
            this.TakeOffW3Bttn.Text = "Take off";
            this.TakeOffW3Bttn.UseVisualStyleBackColor = false;
            this.TakeOffW3Bttn.Click += new System.EventHandler(this.TakeOffWeaponBttn_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.OxygenProgressBar);
            this.groupBox1.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.Black;
            this.groupBox1.Location = new System.Drawing.Point(12, 116);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(411, 74);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Life Support System";
            // 
            // timeLabel
            // 
            this.timeLabel.AutoSize = true;
            this.timeLabel.Font = new System.Drawing.Font("Georgia", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeLabel.ForeColor = System.Drawing.Color.Black;
            this.timeLabel.Location = new System.Drawing.Point(49, 37);
            this.timeLabel.Name = "timeLabel";
            this.timeLabel.Size = new System.Drawing.Size(0, 38);
            this.timeLabel.TabIndex = 0;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.timeLabel);
            this.groupBox4.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.ForeColor = System.Drawing.Color.Black;
            this.groupBox4.Location = new System.Drawing.Point(12, 12);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(411, 100);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Remaining Time";
            // 
            // EvacuateShipBttn
            // 
            this.EvacuateShipBttn.BackColor = System.Drawing.Color.Red;
            this.EvacuateShipBttn.Font = new System.Drawing.Font("Georgia", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EvacuateShipBttn.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.EvacuateShipBttn.Location = new System.Drawing.Point(16, 426);
            this.EvacuateShipBttn.Name = "EvacuateShipBttn";
            this.EvacuateShipBttn.Size = new System.Drawing.Size(621, 58);
            this.EvacuateShipBttn.TabIndex = 1;
            this.EvacuateShipBttn.Text = "EVACUATE SHIP !";
            this.EvacuateShipBttn.UseVisualStyleBackColor = false;
            this.EvacuateShipBttn.Click += new System.EventHandler(this.EvacuateShipBttn_Click);
            // 
            // consoleTxtBox
            // 
            this.consoleTxtBox.BackColor = System.Drawing.SystemColors.MenuText;
            this.consoleTxtBox.Font = new System.Drawing.Font("Lucida Sans Unicode", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.consoleTxtBox.ForeColor = System.Drawing.SystemColors.Window;
            this.consoleTxtBox.Location = new System.Drawing.Point(16, 12);
            this.consoleTxtBox.Name = "consoleTxtBox";
            this.consoleTxtBox.ReadOnly = true;
            this.consoleTxtBox.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.consoleTxtBox.Size = new System.Drawing.Size(621, 396);
            this.consoleTxtBox.TabIndex = 0;
            this.consoleTxtBox.Text = "WELCOME!!";
            this.consoleTxtBox.WordWrap = false;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.groupBox4);
            this.splitContainer1.Panel1.Controls.Add(this.groupBox1);
            this.splitContainer1.Panel1.Controls.Add(this.groupBox2);
            this.splitContainer1.Panel1.Controls.Add(this.groupBox3);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.EvacuateShipBttn);
            this.splitContainer1.Panel2.Controls.Add(this.consoleTxtBox);
            this.splitContainer1.Panel2.ForeColor = System.Drawing.SystemColors.Control;
            this.splitContainer1.Size = new System.Drawing.Size(1109, 514);
            this.splitContainer1.SplitterDistance = 445;
            this.splitContainer1.TabIndex = 1;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1109, 514);
            this.Controls.Add(this.splitContainer1);
            this.Name = "MainForm";
            this.Text = "Form1";
            this.Shown += new System.EventHandler(this.MainForm_Shown);
            this.groupBox3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button RepairW3Bttn;
        private System.Windows.Forms.Button TakeOffW1Bttn;
        private System.Windows.Forms.Button RepairW1Bttn;
        private System.Windows.Forms.ProgressBar W2ProgressBar;
        private System.Windows.Forms.Button BoostShieldsBttn;
        private System.Windows.Forms.Button Shield2Bttn;
        private System.Windows.Forms.Button Shield3Bttn;
        private System.Windows.Forms.Button Shield4Bttn;
        private System.Windows.Forms.ProgressBar W1ProgressBar;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button Shield6Bttn;
        private System.Windows.Forms.Button Shield7Bttn;
        private System.Windows.Forms.Button Shield8Bttn;
        private System.Windows.Forms.Button Shield9Bttn;
        private System.Windows.Forms.Button Shield5Bttn;
        private System.Windows.Forms.ProgressBar W3ProgressBar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ProgressBar OxygenProgressBar;
        private System.Windows.Forms.Button Shield1Bttn;
        private System.Windows.Forms.Button TakeOffW2Bttn;
        private System.Windows.Forms.Button RepairW2Bttn;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button TakeOffW3Bttn;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label timeLabel;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button EvacuateShipBttn;
        private System.Windows.Forms.RichTextBox consoleTxtBox;
        private System.Windows.Forms.SplitContainer splitContainer1;
    }
}

