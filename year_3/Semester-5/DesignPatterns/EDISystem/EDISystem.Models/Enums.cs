﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EDISystem.Models
{
    public enum Components
    {
        Shield,
        Weapon
    }

    public enum StateName
    {
        Red,
        Green,
        Orange,
        Online,
        Offline,
        Jammed,
        Balanced,
        Disabled,
        Crashed
    }

    public enum ArticleType
    {
        Oxygen,
        Heat,
        Energy,
        Power
    }

    public enum SystemName
    {
        LifeSupport,
        Weapons,
        Shield
    }
}
