﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EDISystem.Models.States;

namespace EDISystem.Models
{
    public class Weapon : BaseModel
    {
        public int TotalAmmunition { get; set; }
        public int CurrentAmmunition { get; set; }
        public string WeaponName { get; set; }

        readonly Random _random = new Random(Guid.NewGuid().GetHashCode());

        public Weapon()
        {
            State = new GreenState()
            {
                Level = 100,
                System = this
            };
            ConsumptionRate = _random.NextDouble() * 1.1;
        }

        public Weapon(string name, ArticleType heat, ShipSystem systemEngine)
        {
            WeaponName= name;
            Article = heat;
            State = new GreenState()
            {
                Level = 100,
                MainSystem = systemEngine,
                System = this
            };
        }

        public override void Add(BaseModel model)
        {
            throw new NotImplementedException();
        }

        public override void Remove(BaseModel model)
        {
            throw new NotImplementedException();
        }
    }
}
