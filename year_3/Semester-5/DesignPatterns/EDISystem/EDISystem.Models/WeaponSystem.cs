﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EDISystem.Models.States;

namespace EDISystem.Models
{
    public class WeaponSystem : BaseModel
    {
        public List<Weapon> Weapons = new List<Weapon>();

        public WeaponSystem()
        {
            Name = SystemName.Weapons;
            State = new GreenState()
            {
                System = this
            };
            ConsumptionRate = 0;
        }

        public override void Add(BaseModel model)
        {
            Weapons.Add((Weapon)model);
        }

        public override void Remove(BaseModel model)
        {
            Weapons.Remove((Weapon)model);
        }
    }
}
