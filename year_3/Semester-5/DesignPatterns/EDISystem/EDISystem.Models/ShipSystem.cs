﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EDISystem.Models.States;

namespace EDISystem.Models
{
    public class ShipSystem : BaseModel
    {
        public List<BaseModel> SubSystems;
        public ShipSystem()
        {
            Name = SystemName.Shield;
            State = new GreenState()
            {
                System = this
            };
            ConsumptionRate = 0;
            SubSystems = new List<BaseModel>();
        }

        public override void Add(BaseModel model)
        {
            SubSystems.Add(model);
        }

        public override void Remove(BaseModel model)
        {
            SubSystems.Remove(model);
        }
    }
}
