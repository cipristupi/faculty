﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EDISystem.Models.States
{
    public class GreenState : State
    {
        public GreenState()
        {
            StateName = StateName.Green;
        }
        public override void StateChangeCheck()
        {
            double lowLimit = 0;
            switch (System.Article)
            {
                case ArticleType.Oxygen:
                    lowLimit = 11;
                    break;
                case ArticleType.Heat:
                    lowLimit = 21;
                    break;
                case ArticleType.Energy:
                    lowLimit = 26;
                    break;
            }
            if (Level < lowLimit)
            {
                System.State = new RedState();
            }
            else if (Level < LowerLimit)
            {
                System.State = new BalancedState();
            }
        }

        public override void Initialize()
        {
            UpperLimit = 100;

            LowerLimit = System.Article == ArticleType.Energy ? 76 : 61;
        }

        public override void Fullfill(int amount)
        {
            Level += amount;
            if (Level > 100)
                Level = 100;
            StateChangeCheck();
        }
    }
}
