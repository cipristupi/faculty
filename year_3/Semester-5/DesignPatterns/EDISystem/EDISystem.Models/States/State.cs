﻿using System;
using System.Linq;

namespace EDISystem.Models.States
{
    public abstract class State
    {
        protected Random Random;

        public StateName StateName { get; set; }
        public BaseModel System { get; set; }
        public ShipSystem MainSystem { get; set; }
        public int LowerLimit { get; set; }
        public int UpperLimit { get; set; }
        public double Level { get; set; }

        public void Offline()
        {
            System.State = new OfflineState();
        }

        public void Enable(State state = null)
        {
            System.State = state ?? new OnlineState();
        }

        public void SetState(State state)
        {
            System.State = state;
        }

        public abstract void StateChangeCheck();
        public abstract void Initialize();
        public abstract void Fullfill(int amount);

        public virtual void Consume()
        {
            if (System.State.Level <= 0 || System.State.StateName == StateName.Disabled) return;
            SystemName name = System.Name;
            switch (name)
            {
                case SystemName.Shield:
                    var shieldMat = System as ShieldMatrix;
                    if (shieldMat != null)
                    {
                        var shields = shieldMat.Shields;
                        double avg = shields.Aggregate<Shield, double>(0, (current, t) => current + t.State.Level);
                        Level = (avg / shields.Count);
                    }
                    break;
                case SystemName.Weapons:
                    var weaponTools = System as WeaponSystem;
                    if (weaponTools != null)
                    {
                        var weapons = weaponTools.Weapons;
                        double avg1 = weapons.Aggregate<Weapon, double>(0, (current, t) => current + t.State.Level);
                        Level = (avg1 / weapons.Count);
                    }
                    break;
                case SystemName.LifeSupport:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            Level -= (int) System.ConsumptionRate;
            StateChangeCheck();
        }

        public void Disable()
        {
            System.State = new DisabledState();
        }
    }
}