﻿using System;
using System.Linq;

namespace EDISystem.Models.States
{
    public class BalancedState : State
    {
        public BalancedState()
        {
            StateName = StateName.Balanced;
            Initialize();
        }

        public override void Initialize()
        {
            Random = new Random();
            switch (System.Article)
            {
                case ArticleType.Oxygen:
                    LowerLimit = 11;
                    UpperLimit = 60;
                    break;
                case ArticleType.Heat:
                    LowerLimit = 21;
                    UpperLimit = 60;
                    MainSystem.SubSystems.First(s => s.Article == ArticleType.Oxygen).ConsumptionRate *= Random.NextDouble() * 2.5;
                    System.ConsumptionRate *= Random.NextDouble() * 1.5;
                    break;
                case ArticleType.Energy:
                    LowerLimit = 25;
                    UpperLimit = 75;  // 25%
                    MainSystem.SubSystems.First(s => s.Article == ArticleType.Oxygen).ConsumptionRate *= Random.NextDouble() * 1.5;
                    System.ConsumptionRate *= Random.NextDouble() * 1.5;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public override void Fullfill(int amount)
        {
            Level += amount;
            if (Level > 100)
                Level = 100;
            switch (System.Article)
            {
                case ArticleType.Heat:
                    System.ConsumptionRate /= Random.NextDouble() * 1.5;
                    break;
                case ArticleType.Energy:
                    System.ConsumptionRate /= Random.NextDouble() * 1.5;
                    break;
                case ArticleType.Oxygen:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            StateChangeCheck();
        }

        public override void StateChangeCheck()
        {
            if (Level < LowerLimit)
            {
                System.State = new RedState();
            }
            else if (Level > UpperLimit)
            {
                System.State = new GreenState();
            }
        }
    }
}
