﻿using System;

namespace EDISystem.Models.States
{
    public class RedState : State
    {
        public RedState()
        {
            Random = new Random(Guid.NewGuid().GetHashCode());
            StateName = StateName.Red;
        }

        public override void Initialize()
        {
            Random = new Random();
            switch (System.Article)
            {
                case ArticleType.Oxygen:
                    LowerLimit = 0;
                    UpperLimit = 10;  // 10%
                    break;
                case ArticleType.Heat:
                    LowerLimit = 0;
                    UpperLimit = 20;  // 20%
                    System.ConsumptionRate *= Random.NextDouble() * 2;
                    break;
                case ArticleType.Energy:
                    LowerLimit = 0;
                    UpperLimit = 25;  // 25%
                    System.ConsumptionRate *= Random.NextDouble() * 3.5;
                    break;
            }
        }

        public override void Fullfill(int amount)
        {
            Level += amount;
            if (Level > 100)
                Level = 100;
            switch (System.Article)
            {
                case ArticleType.Heat:
                    System.ConsumptionRate /= Random.NextDouble() * 2;
                    break;
                case ArticleType.Energy:
                    LowerLimit = 0;
                    UpperLimit = 25;  // 25%
                    System.ConsumptionRate /= Random.NextDouble() * 3.5;
                    break;
                case ArticleType.Oxygen:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            StateChangeCheck();
        }

        public override void StateChangeCheck()
        {
            if (Level <= 0 && System.Name != SystemName.LifeSupport)
            {
                System.State = new BalancedState();
            }
            if (Level > UpperLimit)
            {
                System.State = new BalancedState();
            }
        }
    }
}
