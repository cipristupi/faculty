﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EDISystem.Models.States
{
    class OfflineState : State
    {
        public OfflineState()
        {
            StateName = StateName.Offline;
        }
        public override void StateChangeCheck()
        {
            throw new NotImplementedException();
        }

        public override void Initialize()
        {
            throw new NotImplementedException();
        }

        public override void Fullfill(int amount)
        {
            throw new NotImplementedException();
        }
    }
}
