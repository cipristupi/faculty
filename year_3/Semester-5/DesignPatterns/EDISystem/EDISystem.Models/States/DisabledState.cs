﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EDISystem.Models.States
{
    public class DisabledState : State
    {
        public DisabledState()
        {
            StateName = StateName.Disabled;
        }

        public override void Fullfill(int amount)
        {
            Level += 10;
            System.State = new RedState();
        }

        public override void StateChangeCheck()
        {
            throw new NotImplementedException();
        }

        public override void Initialize()
        {
            throw new NotImplementedException();
        }
    }
}
