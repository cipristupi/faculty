﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EDISystem.Models.States
{
    public class CrashedState : State
    {
        public override void StateChangeCheck()
        {
            throw new NotImplementedException();
        }

        public override void Initialize()
        {
            throw new NotImplementedException();
        }

        public override void Fullfill(int amount)
        {
            Level = 0;
        }
        public void Crash(List<Shield> shields)
        {
            shields.Remove((Shield)System);
        }
    }
}
