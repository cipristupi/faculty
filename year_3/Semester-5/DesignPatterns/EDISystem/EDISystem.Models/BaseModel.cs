﻿using System;
using EDISystem.Models.States;

namespace EDISystem.Models
{
    public abstract class BaseModel
    {
        public string Id { get; set; } = Guid.NewGuid().ToString();
        public State State { get; set; }
        public ShipSystem Engine { get; set; }

        public SystemName Name { get; set; }

        public ArticleType Article { get; set; }
        public double ConsumptionRate { get; set; }

        public abstract void Add(BaseModel model);
        public abstract void Remove(BaseModel model);
    }
}
