﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EDISystem.Models.States;

namespace EDISystem.Models
{
    public class LifeSupportSystem : BaseModel
    {
        public LifeSupportSystem()
        {
            Name = SystemName.LifeSupport;
            State = new GreenState()
            {
                System = this
            };
            ConsumptionRate = 1;
        }
        public override void Add(BaseModel model)
        {
            throw new NotImplementedException();
        }

        public override void Remove(BaseModel model)
        {
            throw new NotImplementedException();
        }
    }
}
