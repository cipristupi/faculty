﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EDISystem.Models.States;

namespace EDISystem.Models
{
    public class Shield : BaseModel
    {
        public string ShieldName { get; set; }
        readonly Random _random = new Random(Guid.NewGuid().GetHashCode());

        public Shield()
        {
            State = new GreenState();
            ConsumptionRate = _random.NextDouble() * 1.2;

        }

        public Shield(string name, ArticleType energy, ShipSystem systemEngine)
        {
            ShieldName = name;
            Article = energy;
            State = new GreenState()
            {
                Level = 100,
                MainSystem = systemEngine,
                StateName = StateName.Green,
                System = this
            };
        }

        public override void Add(BaseModel model)
        {
            throw new NotImplementedException();
        }

        public override void Remove(BaseModel model)
        {
            throw new NotImplementedException();
        }
    }
}
