﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EDISystem.Models.States;

namespace EDISystem.Models
{
    public class ShieldMatrix : BaseModel
    {
        public List<Shield> Shields = new List<Shield>();
        public ShieldMatrix()
        {
            State = new GreenState();
            ConsumptionRate = 0;
        }

        public override void Add(BaseModel model)
        {
            Shields.Add((Shield)model);
        }

        public override void Remove(BaseModel model)
        {
            Shields.Remove((Shield)model);
        }
    }
}
