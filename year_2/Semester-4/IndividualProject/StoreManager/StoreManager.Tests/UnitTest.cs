﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StoreManager.Repository;

namespace StoreManager.Tests
{
    [TestClass]
    public class UnitTest
    {
        IFileRepository repository;

        [TestInitialize]
        public void Initialize()
        {
            repository = new FileRepository(System.IO.Directory.GetCurrentDirectory() + "\\Test.txt");
        }

        [TestCleanup]
        public void Cleanup()
        {

        }

        [TestMethod]
        public void Add()
        {
            Material material = new Material()
            {
                Code = "4",
                Name = "cola",
                Price = 100,
                Quantity = 100
            };
            repository.Add(material);
            Material savedMaterial = repository.GetByCode(material.Code);
            Assert.AreEqual(material.Name, savedMaterial.Name);
            Assert.AreEqual(material.Price, savedMaterial.Price);
            Assert.AreEqual(material.Quantity,savedMaterial.Quantity);
        }

        [TestMethod]
        public void Update()
        {
            Material material = new Material()
            {
                Code = "3",
                Name = "Something",
                Price = 101,
                Quantity = 101
            };
            repository.Update(material);
            Material savedMaterial = repository.GetByCode(material.Code);
            Assert.AreEqual(material.Name, savedMaterial.Name);
            Assert.AreEqual(material.Price, savedMaterial.Price);
            Assert.AreEqual(material.Quantity, savedMaterial.Quantity);
        }

        [TestMethod]
        public void Delete()
        {
            int initCount = repository.GetAll().Count;
            repository.Delete("4");
            int finalCount = repository.GetAll().Count;
            Assert.AreNotEqual(initCount, finalCount);
        }

    }
}
