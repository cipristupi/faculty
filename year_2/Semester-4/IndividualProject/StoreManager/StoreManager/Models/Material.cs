﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace StoreManager
{
    public class Material 
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public int Quantity { get; set; }
        public float Price { get; set; }

        /// <summary>
        /// Create an material from a string
        /// </summary>
        /// <param name="s">material in string format from file</param>
        /// <returns>a material</returns>

        public static Material Read(string s)
        {
            Material m = new Material();
			string[] ms = s.Split('|');
			m.Code = ms[0];
			m.Name = ms[1];
			m.Quantity = int.Parse(ms[2]);
			m.Price = float.Parse(ms[3]);
            return m;
        }

        public override string ToString()
        {
            return string.Format("{0}|{1}|{2}|{3}", Code, Name, Quantity, Price);
        }
    }
}
