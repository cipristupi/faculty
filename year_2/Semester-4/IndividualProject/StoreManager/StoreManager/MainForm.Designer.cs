﻿namespace StoreManager
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.txtPriceAdd = new System.Windows.Forms.TextBox();
            this.txtQuantityAdd = new System.Windows.Forms.TextBox();
            this.txtNameAdd = new System.Windows.Forms.TextBox();
            this.txtCodeAdd = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnLoadMaterials = new System.Windows.Forms.Button();
            this.cmbCodeUpdate = new System.Windows.Forms.ComboBox();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.txtPriceUpdate = new System.Windows.Forms.TextBox();
            this.txtQuantityUpdate = new System.Windows.Forms.TextBox();
            this.txtNameUpdate = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnLower = new System.Windows.Forms.Button();
            this.btnGreater = new System.Windows.Forms.Button();
            this.txtFilter = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.btnGetByPrice = new System.Windows.Forms.Button();
            this.btnQuantityZero = new System.Windows.Forms.Button();
            this.btnCompleteInventary = new System.Windows.Forms.Button();
            this.btnSaveToFile = new System.Windows.Forms.Button();
            this.dgvReportPreview = new System.Windows.Forms.DataGridView();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btnLoadDelete = new System.Windows.Forms.Button();
            this.cmbDelete = new System.Windows.Forms.ComboBox();
            this.btnDelete = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReportPreview)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnSave);
            this.groupBox1.Controls.Add(this.txtPriceAdd);
            this.groupBox1.Controls.Add(this.txtQuantityAdd);
            this.groupBox1.Controls.Add(this.txtNameAdd);
            this.groupBox1.Controls.Add(this.txtCodeAdd);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(17, 16);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(424, 220);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Add Material";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(12, 172);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(133, 38);
            this.btnSave.TabIndex = 8;
            this.btnSave.Text = "Add";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txtPriceAdd
            // 
            this.txtPriceAdd.Location = new System.Drawing.Point(82, 134);
            this.txtPriceAdd.Name = "txtPriceAdd";
            this.txtPriceAdd.Size = new System.Drawing.Size(319, 23);
            this.txtPriceAdd.TabIndex = 7;
            // 
            // txtQuantityAdd
            // 
            this.txtQuantityAdd.Location = new System.Drawing.Point(82, 105);
            this.txtQuantityAdd.Name = "txtQuantityAdd";
            this.txtQuantityAdd.Size = new System.Drawing.Size(319, 23);
            this.txtQuantityAdd.TabIndex = 6;
            // 
            // txtNameAdd
            // 
            this.txtNameAdd.Location = new System.Drawing.Point(82, 63);
            this.txtNameAdd.Name = "txtNameAdd";
            this.txtNameAdd.Size = new System.Drawing.Size(319, 23);
            this.txtNameAdd.TabIndex = 5;
            // 
            // txtCodeAdd
            // 
            this.txtCodeAdd.Location = new System.Drawing.Point(82, 25);
            this.txtCodeAdd.Name = "txtCodeAdd";
            this.txtCodeAdd.Size = new System.Drawing.Size(319, 23);
            this.txtCodeAdd.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 137);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 17);
            this.label4.TabIndex = 3;
            this.label4.Text = "Price:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 108);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Quantity";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 66);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Name:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 28);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Code:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnLoadMaterials);
            this.groupBox2.Controls.Add(this.cmbCodeUpdate);
            this.groupBox2.Controls.Add(this.btnUpdate);
            this.groupBox2.Controls.Add(this.txtPriceUpdate);
            this.groupBox2.Controls.Add(this.txtQuantityUpdate);
            this.groupBox2.Controls.Add(this.txtNameUpdate);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Location = new System.Drawing.Point(17, 244);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(424, 211);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Update Material";
            // 
            // btnLoadMaterials
            // 
            this.btnLoadMaterials.Location = new System.Drawing.Point(176, 166);
            this.btnLoadMaterials.Name = "btnLoadMaterials";
            this.btnLoadMaterials.Size = new System.Drawing.Size(133, 38);
            this.btnLoadMaterials.TabIndex = 19;
            this.btnLoadMaterials.Text = "Load Codes";
            this.btnLoadMaterials.UseVisualStyleBackColor = true;
            this.btnLoadMaterials.Click += new System.EventHandler(this.btnLoadMaterials_Click);
            // 
            // cmbCodeUpdate
            // 
            this.cmbCodeUpdate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCodeUpdate.FormattingEnabled = true;
            this.cmbCodeUpdate.Location = new System.Drawing.Point(89, 24);
            this.cmbCodeUpdate.Name = "cmbCodeUpdate";
            this.cmbCodeUpdate.Size = new System.Drawing.Size(319, 24);
            this.cmbCodeUpdate.TabIndex = 18;
            this.cmbCodeUpdate.SelectedIndexChanged += new System.EventHandler(this.cmbCodeUpdate_SelectedIndexChanged);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(19, 167);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(133, 38);
            this.btnUpdate.TabIndex = 17;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // txtPriceUpdate
            // 
            this.txtPriceUpdate.Location = new System.Drawing.Point(89, 138);
            this.txtPriceUpdate.Name = "txtPriceUpdate";
            this.txtPriceUpdate.Size = new System.Drawing.Size(319, 23);
            this.txtPriceUpdate.TabIndex = 16;
            // 
            // txtQuantityUpdate
            // 
            this.txtQuantityUpdate.Location = new System.Drawing.Point(89, 107);
            this.txtQuantityUpdate.Name = "txtQuantityUpdate";
            this.txtQuantityUpdate.Size = new System.Drawing.Size(319, 23);
            this.txtQuantityUpdate.TabIndex = 15;
            // 
            // txtNameUpdate
            // 
            this.txtNameUpdate.Location = new System.Drawing.Point(89, 65);
            this.txtNameUpdate.Name = "txtNameUpdate";
            this.txtNameUpdate.Size = new System.Drawing.Size(319, 23);
            this.txtNameUpdate.TabIndex = 14;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 144);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 17);
            this.label5.TabIndex = 12;
            this.label5.Text = "Price:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, 107);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 17);
            this.label6.TabIndex = 11;
            this.label6.Text = "Quantity";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(16, 65);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 17);
            this.label7.TabIndex = 10;
            this.label7.Text = "Name:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(16, 27);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(45, 17);
            this.label8.TabIndex = 9;
            this.label8.Text = "Code:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnLower);
            this.groupBox3.Controls.Add(this.btnGreater);
            this.groupBox3.Controls.Add(this.txtFilter);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.btnGetByPrice);
            this.groupBox3.Controls.Add(this.btnQuantityZero);
            this.groupBox3.Controls.Add(this.btnCompleteInventary);
            this.groupBox3.Controls.Add(this.btnSaveToFile);
            this.groupBox3.Controls.Add(this.dgvReportPreview);
            this.groupBox3.Location = new System.Drawing.Point(465, 16);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(768, 541);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Reports";
            // 
            // btnLower
            // 
            this.btnLower.Location = new System.Drawing.Point(177, 454);
            this.btnLower.Name = "btnLower";
            this.btnLower.Size = new System.Drawing.Size(147, 41);
            this.btnLower.TabIndex = 11;
            this.btnLower.Text = "Lower:";
            this.btnLower.UseVisualStyleBackColor = true;
            this.btnLower.Click += new System.EventHandler(this.btnLower_Click);
            // 
            // btnGreater
            // 
            this.btnGreater.Location = new System.Drawing.Point(177, 395);
            this.btnGreater.Name = "btnGreater";
            this.btnGreater.Size = new System.Drawing.Size(147, 41);
            this.btnGreater.TabIndex = 10;
            this.btnGreater.Text = "Greater:";
            this.btnGreater.UseVisualStyleBackColor = true;
            this.btnGreater.Click += new System.EventHandler(this.btnGreater_Click);
            // 
            // txtFilter
            // 
            this.txtFilter.Location = new System.Drawing.Point(385, 366);
            this.txtFilter.Name = "txtFilter";
            this.txtFilter.Size = new System.Drawing.Size(319, 23);
            this.txtFilter.TabIndex = 9;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(382, 346);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(44, 17);
            this.label9.TabIndex = 8;
            this.label9.Text = "Price:";
            // 
            // btnGetByPrice
            // 
            this.btnGetByPrice.Location = new System.Drawing.Point(177, 346);
            this.btnGetByPrice.Name = "btnGetByPrice";
            this.btnGetByPrice.Size = new System.Drawing.Size(147, 41);
            this.btnGetByPrice.TabIndex = 5;
            this.btnGetByPrice.Text = "By price:";
            this.btnGetByPrice.UseVisualStyleBackColor = true;
            this.btnGetByPrice.Click += new System.EventHandler(this.btnGetByPrice_Click);
            // 
            // btnQuantityZero
            // 
            this.btnQuantityZero.Location = new System.Drawing.Point(7, 454);
            this.btnQuantityZero.Name = "btnQuantityZero";
            this.btnQuantityZero.Size = new System.Drawing.Size(147, 41);
            this.btnQuantityZero.TabIndex = 3;
            this.btnQuantityZero.Text = "Quantity zero";
            this.btnQuantityZero.UseVisualStyleBackColor = true;
            this.btnQuantityZero.Click += new System.EventHandler(this.btnQuantityZero_Click);
            // 
            // btnCompleteInventary
            // 
            this.btnCompleteInventary.Location = new System.Drawing.Point(7, 395);
            this.btnCompleteInventary.Name = "btnCompleteInventary";
            this.btnCompleteInventary.Size = new System.Drawing.Size(147, 41);
            this.btnCompleteInventary.TabIndex = 2;
            this.btnCompleteInventary.Text = "Complete Inventary";
            this.btnCompleteInventary.UseVisualStyleBackColor = true;
            this.btnCompleteInventary.Click += new System.EventHandler(this.btnCompleteInventary_Click);
            // 
            // btnSaveToFile
            // 
            this.btnSaveToFile.Location = new System.Drawing.Point(7, 346);
            this.btnSaveToFile.Name = "btnSaveToFile";
            this.btnSaveToFile.Size = new System.Drawing.Size(147, 41);
            this.btnSaveToFile.TabIndex = 1;
            this.btnSaveToFile.Text = "Save to file";
            this.btnSaveToFile.UseVisualStyleBackColor = true;
            this.btnSaveToFile.Click += new System.EventHandler(this.btnSaveToFile_Click);
            // 
            // dgvReportPreview
            // 
            this.dgvReportPreview.AllowUserToAddRows = false;
            this.dgvReportPreview.AllowUserToDeleteRows = false;
            this.dgvReportPreview.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvReportPreview.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvReportPreview.Location = new System.Drawing.Point(7, 23);
            this.dgvReportPreview.Name = "dgvReportPreview";
            this.dgvReportPreview.ReadOnly = true;
            this.dgvReportPreview.Size = new System.Drawing.Size(755, 316);
            this.dgvReportPreview.TabIndex = 0;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btnLoadDelete);
            this.groupBox4.Controls.Add(this.cmbDelete);
            this.groupBox4.Controls.Add(this.btnDelete);
            this.groupBox4.Controls.Add(this.label13);
            this.groupBox4.Location = new System.Drawing.Point(9, 463);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox4.Size = new System.Drawing.Size(424, 108);
            this.groupBox4.TabIndex = 20;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Delete Material";
            // 
            // btnLoadDelete
            // 
            this.btnLoadDelete.Location = new System.Drawing.Point(184, 58);
            this.btnLoadDelete.Name = "btnLoadDelete";
            this.btnLoadDelete.Size = new System.Drawing.Size(133, 38);
            this.btnLoadDelete.TabIndex = 19;
            this.btnLoadDelete.Text = "Load Codes";
            this.btnLoadDelete.UseVisualStyleBackColor = true;
            this.btnLoadDelete.Click += new System.EventHandler(this.btnLoadDelete_Click);
            // 
            // cmbDelete
            // 
            this.cmbDelete.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDelete.FormattingEnabled = true;
            this.cmbDelete.Location = new System.Drawing.Point(89, 24);
            this.cmbDelete.Name = "cmbDelete";
            this.cmbDelete.Size = new System.Drawing.Size(319, 24);
            this.cmbDelete.TabIndex = 18;
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(20, 58);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(133, 38);
            this.btnDelete.TabIndex = 17;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(16, 27);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(45, 17);
            this.label13.TabIndex = 9;
            this.label13.Text = "Code:";
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.saveFileDialog1_FileOk);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1245, 584);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "MainForm";
            this.Text = "Store Manager";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReportPreview)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox txtPriceAdd;
        private System.Windows.Forms.TextBox txtQuantityAdd;
        private System.Windows.Forms.TextBox txtNameAdd;
        private System.Windows.Forms.TextBox txtCodeAdd;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.TextBox txtPriceUpdate;
        private System.Windows.Forms.TextBox txtQuantityUpdate;
        private System.Windows.Forms.TextBox txtNameUpdate;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cmbCodeUpdate;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnGetByPrice;
        private System.Windows.Forms.Button btnQuantityZero;
        private System.Windows.Forms.Button btnCompleteInventary;
        private System.Windows.Forms.Button btnSaveToFile;
        private System.Windows.Forms.DataGridView dgvReportPreview;
        private System.Windows.Forms.TextBox txtFilter;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnLoadMaterials;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btnLoadDelete;
        private System.Windows.Forms.ComboBox cmbDelete;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Label label13;
		private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Button btnLower;
        private System.Windows.Forms.Button btnGreater;
	}
}

