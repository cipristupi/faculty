﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StoreManager.Utils;

namespace StoreManager.Repository
{
	public class FileRepository : IFileRepository
	{
		private string pathFile;
		private List<Material> materials;
        private string p;
        /// <summary>
        /// Constructor to create an object and to load automaticaly data from file
        /// </summary>
		public FileRepository()
		{
			pathFile = System.IO.Directory.GetCurrentDirectory() + "\\Store.txt";
			materials = new List<Material>();
			LoadFromFile();
		}
        /// <summary>
        /// Secound constructor to initialize with a given path to file
        /// </summary>
        /// <param name="path">Directory path to text file</param>

        public FileRepository(string path)
        {
            pathFile = path;
            materials = new List<Material>();
            LoadFromFile();
        }

        /// <summary>
        /// Read all data from files and inserts material into in memory list
        /// </summary>
		private void LoadFromFile()
		{
			string line;
            try
            {
                StreamReader file = new StreamReader(pathFile);
                while ((line = file.ReadLine()) != null)
                {
                    materials.Add(Material.Read(line));
                }
                file.Close();
            }
            catch (Exception ex)
            {
                throw new CustomException("Loading file failed", ex);
            }
		}

        /// <summary>
        /// Read all data from files and inserts materials into a given material list send by referenta
        /// </summary>
        /// <param name="materialsList">Material list to file up</param>
        private void LoadFromFileToList(ref List<Material> materialsList)
        {
            string line;
            try
            {
                StreamReader file = new StreamReader(pathFile);
                while ((line = file.ReadLine()) != null)
                {
                    materialsList.Add(Material.Read(line));
                }
                file.Close();
            }
            catch (Exception ex)
            {
                throw new CustomException("Loading file failed", ex);
            }
        }

        /// <summary>
        /// Write all data from memory to txt file
        /// </summary>
		private void WriteToFile()
		{
            try
            {
                using (StreamWriter file = new StreamWriter(pathFile))
                {
                    foreach (Material material in materials)
                    {
                        file.WriteLine(material.ToString());
                    }
                    file.Flush();
                }
            }
            catch (Exception ex)
            {
                
                throw new CustomException("Write to file failed",ex);
            }
		}

        /// <summary>
        /// Add new material to existing one
        /// </summary>
        /// <param name="m">New material to be added</param>
		public void Add(Material m)
		{
			materials.Add(m);
			WriteToFile();
		}
        /// <summary>
        /// Update an existing material
        /// </summary>
        /// <param name="m">Material with updated values</param>
		public void Update(Material m)
		{
			for (int i = 0; i < materials.Count; i++)
			{
				if (materials[i].Code == m.Code)
				{
					materials[i].Name = m.Name;
					materials[i].Price = m.Price;
					materials[i].Quantity = m.Quantity;
				}
			}
			WriteToFile();
		}
        /// <summary>
        /// Deletes and material with a given code
        /// </summary>
        /// <param name="code">Code of material to be deleted</param>
		public void Delete(string code)
		{
			for (int i = 0; i < materials.Count; i++)
			{
				if (materials[i].Code == code)
				{
					materials.RemoveAt(i);
				}
			}
			WriteToFile();
		}
        /// <summary>
        /// Returns all materials from memory
        /// </summary>
        /// <returns>List of materials </returns>
		public List<Material> GetAll()
		{
			return materials;
		}
        /// <summary>
        /// Returns an material with a given code
        /// </summary>
        /// <param name="code">Code after we search </param>
        /// <returns>Returns an material if exists or null otherwise</returns>
        public Material GetByCode(string code)
        {
            for (int i = 0; i < materials.Count; i++)
            {
                if (materials[i].Code == code)
                {
                    return materials[i];
                }
            }
            return null;
        }
        /// <summary>
        /// Export function to save into a txt file
        /// </summary>
        /// <param name="materials">Materials list</param>
        /// <param name="filename">Filename for the file</param>
		public void ExportInFile(List<Material> materials, string filename)
		{
			//string data = string.Empty;
			//data += "Code|Name|Quantity|Price\n";
			//foreach (Material m in materials)
			//{
			//	data += m.ToString();
			//	data += "\n";
			//}
			//         return data;

            try
            {
                using (StreamWriter sw = new StreamWriter(filename))
                {
                    sw.WriteLine("Code|Name|Quantity|Price");
                    foreach (Material m in materials)
                    {
                        sw.WriteLine(m.ToString());
                    }
                    sw.Flush();
                    sw.Close();
                }
            }
            catch (Exception ex)
            {
                
                throw new CustomException("Failed to create file",ex);
            }
		}
	}
}
