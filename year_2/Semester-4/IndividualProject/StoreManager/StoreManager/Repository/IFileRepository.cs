﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StoreManager.Repository
{
    public interface IFileRepository
    {
        void Add(Material e);
        void Update(Material e);
        void Delete(string code);
        List<Material> GetAll();

        Material GetByCode(string code);
    }
}
