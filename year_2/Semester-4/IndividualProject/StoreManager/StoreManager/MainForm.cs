﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using StoreManager.Controllers;
using StoreManager.Repository;
using System.IO;

namespace StoreManager
{
	public partial class MainForm : Form
	{
		private MainController controller;
		public MainForm()
		{
			InitializeComponent();
			FileRepository repo = new FileRepository();
			controller = new MainController(repo);
		}
		private void btnSave_Click(object sender, EventArgs e)
		{
			float price;
			int quantity;
			if (string.IsNullOrWhiteSpace(txtCodeAdd.Text) || string.IsNullOrWhiteSpace(txtNameAdd.Text) || string.IsNullOrWhiteSpace(txtPriceAdd.Text) || string.IsNullOrWhiteSpace(txtQuantityAdd.Text))
			{
				MessageBox.Show("Fill please all fields.");
			}
			else
			{
				if (controller.GetByCode(txtCodeAdd.Text) != null)
				{
					MessageBox.Show("This code exists.");
				}
				else
				{
					if (!float.TryParse(txtPriceAdd.Text, out price))
					{
						MessageBox.Show("Price need to be a number");
						return;
					}
					if (!int.TryParse(txtQuantityAdd.Text, out quantity))
					{
						MessageBox.Show("Quantity need to be a number");
						return;
					}
                    if(quantity < 0  )
                    {
                        MessageBox.Show("Quantity must be a positive number");
                        return;
                    }
                    if( price < 0)
                    {
                        MessageBox.Show("Price must be a positive number");
                        return;
                    }
					Material material = new Material()
					{
						Code = txtCodeAdd.Text,
						Name = txtNameAdd.Text,
						Price = price,
						Quantity = quantity
					};
					controller.Add(material);
					MessageBox.Show("Successful added.");
					txtCodeAdd.Clear();
					txtNameAdd.Clear();
					txtPriceAdd.Clear();
					txtQuantityAdd.Clear();
					LoadAllData();
				}
			}
		}

		private void btnCompleteInventary_Click(object sender, EventArgs e)
		{
			LoadAllData();
		}

		private void cmbCodeUpdate_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (cmbCodeUpdate.Items.Count != 0)
			{
				Material m = controller.GetByCode(cmbCodeUpdate.SelectedValue.ToString());
				txtPriceUpdate.Text = m.Price.ToString();
				txtQuantityUpdate.Text = m.Quantity.ToString();
				txtNameUpdate.Text = m.Name.ToString();
			}
		}

		private void btnLoadMaterials_Click(object sender, EventArgs e)
		{
			cmbCodeUpdate.DataSource = controller.GetAllListCode();
		}

		private void btnLoadDelete_Click(object sender, EventArgs e)
		{
			cmbDelete.DataSource = controller.GetAllListCode();
		}

		private void btnDelete_Click(object sender, EventArgs e)
		{
			if (cmbDelete.Items.Count == 0)
				MessageBox.Show("Please load codes");
			else
			{
				controller.Delete(cmbDelete.SelectedValue.ToString());
				cmbDelete.DataSource = null;
				cmbDelete.DataSource = controller.GetAllListCode();
				LoadAllData();
			}
		}

		private void btnUpdate_Click(object sender, EventArgs e)
		{
			float price;
			int quantity;
			if (string.IsNullOrWhiteSpace(txtNameUpdate.Text) || string.IsNullOrWhiteSpace(txtPriceUpdate.Text) || string.IsNullOrWhiteSpace(txtQuantityUpdate.Text))
			{
				MessageBox.Show("Fill please all fields.");
			}
			else
			{

				if (!float.TryParse(txtPriceUpdate.Text, out price))
				{
					MessageBox.Show("Price need to be a number");
					return;
				}
				if (!int.TryParse(txtQuantityUpdate.Text, out quantity))
				{
					MessageBox.Show("Quantity need to be a number");
					return;
				}
                if (quantity < 0)
                {
                    MessageBox.Show("Quantity must be a positive number");
                    return;
                }
                if (price < 0)
                {
                    MessageBox.Show("Price must be a positive number");
                    return;
                }
				Material material = new Material()
				{
					Code = cmbCodeUpdate.SelectedValue.ToString(),
					Name = txtNameUpdate.Text,
					Price = price,
					Quantity = quantity
				};
				controller.Update(material);
				MessageBox.Show("Successful updated.");
				LoadAllData();
			}
		}


		private void LoadAllData()
		{
			List<Material> materials = controller.GetAll();
			dgvReportPreview.DataSource = null;
			dgvReportPreview.DataSource = materials;
		}

		private void btnQuantityZero_Click(object sender, EventArgs e)
		{
			List<Material> materials = controller.GetAllQuantityZero();
			dgvReportPreview.DataSource = null;
			dgvReportPreview.DataSource = materials;
		}

		private void btnGetByPrice_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty(txtFilter.Text))
			{
				MessageBox.Show("Please specify the price for filter");
			}
			else
			{
				float price;
				if (!float.TryParse(txtFilter.Text, out price))
				{
					MessageBox.Show("Price need to be a number");
					return;
				}
				List<Material> materials = controller.GetAllByPrice(price);
				dgvReportPreview.DataSource = null;
				dgvReportPreview.DataSource = materials;
			}
		}

		private void btnSaveToFile_Click(object sender, EventArgs e)
		{
			saveFileDialog1.DefaultExt = "txt";
			saveFileDialog1.Filter = "Text|*.txt";
			saveFileDialog1.FileName = "export";
			saveFileDialog1.ShowDialog();
		}

		private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
		{
			
			string name = saveFileDialog1.FileName;
			List<Material> materials = new List<Material>();
			for (int row = 0; row < dgvReportPreview.RowCount; row++)
			{
				materials.Add(new Material()
				{
					Code = dgvReportPreview.Rows[row].Cells[0].Value.ToString(),
					Name = dgvReportPreview.Rows[row].Cells[1].Value.ToString(),
					Price =float.Parse(dgvReportPreview.Rows[row].Cells[2].Value.ToString()),
					Quantity = int.Parse(dgvReportPreview.Rows[row].Cells[3].Value.ToString())
				});
			}
			controller.GetExport(materials, name);
		}

        private void btnLower_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtFilter.Text))
            {
                MessageBox.Show("Please specify the price for filter");
            }
            else
            {
                float price;
                if (!float.TryParse(txtFilter.Text, out price))
                {
                    MessageBox.Show("Price need to be a number");
                    return;
                }
                List<Material> materials = controller.GetAllByPriceLower(price);
                dgvReportPreview.DataSource = null;
                dgvReportPreview.DataSource = materials;
            }
        }

        private void btnGreater_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtFilter.Text))
            {
                MessageBox.Show("Please specify the price for filter");
            }
            else
            {
                float price;
                if (!float.TryParse(txtFilter.Text, out price))
                {
                    MessageBox.Show("Price need to be a number");
                    return;
                }
                List<Material> materials = controller.GetAllByPriceGreater(price);
                dgvReportPreview.DataSource = null;
                dgvReportPreview.DataSource = materials;
            }
        }
	}
}
