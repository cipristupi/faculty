﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using StoreManager.Repository;

namespace StoreManager.Controllers
{
    class MainController
    {
        private FileRepository repository;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="repo">Repository to use</param>
        public MainController(FileRepository repo)
        {
            repository = repo;
        }
        /// <summary>
        /// Add new material to existing ones
        /// </summary>
        /// <param name="material">New Material</param>
        public void Add(Material material)
        {
            repository.Add(material);
        }
        /// <summary>
        /// Update an existing material
        /// </summary>
        /// <param name="material">Material with updated fields</param>
        public void Update(Material material)
        {
            repository.Update(material);
        }
        /// <summary>
        /// Delete an material
        /// </summary>
        /// <param name="code">Code of material to be deleted</param>
        public void Delete(string code)
        {
            repository.Delete(code);
        }
        /// <summary>
        /// Returns all materials
        /// </summary>
        /// <returns>List of materials</returns>
        public List<Material> GetAll()
        {
            return repository.GetAll();
        }
        /// <summary>
        /// Get all materials having a given price
        /// </summary>
        /// <param name="price">Price to filter</param>
        /// <returns>List of materials</returns>
        public List<Material> GetAllByPrice(float price)
        {
            List<Material> r = repository.GetAll();
            return r.Where(p => p.Price == price).ToList();
        }
        /// <summary>
        /// Get all materials with quantity zero (0)
        /// </summary>
        /// <returns>List of materials</returns>
        public List<Material> GetAllQuantityZero()
        {
            List<Material> r = repository.GetAll();
            return r.Where(p => p.Quantity == 0).ToList();
        }
       
        /// <summary>
        /// Get an material by code
        /// </summary>
        /// <param name="code">Code of material</param>
        /// <returns>Material if exists or null otherwise</returns>
        public Material GetByCode(string code)
        {
            List<Material> r = repository.GetAll();
            for(int i=0;i<r.Count;i++)
                if (r[i].Code == code)
                    return r[i];
            return null;
        }
        /// <summary>
        /// Get all codes from existing materials
        /// </summary>
        /// <returns>List of string represent materials codes</returns>
		public List<string> GetAllListCode()
		{
			List<string> codes = new List<string>();
			List<Material> r = repository.GetAll();
			for (int i = 0; i < r.Count; i++)
				codes.Add(r[i].Code);
			return codes;
		}
        /// <summary>
        /// Export data to txt file
        /// </summary>
        /// <param name="dataSource">List of materials</param>
        /// <param name="filename">Filename of the txt file</param>
		public void GetExport(List<Material> dataSource,string filename)
		{
			repository.ExportInFile(dataSource,filename);
		}
        /// <summary>
        /// Get all materials with a price greater than a given value
        /// </summary>
        /// <param name="price">Price to be greater</param>
        /// <returns>List of materials</returns>
        public List<Material> GetAllByPriceGreater(float price)
        {
            List<Material> r = repository.GetAll();
            return r.Where(p => p.Price > price).ToList();
        }
        /// <summary>
        /// Get all materials with a price lower than a given value
        /// </summary>
        /// <param name="price">Price to be lower</param>
        /// <returns>List of materials</returns>
        public List<Material> GetAllByPriceLower(float price)
        {
            List<Material> r = repository.GetAll();
            return r.Where(p => p.Price < price).ToList();
        }
	}
}
