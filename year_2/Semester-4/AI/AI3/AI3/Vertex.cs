﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AI3
{
    public class Vertex
    {
        public int Name { get; set; }
        public List<int> Edges { get; set; }
        public Vertex()
        {
            Edges = new List<int>();
        }
    }
}
