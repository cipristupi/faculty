﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AI3
{
    class HeuristicSearch
    {
        private int edgeNumber, vertexNumber;
        private int[] solution;
        private int[,] G;

        public HeuristicSearch(int[,] g, int edgeNumber, int vertexNumber)
        {
            G = g;
            this.edgeNumber = edgeNumber;
            this.vertexNumber = vertexNumber;
            solution = new int[edgeNumber];
            for (int i = 0; i < edgeNumber; i++)
                solution[i] = -1;
        }

        public void StartSearch()
        {
            Queue<Vertex> vertexesQueue = InitVertexes();
            Vertex v;
            while (vertexesQueue.Count != 0)//we take all vertexes in decreasing order by incidence degree
            {
                v = vertexesQueue.Dequeue();
                foreach (int edge in v.Edges)
                {
                    if (solution[edge] == -1)
                    {
                        for (int c = 0; c < edgeNumber; c++)//try to add colors to the current edge
                        {
                            solution[edge] = c;
                            if (ValidColor(edge))//if we found a valid color we stop 
                            {
                                break;
                            }
                        }
                    }
                }
            }
            PrintOutput();
        }

        bool ValidColor(int k)
        {
            List<int> edges = new List<int>();
            if (k < edgeNumber)
            {
                for (int i = 0; i < vertexNumber; i++)//go throw each vertex 
                {
                    if (G[k, i] == 1)//check if edge k is adjective with vertex i
                    {
                        for (int j = 0; j < edgeNumber; j++)//we go throw each edge adjactive in the vertex i
                        {
                            if (G[j, i] == 1 && k != j)
                                edges.Add(j);
                        }
                    }
                }
                foreach (int edge in edges)
                {
                    if (solution[k] == solution[edge])//check if have the same color in adjactive edges 
                        return false;
                }
            }
            return true;
        }

        public Queue<Vertex> InitVertexes()
        {
            Queue<Vertex> vertexesQueue = new Queue<Vertex>();
            List<Vertex> vertexesList = new List<Vertex>();
            for (int i = 0; i < vertexNumber; i++)
            {
                Vertex v = new Vertex();
                v.Name = i;
                for (int j = 0; j < edgeNumber; j++)
                {
                    if (G[j, i] == 1)
                    {
                        v.Edges.Add(j);
                    }
                }
                vertexesList.Add(v);
            }
            vertexesList = vertexesList.OrderByDescending(x => x.Edges.Count).ToList();
            foreach (Vertex v in vertexesList)
            {
                vertexesQueue.Enqueue(v);
            }
            return vertexesQueue;
        }


        void PrintOutput()
        {
            string uriPath = new Uri(System.IO.Directory.GetCurrentDirectory() + "\\result.txt").LocalPath;
            for (int i = 0; i < edgeNumber; i++)
            {
                Console.Write("edge[{0}]={1}  ", i, solution[i]);
            }
            Console.WriteLine();
            Console.WriteLine("Minimum number of colors is :  {0}", solution.Distinct().ToArray().Length);

        }
    }
}
