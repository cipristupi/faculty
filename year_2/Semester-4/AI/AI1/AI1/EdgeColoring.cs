﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AI1
{
    public class EdgeColoring
    {
        private int[,] g;
        //private int?[,] c;
        private int[] solution;
        private int n;//vertex number
        private int e;//edge number
        private int[] minimumSolution;
        int minimumColor = int.MaxValue;
        public EdgeColoring(int[,] G, int verticesNo, int edges)
        {
            g = G;
            n = verticesNo;
            e = edges;
            minimumSolution = new int[e + 1];
            InitSolutionVector(e);
        }

        private void InitSolutionVector(int e)
        {
            solution = new int[e + 1];
            for (int i = 0; i < e + 1; i++)
                solution[i] = -1;
        }

        void PrintOutput(int k)
        {
            string uriPath = new Uri(System.IO.Directory.GetCurrentDirectory() + "\\result.txt").LocalPath;
           /* using (StreamWriter file = new StreamWriter(uriPath, true))
            {*/
                for (int i = 0; i < e; i++)
                {
                    Console.Write("e[{0}]={1}  ", i, solution[i]);
                    //file.Write("e[{0}]={1}  ", i, solution[i]);
                }
                Console.WriteLine();
            //    file.WriteLine();
            //    file.WriteLine();
            //    file.Flush();
            //}
            if (solution.Max() < minimumColor)
            {
                minimumColor = solution.Max();
                Array.Copy(solution, minimumSolution, solution.Length);
            }
        }
        public void StartBT(int k)
        {
            DateTime startTime = DateTime.Now;
            BackTracking(k);
            Console.WriteLine("Minimum color is {0}", minimumColor+1);
            for (int i = 0; i < e; i++)
            {
                Console.Write("e[{0}]={1}  ", i, minimumSolution[i]);
            }
            Console.WriteLine();
            DateTime endTime = DateTime.Now;
            Console.WriteLine("Time: {0}", endTime - startTime);

        }
        public void BackTracking(int k)
        {
            int i;
            for (i = 0; i < e; i++)
            {
                solution[k] = i;
                if (ValidGraph(k))
                    if (k == e)
                    {
                        PrintOutput(k);
                        return;
                    }
                    else
                        BackTracking(k + 1);
            }
        }

        bool ValidGraph(int k)
        {
            List<int> edges = new List<int>();
            if (k < e)
            {
                for (int i = 0; i < n; i++)//go throw each vertex 
                {
                    if (g[k, i] == 1)//check if edge k is adjective with vertex i
                    {
                        for (int j = 0; j < e; j++)//we go throw each edge adjactive in the vertex i
                        {
                            if (g[j, i] == 1 && k != j)
                                edges.Add(j);
                        }
                    }
                }
                foreach (int edge in edges)
                {
                    if (solution[k] == solution[edge])//check if have the same color in adjactive edges 
                        return false;
                }
            }
            return true;
        }


    }
}
