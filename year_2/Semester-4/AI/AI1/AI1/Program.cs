﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AI1
{
	class Program
	{
		
		static void Main(string[] args)
		{
			int n,root,e;
            //Console.Write("Number of vertexes:");
            //n = int.Parse(Console.ReadLine());
            //Console.Write("Number of edges:");
            //e = int.Parse(Console.ReadLine());
            //Console.Write("Starting vertex:");
            //root = int.Parse(Console.ReadLine());
            n = 4;// 3;//5;
            e = 6;// 3;//4;
           // n = 5;// 3;//5;
            //e = 4;// 3;//4;
            root = 0;
			int[,] g = ReadMatrix(GetPath(n), n,e);
			EdgeColoring ec = new EdgeColoring(g, n, e);
			//ec.bt(root);
            ec.StartBT(root);
			Console.ReadLine();
		}

		private static string GetPath(int n)
		{
			string uriPath = System.IO.Directory.GetCurrentDirectory()+"\\" + n + ".txt";
			return new Uri(uriPath).LocalPath;
		}


		static private int[,] ReadMatrix(string path, int n,int m)
		{
			int[,] g = new int[m, n];
			String input = File.ReadAllText(path);
			int i = 0, j = 0;
			foreach (var row in input.Split('\n'))
			{
				j = 0;
				foreach (var col in row.Trim().Split(' '))
				{
					g[i, j] = int.Parse(col.Trim());
					j++;
				}
				i++;
			}
			return g;
		}
	}
	
}
