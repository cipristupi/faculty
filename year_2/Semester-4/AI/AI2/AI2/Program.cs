﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AI2
{
	class Program
	{
		static void Main(string[] args)
		{
			int numberOfEdge, numberOfVertex;
			numberOfVertex = 4;// 3;//5;
			numberOfEdge =6;// 3;//4;
			// vertexNumber = 5;// 3;//5;
			//edgeNumber = 4;// 3;//4;
			int[,] g = ReadMatrix(GetPath(numberOfVertex), numberOfVertex, numberOfEdge);
			GeneticAlgorithm ga = new GeneticAlgorithm();
			int populationSize = 20;
			int numberOfGenerations = 100;
			double crossOverProbability = 0.9;
			double mutationProbability = 0.1;
			DateTime start = DateTime.Now;
			ga.StartGeneticAlgorithm(populationSize, numberOfGenerations, crossOverProbability, mutationProbability, g, numberOfVertex, numberOfEdge);
			DateTime end = DateTime.Now;
			Console.WriteLine("Duration: {0}", end - start); 
            Console.ReadLine();
		}
		private static string GetPath(int n)
		{
			string uriPath = System.IO.Directory.GetCurrentDirectory() + "\\" + n + ".txt";
			return new Uri(uriPath).LocalPath;
		}


		static private int[,] ReadMatrix(string path, int n, int m)
		{
			int[,] g = new int[m, n];
			String input = File.ReadAllText(path);
			int i = 0, j = 0;
			foreach (var row in input.Split('\n'))
			{
				j = 0;
				foreach (var col in row.Trim().Split(' '))
				{
					g[i, j] = int.Parse(col.Trim());
					j++;
				}
				i++;
			}
			return g;
		}
	}
}
