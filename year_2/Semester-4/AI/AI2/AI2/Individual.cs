﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AI2
{
	public class Individual
	{
		private int edgeNumber;
		private int vertexNumber;
		public int Fitness { get; set; }//Number of bad vertexes [Number of vertexes which have the same color for adjective edges]
		public int[] Chromosome { get; set; }

		public Individual(int e, int v, int[,] G)
		{
			edgeNumber = e;
			vertexNumber = v;
			Chromosome = new int[e];
			InitializaChromosome();
			ComputeFitness(G);
		}


		/// <summary>
		/// Fitness represent number of verteces which have at least 2 edges which have the same color
		/// </summary>
		/// <param name="G"></param>
		public void ComputeFitness(int[,] G)
		{
			int f = 0;
			bool ok = true;
			Fitness = 0;
			List<int> edgeInVertex = new List<int>();
			for (int i = 0; i < vertexNumber; i++)//got throw each vertex.
			{
				edgeInVertex.Clear();
				ok = true;
				for (int j = 0; j < edgeNumber; j++)
				{
					if (G[j, i] == 1)
						edgeInVertex.Add(j);//add edge number 
				}
				for (int e = 0; e < edgeInVertex.Count; e++)
				{
					for (int en = e + 1; en < edgeInVertex.Count; en++)
					{
						if (Chromosome[edgeInVertex[e]] == Chromosome[edgeInVertex[en]])
						{
							ok = false;
							break;
						}
					}
				}
				if (!ok)
				{
					f += 1;
				}
			}
			if (f > (int)vertexNumber / 2)
			{
				Fitness += 2;
			}
			Fitness += f;
			var distinctArray = Chromosome.Distinct().ToArray();
			Fitness += distinctArray.Length;//Numarul de culori disticte.
			//Fitness += distinctArray.Max();
        }

		/// <summary>
		/// Generate random color for each edge
		/// </summary>
		public void InitializaChromosome()
		{
			Random random = new Random(Guid.NewGuid().GetHashCode());
			for (int i = 0; i < Chromosome.Length; i++)
			{
				Chromosome[i] = random.Next(0, edgeNumber);
			}
		}


		/// <summary>
		/// Randomly change color for an edge if an edge have other adjactive edges with the same color
		/// </summary>
		/// <param name="G"></param>
		/// <param name="mutationProbability"></param>
		public void Mutation(int[,] G, double mutationProbability)
		{
			Random random = new Random(Guid.NewGuid().GetHashCode());
			for (int i = 0; i < edgeNumber; i++)
			{
				random = new Random(Guid.NewGuid().GetHashCode());
				double localMutationProbability = random.NextDouble();
				if (localMutationProbability < mutationProbability)
				{
					/*if (CheckColorExists(i, G))
					{*/
						Chromosome[i] = random.Next(0, edgeNumber);
					//}
				}
			}
		}

		/// <summary>
		/// Check if an edge have unique color
		/// </summary>
		/// <param name="k"></param>
		/// <param name="G"></param>
		/// <returns></returns>
		private bool CheckColorExists(int k, int[,] G)
		{
			List<int> edges = new List<int>();
			if (k < edgeNumber)
			{
				for (int i = 0; i < vertexNumber; i++)//go throw each vertex 
				{
					if (G[k, i] == 1)//check if edge k is adjective with vertex i
					{
						for (int j = 0; j < edgeNumber; j++)//we go throw each edge adjactive in the vertex i
						{
							if (G[j, i] == 1 && k != j)
								edges.Add(j);
						}
					}
				}
				foreach (int edge in edges)
				{
					if (Chromosome[k] == Chromosome[edge])//check if have the same color in adjactive edges 
						return true;
				}
			}
			return false;
		}


	}
}
