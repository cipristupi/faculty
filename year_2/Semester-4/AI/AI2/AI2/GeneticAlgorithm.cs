﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AI2
{
	public class GeneticAlgorithm
	{
		private List<Individual> population;
		private List<Individual> originalPopulation;
		public void StartGeneticAlgorithm(int populationSize, int numberOfGenerations, int crossOver, int crossOverPoint, int[,] G, int numberOfVertex, int numberOfEdge)
		{
			population = new List<Individual>();
			originalPopulation = new List<Individual>();
			Individual initialIndividual;
			Individual individual1, individual2;
			for (int i = 0; i < populationSize; i++)
			{
				population.Add(new Individual(numberOfEdge, numberOfVertex, G));
			}
			//Test(populationSize, numerOfGenerations, crossOverPoint, G, numberOfVertex, numberOfEdge);
			SortPopulation();
			PrintChromosome(numberOfEdge);
			for (int i = 0; i < numberOfGenerations; i++)
			{
				for (int p = 0; p < populationSize; p += 2)
				{

				}
			}
		}

		public void StartGeneticAlgorithm(int populationSize, int numberOfGenerations, double crossOverProbability, double mutationProbability, int[,] G, int numberOfVertex, int numberOfEdge)
		{
			population = new List<Individual>();
			Individual individual1, individual2;
			double localCrossOverProbability, localMutationProbability;
			Random random;
			List<Individual> posibleSolutions = new List<Individual>();
			Individual child = null;
			for (int i = 0; i < populationSize; i++)
			{
				population.Add(new Individual(numberOfEdge, numberOfVertex, G));
			}
			originalPopulation = new List<Individual>(population);
			SortPopulation();
			PrintChromosome(numberOfEdge);
			for (int i = 0; i < numberOfGenerations; i++)
			{
				for (int p = 0; p < populationSize; p += 2)
				{
					random = new Random(Guid.NewGuid().GetHashCode());
					individual1 = GetRandomIndividual();
					individual2 = GetRandomIndividual();
					localCrossOverProbability = random.NextDouble();
					if (localCrossOverProbability < crossOverProbability)
					{
						//OneCutPointCrossOver(ref individual1, ref individual2, numberOfEdge);//It's ok to change the parent or i need to return a child?
						child = OneCutPointCrossOverWithChild(individual1, individual2, numberOfEdge, numberOfVertex, G);
					}
					individual1.Mutation(G,mutationProbability);
					individual1.ComputeFitness(G);
					individual2.Mutation(G,mutationProbability);
					individual2.ComputeFitness(G);
					//if (individual1.Fitness < population[population.Count - 1].Fitness)
					//{
					//	population[population.Count - 1] = individual1;//this replacement it's ok?
					//	SortPopulation();
					//}
					//if (individual1.Fitness < population[population.Count - 1].Fitness)
					//{
					//	population[population.Count - 1] = individual2;
					//	SortPopulation();
					//}
					//if (individual1.Fitness == 0)
					//	posibleSolutions.Add(individual1);
					//if (individual2.Fitness == 0)
					//	posibleSolutions.Add(individual2);
					if (child != null)
					{
						//if (child.Fitness < individual1.Fitness)
						//{
						//	individual1 = child;
						//}
						//if (child.Fitness < individual2.Fitness)
						//{
						//	individual2 = child;
						//}
						if (child.Fitness < population[population.Count - 1].Fitness)
						{
							population[population.Count - 1] = child;
							SortPopulation();
                        }
                    }
					SortPopulation();
				}
				Console.WriteLine("Generation {0}", i);
				PrintChromosome(numberOfEdge);
				PrintToFileChromosome(i, numberOfEdge);
				PrintDifference(i,numberOfEdge);
			}
		}
		private Individual GetRandomIndividual()//Random choosen individual
		{
			Random random = new Random(Guid.NewGuid().GetHashCode());
			return population[random.Next(0, population.Count)];
		}

		private void OneCutPointCrossOver(ref Individual individual1, ref Individual individual2, int numberOfEdge)//Uniform crossOver
		{
			Random random = new Random(Guid.NewGuid().GetHashCode());
			int crossOverPoint = random.Next(0, numberOfEdge);
			for (int i = crossOverPoint; i < numberOfEdge; i++)
			{
				individual1.Chromosome[i] = individual2.Chromosome[i];
				individual2.Chromosome[i] = individual1.Chromosome[i];
			}
		}

		private Individual OneCutPointCrossOverWithChild(Individual individual1, Individual individual2, int numberOfEdge, int numberOfVertex,int[,] G)//Uniform crossOver
		{
			Random random = new Random(Guid.NewGuid().GetHashCode());
			int crossOverPoint = random.Next(0, numberOfEdge);
			Individual child = new Individual(numberOfEdge, numberOfVertex, G);
			for (int i = 0; i < crossOverPoint; i++)
			{
				child.Chromosome[i] = individual1.Chromosome[i];
            }
			for (int i = crossOverPoint; i < numberOfEdge; i++)
			{
				child.Chromosome[i] = individual2.Chromosome[i];
				//individual2.Chromosome[i] = individual1.Chromosome[i];
			}
			child.ComputeFitness(G);
			return child;
		}


		private void SortPopulation()
		{
			population = population.OrderBy(x => x.Fitness).ToList();
		}

		private void PrintChromosome(int numberOfEdge)
		{
			foreach (Individual individual in population)
			{
				Console.Write("Fitness={0} Chromosome: ", individual.Fitness);
				for (int i = 0; i < numberOfEdge; i++)
				{
					Console.Write("{0} ", individual.Chromosome[i]);
				}
				Console.WriteLine();
			}
		}

		private void Test(int populationSize, int numerOfGenerations, int crossOverPoint, int[,] G, int numberOfVertex, int numberOfEdge)
		{
			Individual initialIndividual;
			initialIndividual = new Individual(numberOfEdge, numberOfVertex, G);
			//initialIndividual.Chromosome[0] = 0;
			//initialIndividual.Chromosome[1] = 1;
			//initialIndividual.Chromosome[2] = 0;
			//initialIndividual.Chromosome[3] = 1;
			//initialIndividual.Chromosome[4] = 2;
			//initialIndividual.Chromosome[5] = 2;
			initialIndividual.Chromosome[0] = 2;
			initialIndividual.Chromosome[1] = 2;
			initialIndividual.Chromosome[2] = 2;
			//initialIndividual.Mutation(G);
			//PrintChromosome(numberOfEdge);
		}

		private void PrintToFileChromosome(int generationNumber,int numberOfEdge)
		{
			string uriPath = new Uri(System.IO.Directory.GetCurrentDirectory() + "\\result.txt").LocalPath;
			using (StreamWriter file = new StreamWriter(uriPath, true))
			{
				file.WriteLine("Generation number: {0}", generationNumber);
				foreach (Individual individual in population)
				{
					file.Write("Fitness={0} Chromosome:", individual.Fitness);
					for (int i = 0; i < numberOfEdge; i++)
					{
						file.Write("{0} ", individual.Chromosome[i]);
					}
					file.WriteLine();
				}
				file.Flush();
			}
		}


		/// <summary>
		/// Print difference between origin population and final population
		/// </summary>
		private void PrintDifference(int generationNumber,int numberOfEdge)
		{
			var difference1 = population.Except(originalPopulation);
			var difference2 = originalPopulation.Except(population);

			string uriPath = new Uri(System.IO.Directory.GetCurrentDirectory() + "\\resultCompare.txt").LocalPath;
			using (StreamWriter file = new StreamWriter(uriPath, true))
			{
				file.WriteLine("Generation number: {0}", generationNumber);
				file.WriteLine("Difference 1 population except original population");
				foreach (Individual individual in difference1)
				{
					file.Write("Fitness={0} Chromosome:", individual.Fitness);
					for (int i = 0; i < numberOfEdge; i++)
					{
						file.Write("{0} ", individual.Chromosome[i]);
					}
					file.WriteLine();
				}

				file.WriteLine("Difference 2 original population except population");
				foreach (Individual individual in difference2)
				{
					file.Write("Fitness={0} Chromosome:", individual.Fitness);
					for (int i = 0; i < numberOfEdge; i++)
					{
						file.Write("{0} ", individual.Chromosome[i]);
					}
					file.WriteLine();
				}
				file.WriteLine("=======================================================================");

				file.Flush();
			}
		}
	}
}
