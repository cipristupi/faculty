﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static int[] sol;
        static int n, e, root;
        static int[,] G;
        static int minimumColor =int.MaxValue;
        static void Main(string[] args)
        {
           
            //Console.Write("Number of vertexes:");
            //n = int.Parse(Console.ReadLine());
            //Console.Write("Number of edges:");
            //e = int.Parse(Console.ReadLine());
            n = 4;// 3;//5;
            e = 6;// 3;//4;
            //Console.Write("Starting vertex:");
            //root = int.Parse(Console.ReadLine());
            initSolution(e);
            G = ReadMatrix(GetPath(n), n, e);
            //for (int i = 0; i < e; i++)
            //{
            //GetAdjective(0);
            //}
            back(0);
            Console.WriteLine("Minimum color is {0}", minimumColor);
            Console.ReadLine();
        }
        private static void initSolution(int e)
        {
            sol = new int[e+1];
            for (int i = 0; i < e+1; i++)
                sol[i] = -1;
        }

        private static string GetPath(int n)
        {
            string uriPath = System.IO.Directory.GetCurrentDirectory() + "\\" + n + ".txt";
            return new Uri(uriPath).LocalPath;
        }


        static private int[,] ReadMatrix(string path, int n, int m)
        {
            int[,] g = new int[m, n];
            String input = File.ReadAllText(path);
            int i = 0, j = 0;
            foreach (var row in input.Split('\n'))
            {
                j = 0;
                foreach (var col in row.Trim().Split(' '))
                {
                    g[i, j] = int.Parse(col.Trim());
                    j++;
                }
                i++;
            }
            for (int a = 0; a < m; a++)
            {
                for (int b = 0; b < n; b++)
                    Console.Write("{0} ", g[a, b]);
                Console.WriteLine();
            }
            return g;
        }

        //static void afisare(int k)
        //{
        //    int i;
        //    for (i = 1; i <= n; i++)
        //        Console.Write("{0}  ", sol[i]);
        //    Console.WriteLine();
        //}
        //static void afisare(int k)
        //{
        //    for (int i = 0; i < e; i++)
        //        Console.Write("e[{0}]={1}  ",i, sol[i]);
        //    Console.WriteLine();
        //}

        static void afisare(int k)
        {
            //string uriPath = new Uri(System.IO.Directory.GetCurrentDirectory() + "\\result.txt").LocalPath;
            //using (StreamWriter file = new StreamWriter(uriPath, true))
            //{
            for (int i = 0; i < e; i++)
            {
                Console.Write("e[{0}]={1}  ", i, sol[i]);
                // file.Write("e[{0}]={1}  ", i, sol[i]);
            }
            Console.WriteLine();
            //file.WriteLine();
            //file.WriteLine();
            //file.Flush();
            //}
            if (sol.Max() < minimumColor)
            {
                minimumColor = sol.Max();
            }
        }

        static bool valid(int k)
        {
            int i;
            for (i = 1; i < k; i++)
                if (sol[i] == sol[k])
                    return false;
            return true;
        }
        static void GetAdjective(int k)
        {
            for (int i = 0; i < n; i++)//vertex
            {
                //Console.WriteLine("Edge {0} Vertex: {1}", k, i);
                if (G[k, i] == 1)//check if edge k is adjactive with vertex i
                {
                    //Console.WriteLine("Edge {0} Vertex: {1} Adjective", k, i);
                    Console.WriteLine("Vertex {0}", i);
                    for (int j = 0; j < e; j++)//we go throw each edge adjactive in the vertex i
                    {
                        if (G[i, j] == 1)
                            Console.WriteLine("Edge {0} ", j);
                    }
                }
                Console.WriteLine("============================");
            }
        }

        static bool validGraph(int k)
        {
            if (k < e)
            {
                for (int i = 0; i < n; i++)//vertex
                {
                    if (G[k, i] == 1 && i != k)//check if edge k is adjactive with vertex i
                    {
                        for (int j = 0; j < e; j++)//we go throw each edge adjactive in the vertex i
                        {
                            if (k != j)//exclude the 
                            {
                                if (G[i, j] == 1 && sol[k] == sol[j])
                                    return false;
                            }
                        }
                    }
                }
            }
            return true;
        }

        static bool validGraph2(int k)
        {
            List<int> edges = new List<int>();
            if (k < e)
            {
                for (int i = 0; i < n; i++)//vertex
                {
                    if (G[k, i] == 1)// && i != k)//check if edge k is adjactive with vertex i
                    {
                        for (int j = 0; j < e; j++)//we go throw each edge adjactive in the vertex i
                        {
                            if (G[j, i] == 1 && k != j)
                                edges.Add(j);
                        }
                    }
                }
                foreach (int edge in edges)
                {
                    if (sol[k] == sol[edge])// && edge!=k)
                        return false;
                }
            }
            return true;
        }

        static void back(int k)
        {
            int i;
            for (i = 0; i < e; i++)
            {
                sol[k] = i;
                if (validGraph2(k))
                    if (k == e)
                    {
                        afisare(k);
                        return;
                    }
                    else
                        back(k + 1);
            }
        }

    }
}
