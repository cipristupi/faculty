﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laboratory1.DataLayer
{
	public class User : BaseObject
	{
		public string UserName { get; set; }
		public string Password { get; set; }
		public int RoleId { get; set; }

		public static User Read(IDataReader reader)
		{
			User user = new User()
			{
				ID = reader.GetInt32(0),
				UserName =reader.GetString(1),
				Password = reader.GetString(2),
				RoleId = reader.GetInt32(3)
			};
			return user;
		}
	}
}
