﻿using Laboratory1.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Laboratory1.Entities
{
	public class Role : BaseObject
	{
		public string Name { get; set; }

		public static BaseObject Read(SqlDataReader reader)
		{
			Role role = new Role
			{
				ID = reader.GetInt32(0),
				Name = reader.GetString(1)
			};
			return role;
		}
	}
}
