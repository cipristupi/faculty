﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laboratory1.DataLayer
{
	interface IBaseOperations
	{
		void Add(BaseObject e);
		void Update(BaseObject e);
		List<BaseObject> GetAll();
		void Delete(int id);

		BaseObject GetById(int id);
	}
}
