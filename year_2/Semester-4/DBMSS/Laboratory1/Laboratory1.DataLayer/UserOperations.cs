﻿using Laboratory1.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laboratory1.DataLayer
{
	public class UserOperations : BaseOperations, IBaseOperations
	{
		private SqlCommand cmd;
		private SqlConnection conn;
		private SqlDataReader reader;
		public void Add(BaseObject e)
		{
			User u = (User)e;
			conn = new SqlConnection(ConnectionString);
			try
			{
				conn.Open();
				cmd = new SqlCommand("INSERT INTO [dbo].[Users] ([Username],[Password],[RolesId]) VALUES (@username,@password,@roleId)", conn);
				cmd.Parameters.Add(new SqlParameter("@username", u.UserName));
				cmd.Parameters.Add(new SqlParameter("@password", u.Password));
				cmd.Parameters.Add(new SqlParameter("@roleId", u.RoleId));
				cmd.ExecuteNonQuery();
				conn.Close();
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public void Delete(int id)
		{
			conn = new SqlConnection(ConnectionString);
			try
			{
				conn.Open();
				cmd = new SqlCommand("DELETE FROM [dbo].[Users] WHERE [Id] = @userId", conn);
				cmd.Parameters.Add(new SqlParameter("@userId", id));
				cmd.ExecuteNonQuery();
				conn.Close();
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public List<BaseObject> GetAll()
		{
			List<BaseObject> userList = new List<BaseObject>();
			conn = new SqlConnection(ConnectionString);
			try
			{
				conn.Open();
				cmd = new SqlCommand("SELECT * FROM [dbo].[Users] ", conn);
				reader = cmd.ExecuteReader();
				while (reader.Read())
				{
					userList.Add(User.Read(reader));
				}
				conn.Close();
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return userList;
		}

		public void Update(BaseObject e)
		{
			User u = (User)e;
			conn = new SqlConnection(ConnectionString);
			try
			{
				conn.Open();
				cmd = new SqlCommand("UPDATE Users SET Username =@username, Password =@password, RolesId =@roleId WHERE Id =@userId", conn);
				cmd.Parameters.Add(new SqlParameter("@username", u.UserName));
				cmd.Parameters.Add(new SqlParameter("@password", u.Password));
				cmd.Parameters.Add(new SqlParameter("@roleId", u.RoleId));
				cmd.Parameters.Add(new SqlParameter("@userId", u.ID));
				cmd.ExecuteNonQuery();
				conn.Close();
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public List<BaseObject> GetAllByRole(int roleId)
		{
			List<BaseObject> userList = new List<BaseObject>();
			conn = new SqlConnection(ConnectionString);
			try
			{
				conn.Open();
				cmd = new SqlCommand("SELECT * FROM [dbo].[Users] WHERE RolesId = @roleId", conn);
				cmd.Parameters.Add(new SqlParameter("@roleId", roleId));
				reader = cmd.ExecuteReader();
				while (reader.Read())
				{
					userList.Add(User.Read(reader));
				}
				conn.Close();
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return userList;
		}

		public BaseObject GetById(int id)
		{
			BaseObject b;
			conn = new SqlConnection(ConnectionString);
			try
			{
				conn.Open();
				cmd = new SqlCommand("SELECT * FROM [dbo].[Users] WHERE Id = @id", conn);
				cmd.Parameters.Add(new SqlParameter("@id", id));
				reader = cmd.ExecuteReader();
				reader.Read();
				b = User.Read(reader);
				conn.Close();
				return b;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
	}
}
