﻿using Laboratory1.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laboratory1.DataLayer
{
	public class RoleOperations :BaseOperations, IBaseOperations
	{
		private SqlCommand cmd;
		private SqlConnection conn;
		private SqlDataReader reader;
		public void Add(BaseObject e)
		{
			Role role = (Role)e;
			conn = new SqlConnection(ConnectionString);
			try
			{
				conn.Open();
				cmd = new SqlCommand("INSERT INTO [dbo].[Roles] ([Name]) VALUES (@name)", conn);
				cmd.Parameters.Add(new SqlParameter("@name", role.Name));
				cmd.ExecuteNonQuery();
				conn.Close();
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public void Delete(int id)
		{
			conn = new SqlConnection(ConnectionString);
			try
			{
				conn.Open();
				cmd = new SqlCommand("DELETE FROM [dbo].[Roles] WHERE [Id] = @roleId", conn);
				cmd.Parameters.Add(new SqlParameter("@roleId", id));
				cmd.ExecuteNonQuery();
				conn.Close();
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public List<BaseObject> GetAll()
		{
			List<BaseObject> roles = new List<BaseObject>();
			conn = new SqlConnection(ConnectionString);
			try
			{
				conn.Open();
				cmd = new SqlCommand("SELECT * FROM [dbo].[Roles] ", conn);
				reader = cmd.ExecuteReader();
				while (reader.Read())
				{
					roles.Add(Role.Read(reader));
				}
				conn.Close();
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return roles;
		}

		public BaseObject GetById(int id)
		{
			BaseObject b;
			conn = new SqlConnection(ConnectionString);
			try
			{
				conn.Open();
				cmd = new SqlCommand("SELECT * FROM [dbo].[Roles] WHERE Id = @id", conn);
				cmd.Parameters.Add(new SqlParameter("@id", id));
				reader = cmd.ExecuteReader();
				reader.Read();
				b = Role.Read(reader);
				conn.Close();
				return b;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public void Update(BaseObject e)
		{
			Role role = (Role)e;
			conn = new SqlConnection(ConnectionString);
			try
			{
				conn.Open();
				cmd = new SqlCommand("UPDATE Roles SET Name =@name WHERE Id = @roleId", conn);
				cmd.Parameters.Add(new SqlParameter("@name", role.Name));
				cmd.Parameters.Add(new SqlParameter("@roleId", role.ID));
				cmd.ExecuteNonQuery();
				conn.Close();
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
	}
}
