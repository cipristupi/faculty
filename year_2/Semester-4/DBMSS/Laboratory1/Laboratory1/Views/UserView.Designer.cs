﻿namespace Laboratory1.Views
{
	partial class UserView
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.bttnLoadRoleUpdateUser = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.cboNewRole = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.cboUserName = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.btnUpdateData = new System.Windows.Forms.Button();
            this.txtUpdatePassword = new System.Windows.Forms.TextBox();
            this.txtUpdateUsername = new System.Windows.Forms.TextBox();
            this.cboRoleupdate = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnLoadRoleUsersByRole = new System.Windows.Forms.Button();
            this.cboGetUserbyRole = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.dgdUserByRoles = new System.Windows.Forms.DataGridView();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lstRoles = new System.Windows.Forms.ListBox();
            this.bttnLoadUsersByRole = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnLoadRolesNewUser = new System.Windows.Forms.Button();
            this.bttnSave = new System.Windows.Forms.Button();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.txtUsername = new System.Windows.Forms.TextBox();
            this.cboRole = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.txtRoleName = new System.Windows.Forms.TextBox();
            this.btnAddNewRole = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.btnLoadRoleNameUpdate = new System.Windows.Forms.Button();
            this.cboRoleUpdateName = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtRoleNewName = new System.Windows.Forms.TextBox();
            this.btnUpdateRoleName = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgdUserByRoles)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.bttnLoadRoleUpdateUser);
            this.groupBox4.Controls.Add(this.btnDelete);
            this.groupBox4.Controls.Add(this.cboNewRole);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.cboUserName);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Controls.Add(this.btnUpdateData);
            this.groupBox4.Controls.Add(this.txtUpdatePassword);
            this.groupBox4.Controls.Add(this.txtUpdateUsername);
            this.groupBox4.Controls.Add(this.cboRoleupdate);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(819, 403);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(409, 259);
            this.groupBox4.TabIndex = 7;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Update user data";
            // 
            // bttnLoadRoleUpdateUser
            // 
            this.bttnLoadRoleUpdateUser.Location = new System.Drawing.Point(280, 206);
            this.bttnLoadRoleUpdateUser.Name = "bttnLoadRoleUpdateUser";
            this.bttnLoadRoleUpdateUser.Size = new System.Drawing.Size(131, 35);
            this.bttnLoadRoleUpdateUser.TabIndex = 12;
            this.bttnLoadRoleUpdateUser.Text = "Load roles";
            this.bttnLoadRoleUpdateUser.UseVisualStyleBackColor = true;
            this.bttnLoadRoleUpdateUser.Click += new System.EventHandler(this.bttnLoadRoleUpdateUser_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(143, 206);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(131, 35);
            this.btnDelete.TabIndex = 11;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // cboNewRole
            // 
            this.cboNewRole.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboNewRole.FormattingEnabled = true;
            this.cboNewRole.Location = new System.Drawing.Point(130, 165);
            this.cboNewRole.Name = "cboNewRole";
            this.cboNewRole.Size = new System.Drawing.Size(259, 24);
            this.cboNewRole.TabIndex = 10;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(16, 169);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(76, 17);
            this.label9.TabIndex = 9;
            this.label9.Text = "New role:";
            // 
            // cboUserName
            // 
            this.cboUserName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboUserName.FormattingEnabled = true;
            this.cboUserName.Location = new System.Drawing.Point(130, 59);
            this.cboUserName.Name = "cboUserName";
            this.cboUserName.Size = new System.Drawing.Size(259, 24);
            this.cboUserName.TabIndex = 8;
            this.cboUserName.SelectedIndexChanged += new System.EventHandler(this.cboUserName_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(16, 63);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(94, 17);
            this.label8.TabIndex = 7;
            this.label8.Text = "Usernames:";
            // 
            // btnUpdateData
            // 
            this.btnUpdateData.Location = new System.Drawing.Point(6, 206);
            this.btnUpdateData.Name = "btnUpdateData";
            this.btnUpdateData.Size = new System.Drawing.Size(131, 35);
            this.btnUpdateData.TabIndex = 6;
            this.btnUpdateData.Text = "Save";
            this.btnUpdateData.UseVisualStyleBackColor = true;
            this.btnUpdateData.Click += new System.EventHandler(this.btnUpdateData_Click);
            // 
            // txtUpdatePassword
            // 
            this.txtUpdatePassword.Location = new System.Drawing.Point(130, 131);
            this.txtUpdatePassword.Name = "txtUpdatePassword";
            this.txtUpdatePassword.Size = new System.Drawing.Size(259, 23);
            this.txtUpdatePassword.TabIndex = 5;
            // 
            // txtUpdateUsername
            // 
            this.txtUpdateUsername.Location = new System.Drawing.Point(130, 98);
            this.txtUpdateUsername.Name = "txtUpdateUsername";
            this.txtUpdateUsername.Size = new System.Drawing.Size(259, 23);
            this.txtUpdateUsername.TabIndex = 4;
            // 
            // cboRoleupdate
            // 
            this.cboRoleupdate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboRoleupdate.FormattingEnabled = true;
            this.cboRoleupdate.Location = new System.Drawing.Point(130, 26);
            this.cboRoleupdate.Name = "cboRoleupdate";
            this.cboRoleupdate.Size = new System.Drawing.Size(259, 24);
            this.cboRoleupdate.TabIndex = 3;
            this.cboRoleupdate.SelectedIndexChanged += new System.EventHandler(this.cboRoleupdate_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 134);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 17);
            this.label4.TabIndex = 2;
            this.label4.Text = "Password";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 101);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(86, 17);
            this.label5.TabIndex = 1;
            this.label5.Text = "Username:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, 30);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 17);
            this.label6.TabIndex = 0;
            this.label6.Text = "Role:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnLoadRoleUsersByRole);
            this.groupBox3.Controls.Add(this.cboGetUserbyRole);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.dgdUserByRoles);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(436, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(792, 394);
            this.groupBox3.TabIndex = 6;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Users list by role";
            // 
            // btnLoadRoleUsersByRole
            // 
            this.btnLoadRoleUsersByRole.Location = new System.Drawing.Point(364, 22);
            this.btnLoadRoleUsersByRole.Name = "btnLoadRoleUsersByRole";
            this.btnLoadRoleUsersByRole.Size = new System.Drawing.Size(131, 32);
            this.btnLoadRoleUsersByRole.TabIndex = 14;
            this.btnLoadRoleUsersByRole.Text = "Load roles";
            this.btnLoadRoleUsersByRole.UseVisualStyleBackColor = true;
            this.btnLoadRoleUsersByRole.Click += new System.EventHandler(this.btnLoadRoleUsersByRole_Click);
            // 
            // cboGetUserbyRole
            // 
            this.cboGetUserbyRole.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboGetUserbyRole.FormattingEnabled = true;
            this.cboGetUserbyRole.Location = new System.Drawing.Point(78, 30);
            this.cboGetUserbyRole.Name = "cboGetUserbyRole";
            this.cboGetUserbyRole.Size = new System.Drawing.Size(259, 24);
            this.cboGetUserbyRole.TabIndex = 10;
            this.cboGetUserbyRole.SelectedIndexChanged += new System.EventHandler(this.cboGetUserbyRole_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(16, 33);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(46, 17);
            this.label7.TabIndex = 9;
            this.label7.Text = "Role:";
            // 
            // dgdUserByRoles
            // 
            this.dgdUserByRoles.AllowUserToAddRows = false;
            this.dgdUserByRoles.AllowUserToDeleteRows = false;
            this.dgdUserByRoles.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgdUserByRoles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgdUserByRoles.Location = new System.Drawing.Point(6, 60);
            this.dgdUserByRoles.Name = "dgdUserByRoles";
            this.dgdUserByRoles.ReadOnly = true;
            this.dgdUserByRoles.Size = new System.Drawing.Size(766, 323);
            this.dgdUserByRoles.TabIndex = 8;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lstRoles);
            this.groupBox2.Controls.Add(this.bttnLoadUsersByRole);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(3, 317);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(427, 345);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Users list by role";
            // 
            // lstRoles
            // 
            this.lstRoles.FormattingEnabled = true;
            this.lstRoles.ItemHeight = 16;
            this.lstRoles.Location = new System.Drawing.Point(19, 22);
            this.lstRoles.Name = "lstRoles";
            this.lstRoles.Size = new System.Drawing.Size(389, 260);
            this.lstRoles.TabIndex = 8;
            // 
            // bttnLoadUsersByRole
            // 
            this.bttnLoadUsersByRole.Location = new System.Drawing.Point(99, 288);
            this.bttnLoadUsersByRole.Name = "bttnLoadUsersByRole";
            this.bttnLoadUsersByRole.Size = new System.Drawing.Size(174, 35);
            this.bttnLoadUsersByRole.TabIndex = 7;
            this.bttnLoadUsersByRole.Text = "Load";
            this.bttnLoadUsersByRole.UseVisualStyleBackColor = true;
            this.bttnLoadUsersByRole.Click += new System.EventHandler(this.bttnLoadUsersByRole_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnLoadRolesNewUser);
            this.groupBox1.Controls.Add(this.bttnSave);
            this.groupBox1.Controls.Add(this.txtPassword);
            this.groupBox1.Controls.Add(this.txtUsername);
            this.groupBox1.Controls.Add(this.cboRole);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(436, 406);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(377, 234);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Add new user";
            // 
            // btnLoadRolesNewUser
            // 
            this.btnLoadRolesNewUser.Location = new System.Drawing.Point(19, 173);
            this.btnLoadRolesNewUser.Name = "btnLoadRolesNewUser";
            this.btnLoadRolesNewUser.Size = new System.Drawing.Size(174, 35);
            this.btnLoadRolesNewUser.TabIndex = 7;
            this.btnLoadRolesNewUser.Text = "Load roles";
            this.btnLoadRolesNewUser.UseVisualStyleBackColor = true;
            this.btnLoadRolesNewUser.Click += new System.EventHandler(this.btnLoadRolesNewUser_Click);
            // 
            // bttnSave
            // 
            this.bttnSave.Location = new System.Drawing.Point(19, 132);
            this.bttnSave.Name = "bttnSave";
            this.bttnSave.Size = new System.Drawing.Size(174, 35);
            this.bttnSave.TabIndex = 6;
            this.bttnSave.Text = "Save";
            this.bttnSave.UseVisualStyleBackColor = true;
            this.bttnSave.Click += new System.EventHandler(this.bttnSave_Click);
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(130, 90);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(225, 23);
            this.txtPassword.TabIndex = 5;
            // 
            // txtUsername
            // 
            this.txtUsername.Location = new System.Drawing.Point(130, 61);
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.Size = new System.Drawing.Size(225, 23);
            this.txtUsername.TabIndex = 4;
            // 
            // cboRole
            // 
            this.cboRole.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboRole.FormattingEnabled = true;
            this.cboRole.Location = new System.Drawing.Point(130, 30);
            this.cboRole.Name = "cboRole";
            this.cboRole.Size = new System.Drawing.Size(225, 24);
            this.cboRole.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 90);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Password";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Username:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Role:";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.txtRoleName);
            this.groupBox5.Controls.Add(this.btnAddNewRole);
            this.groupBox5.Controls.Add(this.label12);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.Location = new System.Drawing.Point(3, 3);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(427, 116);
            this.groupBox5.TabIndex = 7;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Add new role";
            // 
            // txtRoleName
            // 
            this.txtRoleName.Location = new System.Drawing.Point(119, 22);
            this.txtRoleName.Name = "txtRoleName";
            this.txtRoleName.Size = new System.Drawing.Size(259, 23);
            this.txtRoleName.TabIndex = 7;
            // 
            // btnAddNewRole
            // 
            this.btnAddNewRole.Location = new System.Drawing.Point(119, 66);
            this.btnAddNewRole.Name = "btnAddNewRole";
            this.btnAddNewRole.Size = new System.Drawing.Size(174, 35);
            this.btnAddNewRole.TabIndex = 6;
            this.btnAddNewRole.Text = "Save";
            this.btnAddNewRole.UseVisualStyleBackColor = true;
            this.btnAddNewRole.Click += new System.EventHandler(this.btnAddNewRole_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(16, 30);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(90, 17);
            this.label12.TabIndex = 0;
            this.label12.Text = "Role name:";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.btnLoadRoleNameUpdate);
            this.groupBox6.Controls.Add(this.cboRoleUpdateName);
            this.groupBox6.Controls.Add(this.label11);
            this.groupBox6.Controls.Add(this.txtRoleNewName);
            this.groupBox6.Controls.Add(this.btnUpdateRoleName);
            this.groupBox6.Controls.Add(this.label10);
            this.groupBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox6.Location = new System.Drawing.Point(3, 125);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(427, 184);
            this.groupBox6.TabIndex = 8;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Update role name";
            // 
            // btnLoadRoleNameUpdate
            // 
            this.btnLoadRoleNameUpdate.Location = new System.Drawing.Point(247, 99);
            this.btnLoadRoleNameUpdate.Name = "btnLoadRoleNameUpdate";
            this.btnLoadRoleNameUpdate.Size = new System.Drawing.Size(131, 35);
            this.btnLoadRoleNameUpdate.TabIndex = 13;
            this.btnLoadRoleNameUpdate.Text = "Load roles";
            this.btnLoadRoleNameUpdate.UseVisualStyleBackColor = true;
            this.btnLoadRoleNameUpdate.Click += new System.EventHandler(this.btnLoadRoleNameUpdate_Click);
            // 
            // cboRoleUpdateName
            // 
            this.cboRoleUpdateName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboRoleUpdateName.FormattingEnabled = true;
            this.cboRoleUpdateName.Location = new System.Drawing.Point(109, 21);
            this.cboRoleUpdateName.Name = "cboRoleUpdateName";
            this.cboRoleUpdateName.Size = new System.Drawing.Size(299, 24);
            this.cboRoleUpdateName.TabIndex = 9;
            this.cboRoleUpdateName.SelectedIndexChanged += new System.EventHandler(this.cboRoleUpdateName_SelectedIndexChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(16, 30);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(46, 17);
            this.label11.TabIndex = 8;
            this.label11.Text = "Role:";
            // 
            // txtRoleNewName
            // 
            this.txtRoleNewName.Location = new System.Drawing.Point(109, 64);
            this.txtRoleNewName.Name = "txtRoleNewName";
            this.txtRoleNewName.Size = new System.Drawing.Size(299, 23);
            this.txtRoleNewName.TabIndex = 7;
            // 
            // btnUpdateRoleName
            // 
            this.btnUpdateRoleName.Location = new System.Drawing.Point(19, 96);
            this.btnUpdateRoleName.Name = "btnUpdateRoleName";
            this.btnUpdateRoleName.Size = new System.Drawing.Size(131, 35);
            this.btnUpdateRoleName.TabIndex = 6;
            this.btnUpdateRoleName.Text = "Save";
            this.btnUpdateRoleName.UseVisualStyleBackColor = true;
            this.btnUpdateRoleName.Click += new System.EventHandler(this.btnUpdateRoleName_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(16, 70);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(87, 17);
            this.label10.TabIndex = 0;
            this.label10.Text = "New name:";
            // 
            // UserView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Name = "UserView";
            this.Size = new System.Drawing.Size(1249, 672);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgdUserByRoles)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.GroupBox groupBox4;
		private System.Windows.Forms.Button btnDelete;
		private System.Windows.Forms.ComboBox cboNewRole;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.ComboBox cboUserName;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Button btnUpdateData;
		private System.Windows.Forms.TextBox txtUpdatePassword;
		private System.Windows.Forms.TextBox txtUpdateUsername;
		private System.Windows.Forms.ComboBox cboRoleupdate;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.ComboBox cboGetUserbyRole;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.DataGridView dgdUserByRoles;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.ListBox lstRoles;
		private System.Windows.Forms.Button bttnLoadUsersByRole;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Button bttnSave;
		private System.Windows.Forms.TextBox txtPassword;
		private System.Windows.Forms.TextBox txtUsername;
		private System.Windows.Forms.ComboBox cboRole;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button btnLoadRolesNewUser;
		private System.Windows.Forms.GroupBox groupBox5;
		private System.Windows.Forms.TextBox txtRoleName;
		private System.Windows.Forms.Button btnAddNewRole;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Button bttnLoadRoleUpdateUser;
		private System.Windows.Forms.GroupBox groupBox6;
		private System.Windows.Forms.ComboBox cboRoleUpdateName;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.TextBox txtRoleNewName;
		private System.Windows.Forms.Button btnUpdateRoleName;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Button btnLoadRoleNameUpdate;
		private System.Windows.Forms.Button btnLoadRoleUsersByRole;
	}
}
