﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Laboratory1.Entities;
using Laboratory1.Controller;
using Laboratory1.DataLayer;

namespace Laboratory1.Views
{
	public partial class UserView : UserControl
	{
		public UserView()
		{
			InitializeComponent();
		}

		#region Add new user
		private void bttnSave_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty(txtUsername.Text) || string.IsNullOrEmpty(txtPassword.Text) || cboRole.SelectedText == "Select")
			{
				MessageBox.Show("Please fill all inputs");
			}
			else
			{
				User u = new User
				{
					UserName = txtUsername.Text,
					Password = txtPassword.Text,
					RoleId = GetIdCbo(cboRole)
				};
				try
				{
					UserController userController = new UserController();
					userController.Add(u);
					cboRole.SelectedText = "Select";
					txtPassword.Clear();
					txtUsername.Clear();
				}
				catch (Exception ex) { MessageBox.Show(ex.ToString()); }
			}
		}
		private void btnLoadRolesNewUser_Click(object sender, EventArgs e)
		{
			RoleController roleController = new RoleController();
			Dictionary<int, string> roles = roleController.GetAllDictionary();
			cboRole.DataSource = new BindingSource(roles, null);
			cboRole.DisplayMember = "Value";
			cboRole.ValueMember = "Key";
		}
		#endregion
		private int GetIdCbo(ComboBox cbo)
		{
			int value;
			value = cbo.SelectedValue is KeyValuePair<int, string> ? ((KeyValuePair<int, string>)cbo.SelectedValue).Key : (int)cbo.SelectedValue;
			return value;
		}


		#region Update User Data
		private void btnUpdateData_Click(object sender, EventArgs e)
		{
			int newRoleId = GetIdCbo(cboNewRole);
			int userId = GetIdCbo(cboUserName);
			User user = new User()
			{
				UserName = txtUpdateUsername.Text,
				Password = txtUpdatePassword.Text,
				RoleId = newRoleId,
				ID = userId
			};
			UserController userController = new UserController();
			userController.Update(user);
		}
		private void btnDelete_Click(object sender, EventArgs e)
		{
			int userId = GetIdCbo(cboUserName);
			UserController userController = new UserController();
			userController.Delete(userId);
		}
		private void bttnLoadRoleUpdateUser_Click(object sender, EventArgs e)
		{
			RoleController roleController = new RoleController();
			Dictionary<int, string> roles = roleController.GetAllDictionary();
			cboNewRole.DataSource = new BindingSource(roles, null);
			cboNewRole.DisplayMember = "Value";
			cboNewRole.ValueMember = "Key";

			cboRoleupdate.DataSource = new BindingSource(roles, null);
			cboRoleupdate.DisplayMember = "Value";
			cboRoleupdate.ValueMember = "Key";
		}

		private void cboRoleupdate_SelectedIndexChanged(object sender, EventArgs e)
		{
			int roleId = GetIdCbo(cboRoleupdate);
			UserController userController = new UserController();
			Dictionary<int, string> users = null;
			if (roleId != -1)
			{
				users = userController.GetAllByRoleDictionary(roleId);
				cboUserName.DataSource = new BindingSource(users, null);
				cboUserName.DisplayMember = "Value";
				cboUserName.ValueMember = "Key";
			}
		}

		private void cboUserName_SelectedIndexChanged(object sender, EventArgs e)
		{
			int userId = GetIdCbo(cboUserName);
			UserController userController = new UserController();
			User user = userController.GetById(userId);
			txtUpdateUsername.Text = user.UserName;
			txtUpdatePassword.Text = user.Password;
			cboNewRole.SelectedValue = user.RoleId;
		}

		#endregion

		#region Add new role
		private void btnAddNewRole_Click(object sender, EventArgs e)
		{
			RoleController roleController = new RoleController();
			if (string.IsNullOrEmpty(txtRoleName.Text))
				MessageBox.Show("Please fill name field");
			else
			{
				Role role = new Role
				{
					Name = txtRoleName.Text
				};
				roleController.Add(role);
			}
		}

		#endregion

		#region Load Users by role
		private void btnLoadRoleUsersByRole_Click(object sender, EventArgs e)
		{
			RoleController roleController = new RoleController();
			Dictionary<int, string> roles = roleController.GetAllDictionary();
			roles.Add(0, "All");
			cboGetUserbyRole.DataSource = new BindingSource(roles, null);
			cboGetUserbyRole.DisplayMember = "Value";
			cboGetUserbyRole.ValueMember = "Key";
		}
		private void cboGetUserbyRole_SelectedIndexChanged(object sender, EventArgs e)
		{
			int roleId = GetIdCbo(cboGetUserbyRole);
			UserController userController = new UserController();
			switch (roleId)
			{
				case -1:
					{
						dgdUserByRoles.DataSource = null;
						break;
					}
				case 0:
					{
						dgdUserByRoles.DataSource = userController.GetAll();
						dgdUserByRoles.AutoResizeColumns();
						break;
					}
				default:
					{
						dgdUserByRoles.DataSource = userController.GetAllByRole(roleId);
						dgdUserByRoles.AutoResizeColumns();
						break;
					}
			}
		}

		#endregion

		#region View list of roles
		private void bttnLoadUsersByRole_Click(object sender, EventArgs e)
		{
			RoleController roleController = new RoleController();
			lstRoles.Items.Clear();
			foreach (Role r in roleController.GetAll())
				lstRoles.Items.Add(r.Name);
		}
		#endregion

		#region Update Role Data
		private void btnLoadRoleNameUpdate_Click(object sender, EventArgs e)
		{
			RoleController roleController = new RoleController();
			Dictionary<int, string> roles = roleController.GetAllDictionary();
			cboRoleUpdateName.DataSource = new BindingSource(roles, null);
			cboRoleUpdateName.DisplayMember = "Value";
			cboRoleUpdateName.ValueMember = "Key";
		}
		

		private void cboRoleUpdateName_SelectedIndexChanged(object sender, EventArgs e)
		{
			RoleController roleController = new RoleController();
			int roleId = GetIdCbo(cboRoleUpdateName);
			Role role = roleController.GetById(roleId);
			txtRoleNewName.Text = role.Name;
        }

		private void btnUpdateRoleName_Click(object sender, EventArgs e)
		{
			RoleController roleController = new RoleController();
			int roleId = GetIdCbo(cboRoleUpdateName);
			Role role = new Role
			{
				ID = roleId,
				Name = txtRoleNewName.Text
			};
			roleController.Update(role);
		}
		private void btnDeleteRole_Click(object sender, EventArgs e)
		{
			RoleController roleController = new RoleController();
			int roleId = GetIdCbo(cboRoleUpdateName);
			roleController.Delete(roleId);
		}

		#endregion


	}
}
