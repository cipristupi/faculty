﻿using Laboratory1.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laboratory1.Controller
{
	interface IBaseController
	{
		void Add(BaseObject e);
		void Update(BaseObject e);
		void Delete(int id);
	}
}
