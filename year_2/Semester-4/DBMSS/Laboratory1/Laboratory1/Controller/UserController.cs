﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Laboratory1.DataLayer;

namespace Laboratory1.Controller
{
	public class UserController : IBaseController
	{
		UserOperations userOperations;
		public UserController()
		{
			userOperations = new UserOperations();
		}
		public void Add(BaseObject e)
		{
			userOperations.Add(e);
		}

		public void Delete(int id)
		{
			userOperations.Delete(id);
		}

		public void Update(BaseObject e)
		{
			userOperations.Update(e);
		}

		public List<User> GetAll()
		{
			List<User> users = new List<User>();
			foreach (BaseObject user in userOperations.GetAll())
				users.Add((User)user);
			return users;
		}

		public List<User> GetAllByRole(int roleId)
		{
			List<User> users = new List<User>();
			foreach (BaseObject user in userOperations.GetAllByRole(roleId))
				users.Add((User)user);
			return users;
		}

		public Dictionary<int, string> GetAllByRoleDictionary(int roleId)
		{
			Dictionary<int, string> users =new Dictionary<int, string>();
			foreach (BaseObject user in userOperations.GetAllByRole(roleId))
			{
				User u = (User)user;
                users.Add(u.ID,u.UserName);
			}
			return users;
		}

		public User GetById(int id)
		{
			return (User)userOperations.GetById(id);
		}
	}
}
