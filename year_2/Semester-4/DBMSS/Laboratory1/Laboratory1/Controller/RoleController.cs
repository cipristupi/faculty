﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Laboratory1.DataLayer;
using Laboratory1.Entities;

namespace Laboratory1.Controller
{
	public class RoleController : IBaseController
	{
		RoleOperations roleOperations;
		public RoleController()
		{
			roleOperations = new RoleOperations();
        }
		public void Add(BaseObject e)
		{
			roleOperations.Add(e);
		}

		public void Delete(int id)
		{
			roleOperations.Delete(id);
		}

		public void Update(BaseObject e)
		{
			roleOperations.Update(e);
		}

		public List<Role> GetAll()
		{
			List<Role> roles = new List<Role>();
			foreach (BaseObject user in roleOperations.GetAll())
				roles.Add((Role)user);
			return roles;
		}

		public Dictionary<int, string> GetAllDictionary()
		{
			Dictionary<int, string> roles = new Dictionary<int, string>();
			foreach (BaseObject r in roleOperations.GetAll())
			{
				Role role = (Role)r;
				roles.Add(role.ID, role.Name);
			}
			return roles;
		}

		public Role GetById(int id)
		{
			return (Role)roleOperations.GetById(id);
		}
	}
}
