﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laboratory2
{
	public class Relation
	{
		public TableChild Child{get;set; }
		public TableParent Parent { get; set; }

		public string Name { get; set; }
	}
}
