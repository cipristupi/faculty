﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laboratory2
{
    public class TableChild
    {
        public string Name { get; set; }
        public string PrimaryKeyName { get; set; }
        public bool PrimaryKeyAutoIncrement { get; set; }
		public string ForeignKeyName { get; set; }

		public string UpdateQuery { get; set; }
        public string DeleteQuery { get; set; }
        public string AddQuery { get; set; }
        public string SelectQuery { get; set; }
		public string SelectParameterName { get; set; }
    }
}
