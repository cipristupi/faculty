﻿namespace Laboratory2
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.ChildDataGridView = new System.Windows.Forms.DataGridView();
			this.parentDataGridView = new System.Windows.Forms.DataGridView();
			this.comboBox1 = new System.Windows.Forms.ComboBox();
			this.bttnLoadRelations = new System.Windows.Forms.Button();
			this.btnLoadRelation = new System.Windows.Forms.Button();
			this.btnLoadChild = new System.Windows.Forms.Button();
			this.btnSaveChild = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.ChildDataGridView)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.parentDataGridView)).BeginInit();
			this.SuspendLayout();
			// 
			// ChildDataGridView
			// 
			this.ChildDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
			this.ChildDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.ChildDataGridView.Location = new System.Drawing.Point(12, 269);
			this.ChildDataGridView.Name = "ChildDataGridView";
			this.ChildDataGridView.Size = new System.Drawing.Size(721, 215);
			this.ChildDataGridView.TabIndex = 23;
			// 
			// parentDataGridView
			// 
			this.parentDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
			this.parentDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.parentDataGridView.Location = new System.Drawing.Point(12, 12);
			this.parentDataGridView.Name = "parentDataGridView";
			this.parentDataGridView.Size = new System.Drawing.Size(721, 226);
			this.parentDataGridView.TabIndex = 22;
			// 
			// comboBox1
			// 
			this.comboBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.comboBox1.FormattingEnabled = true;
			this.comboBox1.Location = new System.Drawing.Point(751, 12);
			this.comboBox1.Name = "comboBox1";
			this.comboBox1.Size = new System.Drawing.Size(236, 21);
			this.comboBox1.TabIndex = 24;
			// 
			// bttnLoadRelations
			// 
			this.bttnLoadRelations.Location = new System.Drawing.Point(751, 55);
			this.bttnLoadRelations.Name = "bttnLoadRelations";
			this.bttnLoadRelations.Size = new System.Drawing.Size(116, 38);
			this.bttnLoadRelations.TabIndex = 25;
			this.bttnLoadRelations.Text = "Load Relations";
			this.bttnLoadRelations.UseVisualStyleBackColor = true;
			this.bttnLoadRelations.Click += new System.EventHandler(this.bttnLoadRelations_Click);
			// 
			// btnLoadRelation
			// 
			this.btnLoadRelation.Location = new System.Drawing.Point(873, 55);
			this.btnLoadRelation.Name = "btnLoadRelation";
			this.btnLoadRelation.Size = new System.Drawing.Size(116, 38);
			this.btnLoadRelation.TabIndex = 26;
			this.btnLoadRelation.Text = "Load Relation";
			this.btnLoadRelation.UseVisualStyleBackColor = true;
			this.btnLoadRelation.Click += new System.EventHandler(this.btnLoadRelation_Click);
			// 
			// btnLoadChild
			// 
			this.btnLoadChild.Location = new System.Drawing.Point(751, 113);
			this.btnLoadChild.Name = "btnLoadChild";
			this.btnLoadChild.Size = new System.Drawing.Size(116, 38);
			this.btnLoadChild.TabIndex = 27;
			this.btnLoadChild.Text = "Load Child";
			this.btnLoadChild.UseVisualStyleBackColor = true;
			this.btnLoadChild.Click += new System.EventHandler(this.btnLoadChild_Click);
			// 
			// btnSaveChild
			// 
			this.btnSaveChild.Location = new System.Drawing.Point(751, 168);
			this.btnSaveChild.Name = "btnSaveChild";
			this.btnSaveChild.Size = new System.Drawing.Size(116, 38);
			this.btnSaveChild.TabIndex = 28;
			this.btnSaveChild.Text = "Save Child";
			this.btnSaveChild.UseVisualStyleBackColor = true;
			this.btnSaveChild.Click += new System.EventHandler(this.btnSaveChild_Click);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(999, 496);
			this.Controls.Add(this.btnSaveChild);
			this.Controls.Add(this.btnLoadChild);
			this.Controls.Add(this.btnLoadRelation);
			this.Controls.Add(this.bttnLoadRelations);
			this.Controls.Add(this.comboBox1);
			this.Controls.Add(this.ChildDataGridView);
			this.Controls.Add(this.parentDataGridView);
			this.Name = "Form1";
			this.Text = "Form1";
			this.Load += new System.EventHandler(this.Form1_Load);
			((System.ComponentModel.ISupportInitialize)(this.ChildDataGridView)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.parentDataGridView)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion
        private System.Windows.Forms.DataGridView ChildDataGridView;
        private System.Windows.Forms.DataGridView parentDataGridView;
		private System.Windows.Forms.ComboBox comboBox1;
		private System.Windows.Forms.Button bttnLoadRelations;
		private System.Windows.Forms.Button btnLoadRelation;
		private System.Windows.Forms.Button btnLoadChild;
		private System.Windows.Forms.Button btnSaveChild;
	}
}

