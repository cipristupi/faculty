﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace Laboratory2
{
	public partial class Form1 : Form
	{
		private SqlCommand cmd;
		private SqlConnection conn;
		private SqlDataReader reader;
		private string ConnectionString = @"Data Source=.\sql;Initial Catalog=SchoolManager;Integrated Security=True";
		private DataSet dataSet;
		private SqlDataAdapter childTableAdapter, parentTableAdapter;
		private Relation selectedRelation;
		public Form1()
		{
			InitializeComponent();
		}

		private List<Relation> relations;

		private void Form1_Load(object sender, EventArgs e)
		{
		}

		private void btnSaveChild_Click(object sender, EventArgs e)
		{
			try
			{
				SqlCommandBuilder builder = new SqlCommandBuilder(childTableAdapter);
				builder.GetUpdateCommand();
				childTableAdapter.Update(dataSet, selectedRelation.Child.Name);
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
		}

		private void btnLoadChild_Click(object sender, EventArgs e)
		{
			try
			{
				if (parentDataGridView.SelectedCells.Count > 1 || parentDataGridView.SelectedCells.Count == 0)
				{
					MessageBox.Show("Select only one cell");
				}
				else
				{
					var selectedCell = parentDataGridView.SelectedCells[0].Value;
					childTableAdapter.SelectCommand.Parameters.Clear();
					childTableAdapter.SelectCommand.Parameters.Add(new SqlParameter(selectedRelation.Child.SelectParameterName, selectedCell));
					dataSet.Tables[selectedRelation.Child.Name].Clear();
                    childTableAdapter.Fill(dataSet, selectedRelation.Child.Name);
					ChildDataGridView.DataSource = null;
					ChildDataGridView.Update();
                    ChildDataGridView.DataSource = dataSet.Tables[selectedRelation.Child.Name];
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
		}

		private void btnLoadRelation_Click(object sender, EventArgs e)
		{
			try
			{
				selectedRelation = relations[comboBox1.SelectedIndex];
				conn = new SqlConnection(ConnectionString);
				dataSet = new DataSet();
				dataSet.Tables.Add(selectedRelation.Parent.Name);
				dataSet.Tables.Add(selectedRelation.Child.Name);
				parentTableAdapter = new SqlDataAdapter(selectedRelation.Parent.SelectQuery, conn);
				childTableAdapter = new SqlDataAdapter(selectedRelation.Child.SelectQuery, conn);
				dataSet.Tables[selectedRelation.Parent.Name].Clear();
                parentTableAdapter.Fill(dataSet, selectedRelation.Parent.Name);
				parentDataGridView.DataSource = null;
				parentDataGridView.Refresh();
				parentDataGridView.DataSource = dataSet.Tables[selectedRelation.Parent.Name];
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
		}

		private void bttnLoadRelations_Click(object sender, EventArgs e)
		{
			try
			{
				relations = new List<Relation>();
				string uriPath = System.IO.Directory.GetCurrentDirectory() + "\\CustomConfig.xml";
				XmlDocument doc = new XmlDocument();
				doc.Load(new Uri(uriPath).LocalPath);
				XmlNodeList xnList = doc.SelectNodes("/Config/Relations/Relation");
				foreach (XmlNode node in xnList)
				{
					Relation r = new Relation();
					XmlNode xmlNodeParent = node.SelectSingleNode("Parent");
					TableParent p = new TableParent();
					p.Name = xmlNodeParent["Name"].InnerText;
					XmlNode xmlPrimary = xmlNodeParent["PrimaryKey"];
					p.PrimaryKeyName = xmlPrimary.SelectSingleNode("Name").InnerText;
					p.PrimaryKeyAutoIncrement = xmlPrimary.SelectSingleNode("AutoIncrement").InnerText == "True" ? true : false;
					p.SelectQuery = xmlNodeParent["Select"].InnerText;

					XmlNode xmlNodeChild = node.SelectSingleNode("Child");
					XmlNode xmlPrimaryChild = xmlNodeChild["PrimaryKey"];
					TableChild c = new TableChild();
					c.Name = xmlNodeChild["Name"].InnerText;
					c.PrimaryKeyName = xmlPrimaryChild.SelectSingleNode("Name").InnerText;
					c.PrimaryKeyAutoIncrement = xmlPrimaryChild.SelectSingleNode("AutoIncrement").InnerText == "True" ? true : false;
					c.SelectQuery = xmlNodeChild["Select"].InnerText;
					c.UpdateQuery = xmlNodeChild["Update"].InnerText;
					c.DeleteQuery = xmlNodeChild["Delete"].InnerText;
					c.AddQuery = xmlNodeChild["Add"].InnerText;
					XmlNode childForeignKey = xmlNodeChild["ForeignKey"];
					c.ForeignKeyName = childForeignKey.SelectSingleNode("Name").InnerText;
					c.SelectParameterName = xmlNodeChild["SelectParameter"].InnerText;

					r.Name = node.Attributes["Name"].Value;
					r.Parent = p;
					r.Child = c;
					relations.Add(r);
				}
				foreach (Relation r in relations)
				{
					comboBox1.Items.Add(r.Name);
				}
				comboBox1.SelectedIndex = 0;
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
		}
	}
}
