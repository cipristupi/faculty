SELECT c.Name --, v.Name, v.ID,r.ID
FROM dbo.FavoriteRides fr --favorite rides 
JOIN dbo.Visitors v ON fr.VisitorID = v.ID --get user for favorites rides
JOIN dbo.Rides r ON fr.RideID = r.ID -- get rides for favorites rides
JOIN dbo.Categories c ON c.ID = r.CategoryID -- get categories for getted rides
WHERE v.HappinessScore = 5
 AND (
 (SELECT COUNT(*) FROM dbo.RepairLog rl WHERE rl.RideID = r.ID) >= 2  --gets rides having more than 2 repairs
 AND
 (SELECT COUNT(*) FROM dbo.RepairLog rl WHERE rl.[Time] >= 30 AND rl.RideID =r.ID) >=2
  )
ORDER BY c.Name