USE [master]
GO
/****** Object:  Database [ThemePark]    Script Date: 6/8/2015 08:44:06 PM ******/
CREATE DATABASE [ThemePark]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'ThemePark', FILENAME = N'D:\Program Files\Development Tools\Microsoft SQL Server\MSSQL12.SQL\MSSQL\DATA\ThemePark.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'ThemePark_log', FILENAME = N'D:\Program Files\Development Tools\Microsoft SQL Server\MSSQL12.SQL\MSSQL\DATA\ThemePark_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [ThemePark] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [ThemePark].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [ThemePark] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [ThemePark] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [ThemePark] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [ThemePark] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [ThemePark] SET ARITHABORT OFF 
GO
ALTER DATABASE [ThemePark] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [ThemePark] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [ThemePark] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [ThemePark] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [ThemePark] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [ThemePark] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [ThemePark] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [ThemePark] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [ThemePark] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [ThemePark] SET  DISABLE_BROKER 
GO
ALTER DATABASE [ThemePark] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [ThemePark] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [ThemePark] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [ThemePark] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [ThemePark] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [ThemePark] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [ThemePark] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [ThemePark] SET RECOVERY FULL 
GO
ALTER DATABASE [ThemePark] SET  MULTI_USER 
GO
ALTER DATABASE [ThemePark] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [ThemePark] SET DB_CHAINING OFF 
GO
ALTER DATABASE [ThemePark] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [ThemePark] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [ThemePark] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'ThemePark', N'ON'
GO
USE [ThemePark]
GO
/****** Object:  Table [dbo].[Categories]    Script Date: 6/8/2015 08:44:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Categories](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [text] NULL,
 CONSTRAINT [PK_Categories] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CategoriesRides]    Script Date: 6/8/2015 08:44:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CategoriesRides](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RideID] [int] NOT NULL,
	[CategoryID] [int] NOT NULL,
 CONSTRAINT [PK_CategoriesRides] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FavoriteRides]    Script Date: 6/8/2015 08:44:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FavoriteRides](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[VisitorID] [int] NOT NULL,
	[RideID] [int] NOT NULL,
 CONSTRAINT [PK_FavoriteRides] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Mechanics]    Script Date: 6/8/2015 08:44:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Mechanics](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Age] [int] NOT NULL,
 CONSTRAINT [PK_Mechanics] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RepairLog]    Script Date: 6/8/2015 08:44:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RepairLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MechanicID] [int] NOT NULL,
	[RideID] [int] NOT NULL,
	[Date] [date] NOT NULL,
	[Time] [int] NOT NULL,
 CONSTRAINT [PK_RepairLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Rides]    Script Date: 6/8/2015 08:44:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Rides](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[PopularityScore] [int] NOT NULL,
	[CategoryID] [int] NOT NULL,
 CONSTRAINT [PK_Rides] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Visitors]    Script Date: 6/8/2015 08:44:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Visitors](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Age] [int] NOT NULL,
	[HappinessScore] [int] NOT NULL,
 CONSTRAINT [PK_Visitors] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Categories] ON 

INSERT [dbo].[Categories] ([ID], [Name], [Description]) VALUES (1, N'Flat rides', NULL)
INSERT [dbo].[Categories] ([ID], [Name], [Description]) VALUES (2, N'Gravity rides', NULL)
INSERT [dbo].[Categories] ([ID], [Name], [Description]) VALUES (3, N'Vertical rides', NULL)
SET IDENTITY_INSERT [dbo].[Categories] OFF
SET IDENTITY_INSERT [dbo].[FavoriteRides] ON 

INSERT [dbo].[FavoriteRides] ([ID], [VisitorID], [RideID]) VALUES (1, 1, 1)
INSERT [dbo].[FavoriteRides] ([ID], [VisitorID], [RideID]) VALUES (2, 1, 2)
INSERT [dbo].[FavoriteRides] ([ID], [VisitorID], [RideID]) VALUES (3, 1, 4)
INSERT [dbo].[FavoriteRides] ([ID], [VisitorID], [RideID]) VALUES (6, 8, 1)
INSERT [dbo].[FavoriteRides] ([ID], [VisitorID], [RideID]) VALUES (7, 8, 2)
INSERT [dbo].[FavoriteRides] ([ID], [VisitorID], [RideID]) VALUES (8, 8, 4)
INSERT [dbo].[FavoriteRides] ([ID], [VisitorID], [RideID]) VALUES (11, 9, 1)
INSERT [dbo].[FavoriteRides] ([ID], [VisitorID], [RideID]) VALUES (12, 9, 2)
INSERT [dbo].[FavoriteRides] ([ID], [VisitorID], [RideID]) VALUES (13, 9, 4)
SET IDENTITY_INSERT [dbo].[FavoriteRides] OFF
SET IDENTITY_INSERT [dbo].[Mechanics] ON 

INSERT [dbo].[Mechanics] ([ID], [Name], [Age]) VALUES (1, N'Dorel', 50)
INSERT [dbo].[Mechanics] ([ID], [Name], [Age]) VALUES (2, N'Vasile', 25)
INSERT [dbo].[Mechanics] ([ID], [Name], [Age]) VALUES (3, N'Gigel', 36)
SET IDENTITY_INSERT [dbo].[Mechanics] OFF
SET IDENTITY_INSERT [dbo].[RepairLog] ON 

INSERT [dbo].[RepairLog] ([ID], [MechanicID], [RideID], [Date], [Time]) VALUES (1, 1, 1, CAST(N'2015-06-08' AS Date), 45)
INSERT [dbo].[RepairLog] ([ID], [MechanicID], [RideID], [Date], [Time]) VALUES (3, 2, 2, CAST(N'2014-09-09' AS Date), 60)
INSERT [dbo].[RepairLog] ([ID], [MechanicID], [RideID], [Date], [Time]) VALUES (4, 3, 1, CAST(N'2011-11-11' AS Date), 35)
INSERT [dbo].[RepairLog] ([ID], [MechanicID], [RideID], [Date], [Time]) VALUES (7, 1, 1, CAST(N'2012-12-12' AS Date), 5)
INSERT [dbo].[RepairLog] ([ID], [MechanicID], [RideID], [Date], [Time]) VALUES (8, 1, 4, CAST(N'2015-10-15' AS Date), 35)
INSERT [dbo].[RepairLog] ([ID], [MechanicID], [RideID], [Date], [Time]) VALUES (9, 1, 4, CAST(N'2005-05-05' AS Date), 75)
SET IDENTITY_INSERT [dbo].[RepairLog] OFF
SET IDENTITY_INSERT [dbo].[Rides] ON 

INSERT [dbo].[Rides] ([ID], [Name], [PopularityScore], [CategoryID]) VALUES (1, N'Carousels', 1, 1)
INSERT [dbo].[Rides] ([ID], [Name], [PopularityScore], [CategoryID]) VALUES (2, N'Haunted houses', 5, 1)
INSERT [dbo].[Rides] ([ID], [Name], [PopularityScore], [CategoryID]) VALUES (4, N'Rollercoasters', 3, 2)
INSERT [dbo].[Rides] ([ID], [Name], [PopularityScore], [CategoryID]) VALUES (10, N'Something', 1, 1)
SET IDENTITY_INSERT [dbo].[Rides] OFF
SET IDENTITY_INSERT [dbo].[Visitors] ON 

INSERT [dbo].[Visitors] ([ID], [Name], [Age], [HappinessScore]) VALUES (1, N'Gigel Constantinescu', 15, 1)
INSERT [dbo].[Visitors] ([ID], [Name], [Age], [HappinessScore]) VALUES (8, N'Vasile', 10, 2)
INSERT [dbo].[Visitors] ([ID], [Name], [Age], [HappinessScore]) VALUES (9, N'Costel', 50, 5)
SET IDENTITY_INSERT [dbo].[Visitors] OFF
/****** Object:  Index [NonClusteredIndex-20150608-122039]    Script Date: 6/8/2015 08:44:08 PM ******/
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20150608-122039] ON [dbo].[RepairLog]
(
	[RideID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [NonClusteredIndex-20150608-121024]    Script Date: 6/8/2015 08:44:08 PM ******/
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20150608-121024] ON [dbo].[Rides]
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FavoriteRides]  WITH CHECK ADD  CONSTRAINT [FK_FavoriteRides_Rides] FOREIGN KEY([RideID])
REFERENCES [dbo].[Rides] ([ID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[FavoriteRides] CHECK CONSTRAINT [FK_FavoriteRides_Rides]
GO
ALTER TABLE [dbo].[FavoriteRides]  WITH CHECK ADD  CONSTRAINT [FK_FavoriteRides_Visitors] FOREIGN KEY([VisitorID])
REFERENCES [dbo].[Visitors] ([ID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[FavoriteRides] CHECK CONSTRAINT [FK_FavoriteRides_Visitors]
GO
ALTER TABLE [dbo].[RepairLog]  WITH CHECK ADD  CONSTRAINT [FK_RepairLog_Mechanics] FOREIGN KEY([MechanicID])
REFERENCES [dbo].[Mechanics] ([ID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[RepairLog] CHECK CONSTRAINT [FK_RepairLog_Mechanics]
GO
ALTER TABLE [dbo].[RepairLog]  WITH CHECK ADD  CONSTRAINT [FK_RepairLog_Rides] FOREIGN KEY([RideID])
REFERENCES [dbo].[Rides] ([ID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[RepairLog] CHECK CONSTRAINT [FK_RepairLog_Rides]
GO
ALTER TABLE [dbo].[Rides]  WITH CHECK ADD  CONSTRAINT [FK_Rides_Categories] FOREIGN KEY([CategoryID])
REFERENCES [dbo].[Categories] ([ID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Rides] CHECK CONSTRAINT [FK_Rides_Categories]
GO
ALTER TABLE [dbo].[Rides]  WITH NOCHECK ADD  CONSTRAINT [CK_Rides] CHECK  (([PopularityScore]>=(1) AND [PopularityScore]<=(5)))
GO
ALTER TABLE [dbo].[Rides] CHECK CONSTRAINT [CK_Rides]
GO
ALTER TABLE [dbo].[Visitors]  WITH NOCHECK ADD  CONSTRAINT [CK_Visitors] CHECK  (([HappinessScore]>=(1) AND [HappinessScore]<=(5)))
GO
ALTER TABLE [dbo].[Visitors] CHECK CONSTRAINT [CK_Visitors]
GO
USE [master]
GO
ALTER DATABASE [ThemePark] SET  READ_WRITE 
GO
