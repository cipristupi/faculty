﻿namespace ExamDBMS
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.loadsCategories = new System.Windows.Forms.Button();
            this.saveChanges = new System.Windows.Forms.Button();
            this.loadRidesForCategories = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(13, 13);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(513, 445);
            this.dataGridView1.TabIndex = 0;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(545, 13);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(196, 21);
            this.comboBox1.TabIndex = 1;
            // 
            // loadsCategories
            // 
            this.loadsCategories.Location = new System.Drawing.Point(545, 60);
            this.loadsCategories.Name = "loadsCategories";
            this.loadsCategories.Size = new System.Drawing.Size(196, 33);
            this.loadsCategories.TabIndex = 2;
            this.loadsCategories.Text = "Load  categories";
            this.loadsCategories.UseVisualStyleBackColor = true;
            this.loadsCategories.Click += new System.EventHandler(this.loadsCategories_Click);
            // 
            // saveChanges
            // 
            this.saveChanges.Location = new System.Drawing.Point(545, 138);
            this.saveChanges.Name = "saveChanges";
            this.saveChanges.Size = new System.Drawing.Size(196, 33);
            this.saveChanges.TabIndex = 3;
            this.saveChanges.Text = "Save changes";
            this.saveChanges.UseVisualStyleBackColor = true;
            this.saveChanges.Click += new System.EventHandler(this.saveChanges_Click);
            // 
            // loadRidesForCategories
            // 
            this.loadRidesForCategories.Location = new System.Drawing.Point(545, 99);
            this.loadRidesForCategories.Name = "loadRidesForCategories";
            this.loadRidesForCategories.Size = new System.Drawing.Size(196, 33);
            this.loadRidesForCategories.TabIndex = 4;
            this.loadRidesForCategories.Text = "Load rides for category";
            this.loadRidesForCategories.UseVisualStyleBackColor = true;
            this.loadRidesForCategories.Click += new System.EventHandler(this.loadRidesForCategories_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(761, 470);
            this.Controls.Add(this.loadRidesForCategories);
            this.Controls.Add(this.saveChanges);
            this.Controls.Add(this.loadsCategories);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button loadsCategories;
        private System.Windows.Forms.Button saveChanges;
        private System.Windows.Forms.Button loadRidesForCategories;
    }
}

