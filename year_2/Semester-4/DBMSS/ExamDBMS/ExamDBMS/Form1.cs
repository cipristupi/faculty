﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExamDBMS
{
    public partial class Form1 : Form
    {
        private SqlCommand cmd;
        private SqlConnection conn;
        private SqlDataReader reader;
        private string ConnectionString = @"Data Source=.\sql;Initial Catalog=ThemePark;Integrated Security=True";
        private DataSet dataSet;
        private SqlDataAdapter childTableAdapter, parentTableAdapter;
        private string selectCategoriesQuery = "SELECT ID, Name FROM Categories";
        private string selectRidesQuery = "SELECT * FROM Rides WHERE CategoryID = @id";
        private string firstTableName = "Categories";
        private string secoundTableName = "Rides";
        public Form1()
        {
            InitializeComponent();

        }

        private void loadsCategories_Click(object sender, EventArgs e)
        {
            Dictionary<int, string> categories = new Dictionary<int, string>();
            conn = new SqlConnection(ConnectionString);
            try
            {
                conn.Open();
                cmd = new SqlCommand(selectCategoriesQuery, conn);
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    categories.Add(reader.GetInt32(0), reader.GetString(1));
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            comboBox1.DataSource = new BindingSource(categories, null);
            comboBox1.DisplayMember = "Value";
            comboBox1.ValueMember = "Key";
        }

        private void loadRidesForCategories_Click(object sender, EventArgs e)
        {
            try
            {
                int id = GetIdCbo(comboBox1);
                childTableAdapter = new SqlDataAdapter(selectRidesQuery, conn);
                dataSet = new DataSet();
                childTableAdapter.SelectCommand.Parameters.Clear();
                childTableAdapter.SelectCommand.Parameters.Add(new SqlParameter("@id", id));
                //dataSet.Tables[secoundTableName].Clear();
                childTableAdapter.Fill(dataSet, secoundTableName);
                dataGridView1.DataSource = null;
                dataGridView1.Update();
                dataGridView1.DataSource = dataSet.Tables[secoundTableName];
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private int GetIdCbo(ComboBox cbo)
        {
            int value;
            value = cbo.SelectedValue is KeyValuePair<int, string> ? ((KeyValuePair<int, string>)cbo.SelectedValue).Key : (int)cbo.SelectedValue;
            return value;
        }

        private void saveChanges_Click(object sender, EventArgs e)
        {
            try
            {
                SqlCommandBuilder builder = new SqlCommandBuilder(childTableAdapter);
                builder.GetUpdateCommand();
                childTableAdapter.Update(dataSet, secoundTableName);
                loadRidesForCategories.PerformClick();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
    }
}
