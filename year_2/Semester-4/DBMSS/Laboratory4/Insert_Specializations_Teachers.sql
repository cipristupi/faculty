DECLARE @date DATE
SET @date= (SELECT GETDATE())
INSERT INTO [dbo].[Teacher_Specializations]
SELECT t.Id AS TeachersId , s.Id AS SpecializationsId , @date AS Date_of_get
FROM [SchoolManager].[dbo].[Teachers] t
CROSS JOIN [Specializations] s

SELECT tc.Id ,tc.TeachersId,tc.SpecializationsId,tc.Date_of_get, t.FirstName,s.Name FROM dbo.Teacher_Specializations tc
JOIN dbo.Teachers t ON tc.TeachersId = t.Id
JOIN dbo.Specializations s ON s.Id = tc.SpecializationsId