--time 00:34 without RIGTH JOIN
--time with Distict 2:57
--time with ORDER BY by FirstName 00:07 and without 00:01
SELECT t.FirstName ,t.LastName, s.Name AS Specialization , cl.Name AS 'Class level', ce.Name AS 'Class Letter' FROM dbo.Teachers t
INNER JOIN dbo.Teacher_Specializations ts ON ts.TeachersId = t.Id --join to get specialization for teacher
INNER JOIN dbo.Specializations s ON ts.SpecializationsId = s.Id --join to get name for specialization
INNER JOIN dbo.Teacher_Classes tc ON tc.TeachersId = t.Id --join to get teachers classes
INNER JOIN dbo.Classes c ON c.Id = tc.ClassesId --join to get ids for name of level and enum for classes BEST WITH RIGHT OUTER JOIN
INNER JOIN dbo.Classes_Enum ce ON ce.Id = c.Classes_EnumId --BEST WITH RIGHT OUTER JOIN
INNER JOIN dbo.Classes_Level cl ON cl.Id = c.Classes_LevelId --BEST WITH RIGHT OUTER JOIN
WHERE ce.Name = 'A' AND cl.Name ='9' AND s.Name='algebra' AND t.FirstName LIKE '%10%' --filter
ORDER BY (SELECT COUNT(*) FROM dbo.Students WHERE ClassesId = c.Id) -- time 3:15
--ORDER BY ce.Name --time 2:53
--ORDER BY t.FirstName --order