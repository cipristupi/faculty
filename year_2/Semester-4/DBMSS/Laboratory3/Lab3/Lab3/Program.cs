﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Lab3
{
    class Program
    {
        private static SqlConnection sqlConnection;
        private static string conString = @"Data Source=.\sql;Initial Catalog=SchoolManager;Integrated Security=True";
        private static int count1 = 0, count2 = 0;

        static void Main(string[] args)
        {
            sqlConnection = new SqlConnection(conString);
            if (sqlConnection.State == System.Data.ConnectionState.Closed)
            {
                sqlConnection.Open();
            }
            Thread transaction2 = new Thread(Thread2);
            transaction2.Start();
            int result = Transaction1();
            Console.WriteLine("Done");
            Console.Read();
        }

        private static void Thread2()
        {
            int result = Transaction2();
        }

        private static int Transaction1()
        {
            int result = 0;
            using (sqlConnection = new SqlConnection(conString))
            {
                //sqlConnection.Open();
                //if(sqlConnection.State == System.Data.ConnectionState.Closed)
                //{
                //    sqlConnection.Open();
                //}
                sqlConnection.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = sqlConnection;
                    cmd.CommandText = @"BEGIN TRANSACTION; UPDATE Teacher SET CNP='cnpgood' WHERE CNP = 'CNPGOOD'; WAITFOR DELAY '00:00:05.00'; UPDATE Specializations SET Name='MATH' WHERE Name = 'math'; ROLLBACK TRANSACTION";
                    cmd.CommandType = System.Data.CommandType.Text;
                    try
                    {
                        result = cmd.ExecuteNonQuery();
                        Console.WriteLine("Thread 1: {0} rows affected", result);
                        count1++;
                    }
                    catch (Exception sql)
                    {
                        if (count1 < 3)
                        {
                            Console.WriteLine("Thread 1: Execution timeout! Retrying..");
                            Transaction1();
                        }
                        else
                        {
                            Console.WriteLine("Thread 1: Operation aborted!");
                        }
                    }
                }
            }
            return result;
        }

        private static int Transaction2()
        {
            int result = 0;
            using (sqlConnection = new SqlConnection(conString))
            {
                sqlConnection.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = sqlConnection;
                    cmd.CommandText = @"BEGIN TRANSACTION; UPDATE Specializations SET Name='MATH' WHERE Name = 'math'; WAITFOR DELAY '00:00:05.00'; UPDATE Teachers SET CNP='cnpgood' WHERE CNP = 'CNPGOOD'; ROLLBACK TRANSACTION";
                    cmd.CommandType = System.Data.CommandType.Text;
                    try
                    {
                        result = cmd.ExecuteNonQuery();
                        Console.WriteLine("Thread 2: {0} rows affected", result);
                        count2++;
                    }
                    catch (Exception sql)
                    {
                        if (count2 < 3)
                        {
                            Console.WriteLine("Thread 2: Execution timeout! Retrying..");
                            Transaction2();
                        }
                        else
                        {
                            Console.WriteLine("Thread 2: Operation aborted!");
                        }
                    }
                }
            }
            return result;
        }

    }
}
