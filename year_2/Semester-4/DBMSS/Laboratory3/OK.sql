USE [SchoolManager]
GO

DECLARE	@return_value int

EXEC	@return_value = [dbo].[CompleteRollBack]
		@username = N'test',
		@password = N'password777',
		@firstname = N'test+777',
		@lastname = N'test',
		@cnp = N'12345678903777',
		@locality = N'test',
		@address = N'test',
		@positionName = N'teacher',
		@specializationName = N'math',
		@roleName = N'teacher',
		@dateOfGet = N'10/10/2010'

SELECT	'Return Value' = @return_value

GO
