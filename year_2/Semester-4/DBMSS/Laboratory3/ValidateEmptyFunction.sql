
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION ValidateEmpty
(
	@parameter NVARCHAR(MAX)
)
RETURNS bit
AS
BEGIN
	IF LEN(@parameter) > 0
	BEGIN
		RETURN 1
	END	
	RETURN 0

END
GO

