USE [SchoolManager]
GO

DECLARE	@return_value int

EXEC	@return_value = [dbo].[CompleteRollBack]
		@username = N'rollback1',
		@password = N'rollback1',
		@firstname = N'rollback+1',
		@lastname = N'rollback1',
		@cnp = N'12345678903777',
		@locality = N'rollback1',
		@address = N'rollback1',
		@positionName = 'teacher',
		@specializationName = N'math',
		@roleName = N'teacher',
		@dateOfGet = N'10/10/2010'

SELECT	'Return Value' = @return_value

GO
