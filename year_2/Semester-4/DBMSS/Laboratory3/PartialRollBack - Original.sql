
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ciprian
-- Create date: 
-- Description:	
-- =============================================
IF EXISTS ( SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_NAME = 'PartialRollBack')
	BEGIN
		DROP PROC PartialRollBack
	END
GO

CREATE PROCEDURE PartialRollBack 
	@username NVARCHAR(50),
	@password NVARCHAR(200),
	@firstname NVARCHAR(50),
	@lastname NVARCHAR(50),
	@cnp NVARCHAR(13),
	@locality NVARCHAR(50),
	@address NVARCHAR(50),
	@positionName NVARCHAR(50),
	@specializationName NVARCHAR(50),
	@roleName NVARCHAR(50),
	@dateOfGet DATE
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @roleID INT
	DECLARE @userID INT
	DECLARE @teacherID INT
	DECLARE @positionID  INT
    DECLARE @specializationID INT
	DECLARE @firstSection BIT = 0
	DECLARE @secoundSection BIT = 0
	DECLARE @thirdSection BIT = 0
	BEGIN TRY
	BEGIN TRAN


	--First section
	SAVE TRANSACTION BEFOREALL;
	IF	(SELECT [SchoolManager].[dbo].ValidateEmpty(@username)) = 0
	BEGIN
		SET @firstsection = 1
		RAISERROR('Username is empty',16,1)
	END
	IF	(SELECT [SchoolManager].[dbo].ValidateEmpty(@password)) = 0
	BEGIN
		SET @firstsection = 1
		RAISERROR('Password is empty',16,1)
	END
	IF	(SELECT [SchoolManager].[dbo].ValidateEmpty(@roleName)) = 0
	BEGIN
		SET @firstsection = 1
		RAISERROR('Role name  is empty',16,1)
	END

	IF	(SELECT COUNT(*) FROM [SchoolManager].[dbo].[Roles] WHERE Name = @roleName) = 0
	BEGIN
		SET @firstsection = 1
		RAISERROR ('Role does not exists',16,1)
	END


	SET @roleID = (SELECT ID FROM [SchoolManager].[dbo].[Roles] WHERE Name = @roleName)
	
	INSERT INTO [dbo].[Users]([Username],[Password],[RolesId]) VALUES(@username,@password,@roleID)

	SET @userID = SCOPE_IDENTITY()

	SAVE TRANSACTION FirstSection;
	--Here start the next set of validations


	IF	(SELECT [SchoolManager].[dbo].ValidateEmpty(@firstname)) = 0
	BEGIN
		SET @secoundSection = 1
		RAISERROR('First name is empty',16,1)
	END
	IF	(SELECT [SchoolManager].[dbo].ValidateEmpty(@lastname)) = 0
	BEGIN
		SET @secoundSection = 1
		RAISERROR('Last name is empty',16,1)
	END
	IF	(SELECT [SchoolManager].[dbo].ValidateEmpty(@cnp)) = 0
	BEGIN
		SET @secoundSection = 1
		RAISERROR('CNP is empty',16,1)
	END

	IF	(SELECT [SchoolManager].[dbo].ValidateEmpty(@locality)) = 0
	BEGIN
		SET @secoundSection = 1
		RAISERROR('Locality is empty',16,1)
	END

	IF	(SELECT [SchoolManager].[dbo].ValidateEmpty(@address)) = 0
	BEGIN
		SET @secoundSection = 1
		RAISERROR('Address is empty',16,1)
	END

	IF	(SELECT [SchoolManager].[dbo].ValidateEmpty(@positionName)) = 0
	BEGIN
		SET @secoundSection = 1
		RAISERROR('Position is empty',16,1)
	END

	IF	(SELECT COUNT(*) FROM [SchoolManager].[dbo].[Positions] WHERE Name = @positionName) = 0
	BEGIN
		SET @secoundSection = 1
		RAISERROR ('Position does not exists',16,1)
	END

	SET @positionID = (SELECT ID FROM [SchoolManager].[dbo].[Positions] WHERE Name = @positionName)


	INSERT INTO [dbo].[Teachers]
           ([FirstName]
           ,[LastName]
           ,[CNP]
           ,[Locality]
           ,[Address]
           ,[PositionsId]
           ,[UsersId])
     VALUES
           (@firstName,@lastName,@cnp,@locality,@address,@positionID,@userID)

	SET @teacherID = SCOPE_IDENTITY()

	SAVE TRANSACTION SecoundSection;

	IF	(SELECT [SchoolManager].[dbo].ValidateEmpty(@specializationName)) = 0
	BEGIN
		SET @thirdSection = 1
		RAISERROR('Specialization is empty',16,1)
	END

	IF	(SELECT COUNT(*) FROM [SchoolManager].[dbo].[Specializations] WHERE Name = @specializationName) = 0
	BEGIN
		SET @thirdSection = 1
		RAISERROR ('Specialization does not exists',16,1)
	END

	SET @specializationID = (SELECT ID FROM [SchoolManager].[dbo].[Specializations] WHERE Name = @specializationName)

	INSERT INTO [dbo].[Teacher_Specializations]
           ([TeachersId]
           ,[SpecializationsId]
           ,[Date_of_get])
     VALUES
           (@teacherID
           ,@specializationID
           ,@dateOfGet)
	PRINT 'All the data are ok'
	COMMIT TRAN
	END TRY
	BEGIN CATCH
		IF	@@TRANCOUNT>0
		BEGIN
			IF	@firstSection = 1
			BEGIN
				PRINT 'Rollback to begining'
				ROLLBACK TRANSACTION BEFOREALL;
			END

			IF @secoundSection = 1
			BEGIN
				PRINT 'Rollback to first section'
				ROLLBACK TRANSACTION FirstSection;
			END
			IF @thirdSection = 1
			BEGIN
				PRINT 'Rollback to secound section'
				ROLLBACK TRANSACTION SecoundSection;
			END
			--ROLLBACK TRAN
		END
		DECLARE @msg varchar(100),@severity INT ,@state INT
		SELECT @msg =ERROR_MESSAGE(), @severity=ERROR_SEVERITY(), @state=ERROR_STATE()
		RAISERROR(@msg,@severity,@state)
	END CATCH
END
GO
