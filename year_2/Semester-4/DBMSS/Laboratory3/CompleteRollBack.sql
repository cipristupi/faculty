
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ciprian
-- Create date: 
-- Description:	
-- =============================================
IF EXISTS ( SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_NAME = 'CompleteRollBack')
	BEGIN
		DROP PROC CompleteRollBack
	END
GO

CREATE PROCEDURE CompleteRollBack 
	@username NVARCHAR(50),
	@password NVARCHAR(200),
	@firstname NVARCHAR(50),
	@lastname NVARCHAR(50),
	@cnp NVARCHAR(13),
	@locality NVARCHAR(50),
	@address NVARCHAR(50),
	@positionName NVARCHAR(50),
	@specializationName NVARCHAR(50),
	@roleName NVARCHAR(50),
	@dateOfGet DATE
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @roleID INT
	DECLARE @userID INT
	DECLARE @teacherID INT
	DECLARE @positionID  INT
    DECLARE @specializationID INT
	BEGIN TRY
	BEGIN TRAN
	IF	(SELECT [SchoolManager].[dbo].ValidateEmpty(@username)) = 0
	BEGIN
		RAISERROR('Username is empty',16,1)
	END
	IF	(SELECT [SchoolManager].[dbo].ValidateEmpty(@password)) = 0
	BEGIN
		RAISERROR('Password is empty',16,1)
	END
	IF	(SELECT [SchoolManager].[dbo].ValidateEmpty(@roleName)) = 0
	BEGIN
		RAISERROR('Role name  is empty',16,1)
	END
	IF	(SELECT [SchoolManager].[dbo].ValidateEmpty(@firstname)) = 0
	BEGIN
		RAISERROR('First name is empty',16,1)
	END
	IF	(SELECT [SchoolManager].[dbo].ValidateEmpty(@lastname)) = 0
	BEGIN
		RAISERROR('Last name is empty',16,1)
	END
	IF	(SELECT [SchoolManager].[dbo].ValidateEmpty(@cnp)) = 0
	BEGIN
		RAISERROR('CNP is empty',16,1)
	END

	IF	(SELECT [SchoolManager].[dbo].ValidateEmpty(@locality)) = 0
	BEGIN
		RAISERROR('Locality is empty',16,1)
	END

	IF	(SELECT [SchoolManager].[dbo].ValidateEmpty(@address)) = 0
	BEGIN
		RAISERROR('Address is empty',16,1)
	END

	IF	(SELECT [SchoolManager].[dbo].ValidateEmpty(@positionName)) = 0
	BEGIN
		RAISERROR('Position is empty',16,1)
	END

	IF	(SELECT [SchoolManager].[dbo].ValidateEmpty(@specializationName)) = 0
	BEGIN
		RAISERROR('Specialization is empty',16,1)
	END

	
	IF	(SELECT COUNT(*) FROM [SchoolManager].[dbo].[Roles] WHERE Name = @roleName) = 0
	BEGIN
		RAISERROR ('Role does not exists',16,1)
	END

	SET @roleID = (SELECT ID FROM [SchoolManager].[dbo].[Roles] WHERE Name = @roleName)
	
	INSERT INTO [dbo].[Users]([Username],[Password],[RolesId]) VALUES(@username,@password,@roleID)

	SET @userID = SCOPE_IDENTITY()


	IF	(SELECT COUNT(*) FROM [SchoolManager].[dbo].[Positions] WHERE Name = @positionName) = 0
	BEGIN
		RAISERROR ('Position does not exists',16,1)
	END

	SET @positionID = (SELECT ID FROM [SchoolManager].[dbo].[Positions] WHERE Name = @positionName)


	INSERT INTO [dbo].[Teachers]
           ([FirstName]
           ,[LastName]
           ,[CNP]
           ,[Locality]
           ,[Address]
           ,[PositionsId]
           ,[UsersId])
     VALUES
           (@firstName,@lastName,@cnp,@locality,@address,@positionID,@userID)

	SET @teacherID = SCOPE_IDENTITY()

	IF	(SELECT COUNT(*) FROM [SchoolManager].[dbo].[Specializations] WHERE Name = @specializationName) = 0
	BEGIN
		RAISERROR ('Specialization does not exists',16,1)
	END


	SET @specializationID = (SELECT ID FROM [SchoolManager].[dbo].[Specializations] WHERE Name = @specializationName)

	INSERT INTO [dbo].[Teacher_Specializations]
           ([TeachersId]
           ,[SpecializationsId]
           ,[Date_of_get])
     VALUES
           (@teacherID
           ,@specializationID
           ,@dateOfGet)
	PRINT 'All the data are ok'
	COMMIT TRAN
	END TRY
	BEGIN CATCH
		IF	@@TRANCOUNT>0
		BEGIN
			ROLLBACK TRAN
		END
		declare @msg varchar(100),@severity int ,@state int
		SELECT @msg =ERROR_MESSAGE(), @severity=ERROR_SEVERITY(), @state=ERROR_STATE()
		RAISERROR(@msg,@severity,@state)
	END CATCH
END
GO
