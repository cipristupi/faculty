﻿namespace DataSet1
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.parent = new System.Windows.Forms.DataGridView();
			this.child = new System.Windows.Forms.DataGridView();
			this.loadChild = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.loadParent = new System.Windows.Forms.Button();
			this.button1 = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.parent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.child)).BeginInit();
			this.SuspendLayout();
			// 
			// parent
			// 
			this.parent.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.parent.Location = new System.Drawing.Point(13, 13);
			this.parent.Name = "parent";
			this.parent.Size = new System.Drawing.Size(529, 187);
			this.parent.TabIndex = 0;
			// 
			// child
			// 
			this.child.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.child.Location = new System.Drawing.Point(13, 225);
			this.child.Name = "child";
			this.child.Size = new System.Drawing.Size(529, 187);
			this.child.TabIndex = 1;
			// 
			// loadChild
			// 
			this.loadChild.Location = new System.Drawing.Point(560, 39);
			this.loadChild.Name = "loadChild";
			this.loadChild.Size = new System.Drawing.Size(129, 41);
			this.loadChild.TabIndex = 2;
			this.loadChild.Text = "loadChild";
			this.loadChild.UseVisualStyleBackColor = true;
			this.loadChild.Click += new System.EventHandler(this.loadChild_Click);
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(560, 298);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(129, 41);
			this.button2.TabIndex = 3;
			this.button2.Text = "saveChild";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// loadParent
			// 
			this.loadParent.Location = new System.Drawing.Point(560, 106);
			this.loadParent.Name = "loadParent";
			this.loadParent.Size = new System.Drawing.Size(129, 41);
			this.loadParent.TabIndex = 4;
			this.loadParent.Text = "Load Parent";
			this.loadParent.UseVisualStyleBackColor = true;
			this.loadParent.Click += new System.EventHandler(this.loadParent_Click);
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(560, 159);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(129, 41);
			this.button1.TabIndex = 5;
			this.button1.Text = "button1";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(701, 424);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.loadParent);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.loadChild);
			this.Controls.Add(this.child);
			this.Controls.Add(this.parent);
			this.Name = "Form1";
			this.Text = "Form1";
			((System.ComponentModel.ISupportInitialize)(this.parent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.child)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.DataGridView parent;
		private System.Windows.Forms.DataGridView child;
		private System.Windows.Forms.Button loadChild;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button loadParent;
		private System.Windows.Forms.Button button1;
	}
}

