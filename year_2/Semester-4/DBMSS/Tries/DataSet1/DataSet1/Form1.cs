﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataSet1
{
	public partial class Form1 : Form
	{
		string connString = @"Data Source=.\sql;Initial Catalog=SchoolManager;Integrated Security=True";
		private SqlConnection conn;
		DataSet ds;
		SqlDataAdapter sda1, sda2;
		public Form1()
		{
			//SqlConnection conn = new SqlConnection(connection);
			//SqlDataAdapter adapter = new SqlDataAdapter();
			//adapter.SelectCommand = new SqlCommand(query, conn);
			//adapter.Fill(dataset);
			InitializeComponent();
			conn = new SqlConnection(connString);
			ds = new DataSet();
			ds.Tables.Add("Students");
			ds.Tables.Add("Users");
			sda1 = new SqlDataAdapter("Select * from Users",conn);
			sda2 = new SqlDataAdapter("Select * from Students", conn);
			sda1.Fill(ds, "Users");
			sda2.Fill(ds, "Students");
			parent.DataSource = ds.Tables["Users"];
			child.DataSource = ds.Tables["Students"];
		}

		private void loadParent_Click(object sender, EventArgs e)
		{

		}

		private void button2_Click(object sender, EventArgs e)
		{
			try
			{
				SqlCommandBuilder builder = new SqlCommandBuilder(sda2);


				builder.GetUpdateCommand();

				//Without the SqlCommandBuilder this line would fail

				sda2.Update(ds, "Students");
			}
			catch (Exception ex)
			{
				Debug.WriteLine(ex.ToString());
			}
		}

		private void button1_Click(object sender, EventArgs e)//save parent
		{
			try
			{
				SqlCommandBuilder builder = new SqlCommandBuilder(sda1);
				builder.GetUpdateCommand();
				sda1.Update(ds, "Users");
				sda1.Fill(ds, "Users");
			}
			catch (Exception ex)
			{
				Debug.WriteLine(ex.ToString());
			}
		}

		private void loadChild_Click(object sender, EventArgs e)
		{

		}
	}
}
