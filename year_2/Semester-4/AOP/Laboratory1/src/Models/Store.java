/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import Repository.IMainRepository;
import Repository.MainRepository;
import java.util.ArrayList;
import java.util.Observable;

/**
 *
 * @author Ciprian
 */
public class Store extends Observable {
    private MainRepository repository;
    public Store(MainRepository repo)
    {
        repository =  repo;
    }
    
    public void AddProduct(Product p)
    {
        repository.Add(p);
        setChanged();
        notifyObservers();
    }
    public void UpdateProduct(Product p)
    {
        repository.Update(p);
        setChanged();
        notifyObservers();
    }
    
    public void DeleteProduct(int code)
    {
        repository.Delete(code);
        setChanged();
        notifyObservers();
    }
    
    public ArrayList<Product> GetAll()
    {
        return repository.GetAll();
    }
    public Product GetByCode(int code)
    {
        return repository.GetByCode(code);
    }
}
