package Aspects;

public interface Observer {
	public void update(Object data);
}
