package Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import Models.Product;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

public class DAOrepository extends JdbcDaoSupport implements IMainRepository{

	public DAOrepository(){
		
	}

	@Override
	public void Add(Product b){
		getJdbcTemplate().update("INSERT INTO Products (Code,Name,Price,Quantity) VALUES (?,?,?,?)", b.getCode(), b.getName(), b.getPrice(), b.getQuantity());
	}
	
	@Override
	public void Update(Product b){
		getJdbcTemplate().update("UPDATE Products SET Code=?, Name=?, Price=?, Quantity=? WHERE Code=?", b.getCode(), b.getName(), b.getPrice(), b.getQuantity(), b.getCode());
	}
	
	@Override
	public void Delete(int code){
		getJdbcTemplate().update("DELETE FROM Products WHERE Code=?", code);
	}
	
	@Override
	public ArrayList<Product> GetAll(){
		ArrayList<Product> list = (ArrayList<Product>) (getJdbcTemplate().query("SELECT * FROM Products", new PartMapper()));
		return list;
	}
	
	public Product GetByCode(int code){
		ArrayList<Product> list = (ArrayList<Product>) getJdbcTemplate().query("SELECT * FROM Products WHERE Code=?",  new PartMapper(), code);
		if(list.size() != 0)
			return list.get(0);
		return null;
	}
	
	private static class PartMapper implements RowMapper<Product> {
        public Product mapRow(ResultSet rs, int rowNum) throws SQLException {
        	String name = rs.getString("Name");
        	int code = rs.getInt("Code");
            float price = rs.getFloat("Price");
            int quantity = rs.getInt("Quantity");
            Product b = new Product(name, code, price, quantity);
            return b;
        }
    }
	
}
