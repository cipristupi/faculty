/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Repository;

import Models.Product;
import java.util.ArrayList;

/**
 *
 * @author Ciprian
 */
public interface IMainRepository {
    void Add(Product p);
    void Delete(int code);
    void Update(Product p);
    ArrayList<Product> GetAll();
}
