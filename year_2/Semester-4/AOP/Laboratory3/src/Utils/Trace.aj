package Utils;
import org.apache.logging.log4j.Logger; 
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Level;
//import org.apache.logging.log4j.core.Logger;
import org.aspectj.lang.Signature;

import Repository.MainRepository;

public aspect Trace {
	private static Logger l = LogManager.getLogger("store");

    public Trace() {
        System.out.println("Welcome!\n");
    }

    pointcut tracePublicMethods(): execution(* *(..));

    before(): tracePublicMethods(){
        Signature signature = thisJoinPoint.getSignature();
        l.info("[Entering:] " + signature.getDeclaringTypeName() + "." + signature.getName());
    }

    after(): tracePublicMethods(){

        Signature signature = thisJoinPoint.getSignature();
        l.info("[Exiting:] " + signature.getDeclaringTypeName() + "." + signature.getName());
    }

}
