/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Repository;

import Models.Product;
import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Logger;

/**
 *
 * @author Ciprian
 */
public class MainRepository implements IMainRepository {

    ArrayList<Product> products;
    String filePath = null;
    //private static Logger logger = Logger.getLogger("store");

    public MainRepository() {
       // logger.info("[Entering] ");
        filePath = System.getProperty("user.dir") + "\\src\\Repository\\Store.sqlite";
    }

    @Override
    public void Add(Product p) {
        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + filePath);
            c.setAutoCommit(false);
            stmt = c.createStatement();
            //String sql = String.format("INSERT INTO Products (Code,Name,Price,Quantity) VALUES (%1$s, %2$s, %3$s, %4$s);", p.getCode(), p.getName(), p.getPrice(), p.getQuantity());
            String sql = "INSERT INTO Products (Code,Name,Price,Quantity) VALUES ("+ p.getCode()+",'"+ p.getName()+"',"+ p.getPrice()+","+ p.getQuantity()+");";
            stmt.executeUpdate(sql);
            stmt.close();
            c.commit();
            c.close();
           // logger.info("[Add:] " + p);
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
           // logger.info("[Error Add:] " + e.getClass().getName() + ": " + e.getMessage());
        }
    }

    @Override
    public void Delete(int code) {
        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + filePath);
            c.setAutoCommit(false);
            stmt = c.createStatement();
            String sql = String.format("DELETE FROM Products WHERE Code =%1$s;", code);
            stmt.executeUpdate(sql);
            c.commit();
            stmt.close();
            c.close();
           // logger.info("[Delete:] " + code);
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
          //  logger.info("[Error Delete:] " + e.getClass().getName() + ": " + e.getMessage());
        }
    }

    @Override
    public void Update(Product p) {
        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + filePath);
            c.setAutoCommit(false);
            stmt = c.createStatement();
            String sql = String.format("UPDATE Products SET Name = '%1$s',Price = %2$s,Quantity = %3$s , Code = %4$s WHERE Code = %4$s;", p.getName(), p.getPrice(), p.getQuantity(), p.getCode(),p.getCode());
            stmt.executeUpdate(sql);
            c.commit();
            stmt.close();
            c.close();
           // logger.info("[Update:] " + p);
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
           // logger.info("[Error Update:] " + e.getClass().getName() + ": " + e.getMessage());
        }
    }

    @Override
    public ArrayList<Product> GetAll() {
        ArrayList<Product> result = new ArrayList<>();
        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + filePath);
            c.setAutoCommit(false);
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM Products;");
            while (rs.next()) {
                int code = rs.getInt("Code");
                String name = rs.getString("Name");
                int quantity = rs.getInt("Quantity");
                float price = rs.getFloat("Price");
                result.add(new Product(name, code, price, quantity));
            }
            rs.close();
            stmt.close();
            c.close();
           // logger.info("[GetAll] ");
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
           // logger.info("[Error GetAll:] " + e.getClass().getName() + ": " + e.getMessage());
        }
        return result;
    }

    public Product GetByCode(int code) {
        Connection c = null;
        Statement stmt = null;
        Product p = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + filePath);
            c.setAutoCommit(false);
            String sql = String.format("SELECT * FROM Products WHERE Code =%1$s;",code);
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                int codeDb = rs.getInt("Code");
                String name = rs.getString("Name");
                int quantity = rs.getInt("Quantity");
                float price = rs.getFloat("Price");
                p = new Product(name, code, price, quantity);
            }
            rs.close();
            stmt.close();
            c.close();
           // logger.info("[GetByCode] ");
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
           // logger.info("[Error GetByCode:] " + e.getClass().getName() + ": " + e.getMessage());
        }
        return p;
    }
}
