/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

/**
 *
 * @author Ciprian
 */
public class CustomFormatter extends Formatter {

    @Override
    public String format(LogRecord record) {
        return "["+record.getLoggerName()+"] "+new Date(record.getMillis())+" ["+record.getLevel()+"] "+record.getMessage()+System.getProperty("line.separator");
    }
    
}
