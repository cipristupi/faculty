package Aspects;

//import Models.Store;
import laboratory1.MainForm;
import java.util.List;
import java.util.ArrayList;
import Controller.MainController;
import Utils.*;

import org.aspectj.lang.annotation.SuppressAjWarnings;

public aspect StoreObserverPattern {
	declare parents: MainController implements Subject;
	declare parents: MainForm implements Observer;

	private List<Observer> Subject.observers = new ArrayList<Observer>();

	public void Subject.addObserver(Observer obs) {
		System.out.println("Adding observer");
		observers.add(obs);
	}

	public void Subject.removeObserver(Observer obs) {
		System.out.println("Removing observer");
		observers.remove(obs);
	}

	public void Subject.notifyObservers(Object o) {
		for (Observer ob : observers) {
			ob.update(o);
		}
	}

	pointcut observed(MainController str): execution(@SubjectChanged * *(..)) && this(str);

	MainController store;

	// adding an observer
	@SuppressAjWarnings({ "adviceDidNotMatch" })
after(MainController str, MainForm strf): initialization(laboratory1.MainForm.new(*))&&this(strf)&&args(str){
		str.addObserver(strf);
		store = str;
	}

	// observers notification
	after(MainController str) returning: observed(str){
		System.out.println("Inside ObserverAspect: notifyObservers");
		str.notifyObservers(null);
	}

	// observer action
	public void MainForm.update(Object o) {
		System.out.println("Inside ObserverAspect: MainForm.update ");
		// updateTable();
		updateTable();
	}

	// removing an observer
	after(MainForm strf):execution(* MainForm.close())&& this(strf){
		store.removeObserver(strf);
	}

}
