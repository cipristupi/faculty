package Aspects;

import org.apache.logging.log4j.LogManager;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Around; 

import Models.Store;

@Aspect
public class PerformanceMonitoring{
	
	private static org.apache.logging.log4j.Logger log = LogManager.getLogger(Store.class);
	
	@Around("@annotation(Annotations.Monitored)")
	public Object calculatePerformance(ProceedingJoinPoint jp) throws Throwable{
		final long start = System.currentTimeMillis();
		final Object result = jp.proceed();
		final long duration = System.currentTimeMillis() - start;
		log.info("Method " + jp.getSignature().getName() + " from " + jp.getSignature().getDeclaringTypeName() + " took " + duration + " miliseconds.");;
		return result;
	}
}
