
package laboratory1;

import Controller.MainController;
import Models.Store;
import Repository.MainRepository;

import javax.swing.*;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Laboratory1 {

    /**
     * @param args the command line arguments
     *//*
     *
     **
    public static void main(String[] args) {
        // TODO code application logic here
        MainRepository repo =new MainRepository();
        Store store =new Store(repo);
        MainController controller =new MainController(store);
        MainForm form1 =new MainForm(controller);
        //store.addObserver(form1);
        form1.setVisible(true);
        MainForm form2 =new MainForm(controller);
        form2.setVisible(true);
        //store.addObserver(form2);
    }*/
	public static void main(String[] args){
		
	    ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");

		final MainController ctrl = (MainController) context.getBean("controller");
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
            	MainForm form1 = new MainForm(ctrl);
            	form1.setVisible(true);
            }
        });
        
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
            	MainForm form2 = new MainForm(ctrl);
            	form2.setVisible(true);
            }
        });
       
        ((ClassPathXmlApplicationContext) context).close();
	}
    
}
