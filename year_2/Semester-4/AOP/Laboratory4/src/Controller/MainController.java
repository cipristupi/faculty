/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;
import Utils.SubjectChanged;

import Models.Product;
import Models.Store;

import java.util.ArrayList;

/**
 *
 * @author Ciprian
 */
public class MainController {
    private Store store;
    public MainController(Store str)
    {
        store =str;
    }
    @SubjectChanged
    public void AddProduct(Product p)
    {
        store.AddProduct(p);
    }
    @SubjectChanged
    public void DeleteProduct(int code)
    {
        store.DeleteProduct(code);
    }
    @SubjectChanged
    public void UpdateProduct(Product p)
    {
        store.UpdateProduct(p);
    }
    
    public ArrayList<Product> GetAll()
    {
        return store.GetAll();
    }
    
    public ArrayList<Product> GetByName(String name)
    {
        ArrayList<Product> result =new ArrayList<Product>();
        ArrayList<Product> products =store.GetAll();
        for(Product p : products)
        {
            if(p.getName().toLowerCase().contains(name.toLowerCase()))
                result.add(p);
        }
        return result;
    }
    @SubjectChanged
    public void AddOrder(int code, int quantity)
    {
        Product p = this.GetByCode(code);
        p.setQuantity(p.getQuantity() - quantity);
        store.UpdateProduct(p);
    }
    
    public Product GetByCode(int code)
    {
        return store.GetByCode(code);
    }
}
