#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>
#include <float.h>

int c;

//time out mechanism
void time_out(int signalValue)
{
    int32_t r = -1;
    r = htonl(r);
    printf("Time out.\n");
    send(c, &r, sizeof(int32_t), 0);
    close(c);
    exit(1);
}

//Return values
// 1 valid float
// 2 valid int
// -1 invalid string
int validateString(char* s)
{
    int countPoint =0;
    int i;
    for(i=0; i<strlen(s); i++)
    {
        if(s[i] == '.')
            countPoint++;
    }
    if(countPoint > 1) //
        return -1;
    if(countPoint == 1)//float
        return 1;
    if(s[0] == '-' && countPoint == 0)//validation for signed ints
        return -1;
    return 2;//unsigned int
}

void ProcessesClient()
{
    int code,rc, validString,a;
    int32_t r;
    uint32_t intConvertedNumber;
    float sum, floatConvertedNumber;
    char chr;
    char* str;
    char *errorConversion;
    struct sockaddr_in server;

    if (c < 0)
    {
        fprintf(stderr, "Error establish connection with client.\n");
        exit(1);
    }
    else
        printf("A new client had connected\n");

    signal(SIGALRM, time_out);
    alarm(20);

    r = 0; //error code
    sum = 0; // the result , the sum of number getted from client
    while(1)
    {
        str = malloc(sizeof(float));
        do
        {
            rc=recv(c,&chr,1,0);
            //printf("We received %d characters. And character %c \n", rc,chr);
            if (rc == 1)
                alarm(20);
            if (rc != 1)
            {
                r = -1;
                break;
            }
            if(chr != NULL)
            {
                strcat(str,&chr);
            }
            if(chr == '$')
            {
                break;
            }
        }
        while(chr != NULL);

        printf("Number %s , Length %d \n",str,strlen(str));
        if(chr == '$')
            break;
        if (rc != 1)
        {
            r = -1;
            break;
        }
        alarm(20);
        validString =validateString(str);
        if(validString == 2)
        {
            //int
            intConvertedNumber = strtol(str, &errorConversion,10);
            if(*errorConversion != 0)
            {
                r = -3;
            }
            else
            {
                sum += intConvertedNumber;
            }

        }
        if(validString == 1)
        {
            //float
            floatConvertedNumber = strtof(str, &errorConversion);
            if(*errorConversion != 0)
            {
                r = -3;
            }
            else
            {
                sum += floatConvertedNumber;
            }

        }
        if(validString == -1)
        {
            r =-3;
        }
        if(r == -3)
        {
            printf("Error conversion.\n");
            r = htonl(r);
            a = send(c, &r, sizeof(int32_t), 0);
            r = 0;
        }
        free(str);//free the space memory for the next string
        printf("==========> Sum is %f \n",sum);
    }
    alarm(0); // stop the timer
    if(r == 0)
    {
        r = htonl(r);
        a = send(c, &r, sizeof(int32_t), 0);
        r = ntohl(r);
        sum = htonl(sum);
        send(c, &sum, sizeof(float), 0);
        sum = ntohl(sum);
        close(c);
    }
    else
    {
        r = htonl(r);
        a = send(c, &r, sizeof(int32_t), 0);//send the error code in binary format
        r = ntohl(r);
    }
    if (r >= 0)
        printf("Connection closed succesfully. The sended sum %f \n", sum);
    else
    {
        printf("Connection closed with error. Error code %d \n", r);
        exit(1);
    }

    exit(0);
}
int main()
{
    int s, l, code;
    struct sockaddr_in client, server;

    s = socket(PF_INET, SOCK_STREAM, 0);
    if (s < 0)
    {
        fprintf(stderr, "Error at creating the server.\n");
        return 1;
    }

    memset(&server, 0, sizeof(struct sockaddr_in));
    server.sin_family = AF_INET;
    server.sin_port = htons(4321);
    server.sin_addr.s_addr = INADDR_ANY;

    code = bind(s, (struct sockaddr *) &server, sizeof(struct sockaddr_in));
    if (code < 0)
    {
        fprintf(stderr, "Error at binding. The port is used.\n");
        return 1;
    }

    listen(s, 5);

    while (1)
    {
        memset(&client, 0, sizeof(client));
        l = sizeof(client);

        printf("We wait for client to connect.\n");
        c = accept(s, (struct sockaddr *) &client, &l);
        //printf("S-a conectat clientul cu adresa %s si portul %d.\n", inet_ntoa(client.sin_addr), ntohs(client.sin_port));
        //printf("The client with address %s and port %d just connect.\n", inet_ntoa(client.sin_addr), ntohs(client.sin_port));

        if (fork() == 0)
        {
            ProcessesClient();
        }
    }
}
