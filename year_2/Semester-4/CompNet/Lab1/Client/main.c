#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <float.h>

#define max 100

int main()
{
    int c, cod;
    int32_t r, e;
    float sumFromServer;
    struct sockaddr_in server;
    char* s = NULL;

    c = socket(PF_INET, SOCK_STREAM, 0);
    if (c < 0)
    {
        fprintf(stderr, "Eroare la creare socket client.\n");
        return 1;
    }

    memset(&server, 0, sizeof(struct sockaddr_in));
    server.sin_family = AF_INET;
    server.sin_port = htons(4321);
    server.sin_addr.s_addr = inet_addr("127.0.0.1");

    cod = connect(c, (struct sockaddr *) &server, sizeof(struct sockaddr_in));
    if (cod < 0)
    {
        fprintf(stderr, "Eroare la conectarea la server.\n");
        return 1;
    }
    while(1)
    {
        s = malloc(sizeof(float));
        printf("Dati un numar pentru trimis la server: ");
        gets(s);
        cod = send(c, s, strlen(s)+1, 0);
        if (cod != strlen(s)+1) //check if all bytes was send to server
        {
            fprintf(stderr, "Eroare la trimiterea datelor la server.\n");
            break;
            return 1;
        }
        /*cod = recv(c, &r, sizeof(int32_t), MSG_WAITALL);
        if(cod >= 1)
        {
            break;
        }*/
        if(strcmp(s,"$") == 0)
            break;
        free(s);
    }
    cod = recv(c, &r, sizeof(int32_t), 0);
    e = ntohl(r);
    switch(e)
    {
        case 0:
        {
            cod = recv(c, &sumFromServer, sizeof(float), MSG_WAITALL);
            if(cod != 0 )
            {
                sumFromServer = ntohl(sumFromServer);
                printf("The sum is %f",sumFromServer);
            }
            break;
        }
        case -1:
        {
            printf("Server time out \n");
            break;
        }
        case -3:
        {
            printf("Invalid string \n");
            break;
        }
        case -2:
        {
            printf("Overflow error.\n");
            break;
        }
    }
    close(c);
}
