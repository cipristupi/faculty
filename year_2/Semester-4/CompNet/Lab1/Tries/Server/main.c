#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>
#include<float.h>

int c;

// Mecanismul de time-out. Paragraful 3.
void time_out(int signalValue)
{
    int32_t r = -1;
    r = htonl(r);
    printf("Time out.\n");
    send(c, &r, sizeof(int32_t), 0);
    close(c); // desi nu am primit nimic de la client in 20 secunde, inchidem civilizat conexiunea cu acesta
    exit(1);
}
//Return values
// 1 valid float
// 2 valid int
// -1 invalid string
int validateString(char* s)
{
    int countPoint =0;
    int i;
    for(i=0; i<strlen(s); i++)
    {
        if(s[i] == '.')
            countPoint++;
    }
    if(countPoint > 1)
        return -1;
    if(s[0] == '-' && countPoint == 1)
        return 1;
    if(s[0] == '-' && countPoint == 0)
        return -1;
    return 2;
}

void ProcessesClient()
{
    int code;
    int rc;
    float sum;
    //uint8_t s;
    char chr;
    int32_t r;
    char* str;
    struct sockaddr_in server;

    if (c < 0)
    {
        //fprintf(stderr, "Eroare la stabilirea conexiunii cu clientul.\n");
        fprintf(stderr, "Error establish connection with client.\n");
        exit(1);
    }
    else
        //printf("Un nou client s-a conectat cu succes.\n");
        printf("A new client had connected\n");

    signal(SIGALRM, time_out);
    alarm(20);

    //r = 0; // rezultatul, numarul de caractere spatii primite de la client
    sum = 0; // the result , the sum of number getted from client
    while(1)
    {
        str = malloc(sizeof(float));
        do
        {
            rc=recv(c,&chr,1,0);
            printf("Am primit %d caractere. Si caracterul %c \n", rc,chr);
            if (rc == 1) // citire cu succes a unui caracter
                alarm(20);  // resetam timerul si asteptam din nou 10 secunde urmatorul caracter
            if (rc != 1)
            {
                r = -1;
                break;
            }
            if(chr != NULL)
            {
                strcat(str,&chr);
            }
            if(chr == '$')
            {
                break;
            }
        }
        while(chr != NULL);
        //make sum
        printf("=================>Number %s , Length %d \n",str,strlen(str));
        if(chr == '$')
            break;
        if (rc != 1)
        {
            r = -1;
            break;
        }
        printf("Validation code: %d",validateString(str));
        free(str);
    }

    alarm(0); // oprim timerul
    r=0;
    r = htonl(r);
    send(c, &r, sizeof(int32_t), 0);
    r = ntohl(r);


    sum = htonl(sum);
    send(c, &sum, sizeof(FLT_MAX), 0);
    sum = ntohl(sum);

    close(c);

    if (r >= 0)
        printf("Connection closed succesfully. The sended sum %f", sum);
    else
    {
        printf("Connection closed with error. Error code %d \n", r);
        exit(1);
    }

    exit(0);
}

void tratare()
{
    int cod;
    int32_t r;
    uint8_t b;
    char* s =malloc(sizeof(float));
    // Observatie: Deoarece dimensiunea tipului int difera de la platforma la platforma,
    // (spre exemplu, in Borland C in DOS e reprezentat pe 2 octeti, iar in C sub Linux pe
    // 4 octeti) este necesara utilizarea unor tipuri intregi standard. A se vedea
    // stdint.h.
    struct sockaddr_in server;

    if (c < 0)
    {
        fprintf(stderr, "Eroare la stabilirea conexiunii cu clientul.\n");
        exit(1);
    }
    else
        printf("Un nou client s-a conectat cu succes.\n");

    signal(SIGALRM, time_out);
    alarm(10);

    r = 0; // rezultatul, numarul de caractere spatii primite de la client
    while(1 == 1)
    {
        do
        {
            cod = recv(c, &b, 1, 0);
            printf("Am primit %d caractere.\n", cod);

            if (cod == 1) // citire cu succes a unui caracter
                alarm(10);  // resetam timerul si asteptam din nou 10 secunde urmatorul caracter

            if (cod != 1)
            {
                r = -1;
                break;
            }

            strcat(s,b);

        }
        while (b != 0); // sirul de caractere de la client se considera terminat la intalnirea caracterului 0 (NULL, '\0')
        // Paragraful 2 - terminam citirea sirului de la client la primirea caracterului NULL

    }

    alarm(0); // oprim timerul

    printf("stringul citit %s",s);
    r = htonl(r);
    send(c, &r, sizeof(int32_t), 0);
    r = ntohl(r);

    close(c);

    if (r >= 0)
        printf("Am inchis cu succes conexiunea cu un client. I-am trimis %d spatii.\n", r);
    else
    {
        printf("Am inchis cu eroare conexiunea cu un client. Cod de eroare %d.\n", r);
        exit(1);
    }

    exit(0);
    // Terminam procesul fiu - foarte important!!! altfel numarul de procese creste exponential.
    // Fiul se termina dupa ce deserveste clientul.
}
int main()
{
    int s, l, code;
    struct sockaddr_in client, server;

    s = socket(PF_INET, SOCK_STREAM, 0);
    if (s < 0)
    {
        fprintf(stderr, "Eroare la creare socket server.\n");
        return 1;
    }

    memset(&server, 0, sizeof(struct sockaddr_in));
    server.sin_family = AF_INET;
    server.sin_port = htons(4321);
    server.sin_addr.s_addr = INADDR_ANY;

    code = bind(s, (struct sockaddr *) &server, sizeof(struct sockaddr_in));
    if (code < 0)
    {
        fprintf(stderr, "Eroare la bind. Portul este deja folosit.\n");
        return 1;
    }

    listen(s, 5);

    while (1)   // deserveste oricati clienti
    {

        memset(&client, 0, sizeof(client));
        l = sizeof(client);

        printf("Astept sa se conecteze un client.\n");
        c = accept(s, (struct sockaddr *) &client, &l);
        //printf("S-a conectat clientul cu adresa %s si portul %d.\n", inet_ntoa(client.sin_addr), ntohs(client.sin_port));
        printf("The client with address %s and port %d just connect.\n", inet_ntoa(client.sin_addr), ntohs(client.sin_port));

        /* if (fork() == 0)   // server concurent, conexiunea va fi tratata de catre un proces fiu separat
         {*/
        ProcessesClient();
        //tratare();
        //}
    }
}
