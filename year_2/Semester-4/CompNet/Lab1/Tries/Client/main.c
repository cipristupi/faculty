#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <float.h>

#define max 100

int main()
{
    int c, cod;
    int32_t r;
    float sumFromServer;
    struct sockaddr_in server;
    char* s = NULL;

    c = socket(PF_INET, SOCK_STREAM, 0);
    if (c < 0)
    {
        fprintf(stderr, "Eroare la creare socket client.\n");
        return 1;
    }

    memset(&server, 0, sizeof(struct sockaddr_in));
    server.sin_family = AF_INET;
    server.sin_port = htons(4321);
    server.sin_addr.s_addr = inet_addr("127.0.0.1");

    cod = connect(c, (struct sockaddr *) &server, sizeof(struct sockaddr_in));
    if (cod < 0)
    {
        fprintf(stderr, "Eroare la conectarea la server.\n");
        return 1;
    }

    do
    {
        s = malloc(sizeof(float));
        printf("Dati un numar pentru trimis la server: ");
        gets(s);
        printf("String %s \n\n",s);
        cod = send(c, s, strlen(s)+1, 0);
        if (cod != strlen(s)+1)
        {
            fprintf(stderr, "Eroare la trimiterea datelor la server.\n");
            return 1;
        }
        free(s);
    }
    while(strcmp(s,"$") != 0);
    //while(s !='$');
    printf("HERE");
    cod = recv(c, &r, sizeof(int32_t), MSG_WAITALL);
    r = ntohl(r);
    /*if(r == 0)
    {
        cod = recv(c, &sumFromServer, sizeof(float), MSG_WAITALL);
        printf("The sum is %f",sumFromServer);
    }*/
    switch(r)
    {
    case 0:
    {
        cod = recv(c, &sumFromServer, sizeof(float), MSG_WAITALL);
        printf("The sum is %f",sumFromServer);
        break;
    }
    case -1:
    {
        printf("Error 1 \n");
        break;
    }
    }
    if (cod != sizeof(int))
    {
        fprintf(stderr, "Eroare la primirea datelor de la client.\n");
        return 1;
    }
    close(c);
}
