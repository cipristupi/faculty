// Laboratory3.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "winsock2.h"
#include <iostream>
#include <conio.h>
#include <string.h>
#include <sstream>
#include <ctime>
#include <map>
#include <list>
#include<vector>
#include <stdio.h>
#include <WinSock2.h>
#include <omp.h>
#include <conio.h>
using namespace std;

#pragma comment(lib,"ws2_32.lib") //Winsock Library
#pragma comment(lib,"Winmm.lib")

#define BUFLEN 512  //Max length of buffer
#define PORT 7777 
//#define _CRT_SECURE_NO_WARNINGS
SOCKET s;
struct sockaddr_in senders, receiver, senders2, listenAddr, broadcaster;
int slen, recv_len = sizeof(struct sockaddr_in);
WSADATA wsa;
char timeQueryString[] = "TIMEQUERY\0";
char dateQueryString[] = "DATEQUERY\0";

typedef struct {
	struct sockaddr_in addr;
	int numberOfFailedTries;
	char lastDate[20];
	char lastTime[20];

} NetworkClient;

//typedef struct {
//	string addr;
//	int numberOfFailedTries;
//	char lastDate[20];
//	char lastTime[20];
//
//} NetworkClient;
vector<NetworkClient *> networkClients;
vector<std::string> malformedData;


void printLists() {
	//clearScreen();
	system("cls");
	printf("Results below: \n");
	char senderAddr[50];
	for (int it = 0; it < networkClients.size(); it++) {
		//inet_ntop(AF_INET, &(sa.sin_addr), str, INET_ADDRSTRLEN);
		//strcpy(senderAddr, networkClients[it]->addr.sin_addr);
		//std::cout << inet_ntop(networkClients[it]->addr.sin_addr) << " - " << networkClients[it]->lastTime << " " << networkClients[it]->lastDate << std::endl;
		std::cout << inet_ntoa(networkClients[it]->addr.sin_addr) << " - " << networkClients[it]->lastTime << " " << networkClients[it]->lastDate << std::endl;
	}

	for (int it = 0; it < malformedData.size(); it++) {
		std::cout << malformedData[it] << std::endl;
	}
}

void CALLBACK sendTimeQuery(UINT uTimerID, UINT uMsg, DWORD_PTR dwUser, DWORD_PTR dw1, DWORD_PTR dw2) {
	if (sendto(s, timeQueryString, strlen(timeQueryString) + 1, 0, (sockaddr *)&broadcaster, sizeof(broadcaster)) < 0)
	{
		printf("Error in Sending.");
		return;
	}
}

void CALLBACK sendDateQuery(UINT uTimerID, UINT uMsg, DWORD_PTR dwUser, DWORD_PTR dw1, DWORD_PTR dw2) {
	if (sendto(s, dateQueryString, strlen(dateQueryString) + 1, 0, (sockaddr *)&broadcaster, sizeof(broadcaster)) < 0)
	{
		printf("Error in Sending.");
		return;
	}
}

void listenFunction()
{
	char recvBuffer[50];
	int recvBufferLen = 50, toSendLen = 100;
	char toSend[100];
	while (1)
	{
		struct sockaddr_in clientAddr;
		memset(&clientAddr, 0, sizeof(struct sockaddr_in));
		time_t currentTime;
		struct tm *localTime;
		time(&currentTime);                   // Get the current time
		localTime = localtime(&currentTime);  // Convert the current time to the local time
		string a;
		//if (recvfrom(s, recvBuffer, recvBufferLen, 0, (struct sockaddr *)&clientAddr, &slen) > 0)
		if (recvfrom(s, recvBuffer, recvBufferLen, 0, (struct sockaddr *)&clientAddr, &recv_len) > 0)
			//if (recvfrom(s, recvBuffer, recvBufferLen, 0, (struct sockaddr *)&senders, &recv_len) > 0)
		{
			cout << inet_ntoa(clientAddr.sin_addr) << endl;
			a = inet_ntoa(clientAddr.sin_addr);
			if (!strcmp(recvBuffer, timeQueryString))
			{
				a = inet_ntoa(clientAddr.sin_addr);
				strftime(toSend, toSendLen, "TIME %H:%M:%S", localTime);
				//if (sendto(s, toSend, strlen(toSend)+1, 0, (struct sockaddr *)&clientAddr, slen) < 0) {
				if (sendto(s, toSend, strlen(toSend) + 1, 0, (struct sockaddr *)&clientAddr, sizeof(clientAddr)) < 0) {
					//if (sendto(s, toSend, strlen(toSend) + 1, 0, (struct sockaddr *)&senders, sizeof(senders)) < 0) {
					perror("error");
					printf("Failed to send TIMEQUERY response!\n");
				}
			}
			else
				if (!strcmp(recvBuffer, dateQueryString))
				{
					a = inet_ntoa(clientAddr.sin_addr);
					strftime(toSend, toSendLen, "DATE %d:%m:%Y", localTime);
					if (sendto(s, toSend, strlen(toSend) + 1, 0, (struct sockaddr *)&clientAddr, sizeof(clientAddr)) < 0) {
						//if(sendto(s, toSend, strlen(toSend) + 1, 0, (struct sockaddr *)&senders, sizeof(senders)) < 0) {
						printf("Failed to send DATEQUERY response!\n");
					}
				}
				else
				{
					a = inet_ntoa(clientAddr.sin_addr);
					//Query response
					char type[5];
					strncpy(type, recvBuffer, 4);//here can be problems
					type[4] = '\0';
					NetworkClient *peer = NULL;
					bool newNetworkClient = true;
					for (int i = 0; i < networkClients.size(); i++)
					{
						char *localPeer = inet_ntoa(networkClients[i]->addr.sin_addr);
						char *currentPeer = inet_ntoa(clientAddr.sin_addr);
						//char *currentPeer = inet_ntoa(senders.sin_addr);
						if (!strcmp(localPeer, currentPeer)) {
							peer = networkClients[i];
							newNetworkClient = false;
							break;
						}
					}
					if (newNetworkClient)
					{
						peer = (NetworkClient *)malloc(sizeof(NetworkClient));
						peer->numberOfFailedTries = 0;
						peer->addr = clientAddr;
					}
					if (!strcmp(type, "TIME"))
					{
						strcpy(peer->lastTime, recvBuffer);
					}
					else
						if (!strcmp(type, "DATE"))
						{
							strcpy(peer->lastDate, recvBuffer);
						}
						else
						{
							// Malformed response
							std::string message;
							message.append(inet_ntoa(clientAddr.sin_addr));
							message.append(" - ");
							message.append(recvBuffer);
						}

					if (newNetworkClient) {
						networkClients.push_back(peer);
					}
					else {
						peer->numberOfFailedTries--;
					}
					printLists();
				}
		}
	}
}

int main(int argc, char* argv[])
{
	slen = sizeof(struct sockaddr_in);
	//Initialise winsock
	printf("\nInitialising Winsock...");
	if (WSAStartup(MAKEWORD(2, 2), &wsa) != 0)
	{
		printf("Failed. Error Code : %d", WSAGetLastError());
		exit(EXIT_FAILURE);
	}
	printf("Initialised.\n");

	//Create a socket
	if ((s = socket(AF_INET, SOCK_DGRAM, 0)) == INVALID_SOCKET)
	{
		printf("Could not create socket : %d", WSAGetLastError());
	}
	printf("Socket created.\n");

	char broadcast = '1';

	if (setsockopt(s, SOL_SOCKET, SO_BROADCAST, &broadcast, sizeof(broadcast)) < 0)
	{
		printf("Error in setting Broadcast option");
		_getch();
		closesocket(s);
		return 0;
	}

	broadcaster.sin_family = AF_INET;
	broadcaster.sin_port = htons(PORT);
	broadcaster.sin_addr.s_addr = inet_addr("192.168.137.255");//Here need to change to network broadcast address

	struct sockaddr_in listenAddr;
	memset(&listenAddr, 0, sizeof(struct sockaddr_in));

	listenAddr.sin_family = AF_INET;
	listenAddr.sin_addr.s_addr = INADDR_ANY;
	listenAddr.sin_port = htons(PORT);

	if (bind(s, (struct sockaddr *)&listenAddr, sizeof(struct sockaddr_in)) < 0) {
		//if (bind(s, (struct sockaddr *)&listenAddr, (slen)sizeof(struct sockaddr_in)) < 0) {
		printf("Failed to bind!\n");
		return errno;
	}


	timeSetEvent(3000, 10, sendTimeQuery, (DWORD)0, (TIME_PERIODIC | TIME_KILL_SYNCHRONOUS));
	timeSetEvent(10000, 10, sendDateQuery, (DWORD)0, (TIME_PERIODIC | TIME_KILL_SYNCHRONOUS));
	//#pragma omp parallel num_threads(1) // Creating threads..
	//	{
	//#pragma omp sections // Work work..
	//				{
	//#pragma omp section
	//					{
	listenFunction();
	//				}

	//			}
	//}

	_getch();
	closesocket(s);
	return 0;
}

