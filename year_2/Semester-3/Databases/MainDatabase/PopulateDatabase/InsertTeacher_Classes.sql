INSERT INTO Teacher_Classes
SELECT c.Id AS ClassesId , t.Id AS TeacherId , p.Id AS PositionsId
FROM [SchoolManager].[dbo].[Classes] c
CROSS JOIN [Teachers] t
CROSS JOIN [Positions] p WHERE p.Name ='teacher'