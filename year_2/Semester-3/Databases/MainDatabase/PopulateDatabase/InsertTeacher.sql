-- =============================================
-- Author:		Ciprian
-- Create date: 04.11.2014
-- Description:	This Stored Procedure insert a given number of teachers
-- =============================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--Here check if exists the store procedure. If exist we delete it and recreate
IF EXISTS ( SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_NAME = 'InsertTeacher')
	BEGIN
		DROP PROC InsertTeacher
	END
GO


CREATE PROCEDURE [dbo].[InsertTeacher]
	@teacherNumber BIGINT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE  @counter INT
	DECLARE @currentT NVARCHAR(100)
	DECLARE @currentID INT
	DECLARE @positionID INT

	SET @counter=0
	WHILE @counter < @teacherNumber
		BEGIN
		SET @currentT = CONCAT('prof',@counter)
		INSERT INTO [dbo].[Users] ([Username],[Password],[RolesId]) VALUES(@currentT,CONCAT('pass',@counter),2)
		SELECT @currentID = ID FROM [dbo].[Users] WHERE [Username] = @currentT
		SELECT @positionID = ID FROM [dbo].[Positions] WHERE [Name] = 'teacher'
		INSERT INTO [dbo].[Teachers]
           ([FirstName]
           ,[LastName]
           ,[CNP]
           ,[Locality]
           ,[Address]
           ,[PositionsId]
           ,[UsersId])
		VALUES
           (CONCAT('FirstName',@counter),
		     CONCAT('LastName',@counter),
           CONCAT('CNP', @counter),
           CONCAT('Locality', @counter),
           CONCAT('Address',@counter),
           @positionID,
          @currentID)
		  SET @counter = @counter+1
		END
	SELECT * FROM [dbo].[Users]
	SELECT * FROM [dbo].[Teachers]
END
GO