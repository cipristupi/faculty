-- =============================================
-- Author:		Ciprian
-- Create date: 06.11.2014
-- Description:	This Stored Procedure insert a given number of students	
-- =============================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--Here check if exists the store procedure. If exist we delete it and recreate
IF EXISTS ( SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_NAME = 'InsertStudents')
	BEGIN
		DROP PROC InsertStudents
	END
GO


CREATE PROCEDURE [dbo].[InsertStudents]
	@studentNumber BIGINT --student number per class
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @classIds TABLE(ID int)

	INSERT INTO @classIds
	SELECT Id FROM [dbo].[Classes] 

	DECLARE  @counter INT --counter for number of classes
	DECLARE @currentStudent NVARCHAR(100) -- current Student username
	DECLARE @currentClassId INT -- current id for class
	DECLARE @classNo INT
	DECLARE @innerCounter INT
	DECLARE @currentID INT
	DECLARE @dateI date

	SET @counter = 0
	SET @classNo = (SELECT COUNT(*) FROM Classes)
	PRINT @classNo
	WHILE @counter < @classNo
		BEGIN
		
		PRINT CONCAT('Counter ' ,@counter)
		SELECT @currentClassId = (SELECT TOP 1 [ID] FROM @classIds )

		SET @dateI = (SELECT GETDATE())
		
		PRINT 'HERE'
		SET @innerCounter = 0
		WHILE @innerCounter < @studentNumber
			BEGIN
			SET @currentStudent = CONCAT('std',@counter,@currentClassId)

			INSERT INTO [dbo].[Users] ([Username],[Password],[RolesId]) VALUES(@currentStudent,CONCAT('pass',@counter,@currentClassId),3)
		
			SELECT @currentID = ID FROM [dbo].[Users] WHERE [Username] = @currentStudent

			INSERT INTO [dbo].[Students]
			           ([Registration_no]
			           ,[FirstName]
			           ,[LastName]
			           ,[FirstName_Father]
			           ,[FirstName_Mother]
			           ,[Address]
			           ,[Locality]
			           ,[Phone]
			           ,[Address_Custode]
			           ,[Phone_Custode]
			           ,[CNP]
			           ,[Date_enrollment]
			           ,[Observations]
			           ,[ClassesId]
			           ,[UsersId])
			     VALUES
			           (CONCAT(@innerCounter,@currentClassId,@currentID)
			           ,CONCAT('FirstName ',@innerCounter,@currentClassId,@currentID)
			           ,CONCAT('LastName ',@innerCounter,@currentClassId,@currentID)
			           ,CONCAT('FirstName_Father ',@innerCounter,@currentClassId,@currentID)
			           ,CONCAT('FirstName_Mother ',@innerCounter,@currentClassId,@currentID)
			           ,CONCAT('Address ',@innerCounter,@currentClassId,@currentID)
			           ,CONCAT('Cluj ',@innerCounter,@currentClassId,@currentID)
			           ,CONCAT('07414',@innerCounter,@currentClassId,@currentID)
			           ,CONCAT('Custode ',@innerCounter,@currentClassId,@currentID)
			           ,CONCAT('07555',@innerCounter,@currentClassId,@currentID)
			           ,CONCAT('1911',@innerCounter,@currentClassId)
			           ,@dateI
			           ,'empty'
			           ,@currentClassId
			           ,@currentID)

			SET @innerCounter = @innerCounter + 1
			END
		DELETE FROM @classIds WHERE Id = @currentClassId
		SET @counter = @counter+1
		PRINT'AFTER SECOND WHILE'
		END

END
GO