-- =============================================
-- Author:		Ciprian
-- Create date: 27.10.2014
-- Description:	This Stored Procedure drop a table.
-- =============================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON

--Here check if exists the store procedure. If exist we delete it and recreate
IF EXISTS ( SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_NAME = 'DropTable')
	BEGIN
		DROP PROC DropTable
	END
GO

CREATE PROCEDURE [dbo].[DropTable]
	-- Add the parameters for the stored procedure here
	@tableName NVARCHAR(200),
	@isVrsControl BIT = 0 --this variable i use to insert values into Version Control table
	--If isVrsControl is 1(true) i don't insert new row into Version Control table 
	--Else I insert new row into new row into Version Control table
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Variabile declaration
	DECLARE @tableCount SMALLINT
	DECLARE @dropSQL NVARCHAR(MAX)
	--End variabile declaration

	SELECT @tableCount =  COUNT(*) FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = @TableName

	IF @tableCount = 0 
	BEGIN
		PRINT 'The given table doesnt exists'
	END
	ELSE IF	@tableCount > 1
		PRINT 'There are more table with the same name'
	ELSE
		BEGIN
			
			SET @dropSQL =  'DROP TABLE ' + QUOTENAME(@tableName)
			EXEC(@dropSQL)
			PRINT 'Delete successfully'
		END
	

END
GO
