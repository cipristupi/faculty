-- =============================================
-- Author:		Ciprian
-- Create date: 28.10.2014
-- Description:	This Stored Procedure change type of a given column from a table.
-- =============================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--Here check if exists the store procedure. If exist we delete it and recreate
IF EXISTS ( SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_NAME = 'ChangeColumnType')
	BEGIN
		DROP PROC ChangeColumnType
	END
GO


CREATE PROCEDURE [dbo].[ChangeColumnType]
	-- Add the parameters for the stored procedure here
	@tableName NVARCHAR(200),
	@columnName NVARCHAR(200),
	@columnType NVARCHAR(200),
	@isVrsControl BIT = 0 --this variable i use to insert values into Version Control table
	--If isVrsControl is 1(true) i don't insert new row into Version Control table 
	--Else I insert new row into new row into Version Control table
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @updateColumnSQL NVARCHAR(MAX)
	DECLARE @tableCount SMALLINT

	SELECT @tableCount =  COUNT(*) FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = @TableName
	IF @tableCount = 0 
	BEGIN
		PRINT 'The given table doesnt exists'
	END
	ELSE
		BEGIN
			--IF @isVrsControl = 1
				--BEGIN
				--TODO
				--Here need to add new entry into the Version Control table
				--END
			IF (EXISTS(SELECT DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = QUOTENAME(@tableName) AND COLUMN_NAME= QUOTENAME(@columnName))) --here check if the given column exists
				SET @updateColumnSQL = 'TABLE ' + QUOTENAME(@tableName)  +'	ALTER COLUMN ' + QUOTENAME(@columnName) + ' ' + QUOTENAME(@columnType)
				EXEC(@updateColumnSQL)
				PRINT 'Column updated successfully'
		END
END
GO


-- =============================================
--Improvements
-- =============================================

--In the future we can make checks if the given columns exist,
--and if doesn't we print an error message
--We can check if the given type is valid.
--SELECT * FROM sys.types