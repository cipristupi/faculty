-- =============================================
-- Author:		Ciprian
-- Create date: 30.10.2014
-- Description:	This Stored Procedure drop a default constraint from  a column to a table.
-- =============================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Here check if exists the store procedure. If exist we delete it and recreate
IF EXISTS ( SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_NAME = 'DropDefaultConstraint')
	BEGIN
		DROP PROC DropDefaultConstraint
	END
GO

CREATE PROCEDURE [dbo].[DropDefaultConstraint]
	@tableName NVARCHAR(MAX),
	@constraintName NVARCHAR(MAX),
	@isVrsControl BIT = 0 --this variable i use to insert values into Version Control table
		--If isVrsControl is 1(true) i don't insert new row into Version Control table 
		--Else I insert new row into new row into Version Control table
AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @updateColumnSQL NVARCHAR(MAX)
	DECLARE @tableCount SMALLINT
	SELECT @tableCount =  COUNT(*) FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = @TableName
	IF @tableCount = 0 
		BEGIN
			PRINT 'The given table doesnt exists'
		END
	ELSE
		BEGIN
			SET @updateColumnSQL =  'ALTER TABLE ' + QUOTENAME(@tableName) + ' DROP ' + QUOTENAME(@constraintName)
			EXEC(@updateColumnSQL)
			--PRINT 'Column updated successfully'
		END
END
GO
