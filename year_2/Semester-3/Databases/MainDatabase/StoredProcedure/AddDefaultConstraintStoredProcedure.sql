-- =============================================
-- Author:		Ciprian
-- Create date: 28.10.2014
-- Description:	This Stored Procedure add default constraint for a  given column from a table.
-- =============================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--Here check if exists the store procedure. If exist we delete it and recreate
IF EXISTS ( SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_NAME = 'AddDefaultConstraint')
	BEGIN
		DROP PROC AddDefaultConstraint
	END
GO

CREATE PROCEDURE [dbo].[AddDefaultConstraint]
	-- Add the parameters for the stored procedure here
		@tableName NVARCHAR(200),
		@columnName NVARCHAR(200),
		@nameConstraint NVARCHAR(200),
		@defaultValue NVARCHAR(200),
		@isVrsControl BIT = 0 --this variable i use to insert values into Version Control table
		--If isVrsControl is 1(true) i don't insert new row into Version Control table 
		--Else I insert new row into new row into Version Control table
AS
BEGIN
	SET NOCOUNT ON;

    DECLARE @updateColumnSQL NVARCHAR(MAX)
	DECLARE @tableCount SMALLINT
	SELECT @tableCount =  COUNT(*) FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = @TableName
	IF @tableCount = 0 
	BEGIN
		PRINT 'The given table doesnt exists'
	END
	ELSE
		BEGIN
			SET @updateColumnSQL =  'ALTER TABLE ' + QUOTENAME(@tableName) + ' ADD CONSTRAINT ' + @nameConstraint + ' DEFAULT ''' + QUOTENAME(@defaultValue) + ''' FOR ' + QUOTENAME(@columnName)
			EXEC(@updateColumnSQL)
			--PRINT 'Column updated successfully'
		END
END
GO

-- =============================================
--Improvements
-- =============================================

--In the future we can make checks if the given columns exist,
--and if doesn't we print an error message