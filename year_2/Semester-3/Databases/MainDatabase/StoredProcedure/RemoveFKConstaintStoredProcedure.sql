-- =============================================
-- Author:		Ciprian
-- Create date: 29.10.2014
-- Description:	This Stored Procedure remove a fk constraint.
-- =============================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--Here check if exists the store procedure. If exist we delete it and recreate
IF EXISTS ( SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_NAME = 'RemoveFKConstraint')
	BEGIN
		DROP PROC RemoveFKConstraint
	END
GO

CREATE PROCEDURE RemoveFKConstraint
	-- Add the parameters for the stored procedure here
	@tableName NVARCHAR(MAX), --table to alter, table where the fk will be put
	@constraintName NVARCHAR(MAX), --name for constraint
	@isVrsControl BIT = 0 --this variable i use to insert values into Version Control table
	--If isVrsControl is 1(true) i don't insert new row into Version Control table 
	--Else I insert new row into new row into Version Control table
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @removeFKSQL NVARCHAR(MAX)
	DECLARE @tableCount1 SMALLINT
	DECLARE @fkCount SMALLINT
	

	--here count the tables , if the result is 0 then we print an error message
	SELECT @tableCount1 =  COUNT(*) FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = @tableName
	

	IF @tableCount1 = 0 
		BEGIN
		PRINT 'Table doesnt exists'
		END
	ELSE
		BEGIN
			SELECT @fkCount = COUNT(*) FROM sys.foreign_keys WHERE NAME = @constraintName
			
			IF @fkCount = 0 
				BEGIN
					PRINT 'Constraint doesnt exists'
				END
			ELSE
				BEGIN				
					SET @removeFKSQL = 'ALTER TABLE ' + QUOTENAME(@tableName) + ' DROP CONSTRAINT ' + QUOTENAME(@constraintName)
					EXEC(@removeFKSQL)
					PRINT 'Message'
				END
		END


	
END
GO
