-- =============================================
-- Author:		Ciprian
-- Create date: 03.11.2014
-- Description:	This Stored Procedure make upgrade or revert on database version
-- =============================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--Here check if exists the store procedure. If exist we delete it and recreate
IF EXISTS ( SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_NAME = 'VersionControle')
	BEGIN
		DROP PROC VersionControle
	END
GO

CREATE PROCEDURE VersionControle
	@desireVersion BIGINT
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @currentVrs BIGINT
	DECLARE @versions TABLE(ID bigint)
	
	SELECT @currentVrs = ID FROM DatabaseVersions WHERE CurrentVersion = 1
	IF @desireVersion < @currentVrs --downgrade
		BEGIN
			INSERT INTO @versions
			SELECT ID FROM DatabaseVersions WHERE ID<@currentVrs AND ID > @desireVersion
		END
	ELSE
		--upgrade
		BEGIN
			INSERT INTO @versions
			SELECT ID FROM DatabaseVersions WHERE ID>@currentVrs AND ID < @desireVersion
		END

END
GO

-- =============================================
--Documentation
-- =============================================

--1 - Create Table [Parameters: ]
--2 - Drop Table [Parameters: tableName]

--3 - Add Column [Parameters: tableName, columnName, columnType]
--4 - Drop Column [Parameters: tableName, columnName]

--5 - Create FK Constraint [Parameters: firstTableName, secondTableName, constraintName, firstColumnName, secondColumnName]
--6 - Drop FK Constraint [Parameters: tableName, constraintName]

--7 - Add Default Constraint [Parameters: tableName, columnName, nameConstraint, defaultValue]
--8 - Remove Default Constraint [Parameters: tableName, constraintName]
 
--9 - Change Column Type [Parameters: tableName, columnName, columnType]