-- =============================================
-- Author:		Ciprian
-- Create date: 07.11.2014
-- Description:	This Stored Procedure add a new table
-- =============================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--Here check if exists the store procedure. If exist we delete it and recreate
IF EXISTS ( SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_NAME = 'AddNewTable')
	BEGIN
		DROP PROC AddNewTable
	END
GO


CREATE PROCEDURE [dbo].[AddNewTable]
	-- Add the parameters for the stored procedure here
	@tableName NVARCHAR(200),
	@tableColumns NVARCHAR(MAX),
	@isVrsControl BIT = 0 --this variable i use to insert values into Version Control table
	--If isVrsControl is 1(true) i don't insert new row into Version Control table 
	--Else I insert new row into new row into Version Control table
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @addTableSQL NVARCHAR(MAX)
	DECLARE @tableCount SMALLINT

	SELECT @tableCount =  COUNT(*) FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = @TableName
	IF @tableCount = 1 
	BEGIN
		PRINT 'The given table exists'
	END
	ELSE
		BEGIN
			PRINT @tableColumns
			SET @addTableSQL =  'CREATE TABLE '+ QUOTENAME(@tableName) + ' ( ' + @tableColumns + ' );'
			EXEC(@addTableSQL)
			PRINT 'Table added successfully'
		END
END
GO


-- =============================================
--Improvements
-- =============================================
--We can check if the given type is valid.
--SELECT * FROM sys.types