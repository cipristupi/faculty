-- =============================================
-- Author:		Ciprian
-- Create date: 31.10.2014
-- Description:	This Stored Procedure create a table with a single column
-- =============================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--Here check if exists the store procedure. If exist we delete it and recreate
IF EXISTS ( SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_NAME = 'CreateTable')
	BEGIN
		DROP PROC CreateTable
	END
GO
CREATE PROCEDURE CreateTable
	@tableName  NVARCHAR(MAX),
	@columnName NVARCHAR(MAX),
	@columnType NVARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @sqlQuery NVARCHAR(MAX)

	SET @sqlQuery = 'CREATE TABLE ' + QUOTENAME(@tableName) +' ( ' + QUOTENAME(@columnName) + ' ' + QUOTENAME(@columnType) +' )'
	EXEC(@sqlQuery)
END
GO
