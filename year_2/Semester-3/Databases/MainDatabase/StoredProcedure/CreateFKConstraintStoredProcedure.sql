-- =============================================
-- Author:		Ciprian
-- Create date: 29.10.2014
-- Description:	This Stored Procedure add a fk constraint.
-- =============================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--Here check if exists the store procedure. If exist we delete it and recreate
IF EXISTS ( SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_NAME = 'CreateFKConstraint')
	BEGIN
		DROP PROC CreateFKConstraint
	END
GO

CREATE PROCEDURE CreateFKConstraint
	-- Add the parameters for the stored procedure here
	@firstTableName NVARCHAR(MAX), --table to alter, table where the fk will be put
	@secondTableName NVARCHAR(MAX), --table where the pk is
	@constraintName NVARCHAR(MAX), --name for constraint
	@firstColumnName NVARCHAR(MAX), -- column which will be fk
	@secondColumnName NVARCHAR(MAX), -- column wich will be pk
	@isVrsControl BIT = 0 --this variable i use to insert values into Version Control table
	--If isVrsControl is 1(true) i don't insert new row into Version Control table 
	--Else I insert new row into new row into Version Control table
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @addFKSQL NVARCHAR(MAX)
	DECLARE @tableCount1 SMALLINT
	DECLARE @tableCount2 SMALLINT
	DECLARE @columnCount1 SMALLINT
	DECLARE @columnCount2 SMALLINT

	--here count the tables , if the result is 0 then we print an error message
	SELECT @tableCount1 =  COUNT(*) FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = @firstTableName
	SELECT @tableCount2 =  COUNT(*) FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = @secondTableName

	IF @tableCount1 = 0 AND @tableCount2 = 0
		BEGIN
		PRINT 'Tables doesnt exists'
		END
	ELSE
		BEGIN
			SELECT @columnCount1 = COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = @firstTableName AND COLUMN_NAME = @firstColumnName
			SELECT @columnCount2 = COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = @secondTableName AND COLUMN_NAME = @secondColumnName
			IF @columnCount1 = 0 AND @columnCount2 =0
				BEGIN
					PRINT 'Columns doesnt exists'
				END
			ELSE
				BEGIN
					SET @addFKSQL = 'ALTER TABLE ' + QUOTENAME(@firstTableName) + ' ADD CONSTRAINT ' + QUOTENAME(@constraintName) + ' FOREIGN KEY ('+QUOTENAME(@firstColumnName)+')	REFERENCES ' + QUOTENAME(@secondTableName)+'('+ @secondColumnName+')'
					EXEC(@addFKSQL)
					PRINT 'Message'
				END
		END


	
END
GO
