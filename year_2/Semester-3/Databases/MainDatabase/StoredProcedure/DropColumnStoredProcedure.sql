-- =============================================
-- Author:		Ciprian
-- Create date: 28.10.2014
-- Description:	This Stored Procedure drop a column to a table.
-- =============================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--Here check if exists the store procedure. If exist we delete it and recreate
IF EXISTS ( SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_NAME = 'DropColumn')
	BEGIN
		DROP PROC DropColumn
	END
GO


CREATE PROCEDURE [dbo].[DropColumn]
	-- Add the parameters for the stored procedure here
	@tableName NVARCHAR(200),
	@columnName NVARCHAR(200),
	@isVrsControl BIT = 0 --this variable i use to insert values into Version Control table
	--If isVrsControl is 1(true) i don't insert new row into Version Control table 
	--Else I insert new row into new row into Version Control table
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @dropColumnSQL NVARCHAR(MAX)
	DECLARE @tableCount SMALLINT

	SELECT @tableCount =  COUNT(*) FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = @TableName
	IF @tableCount = 0 
	BEGIN
		PRINT 'The given table doesnt exists'
	END
	ELSE
		BEGIN
			--IF @isVrsControl = 1
				--BEGIN
				--TODO
				--Here need to add new entry into the Version Control table
				--END

			SET @dropColumnSQL =  'ALTER TABLE ' + QUOTENAME(@tableName) + ' DROP COLUMN ' + QUOTENAME(@columnName)
			EXEC(@dropColumnSQL)
			PRINT 'Column droped successfully'
		END
END
GO


-- =============================================
--Improvements
-- =============================================

--In the future we can make checks if the given columns exist,
--and if doesn't we print an error message