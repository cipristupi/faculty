-- =============================================
-- Author:		Ciprian
-- Create date: 28.10.2014
-- Description:	This Stored Procedure add a column to a table.
-- =============================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--Here check if exists the store procedure. If exist we delete it and recreate
IF EXISTS ( SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_NAME = 'AddColumn')
	BEGIN
		DROP PROC AddColumn
	END
GO


CREATE PROCEDURE [dbo].[AddColumn]
	-- Add the parameters for the stored procedure here
	@tableName NVARCHAR(200),
	@columnName NVARCHAR(200),
	@columnType NVARCHAR(200),
	@isVrsControl BIT = 0 --this variable i use to insert values into Version Control table
	--If isVrsControl is 1(true) i don't insert new row into Version Control table 
	--Else I insert new row into new row into Version Control table
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @addColumnSQL NVARCHAR(MAX)
	DECLARE @tableCount SMALLINT

	SELECT @tableCount =  COUNT(*) FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = @TableName
	IF @tableCount = 0 
	BEGIN
		PRINT 'The given table doesnt exists'
	END
	ELSE
		BEGIN
			--IF @isVrsControl = 1
				--BEGIN
				--TODO
				--Here need to add new entry into the Version Control table
				--END

			SET @addColumnSQL =  'ALTER TABLE ' + QUOTENAME(@tableName) + ' ADD ' + QUOTENAME(@columnName) + ' ' + QUOTENAME(@columnType)
			EXEC(@addColumnSQL)
			PRINT 'Column added successfully'
		END
END
GO


-- =============================================
--Improvements
-- =============================================
--We can check if the given type is valid.
--SELECT * FROM sys.types