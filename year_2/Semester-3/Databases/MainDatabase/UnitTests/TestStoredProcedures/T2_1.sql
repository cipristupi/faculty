-- =============================================
-- Author:		Ciprian
-- Create date: 10.12.2014
-- Description:	This stored procedure insert a big number of users
-- =============================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS ( SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_NAME = 'T2_1')
	BEGIN
		DROP PROC T2_1 
	END
GO
CREATE PROCEDURE T2_1 

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @idRole INT
    DECLARE @counter INT
	SET @counter = 0
	SET @idRole = (SELECT TOP 1 Id FROM Roles)

	WHILE @counter < 10000
	BEGIN
		INSERT INTO [dbo].[Users]
			   ([Username]
			   ,[Password]
			   ,[RolesId])
		 VALUES
			   (CONCAT('test_',@counter)
			   ,CONCAT('test_',@counter)
			   ,@idRole)
		SET @counter = @counter + 1
	END
END
GO
