-- =============================================
-- Author:		Ciprian
-- Create date: 06.11.2014
-- Description:	This Stored Procedure insert a given number of students	
-- =============================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--Here check if exists the store procedure. If exist we delete it and recreate
IF EXISTS ( SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_NAME = 'T3_2')
	BEGIN
		DROP PROC T3_2
	END
GO


CREATE PROCEDURE [dbo].[T3_2]
	
AS
BEGIN
	SET NOCOUNT ON;
	
	DELETE FROM [dbo].[Users]
      WHERE Username LIKE 'test_std_%'

	DELETE FROM [dbo].[Students]
      WHERE FirstName LIKE 'testStudents_%'

END
GO