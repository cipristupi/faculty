-- =============================================
-- Author:		Ciprian
-- Create date: 10.12.2014
-- Description:	This stored procedure delete a big number of users
-- =============================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS ( SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_NAME = 'T2_2')
	BEGIN
		DROP PROC T2_2
	END
GO
CREATE PROCEDURE T2_2 

AS
BEGIN
	SET NOCOUNT ON;

	DELETE FROM [dbo].[Users]
      WHERE Username LIKE 'test%'
END
GO
