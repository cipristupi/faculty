-- =============================================
-- Author:		Ciprian
-- Create date: 06.11.2014
-- Description:	This Stored Procedure make a select on a table	
-- =============================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--Here check if exists the store procedure. If exist we delete it and recreate
IF EXISTS ( SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_NAME = 'V3_3')
	BEGIN
		DROP PROC V3_3
	END
GO


CREATE PROCEDURE [dbo].[V3_3]
	
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT * FROM View_3

END
GO