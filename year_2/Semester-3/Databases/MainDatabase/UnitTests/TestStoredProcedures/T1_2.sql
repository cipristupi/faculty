-- =============================================
-- Author:		Ciprian
-- Create date: 10.12.2014
-- Description:	This stored procedure insert a big number of specializations
-- =============================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS ( SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_NAME = 'T1_2')
	BEGIN
		DROP PROC T1_2
	END
GO
CREATE PROCEDURE T1_2 

AS
BEGIN
	SET NOCOUNT ON;
	DELETE FROM [dbo].[Specializations]
      WHERE Name LIKE 'test%'
END
GO
