-- =============================================
-- Author:		Ciprian
-- Create date: 10.12.2014
-- Description:	This stored procedure insert a big number of specializations
-- =============================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS ( SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_NAME = 'T1_1')
	BEGIN
		DROP PROC T1_1 
	END
GO
CREATE PROCEDURE T1_1 

AS
BEGIN
	SET NOCOUNT ON;

    DECLARE @counter INT
	SET @counter = 0

	WHILE @counter < 10000
	BEGIN
		INSERT INTO [dbo].[Specializations] ([Name])VALUES (CONCAT('test_',@counter))
		SET @counter = @counter + 1
	END
END
GO
