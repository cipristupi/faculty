-- =============================================
-- Author:		Ciprian
-- Create date: 29.12.2014
-- Description:	This stored procedure evaluate tests with cursor
-- =============================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS ( SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_NAME = 'Test_Evaluation_Cursor')
	BEGIN
		DROP PROC Test_Evaluation_Cursor
	END
GO
CREATE PROCEDURE Test_Evaluation_Cursor
AS
BEGIN
	SET NOCOUNT ON;

	--Count Variables
	DECLARE @countTestTables INT
	DECLARE @countTestViews INT
	
	--DateTimes Variables
	DECLARE @startAtTestRuns DATETIME
	DECLARE @endAtTestRuns DATETIME
	DECLARE @startAt DATETIME
	DECLARE @endAt DATETIME

	--Other Variables
	DECLARE @storedProcedureName NVARCHAR(MAX)

	--here begin cursor

	--Current IDs
	DECLARE @currentTestID INT
	DECLARE @currentTestTableId INT
	DECLARE @currentTestViewId INT
	DECLARE @currentTestRunsId INT

	--Cursor variables
	DECLARE @cursorTestID CURSOR
	DECLARE	@cursorTablesID CURSOR
	DECLARE @cursorViewsID CURSOR


	SET @cursorTestID = CURSOR FOR
	SELECT TestID
	FROM Tests

	OPEN @cursorTestID
	FETCH NEXT
	FROM @cursorTestID INTO @currentTestID
	WHILE @@FETCH_STATUS = 0
		BEGIN
			SET @startAtTestRuns = (SELECT GETDATE())
			--INSERT INTO [dbo].[TestRuns] ([Description] ,[StartAt] ,[EndAt]) VALUES (NULL,@startAtTestRuns,NULL)

			SET @currentTestRunsId = (SELECT MAX(TestRunID) FROM TestRuns)
			SET @countTestTables = (SELECT COUNT(*) FROM TestTables WHERE TestID = @currentTestId)
			SET @countTestViews = (SELECT COUNT(*) FROM TestViews WHERE TestID = @currentTestId)

			IF @countTestTables != 0
			BEGIN
				SET @cursorTablesID = CURSOR FOR
				SELECT TableID
				FROM TestTables

				OPEN @cursorTablesID
				FETCH NEXT
				FROM @cursorTablesID INTO @currentTestTableId

				WHILE @@FETCH_STATUS = 0
				BEGIN
					PRINT @currentTestTableId
					SET @storedProcedureName = CONCAT('T',@currentTestTableId,'_',@currentTestId)
					PRINT @storedProcedureName
					SET @startAt = (SELECT GETDATE())
					EXEC @storedProcedureName
					SET @endAt = (SELECT GETDATE())

					--INSERT INTO [dbo].[TestRunTables]([TestRunID],[TableID],[StartAt],[EndAt])VALUES(@currentTestRunsId,@currentTestTableId,@startAt,@endAt)
					FETCH NEXT
					FROM @cursorTablesID INTO @currentTestTableId
				END

				CLOSE @cursorTablesID
				DEALLOCATE @cursorTablesID
			END

			IF @countTestViews != 0
			BEGIN
					PRINT @currentTestViewId
					SET @cursorViewsID = CURSOR FOR
					SELECT ViewID
					FROM TestViews

					OPEN @cursorViewsID
					FETCH NEXT
					FROM @cursorViewsID INTO @currentTestViewId

					WHILE @@FETCH_STATUS = 0
					BEGIN
						SET @storedProcedureName = CONCAT('V',@currentTestViewId,'_',@currentTestId)
						PRINT @storedProcedureName
						SET @startAt = (SELECT GETDATE())
						EXEC @storedProcedureName
						SET @endAt = (SELECT GETDATE())

						--INSERT INTO [dbo].[TestRunViews]([TestRunID],[ViewID],[StartAt],[EndAt])VALUES(@currentTestRunsId,@currentTestViewId,@startAt,@endAt)
						
						FETCH NEXT
						FROM @cursorViewsID INTO @currentTestViewId
					END
					CLOSE @cursorViewsID
					DEALLOCATE @cursorViewsID
			END

			SET @endAtTestRuns = (SELECT GETDATE())

			--UPDATE [dbo].[TestRuns]
			--SET [EndAt] = @endAtTestRuns
			--WHERE TestRunID = @currentTestRunsId

			FETCH NEXT
			FROM @cursorTestID INTO @currentTestID
		END
	CLOSE @cursorTestID
	DEALLOCATE @cursorTestID

END
GO