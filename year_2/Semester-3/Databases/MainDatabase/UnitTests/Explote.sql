SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE function [dbo].[Explode]
(
@String NVARCHAR(MAX),
@Delimiter char(1)
) Returns @Results Table (Items NVARCHAR(MAX))
As
Begin
	Declare @Index int
	Declare @Slice NVARCHAR(MAX)
	Select @Index = 1
	If @String Is NULL Return
	If Len(@String) = 0 Return
	While @Index != 0
	Begin
		Select @Index = CharIndex(@Delimiter, @String)
		If (@Index != 0)
			Select @Slice = left(@String, @Index - 1)
		else
			Select @Slice = @String
		Insert into @Results(Items) Values (@Slice)
		Select @String = right(@String, Len(@String) - @Index)
		If Len(@String) = 0 break
	End
	Return
End



GO
