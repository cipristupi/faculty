-- =============================================
-- Author:		Ciprian
-- Create date: 06.12.2014
-- Description:	This stored procedure evaluate tests
-- =============================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS ( SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_NAME = 'Test_Evaluation')
	BEGIN
		DROP PROC Test_Evaluation
	END
GO
CREATE PROCEDURE Test_Evaluation
AS
BEGIN
	SET NOCOUNT ON;
	--Table variables
	DECLARE @testIds Table (Id INT)
	DECLARE @testTablesIds TABLE ( Id INT)
	DECLARE @testViewsIds TABLE (Id INT)

	--Current Ids variables
	DECLARE @currentTestId INT
	DECLARE @currentTestTableId INT
	DECLARE @currentTestViewId INT
	DECLARE @currenTestRunsId INT

	--Counters
	DECLARE @counterTestIds INT
	DECLARE @counterTestTablesIds INT
	DECLARE @counterTestViewsIds INT

	--Count Variables
	DECLARE @countTestTables INT
	DECLARE @countTestViews INT
	
	--DateTimes Variables
	DECLARE @startAtTestRuns DATETIME
	DECLARE @endAtTestRuns DATETIME
	DECLARE @startAt DATETIME
	DECLARE @endAt DATETIME

	--Other Variables
	DECLARE @storedProcedureName NVARCHAR(MAX)

	INSERT INTO @testIds
	SELECT TestId FROM Tests

	SET @counterTestIds = 0
	WHILE @counterTestIds < (SELECT COUNT(*) FROM Tests)
	BEGIN
		SET @currentTestId = (SELECT TOP 1 Id FROM @testIds)
		SET @startAtTestRuns = (SELECT GETDATE())
		INSERT INTO [dbo].[TestRuns] ([Description] ,[StartAt] ,[EndAt]) VALUES (NULL,@startAtTestRuns,NULL)

		SET @currenTestRunsId = (SELECT MAX(TestRunID) FROM TestRuns)
		SET @countTestTables = (SELECT COUNT(*) FROM TestTables WHERE TestID = @currentTestId)
		SET @countTestViews = (SELECT COUNT(*) FROM TestViews WHERE TestID = @currentTestId)

		PRINT @countTestTables
		PRINT @countTestViews
		IF @countTestTables != 0
		BEGIN
			INSERT INTO @testTablesIds
			SELECT TableID FROM TestTables WHERE TestID = @currentTestId

			SET @counterTestTablesIds = 0
			WHILE @counterTestTablesIds < @countTestTables
			BEGIN
				SET @currentTestTableId = (SELECT TOP 1 Id FROM @testTablesIds)
				SET @storedProcedureName = CONCAT('T',@currentTestTableId,'_',@currentTestId)
				PRINT @storedProcedureName
				SET @startAt = (SELECT GETDATE())
				EXEC @storedProcedureName
				SET @endAt = (SELECT GETDATE())

				INSERT INTO [dbo].[TestRunTables]([TestRunID],[TableID],[StartAt],[EndAt])VALUES(@currenTestRunsId,@currentTestTableId,@startAt,@endAt)
				DELETE FROM @testTablesIds WHERE Id = @currentTestTableId
				SET @counterTestTablesIds = @counterTestTablesIds + 1
			END
		END

		IF @countTestViews != 0
		BEGIN
				INSERT INTO @testViewsIds
				SELECT ViewID FROM TestViews WHERE TestID = @currentTestId

				SET @counterTestViewsIds = 0
				WHILE @counterTestViewsIds < @countTestViews
				BEGIN
					SET @currentTestViewId = (SELECT TOP 1 Id FROM @testViewsIds)
					SET @storedProcedureName = CONCAT('V',@currentTestViewId,'_',@currentTestId)
					PRINT @storedProcedureName
					SET @startAt = (SELECT GETDATE())
					EXEC @storedProcedureName
					SET @endAt = (SELECT GETDATE())

					INSERT INTO [dbo].[TestRunViews]([TestRunID],[ViewID],[StartAt],[EndAt])VALUES(@currenTestRunsId,@currentTestViewId,@startAt,@endAt)

					DELETE FROM @testViewsIds WHERE Id = @currentTestViewId
					SET @counterTestViewsIds = @counterTestViewsIds + 1
				END
		END

		SET @endAtTestRuns = (SELECT GETDATE())

		UPDATE [dbo].[TestRuns]
		   SET [EndAt] = @endAtTestRuns
		 WHERE TestRunID = @currenTestRunsId

		DELETE FROM @testIds WHERE Id = @currentTestId
		SET @counterTestIds = @counterTestIds + 1
	END

END
GO
