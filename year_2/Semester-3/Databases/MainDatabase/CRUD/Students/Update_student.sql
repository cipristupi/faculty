SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ciprian
-- Create date: 14.01.2015
-- Description:	This stored procedure creates a new entry for specializations
-- =============================================
IF EXISTS ( SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_NAME = 'Update_student')
	BEGIN
		DROP PROC Update_student
	END
GO
CREATE PROCEDURE Update_student 
	-- Add the parameters for the stored procedure here
	@Registration_no nvarchar(100),
           @FirstName nvarchar(100),
           @LastName nvarchar(100),
           @FirstName_Father nvarchar(100),
           @FirstName_Mother nvarchar(100),
           @ADDRESS nvarchar(100),
           @Locality nvarchar(100),
           @Phone nvarchar(100),
           @Address_Custode nvarchar(100),
           @Phone_Custode nvarchar(100),
           @CNP nvarchar(13),
           @Date_enrollment NVARCHAR(MAX),
           @Observations text,
	@ID INT
AS
BEGIN
	SET NOCOUNT ON;
	IF (SELECT COUNT(*) FROM dbo.Students WHERE Id = @ID) = 1
	BEGIN
			UPDATE [dbo].[Students]
			   SET [Registration_no] = @Registration_no
				  ,[FirstName] = @FirstName
				  ,[LastName] = @LastName
				  ,[FirstName_Father] = @FirstName_Father
				  ,[FirstName_Mother] = @FirstName_Mother
				  ,[Address] = @ADDRESS
				  ,[Locality] = @Locality
				  ,[Phone] = @Phone
				  ,[Address_Custode] = @Address_Custode
				  ,[Phone_Custode] = @Phone_Custode
				  ,[CNP] = @CNP
				  ,[Date_enrollment] = @Date_enrollment
				  ,[Observations] = @Observations
			 WHERE Id = @ID
	END
	ELSE
	BEGIN
		RAISERROR ('The student id does not exist.',18,1)
	END
END
GO
