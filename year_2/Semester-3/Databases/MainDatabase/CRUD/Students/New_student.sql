SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ciprian
-- Create date: 14.01.2015
-- Description:	This stored procedure creates a new entry for specializations
-- =============================================
IF EXISTS ( SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_NAME = 'New_student')
	BEGIN
		DROP PROC New_student
	END
GO
CREATE PROCEDURE New_student 
	-- Add the parameters for the stored procedure here
           @Registration_no nvarchar(100),
           @FirstName nvarchar(100),
           @LastName nvarchar(100),
           @FirstName_Father nvarchar(100),
           @FirstName_Mother nvarchar(100),
           @ADDRESS nvarchar(100),
           @Locality nvarchar(100),
           @Phone nvarchar(100),
           @Address_Custode nvarchar(100),
           @Phone_Custode nvarchar(100),
           @CNP nvarchar(13),
           @Date_enrollment NVARCHAR(MAX),
           @Observations text,
           @ClassesId int,
		   @userName NVARCHAR(50),
		@password NVARCHAR(200),
		@roleId INT 
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @UsersId int
	IF (SELECT COUNT(*) FROM dbo.Teachers WHERE CNP = @CNP) = 0 AND (SELECT COUNT(*) FROM dbo.Classes WHERE Id = @ClassesId ) = 1
	BEGIN
		IF ISDATE(@Date_enrollment) = 1
		BEGIN
			INSERT INTO [dbo].[Users]
				   ([Username]
				   ,[Password]
				   ,[RolesId])
			 VALUES
				   (@userName
				   ,@password
				   ,@roleId)
			SELECT @UsersId = ID FROM [dbo].[Users] WHERE [Username] = @userName
			INSERT INTO [dbo].[Students]
			   ([Registration_no]
			   ,[FirstName]
			   ,[LastName]
			   ,[FirstName_Father]
			   ,[FirstName_Mother]
			   ,[Address]
			   ,[Locality]
			   ,[Phone]
			   ,[Address_Custode]
			   ,[Phone_Custode]
			   ,[CNP]
			   ,[Date_enrollment]
			   ,[Observations]
			   ,[ClassesId]
			   ,[UsersId])
		 VALUES
			   (@Registration_no
			   ,@FirstName
			   ,@LastName
			   ,@FirstName_Father
			   ,@FirstName_Mother
			   ,@ADDRESS
			   ,@Locality
			   ,@Phone
			   ,@Address_Custode
			   ,@Phone_Custode
			   ,@CNP
			   ,@Date_enrollment
			   ,@Observations
			   ,@ClassesId
			   ,@UsersId)
		   END
		   ELSE
		   BEGIN
			RAISERROR ('Invalide date',18,1)
		   END
	END
	ELSE
	BEGIN
		RAISERROR ('The cnp exists.',18,1)
	END
END
GO
