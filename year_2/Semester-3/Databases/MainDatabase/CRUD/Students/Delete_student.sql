SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ciprian
-- Create date: 14.01.2015
-- Description:	This stored procedure creates a new entry for specializations
-- =============================================
IF EXISTS ( SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_NAME = 'Delete_student')
	BEGIN
		DROP PROC Delete_student
	END
GO
CREATE PROCEDURE Delete_student 
	-- Add the parameters for the stored procedure here
	@ID INT
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @UserID INT
	IF (SELECT COUNT(*) FROM dbo.Students WHERE Id = @ID) = 1
	BEGIN
			SET @UserID = (SELECT UsersId FROM dbo.Students WHERE Id = @ID)
			DELETE [dbo].Users
			WHERE Id = @UserID
	END
	ELSE
	BEGIN
		RAISERROR ('The student id does not exist.',18,1)
	END
END
GO
