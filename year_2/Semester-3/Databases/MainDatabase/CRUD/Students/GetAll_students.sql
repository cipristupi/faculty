SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ciprian
-- Create date: 14.01.2015
-- Description:	This stored procedure creates a new entry for specializations
-- =============================================
IF EXISTS ( SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_NAME = 'GetAll_students')
	BEGIN
		DROP PROC GetAll_students
	END
GO
CREATE PROCEDURE GetAll_students 
	-- Add the parameters for the stored procedure here
	@ClassId INT
AS
BEGIN
	SET NOCOUNT ON;
	SELECT * FROM dbo.Students WHERE ClassesId = @ClassId
END
GO
