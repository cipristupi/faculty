USE [SchoolManager]
GO

/****** Object:  Index [NonClusteredIndex-20150114-204911]    Script Date: 1/14/2015 08:49:36 PM ******/
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20150114-204911] ON [dbo].[Students]
(
	[Id] ASC,
	[Registration_no] ASC,
	[CNP] ASC,
	[ClassesId] ASC,
	[UsersId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


