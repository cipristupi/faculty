SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ciprian
-- Create date: 10.01.2015
-- Description:	This stored procedure returns number of users with a given role
-- =============================================
--Here check if exists the store procedure. If exist we delete it and recreate
IF EXISTS ( SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_NAME = 'Select_count_role')
	BEGIN
		DROP PROC Select_count_role
	END
GO
CREATE PROCEDURE Select_count_role 
	 @roleId INT ,
	 @userCountRole INT OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	IF (SELECT COUNT(*) FROM dbo.Roles WHERE Id = @roleId) = 1
	BEGIN
		SET @userCountRole = (SELECT COUNT(*) FROM dbo.Users WHERE RolesId = @roleId)
	END
	ELSE
	BEGIN
		RAISERROR ('The role id does not exist.',18,1)
	END
	RETURN
END
GO
