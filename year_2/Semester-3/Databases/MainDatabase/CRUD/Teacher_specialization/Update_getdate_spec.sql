SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ciprian
-- Create date: 14.01.2015
-- Description:	This stored procedure creates a new entry for specializations
-- =============================================
IF EXISTS ( SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_NAME = 'Update_getdate_spec')
	BEGIN
		DROP PROC Update_getdate_spec
	END
GO
CREATE PROCEDURE Update_getdate_spec 
	-- Add the parameters for the stored procedure here
	@TeacherID INT,
	@SpecializationID INT,
	@DateOfGet NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;
	IF (SELECT COUNT(*) FROM dbo.Teachers WHERE Id =@TeacherID) = 1 AND (SELECT COUNT(*) FROM dbo.Specializations WHERE Id = @SpecializationID ) = 1
	BEGIN
			IF ISDATE(@DateOfGet) = 1
			BEGIN
					UPDATE [dbo].[Teacher_Specializations]
					SET [Date_of_get] = @DateOfGet
					 WHERE TeachersId = @TeacherID AND SpecializationsId = @SpecializationID
			END
			ELSE
			BEGIN
			 RAISERROR ('Invalide date',18,1) 
			END
	END
	ELSE
	BEGIN
		RAISERROR ('Invalide ids.',18,1)
	END
END
GO
