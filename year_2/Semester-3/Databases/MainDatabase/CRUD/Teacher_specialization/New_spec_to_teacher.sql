SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ciprian
-- Create date: 14.01.2015
-- Description:	This stored procedure creates a new entry for specializations
-- =============================================
IF EXISTS ( SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_NAME = 'New_spec_to_teacher')
	BEGIN
		DROP PROC New_spec_to_teacher
	END
GO
CREATE PROCEDURE New_spec_to_teacher 
	-- Add the parameters for the stored procedure here
	@TeacherID INT,
	@SpecializationID INT,
	@DateOfGet NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;
	IF (SELECT COUNT(*) FROM dbo.Teachers WHERE Id =@TeacherID) = 1 AND (SELECT COUNT(*) FROM dbo.Specializations WHERE Id = @SpecializationID ) = 1
	BEGIN
		IF ISDATE(@DateOfGet) =1
		BEGIN
		INSERT INTO [dbo].[Teacher_Specializations]
           ([TeachersId]
           ,[SpecializationsId]
           ,[Date_of_get])
     VALUES
           (@TeacherID
           ,@SpecializationID
           ,@DateOfGet)
		   END
		   ELSE
		   BEGIN
		   RAISERROR ('Invalide date',18,1)
		   END

	END
	ELSE
	BEGIN
		RAISERROR ('The specialization or teacher does not exists.',18,1)
	END
END
GO
