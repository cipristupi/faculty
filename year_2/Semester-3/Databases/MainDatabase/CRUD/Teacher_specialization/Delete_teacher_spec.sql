SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ciprian
-- Create date: 14.01.2015
-- Description:	This stored procedure creates a new entry for specializations
-- =============================================
IF EXISTS ( SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_NAME = 'Delete_teacher_spec')
	BEGIN
		DROP PROC Delete_teacher_spec
	END
GO
CREATE PROCEDURE Delete_teacher_spec 
-- Add the parameters for the stored procedure here
	@TeacherID INT,
	@SpecializationID INT,
AS
BEGIN
	SET NOCOUNT ON;
	IF (SELECT COUNT(*) FROM dbo.Teachers WHERE Id =@TeacherID) = 1 AND (SELECT COUNT(*) FROM dbo.Specializations WHERE Id = @SpecializationID ) = 1
	BEGIN
					DELETE [dbo].[Teacher_Specializations]
					 WHERE TeachersId = @TeacherID AND SpecializationsId = @SpecializationID
	END
	ELSE
	BEGIN
		RAISERROR ('Invalide ids.',18,1)
	END
END
GO
