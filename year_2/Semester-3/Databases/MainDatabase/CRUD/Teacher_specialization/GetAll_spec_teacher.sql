SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ciprian
-- Create date: 14.01.2015
-- Description:	This stored procedure creates a new entry for specializations
-- =============================================
IF EXISTS ( SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_NAME = 'GetAll_spec_teacher')
	BEGIN
		DROP PROC GetAll_spec_teacher
	END
GO
CREATE PROCEDURE GetAll_spec_teacher 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	SET NOCOUNT ON;
	SELECT t.FirstName , t.LastName, s.Name , tc.Date_of_get
	FROM dbo.Teacher_Specializations tc
	JOIN dbo.Teachers t ON t.Id = tc.TeachersId
	JOIN dbo.Specializations s ON s.Id = tc.SpecializationsId
END
GO
