SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ciprian
-- Create date: 14.01.2015
-- Description:	This stored procedure creates a new entry for specializations
-- =============================================
IF EXISTS ( SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_NAME = 'Delete_teacher')
	BEGIN
		DROP PROC Delete_teacher
	END
GO
CREATE PROCEDURE Delete_teacher 
	-- Add the parameters for the stored procedure here
	@ID INT
AS
BEGIN
	SET NOCOUNT ON;
	IF (SELECT COUNT(*) FROM dbo.Teachers WHERE Id = @ID) = 1
	BEGIN
			DELETE [dbo].Teachers
			WHERE Id = @ID
	END
	ELSE
	BEGIN
		RAISERROR ('The teacher id does not exist.',18,1)
	END
END
GO
