SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ciprian
-- Create date: 14.01.2015
-- Description:	This stored procedure creates a new entry for teacher
-- =============================================
IF EXISTS ( SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_NAME = 'New_teacher')
	BEGIN
		DROP PROC New_teacher
	END
GO
CREATE PROCEDURE New_teacher 
	-- Add the parameters for the stored procedure here
	@FirstName  nvarchar(50),
	@LastName  nvarchar(50),
	@CNP  nvarchar(13),
	@Locality  nvarchar(50),
	@ADDRESS  nvarchar(50),
	@PositionsId  int,
	@userName NVARCHAR(50),
	@password NVARCHAR(200) ,
	@roleId INT  ,
	@currentID INT,
	@positionID INT

AS
BEGIN
	SET NOCOUNT ON;
	IF (SELECT COUNT(*) FROM dbo.Teachers WHERE CNP = @CNP) = 0
	BEGIN
		INSERT INTO [dbo].[Users]
			   ([Username]
			   ,[Password]
			   ,[RolesId])
		 VALUES
			   (@userName
			   ,@password
			   ,@roleId)
		SELECT @currentID = ID FROM [dbo].[Users] WHERE [Username] = @userName
		SELECT @positionID = ID FROM [dbo].[Positions] WHERE [Name] = 'teacher'
		INSERT INTO [dbo].[Teachers]
           ([FirstName]
           ,[LastName]
           ,[CNP]
           ,[Locality]
           ,[Address]
           ,[PositionsId]
           ,[UsersId])
		VALUES
           (@FirstName,
		     @LastName,
           @CNP,
           @Locality,
           @ADDRESS,
           @positionID,
          @currentID)
	END
	ELSE
	BEGIN
		RAISERROR ('The cnp exists.',18,1)
	END

END
GO
