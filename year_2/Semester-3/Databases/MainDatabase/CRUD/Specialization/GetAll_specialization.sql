SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ciprian
-- Create date: 14.01.2015
-- Description:	This stored procedure creates a new entry for specializations
-- =============================================
IF EXISTS ( SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_NAME = 'GetAll_specialization')
	BEGIN
		DROP PROC GetAll_specialization
	END
GO
CREATE PROCEDURE GetAll_specialization 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	SET NOCOUNT ON;
	SELECT Name FROM dbo.Specializations
END
GO
