SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ciprian
-- Create date: 14.01.2015
-- Description:	This stored procedure creates a new entry for specializations
-- =============================================
IF EXISTS ( SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_NAME = 'Delete_specialization')
	BEGIN
		DROP PROC Delete_specialization
	END
GO
CREATE PROCEDURE Delete_specialization 
	-- Add the parameters for the stored procedure here
	@ID INT
AS
BEGIN
	SET NOCOUNT ON;
	IF (SELECT COUNT(*) FROM dbo.Specializations WHERE Id = @ID) = 1
	BEGIN
			DELETE [dbo].[Specializations]
			WHERE Id = @ID
	END
	ELSE
	BEGIN
		RAISERROR ('The specializations id does not exist.',18,1)
	END
END
GO
