SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ciprian
-- Create date: 14.01.2015
-- Description:	This stored procedure creates a new entry for specializations
-- =============================================
IF EXISTS ( SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_NAME = 'New_specialization')
	BEGIN
		DROP PROC New_specialization
	END
GO
CREATE PROCEDURE New_specialization 
	-- Add the parameters for the stored procedure here
	@Name NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;
	INSERT INTO [dbo].[Specializations]
           ([Name])
     VALUES
           (@NAME)
END
GO
