SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ciprian
-- Create date: 10.01.2015
-- Description:	This stored procedure insert a new username
-- =============================================
--Here check if exists the store procedure. If exist we delete it and recreate
IF EXISTS ( SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_NAME = 'Create_Username')
	BEGIN
		DROP PROC Create_Username
	END
GO
CREATE PROCEDURE Create_Username 
	-- Add the parameters for the stored procedure here
	 @userName NVARCHAR(50),
	 @password NVARCHAR(200) ,
	 @roleId INT 
AS
BEGIN
	SET NOCOUNT ON;

	IF (SELECT COUNT(*) FROM dbo.Roles WHERE Id = @roleId) = 1
	BEGIN
		INSERT INTO [dbo].[Users]
           ([Username]
           ,[Password]
           ,[RolesId])
     VALUES
           (@userName
           ,@password
           ,@roleId)
	END
	ELSE
	BEGIN
		RAISERROR ('The role id does not exist.',18,1)
	END
END
GO
