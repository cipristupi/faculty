SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ciprian
-- Create date: 10.01.2015
-- Description:	This stored procedure updates the password for a given user
-- =============================================
--Here check if exists the store procedure. If exist we delete it and recreate
IF EXISTS ( SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_NAME = 'Update_User_Password')
	BEGIN
		DROP PROC Update_User_Password
	END
GO
CREATE PROCEDURE Update_User_Password 
	 @newPassword NVARCHAR(200) ,
	 @userId INT 
AS
BEGIN
	SET NOCOUNT ON;

	IF (SELECT COUNT(*) FROM dbo.Users WHERE Id = @userId) = 1
	BEGIN
		UPDATE [dbo].[Users]
		SET [Password] = @newPassword
		WHERE Id = @userId
	END
	ELSE
	BEGIN
		RAISERROR ('The user does not exist.',18,1)
	END
END
GO
