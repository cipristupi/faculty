SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ciprian
-- Create date: 10.01.2015
-- Description:	This stored procedure delete a given user
-- =============================================
--Here check if exists the store procedure. If exist we delete it and recreate
IF EXISTS ( SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_NAME = 'Delete_User')
	BEGIN
		DROP PROC Delete_User
	END
GO
CREATE PROCEDURE Delete_User 
	 @userId INT 
AS
BEGIN
	SET NOCOUNT ON;

	IF (SELECT COUNT(*) FROM dbo.Users WHERE Id = @userId) = 1
	BEGIN
		DELETE [dbo].[Users]
		WHERE Id = @userId
	END
	ELSE
	BEGIN
		RAISERROR ('The user does not exist.',18,1)
	END
END
GO
