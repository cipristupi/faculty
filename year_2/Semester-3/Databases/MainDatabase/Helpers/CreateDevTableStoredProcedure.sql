-- =============================================
-- Author:		Ciprian
-- Create date: 28.10.2014
-- Description:	This Stored Procedure drop a table.
-- =============================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Here check if exists the store procedure. If exist we delete it and recreate
IF EXISTS ( SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_NAME = 'CreateDevTable')
	BEGIN
		DROP PROC CreateDevTable
	END
GO

CREATE PROCEDURE CreateDevTable
	@tableName NVARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tableCount SMALLINT
	DECLARE @createTableSQL NVARCHAR(MAX)
	DECLARE @dropTableSQL NVARCHAR(MAX)
	SELECT @tableCount =  COUNT(*) FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = @tableName
	
	IF @tableCount != 0 
	BEGIN
		SET @dropTableSQL = 'DROP TABLE '+ QUOTENAME(@tableName)
		EXEC(@dropTableSQL)
	END
    
	SET @createTableSQL = 'CREATE TABLE ' + QUOTENAME(@tableName) + '
		(
			"Column1" NVARCHAR(MAX),
			"Column2" BIT,
			"Column3" TEXT,
			"Column4" XML
		);'
	EXEC(@createTableSQL)
	
END
GO
