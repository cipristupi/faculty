CREATE TABLE [dbo].[Classes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Classes_EnumId] [int] NOT NULL,
	[Classes_LevelId] [int] NOT NULL,
	CONSTRAINT [PK_Classes] PRIMARY KEY CLUSTERED ([Id] ASC))

CREATE TABLE [dbo].[Classes_Enum](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](5) NOT NULL,
 CONSTRAINT [PK_Classes_Enum] PRIMARY KEY CLUSTERED([Id] ASC)
)

CREATE TABLE [dbo].[Classes_Level](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](5) NOT NULL,
 CONSTRAINT [PK_Classes_Level] PRIMARY KEY CLUSTERED([Id] ASC)
 ) 

CREATE TABLE [dbo].[Positions](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](10) NOT NULL,
 CONSTRAINT [PK_Positions] PRIMARY KEY CLUSTERED([Id] ASC)
) 
CREATE TABLE [dbo].[Roles](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](10) NOT NULL,
 CONSTRAINT [PK_Roles] PRIMARY KEY CLUSTERED([Id] ASC))

CREATE TABLE [dbo].[Specializations](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Specializations] PRIMARY KEY CLUSTERED([Id] ASC))

CREATE TABLE [dbo].[Students](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Registration_no] [nvarchar](10) NOT NULL,
	[FirstName] [nvarchar](50) NOT NULL,
	[LastName] [nvarchar](50) NOT NULL,
	[FirstName_Father] [nvarchar](50) NULL,
	[FirstName_Mother] [nvarchar](50) NULL,
	[Address] [nvarchar](50) NOT NULL,
	[Locality] [nvarchar](50) NOT NULL,
	[Phone] [nvarchar](50) NOT NULL,
	[Address_Custode] [nvarchar](50) NULL,
	[Phone_Custode] [nvarchar](50) NULL,
	[CNP] [nvarchar](13) NOT NULL,
	[Date_enrollment] [date] NOT NULL,
	[Observations] [text] NULL,
	[ClassesId] [int] NOT NULL,
	[UsersId] [int] NOT NULL,
 CONSTRAINT [PK_Students] PRIMARY KEY CLUSTERED([Id] ASC))

CREATE TABLE [dbo].[Teacher_Classes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ClassesId] [int] NOT NULL,
	[TeachersId] [int] NOT NULL,
	[PositionsId] [int] NOT NULL,
 CONSTRAINT [PK_Teachers_Classes] PRIMARY KEY CLUSTERED([Id] ASC))


CREATE TABLE [dbo].[Teacher_Specializations](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TeachersId] [int] NOT NULL,
	[SpecializationsId] [int] NOT NULL,
	[Date_of_get] [date] NOT NULL,
 CONSTRAINT [PK_Teacher_Specializations] PRIMARY KEY CLUSTERED([Id] ASC))

CREATE TABLE [dbo].[Teachers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](50) NOT NULL,
	[LastName] [nvarchar](50) NOT NULL,
	[CNP] [nvarchar](13) NOT NULL,
	[Locality] [nvarchar](50) NOT NULL,
	[Address] [nvarchar](50) NOT NULL,
	[PositionsId] [int] NOT NULL,
	[UsersId] [int] NOT NULL,
 CONSTRAINT [PK_Staff] PRIMARY KEY CLUSTERED([Id] ASC))

CREATE TABLE [dbo].[Users](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Username] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](200) NOT NULL,
	[RolesId] [int] NOT NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED([Id] ASC
))


ALTER TABLE [dbo].[Classes]  WITH CHECK ADD  CONSTRAINT [FK_Classes_Classes_Enum] FOREIGN KEY([Classes_EnumId])
REFERENCES [dbo].[Classes_Enum] ([Id])

ALTER TABLE [dbo].[Classes] CHECK CONSTRAINT [FK_Classes_Classes_Enum]


ALTER TABLE [dbo].[Classes]  WITH CHECK ADD  CONSTRAINT [FK_Classes_Classes_Level] FOREIGN KEY([Classes_LevelId])
REFERENCES [dbo].[Classes_Level] ([Id])

ALTER TABLE [dbo].[Classes] CHECK CONSTRAINT [FK_Classes_Classes_Level]

ALTER TABLE [dbo].[Students]  WITH CHECK ADD  CONSTRAINT [FK_Students_Classes] FOREIGN KEY([ClassesId])
REFERENCES [dbo].[Classes] ([Id])

ALTER TABLE [dbo].[Students] CHECK CONSTRAINT [FK_Students_Classes]

ALTER TABLE [dbo].[Students]  WITH CHECK ADD  CONSTRAINT [FK_Students_Users] FOREIGN KEY([UsersId])
REFERENCES [dbo].[Users] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE


ALTER TABLE [dbo].[Students] CHECK CONSTRAINT [FK_Students_Users]


ALTER TABLE [dbo].[Teacher_Classes]  WITH CHECK ADD  CONSTRAINT [FK_Teacher_Classes_Classes] FOREIGN KEY([ClassesId])
REFERENCES [dbo].[Classes] ([Id])


ALTER TABLE [dbo].[Teacher_Classes] CHECK CONSTRAINT [FK_Teacher_Classes_Classes]


ALTER TABLE [dbo].[Teacher_Classes]  WITH CHECK ADD  CONSTRAINT [FK_Teacher_Classes_Teachers] FOREIGN KEY([TeachersId])
REFERENCES [dbo].[Teachers] ([Id])


ALTER TABLE [dbo].[Teacher_Classes] CHECK CONSTRAINT [FK_Teacher_Classes_Teachers]


ALTER TABLE [dbo].[Teacher_Specializations]  WITH CHECK ADD  CONSTRAINT [FK_Teacher_Specializations_Specializations] FOREIGN KEY([SpecializationsId])
REFERENCES [dbo].[Specializations] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE


ALTER TABLE [dbo].[Teacher_Specializations] CHECK CONSTRAINT [FK_Teacher_Specializations_Specializations]


ALTER TABLE [dbo].[Teacher_Specializations]  WITH CHECK ADD  CONSTRAINT [FK_Teacher_Specializations_Teachers] FOREIGN KEY([TeachersId])
REFERENCES [dbo].[Teachers] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE


ALTER TABLE [dbo].[Teacher_Specializations] CHECK CONSTRAINT [FK_Teacher_Specializations_Teachers]


ALTER TABLE [dbo].[Teachers]  WITH CHECK ADD  CONSTRAINT [FK_Teachers_Positions] FOREIGN KEY([PositionsId])
REFERENCES [dbo].[Positions] ([Id])


ALTER TABLE [dbo].[Teachers] CHECK CONSTRAINT [FK_Teachers_Positions]


ALTER TABLE [dbo].[Teachers]  WITH CHECK ADD  CONSTRAINT [FK_Teachers_Users] FOREIGN KEY([UsersId])
REFERENCES [dbo].[Users] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE


ALTER TABLE [dbo].[Teachers] CHECK CONSTRAINT [FK_Teachers_Users]


ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK_Users_Roles] FOREIGN KEY([RolesId])
REFERENCES [dbo].[Roles] ([Id])


ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK_Users_Roles]


