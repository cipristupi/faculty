USE [SchoolManager]
GO

/****** Object:  Table [dbo].[DatabaseVersions]    Script Date: 10/27/2014 10:43:24 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DatabaseVersions](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UpgradeProcedure] [nvarchar](max) NOT NULL,
	[DownGradeProcedure] [nvarchar](50) NOT NULL,
	[CurrentVersion] [bit] NOT NULL,
 CONSTRAINT [PK_DatabaseVersions] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO


