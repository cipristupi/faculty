--Get name of all teachers for the given class level
SELECT tc.Id, t.FirstName +' '+ t.LastName AS 'Name', e.Name , l.Name
FROM Teacher_Classes tc
JOIN Classes c ON tc.ClassesId = c.Id
JOIN Classes_Level l ON c.Classes_LevelId = l.Id
JOIN Classes_Enum e ON c.Classes_EnumId = e.Id
JOIN Teachers t ON t.Id = tc.TeachersId
WHERE l.Name = '12'


--Get name of all teachers for the given class level and name for class
SELECT tc.Id, t.FirstName +' '+ t.LastName AS 'Name', e.Name , l.Name
FROM Teacher_Classes tc
JOIN Classes c ON tc.ClassesId = c.Id
JOIN Classes_Level l ON c.Classes_LevelId = l.Id
JOIN Classes_Enum e ON c.Classes_EnumId = e.Id
JOIN Teachers t ON t.Id = tc.TeachersId



SELECT s.Id, s.FirstName +' '+ s.LastName AS 'Name', e.Name , l.Name AS 'Class name'
FROM Students s
JOIN Classes c ON s.ClassesId = c.Id
JOIN Classes_Level l ON c.Classes_LevelId = l.Id
JOIN Classes_Enum e ON c.Classes_EnumId = e.Id
WHERE l.Name IN (9,10)


SELECT u.Username , t.FirstName +' '+ t.LastName AS 'Name' , r.Name AS 'Role'
FROM Users u
JOIN Teachers t ON t.Id = u.Id
JOIN Roles r ON r.Id = u.RolesId


SELECT u.Username , t.FirstName +' '+ t.LastName AS 'Name' , r.Name AS 'Role'
FROM Users u
JOIN Students t ON t.Id = u.Id
JOIN Roles r ON r.Id = u.RolesId


SELECT t.FirstName +' '+ t.LastName AS 'Name' , s.Name AS 'Specialization'
FROM Teachers t
JOIN Teacher_Specializations ts ON ts.TeachersId = t.Id
JOIN Specializations s ON s.Id = ts.SpecializationsId


SELECT t.FirstName +' '+ t.LastName AS 'Name' , s.Name AS 'Specialization', p.Name AS 'Position'
FROM Teachers t
JOIN Teacher_Specializations ts ON ts.TeachersId = t.Id
JOIN Specializations s ON s.Id = ts.SpecializationsId
JOIN Positions p ON p.Id = t.PositionsId