--Many to many joining
SELECT c.Id, e.Name , l.Name
FROM Classes c
JOIN Classes_Level l ON c.Classes_LevelId = l.Id
JOIN Classes_Enum e ON c.Classes_EnumId = e.Id



SELECT tc.Id, t.FirstName +' '+ t.LastName AS 'Name', e.Name , l.Name
FROM Teacher_Classes tc
JOIN Classes c ON tc.ClassesId = c.Id
JOIN Classes_Level l ON c.Classes_LevelId = l.Id
JOIN Classes_Enum e ON c.Classes_EnumId = e.Id
JOIN Teachers t ON t.Id = tc.TeachersId