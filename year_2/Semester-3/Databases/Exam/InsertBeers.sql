SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS ( SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_NAME = 'InsertBeers')
	BEGIN
		DROP PROC InsertBeers
	END
GO


CREATE PROCEDURE [dbo].InsertBeers
	@quantity INT ,
	@numberOfEntries INT
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @customerId TABLE(ID int)

	INSERT INTO @customerId
	SELECT Id FROM [dbo].Customers 

	DECLARE  @counter INT --counter for number of classes
	DECLARE @innerCounter INT
	DECLARE @currentID INT
	DECLARE @dateI DATE
    
	DECLARE @currentCustomer INT

	SET @counter = 0
	WHILE @counter < (SELECT COUNT(*) FROM dbo.Customers)
		BEGIN
		SELECT @currentCustomer = (SELECT TOP 1 [ID] FROM @customerId )

		SET @dateI = (select Cast(Cast(dateadd(month, -1, GETDATE()) as Date) as Datetime))
		
		SET @innerCounter = 0
		WHILE @innerCounter < @numberOfEntries
			BEGIN
		INSERT INTO [dbo].[Bought_beer]
				   ([BarId]
				   ,[CustomerId]
				   ,[Quantity]
				   ,[TimeStamp])
			 VALUES
				   (1
				   ,@currentCustomer
				   ,@quantity
				   ,@dateI)

			SET @innerCounter = @innerCounter + 1
			END


		DELETE FROM @customerId WHERE Id = @currentCustomer
		SET @counter = @counter+1
		PRINT'AFTER SECOND WHILE'
		END

END
GO