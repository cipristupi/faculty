/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP 1000 c.Name AS 'Customer name', b.Name AS 'Beer name'
  FROM [Exam].[dbo].[FavoriteBeer] fb
 INNER JOIN Exam.dbo.Customers c ON c.Id = fb.CustomerId
 INNER JOIN Exam.dbo.Beers b ON fb.BeerId = b.Id