

CREATE PROCEDURE [dbo].[InsertBeers]
	@quantity INT ,
	@numberOfEntries INT
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @customerId TABLE(ID int)

	INSERT INTO @customerId
	SELECT Id FROM [dbo].Customers 

	DECLARE  @counter INT --counter for number of classes
	DECLARE @innerCounter INT
	DECLARE @currentID INT
	DECLARE @dateI DATE
    
	DECLARE @currentCustomer INT

	SET @counter = 0
	WHILE @counter < (SELECT COUNT(*) FROM dbo.Customers)
		BEGIN
		SELECT @currentCustomer = (SELECT TOP 1 [ID] FROM @customerId )

		SET @dateI = (select Cast(Cast(dateadd(month, -1, GETDATE()) as Date) as Datetime))
		
		SET @innerCounter = 0
		WHILE @innerCounter < @numberOfEntries
			BEGIN

			
INSERT INTO [dbo].[Bought_beer]
           ([BarId]
           ,[CustomerId]
           ,[Quantity]
           ,[TimeStamp])
     VALUES
           (1
           ,@currentCustomer
           ,@quantity
           ,@dateI)

			SET @innerCounter = @innerCounter + 1
			END
		DELETE FROM @customerId WHERE Id = @currentCustomer
		SET @counter = @counter+1
		PRINT'AFTER SECOND WHILE'
		END

END


CREATE PROCEDURE [dbo].[SetFavoriteBeer]
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @lastMonth DATE
	SET @lastMonth =( select Cast(dateadd(month, -1, GETDATE()) as Date))

	DECLARE @customerId TABLE(ID int)
	DECLARE @beerToCustomer INT

	INSERT INTO @customerId
	SELECT Id FROM [dbo].Customers 

	DECLARE  @counter INT --counter for number of classes
    
	DECLARE @currentCustomer INT
	DECLARE @beerID INT

	SET @counter = 0
	WHILE @counter < (SELECT COUNT(*) FROM dbo.Customers)
	BEGIN
		SELECT @currentCustomer = (SELECT TOP 1 [ID] FROM @customerId )
		SET @beerToCustomer = (SELECT COUNT(*) FROM Bought_Beer bb WHERE bb.CustomerId =@currentCustomer AND CAST(bb.TimeStamp as DATE) = @lastMonth)
		IF @beerToCustomer >= 5
		BEGIN
			SET @beerID =(SELECT TOP 1 bb.BeerId FROM Bought_Beer bb WHERE bb.CustomerId =@currentCustomer AND CAST(bb.TimeStamp as DATE) = @lastMonth)
			IF (SELECT COUNT(*) FROM FavoriteBeer fb WHERE fb.BeerId = @beerID) = 0
			BEGIN
				
				--SET @beerID = (SELECT bb.BeerId FROM Bought_Beer bb WHERE bb.CustomerId =@currentCustomer AND CAST(bb.TimeStamp as DATE) = @lastMonth)
					INSERT INTO [dbo].[FavoriteBeer]
				   ([BeerId]
				   ,[CustomerId])
			 VALUES
				   (@beerID
				   ,@currentCustomer)
			END
		END

		DELETE FROM @customerId WHERE Id = @currentCustomer
		SET @counter = @counter+1
	END
END


CREATE TABLE [dbo].[Bars](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Bars] PRIMARY KEY (Id)
 )

CREATE TABLE [dbo].[Bars_Beers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BarId] [int] NOT NULL,
	[BeerId] [int] NOT NULL,
	[Price] [float] NOT NULL,
 CONSTRAINT [PK_Bars_Beers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)
)


CREATE TABLE [dbo].[BeerCategories](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_BeerCategories] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
))


CREATE TABLE [dbo].[Beers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[ProducerPrice] [float] NOT NULL,
	[CategoryId] [int] NOT NULL,
 CONSTRAINT [PK_Beers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
))



CREATE TABLE [dbo].[Bought_beer](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BarId] [int] NOT NULL,
	[CustomerId] [int] NOT NULL,
	[Quantity] [int] NOT NULL,
	[TimeStamp] [datetime] NOT NULL,
	[BeerId] [int] NULL,
 CONSTRAINT [PK_Bought_beer] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
))



CREATE TABLE [dbo].[Customers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Customers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
))



CREATE TABLE [dbo].[FavoriteBeer](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BeerId] [int] NOT NULL,
	[CustomerId] [int] NOT NULL,
 CONSTRAINT [PK_FavoriteBeer] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
))



CREATE VIEW [dbo].[View_1]
AS
SELECT DISTINCT dbo.Bars.Name, dbo.Bars_Beers.Price, dbo.Beers.Name AS 'Beer name', dbo.Beers.ProducerPrice
FROM            dbo.Bars INNER JOIN
                         dbo.Bars_Beers ON dbo.Bars.Id = dbo.Bars_Beers.BarId INNER JOIN
                         dbo.Beers ON dbo.Bars_Beers.BeerId = dbo.Beers.Id AND dbo.Bars_Beers.Price > dbo.Beers.ProducerPrice * 2

--GO
ALTER TABLE [dbo].[Bars_Beers]  WITH CHECK ADD  CONSTRAINT [FK_Bars_Beers_Bars] FOREIGN KEY([BarId])
REFERENCES [dbo].[Bars] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
--GO
ALTER TABLE [dbo].[Bars_Beers] CHECK CONSTRAINT [FK_Bars_Beers_Bars]
--GO
ALTER TABLE [dbo].[Bars_Beers]  WITH CHECK ADD  CONSTRAINT [FK_Bars_Beers_Beers] FOREIGN KEY([BeerId])
REFERENCES [dbo].[Beers] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
--GO
ALTER TABLE [dbo].[Bars_Beers] CHECK CONSTRAINT [FK_Bars_Beers_Beers]
--GO
ALTER TABLE [dbo].[Beers]  WITH CHECK ADD  CONSTRAINT [FK_Beers_BeerCategories] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[BeerCategories] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
--GO
ALTER TABLE [dbo].[Beers] CHECK CONSTRAINT [FK_Beers_BeerCategories]
--GO
ALTER TABLE [dbo].[Bought_beer]  WITH CHECK ADD  CONSTRAINT [FK_Bought_beer_Bars] FOREIGN KEY([BarId])
REFERENCES [dbo].[Bars] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
--GO
ALTER TABLE [dbo].[Bought_beer] CHECK CONSTRAINT [FK_Bought_beer_Bars]
--GO
ALTER TABLE [dbo].[Bought_beer]  WITH CHECK ADD  CONSTRAINT [FK_Bought_beer_Beers] FOREIGN KEY([BeerId])
REFERENCES [dbo].[Beers] ([Id])
--GO
ALTER TABLE [dbo].[Bought_beer] CHECK CONSTRAINT [FK_Bought_beer_Beers]
--GO
ALTER TABLE [dbo].[Bought_beer]  WITH CHECK ADD  CONSTRAINT [FK_Bought_beer_Customers] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customers] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
--GO
ALTER TABLE [dbo].[Bought_beer] CHECK CONSTRAINT [FK_Bought_beer_Customers]
--GO
ALTER TABLE [dbo].[FavoriteBeer]  WITH CHECK ADD  CONSTRAINT [FK_FavoriteBeer_Beers] FOREIGN KEY([BeerId])
REFERENCES [dbo].[Beers] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
--GO
ALTER TABLE [dbo].[FavoriteBeer] CHECK CONSTRAINT [FK_FavoriteBeer_Beers]
--GO
ALTER TABLE [dbo].[FavoriteBeer]  WITH CHECK ADD  CONSTRAINT [FK_FavoriteBeer_Customers] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customers] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
--GO
ALTER TABLE [dbo].[FavoriteBeer] CHECK CONSTRAINT [FK_FavoriteBeer_Customers]
