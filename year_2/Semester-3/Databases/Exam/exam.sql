USE [Exam]
GO
EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPaneCount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_1'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPane1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_1'

GO
ALTER TABLE [dbo].[FavoriteBeer] DROP CONSTRAINT [FK_FavoriteBeer_Customers]
GO
ALTER TABLE [dbo].[FavoriteBeer] DROP CONSTRAINT [FK_FavoriteBeer_Beers]
GO
ALTER TABLE [dbo].[Bought_beer] DROP CONSTRAINT [FK_Bought_beer_Customers]
GO
ALTER TABLE [dbo].[Bought_beer] DROP CONSTRAINT [FK_Bought_beer_Beers]
GO
ALTER TABLE [dbo].[Bought_beer] DROP CONSTRAINT [FK_Bought_beer_Bars]
GO
ALTER TABLE [dbo].[Beers] DROP CONSTRAINT [FK_Beers_BeerCategories]
GO
ALTER TABLE [dbo].[Bars_Beers] DROP CONSTRAINT [FK_Bars_Beers_Beers]
GO
ALTER TABLE [dbo].[Bars_Beers] DROP CONSTRAINT [FK_Bars_Beers_Bars]
GO
/****** Object:  View [dbo].[View_1]    Script Date: 2/3/2015 05:47:32 PM ******/
DROP VIEW [dbo].[View_1]
GO
/****** Object:  Table [dbo].[FavoriteBeer]    Script Date: 2/3/2015 05:47:32 PM ******/
DROP TABLE [dbo].[FavoriteBeer]
GO
/****** Object:  Table [dbo].[Customers]    Script Date: 2/3/2015 05:47:32 PM ******/
DROP TABLE [dbo].[Customers]
GO
/****** Object:  Table [dbo].[Bought_beer]    Script Date: 2/3/2015 05:47:32 PM ******/
DROP TABLE [dbo].[Bought_beer]
GO
/****** Object:  Table [dbo].[Beers]    Script Date: 2/3/2015 05:47:32 PM ******/
DROP TABLE [dbo].[Beers]
GO
/****** Object:  Table [dbo].[BeerCategories]    Script Date: 2/3/2015 05:47:32 PM ******/
DROP TABLE [dbo].[BeerCategories]
GO
/****** Object:  Table [dbo].[Bars_Beers]    Script Date: 2/3/2015 05:47:32 PM ******/
DROP TABLE [dbo].[Bars_Beers]
GO
/****** Object:  Table [dbo].[Bars]    Script Date: 2/3/2015 05:47:32 PM ******/
DROP TABLE [dbo].[Bars]
GO
/****** Object:  StoredProcedure [dbo].[SetFavoriteBeer]    Script Date: 2/3/2015 05:47:32 PM ******/
DROP PROCEDURE [dbo].[SetFavoriteBeer]
GO
/****** Object:  StoredProcedure [dbo].[InsertBeers]    Script Date: 2/3/2015 05:47:32 PM ******/
DROP PROCEDURE [dbo].[InsertBeers]
GO
USE [master]
GO
/****** Object:  Database [Exam]    Script Date: 2/3/2015 05:47:32 PM ******/
DROP DATABASE [Exam]
GO
/****** Object:  Database [Exam]    Script Date: 2/3/2015 05:47:32 PM ******/
CREATE DATABASE [Exam]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Exam', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\Exam.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Exam_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\Exam_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [Exam] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Exam].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Exam] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Exam] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Exam] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Exam] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Exam] SET ARITHABORT OFF 
GO
ALTER DATABASE [Exam] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Exam] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [Exam] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Exam] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Exam] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Exam] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Exam] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Exam] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Exam] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Exam] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Exam] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Exam] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Exam] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Exam] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Exam] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Exam] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Exam] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Exam] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Exam] SET RECOVERY FULL 
GO
ALTER DATABASE [Exam] SET  MULTI_USER 
GO
ALTER DATABASE [Exam] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Exam] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Exam] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Exam] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
EXEC sys.sp_db_vardecimal_storage_format N'Exam', N'ON'
GO
USE [Exam]
GO
/****** Object:  StoredProcedure [dbo].[InsertBeers]    Script Date: 2/3/2015 05:47:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[InsertBeers]
	@quantity INT ,
	@numberOfEntries INT
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @customerId TABLE(ID int)

	INSERT INTO @customerId
	SELECT Id FROM [dbo].Customers 

	DECLARE  @counter INT --counter for number of classes
	DECLARE @innerCounter INT
	DECLARE @currentID INT
	DECLARE @dateI DATE
    
	DECLARE @currentCustomer INT

	SET @counter = 0
	WHILE @counter < (SELECT COUNT(*) FROM dbo.Customers)
		BEGIN
		SELECT @currentCustomer = (SELECT TOP 1 [ID] FROM @customerId )

		SET @dateI = (select Cast(Cast(dateadd(month, -1, GETDATE()) as Date) as Datetime))
		
		SET @innerCounter = 0
		WHILE @innerCounter < @numberOfEntries
			BEGIN

			
INSERT INTO [dbo].[Bought_beer]
           ([BarId]
           ,[CustomerId]
           ,[Quantity]
           ,[TimeStamp])
     VALUES
           (1
           ,@currentCustomer
           ,@quantity
           ,@dateI)

			SET @innerCounter = @innerCounter + 1
			END
		DELETE FROM @customerId WHERE Id = @currentCustomer
		SET @counter = @counter+1
		PRINT'AFTER SECOND WHILE'
		END

END

GO
/****** Object:  StoredProcedure [dbo].[SetFavoriteBeer]    Script Date: 2/3/2015 05:47:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[SetFavoriteBeer]
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @lastMonth DATE
	SET @lastMonth =( select Cast(dateadd(month, -1, GETDATE()) as Date))

	DECLARE @customerId TABLE(ID int)
	DECLARE @beerToCustomer INT

	INSERT INTO @customerId
	SELECT Id FROM [dbo].Customers 

	DECLARE  @counter INT --counter for number of classes
    
	DECLARE @currentCustomer INT
	DECLARE @beerID INT

	SET @counter = 0
	WHILE @counter < (SELECT COUNT(*) FROM dbo.Customers)
	BEGIN
		SELECT @currentCustomer = (SELECT TOP 1 [ID] FROM @customerId )
		SET @beerToCustomer = (SELECT COUNT(*) FROM Bought_Beer bb WHERE bb.CustomerId =@currentCustomer AND CAST(bb.TimeStamp as DATE) = @lastMonth)
		IF @beerToCustomer >= 5
		BEGIN
			SET @beerID =(SELECT TOP 1 bb.BeerId FROM Bought_Beer bb WHERE bb.CustomerId =@currentCustomer AND CAST(bb.TimeStamp as DATE) = @lastMonth)
			IF (SELECT COUNT(*) FROM FavoriteBeer fb WHERE fb.BeerId = @beerID) = 0
			BEGIN
				
				--SET @beerID = (SELECT bb.BeerId FROM Bought_Beer bb WHERE bb.CustomerId =@currentCustomer AND CAST(bb.TimeStamp as DATE) = @lastMonth)
					INSERT INTO [dbo].[FavoriteBeer]
				   ([BeerId]
				   ,[CustomerId])
			 VALUES
				   (@beerID
				   ,@currentCustomer)
			END
		END

		DELETE FROM @customerId WHERE Id = @currentCustomer
		SET @counter = @counter+1
	END
END

GO
/****** Object:  Table [dbo].[Bars]    Script Date: 2/3/2015 05:47:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bars](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Bars] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Bars_Beers]    Script Date: 2/3/2015 05:47:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bars_Beers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BarId] [int] NOT NULL,
	[BeerId] [int] NOT NULL,
	[Price] [float] NOT NULL,
 CONSTRAINT [PK_Bars_Beers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BeerCategories]    Script Date: 2/3/2015 05:47:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BeerCategories](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_BeerCategories] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Beers]    Script Date: 2/3/2015 05:47:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Beers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[ProducerPrice] [float] NOT NULL,
	[CategoryId] [int] NOT NULL,
 CONSTRAINT [PK_Beers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Bought_beer]    Script Date: 2/3/2015 05:47:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bought_beer](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BarId] [int] NOT NULL,
	[CustomerId] [int] NOT NULL,
	[Quantity] [int] NOT NULL,
	[TimeStamp] [datetime] NOT NULL,
	[BeerId] [int] NULL,
 CONSTRAINT [PK_Bought_beer] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Customers]    Script Date: 2/3/2015 05:47:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Customers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FavoriteBeer]    Script Date: 2/3/2015 05:47:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FavoriteBeer](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BeerId] [int] NOT NULL,
	[CustomerId] [int] NOT NULL,
 CONSTRAINT [PK_FavoriteBeer] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[View_1]    Script Date: 2/3/2015 05:47:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_1]
AS
SELECT DISTINCT dbo.Bars.Name
FROM            dbo.Bars INNER JOIN
                         dbo.Bars_Beers ON dbo.Bars.Id = dbo.Bars_Beers.BarId INNER JOIN
                         dbo.Beers ON dbo.Bars_Beers.BeerId = dbo.Beers.Id AND dbo.Bars_Beers.Price > dbo.Beers.ProducerPrice * 2

GO
SET IDENTITY_INSERT [dbo].[Bars] ON 

INSERT [dbo].[Bars] ([Id], [Name]) VALUES (1, N'Petrica')
INSERT [dbo].[Bars] ([Id], [Name]) VALUES (2, N'Seven')
INSERT [dbo].[Bars] ([Id], [Name]) VALUES (3, N'Mist')
INSERT [dbo].[Bars] ([Id], [Name]) VALUES (4, N'Big Apple')
INSERT [dbo].[Bars] ([Id], [Name]) VALUES (5, N'Gigel')
INSERT [dbo].[Bars] ([Id], [Name]) VALUES (6, N'Vasilica')
INSERT [dbo].[Bars] ([Id], [Name]) VALUES (7, N'Mioara')
SET IDENTITY_INSERT [dbo].[Bars] OFF
SET IDENTITY_INSERT [dbo].[Bars_Beers] ON 

INSERT [dbo].[Bars_Beers] ([Id], [BarId], [BeerId], [Price]) VALUES (1, 1, 5, 5000)
INSERT [dbo].[Bars_Beers] ([Id], [BarId], [BeerId], [Price]) VALUES (2, 2, 1, 3)
INSERT [dbo].[Bars_Beers] ([Id], [BarId], [BeerId], [Price]) VALUES (3, 3, 4, 10000)
INSERT [dbo].[Bars_Beers] ([Id], [BarId], [BeerId], [Price]) VALUES (4, 1, 1, 1)
INSERT [dbo].[Bars_Beers] ([Id], [BarId], [BeerId], [Price]) VALUES (5, 1, 4, 60000)
SET IDENTITY_INSERT [dbo].[Bars_Beers] OFF
SET IDENTITY_INSERT [dbo].[BeerCategories] ON 

INSERT [dbo].[BeerCategories] ([Id], [Name]) VALUES (1, N'Cheep')
INSERT [dbo].[BeerCategories] ([Id], [Name]) VALUES (2, N'Medium price')
INSERT [dbo].[BeerCategories] ([Id], [Name]) VALUES (3, N'Expensive')
INSERT [dbo].[BeerCategories] ([Id], [Name]) VALUES (4, N'Wow')
SET IDENTITY_INSERT [dbo].[BeerCategories] OFF
SET IDENTITY_INSERT [dbo].[Beers] ON 

INSERT [dbo].[Beers] ([Id], [Name], [ProducerPrice], [CategoryId]) VALUES (1, N'Ursus', 10, 1)
INSERT [dbo].[Beers] ([Id], [Name], [ProducerPrice], [CategoryId]) VALUES (2, N'Bucegi', 20, 3)
INSERT [dbo].[Beers] ([Id], [Name], [ProducerPrice], [CategoryId]) VALUES (3, N'Abita Amber', 100, 4)
INSERT [dbo].[Beers] ([Id], [Name], [ProducerPrice], [CategoryId]) VALUES (4, N'Abita Andygator', 200, 4)
INSERT [dbo].[Beers] ([Id], [Name], [ProducerPrice], [CategoryId]) VALUES (5, N'Dogfish Head Hellhound', 500, 4)
INSERT [dbo].[Beers] ([Id], [Name], [ProducerPrice], [CategoryId]) VALUES (6, N'Asdf', 1, 1)
SET IDENTITY_INSERT [dbo].[Beers] OFF
SET IDENTITY_INSERT [dbo].[Bought_beer] ON 

INSERT [dbo].[Bought_beer] ([Id], [BarId], [CustomerId], [Quantity], [TimeStamp], [BeerId]) VALUES (2, 1, 1, 1, CAST(0x0000A41500000000 AS DateTime), 1)
INSERT [dbo].[Bought_beer] ([Id], [BarId], [CustomerId], [Quantity], [TimeStamp], [BeerId]) VALUES (3, 1, 1, 1, CAST(0x0000A41500000000 AS DateTime), 1)
INSERT [dbo].[Bought_beer] ([Id], [BarId], [CustomerId], [Quantity], [TimeStamp], [BeerId]) VALUES (11, 1, 1, 1, CAST(0x0000A41500000000 AS DateTime), 1)
INSERT [dbo].[Bought_beer] ([Id], [BarId], [CustomerId], [Quantity], [TimeStamp], [BeerId]) VALUES (12, 1, 1, 1, CAST(0x0000A41500000000 AS DateTime), 1)
INSERT [dbo].[Bought_beer] ([Id], [BarId], [CustomerId], [Quantity], [TimeStamp], [BeerId]) VALUES (21, 1, 1, 1, CAST(0x0000A41500000000 AS DateTime), 1)
INSERT [dbo].[Bought_beer] ([Id], [BarId], [CustomerId], [Quantity], [TimeStamp], [BeerId]) VALUES (22, 1, 3, 1, CAST(0x0000A41500000000 AS DateTime), 1)
INSERT [dbo].[Bought_beer] ([Id], [BarId], [CustomerId], [Quantity], [TimeStamp], [BeerId]) VALUES (27, 1, 3, 1, CAST(0x0000A41500000000 AS DateTime), 1)
INSERT [dbo].[Bought_beer] ([Id], [BarId], [CustomerId], [Quantity], [TimeStamp], [BeerId]) VALUES (29, 1, 3, 1, CAST(0x0000A41500000000 AS DateTime), 1)
INSERT [dbo].[Bought_beer] ([Id], [BarId], [CustomerId], [Quantity], [TimeStamp], [BeerId]) VALUES (31, 1, 4, 1, CAST(0x0000A41500000000 AS DateTime), 1)
INSERT [dbo].[Bought_beer] ([Id], [BarId], [CustomerId], [Quantity], [TimeStamp], [BeerId]) VALUES (33, 1, 4, 1, CAST(0x0000A41500000000 AS DateTime), 1)
INSERT [dbo].[Bought_beer] ([Id], [BarId], [CustomerId], [Quantity], [TimeStamp], [BeerId]) VALUES (41, 1, 5, 1, CAST(0x0000A41500000000 AS DateTime), 3)
INSERT [dbo].[Bought_beer] ([Id], [BarId], [CustomerId], [Quantity], [TimeStamp], [BeerId]) VALUES (42, 1, 5, 1, CAST(0x0000A41500000000 AS DateTime), 3)
INSERT [dbo].[Bought_beer] ([Id], [BarId], [CustomerId], [Quantity], [TimeStamp], [BeerId]) VALUES (43, 1, 5, 1, CAST(0x0000A41500000000 AS DateTime), 3)
INSERT [dbo].[Bought_beer] ([Id], [BarId], [CustomerId], [Quantity], [TimeStamp], [BeerId]) VALUES (44, 1, 5, 1, CAST(0x0000A41500000000 AS DateTime), 3)
INSERT [dbo].[Bought_beer] ([Id], [BarId], [CustomerId], [Quantity], [TimeStamp], [BeerId]) VALUES (45, 1, 5, 1, CAST(0x0000A41500000000 AS DateTime), 3)
INSERT [dbo].[Bought_beer] ([Id], [BarId], [CustomerId], [Quantity], [TimeStamp], [BeerId]) VALUES (46, 1, 5, 1, CAST(0x0000A41500000000 AS DateTime), 3)
INSERT [dbo].[Bought_beer] ([Id], [BarId], [CustomerId], [Quantity], [TimeStamp], [BeerId]) VALUES (47, 1, 5, 1, CAST(0x0000A41500000000 AS DateTime), 3)
INSERT [dbo].[Bought_beer] ([Id], [BarId], [CustomerId], [Quantity], [TimeStamp], [BeerId]) VALUES (48, 1, 5, 1, CAST(0x0000A41500000000 AS DateTime), 1)
INSERT [dbo].[Bought_beer] ([Id], [BarId], [CustomerId], [Quantity], [TimeStamp], [BeerId]) VALUES (49, 1, 5, 1, CAST(0x0000A41500000000 AS DateTime), 1)
INSERT [dbo].[Bought_beer] ([Id], [BarId], [CustomerId], [Quantity], [TimeStamp], [BeerId]) VALUES (50, 1, 5, 1, CAST(0x0000A41500000000 AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[Bought_beer] OFF
SET IDENTITY_INSERT [dbo].[Customers] ON 

INSERT [dbo].[Customers] ([Id], [Name]) VALUES (1, N'Gigel')
INSERT [dbo].[Customers] ([Id], [Name]) VALUES (2, N'Costel')
INSERT [dbo].[Customers] ([Id], [Name]) VALUES (3, N'Vasile')
INSERT [dbo].[Customers] ([Id], [Name]) VALUES (4, N'Vasilica')
INSERT [dbo].[Customers] ([Id], [Name]) VALUES (5, N'Oan')
SET IDENTITY_INSERT [dbo].[Customers] OFF
SET IDENTITY_INSERT [dbo].[FavoriteBeer] ON 

INSERT [dbo].[FavoriteBeer] ([Id], [BeerId], [CustomerId]) VALUES (2, 3, 5)
INSERT [dbo].[FavoriteBeer] ([Id], [BeerId], [CustomerId]) VALUES (3, 1, 1)
SET IDENTITY_INSERT [dbo].[FavoriteBeer] OFF
ALTER TABLE [dbo].[Bars_Beers]  WITH CHECK ADD  CONSTRAINT [FK_Bars_Beers_Bars] FOREIGN KEY([BarId])
REFERENCES [dbo].[Bars] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Bars_Beers] CHECK CONSTRAINT [FK_Bars_Beers_Bars]
GO
ALTER TABLE [dbo].[Bars_Beers]  WITH CHECK ADD  CONSTRAINT [FK_Bars_Beers_Beers] FOREIGN KEY([BeerId])
REFERENCES [dbo].[Beers] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Bars_Beers] CHECK CONSTRAINT [FK_Bars_Beers_Beers]
GO
ALTER TABLE [dbo].[Beers]  WITH CHECK ADD  CONSTRAINT [FK_Beers_BeerCategories] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[BeerCategories] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Beers] CHECK CONSTRAINT [FK_Beers_BeerCategories]
GO
ALTER TABLE [dbo].[Bought_beer]  WITH CHECK ADD  CONSTRAINT [FK_Bought_beer_Bars] FOREIGN KEY([BarId])
REFERENCES [dbo].[Bars] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Bought_beer] CHECK CONSTRAINT [FK_Bought_beer_Bars]
GO
ALTER TABLE [dbo].[Bought_beer]  WITH CHECK ADD  CONSTRAINT [FK_Bought_beer_Beers] FOREIGN KEY([BeerId])
REFERENCES [dbo].[Beers] ([Id])
GO
ALTER TABLE [dbo].[Bought_beer] CHECK CONSTRAINT [FK_Bought_beer_Beers]
GO
ALTER TABLE [dbo].[Bought_beer]  WITH CHECK ADD  CONSTRAINT [FK_Bought_beer_Customers] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customers] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Bought_beer] CHECK CONSTRAINT [FK_Bought_beer_Customers]
GO
ALTER TABLE [dbo].[FavoriteBeer]  WITH CHECK ADD  CONSTRAINT [FK_FavoriteBeer_Beers] FOREIGN KEY([BeerId])
REFERENCES [dbo].[Beers] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[FavoriteBeer] CHECK CONSTRAINT [FK_FavoriteBeer_Beers]
GO
ALTER TABLE [dbo].[FavoriteBeer]  WITH CHECK ADD  CONSTRAINT [FK_FavoriteBeer_Customers] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customers] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[FavoriteBeer] CHECK CONSTRAINT [FK_FavoriteBeer_Customers]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[21] 2[16] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Bars"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 129
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Bars_Beers"
            Begin Extent = 
               Top = 6
               Left = 246
               Bottom = 136
               Right = 416
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Beers"
            Begin Extent = 
               Top = 6
               Left = 454
               Bottom = 136
               Right = 624
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_1'
GO
USE [master]
GO
ALTER DATABASE [Exam] SET  READ_WRITE 
GO
