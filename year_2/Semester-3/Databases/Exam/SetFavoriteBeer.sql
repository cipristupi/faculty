SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--Here check if exists the store procedure. If exist we delete it and recreate
IF EXISTS ( SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_NAME = 'SetFavoriteBeer')
	BEGIN
		DROP PROC SetFavoriteBeer
	END
GO


CREATE PROCEDURE [dbo].SetFavoriteBeer
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @lastMonth DATE
	SET @lastMonth =( select Cast(dateadd(month, -1, GETDATE()) as Date))

	DECLARE @customerId TABLE(ID int)
	DECLARE @beerToCustomer INT

	INSERT INTO @customerId
	SELECT Id FROM [dbo].Customers 

	DECLARE  @counter INT --counter for number of classes
    
	DECLARE @currentCustomer INT
	DECLARE @beerID INT

	SET @counter = 0
	WHILE @counter < (SELECT COUNT(*) FROM dbo.Customers)
	BEGIN
		SELECT @currentCustomer = (SELECT TOP 1 [ID] FROM @customerId )
		SET @beerToCustomer = (SELECT COUNT(*) FROM Bought_Beer bb WHERE bb.CustomerId =@currentCustomer AND CAST(bb.TimeStamp as DATE) = @lastMonth)
		IF @beerToCustomer >= 5
		BEGIN
			SET @beerID =(SELECT TOP 1 bb.BeerId FROM Bought_Beer bb WHERE bb.CustomerId =@currentCustomer AND CAST(bb.TimeStamp as DATE) = @lastMonth)
			IF (SELECT COUNT(*) FROM FavoriteBeer fb WHERE fb.BeerId = @beerID) = 0
			BEGIN
				
				--SET @beerID = (SELECT bb.BeerId FROM Bought_Beer bb WHERE bb.CustomerId =@currentCustomer AND CAST(bb.TimeStamp as DATE) = @lastMonth)
					INSERT INTO [dbo].[FavoriteBeer]
				   ([BeerId]
				   ,[CustomerId])
			 VALUES
				   (@beerID
				   ,@currentCustomer)
			END
		END

		DELETE FROM @customerId WHERE Id = @currentCustomer
		SET @counter = @counter+1
	END
END
GO