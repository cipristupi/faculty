--This script add an contraint to a given column
--Replace [DevTable] with your table name
--Replace [test_constraint] with your name for contraint
--Replace ['TEST'] with you default value for insert
--Replace ["Test"] with you column name where you add the constraint
ALTER TABLE  DevTable ADD CONSTRAINT test_constraint DEFAULT 'TEST' FOR "Test"