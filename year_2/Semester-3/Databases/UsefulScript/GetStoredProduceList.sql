--This script returns the list of all stored procedure from current database
--Add [ AND SPECIFIC_NAME = 'storedProcedureName'] to get information for a given stored procedure
SELECT * FROM INFORMATION_SCHEMA.ROUTINES 
WHERE ROUTINE_TYPE = 'PROCEDURE' 