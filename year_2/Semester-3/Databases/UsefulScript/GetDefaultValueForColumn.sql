--This script gets the default value for a given column
SELECT COLUMN_DEFAULT 
FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_NAME = 'DevTable' AND COLUMN_NAME = 'Column1'