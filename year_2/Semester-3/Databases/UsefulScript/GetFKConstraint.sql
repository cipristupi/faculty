--This script returns a given foreign_keys contraint with a given name
--Can use this script to check if a fk constraint was created
--Replace 'test_fk' with your constraint name
SELECT * FROM sys.foreign_keys WHERE   name = 'test_fk'