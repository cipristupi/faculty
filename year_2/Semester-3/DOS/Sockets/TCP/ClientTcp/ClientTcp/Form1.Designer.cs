﻿namespace ClientTcp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ip_txtbox = new System.Windows.Forms.TextBox();
            this.port_txtbox = new System.Windows.Forms.TextBox();
            this.connect_bttn = new System.Windows.Forms.Button();
            this.msg_to_sent = new System.Windows.Forms.RichTextBox();
            this.msg_from_srv = new System.Windows.Forms.RichTextBox();
            this.send_bttn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ip_txtbox
            // 
            this.ip_txtbox.Location = new System.Drawing.Point(12, 23);
            this.ip_txtbox.Name = "ip_txtbox";
            this.ip_txtbox.Size = new System.Drawing.Size(143, 20);
            this.ip_txtbox.TabIndex = 0;
            this.ip_txtbox.Text = "Server Ip";
            // 
            // port_txtbox
            // 
            this.port_txtbox.Location = new System.Drawing.Point(12, 59);
            this.port_txtbox.Name = "port_txtbox";
            this.port_txtbox.Size = new System.Drawing.Size(143, 20);
            this.port_txtbox.TabIndex = 1;
            this.port_txtbox.Text = "Server Port";
            // 
            // connect_bttn
            // 
            this.connect_bttn.Location = new System.Drawing.Point(12, 97);
            this.connect_bttn.Name = "connect_bttn";
            this.connect_bttn.Size = new System.Drawing.Size(143, 33);
            this.connect_bttn.TabIndex = 2;
            this.connect_bttn.Text = "Connect";
            this.connect_bttn.UseVisualStyleBackColor = true;
            this.connect_bttn.Click += new System.EventHandler(this.connect_bttn_Click);
            // 
            // msg_to_sent
            // 
            this.msg_to_sent.Location = new System.Drawing.Point(12, 146);
            this.msg_to_sent.Name = "msg_to_sent";
            this.msg_to_sent.Size = new System.Drawing.Size(238, 216);
            this.msg_to_sent.TabIndex = 3;
            this.msg_to_sent.Text = "";
            // 
            // msg_from_srv
            // 
            this.msg_from_srv.Location = new System.Drawing.Point(278, 23);
            this.msg_from_srv.Name = "msg_from_srv";
            this.msg_from_srv.Size = new System.Drawing.Size(474, 339);
            this.msg_from_srv.TabIndex = 4;
            this.msg_from_srv.Text = "";
            // 
            // send_bttn
            // 
            this.send_bttn.Location = new System.Drawing.Point(12, 369);
            this.send_bttn.Name = "send_bttn";
            this.send_bttn.Size = new System.Drawing.Size(238, 36);
            this.send_bttn.TabIndex = 5;
            this.send_bttn.Text = "Send";
            this.send_bttn.UseVisualStyleBackColor = true;
            this.send_bttn.Click += new System.EventHandler(this.send_bttn_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(764, 417);
            this.Controls.Add(this.send_bttn);
            this.Controls.Add(this.msg_from_srv);
            this.Controls.Add(this.msg_to_sent);
            this.Controls.Add(this.connect_bttn);
            this.Controls.Add(this.port_txtbox);
            this.Controls.Add(this.ip_txtbox);
            this.Name = "Form1";
            this.Text = "Client";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox ip_txtbox;
        private System.Windows.Forms.TextBox port_txtbox;
        private System.Windows.Forms.Button connect_bttn;
        private System.Windows.Forms.RichTextBox msg_to_sent;
        private System.Windows.Forms.RichTextBox msg_from_srv;
        private System.Windows.Forms.Button send_bttn;
    }
}

