﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientTcp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        UdpClient client = new UdpClient();
        NetworkStream serverStream;
        private bool connected =false;
        private void connect_bttn_Click(object sender, EventArgs e)
        {

            if (String.IsNullOrEmpty(ip_txtbox.Text) || String.IsNullOrEmpty(port_txtbox.Text))
                MessageBox.Show("Please provide server ip and server port");
            else
            {
                if (connected == false)
                {
                    try
                    {
                        IPAddress address = IPAddress.Parse(ip_txtbox.Text);
                        client.Connect(address, int.Parse(port_txtbox.Text));
                        ((Button)sender).Text = "Connected";
                        ((Button)sender).BackColor = Color.Green;
                        connected = true;
                        msg("Client Started");
                    }
                    catch (Exception ex)
                    {
                        
                       msg(ex.ToString());
                    }
                   
                }
                else
                {
                    
                    ((Button)sender).Text = "Connect";
                    ((Button)sender).BackColor = Color.Gray;
                    connected = false;
                    msg("Close Port Command Sent");  //user feedback
                    client.Close();  //close connection
                    msg("Client Disconnected");
                }
            }
        }

        private void send_bttn_Click(object sender, EventArgs e)
        {
            string data = "";
            byte[] sendBytes = new Byte[100240];
            byte[] rcvPacket = new Byte[100240];
            IPEndPoint remoteIPEndPoint = new IPEndPoint(IPAddress.Any, 0);

            msg("Client is Started");
            msg("Type your message");

            data = msg_to_sent.Text;
            sendBytes = Encoding.ASCII.GetBytes(DateTime.Now.ToString() + " " + data);
            client.Send(sendBytes, sendBytes.GetLength(0));
            rcvPacket = client.Receive(ref remoteIPEndPoint);

            string rcvData = Encoding.ASCII.GetString(rcvPacket);
            msg("Handling client at " + remoteIPEndPoint + " - ");
            msg("Message Received: " + rcvPacket.ToString());
        }

        public void msg(string mesg)
        {

            msg_from_srv.Text = msg_from_srv.Text + Environment.NewLine + " >> " + mesg;

        } 
    }
}
