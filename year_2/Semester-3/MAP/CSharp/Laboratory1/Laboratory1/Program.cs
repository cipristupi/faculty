﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laboratory1
{
    class Program
    {
        static void Main(string[] args)
        {
            string date;//look out at format for date. It's american format
            DateTime birthday;
            TimeSpan ageInDays;
            Console.Write("Birthday date: ");
            date = Console.ReadLine();
            try
            {
                birthday = DateTime.Parse(date);
                ageInDays = DateTime.Now - birthday;
                Console.WriteLine("Days: " + ageInDays.Days.ToString());
            }
            catch
            {
                Console.WriteLine("Please provide a valid date");
            }
            Console.ReadLine();
        }
    }
}
