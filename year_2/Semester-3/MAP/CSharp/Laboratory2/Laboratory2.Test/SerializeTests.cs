﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Laboratory2.Repository;
using Laboratory2.Models;

namespace Laboratory2.Test
{
    [TestClass]
    public class SerializeTests
    {
        [TestMethod]
        public void Serialize_Deserialize_No_Error()
        {
            MainRepository r = new MainRepository();
            Statement s = new CompoundStmt(new AssignStmt("v", new ConstExpr(2)), new PrintStmt(new ArithExpr(new VarExpr("v"), new ConstExpr(3), '+')));
            r.addPrgState(s);
            r.Serialize(0);
            PrgState prgState = r.Deserialize(0);
            PrgState inMemPrgState = r.getPrgStateById(0);
            //Assert.AreEqual(prgState.getStack(), inMemPrgState.getStack());

            r.SaveTofile(0);
        }
    }
}
