﻿using Laboratory2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laboratory2.Repository
{
    [Serializable]
    public class PrgState
    {
        private Dictionary<string, int> tableOfSymbols;
        private Stack<Statement> exeStack;
        private List<string> outBuff;

        public PrgState(Statement initialStatement)
        {
            tableOfSymbols = new Dictionary<string, int>();
            exeStack = new Stack<Statement>();
            outBuff = new List<String>();
            exeStack.Push(initialStatement);
        }

        public Dictionary<string, int> getTable()
        {
            return tableOfSymbols;
        }
        public Stack<Statement> getStack()
        {
            return exeStack;
        }
        public List<string> getOut()
        {
            return outBuff;
        }

        public override string ToString()
        {
            string o = "";
            o += "\n //Stack content \n";
            foreach (Statement s in exeStack)
            {
                o += s.ToString() + "\n";
            }
            o += "\n //SymTable" + "\n";
            foreach (KeyValuePair<string, int> pair in tableOfSymbols)
            {
                o += String.Format("{0} -> {1} \n", pair.Key, pair.Value);
            }
            o += "\n //Out content \n";
            foreach (string s in outBuff)
            {
                o += s + "\n";
            }
            o += "\n";
            return o;

        }
    }
}
