﻿using Laboratory2.Models;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace Laboratory2.Repository
{
    public class MainRepository
    {
        private Dictionary<int, PrgState> prgstateList;

        public MainRepository()
        {
            prgstateList = new Dictionary<int, PrgState>();
        }

        /// <summary>
        /// Add statement to a given prgState
        /// </summary>
        /// <param name="s"></param>
        /// <param name="i"></param>
        public void addToState(Statement s, int i)
        {
            prgstateList[i].getStack().Push(s);
        }

        public void addPrgState(Statement initialState)
        {
            prgstateList.Add(0, new PrgState(initialState));
        }

        public List<PrgState> getAllPrgState()
        {
            List<PrgState> stmtList = new List<PrgState>();
            List<int> keys = prgstateList.Keys.ToList();
            for (int i = 0; i < keys.Count; i++)
            {
                stmtList.Add(prgstateList[keys[i]]);
            }
            return stmtList;
        }

        public PrgState getPrgStateById(int i)
        {
            return prgstateList[i];
        }

        public bool programExists(int id)
        {
            return prgstateList[id] != null ? true : false;
        }

        public void Serialize(int programID)
        {
            PrgState s = this.getPrgStateById(programID);
            string uriPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase) + "\\outputs\\" + programID.ToString() + ".txt";
            string filePath = new Uri(uriPath).LocalPath;
            IFormatter formatter = new BinaryFormatter();
            Stream stream = new FileStream(filePath, FileMode.Create, FileAccess.Write, FileShare.None);
            formatter.Serialize(stream, s);
            stream.Close();
        }

        public PrgState Deserialize(int programID)
        {
            string uriPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase) + "\\outputs\\" + programID.ToString() + ".txt";
            string filePath = new Uri(uriPath).LocalPath;
            IFormatter formatter = new BinaryFormatter();
            Stream stream = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read);
            PrgState obj = (PrgState)formatter.Deserialize(stream);
            stream.Close();
            return obj;
        }

        public void SaveTofile(int programID)
        {
            PrgState r = this.getPrgStateById(programID);
            SaveTofile(r);
        }

        public void SaveTofile(PrgState r, bool append = false)
        {
            string uriPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase) + "\\outputs\\" + "prgState" + ".txt";
            string filePath = new Uri(uriPath).LocalPath;
            string result = r.ToString();
            using (StreamWriter file = new StreamWriter(filePath, append))//append new line at the end of file
            {
                if(append)
                {
                    result += "\n--------------------------------------------------";
                }
                file.WriteLine(result);
                file.Flush();
            }
        }
    }
}
