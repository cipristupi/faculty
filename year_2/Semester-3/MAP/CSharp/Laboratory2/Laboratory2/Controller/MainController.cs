﻿using Laboratory2.Models;
using Laboratory2.Repository;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using Laboratory2.Utils.Observer;

namespace Laboratory2.Controller
{
    class MainController
    {
        private MainRepository repo;
        private PrgState currentPrg;
        private Subject subject;
        private IMyObserver stackObserver;
        private IMyObserver outObserver;
        private IMyObserver tableObserver;
        public MainController(MainRepository r)
        {
            repo = r;
            subject = new Subject();
            stackObserver = new StackObserver();
            outObserver = new OutObserver();
            tableObserver = new TableObserver();
            subject.Attach(stackObserver);
            subject.Attach(outObserver);
            subject.Attach(tableObserver);
        }


        public PrgState run(int programID)
        {
            if (!repo.programExists(programID))
            {
                return null;
            }
            currentPrg = repo.getPrgStateById(programID);
            while (currentPrg.getStack().Count != 0)
            {
                runStepByStep(programID);
            }
            return currentPrg;

        }
        public PrgState runStepByStep(int programID)
        {
            if (!repo.programExists(programID))
            {
                return null;
            }
            InitPrgState(programID);
            Statement st = currentPrg.getStack().Pop();
            subject.Notify(currentPrg);//here the stack is modified
            if (st is CompoundStmt)
            {
                execCompound((CompoundStmt)st);
            }
            else
                if (st is IfStmt)
                {
                    execIf((IfStmt)st);
                }
                else
                    if (st is PrintStmt)
                    {
                        execPrint((PrintStmt)st);
                    }
                    else
                        if (st is AssignStmt)
                        {
                            execAssign((AssignStmt)st);
                        }
            return currentPrg;
        }


        private void execAssign(AssignStmt st)
        {
            Expression expr = st.getExpr();
            String varName = st.getName();
            if (expr is ConstExpr)
            {
                int value = ((ConstExpr)expr).getConstant();
                currentPrg.getTable().Add(varName, value);
                
            }
            else if (expr is VarExpr)
            {
                String id = ((VarExpr)expr).getName();
                int value = currentPrg.getTable()[id];
                currentPrg.getTable().Add(st.getName(), value);
            }
            else if (expr is ArithExpr)
            {
                int value = ((ArithExpr)expr).eval(currentPrg.getTable());
                currentPrg.getTable().Add(st.getName(), value);
            }
            subject.Notify(currentPrg);//observer for table
        }

        private void execPrint(PrintStmt st)
        {
            Expression toPrint = st.getExpr();
            try
            {
                int result = 0;
                if (toPrint is ConstExpr)
                {
                    result = ((ConstExpr)toPrint).getConstant();
                }
                else if (toPrint is VarExpr)
                {
                    String id = ((VarExpr)toPrint).getName();
                    result = currentPrg.getTable()[id];
                }
                else if (toPrint is ArithExpr)
                {
                    result = ((ArithExpr)toPrint).eval(currentPrg.getTable());
                }
                String x = result.ToString();
                currentPrg.getOut().Add(x);
            }
            catch (Exception ex)
            {
                currentPrg.getOut().Add(ex.ToString());
            }
            subject.Notify(currentPrg);//observer for outbuff
        }

        private void execIf(IfStmt st)
        {
            Expression expr = st.getExpr();
            int value;
            if (expr is ConstExpr)
            {
                value = ((ConstExpr)expr).getConstant();
            }
            else
                if (expr is VarExpr)
                {
                    String id = ((VarExpr)expr).getName();
                    value = currentPrg.getTable()[id];
                }
                else
                    if (expr is ArithExpr)
                    {
                        value = ((ArithExpr)expr).eval(currentPrg.getTable());
                    }
                    else
                    {
                        value = 0;
                    }

            if (value != 0)
            {
                currentPrg.getStack().Push(st.getSt1());
            }
            else
            {
                currentPrg.getStack().Push(st.getSt2());
            }
            subject.Notify(currentPrg);//observer for stack
        }

        private void execCompound(CompoundStmt st)
        {
            currentPrg.getStack().Push(st.getStmt2());
            currentPrg.getStack().Push(st.getStmt1());
            subject.Notify( currentPrg);//observer for table
        }

        public void addPrgState(Statement s)
        {
            repo.addPrgState(s);
        }

        public PrgState GetPrgState(int i)
        {
            return repo.getPrgStateById(i);
        }
        public PrgState GetCurrentPrgState()
        {
            return currentPrg;
        }

        public Stack<Statement> GetCurrentStack(int programID)
        {
            InitPrgState(programID);
            return currentPrg.getStack();
        }


        public void InitPrgState(int programID)
        {
            if (currentPrg == null)
                currentPrg = repo.getPrgStateById(programID);

        }
    }
}
