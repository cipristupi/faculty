﻿using Laboratory2.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laboratory2.Utils.Observer
{
    interface IMyObserver
    {
        void Update(PrgState prgState);
    }
}
