﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laboratory2.Models
{
    [Serializable]
    public class IfStmt : Statement
    {
        private Expression expr;
        private Statement st1;
        private Statement st2;
        public IfStmt(Expression ex, Statement t, Statement e)
        {
            expr = ex;
            st1 = t;
            st2 = e;
        }

        public Expression getExpr()
        {
            return expr;
        }
        public Statement getSt1()
        {
            return st1;
        }
        public Statement getSt2()
        {
            return st2;
        }

        public override string ToString()
        {
            String s = "";
            s += "If ";
            s += expr.ToString();
            s += " then ";
            s += st1.ToString();
            if (st2.ToString() != "")
            {
                s += " else ";
                s += st2.ToString();
            }
            return s;
        }
    }
}
