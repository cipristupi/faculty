﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laboratory2.Models
{
    [Serializable]
    public class VarExpr : Expression
    {
        private string varName;
        public VarExpr(string v)
        {
            varName = v;
        }
        public string getName()
        {
            return varName;
        }

        public override string ToString()
        {
            return varName;
        }
    }
}
