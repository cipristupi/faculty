﻿using Laboratory2.Controller;
using Laboratory2.Models;
using Laboratory2.Repository;
using Laboratory2.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laboratory2
{
    class Program
    {
        static void Main(string[] args)
        {
            //Statement s = new IfStmt(new ConstExpr(1), new PrintStmt(new ConstExpr(0)), new PrintStmt(new ConstExpr(1)));
            Statement s = new CompoundStmt(new AssignStmt("v", new ConstExpr(2)), new PrintStmt(new ArithExpr(new VarExpr("v"), new ConstExpr(3), '+')));
            MainRepository repo = new MainRepository();
            MainController cntr = new MainController(repo);
            MainConsole console = new MainConsole(cntr);
            console.EntryPoint();
        }
    }
}
