﻿using Laboratory2.Controller;
using Laboratory2.Models;
using Laboratory2.Repository;
using Laboratory2.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Laboratory2
{
    class Program : Form
    {
        [STAThread] 
        static void Main()
        {
            //Application.EnableVisualStyles();
            //Application.Run(new MainForm()); //of course you can put your own custom Form here.
            Statement s = new IfStmt(new ConstExpr(1), new PrintStmt(new ConstExpr(0)), new PrintStmt(new ConstExpr(1)));
            Statement s1 = new CompoundStmt(new AssignStmt("v", new ConstExpr(2)), new PrintStmt(new ArithExpr(new VarExpr("v"), new ConstExpr(3), '+')));
            Statement s2 = new CompoundStmt(new PrintStmt(new ConstExpr(3)), new ForkStmt(new CompoundStmt(new AssignStmt("v", new ConstExpr(2)), new PrintStmt(new ArithExpr(new VarExpr("v"), new ConstExpr(3), '+')))));
            Statement s3 = new CompoundStmt(new AssignStmt("v", new NewExpr(2)), new ForkStmt(new PrintStmt(new ArithExpr(new RExpr("v"), new ConstExpr(3), '+'))));
            MainRepository repo = new MainRepository();
            repo.AddStatement(s);
            repo.AddStatement(s1);
            repo.AddStatement(s2);
            repo.AddStatement(s3);
            MainController cntr = new MainController(repo);
            List<Statement> sa = cntr.AllPrograms();
            MainConsole console = new MainConsole(cntr);
            console.EntryPoint();
        }
    }
}
