﻿using Laboratory2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laboratory2.Repository
{
    [Serializable]
    public class PrgState
    {
        private Dictionary<string, int> tableOfSymbols;
        private Stack<Statement> exeStack;
        private List<string> outBuff;
        private int?[] heap;

        public PrgState(Statement initialStatement)
        {
            tableOfSymbols = new Dictionary<string, int>();
            exeStack = new Stack<Statement>();
            outBuff = new List<String>();
            exeStack.Push(initialStatement);
            heap = new int?[50];
        }

        public PrgState(Dictionary<string,int> st, Stack<Statement> s, List<string> o, int?[] h)
        {
            tableOfSymbols = st;
            exeStack = s;
            outBuff = o;
            heap = h;
        }

        public void AddStatement(Statement s)
        {
            exeStack.Push(s);
        }

        public Dictionary<string, int> getTable()
        {
            return tableOfSymbols;
        }
        public Stack<Statement> getStack()
        {
            return exeStack;
        }
        public List<string> getOut()
        {
            return outBuff;
        }

        public int?[] getHeap()
        {
            return heap;
        }

        public void setHeap(int?[] n)
        {
            heap = n;
        }

        public void setOut(List<string> n)
        {
            outBuff = n;
        }

        public void copyTable(Dictionary<string, int> s)
        {
            foreach (var pair in s)
            {
                tableOfSymbols.Add(pair.Key, pair.Value);
            }
        }

        public int addToHeap(int value)
        {
            int pos=0;
            for (int i = 0; i < 50; i++)
            {
                if (heap[i] == null)
                {
                    heap[i] = value;
                    pos = i;
                    break;
                }
            }
            return pos;
        }


        public override string ToString()
        {
            string o = "";
            o += "\n //Stack content \n";
            foreach (Statement s in exeStack)
            {
                o += s.ToString() + "\n";
            }
            o += "\n //SymTable" + "\n";
            foreach (KeyValuePair<string, int> pair in tableOfSymbols)
            {
                o += String.Format("{0} -> {1} \n", pair.Key, pair.Value);
            }
            o += "\n //Out content \n";
            foreach (string s in outBuff)
            {
                o += s + "\n";
            }
            o += "\n";
            return o;

        }
    }
}
