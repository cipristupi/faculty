﻿using Laboratory2.Models;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace Laboratory2.Repository
{
    public class MainRepository
    {
        private List<Statement> programList;
        private List<PrgState> prgstateList;

        public MainRepository()
        {
            prgstateList = new List<PrgState>();
            programList = new List<Statement>();
        }

        public void addToState(Statement s, int i)
        {
            prgstateList[i].getStack().Push(s);
        }

        public void AddStatement(Statement statement)
        {
            programList.Add(statement);
            //prgstateList.Add(id, new PrgState(statement));
        }

        public List<PrgState> getAllPrgState()
        {
            //List<PrgState> stmtList = new List<PrgState>();
            //List<int> keys = prgstateList.Keys.ToList();
            //for (int i = 0; i < keys.Count; i++)
            //{
            //    stmtList.Add(prgstateList[keys[i]]);
            //}
            //return stmtList;
            return prgstateList;
        }

        public void NewPrgState(int id)
        {
            prgstateList.Add(new PrgState(programList[id]));
        }

        public List<Statement> getAllPrograms()
        {
            //List<Statement> prgList = new List<Statement>();
            //List<int> keys = prgstateList.Keys.ToList();
            //for (int i = 0; i < keys.Count; i++)
            //{
            //    prgList.Add(programList[keys[i]]);
            //}
            //return prgList;
            return programList;
        }

        public void addFork(Statement s, PrgState parent)
        {
            prgstateList.Add(new PrgState(s));
            int last = prgstateList.Count - 1;
            PrgState f = prgstateList[last];
            f.setHeap(parent.getHeap());
            f.copyTable(parent.getTable());
            f.setOut(parent.getOut());
        }

        public PrgState getPrgStateById(int i)
        {
            return prgstateList[i];
        }

        public Statement getProgramById(int i)
        {
            return programList[i];
        }

        public bool programExists(int id)
        {
            return prgstateList[id] != null ? true : false;
            //return programList[id] != null ? true : false;
        }


        #region Serialization and File Operations
        public void Serialize(int programID)
        {
            PrgState s = this.getPrgStateById(programID);
            string uriPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase) + "\\outputs\\" + programID.ToString() + ".txt";
            string filePath = new Uri(uriPath).LocalPath;
            IFormatter formatter = new BinaryFormatter();
            Stream stream = new FileStream(filePath, FileMode.Create, FileAccess.Write, FileShare.None);
            formatter.Serialize(stream, s);
            stream.Close();
        }

        public PrgState Deserialize(int programID)
        {
            string uriPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase) + "\\outputs\\" + programID.ToString() + ".txt";
            string filePath = new Uri(uriPath).LocalPath;
            IFormatter formatter = new BinaryFormatter();
            Stream stream = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read);
            PrgState obj = (PrgState)formatter.Deserialize(stream);
            stream.Close();
            return obj;
        }

        public void SaveTofile(int programID)
        {
            PrgState r = this.getPrgStateById(programID);
            SaveTofile(r);
        }

        public void SaveTofile(PrgState r, bool append = false)
        {
            string uriPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase) + "\\outputs\\" + "prgState" + ".txt";
            string filePath = new Uri(uriPath).LocalPath;
            string result = r.ToString();
            using (StreamWriter file = new StreamWriter(filePath, append))//append new line at the end of file
            {
                if(append)
                {
                    result += "\n--------------------------------------------------";
                }
                file.WriteLine(result);
                file.Flush();
            }
        }
        #endregion

        public int  GetProgramsNumber()
        {
            return prgstateList.Count;
        }

        public  bool checkProgramStackIsEmpty(int i)
        {
            if (getPrgStateById(i).getStack().Count == 0)
            {
                prgstateList.RemoveAt(i);
                return false;
            }
            return true;
        }
    }
}
