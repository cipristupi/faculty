﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laboratory2.Utils
{
    class Dictionary<TKey, TValue> : IDictionary<TKey, TValue>
    {
        private List<Node> elem;
        public Dictionary()
        {
            elem = new List<Node>();
        }
        public void add(TKey k, TValue v)
        {
            //ToDo check if key exists
            Node n = new Node(k, v);
            if (this.checkKey(k) == false)
            {
                elem.add(n);
            }
            else
            {
                this.updateValue(k, v);
            }
        }
        private Boolean checkKey(TKey k)
        {
            List<TKey> l = this.getKeys();
            for (int i = 0; i < l.getSize(); i++)
            {
                if (l.getAt(i).Equals(k))
                {
                    return true;
                }
            }
            return false;
        }
        public TValue searchKey(TKey k)
        {
            for (int i = 0; i < elem.getSize(); i++)
            {
                if (elem.getAt(i).key.Equals(k))
                {
                    return (TValue)elem.getAt(i).value;
                }
            }
            return default(TValue);
        }

        public List<TKey> getKeys()
        {
            List<TKey> l = new List<TKey>();
            for (int i = 0; i < elem.getSize(); i++)
            {
                l.add((TKey)elem.getAt(i).key);
            }
            return l;
        }

        public void updateValue(TKey k, TValue v)
        {
            for (int i = 0; i < elem.getSize(); i++)
            {
                if (elem.getAt(i).key.Equals(k))//Equals ???
                {
                    elem.getAt(i).value = v;
                }
            }
        }

        public override string ToString()
        {
            string result = "";
            for (int i = 0; i < elem.getSize();i++)
            {
                result += elem.getAt(i).key.ToString() + ":" + elem.getAt(i).value.ToString() + "\n";
            }
            return result;
        }
    }
}
