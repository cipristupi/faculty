﻿using Laboratory2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laboratory2.Utils.Observer
{
    class StackObserver :IMyObserver
    {
        public void Update(Repository.PrgState prgState)
        {
            string o = "//Stack content \n";
            foreach (Statement s in prgState.getStack())
            {
                o += s.ToString() + "\n";
            }
            Console.WriteLine(o);
        }
    }
}
