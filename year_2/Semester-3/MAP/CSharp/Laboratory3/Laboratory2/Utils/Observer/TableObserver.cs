﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laboratory2.Utils.Observer
{
    class TableObserver :IMyObserver
    {
        public void Update(Repository.PrgState prgState)
        {
            string o = "//SymTable" + "\n";
            foreach (KeyValuePair<string, int> pair in prgState.getTable())
            {
                o += String.Format("{0} -> {1} \n", pair.Key, pair.Value);
            }
            Console.WriteLine(o);
        }
    }
}
