﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laboratory2.Models
{
    [Serializable]
    public class AssignStmt : Statement
    {
        private String varName;
        private Expression expr;
        public AssignStmt(String varname, Expression e)
        {
            varName = varname;
            expr = e;
        }

        public String getName()
        {
            return varName;
        }

        public Expression getExpr()
        {
            return expr;
        }

        public override string ToString()
        {
            return varName + "=" + expr.ToString();
        }
    }
}
