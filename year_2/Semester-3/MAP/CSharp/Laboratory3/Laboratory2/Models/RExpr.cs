﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laboratory2.Models
{
    [Serializable]
    class RExpr : Expression
    {
        private string varName;

        public RExpr(string var)
        {
           varName = var;
        }
        public int eval(Dictionary<string, int> tableOfSysmbols, int?[] heap)
        {
            int address = tableOfSysmbols[varName];
            int valueHeap = (int)heap[address];
            return valueHeap;
        }
        public override string ToString()
        {
            return "r(" + varName + ")";
        }
    }
}
