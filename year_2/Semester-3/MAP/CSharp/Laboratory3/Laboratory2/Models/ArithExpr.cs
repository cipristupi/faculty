﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laboratory2.Models
{
    [Serializable]
    public class ArithExpr : Expression
    {
        private Expression expr1, expr2;
        private char sign;
        public ArithExpr(Expression ex1, Expression ex2, char t)
        {
            expr1 = ex1;
            expr2 = ex2;
            sign = t;
        }

        public int eval(Dictionary<string, int> s, int?[] h)
        {
            int resultEx1;
            int resultEx2;

            resultEx1 = GetValueExpression(expr1, s,h);
            resultEx2 = GetValueExpression(expr2, s,h);
            switch (this.sign)
            {
                case '+':
                    return resultEx1 + resultEx2;
                case '-':
                    return resultEx1 - resultEx2;
                case '*':
                    return resultEx1 * resultEx2;
                case '/':
                    return resultEx1 / resultEx2;
            }
            return 0;
        }

        private int GetValueExpression(Expression e, Dictionary<string, int> s, int?[] h)
        {
            if (e is ConstExpr)
            {
                return ((ConstExpr)e).getConstant();
            }
            else
                if (e is VarExpr)
                {
                    string id = ((VarExpr)e).getName();
                    //return  s.searchKey(id);
                    return s[id];
                }
                else
                    if (e is ArithExpr)
                    {
                        return ((ArithExpr)e).eval(s,h);
                    }
                    else
                        if(e is RExpr)
                        {
                            return ((RExpr)e).eval(s, h);
                        }
                        //else
                        //    if(e is NewExpr)
                        //    {
                        //        return ((NewExpr)e);
                        //    }

            return 0;
        }

        public override string ToString()
        {
            String s = "(";
            s += expr1.ToString();
            if (sign == '+')
                s += "+";
            if (sign == '-')
                s += "-";
            if (sign == '*')
                s += "*";
            if (sign == '/')
                s += "/";
            s += expr2.ToString();
            s += ")";
            return s;
        }
    }
}
