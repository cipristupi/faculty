﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laboratory2.Models
{
    [Serializable]
    public class CompoundStmt : Statement
    {
        private Statement stmt1;
        private Statement stmt2;
        public CompoundStmt(Statement s1, Statement s2)
        {
            stmt1 = s1;
            stmt2 = s2;
        }

        public Statement getStmt1()
        {
            return stmt1;
        }
        public Statement getStmt2()
        {
            return stmt2;
        }
        public override string ToString()
        {
            String s = "CompoundStatement( ";
            s += stmt1.ToString();
            s += ";";
            s += stmt2.ToString();
            s += ")";
            return s;
        }
    }
}
