﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laboratory2.Models
{
    [Serializable]
    public class ConstExpr :Expression
    {
        private int val;
        public ConstExpr(int v)
        {
            val = v;
        }

        public int getConstant()
        {
            return val;
        }

        public override string ToString()
        {
            return val.ToString();
        }
    }
}
