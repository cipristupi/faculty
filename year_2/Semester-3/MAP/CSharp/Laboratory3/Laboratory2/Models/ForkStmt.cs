﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laboratory2.Models
{
    class ForkStmt : Statement
    {
        private Statement toFork;
        public ForkStmt(Statement s)
        {
            toFork = s;
        }
        public Statement getFork()
        {
            return toFork;
        }

        public override string ToString()
        {
            return "fork(" + toFork.ToString() + ")";
        }
    }
}
