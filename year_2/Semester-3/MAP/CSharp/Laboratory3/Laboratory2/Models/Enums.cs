﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laboratory2.Models
{
    public enum Statements
    {
        Compound,
        Fork,
        Print,
        If
    };

    public enum Expressions
    {
        Arithmetic,
        Assignation,
        New,
        R,
        Variable,
        Constant
    };

    public enum EvalType
    {
        StepByStep,
        Complete,
        Debug
    };
}
