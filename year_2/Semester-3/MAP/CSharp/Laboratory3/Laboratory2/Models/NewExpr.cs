﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laboratory2.Models
{
    [Serializable]
    class NewExpr : Expression
    {
        private int val;
        public NewExpr(int v)
        {
            this.val = v;
        }

        public int Value
        {
            get {return val;}
        }
        //public int New(int value, ref int?[] heap)
        //{
        //    int pos=0;
        //    for (int i = 0; i < 50; i++)
        //    {
        //        if (heap[i] == null)
        //        {
        //            heap[i] = value;
        //            pos = i;
        //            break;
        //        }
        //    }
        //    return pos;
        //}

        public override string ToString()
        {
            return "New(" + val + ")";
        }
    }

}
