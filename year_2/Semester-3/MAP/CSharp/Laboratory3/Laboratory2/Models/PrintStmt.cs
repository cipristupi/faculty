﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laboratory2.Models
{
    [Serializable]
    public class PrintStmt :Statement
    {
        private Expression expr;
        public PrintStmt(Expression e)
        {
            expr = e;
        }

        public Expression getExpr()
        {
            return expr;
        }

        public string print()
        {
            return expr.ToString();
        }

        public override string ToString()
        {
            return "Print(" + expr.ToString() + ") ";
        }
    }
}
