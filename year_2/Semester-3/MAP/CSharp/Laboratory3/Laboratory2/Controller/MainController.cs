﻿using Laboratory2.Models;
using Laboratory2.Repository;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using Laboratory2.Utils.Observer;

namespace Laboratory2.Controller
{
    class MainController
    {
        private MainRepository repo;
        private PrgState currentPrg;
        private Subject subject;
        private IMyObserver stackObserver;
        private IMyObserver outObserver;
        private IMyObserver tableObserver;
        public MainController(MainRepository r)
        {
            repo = r;
            subject = new Subject();
            stackObserver = new StackObserver();
            outObserver = new OutObserver();
            tableObserver = new TableObserver();
            subject.Attach(stackObserver);
            subject.Attach(outObserver);
            subject.Attach(tableObserver);
        }


        public void parallelRun(int programID)
        {
            int programNumber;
            repo.NewPrgState(programID);
            while (true)
            {
                programNumber = repo.GetProgramsNumber();
                if (programNumber == 0)
                {
                    return;
                }

                for (int i = 0; i < programNumber; i++)
                {
                    if (repo.checkProgramStackIsEmpty(i))
                    {
                        this.runStepByStep(i);
                    }
                    else
                    {
                        programNumber--;
                    }
                }
            }
        }

        public PrgState run(int programID)
        {
            if (!repo.programExists(programID))
            {
                return null;
            }
            repo.NewPrgState(programID);
            currentPrg = repo.getPrgStateById(programID);
            while (currentPrg.getStack().Count != 0)
            {
                runStepByStep(programID);
            }
            return currentPrg;

        }
        public PrgState runStepByStep(int programID)
        {
            if (!repo.programExists(programID))
            {
                return null;
            }
            InitPrgState(programID);
            Statement st = currentPrg.getStack().Pop();
            subject.Notify(currentPrg);//here the stack is modified
            if (st is CompoundStmt)
            {
                execCompound((CompoundStmt)st);
            }
            else
                if (st is IfStmt)
                {
                    execIf((IfStmt)st);
                }
                else
                    if (st is PrintStmt)
                    {
                        execPrint((PrintStmt)st);
                    }
                    else
                        if (st is AssignStmt)
                        {
                            execAssign((AssignStmt)st);
                        }
                        else
                            if (st is ForkStmt)
                            {
                                execFork((ForkStmt)st);
                            }
            return currentPrg;
        }

        private void execFork(ForkStmt st)
        {
            Statement x = st.getFork();
            repo.addFork(x, currentPrg);
        }


        private void execAssign(AssignStmt st)
        {
            Expression expr = st.getExpr();
            String varName = st.getName();
            if (expr is ConstExpr)
            {
                int value = ((ConstExpr)expr).getConstant();
                currentPrg.getTable().Add(varName, value);
            }
            else if (expr is VarExpr)
            {
                String id = ((VarExpr)expr).getName();
                int value = currentPrg.getTable()[id];
                currentPrg.getTable().Add(varName, value);
            }
            else if (expr is ArithExpr)
            {
                int value = ((ArithExpr)expr).eval(currentPrg.getTable(), currentPrg.getHeap());
                currentPrg.getTable().Add(varName, value);
            }
            else if (expr is NewExpr)
            {
                int value = ((NewExpr)expr).Value;
                int address = currentPrg.addToHeap(value);
                currentPrg.getTable().Add(varName, address);
            }
            else if (expr is RExpr)
            {
                int value = ((RExpr)expr).eval(currentPrg.getTable(), currentPrg.getHeap());
                currentPrg.getTable().Add(varName, value);
            }
            subject.Notify(currentPrg);//observer for table
        }

        private void execPrint(PrintStmt st)
        {
            Expression toPrint = st.getExpr();
            try
            {
                int result = 0;
                if (toPrint is ConstExpr)
                {
                    result = ((ConstExpr)toPrint).getConstant();
                }
                else if (toPrint is VarExpr)
                {
                    String id = ((VarExpr)toPrint).getName();
                    result = currentPrg.getTable()[id];
                }
                else if (toPrint is ArithExpr)
                {
                    result = ((ArithExpr)toPrint).eval(currentPrg.getTable(), currentPrg.getHeap());
                }
                else if (toPrint is NewExpr)
                {
                    int value = ((NewExpr)toPrint).Value;
                    result = currentPrg.addToHeap(value);
                }
                else if (toPrint is RExpr)
                {
                    result = ((RExpr)toPrint).eval(currentPrg.getTable(), currentPrg.getHeap());
                }
                String x = result.ToString();
                currentPrg.getOut().Add(x);
            }
            catch (Exception ex)
            {
                currentPrg.getOut().Add(ex.ToString());
            }
            subject.Notify(currentPrg);//observer for outbuff
        }

        private void execIf(IfStmt st)
        {
            Expression expr = st.getExpr();
            int value;
            if (expr is ConstExpr)
            {
                value = ((ConstExpr)expr).getConstant();
            }
            else
                if (expr is VarExpr)
                {
                    String id = ((VarExpr)expr).getName();
                    value = currentPrg.getTable()[id];
                }
                else
                    if (expr is ArithExpr)
                    {
                        value = ((ArithExpr)expr).eval(currentPrg.getTable(), currentPrg.getHeap());
                    }
                    else if (expr is NewExpr)
                    {
                        value = currentPrg.addToHeap(((NewExpr)expr).Value);
                    }
                    else if (expr is RExpr)
                    {
                        value = ((RExpr)expr).eval(currentPrg.getTable(), currentPrg.getHeap());
                    }
                    else
                    {
                        value = 0;
                    }

            if (value != 0)
            {
                currentPrg.getStack().Push(st.getSt1());
            }
            else
            {
                currentPrg.getStack().Push(st.getSt2());
            }
            subject.Notify(currentPrg);//observer for stack
        }

        private void execCompound(CompoundStmt st)
        {
            currentPrg.getStack().Push(st.getStmt2());
            currentPrg.getStack().Push(st.getStmt1());
            subject.Notify(currentPrg);//observer for table
        }

        public void AddStatement(Statement s)
        {
            repo.AddStatement(s);
        }

        public PrgState GetPrgState(int i)
        {
            return repo.getPrgStateById(i);
        }
        public PrgState GetCurrentPrgState()
        {
            return currentPrg;
        }

        public Stack<Statement> GetCurrentStack(int programID)
        {
            InitPrgState(programID);
            return currentPrg.getStack();
        }


        public void InitPrgState(int programID)
        {
            currentPrg = repo.getPrgStateById(programID);
        }

        public List<Statement> AllPrograms()
        {
            return repo.getAllPrograms();
        }
    }
}
