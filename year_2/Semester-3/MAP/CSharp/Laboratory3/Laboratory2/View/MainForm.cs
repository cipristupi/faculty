﻿using Laboratory2.Controller;
using Laboratory2.Models;
using Laboratory2.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Laboratory2.View
{
    public partial class MainForm : Form
    {
        MainController ctr;
        public MainForm()
        {
            InitializeComponent();
            MainRepository repo = new MainRepository();
            ctr = new MainController(repo);
            CreateGroupBox();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            evalType.Items.Add(EvalType.Complete);
            evalType.Items.Add(EvalType.StepByStep);
            evalType.Items.Add(EvalType.Debug);
        }

        private void CreateGroupBox()
        {
            Label l1 = new Label();
            l1.AutoSize = true;
            l1.BackColor = Color.Transparent;
            l1.Text = "Please select another statement";
            l1.Location = new Point(groupBox2.Location.X , groupBox2.Location.Y + 15);
            this.Controls.Add(l1);

            #region Assign Statement
            GroupBox assignGb = new GroupBox();
            assignGb.Width = 456;
            assignGb.Height = 60;
            assignGb.Location = new Point(groupBox2.Location.X, groupBox2.Bottom + 25);
            assignGb.Text = "Assign statement";

            Label asLb1 = new Label();
            asLb1.Text = "Enter a string";
            asLb1.AutoSize = true;
            asLb1.BackColor = Color.Transparent;
            asLb1.Location = new Point(15, 30);

            TextBox asTxtBox = new TextBox();
            asTxtBox.Size = new Size(150, 50);
            asTxtBox.Location = new Point(asLb1.Right + 5, asLb1.Location.Y-5);


            Button asOkBttn = new Button();
            asOkBttn.Size = new Size(100, 25);
            asOkBttn.Text = "Ok";
            asOkBttn.Location = new Point(asTxtBox.Right + 10, asLb1.Location.Y-5);

            assignGb.Controls.Add(asLb1);
            assignGb.Controls.Add(asTxtBox);
            assignGb.Controls.Add(asOkBttn);

            assignGb.Visible = false;
            this.splitContainer1.Panel1.Controls.Add(assignGb);

            #endregion

            #region Constant Statement
            GroupBox constGb = new GroupBox();
            constGb.Width = 456;
            constGb.Height = 60;
            constGb.Location = new Point(groupBox2.Location.X, groupBox2.Bottom + 25);
            constGb.Text = "Constant statement";

            Label consLb = new Label();
            consLb.Text = "Enter a number";
            consLb.AutoSize = true;
            consLb.BackColor = Color.Transparent;
            consLb.Location = new Point(15, 30);

            TextBox constTxtBox = new TextBox();
            constTxtBox.Size = new Size(150, 50);
            constTxtBox.Location = new Point(consLb.Right + 5, consLb.Location.Y - 5);


            Button constOkBttn = new Button();
            constOkBttn.Size = new Size(100, 25);
            constOkBttn.Text = "Ok";
            constOkBttn.Location = new Point(constTxtBox.Right + 10, consLb.Location.Y - 5);

            constGb.Controls.Add(consLb);
            constGb.Controls.Add(constTxtBox);
            constGb.Controls.Add(constOkBttn);

            this.splitContainer1.Panel1.Controls.Add(constGb);
            #endregion
        }

        private void add_stmt_Click(object sender, EventArgs e)
        {
           
        }


        private void button1_Click(object sender, EventArgs e)
        {
            //assign button
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //print button
            groupBox2.Enabled = true;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //compound
        }

        private void button5_Click(object sender, EventArgs e)
        {
            //fork
        }

        private void button4_Click(object sender, EventArgs e)
        {
            //if
        }




    }
}
