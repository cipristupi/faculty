﻿using Laboratory2.Controller;
using Laboratory2.Models;
using Laboratory2.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laboratory2.View
{
    class MainConsole
    {
        private MainController ctr;
        public MainConsole(MainController c)
        {
            ctr = c;
        }
        public void EntryPoint()
        {
            ReadMenu();
        }

        public void ReadMenu()
        {
            MainMenuPrint();
            Console.Write("Command: ");
            int cmd = int.Parse(Console.ReadLine());
            switch (cmd)
            {
                case 0:
                    break;
                case 1:
                    {
                        //read statement and execute
                        Statement s = readStatement("");
                        ctr.AddStatement(s);
                        this.ReadMenu();
                        break;
                    }
                case 2:
                    {
                        //step by step
                        Console.WriteLine("Program Id:");
                        int programId = int.Parse(Console.ReadLine());
                        OneStepEvaluation(false,programId);
                        break;
                    }
                case 3:
                    {
                        //complete evaluation
                        Console.WriteLine("Program Id:");
                        int programId = int.Parse(Console.ReadLine());
                        ctr.parallelRun(programId);
                        //PrintResult(ctr.run(programId).getOut());
                        break;
                    }
                case 4:
                    {
                        //debug
                        Console.WriteLine("Program Id:");
                        int programId = int.Parse(Console.ReadLine());
                        OneStepEvaluation(true,programId);
                        break;
                    }
                case 5:
                    {
                        //print list of available programs
                        PrintAvailablePrograms();
                        break;
                    }
            }
            Console.WriteLine("*************END OF EXECUTION*************");
            Console.ReadLine();
        }


        public void OneStepEvaluation(bool debug,int programId)
        {
            PrgState prg = ctr.GetPrgState(programId);
            int cmd = 1;
            int c = 1;
            while (ctr.GetCurrentStack(0).Count !=0 && cmd != 0)
            {
                Console.Write("Press 1 for continue: ");
                cmd = int.Parse(Console.ReadLine());
                Console.WriteLine("Step " + c.ToString());
                if (debug)
                {
                    Console.WriteLine("***DEBUG***\n{0}***END DEBUG***\n" ,ctr.runStepByStep(0).ToString());
                }
                else
                {
                    PrintResult(ctr.runStepByStep(0).getOut());
                }
                c++;
            }
        }

        public void PrintResult(List<String> outBuff)
        {
            Console.WriteLine("Results");
            for (int i = 0; i < outBuff.Count; i++)
            {
                Console.WriteLine(outBuff[i].ToString()); 
            }           
        }

        public void MainMenuPrint()
        {
            Console.WriteLine("Menu: ");
            Console.WriteLine("0 - to exit");
            Console.WriteLine("1 - to enter a program");
            Console.WriteLine("2 - for one step evaluation");
            Console.WriteLine("3 - for complete evaluation");
            Console.WriteLine("4 - for debuging");
            Console.WriteLine("5 - list of available programs");
            Console.WriteLine("---------------------------------");
        }

        public void PrintAvailablePrograms()
        {
            List<Statement> s = ctr.AllPrograms();
            for (int i = 0; i < s.Count; i++)
            {
                Console.WriteLine(i.ToString() + "  " + s[i].ToString());
            }
            ReadMenu();
        }

        #region Expression Reading

        public void printReadExpressionMenu(string msg)
        {
            if (!string.IsNullOrEmpty(msg))
            {
                Console.WriteLine("---------------------------------");
                Console.WriteLine(msg);
                Console.WriteLine();
                Console.WriteLine("---------------------------------");
            }
            Console.WriteLine("Choose expression type: ");
            Console.WriteLine("0 - arithmetic expression");
            Console.WriteLine("1 - constant expression");
            Console.WriteLine("2 - var expression");
            Console.WriteLine("3 - r expression");
            Console.WriteLine("4 - new expression");
            Console.WriteLine("---------------------------------");
        }



        public void printSignMenu()
        {
            Console.WriteLine("Choose sign (+,-,*,/): ");
        }

        public char readSign()
        {
            Console.Write("Sign: ");
            char toReturn = char.Parse(Console.ReadLine());
            return toReturn;
        }

        public String readVar()
        {
            Console.Write("Enter var name: ");
            string toReturn = Console.ReadLine();
            return toReturn;
        }

        public Expression readExpression(string msg)
        {
            printReadExpressionMenu(msg);
            int expr = readStatementMenu();
            switch (expr)
            {
                case 0:
                    //arithmetic expression
                    Expression ex1 = readExpression("Expression one");
                    Expression ex2 = readExpression("Expression two");
                    printSignMenu();
                    char sign = readSign();
                    return new ArithExpr(ex1, ex2, sign);
                case 1:
                    //constant expression
                    Console.Write("Constant value: ");
                    int constantX = int.Parse(Console.ReadLine());
                    return new ConstExpr(constantX);
                case 2:
                    //var expression
                    String varX = readVar();
                    return new VarExpr(varX);
                case 3:
                    //R expression
                    Console.WriteLine("Variable name: ");
                    string var = Console.ReadLine();
                    return new RExpr(var);
                case 4:
                    //new expression
                    Console.WriteLine("New: ");
                    int val = int.Parse(Console.ReadLine());
                    return new NewExpr(val);
            }

            return null;
        }

        #endregion

        #region Statement reading

        public void printReadStatementMenu(string msg)
        {
            if (!string.IsNullOrEmpty(msg))
            {
                Console.WriteLine("---------------------------------");
                Console.WriteLine(msg);
                Console.WriteLine("");
                Console.WriteLine("---------------------------------");
            }
            Console.WriteLine("Choose statement type: ");
            Console.WriteLine("0 - compound statement");
            Console.WriteLine("1 - assignment statement");
            Console.WriteLine("2 - print statement");
            Console.WriteLine("3 - if statement");
            Console.WriteLine("---------------------------------");
        }
        public int readStatementMenu()
        {
            Console.WriteLine();
            Console.Write("Command: ");
            int toReturn = int.Parse(Console.ReadLine());
            return toReturn;
        }

        public Statement readStatement(string msg)
        {
            printReadStatementMenu(msg);
            int stmt = readStatementMenu();
            switch (stmt)
            {
                case 0:
                    //compound statement
                    Statement st1 = readStatement("Statement one");
                    Statement st2 = readStatement("Statement two");
                    return new CompoundStmt(st1, st2);
                case 1:
                    //assignment statement
                    String varX = readVar();
                    Expression ex = readExpression("");
                    return new AssignStmt(varX, ex);
                case 2:
                    //print statement
                    Expression ex2 = readExpression("");
                    return new PrintStmt(ex2);
                case 3:
                    //if statement
                    Expression ex3 = readExpression("");
                    Statement stx1 = readStatement("Statement one");
                    Statement stx2 = readStatement("Statement two");
                    return new IfStmt(ex3, stx1, stx2);
                case 4:
                    //fork statement
                    Statement s = readStatement("Statement");
                    return new ForkStmt(s);
            }
            return null;
        }
        #endregion
    }
}
