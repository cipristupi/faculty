﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalVersion.Models.Statements
{
    [Serializable]
    public class ForkStmt : Statement
    {
        private Statement toFork;
        public ForkStmt(Statement s)
        {
            toFork = s;
        }
        public Statement ToFork { get { return toFork; } }
        public override string ToString()
        {
            return "fork(" + toFork.ToString() + ")";
        }
    }
}
