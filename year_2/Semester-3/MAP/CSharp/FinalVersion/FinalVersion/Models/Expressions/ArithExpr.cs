﻿using FinalVersion.Utils.Heap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalVersion.Models.Expressions
{
    [Serializable]
    public class ArithExpr :Expression
    {
        private Expression expr1, expr2;
        private char sign;
        public ArithExpr(Expression ex1, Expression ex2, char s)
        {
            expr1 = ex1;
            expr2 = ex2;
            sign = s;
        }

        public int Eval(Dictionary<string,int> symTable,Heap h)
        {
            int resultEx1;
            int resultEx2;

            resultEx1 = GetValueExpression(expr1, symTable, h);
            resultEx2 = GetValueExpression(expr2, symTable, h);
            switch (this.sign)
            {
                case '+':
                    return resultEx1 + resultEx2;
                case '-':
                    return resultEx1 - resultEx2;
                case '*':
                    return resultEx1 * resultEx2;
                case '/':
                    return resultEx1 / resultEx2;
            }
            return 0;
        }

        private int GetValueExpression(Expression e, Dictionary<string, int> symTable ,Heap h)
        {
            if (e is ConstExpr)
            {
                return ((ConstExpr)e).Constant;
            }
            else
                if (e is VarExpr)
                {
                    string key = ((VarExpr)e).Name;
                    //return  s.searchKey(id);
                    return symTable[key];
                }
                else
                    if (e is ArithExpr)
                    {
                        return ((ArithExpr)e).Eval(symTable, h);
                    }
                    else
                        if (e is RExpr)
                        {
                            return ((RExpr)e).Eval(symTable, h);
                        }
                        else
                            if (e is NewExpr)
                            {
                                return ((NewExpr)e).Eval(h);//this can generate heap overflow
                            }
                            else
                                if (e is LogicalExpr)
                                    return ((LogicalExpr)e).Eval(symTable, h);
            return 0;
        }

        public override string ToString()
        {
            String s = "(";
            s += expr1.ToString();
            if (sign == '+')
                s += "+";
            if (sign == '-')
                s += "-";
            if (sign == '*')
                s += "*";
            if (sign == '/')
                s += "/";
            s += expr2.ToString();
            s += ")";
            return s;
        }
    }
}
