﻿using FinalVersion.Utils.Heap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalVersion.Models.Expressions
{
    [Serializable]
    public class RExpr : Expression
    {
        private string varName;
        public RExpr(string varN)
        {
            varName = varN;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="symTable"> Table of symbols</param>
        /// <param name="h"> The heap</param>
        /// <returns>If the address is valid we return the value from the address, if address is not valid we return a dump number</returns>
        public int Eval(Dictionary<string,int> symTable, Heap h)
        {
            int address = symTable[varName];
            if(h.GetAt(address) == null)
            {
                return new Random().Next();
            }
            return (int)h.GetAt(address);
        }

        public override string ToString()
        {
            return "R(" + varName + ")";
        }
    }
}
