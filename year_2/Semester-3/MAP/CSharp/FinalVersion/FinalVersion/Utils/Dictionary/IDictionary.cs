﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laboratory2.Utils
{
    public interface IDictionary<TKey,TValue>
    {
        void add(TKey k, TValue v);
        TValue searchKey(TKey k);
        List<TKey> getKeys();
        void updateValue(TKey k, TValue v);
    }
}
