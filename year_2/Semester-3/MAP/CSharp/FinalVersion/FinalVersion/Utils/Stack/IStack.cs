﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laboratory2.Utils
{
    public interface IStack<TEntity>
    {
        void push(TEntity el);
        TEntity pop();
        bool isEmpty();
        TEntity puk();
    }
}
