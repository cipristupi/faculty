﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalVersion.Utils.Heap
{
    public class Heap : IHeap
    {
        private int?[] heap;
        public Heap()
        {
            heap = new int?[50];
        }
        public int New(int value)
        {
            int pos = 0;
            for (int i = 0; i < 50; i++)
            {
                if (heap[i] == null)
                {
                    heap[i] = value;
                    pos = i;
                    break;
                }
            }
            return pos;
        }

        public int Count { get { return heap.Length; } }

        public int? GetAt(int index)
        {
            return heap[index];
        }
    }
}
