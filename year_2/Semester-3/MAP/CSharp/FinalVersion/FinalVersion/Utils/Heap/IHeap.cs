﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalVersion.Utils.Heap
{
    interface IHeap
    {
        int New(int value);
        int? GetAt(int index);
    }
}
