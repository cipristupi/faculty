﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laboratory2.Utils
{
    class Node
    {
        public Object key;
        public Object value;
        public Node(Object k, Object v)
        {
            key = k;
            value = v;
        }
    }
}
