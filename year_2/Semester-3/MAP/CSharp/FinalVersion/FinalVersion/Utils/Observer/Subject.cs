﻿using FinalVersion.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laboratory2.Utils.Observer
{
    class Subject 
    {
        private System.Collections.Generic.List<IMyObserver> _observers;
        public Subject()
        {
            _observers = new System.Collections.Generic.List<IMyObserver>();
        }

        public void Attach(IMyObserver observer)
        {
            _observers.Add(observer);
        }
        public void Detach(IMyObserver observer)
        {
            if (_observers.Contains(observer))
                _observers.Remove(observer);
        }

        public void Notify(PrgState prgState)
        {
            foreach (IMyObserver o in _observers)
            {
                o.Update(prgState);
            }
        }
    }
}
