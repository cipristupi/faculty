﻿using FinalVersion.Models.Statements;
using FinalVersion.Utils.Heap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalVersion.Repository
{
    [Serializable]
    public class PrgState
    {
        #region Global variables
        private Dictionary<string, int> symTable;
        private Stack<Statement> exeStack;
        private List<string> outBuff;
        private Heap heap;
        private Statement initialStmt;
        #endregion

        #region  Constructors
        public PrgState(Statement initStmt)
        {
            initialStmt = initStmt;//save a copy of initial statement for display
            symTable = new Dictionary<string, int>();
            exeStack = new Stack<Statement>();
            outBuff = new List<string>();
            heap = new Heap();
            exeStack.Push(initStmt);
        }

        public PrgState(Dictionary<string, int> st, Stack<Statement> s, List<string> o, Heap h)
        {
            symTable = st;
            exeStack = s;
            outBuff = o;
            heap = h;
        }

        #endregion

        #region Setters and Getters
        public Dictionary<string, int> SymTable
        { 
            get { return symTable; }
            set { symTable = value; }
        }
        public List<string> OutBuff 
        {
            get { return outBuff; }
            set { outBuff = value; }
        }

        public Stack<Statement> ExeStack
        {
            get { return exeStack; }
            set { exeStack = value; }
        }

        public Heap Heap
        {
            get { return heap; }
            set { heap = value; }
        }

        public Statement InitialStmt { get { return initialStmt; } }

        public int Id { get; set; }
        #endregion

        #region Utils
        public void CopySymTable(Dictionary<string,int> sourceSymTable)
        {
            foreach (var pair in sourceSymTable)
            {
                symTable.Add(pair.Key, pair.Value);
            }
        }

        public override string ToString()
        {
            string o = "";
            o += "\n //Stack content \n";
            foreach (Statement s in exeStack)
            {
                o += s.ToString() + "\n";
            }
            o += "\n //SymTable" + "\n";
            foreach (KeyValuePair<string, int> pair in symTable)
            {
                o += String.Format("{0} -> {1} \n", pair.Key, pair.Value);
            }
            o += "\n //Out content \n";
            foreach (string s in outBuff)
            {
                o += s + "\n";
            }
            o += "\n";

            //TODO print heap here
            return o;

        }



        #endregion
    }
}
