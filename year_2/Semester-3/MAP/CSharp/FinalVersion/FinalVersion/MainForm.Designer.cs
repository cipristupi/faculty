﻿namespace FinalVersion
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.currentprg_label = new System.Windows.Forms.Label();
            this.out_rtb = new System.Windows.Forms.RichTextBox();
            this.symtable_rtb = new System.Windows.Forms.RichTextBox();
            this.exestack_rtb = new System.Windows.Forms.RichTextBox();
            this.heap_rtb = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.prgstate_cmb = new System.Windows.Forms.ComboBox();
            this.debug_chcbox = new System.Windows.Forms.CheckBox();
            this.complete_eval = new System.Windows.Forms.Button();
            this.stepbystep = new System.Windows.Forms.Button();
            this.next_step = new System.Windows.Forms.Button();
            this.add_program = new System.Windows.Forms.Button();
            this.print_file = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // currentprg_label
            // 
            this.currentprg_label.AutoSize = true;
            this.currentprg_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.currentprg_label.Location = new System.Drawing.Point(12, 118);
            this.currentprg_label.Name = "currentprg_label";
            this.currentprg_label.Size = new System.Drawing.Size(0, 17);
            this.currentprg_label.TabIndex = 8;
            // 
            // out_rtb
            // 
            this.out_rtb.Location = new System.Drawing.Point(15, 198);
            this.out_rtb.Name = "out_rtb";
            this.out_rtb.ReadOnly = true;
            this.out_rtb.Size = new System.Drawing.Size(203, 302);
            this.out_rtb.TabIndex = 0;
            this.out_rtb.Text = "";
            // 
            // symtable_rtb
            // 
            this.symtable_rtb.Location = new System.Drawing.Point(239, 198);
            this.symtable_rtb.Name = "symtable_rtb";
            this.symtable_rtb.ReadOnly = true;
            this.symtable_rtb.Size = new System.Drawing.Size(203, 302);
            this.symtable_rtb.TabIndex = 1;
            this.symtable_rtb.Text = "";
            // 
            // exestack_rtb
            // 
            this.exestack_rtb.Location = new System.Drawing.Point(463, 198);
            this.exestack_rtb.Name = "exestack_rtb";
            this.exestack_rtb.ReadOnly = true;
            this.exestack_rtb.Size = new System.Drawing.Size(203, 302);
            this.exestack_rtb.TabIndex = 2;
            this.exestack_rtb.Text = "";
            // 
            // heap_rtb
            // 
            this.heap_rtb.Location = new System.Drawing.Point(697, 198);
            this.heap_rtb.Name = "heap_rtb";
            this.heap_rtb.ReadOnly = true;
            this.heap_rtb.Size = new System.Drawing.Size(203, 302);
            this.heap_rtb.TabIndex = 3;
            this.heap_rtb.Text = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 166);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "Output";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(233, 166);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(106, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "Symbol Table";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(457, 166);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(123, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "Execution Stack";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(691, 166);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 17);
            this.label4.TabIndex = 7;
            this.label4.Text = "Heap";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 23);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(112, 17);
            this.label5.TabIndex = 9;
            this.label5.Text = "Statement list:";
            // 
            // prgstate_cmb
            // 
            this.prgstate_cmb.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.prgstate_cmb.FormattingEnabled = true;
            this.prgstate_cmb.Location = new System.Drawing.Point(139, 19);
            this.prgstate_cmb.Name = "prgstate_cmb";
            this.prgstate_cmb.Size = new System.Drawing.Size(370, 21);
            this.prgstate_cmb.TabIndex = 10;
            // 
            // debug_chcbox
            // 
            this.debug_chcbox.AutoSize = true;
            this.debug_chcbox.Location = new System.Drawing.Point(15, 59);
            this.debug_chcbox.Name = "debug_chcbox";
            this.debug_chcbox.Size = new System.Drawing.Size(58, 17);
            this.debug_chcbox.TabIndex = 11;
            this.debug_chcbox.Text = "Debug";
            this.debug_chcbox.UseVisualStyleBackColor = true;
            // 
            // complete_eval
            // 
            this.complete_eval.Location = new System.Drawing.Point(92, 53);
            this.complete_eval.Name = "complete_eval";
            this.complete_eval.Size = new System.Drawing.Size(126, 23);
            this.complete_eval.TabIndex = 12;
            this.complete_eval.Text = "Complete Evaluation";
            this.complete_eval.UseVisualStyleBackColor = true;
            this.complete_eval.Click += new System.EventHandler(this.complete_eval_Click);
            // 
            // stepbystep
            // 
            this.stepbystep.Location = new System.Drawing.Point(224, 53);
            this.stepbystep.Name = "stepbystep";
            this.stepbystep.Size = new System.Drawing.Size(132, 23);
            this.stepbystep.TabIndex = 13;
            this.stepbystep.Text = "Step By Step";
            this.stepbystep.UseVisualStyleBackColor = true;
            this.stepbystep.Click += new System.EventHandler(this.stepbystep_Click);
            // 
            // next_step
            // 
            this.next_step.Location = new System.Drawing.Point(377, 53);
            this.next_step.Name = "next_step";
            this.next_step.Size = new System.Drawing.Size(132, 23);
            this.next_step.TabIndex = 14;
            this.next_step.Text = "Next Step";
            this.next_step.UseVisualStyleBackColor = true;
            this.next_step.Visible = false;
            this.next_step.Click += new System.EventHandler(this.next_step_Click);
            // 
            // add_program
            // 
            this.add_program.Location = new System.Drawing.Point(550, 19);
            this.add_program.Name = "add_program";
            this.add_program.Size = new System.Drawing.Size(273, 49);
            this.add_program.TabIndex = 9;
            this.add_program.Text = "Add program";
            this.add_program.UseVisualStyleBackColor = true;
            this.add_program.Click += new System.EventHandler(this.add_program_Click);
            // 
            // print_file
            // 
            this.print_file.AutoSize = true;
            this.print_file.Location = new System.Drawing.Point(15, 82);
            this.print_file.Name = "print_file";
            this.print_file.Size = new System.Drawing.Size(86, 17);
            this.print_file.TabIndex = 15;
            this.print_file.Text = "Save To File";
            this.print_file.UseVisualStyleBackColor = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(920, 529);
            this.Controls.Add(this.print_file);
            this.Controls.Add(this.add_program);
            this.Controls.Add(this.next_step);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.stepbystep);
            this.Controls.Add(this.out_rtb);
            this.Controls.Add(this.complete_eval);
            this.Controls.Add(this.symtable_rtb);
            this.Controls.Add(this.debug_chcbox);
            this.Controls.Add(this.exestack_rtb);
            this.Controls.Add(this.prgstate_cmb);
            this.Controls.Add(this.heap_rtb);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.currentprg_label);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Name = "MainForm";
            this.Text = "Toy Language";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label currentprg_label;
        private System.Windows.Forms.RichTextBox out_rtb;
        private System.Windows.Forms.RichTextBox symtable_rtb;
        private System.Windows.Forms.RichTextBox exestack_rtb;
        private System.Windows.Forms.RichTextBox heap_rtb;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox prgstate_cmb;
        private System.Windows.Forms.CheckBox debug_chcbox;
        private System.Windows.Forms.Button complete_eval;
        private System.Windows.Forms.Button stepbystep;
        private System.Windows.Forms.Button next_step;
        private System.Windows.Forms.Button add_program;
        private System.Windows.Forms.CheckBox print_file;
    }
}

