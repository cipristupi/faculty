﻿using FinalVersion.Controller;
using FinalVersion.Models.Expressions;
using FinalVersion.Models.Statements;
using FinalVersion.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;
//using Microsoft.VisualBasic.Interaction;

namespace FinalVersion
{
    public partial class MainForm : Form
    {
        private MainController ctr;
        public MainForm()
        {
            InitializeComponent();

            MainRepository repo = new MainRepository();
            AddSomeInitialStatements(repo);
            ctr = new MainController(repo);
            LoadPrgStates(ctr.GetAllStatement());
        }

        private int step = 0;
        private int i ;

        private void next_step_Click(object sender, EventArgs e)
        {
            int programNumber = ctr.GetPrgStateNumber();
            bool debug  = debug_chcbox.Checked;
            PrgState currentPrgState = null;
            if (programNumber == 0)
            {
                MessageBox.Show("Complete evaluation.");
                i = 0;
                step = 0;
                return;
            }
            Debug.WriteLine("i={0},step={1},programNo={2}", i, step, programNumber);
            if (i == programNumber)
                i = 0;
            if(i<programNumber)
            {
                //currentprg_label.Text = "Current thread:" + i.ToString() + "\n" + ctr.GetPrgStateByID(i).InitialStmt.ToString();
                //currentprg_label.Text = "Current thread:" + ctr.GetPrgStateByID(i).InitialStmt.ToString();
                if (ctr.PrgStateStackIsEmpty(i))
                {
                    ctr.RemovePrgState(i);
                }
                else
                {
                    currentprg_label.Text = "Current thread:" + i.ToString() + "\n" + ctr.GetPrgStateByID(i).ExeStack.Peek().ToString();
                    if (debug)
                        DebugPrint(ctr.RunStepByStep(i),true,i);
                    else
                        currentPrgState = ctr.RunStepByStep(i);

                    if (i != programNumber)
                        i++;
                    step++;
                }
            }
            //if (!debug)
            //{
            //    PrintOutBuff(currentPrgState);
            //}
            //MessageBox.Show("Complete evalution.");
        
        }

        private void stepbystep_Click(object sender, EventArgs e)
        {
            i = 0;
            step = 0;
            ctr.NewPrgState(prgstate_cmb.SelectedIndex);
            ClearRtb();
            next_step.Visible = true;
            stepbystep.Enabled = false;
        }

        private void complete_eval_Click(object sender, EventArgs e)
        {
            step = 0;
            ClearRtb();
            RunPrgState(prgstate_cmb.SelectedIndex, debug_chcbox.Checked,true,print_file.Checked);
        }

        private void ClearRtb()
        {
            out_rtb.Clear();
            exestack_rtb.Clear();
            symtable_rtb.Clear();
            heap_rtb.Clear();
        }

        private void RunPrgState(int programID, bool debug = false, bool isStep=false,bool printFile=false)
        {
            int programNumber;
            ctr.NewPrgState(programID);
            PrgState currentPrgState = null;
            while (true)
            {
                programNumber = ctr.GetPrgStateNumber();
                if (programNumber == 0)
                {
                    break;
                }

                for (int i = 0; i < programNumber; i++)
                {
                    if (ctr.PrgStateStackIsEmpty(i))
                    {
                        programNumber--;
                        ctr.RemovePrgState(i);
                    }
                    else
                    {
                        if (debug)
                        {
                            currentPrgState = ctr.RunStepByStep(i,printFile,step);
                            DebugPrint(currentPrgState, isStep, currentPrgState.Id);
                        }
                        else
                            currentPrgState = ctr.RunStepByStep(i);
                        if (isStep)
                            step++;
                    }
                }
            }
            if (!debug)
                PrintOutBuff(currentPrgState);
            MessageBox.Show("Complete evalution.");
            stepbystep.Enabled = true;
        }


        private void PrintOutBuff(PrgState prgState)
        {
            string outBuff = "";
            foreach (string s in prgState.OutBuff)
            {
                outBuff += s + "\n";
            }
            out_rtb.AppendText(outBuff);
        }
        private void DebugPrint(PrgState prgState, bool isStep = false,int? thread=null)
        {
            string exeStack , outBuff ,symTable , heap ;
            string separator = "";
            if (thread == null)
            {
                exeStack = outBuff = symTable = heap = "";
            }
            else
            {
                exeStack = outBuff = symTable = heap = "[Thread "+((int)thread).ToString()+"]\n";
            }
            //Stack content;
            foreach (Statement s in prgState.ExeStack)
            {
                exeStack += s.ToString() + "\n";
            }
            //SymTable
            foreach (KeyValuePair<string, int> pair in prgState.SymTable)
            {
                symTable += String.Format("{0} -> {1} \n", pair.Key, pair.Value);
            }
            //Out content
            foreach (string s in prgState.OutBuff)
            {
                outBuff += s + "\n";
            }
            //Heap content
            for (int i = 0; i < prgState.Heap.Count; i++)
            {
                if (prgState.Heap.GetAt(i) == null)
                    break;
                heap += "[" + i.ToString() + "]=" + prgState.Heap.GetAt(i).ToString() + "\n";
            }


            if (isStep)
            {
                separator = string.Concat(Enumerable.Repeat("=", 10));
                separator += "STEP " + step.ToString();
                separator += string.Concat(Enumerable.Repeat("=", 10));
                separator += "\n";
            }
            else
            {
                separator = string.Concat(Enumerable.Repeat("=", 32));
                separator += "\n";
            }
            out_rtb.AppendText(separator);
            out_rtb.AppendText(outBuff);

            exestack_rtb.AppendText(separator);
            exestack_rtb.AppendText(exeStack);

            symtable_rtb.AppendText(separator);
            symtable_rtb.AppendText(symTable);

            heap_rtb.AppendText(separator);
            heap_rtb.AppendText(heap);
        }

        private void LoadPrgStates(List<Statement> prgStateList)
        {
            prgstate_cmb.Items.Clear();
            for (int i = 0; i < prgStateList.Count; i++)
            {
                prgstate_cmb.Items.Add(prgStateList[i].ToString());
            }
        }



        private void AddSomeInitialStatements(MainRepository repo)
        {
            Statement s = new IfStmt(new ConstExpr(1), new PrintStmt(new ConstExpr(0)), new PrintStmt(new ConstExpr(1)));
            Statement s1 = new CompoundStmt(new AssignStmt("v", new ConstExpr(2)), new PrintStmt(new ArithExpr(new VarExpr("v"), new ConstExpr(3), '+')));
            Statement s2 = new CompoundStmt(new PrintStmt(new ConstExpr(3)), new ForkStmt(new CompoundStmt(new AssignStmt("v", new ConstExpr(2)), new PrintStmt(new ArithExpr(new VarExpr("v"), new ConstExpr(3), '+')))));
            Statement s3 = new CompoundStmt(new AssignStmt("v", new NewExpr(2)), new ForkStmt(new PrintStmt(new ArithExpr(new RExpr("v"), new ConstExpr(3), '+'))));
            Statement s4 = new CompoundStmt(
                new AssignStmt("v", new NewExpr(2)),
                new ForkStmt(
                    new CompoundStmt(
                        new AssignStmt("V", new NewExpr(2)),
                        new PrintStmt(
                            new ArithExpr(
                                new RExpr("v"),
                                new ArithExpr(
                                    new RExpr("V"),
                                    new ConstExpr(100), '+'), '*')))));

            //n(n+1)/2  n(b-a)/c
            Statement s5 = new CompoundStmt(
                new AssignStmt("n", new NewExpr(200)),
                new CompoundStmt(
                    new AssignStmt("a", new NewExpr(200)),
                    new CompoundStmt(
                        new AssignStmt("b", new NewExpr(1000)),
                        new CompoundStmt(
                            new AssignStmt("c", new NewExpr(5)),
                            new ForkStmt(
                                new PrintStmt(
                                    new ArithExpr(new ArithExpr(new RExpr("n"), new ArithExpr(new RExpr("b"), new RExpr("a"), '-'), '*'), new RExpr("c"), '/')
                                    )
                                )
                            )
                        )
                    )
                );
            Statement s6 = new CompoundStmt(new AssignStmt("v", new ConstExpr(2)), new CompoundStmt(new IncStmt("v"), new PrintStmt(new VarExpr("v"))));
            Statement s7 = new CompoundStmt(new AssignStmt("v", new ConstExpr(10)), new CompoundStmt(new DecStmt("v"), new PrintStmt(new VarExpr("v"))));
            Statement s8 = new CompoundStmt(new AssignStmt("v", new ConstExpr(1)), new PrintStmt(new ArithExpr(new VarExpr("v"), new LogicalExpr(new ConstExpr(20), new ConstExpr(3), ">"), '+')));
            Statement s9 = new CompoundStmt(new AssignStmt("v", new ConstExpr(1)), new PrintStmt(new ArithExpr(new VarExpr("v"), new LogicalExpr(new ConstExpr(1), new ConstExpr(3), ">"), '+')));

            Statement ffff = new CompoundStmt(
                new ForkStmt(s6),
                new CompoundStmt(
                    new ForkStmt(s5),
                    new CompoundStmt(
                        new ForkStmt(s7),
                        new CompoundStmt(
                            new ForkStmt(s2),
                            s3
                            )
                        )
                    )
                );
            Statement s10 = new CompoundStmt(
                new ForkStmt(s),
                new CompoundStmt(
                    new ForkStmt(s1),
                    new CompoundStmt(
                        new ForkStmt(s6),s7
                        )
                    )
                );
            Statement s11 = new CompoundStmt(
                new ForkStmt(s7),
                new ForkStmt(s6)
                );
            #region
            //repo.AddStatement(s);
            //repo.AddStatement(s1);
            //repo.AddStatement(s2);
            //repo.AddStatement(s3);
            //repo.AddStatement(s4);
            //repo.AddStatement(s5);
            //repo.AddStatement(s6);
            //repo.AddStatement(s7);
            //repo.AddStatement(s8);
            //repo.AddStatement(s9);
            repo.AddStatement(ffff);
           // repo.AddStatement(s10);
            //repo.AddStatement(s11);
            #endregion
        }



        /// <summary>
        /// Reads an expression from UI
        /// </summary>
        /// <returns>Expression</returns>
        public Expression ReadExpression()
        {
            int expr = this.ReadExpressionForm();
            switch (expr)
            {
                case 0:
                    Expression ex1 = this.ReadExpression();
                    Expression ex2 = this.ReadExpression();
                    //readConsole.PrintSignMenu();
                    //int sign = readConsole.ReadStatementMenu();
                    char sign = this.ReadSign();
                    return new ArithExpr(ex1, ex2, sign);
                case 1:
                    //int constantX = readConsole.ReadStatementMenu();
                    int constant = this.ReadInt();
                    return new ConstExpr(constant);
                case 2:
                    //string varX = readConsole.readVar();
                    string varX = this.ReadString();
                    return new VarExpr(varX);
                case 3:
                    int numberX = this.ReadInt();
                    return new NewExpr(numberX);
                case 4:
                    string varX2 = this.ReadString();
                    return new RExpr(varX2);
            }
            return null;
        }

        /// <summary>
        /// Reads a statement from UI
        /// </summary>
        /// <returns>Statement</returns>
        public Statement ReadStatement()
        {
            //MyList outBuff = new OutputBuffer();
            //ConsoleUI readConsole = new ConsoleUI(outBuff);
            //readConsole.PrintReadStatementMenu();
            //int stmt = readConsole.ReadStatementMenu();
            int stmt = this.ReadStatementForm();
            switch (stmt)
            {
                case 0:
                    Statement st1 = this.ReadStatement();
                    Statement st2 = this.ReadStatement();
                    return new CompoundStmt(st1, st2);
                case 1:
                    string varX = this.ReadString();
                    Expression ex = this.ReadExpression();
                    return new AssignStmt(varX, ex);
                case 2:
                    Expression ex2 = this.ReadExpression();
                    return new PrintStmt(ex2);
                case 3:
                    Expression ex3 = this.ReadExpression();
                    Statement stx1 = this.ReadStatement();
                    Statement stx2 = this.ReadStatement();
                    return new IfStmt(ex3, stx1, stx2);
                case 4:
                    Statement st4 = this.ReadStatement();
                    return new ForkStmt(st4);
            }
            return null;
        }

        public int ReadInt()
        {
            string x = Interaction.InputBox("Enter a number", "Number", "");
            int toReturn = int.Parse(x);
            return toReturn;
        }

        public string ReadString()
        {
            string x = Interaction.InputBox("Enter a string", "String", "");

            return x;
        }


        public int ReadStatementForm()
        {
            // Create a new instance of the form.
            Form form1 = new Form();

            Label label = new Label();

            label.Text = "Chose statement:";
            label.Location = new Point(0, 0);

            // Create two buttons to use as the accept and cancel buttons.
            Button button1 = new Button();
            Button button2 = new Button();
            Button button3 = new Button();
            Button button4 = new Button();
            Button button5 = new Button();

            // setting texts
            button1.Text = "Compound";
            button2.Text = "Assignment";
            button3.Text = "Print";
            button4.Text = "If";
            button5.Text = "Fork";

            // setting results
            button1.DialogResult = DialogResult.OK;
            button2.DialogResult = DialogResult.Cancel;
            button3.DialogResult = DialogResult.Abort;
            button4.DialogResult = DialogResult.No;
            button5.DialogResult = DialogResult.Ignore;

            // setting locations
            button1.Location = new Point(10, 10);
            button2.Location = new Point(button1.Left, button1.Height + button1.Top + 10);
            button3.Location = new Point(button1.Left, button2.Height + button2.Top + 10);
            button4.Location = new Point(button1.Left, button3.Height + button3.Top + 10);
            button5.Location = new Point(button1.Left, button4.Height + button4.Top + 10);

            // adding buttons
            form1.Controls.Add(button1);
            form1.Controls.Add(button2);
            form1.Controls.Add(button3);
            form1.Controls.Add(button4);
            form1.Controls.Add(button5);

            // Set the caption bar text of the form.   
            form1.Text = "Chose statement type:";

            // Define the border style of the form to a dialog box.
            form1.FormBorderStyle = FormBorderStyle.FixedDialog;

            // Set the accept button of the form to button1.
            //form1.AcceptButton = button1;
            // Set the cancel button of the form to button2.
            //form1.CancelButton = button2;

            // Set the start position of the form to the center of the screen.
            form1.StartPosition = FormStartPosition.CenterScreen;

            // Display the form as a modal dialog box.
            form1.ShowDialog();

            // Determine if the OK button was clicked on the dialog box. 
            if (form1.DialogResult == DialogResult.OK) // Compound = 0
            {
                // Optional: Call the Dispose method when you are finished with the dialog box.
                form1.Dispose();
                return 0;
            }
            else if (form1.DialogResult == DialogResult.Cancel) // Assignment = 1
            {
                form1.Dispose();
                return 1;
            }
            else if (form1.DialogResult == DialogResult.Abort) // Print = 2
            {
                form1.Dispose();
                return 2;
            }
            else if (form1.DialogResult == DialogResult.No) // If = 3
            {
                form1.Dispose();
                return 3;
            }
            else if (form1.DialogResult == DialogResult.Ignore) // fork = 4
            {
                form1.Dispose();
                return 4;
            }

            return -1;

        }

        public int ReadExpressionForm()
        {
            // Create a new instance of the form.
            Form form1 = new Form();

            Label label = new Label();

            label.Text = "Chose Expression:";
            label.Location = new Point(0, 0);

            // Create two buttons to use as the accept and cancel buttons.
            Button button1 = new Button();
            Button button2 = new Button();
            Button button3 = new Button();
            Button button4 = new Button();
            Button button5 = new Button();

            // setting texts
            button1.Text = "Arithmetic";
            button2.Text = "Constant";
            button3.Text = "Var";
            button4.Text = "New";
            button5.Text = "R";

            // setting results
            button1.DialogResult = DialogResult.OK;
            button2.DialogResult = DialogResult.Cancel;
            button3.DialogResult = DialogResult.Abort;
            button4.DialogResult = DialogResult.No;
            button5.DialogResult = DialogResult.Ignore;

            // setting locations
            button1.Location = new Point(10, 10);
            button2.Location = new Point(button1.Left, button1.Height + button1.Top + 10);
            button3.Location = new Point(button1.Left, button2.Height + button2.Top + 10);
            button4.Location = new Point(button1.Left, button3.Height + button3.Top + 10);
            button5.Location = new Point(button1.Left, button4.Height + button4.Top + 10);

            // adding buttons
            form1.Controls.Add(button1);
            form1.Controls.Add(button2);
            form1.Controls.Add(button3);
            form1.Controls.Add(button4);
            form1.Controls.Add(button5);

            // Set the caption bar text of the form.   
            form1.Text = "Chose expression type:";

            // Define the border style of the form to a dialog box.
            form1.FormBorderStyle = FormBorderStyle.FixedDialog;

            // Set the accept button of the form to button1.
            //form1.AcceptButton = button1;
            // Set the cancel button of the form to button2.
            //form1.CancelButton = button2;

            // Set the start position of the form to the center of the screen.
            form1.StartPosition = FormStartPosition.CenterScreen;

            // Display the form as a modal dialog box.
            form1.ShowDialog();

            // Determine if the OK button was clicked on the dialog box. 
            if (form1.DialogResult == DialogResult.OK) // Arithmetic = 0
            {
                // Optional: Call the Dispose method when you are finished with the dialog box.
                form1.Dispose();
                return 0;
            }
            else if (form1.DialogResult == DialogResult.Cancel) // Constant = 1
            {
                form1.Dispose();
                return 1;
            }
            else if (form1.DialogResult == DialogResult.Abort) // Var = 2
            {
                form1.Dispose();
                return 2;
            }
            else if (form1.DialogResult == DialogResult.No) // New = 3
            {
                form1.Dispose();
                return 3;
            }
            else if (form1.DialogResult == DialogResult.Ignore) // r = 4
            {
                form1.Dispose();
                return 4;
            }

            return -1;

        }

        public int readSign()
        {
            // Create a new instance of the form.
            Form form1 = new Form();

            Label label = new Label();

            label.Text = "Chose Expression:";
            label.Location = new Point(0, 0);

            // Create two buttons to use as the accept and cancel buttons.
            Button button1 = new Button();
            Button button2 = new Button();
            Button button3 = new Button();
            Button button4 = new Button();

            // setting texts
            button1.Text = "+";
            button2.Text = "-";
            button3.Text = "*";
            button4.Text = "/";

            // setting results
            button1.DialogResult = DialogResult.OK;
            button2.DialogResult = DialogResult.Cancel;
            button3.DialogResult = DialogResult.Abort;
            button4.DialogResult = DialogResult.No;

            // setting locations
            button1.Location = new Point(10, 10);
            button2.Location = new Point(button1.Left, button1.Height + button1.Top + 10);
            button3.Location = new Point(button1.Left, button2.Height + button2.Top + 10);
            button4.Location = new Point(button1.Left, button3.Height + button3.Top + 10);

            // adding buttons
            form1.Controls.Add(button1);
            form1.Controls.Add(button2);
            form1.Controls.Add(button3);
            form1.Controls.Add(button4);

            // Set the caption bar text of the form.   
            form1.Text = "Chose expression type:";

            // Define the border style of the form to a dialog box.
            form1.FormBorderStyle = FormBorderStyle.FixedDialog;

            // Set the accept button of the form to button1.
            //form1.AcceptButton = button1;
            // Set the cancel button of the form to button2.
            //form1.CancelButton = button2;

            // Set the start position of the form to the center of the screen.
            form1.StartPosition = FormStartPosition.CenterScreen;

            // Display the form as a modal dialog box.
            form1.ShowDialog();

            // Determine if the OK button was clicked on the dialog box. 
            if (form1.DialogResult == DialogResult.OK) // + = 0
            {
                // Optional: Call the Dispose method when you are finished with the dialog box.
                form1.Dispose();
                return 0;
            }
            else if (form1.DialogResult == DialogResult.Cancel) // - = 1
            {
                form1.Dispose();
                return 1;
            }
            else if (form1.DialogResult == DialogResult.Abort) // * = 2
            {
                form1.Dispose();
                return 2;
            }
            else if (form1.DialogResult == DialogResult.No) // / = 3
            {
                form1.Dispose();
                return 3;
            }

            return -1;

        }

        public char ReadSign()
        {
            // Create a new instance of the form.
            Form form1 = new Form();

            Label label = new Label();

            label.Text = "Chose Expression:";
            label.Location = new Point(0, 0);

            // Create two buttons to use as the accept and cancel buttons.
            Button button1 = new Button();
            Button button2 = new Button();
            Button button3 = new Button();
            Button button4 = new Button();

            // setting texts
            button1.Text = "+";
            button2.Text = "-";
            button3.Text = "*";
            button4.Text = "/";

            // setting results
            button1.DialogResult = DialogResult.OK;
            button2.DialogResult = DialogResult.Cancel;
            button3.DialogResult = DialogResult.Abort;
            button4.DialogResult = DialogResult.No;

            // setting locations
            button1.Location = new Point(10, 10);
            button2.Location = new Point(button1.Left, button1.Height + button1.Top + 10);
            button3.Location = new Point(button1.Left, button2.Height + button2.Top + 10);
            button4.Location = new Point(button1.Left, button3.Height + button3.Top + 10);

            // adding buttons
            form1.Controls.Add(button1);
            form1.Controls.Add(button2);
            form1.Controls.Add(button3);
            form1.Controls.Add(button4);

            // Set the caption bar text of the form.   
            form1.Text = "Chose expression type:";

            // Define the border style of the form to a dialog box.
            form1.FormBorderStyle = FormBorderStyle.FixedDialog;

            // Set the accept button of the form to button1.
            //form1.AcceptButton = button1;
            // Set the cancel button of the form to button2.
            //form1.CancelButton = button2;

            // Set the start position of the form to the center of the screen.
            form1.StartPosition = FormStartPosition.CenterScreen;

            // Display the form as a modal dialog box.
            form1.ShowDialog();

            // Determine if the OK button was clicked on the dialog box. 
            if (form1.DialogResult == DialogResult.OK) // + = 0
            {
                // Optional: Call the Dispose method when you are finished with the dialog box.
                form1.Dispose();
                return '+';
            }
            else if (form1.DialogResult == DialogResult.Cancel) // - = 1
            {
                form1.Dispose();
                return '-';
            }
            else if (form1.DialogResult == DialogResult.Abort) // * = 2
            {
                form1.Dispose();
                return '*';
            }
            else if (form1.DialogResult == DialogResult.No) // / = 3
            {
                form1.Dispose();
                return '/';
            }

            return 'a';
        }

        private void add_program_Click(object sender, EventArgs e)
        {
            Statement stmt = this.ReadStatement();
            ctr.AddStatement(stmt);
            LoadPrgStates(ctr.GetAllStatement());
        }
    }
}
