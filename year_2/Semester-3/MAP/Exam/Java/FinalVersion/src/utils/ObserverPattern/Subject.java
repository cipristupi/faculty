package utils.ObserverPattern;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by Ciprian on 12/1/2014.
 */
public class Subject extends Observable {
    public Subject() {
        super();
    }

    @Override
    public void notifyObservers(Object arg) {
        setChanged();
        super.notifyObservers(arg);
    }

   /* @Override
    public  void addObserver(Observer o) {
        super.addObserver(o);
    }

    @Override
    public  void deleteObserver(Observer o) {
        super.deleteObserver(o);
    }*/
}
