package utils.MyDictionary;

import utils.MyList.List;
import utils.MyList.Node;

/**
 * Created by Ciprian on 10/8/2014.
 */
public class Dictionary<TKey,TValue> implements IDictionary<TKey,TValue>
{
    private List<Node> elem;
    public Dictionary()
    {
        elem =new List<Node>();
    }
    public void add(TKey k, TValue v)
    {
        Node n =new Node(k,v);
        if(this.checkKey(k)==false)
        {
            elem.add(n);
        }
        else
        {
            this.updateValue(k,v);
        }
    }
    private Boolean checkKey(TKey k)
    {
        List<TKey> l=this.getKeys();
        for(int i=0;i<l.getSize();i++)
        {
            if(l.getAt(i).equals(k))
            {
                return true;
            }
        }
        return false;
    }
    public TValue searchKey(TKey k)
    {
        for(int i=0;i<elem.getSize();i++)
        {
           if(elem.getAt(i).key.equals(k))
           {
               return (TValue)elem.getAt(i).value;
           }
        }
        return null;
    }

    public List<TKey> getKeys()
    {
        List<TKey> l=new List<TKey>();
        for(int i=0;i<elem.getSize();i++)
        {
             l.add((TKey)elem.getAt(i).key);
        }
        return l;
    }

    public void updateValue(TKey k, TValue v)
    {
        for(int i=0;i<elem.getSize();i++)
        {
            if(elem.getAt(i).key.equals(k))//Equals ???
            {
                elem.getAt(i).value = v;
            }
        }
    }

    public String toString() {
        String result = "";
        for (int i = 0; i < elem.getSize(); i++) {
            result += elem.getAt(i).key.toString() + ":" + elem.getAt(i).value.toString() + "\n";
        }
        return result;
    }
}


