package models.Statements;

/**
 * Created by Ciprian on 1/26/2015.
 */
public class IncStmt extends Statement {
    private String varName;
    public IncStmt(String varN)
    {
        varName = varN;
    }
    public String Name() { return varName; }
    public  String toString()
    {
        return varName + "++" ;
    }
}
