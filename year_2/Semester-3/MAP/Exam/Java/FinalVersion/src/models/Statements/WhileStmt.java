package models.Statements;

import models.Expressions.Expression;

/**
 * Created by Ciprian on 1/28/2015.
 */
public class WhileStmt extends Statement {

    private Expression expr;
    private Statement stmt;

    public Expression Expr() {
        return expr;
    }

    public Statement Stmt() {
        return stmt;
    }

    public WhileStmt(Expression e, Statement s) {
        expr = e;
        stmt = s;
    }

    @Override
    public String toString() {
        return "While(" + expr.toString() + ") " + stmt.toString();
    }

}
