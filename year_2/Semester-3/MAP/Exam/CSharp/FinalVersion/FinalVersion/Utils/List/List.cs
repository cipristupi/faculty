﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laboratory2.Utils
{
    public class List<TEntity> : IList<TEntity>
    {
        private TEntity[] elem;
        private int capacity;
        private int size;
        public List()
        {
            capacity = 10;
            //elem = new TEntity[capacity];
            elem = new TEntity[capacity];
        }

        private void resize()
        {
            TEntity[] tmp = new TEntity[capacity];
            for (int i = 0; i < size; i++)
            {
                tmp[i] = elem[i];
            }
            elem = tmp;
            capacity = capacity * 2;
        }

        public void add(TEntity el)
        {
            if (size == capacity)
            {
                resize();
            }
            elem[size] = el; // == elem[top++]=el;
            size++;
        }

        public bool isEmpty()
        {
            return size == 0;
        }


        public TEntity getAt(int i)
        {
            for (int j = 0; j < size; j++)
            {
                if (j == i)
                    return elem[i];
            }
            return default(TEntity);
        }

        public int getSize()
        {
            return size;
        }
    }
}
