﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laboratory2.Utils
{
    public interface IList<TEntity>
    {
        void add(TEntity el);
        bool isEmpty();
        TEntity getAt(int i);
        int getSize();
    }

}
