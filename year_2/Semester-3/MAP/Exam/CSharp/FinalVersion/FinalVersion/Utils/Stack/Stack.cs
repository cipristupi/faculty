﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laboratory2.Utils
{
    class Stack<TEntity> : IStack<TEntity>
    {
        private int top;
        private TEntity[] elem;
        private int capacity;
        public Stack()
        {
            capacity = 10;
            //elem = new TEntity[capacity];
            elem = new TEntity[capacity];
            top = 0;
        }
        public void push(TEntity el)
        {
            if (top == capacity)
            {
                resize();
            }
            elem[top] = el; // == elem[top++]=el;
            top++;
        }
        private void resize()
        {
            TEntity[] tmp = new TEntity[capacity];
            for (int i = 0; i < top; i++)
            {
                tmp[i] = elem[i];
            }
            elem = tmp;
            capacity = capacity * 2;
        }
        public TEntity pop()
        {
            if (top == 0)
                return default(TEntity);
            return elem[--top];
        }
        public bool isEmpty()
        {
            return top == 0;
        }
        public TEntity puk()
        {
            if (top == 0)
            {
                return default(TEntity);
            }
            return elem[top - 1];
        }

        public override string ToString()
        {
            string result = "";
            for (int i = top; i >= 0; i--)
            {
                result += elem[i].ToString() + "\n";
            }
            return result;
        }
    }
}
