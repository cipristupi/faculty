﻿using FinalVersion.Models.Statements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalVersion.Utils
{

    //C
    public class ProcProp
    {
        private List<string> varNames;
        private Statement stm;

        public ProcProp(List<String> vn, Statement st)
        {
            this.varNames = vn;
            this.stm = st;
        }

        public override string ToString()
        {
            return "( " + varNames + " , " + stm.ToString() + " )";
        }

        public List<string> Parameters
        {
            get { return varNames; }
            set { varNames = value; }
        }

        public Statement Body
        {
            get { return stm; }
            set { stm = value; }
        }
        //public ArrayList<String> getVarNames()
        //{
        //    return this.varNames;
        //}

        //public Stmt getStmt()
        //{
        //    return this.stm;
        //}
    }
}
