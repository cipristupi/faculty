﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalVersion.Utils.ProcTable
{
    public class ProcTable : IProcTable
    {

        /// <summary>
        /// procTable
        /// string :  nameof procedure
        /// ProcProp : listof parameters and the statement;
        /// </summary>
        private Dictionary<string, ProcProp> procTable;
        public ProcTable()
        {
            procTable = new Dictionary<string, ProcProp>();
        }
        public Dictionary<string, ProcProp> GetProcedures()
        {
            return this.procTable;
        }

        public void Insert(string procName, List<string> varNames, Models.Statements.Statement st)
        {
            ProcProp p = new ProcProp(varNames, st);
            this.procTable.Add(procName, p);
        }

        public ProcProp GetByName(string name)
        {
            return procTable[name];
        }
    }
}
