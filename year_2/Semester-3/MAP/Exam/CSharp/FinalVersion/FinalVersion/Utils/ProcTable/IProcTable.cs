﻿using FinalVersion.Models.Statements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalVersion.Utils.ProcTable
{
    interface IProcTable
    {
        Dictionary<string, ProcProp> GetProcedures();

        void Insert(String procName, List<string> varNames, Statement st);

        ProcProp GetByName(String name);
    }
}
