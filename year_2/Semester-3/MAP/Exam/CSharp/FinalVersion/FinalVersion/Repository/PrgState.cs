﻿using FinalVersion.Models.Statements;
using FinalVersion.Utils.Heap;
using FinalVersion.Utils.ProcTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalVersion.Repository
{
    [Serializable]
    public class PrgState
    {
        #region Global variables
        private Stack<Dictionary<string, int>> symTable;
        private Stack<Statement> exeStack;
        private List<string> outBuff;
        private Heap heap;
        private Statement initialStmt;
        private ProcTable procTable;
        #endregion

        #region  Constructors
        public PrgState(Statement initStmt)
        {
            initialStmt = initStmt;//save a copy of initial statement for display
            symTable = new Stack<Dictionary<string, int>>();
            Dictionary<string, int> initialDictionary = new Dictionary<string, int>();
            symTable.Push(initialDictionary);
            exeStack = new Stack<Statement>();
            outBuff = new List<string>();
            heap = new Heap();
            procTable = new Utils.ProcTable.ProcTable();
            exeStack.Push(initStmt);
        }

        public PrgState(Stack<Dictionary<string, int>> st, Stack<Statement> s, List<string> o, Heap h)
        {
            symTable = st;
            exeStack = s;
            outBuff = o;
            heap = h;
        }

        #endregion

        #region Setters and Getters
        public Stack<Dictionary<string, int>> SymTable
        { 
            get { return symTable; }
            set { symTable = value; }
        }
        public List<string> OutBuff 
        {
            get { return outBuff; }
            set { outBuff = value; }
        }

        public Stack<Statement> ExeStack
        {
            get { return exeStack; }
            set { exeStack = value; }
        }

        public Heap Heap
        {
            get { return heap; }
            set { heap = value; }
        }

        public ProcTable ProcTable
        {
            get { return procTable; }
            set { procTable = value; }
        }

        public Statement InitialStmt { get { return initialStmt; } }

        public int Id { get; set; }
        #endregion

        #region Utils
        public void CopySymTable(Stack<Dictionary<string,int>> sourceSymTable)
        {
            foreach (var pair in sourceSymTable)
            {
                symTable.Push(pair);
            }
        }

        public override string ToString()
        {
            string o = "";
            o += "\n //Stack content \n";
            foreach (Statement s in exeStack)
            {
                o += s.ToString() + "\n";
            }
            o += "\n //SymTable" + "\n";
            foreach (var v in symTable)
            {
                foreach (KeyValuePair<string, int> pair in v)
                {
                    o += String.Format("{0} -> {1} \n", pair.Key, pair.Value);
                }
            }
            //foreach (KeyValuePair<string, int> pair in symTable)
            //{
            //    o += String.Format("{0} -> {1} \n", pair.Key, pair.Value);
            //}
            o += "\n //Out content \n";
            foreach (string s in outBuff)
            {
                o += s + "\n";
            }
            o += "\n";

            //TODO print heap here
            return o;

        }



        #endregion
    }
}
