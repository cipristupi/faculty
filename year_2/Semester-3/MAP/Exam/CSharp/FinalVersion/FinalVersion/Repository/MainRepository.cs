﻿using FinalVersion.Models.Statements;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace FinalVersion.Repository
{
    public class MainRepository
    {
        private List<Statement> statementList;//list of available statements to run
        private List<PrgState> prgStateList;//list of running statements;
        public MainRepository()
        {
            prgStateList = new List<PrgState>();
            statementList = new List<Statement>();
        }

        public void AddStatement(Statement stmt)
        {
            statementList.Add(stmt);
        }

        public List<Statement> StatementList { get { return statementList; } }
        public List<PrgState> PrgStateList { get { return prgStateList; } }

        public void NewPrgState(int id)
        {
            prgStateList.Add(new PrgState(statementList[id]));
            int last = prgStateList.Count - 1;
            prgStateList[last].Id = last;
        }

        public void AddFork(Statement stmt, PrgState parent)
        {
            prgStateList.Add(new PrgState(stmt));
            int last = prgStateList.Count - 1;
            PrgState child = prgStateList[last];
            child.Heap = parent.Heap;
            child.CopySymTable(parent.SymTable);
            child.OutBuff = parent.OutBuff;
            child.Id = last;
        }

        public PrgState GetPrgStateById(int id)
        {
            return prgStateList[id];
        }
        public Statement GetStatementById(int id)
        {
            return statementList[id];
        }

        public bool CheckPrgStateExists(int id)
        {
            return prgStateList[id] != null ? true : false;
        }

        public int GetPrgStateNumbers()
        {
            return prgStateList.Count;
        }


        /// <summary>
        /// Check if a prgstate stack is empty
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool PrgStateStackIsEmpty(int id)
        {
            return GetPrgStateById(id).ExeStack.Count == 0;
        }

        public void RemovePrgState(int id)
        {
            prgStateList.RemoveAt(id);
        }

        private void UpdateId()
        {
            //for (int i = 0; i < prgStateList.Count; i++)
            //{
            //    prgStateList[i].Id = i;
            //}
        }


        #region Serialization and File Operations
        public void Serialize(int programID)
        {
            PrgState s = this.GetPrgStateById(programID);
            string uriPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase) + "\\outputs\\" + programID.ToString() + ".txt";
            string filePath = new Uri(uriPath).LocalPath;
            IFormatter formatter = new BinaryFormatter();
            Stream stream = new FileStream(filePath, FileMode.Create, FileAccess.Write, FileShare.None);
            formatter.Serialize(stream, s);
            stream.Close();
        }

        public PrgState Deserialize(int programID)
        {
            string uriPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase) + "\\outputs\\" + programID.ToString() + ".txt";
            string filePath = new Uri(uriPath).LocalPath;
            IFormatter formatter = new BinaryFormatter();
            Stream stream = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read);
            PrgState obj = (PrgState)formatter.Deserialize(stream);
            stream.Close();
            return obj;
        }

        public void SaveTofile(int programID)
        {
            PrgState r = this.GetPrgStateById(programID);
            SaveTofile(r);
        }

        //public void SaveTofile(PrgState r, bool append = false)
        //{
        //    string uriPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase) + "\\outputs\\" + "prgState" + ".txt";
        //    string filePath = new Uri(uriPath).LocalPath;
        //    string result = r.ToString();
        //    using (StreamWriter file = new StreamWriter(filePath, append))//append new line at the end of file
        //    {
        //        if (append)
        //        {
        //            result += "\n--------------------------------------------------";
        //        }
        //        file.WriteLine(result);
        //        file.Flush();
        //    }
        //}

        public void SaveTofile(PrgState r, bool append = false, int? step = null)
        {
            string uriPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase) + "\\Outputs\\" + "prgState" + ".txt";
            string filePath = new Uri(uriPath).LocalPath;
            string result = GetString(r, step);
            using (StreamWriter file = new StreamWriter(filePath, append))//append new line at the end of file
            {
                //if (append)
                //{
                //    result += "\n--------------------------------------------------";
                //}
                file.WriteLine(result);
                file.Flush();
            }
        }


        private string GetString(PrgState prgState, int? step = null)
        {
            string exeStack, outBuff, symTable, heap;
            string separator = "";
            exeStack = outBuff = symTable = heap = "[Thread " + prgState.Id + "]\n";

            exeStack += string.Concat(Enumerable.Repeat("=", 30)) + "      Stack      " + string.Concat(Enumerable.Repeat("=", 30)) + "\n";
            outBuff += string.Concat(Enumerable.Repeat("=", 30)) + "      Out      " + string.Concat(Enumerable.Repeat("=", 30)) + "\n";
            symTable += string.Concat(Enumerable.Repeat("=", 30)) + "      SymTable      " + string.Concat(Enumerable.Repeat("=", 30)) + "\n";
            heap += string.Concat(Enumerable.Repeat("=", 30)) + "Heap" + string.Concat(Enumerable.Repeat("=", 30)) + "\n";
            //Stack content;
            foreach (Statement s in prgState.ExeStack)
            {
                exeStack += s.ToString() + "\n";
            }
            exeStack += "\n\n";
            //SymTable
            foreach (var v in prgState.SymTable)
            {
                foreach (KeyValuePair<string, int> pair in v)
                {
                    symTable += String.Format("{0} -> {1} \n", pair.Key, pair.Value);
                }
            }
            symTable += "\n\n";
            //Out content
            foreach (string s in prgState.OutBuff)
            {
                outBuff += s + "\n";
            }
            outBuff += "\n\n";
            //Heap content
            for (int i = 0; i < prgState.Heap.Count; i++)
            {
                if (prgState.Heap.GetAt(i) == null)
                    break;
                heap += "[" + i.ToString() + "]=" + prgState.Heap.GetAt(i).ToString() + "\n";
            }
            heap += "\n\n";


            if (step != null)
            {
                separator = string.Concat(Enumerable.Repeat("=", 30));
                separator += "   STEP   " + step.ToString() + "      ";
                separator += string.Concat(Enumerable.Repeat("=", 30));
                separator += "\n";
            }
            else
            {
                separator = string.Concat(Enumerable.Repeat("-", 100));
                separator += "\n";
            }
            separator += exeStack;
            separator += outBuff;
            separator += symTable;
            separator += heap;
            return separator;
        }
        #endregion
    }
}
