﻿using FinalVersion.Models.Expressions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalVersion.Models.Statements
{
    [Serializable]
    public class PrintStmt : Statement
    {
        private Expression expr;
        public PrintStmt(Expression ex)
        {
            expr = ex;
        }

        public Expression Expr { get { return expr; } }
        public string Print()
        {
            return expr.ToString();
        }
        public override string ToString()
        {
            return "Print(" + expr.ToString() + ") ";
        }
    }
}
