﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalVersion.Models.Expressions
{
    [Serializable]
    public class ConstExpr : Expression
    {
        private int value;
        public ConstExpr(int v)
        {
            value = v;
        }

        public int Constant { get { return value; } }
        public override string ToString()
        {
            return value.ToString();
        }
    }
}
