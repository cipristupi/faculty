﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalVersion.Models.Expressions
{
    [Serializable]
    public class VarExpr : Expression
    {
        private string varName;
        public VarExpr(string varN)
        {
            varName = varN;
        }

        public string Name { get { return varName; } }
        public override string ToString()
        {
            return varName;
        }
    }
}
