﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalVersion.Models.Statements
{
    [Serializable]
    public class IncStmt : Statement
    {
        private string varName;
        public IncStmt(string varN)
        {
            varName = varN;
        }
        public string Name { get { return varName; } }
        public override string ToString()
        {
            return varName + "++" ;
        }
    }
}
