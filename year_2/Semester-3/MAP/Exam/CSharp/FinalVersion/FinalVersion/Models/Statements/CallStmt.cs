﻿using FinalVersion.Models.Expressions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalVersion.Models.Statements
{
    class CallStmt : Statement
    {
        private String procName;
        private List<Expression> exprs;

        public CallStmt(String procName, List<Expression> exps)
        {
            this.procName = procName;
            this.exprs = exps;
        }

        public override string  ToString()
        {
            String s = procName + " (";
            foreach (Expression e in exprs)
            {
                s += " " + e.ToString() + " ,";
            }
            s += ")";
            return s;
        }

        public String getProcName()
        {
            return this.procName;
        }

        public List<Expression> getExprs()
        {
            return this.exprs;
        }

    }
}
