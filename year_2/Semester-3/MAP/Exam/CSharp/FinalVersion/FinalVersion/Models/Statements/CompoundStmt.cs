﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalVersion.Models.Statements
{
    [Serializable]
    public class CompoundStmt : Statement
    {
        private Statement stmt1, stmt2;
        public CompoundStmt(Statement s1, Statement s2)
        {
            stmt2 = s2;
            stmt1 = s1;
        }

        public Statement Stmt1 { get { return stmt1; } }
        public Statement Stmt2 { get { return stmt2; } }
        public override string ToString()
        {
            String s = "CompoundStatement( ";
            s += stmt1.ToString();
            s += ";";
            s += stmt2.ToString();
            s += ")";
            return s;
        }
    }
}
