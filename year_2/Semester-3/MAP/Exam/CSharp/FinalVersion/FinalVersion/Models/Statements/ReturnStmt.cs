﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalVersion.Models.Statements
{
    class ReturnStmt :Statement
    {

        public override string ToString()
        {
            return "Return";    
        }
    }
}
