﻿using FinalVersion.Models.Expressions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalVersion.Models.Statements
{
    [Serializable]
    public class AssignStmt : Statement
    {
        private string varName;
        private Expression expr;
        public AssignStmt(string varN, Expression e)
        {
            varName = varN;
            expr = e;
        }
        public string Name { get { return varName; } }
        public Expression Expr { get { return expr; } }
        public override string ToString()
        {
            return varName + "=" + expr.ToString();
        }
    }
}
