﻿using FinalVersion.Utils.Heap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalVersion.Models.Expressions
{
    [Serializable]
    public class NewExpr : Expression
    {
        private int value;
        public NewExpr(int v)
        {
            value = v;
        }
        //public int Value
        //{
        //    get { return value; }
        //}

        public int Eval(Heap h)
        {
            return h.New(value);
        }

        public override string ToString()
        {
            return "New(" + value + ")";
        }
    }
}
