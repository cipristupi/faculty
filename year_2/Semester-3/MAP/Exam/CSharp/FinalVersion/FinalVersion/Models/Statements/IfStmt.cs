﻿using FinalVersion.Models.Expressions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalVersion.Models.Statements
{
    [Serializable]
    public class IfStmt :Statement
    {
        private Expression expr;
        private Statement stmt1, stmt2;
        public IfStmt(Expression ex, Statement s1, Statement s2)
        {
            expr = ex;
            stmt1 = s1;
            stmt2 = s2;
        }

        public Expression Expr { get { return expr; } }
        public Statement Stmt1 { get { return stmt1; } }
        public Statement Stmt2 { get { return stmt2; } }
        public override string ToString()
        {
            String s = "";
            s += "If( ";
            s += expr.ToString() + ")";
            s += " Then ";
            s += stmt1.ToString();
            if (stmt2.ToString() != "")
            {
                s += " Else ";
                s += stmt2.ToString();
            }
            return s;
        }
    }
}
