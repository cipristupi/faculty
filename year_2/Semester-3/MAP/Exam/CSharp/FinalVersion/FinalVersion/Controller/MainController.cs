﻿using FinalVersion.Models.Expressions;
using FinalVersion.Models.Statements;
using FinalVersion.Repository;
using FinalVersion.Utils;
using FinalVersion.Utils.ProcTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalVersion.Controller
{
    class MainController
    {
        private MainRepository repo;
        private PrgState currentPrgState;
        private ProcTable procTable;
        public MainController(MainRepository r)
        {
            repo = r;
            procTable = new ProcTable();
        }

        public PrgState CurrentPrgState { get { return currentPrgState; } }

        public PrgState RunStepByStep(int programID)
        {
            if (!repo.CheckPrgStateExists(programID)) { return null; }
            InitializePrgState(programID);
            Statement stmt = currentPrgState.ExeStack.Pop();
            EvalStatement(stmt);
            return currentPrgState;
        }
        public PrgState RunStepByStep(int programID, bool printToFile = false)
        {
            if (!repo.CheckPrgStateExists(programID)) { return null; }
            InitializePrgState(programID);
            Statement stmt = currentPrgState.ExeStack.Pop();
            EvalStatement(stmt);
            if (printToFile)
                repo.SaveTofile(currentPrgState, true);
            return currentPrgState;
        }

        public PrgState RunStepByStep(int programID, bool printToFile = false, int? step = null)
        {
            if (!repo.CheckPrgStateExists(programID)) { return null; }
            InitializePrgState(programID);
            Statement stmt = currentPrgState.ExeStack.Pop();
            EvalStatement(stmt);
            if (printToFile)
                repo.SaveTofile(currentPrgState, true, step);
            return currentPrgState;
        }

        private void EvalStatement(Statement stmt)
        {
            if (stmt is CompoundStmt)
            {
                ExecCompound((CompoundStmt)stmt);
            }
            else
                if (stmt is IfStmt)
                {
                    ExecIf((IfStmt)stmt);
                }
                else
                    if (stmt is PrintStmt)
                    {
                        ExecPrint((PrintStmt)stmt);
                    }
                    else
                        if (stmt is AssignStmt)
                        {
                            ExecAssign((AssignStmt)stmt);
                        }
                        else
                            if (stmt is ForkStmt)
                            {
                                ExecFork((ForkStmt)stmt);
                            }
                            else
                                if (stmt is DecStmt)
                                {
                                    ExecDec((DecStmt)stmt);
                                }
                                else
                                    if (stmt is IncStmt)
                                    {
                                        ExecInc((IncStmt)stmt);
                                    }
                                    else
                                        if (stmt is CallStmt)
                                        {
                                            ExecCall((CallStmt)stmt);
                                        }
                                        else
                                            if (stmt is ReturnStmt)
                                            {
                                                ExecReturn((ReturnStmt)stmt);
                                            }
        }

        private void ExecCall(CallStmt callStmt)
        {
            Dictionary<string,int> newSym = new Dictionary<string,int>();
            
            ProcProp p = procTable.GetByName(callStmt.getProcName());
            List<Expression> exprs = callStmt.getExprs();
            for (int i = 0; i < exprs.Count; i++)
            {
                newSym.Add(p.Parameters[i], EvalExpression(exprs[i]));//eval each parameter
            }
            currentPrgState.SymTable.Push(newSym);
            currentPrgState.ExeStack.Push(new ReturnStmt());//push return statement
            currentPrgState.ExeStack.Push(p.Body); //push the body of call to the execstack;
            //prg.getSym().push(newSym);
            //prg.getStack().push(new ReturnStmt());
            //prg.getStack().push(p.getStmt());
            //throw new NotImplementedException();
        }

        private int EvalExpression(Expression expr)
        {
            int value = 0;
            if (expr is ConstExpr)
            {
                value = ((ConstExpr)expr).Constant;
            }
            else
                if (expr is VarExpr)
                {
                    String id = ((VarExpr)expr).Name;
                    value = currentPrgState.SymTable.Peek()[id];
                }
                else
                    if (expr is ArithExpr)
                    {
                        value = ((ArithExpr)expr).Eval(currentPrgState.SymTable.Peek(), currentPrgState.Heap);
                    }
                    else
                        if (expr is NewExpr)
                        {
                            value = ((NewExpr)expr).Eval(currentPrgState.Heap);
                        }
                        else
                            if (expr is RExpr)
                            {
                                value = ((RExpr)expr).Eval(currentPrgState.SymTable.Peek(), currentPrgState.Heap);
                            }
                            else
                                if (expr is LogicalExpr)
                                {
                                    value = ((LogicalExpr)expr).Eval(currentPrgState.SymTable.Peek(), currentPrgState.Heap);
                                }
            return value;
        }

        private void ExecReturn(ReturnStmt returnStmt)
        {
            currentPrgState.SymTable.Pop();
            //prg.getSym().pop();
        }



        #region Execution of Statements
        private void ExecFork(ForkStmt forkStmt)
        {
            repo.AddFork(forkStmt.ToFork, currentPrgState);
        }

        private void ExecAssign(AssignStmt assignStmt)
        {
            Expression expr = assignStmt.Expr;
            string varName = assignStmt.Name;
            int value = 0;
            if (expr is ConstExpr)
            {
                value = ((ConstExpr)expr).Constant;
            }
            else
                if (expr is VarExpr)
                {
                    String id = ((VarExpr)expr).Name;
                    value = currentPrgState.SymTable.Peek()[id];
                }
                else
                    if (expr is ArithExpr)
                    {
                        value = ((ArithExpr)expr).Eval(currentPrgState.SymTable.Peek(), currentPrgState.Heap);
                    }
                    else
                        if (expr is NewExpr)
                        {
                            value = ((NewExpr)expr).Eval(currentPrgState.Heap);
                        }
                        else
                            if (expr is RExpr)
                            {
                                value = ((RExpr)expr).Eval(currentPrgState.SymTable.Peek(), currentPrgState.Heap);
                            }
                            else
                                if (expr is LogicalExpr)
                                {
                                    value = ((LogicalExpr)expr).Eval(currentPrgState.SymTable.Peek(), currentPrgState.Heap);
                                }
            currentPrgState.SymTable.Peek().Add(varName, value);
        }

        private void ExecPrint(PrintStmt printStmt)
        {
            Expression toPrint = printStmt.Expr;
            try
            {
                int result = 0;
                if (toPrint is ConstExpr)
                {
                    result = ((ConstExpr)toPrint).Constant;
                }
                else if (toPrint is VarExpr)
                {
                    String id = ((VarExpr)toPrint).Name;
                    result = currentPrgState.SymTable.Peek()[id];
                }
                else if (toPrint is ArithExpr)
                {
                    result = ((ArithExpr)toPrint).Eval(currentPrgState.SymTable.Peek(), currentPrgState.Heap);
                }
                else if (toPrint is NewExpr)
                {
                    result = ((NewExpr)toPrint).Eval(currentPrgState.Heap);
                }
                else if (toPrint is RExpr)
                {
                    result = ((RExpr)toPrint).Eval(currentPrgState.SymTable.Peek(), currentPrgState.Heap);
                }
                else if (toPrint is LogicalExpr)
                {
                    result = ((LogicalExpr)toPrint).Eval(currentPrgState.SymTable.Peek(), currentPrgState.Heap);
                }
                String x = result.ToString();
                currentPrgState.OutBuff.Add(x);
            }
            catch (Exception ex)
            {
                currentPrgState.OutBuff.Add(ex.ToString());
            }
        }

        private void ExecIf(IfStmt ifStmt)
        {
            Expression expr = ifStmt.Expr;
            int value = 0;
            if (expr is ConstExpr)
            {
                value = ((ConstExpr)expr).Constant;
            }
            else
                if (expr is VarExpr)
                {
                    String id = ((VarExpr)expr).Name;
                    value = currentPrgState.SymTable.Peek()[id];
                }
                else
                    if (expr is ArithExpr)
                    {
                        value = ((ArithExpr)expr).Eval(currentPrgState.SymTable.Peek(), currentPrgState.Heap);
                    }
                    else
                        if (expr is NewExpr)
                        {
                            value = ((NewExpr)expr).Eval(currentPrgState.Heap);
                        }
                        else
                            if (expr is RExpr)
                            {
                                value = ((RExpr)expr).Eval(currentPrgState.SymTable.Peek(), currentPrgState.Heap);
                            }
                            else
                                if (expr is LogicalExpr)
                                {
                                    value = ((LogicalExpr)expr).Eval(currentPrgState.SymTable.Peek(), currentPrgState.Heap);
                                }

            if (value != 0)
            {
                currentPrgState.ExeStack.Push(ifStmt.Stmt1);
            }
            else
            {
                currentPrgState.ExeStack.Push(ifStmt.Stmt2);
            }
        }

        private void ExecCompound(CompoundStmt compoundStmt)
        {
            currentPrgState.ExeStack.Push(compoundStmt.Stmt2);
            currentPrgState.ExeStack.Push(compoundStmt.Stmt1);
        }

        private void ExecInc(IncStmt incStmt)
        {
            //currentPrgState.SymTable[incStmt.Name]++;
        }

        private void ExecDec(DecStmt decStmt)
        {
            currentPrgState.SymTable.Peek()[decStmt.Name]--;
        }
        #endregion

        #region Utils
        private void InitializePrgState(int programID)
        {
            currentPrgState = repo.GetPrgStateById(programID);
        }
        #endregion

        public void AddStatement(Statement s)
        {
            repo.AddStatement(s);
        }

        public int GetPrgStateNumber()
        {
            return repo.GetPrgStateNumbers();
        }

        public List<Statement> GetAllStatement()
        {
            return repo.StatementList;
        }

        internal void NewPrgState(int programID)
        {
            repo.NewPrgState(programID);
        }

        public bool PrgStateStackIsEmpty(int programID)
        {
            return repo.PrgStateStackIsEmpty(programID);
        }

        public void RemovePrgState(int programID)
        {
            repo.RemovePrgState(programID);
        }

        public PrgState GetPrgStateByID(int programID)
        {
            return repo.GetPrgStateById(programID);
        }

        public void insertProcedure(String procName, List<String> varNames, Statement st)
        {
            procTable.Insert(procName, varNames, st);
        }
    }
}
