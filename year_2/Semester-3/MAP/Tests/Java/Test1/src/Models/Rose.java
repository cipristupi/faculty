package Models;

/**
 * Created by Ciprian on 10/29/2014.
 */
public class Rose extends Flower {

    public Rose(Integer len,String color)
    {
        setLength(len);
        setColor(color);
    }

    public String toString()
    {
        return "Rose: "+getColor() +"  "+ getLength().toString();
    }
}
