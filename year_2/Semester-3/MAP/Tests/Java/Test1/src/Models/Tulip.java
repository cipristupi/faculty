package Models;

/**
 * Created by Ciprian on 10/29/2014.
 */
public class Tulip extends Flower
{
    public Tulip(Integer len,String color)
    {
        setLength(len);
        setColor(color);
    }

    public String toString()
    {
        return "Tulip: "+ getColor() +" " + getLength().toString();
    }
}
