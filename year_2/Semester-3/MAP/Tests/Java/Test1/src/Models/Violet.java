package Models;

/**
 * Created by Ciprian on 10/29/2014.
 */
public class Violet  extends  Flower{
    public Violet(Integer len,String color)
    {
        setLength(len);
        setColor(color);
    }

    public String toString()
    {
        return "Violet: " + getColor() +"  " + getLength().toString();
    }
}
