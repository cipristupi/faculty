package Models;

/**
 * Created by Ciprian on 10/29/2014.
 */
public abstract class Flower {
    private Integer length;
    private String color;
    public Integer getLength(){return this.length;}
    public void setLength(Integer newLength){this.length = newLength;}

    public String getColor(){return this.color;}
    public void setColor(String newColor){this.color = newColor;}
    public String toString(){return "toString not implemented in this class!";}

}
