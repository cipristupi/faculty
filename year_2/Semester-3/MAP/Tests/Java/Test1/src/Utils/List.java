package Utils;

/**
 * Created by Ciprian on 10/29/2014.
 */
public class List<TEntity> implements IList<TEntity> {
    private TEntity elem[];
    private int capacity;
    private int size;
    public List()
    {
        capacity=10;
        //elem = new TEntity[capacity];
        elem = (TEntity[]) new Object[capacity];
    }

    private void resize()
    {
        TEntity[] tmp =(TEntity[]) new Object[capacity];
        for(int i=0;i<size;i++)
        {
            tmp[i]=elem[i];
        }
        elem = tmp;
        capacity = capacity*2;
    }

    public void add(TEntity el)
    {
        if(size == capacity)
        {
            resize();
        }
        elem[size]=el; // == elem[top++]=el;
        size++;
    }

    public boolean isEmpty()
    {
        return size==0;
    }


    public TEntity getAt(int i)
    {
        for(int j=0;j<size;j++)
        {
            if(j==i)
                return elem[i];
        }
        return null;
    }

    public int getSize()
    {
        return size;
    }
}

