package Controller;

import Models.Flower;
import Repository.*;
import Utils.List;

/**
 * Created by Ciprian on 10/29/2014.
 */
public class Controller {
    IRepository r;
    public Controller(Repository repo)
    {
        r=repo;
    }


    public List<Flower> getByLengty(Integer len, String color)
    {
        List<Flower> l = r.getFlowers();
        List<Flower> l2 =new List<Flower>();
        for(int i=0;i<l.getSize();i++)
        {
            if(l.getAt(i).getLength()>len  && l.getAt(i).getColor().equals(color))
            {
                l2.add(l.getAt(i));
            }
        }
        return l2;
    }
}
