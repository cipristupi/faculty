package Repository;

import Models.Flower;
import Utils.List;

/**
 * Created by Ciprian on 10/29/2014.
 */
public interface IRepository {
    void addFlower(Flower f);

     List<Flower> getFlowers();

}
