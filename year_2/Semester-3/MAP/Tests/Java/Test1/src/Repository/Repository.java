package Repository;

import Models.Flower;
import Utils.List;

/**
 * Created by Ciprian on 10/29/2014.
 */
public class Repository implements IRepository {
    List<Flower> l;
    public Repository()
    {
        l=new List<Flower>();
    }
    public void addFlower(Flower f)
    {
        l.add(f);
    }
    public List<Flower> getFlowers()
    {
        return l;
    }
}
