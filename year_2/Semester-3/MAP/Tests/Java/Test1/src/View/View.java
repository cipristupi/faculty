package View;

import Controller.Controller;
import Models.Flower;
import Utils.List;

import java.util.Scanner;

/**
 * Created by Ciprian on 10/29/2014.
 */
public class View {

    private Controller ctr;
    public View(Controller c)
    {
        ctr=c;
    }

    public void run()
    {
        Scanner kb = new Scanner(System.in);
        System.out.println("Length: ");
        int len = kb.nextInt();


        Scanner scanIn = new Scanner(System.in);
        System.out.println("Color: ");
        String color = scanIn.nextLine();
        List<Flower> l =ctr.getByLengty(len,color);
        for(int i=0;i < l.getSize();i++)
        {
            System.out.println(l.getAt(i).toString());
        }
    }

}
