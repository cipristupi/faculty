package com.company;

import Controller.Controller;
import Models.Rose;
import Models.Tulip;
import Models.Violet;
import Repository.*;
import View.View;
public class Main {

    public static void main(String[] args) {
	// write your code here
         Repository r=new Repository();
        r.addFlower(new Violet(10,"red"));
        r.addFlower(new Tulip(10,"red"));
        r.addFlower(new Rose(20,"yellow"));
        r.addFlower(new Violet(15,"black"));
        r.addFlower(new Tulip(19,"yellow"));
        r.addFlower(new Violet(112,"red"));
        r.addFlower(new Violet(10,"blue"));
        r.addFlower(new Violet(14,"blue"));
        Controller ctr =new Controller(r);
        View v =new View(ctr);
        v.run();
    }
}
