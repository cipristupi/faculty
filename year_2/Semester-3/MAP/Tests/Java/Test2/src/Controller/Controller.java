package Controller;

import Model.Toy;
import Repository.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ciprian on 1/7/2015.
 */
public class Controller {
    IRepository r;
    public Controller(Repository repo)
    {
        r=repo;
    }


    public List<Toy> getByColor( String color)
    {
        List<Toy> l = r.GetAllToys();
        List<Toy> l2 =  new ArrayList<Toy>();
        for(Toy t : l)
        {
            if(t.getColor().equals(color))
            {
                l2.add(t);
            }
        }
        return l2;
    }
}
