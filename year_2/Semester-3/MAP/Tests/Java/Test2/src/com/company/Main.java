package com.company;
import Controller.Controller;

import Repository.*;
import View.View;

public class Main {

    public static void main(String[] args) {
        String path = System.getProperty("user.dir")+"/outputs/test.txt";
        Repository r=new Repository(path);
        Controller ctr =new Controller(r);
        View v =new View(ctr);
        v.run();
    }
}
