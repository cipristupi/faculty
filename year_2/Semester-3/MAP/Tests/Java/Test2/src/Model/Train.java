package Model;

/**
 * Created by Ciprian on 1/7/2015.
 */
public class Train extends Toy {
    public Train(String type,String color)
    {
        setColor(color);
        setType(type);
    }

    public String toString()
    {
        return "Train "+getColor() +"  ";
    }
}
