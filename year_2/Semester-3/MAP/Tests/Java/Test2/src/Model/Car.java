package Model;

/**
 * Created by Ciprian on 1/7/2015.
 */
public class Car  extends Toy {
    private String model;
    private int year;
    public Car(String type,String color)
    {
        setColor(color);
        setType(type);
    }
    public Car(String type,String color,String model,int year)
    {
        setColor(color);
        setType(type);
        this.model = model;
        this.year = year;
    }

    public String toString()
    {
        return "Car "+getColor() +"  ";
    }
}
