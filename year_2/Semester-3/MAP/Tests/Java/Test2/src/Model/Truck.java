package Model;

/**
 * Created by Ciprian on 1/7/2015.
 */
public class Truck extends Toy {
    private int year;
    public Truck(String type,String color)
    {
        setColor(color);
        setType(type);
    }
    public Truck(String type,String color,int year)
    {
        setColor(color);
        setType(type);
        this.year = year;
    }

    public String toString()
    {
        return "Truck "+getColor() +"  ";
    }
}
