package Model;

/**
 * Created by Ciprian on 1/7/2015.
 */
public  abstract class Toy {
    private String color;
    private String type;

    public String getColor(){return this.color;}
    public void setColor(String newColor){this.color = newColor;}

    public String getType(){return this.type;}
    public void setType(String newType){this.type = newType;}
    public String toString(){return "toString not implemented in this class!";}
}
