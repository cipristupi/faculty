package View;

import java.util.List;
import java.util.Scanner;
import Controller.Controller;
import Model.Toy;

/**
 * Created by Ciprian on 1/7/2015.
 */
public class View {
    private Controller ctr;
    public View(Controller c)
    {
        ctr=c;
    }

    public void run()
    {
        Scanner scanIn = new Scanner(System.in);
        System.out.println("Color: ");
        String color = scanIn.nextLine();
        List<Toy> l =ctr.getByColor(color);
        for(Toy t : l)
        {
            System.out.println(t.toString());
        }
    }
}
