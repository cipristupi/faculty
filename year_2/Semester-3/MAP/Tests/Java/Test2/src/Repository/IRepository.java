package Repository;

import Model.Toy;

import java.util.List;

/**
 * Created by Ciprian on 1/7/2015.
 */
public interface IRepository {
    public void ReadFromFile();
    public List<Toy> GetAllToys();
}
