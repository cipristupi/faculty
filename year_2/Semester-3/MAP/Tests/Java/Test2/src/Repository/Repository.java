package Repository;

import Model.Car;
import Model.Toy;
import Model.Train;
import Model.Truck;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ciprian on 1/7/2015.
 */
public class Repository implements IRepository {
    private List<Toy> toyList;
    private String pathToFile;
    public Repository(String path)
    {
        pathToFile = path;
        toyList  =  new ArrayList<Toy>();
        this.ReadFromFile();
    }
    @Override
    public void ReadFromFile() {
        try
        {
        BufferedReader br = new BufferedReader(new FileReader(pathToFile));
        String line;

        while ((line = br.readLine()) != null) {
            System.out.println(line);
            String[] lineSplitted = line.split("[$]");//t.getColor().equals
                if(lineSplitted[0].equals("Car") && lineSplitted.length==4)
                {
                    toyList.add(new Car(lineSplitted[0],lineSplitted[1],lineSplitted[2],Integer.parseInt(lineSplitted[3])));
                }

                if(lineSplitted[0].equals("Train"))
                {
                    toyList.add(new Train(lineSplitted[0],lineSplitted[1]));
                }

                if(lineSplitted[0].equals("Truck")) {
                    toyList.add(new Truck(lineSplitted[0], lineSplitted[1], Integer.parseInt(lineSplitted[2])));
                }
        }
        br.close();
        }catch(IOException i)
        {
            i.printStackTrace();
        }
    }

    @Override
    public List<Toy> GetAllToys() {
        return toyList;
    }
}
