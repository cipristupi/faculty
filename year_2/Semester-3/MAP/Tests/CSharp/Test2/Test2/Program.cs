﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test2.Model;
using Test2.Repository;
using System.IO;
using System.Reflection;
using Test2.Controller;
using Test2.View;

namespace Test2
{
    class Program
    {
        static void Main(string[] args)
        {
            string uriPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase) + "test" + ".txt";
            MainRepository r = new MainRepository(uriPath);
            r.AddParticipant(new Actor(50, "Popescu Ion"));
            r.AddParticipant(new Musician(50,"Gigel","piano"));
            r.AddParticipant(new Dancer(30, "Gigel", "waltz,salsa"));
            MainController ctr = new MainController(r);
            MainView v = new MainView(ctr);
            v.run();
        }
    }
}
