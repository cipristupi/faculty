﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test2.Controller;
using Test2.Model;

namespace Test2.View
{
    class MainView
    {
        MainController ctr;
        public MainView(MainController c)
        {
            ctr = c;
        }

        public void run()
        {
            int age;
            Console.Write("Age: ");
            age = int.Parse(Console.ReadLine());

            List<Participant> l = ctr.WriteByAge(age);
            foreach(Participant p in l)
            {
                Console.WriteLine(p.ToString());
            }
            Console.ReadLine();
        }
    }
}
