﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test2.Model;

namespace Test2.Repository
{
    interface IMainRepository
    {
        void AddParticipant(Participant p);
        void WriteToFile(List<Participant> l);
        List<Participant> GetAllParticipants();
    }
}
