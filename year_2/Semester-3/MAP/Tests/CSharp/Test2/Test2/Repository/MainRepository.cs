﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test2.Model;

namespace Test2.Repository
{
    class MainRepository : IMainRepository
    {
        private List<Participant> listOfParticipants;
        private string pathToFile;
        public MainRepository(string path)
        {
            pathToFile = path;
            listOfParticipants = new List<Participant>();
        }
        public void AddParticipant(Participant p)
        {
            listOfParticipants.Add(p);
        }

        public void WriteToFile(List<Participant> l)
        {
            //string uriPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase) + "\\outputs\\" + "prgState" + ".txt";
            string filePath = new Uri(pathToFile).LocalPath;
            using (StreamWriter file = new StreamWriter(filePath))//append new line at the end of file
            {
                foreach (Participant p in l)
                {
                    file.WriteLine(p.ToString() +"\n");
                }
                file.Flush();
            }
        }

        public List<Participant> GetAllParticipants()
        {
            return listOfParticipants;
        }
    }
}
