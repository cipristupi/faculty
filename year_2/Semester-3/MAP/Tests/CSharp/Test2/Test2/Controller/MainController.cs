﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test2.Model;
using Test2.Repository;

namespace Test2.Controller
{
    class MainController
    {
        IMainRepository r;
        public MainController(MainRepository repo)
        {
            r = repo;
        }

        public List<Participant> WriteByAge(int age)
        {
            List<Participant> l = r.GetAllParticipants();
            List<Participant> l1 = new List<Participant>();
            foreach(Participant p in l)
            {
                if(p.Age > age)
                {
                    l1.Add(p);
                }
            }
            r.WriteToFile(l1);
            return l1;
        }
    }
}
