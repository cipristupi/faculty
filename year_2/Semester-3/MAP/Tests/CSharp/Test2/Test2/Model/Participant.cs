﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test2.Model
{
    abstract class Participant
    {
        private int age;
        public int Age { 
            set { age = value; }
            get { return age; }
        }

        private string name;
        public string Name
        {
            set { name = value;}
            get { return name; }
        }
    }
}
