﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test2.Model
{
    class Dancer : Participant
    {
        private string dances;
        public Dancer(int age,string name,string d)
        {
            this.Age = age;
            this.Name = name;
            dances = d;

        }
        public Dancer(int age , string name)
        {
            this.Age = age;
            this.Name = name;
        }

        public override string ToString()
        {
            if(dances == null)
                return "Dancer:" + this.Name + ":" + this.Age;
            else
                return "Dancer:" + this.Name + ":" + dances + ":" + this.Age;
        }
    }
}
