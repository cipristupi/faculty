﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test2.Model
{
    class Actor : Participant
    {
        public Actor(int age,string name)
        {
            this.Age = age;
            this.Name = name;
        }

        public override string ToString()
        {
            return "Actor:" + this.Name + ":" + this.Age.ToString();
        }
    }
}
