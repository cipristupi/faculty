﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test2.Model
{
    class Musician : Participant
    {
        string instruments;
        public Musician(int age, string name, string instr)
        {
            this.Age = age;
            this.Name = name;
            instruments = instr;
        }
        public override string ToString()
        {
            if(instruments == null)
                return "Musician:" + this.Name + ":" + this.Age.ToString();
            else
                return "Musician:" + this.Name + ":" + instruments + ":" + this.Age.ToString();
        }
    }
}
