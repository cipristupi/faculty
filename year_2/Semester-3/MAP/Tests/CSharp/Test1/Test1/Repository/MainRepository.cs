﻿using System;
//using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test1.Controller;
using Test1.Utils;

namespace Test1.Repository
{
    class MainRepository : IMainRepository
    {
        List<Children> l;
        public MainRepository()
        {
            l=new List<Children>();
        }

        public void AddChildren(Children c)
        {
            l.add(c);
        }
        public List<Children> GetAllChildren()
        {
            return l;
        }

    }
}
