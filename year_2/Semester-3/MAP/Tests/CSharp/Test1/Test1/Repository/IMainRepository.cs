﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test1.Controller;
using Test1.Utils;

namespace Test1.Repository
{
    interface IMainRepository
    {
        void AddChildren(Children c);
         List<Children> GetAllChildren();
    }
}
