﻿using System;
//using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test1.Controller;
using Test1.Utils;

namespace Test1.View
{
    class MainView
    {
        MainController ctr;
        public MainView(MainController c)
        {
            ctr = c;
        }

        public void run()
        {
            int age;
            Console.Write("Age: ");
            age = int.Parse(Console.ReadLine());

            List<Children> l = ctr.getByAge(age);
            for(int i=0;i<l.getSize();i++)
            {
                Console.WriteLine(l.getAt(i).ToString());
            }
            Console.ReadLine();
        }
    }
}
