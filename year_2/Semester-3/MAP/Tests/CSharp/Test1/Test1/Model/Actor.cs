﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test1.Controller;

namespace Test1.Model
{
    class Actor : Children
    {

        public Actor(int age)
        {
            base.setAge(age);
        }
        public override string ToString()
        {
            return "Actor age " + base.getAge().ToString();
        }
    }
}
