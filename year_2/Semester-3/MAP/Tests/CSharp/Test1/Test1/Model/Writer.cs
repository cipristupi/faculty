﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test1.Controller;
using Test1.Repository;

namespace Test1.Model
{
    class Writer : Children
    {

        public Writer(int age)
        {
            base.setAge(age);
        }


        public override string ToString()
        {
            return "Writer age " + base.getAge().ToString();
        }
    }
}
