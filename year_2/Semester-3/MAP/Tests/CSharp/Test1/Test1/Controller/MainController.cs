﻿using System;
//using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test1.Repository;
using Test1.Utils;

namespace Test1.Controller
{
    class MainController
    {
        IMainRepository r;
        public MainController(MainRepository repo)
        {
            r = repo;
        }

        public List<Children> getByAge(int age)
        {
            List<Children> l = r.GetAllChildren();
            List<Children> l1 = new List<Children>();
            for(int i=0;i<l.getSize();i++)
            {
                if(l.getAt(i).getAge() > age)
                {
                    l1.add(l.getAt(i));
                }
            }
            return l1;
        }
    }
}
