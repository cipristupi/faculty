﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test1.Controller;
using Test1.Model;
using Test1.Repository;
using Test1.View;

namespace Test1
{
    class Program
    {
        static void Main(string[] args)
        {
            MainRepository r = new MainRepository();
            r.AddChildren(new Actor(10));
            r.AddChildren(new Writer(10));
            r.AddChildren(new Painter(20));
            r.AddChildren(new Painter(15));
            r.AddChildren(new Actor(19));
            r.AddChildren(new Painter(112));
            r.AddChildren(new Writer(10));
            r.AddChildren(new Painter(14));
            MainController ctr = new MainController(r);
            MainView v = new MainView(ctr);
            v.run();
        }
    }
}
