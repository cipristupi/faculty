package utils.MyList;

/**
 * Created by Ciprian on 10/15/2014.
 */
public interface IList<TEntity> {
    void add(TEntity el);
    boolean isEmpty();
    TEntity getAt(int i);
    int getSize();
}
