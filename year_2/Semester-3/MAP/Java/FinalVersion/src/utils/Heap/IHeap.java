package utils.Heap;

/**
 * Created by Ciprian on 1/26/2015.
 */
public interface IHeap {
    int New(int value);
    int GetAt(int index);
}
