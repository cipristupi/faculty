package utils.Heap;

/**
 * Created by Ciprian on 1/26/2015.
 */
public class Heap implements IHeap {
    private int[] heap;
    public Heap()
    {
        heap = new int[50];
    }
    @Override
    public int New(int value) {
        int pos = 0;
        for (int i = 0; i < 50; i++)
        {
                heap[i] = value;
                pos = i;
                break;
        }
        return pos;
    }

    public int Count()
    {
        return heap.length;
    }
    @Override
    public int GetAt(int index) {
        return heap[index];
    }
}
