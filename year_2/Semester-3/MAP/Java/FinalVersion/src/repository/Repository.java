package repository;

import models.Statements.Statement;

import java.io.*;
import java.util.*;
/*import utils.MyDictionary.Dictionary;
import utils.MyList.List;*/

/**
 * Created by Ciprian on 10/26/2014.
 */
public class Repository {
    private List<Statement> programList;
    private List<PrgState> prgstateList;

    public Repository()
    {
        prgstateList = new ArrayList<PrgState>();
        programList = new ArrayList<Statement>();
    }

    /**
     *
     * @param s - statement to be added into program state
     * @param i - index of program state where the statement to be added
     */
    public void addToState(Statement s, int i)
    {
        prgstateList.get(i).getStack().push(s);
    }
    public void addStatement(Statement statement)
    {
        programList.add(statement);
    }

    public void addPrgState(Statement initialState)
    {
        prgstateList.add(new PrgState(initialState));
    }

    public List<PrgState> getPrgStateList()
    {
        /*List<PrgState> stmtList = new ArrayList<PrgState>();
        List<Integer> keys = ( ArrayList<Integer>) prgstateList.keys();
        for (int i = 0; i < keys.size(); i++)
        {
            stmtList.add(prgstateList.get(keys.get(i)));
        }
        return stmtList;*/
        return prgstateList;
    }

    public List<Statement> getStatementList()
    {
        return programList;
    }

    public void NewPrgState(int id)
    {
        Statement s = programList.get(id);
        PrgState p  =new PrgState(s);
        prgstateList.add(p);
        int last = prgstateList.size() - 1;
        p.setId(last);
    }

    public PrgState getPrgStateById(int i)
    {
        return prgstateList.get(i);
    }

    public void addFork(Statement s, PrgState parent)
    {
        prgstateList.add(new PrgState(s));
        int last = prgstateList.size() - 1;
        PrgState f = prgstateList.get(last);
        f.setHeap(parent.getHeap());
        f.copyTable((Map<String, Integer>) parent.getTable());
        f.setOut(parent.getOut());
        f.setId(last);
    }

    public boolean programExists(int id)
    {
        //return prgstateList.searchKey(id) != null ? true : false;
        return prgstateList.get(id) != null ? true : false;
    }

    public int getProgramsNumber()
    {
        return prgstateList.size();
    }

    /*public  Boolean checkProgramStackIsEmpty(int i)
    {
        if (getPrgStateById(i).getStack().size() == 0)
        {
            prgstateList.remove(i);
            return false;
        }
        return true;
    }*/

    public boolean checkPrgStateExists(int id)
    {
        return prgstateList.get(id) != null ?true :false;
    }

    public boolean prgStateStackIsEmpty(int id)
    {
        return this.getPrgStateById(id).getStack().empty();
    }

    public void removePrgState(int id)
    {
        prgstateList.remove(id);
    }

    //region  Serialization and File operations
    public void Serialize(int programId)
    {
        PrgState p =this.getPrgStateById(programId);
        try
        {
            FileOutputStream fileOut =
                    new FileOutputStream(System.getProperty("user.dir")+"/outputs/"+programId +".txt");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(p);
            out.close();
            fileOut.close();
            System.out.printf("Serialized data is saved in "+System.getProperty("user.dir")+"/outputs/"+programId +".txt");
        }catch(IOException i)
        {
            i.printStackTrace();
        }
    }


    public PrgState Deserialize(int programId)
    {
        PrgState e = null;
        try
        {
            FileInputStream fileIn = new FileInputStream(System.getProperty("user.dir")+"/outputs/"+programId +".txt");
            ObjectInputStream in = new ObjectInputStream(fileIn);
            e = (PrgState) in.readObject();
            in.close();
            fileIn.close();
            return  e;
        }catch(IOException i)
        {
            i.printStackTrace();
            return null;
        }catch(ClassNotFoundException c)
        {
            c.printStackTrace();
            return null;
        }
    }

    public  void saveToFile(int programId)
    {
        PrgState r = this.getPrgStateById(programId);
        this.saveToFile(r);
    }

    public  void saveToFile(PrgState r)
    {
        String filePath = System.getProperty("user.dir")+"/outputs/" +"prgState.txt";
        PrintWriter out = null;
        try {
            //out = new PrintWriter(filePath);
            out = new PrintWriter(new BufferedWriter(new FileWriter(filePath, true)));
            out.println(r.toString());
            out.close();
        } catch (IOException  e) {
            e.printStackTrace();
        }

    }

    public  void saveToFile(PrgState r, Integer step)
    {
        String filePath = System.getProperty("user.dir")+"/outputs/" +"prgState.txt";
        PrintWriter out = null;
        StringBuilder separator = new StringBuilder(50);
        for (int i = 0; i < 50; ++i) {
            separator.append("=");
        }
        try {
            //out = new PrintWriter(filePath);
            out = new PrintWriter(new BufferedWriter(new FileWriter(filePath, true)));
            out.println(separator + "  Step "+ step + "   "+separator+"\n");
            out.println(r.toString());
            out.close();
        } catch (IOException  e) {
            e.printStackTrace();
        }

    }
    //endregion
}
