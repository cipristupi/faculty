package repository;

import com.sun.xml.internal.ws.util.StringUtils;
import models.Statements.Statement;
import utils.Heap.Heap;
/*import utils.MyDictionary.Dictionary;
import utils.MyList.List;
import utils.MyStack.Stack;*/

import java.util.*;
import java.util.Dictionary;
import java.util.List;
import java.util.Stack;

/**
 * Created by Ciprian on 10/25/2014.
 */
public class PrgState implements java.io.Serializable {
    //region Global Variables
    private Dictionary<String, Integer> tableOfSymbols;
    private Stack<Statement> exeStack;
    private List<String> outBuff;
    private Heap heap;
    private Statement initialStmt;
    private int Id =-1;
    //endregion

    //region Setters and Getters
    public Dictionary<String, Integer> getTable() {
        return tableOfSymbols;
    }

    public Stack getStack() {
        return exeStack;
    }

    public List<String> getOut() {
        return outBuff;
    }

    public Heap getHeap() {
        return heap;
    }

    public void setHeap(Heap newHeap) {
        this.heap = newHeap;
    }

    public Statement getInitialStmt() {
        return this.initialStmt;
    }

    public void setOut(List<String> n) {
        outBuff = n;
    }

    public void setId(int i) {
        this.Id = i;
    }

    public int getId() {
        return this.Id;
    }
    //endregion

    //region Constructors
    public PrgState(Statement initialStatement) {
        outBuff = new ArrayList<String>();
        exeStack = new Stack<Statement>();
        tableOfSymbols = new Hashtable<String, Integer>();
        heap = new Heap();
        exeStack.push(initialStatement);
    }

    public PrgState(Dictionary<String, Integer> t, Stack<Statement> s, List<String> o, Heap h) {
        this.outBuff = o;
        this.exeStack = s;
        this.tableOfSymbols = t;
        this.heap = h;
    }
    //endregion

    public void AddStatement(Statement s) {
        exeStack.push(s);
    }

    //region Utils
    public void copyTable(Map<String, Integer> s) {
        for (String key : s.keySet())
            tableOfSymbols.put(key, s.get(key));
    }

    public String toString() {
        String o = "";
        List<Statement> stackList = new ArrayList<Statement>();
        Stack<Statement> stack = this.getStack();
        Enumeration symTableKeys = this.getTable().keys();

        StringBuilder separator = new StringBuilder(50);
        for (int i = 0; i < 50; ++i) {
            separator.append("=");
        }

        List<String> outBuff = (ArrayList<String>) this.getOut();
        o+=separator+ "   Thread " +  this.getId() +"   " + separator+"\n";
        o += "//Stack content \n";
        for (Statement st : stack) {
            o += st + "\n";
        }
        o += "\n";
        o += "//SymTable content \n";
        while (symTableKeys.hasMoreElements()) {

            // Get the key (convert to string)
            String strKey = (String) symTableKeys.nextElement();
            // Use key to fetch the item and print out its value
            o += strKey + "->" + this.getTable().get(strKey).toString() + "\n";
        }
        o += "\n";
        o += "//Out content \n";
        for (String s : outBuff) {
            o += s + "\n";
        }


        o += separator;
        o += "\n";
        return o;
    }
//endregion
}
