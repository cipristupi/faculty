package controller;

import models.Expressions.*;
import models.Statements.*;
import repository.PrgState;
import repository.Repository;
import utils.ObserverPattern.Subject;


import java.util.List;
import java.util.Map;
import java.util.Stack;
/*import utils.MyDictionary.Dictionary;
import utils.MyStack.Stack;
import utils.*;
import utils.MyList.List;*/

/**
 * Created by Ciprian on 10/18/2014.
 */
public class Controller {
    private Repository repo;
    private PrgState currentPrg;

    public Controller(Repository r,Subject s)
    {
        repo=r;
    }

    public PrgState getCurrentPrg(){return this.currentPrg;}

    public PrgState parallelRun(Integer programID)
    {
        int programNumber;
        repo.NewPrgState(programID);
        while (true)
        {
            programNumber = repo.getProgramsNumber();
            if (programNumber == 0)
                break;

            for (int i = 0; i < programNumber; i++)
            {
                if (repo.prgStateStackIsEmpty(i)) {
                    repo.removePrgState(i);
                    programNumber--;
                } else {
                    this.runStepByStep(i);
                }
            }
        }
        return currentPrg;
    }
    public PrgState parallelRun(Integer programID ,boolean printFile)
    {
        int programNumber;
        repo.NewPrgState(programID);
        while (true)
        {
            programNumber = repo.getProgramsNumber();
            if (programNumber == 0)
                break;

            for (int i = 0; i < programNumber; i++)
            {
                if (repo.prgStateStackIsEmpty(i)) {
                    repo.removePrgState(i);
                    programNumber--;
                } else {
                    this.runStepByStep(i,printFile);
                }
            }
        }
        return currentPrg;
    }

    public PrgState run(int programID)
    {
        if(!repo.programExists(programID))
        {
            return null;
        }
        currentPrg = repo.getPrgStateById(programID);
        while(!currentPrg.getStack().isEmpty())
        {
            runStepByStep(programID);
        }
        return currentPrg;
    }

    public PrgState runStepByStep(Integer programID)
    {
        if (!repo.programExists(programID))
        {
            return null;
        }
        InitPrgState(programID);
        Statement st = (Statement) GetCurrentStack(programID).pop();
        evalStatement(st);
        return currentPrg;
    }

    public PrgState runStepByStep(Integer programID,boolean printFile)
    {
        if (!repo.programExists(programID))
        {
            return null;
        }
        InitPrgState(programID);
        Statement st = (Statement) GetCurrentStack(programID).pop();
        evalStatement(st);
        if(printFile ==true)
            repo.saveToFile(currentPrg);
        return currentPrg;
    }

    private void evalStatement(Statement st) {
        if (st instanceof CompoundStmt)
        {
            execCompound((CompoundStmt)st);
        }
        else
            if (st instanceof IfStmt)
            {
                execIf((IfStmt)st);
            }
            else
                if (st instanceof PrintStmt)
                {
                    execPrint((PrintStmt)st);
                }
                else
                    if (st instanceof AssignStmt)
                    {
                        execAssign((AssignStmt)st);
                    }
                    else
                        if(st instanceof ForkStmt)
                        {
                            execFork((ForkStmt)st);
                        }
                        else
                            if (st instanceof DecStmt)
                            {
                               execDec((DecStmt) st);
                            }
                            else
                                if (st instanceof IncStmt)
                                {
                                        execInc((IncStmt) st);
                                }
    }

    //region Execution of statements
    private void execInc(IncStmt st) {
        currentPrg.getTable().put(st.Name(), 1 + currentPrg.getTable().get(st.Name()));
    }

    private void execDec(DecStmt st) {
        currentPrg.getTable().put(st.Name(),currentPrg.getTable().get(st.Name())-1);
    }

    private void execFork(ForkStmt st) {
        Statement x = st.getFork();
        repo.addFork(x, currentPrg);
    }



    private void execAssign(AssignStmt st) {
        Expression expr=st.getExpr();
        String varName = st.getName();
        if(expr instanceof ConstExpr) {
            int value = ((ConstExpr) expr).getConstant();
            currentPrg.getTable().put(varName, value);
        } else if(expr instanceof VarExpr) {
            String id = ((VarExpr) expr).getName();
            int value = currentPrg.getTable().get(id);
            currentPrg.getTable().put(st.getName(), value);
        } else if(expr instanceof ArithExpr) {
            int value = ((ArithExpr) expr).eval(currentPrg.getTable(),currentPrg.getHeap());
            currentPrg.getTable().put(st.getName(), value);
        }else if(expr instanceof RExpr){
            int value = ((RExpr)expr).eval(currentPrg.getTable(), currentPrg.getHeap());
            currentPrg.getTable().put(varName, value);
        }else if(expr instanceof NewExpr) {
            int value = ((NewExpr)expr).eval(currentPrg.getHeap());
            currentPrg.getTable().put(varName, value);
        }else
            if(expr instanceof  LogicalExpr)
            {
                int value = ((LogicalExpr)expr).eval(currentPrg.getTable(),currentPrg.getHeap());
                currentPrg.getTable().put(varName, value);
            }
    }

    private void execPrint(PrintStmt st) {
        Expression toPrint = st.getExpr();
        try {
            int result = 0;
            if (toPrint instanceof ConstExpr) {
                result = ((ConstExpr) toPrint).getConstant();
            } else if (toPrint instanceof VarExpr) {
                String id = ((VarExpr) toPrint).getName();
                result = currentPrg.getTable().get(id);
            } else if (toPrint instanceof ArithExpr) {
                result = ((ArithExpr) toPrint).eval(currentPrg.getTable(),currentPrg.getHeap());
            }else if(toPrint instanceof RExpr){
                result = ((RExpr)toPrint).eval(currentPrg.getTable(), currentPrg.getHeap());
            }else if(toPrint instanceof  NewExpr) {
                result = ((NewExpr)toPrint).eval(currentPrg.getHeap());
            }else if(toPrint instanceof  LogicalExpr){
                result = ((LogicalExpr)toPrint).eval(currentPrg.getTable(), currentPrg.getHeap());
            }
            String x = Integer.toString(result);
            currentPrg.getOut().add(x);
            //consoleX.printLastMessageInBuffer();
        } catch(NumberFormatException ex) {
            currentPrg.getOut().add(ex.toString());
            //consoleX.printLastMessageInBuffer();
        }
    }

    private void execIf(IfStmt st) {
        Expression expr=st.getExpr();
        int value;
        if(expr instanceof ConstExpr) {
            value = ((ConstExpr) expr).getConstant();
        }
        else
        if(expr instanceof VarExpr) {
            String id = ((VarExpr) expr).getName();
            value = currentPrg.getTable().get(id);
        }
        else
            if(expr instanceof ArithExpr)
            {
                value = ((ArithExpr) expr).eval(currentPrg.getTable(),currentPrg.getHeap());
            }else
                if (expr instanceof NewExpr)
                {
                    value = ((NewExpr)expr).eval(currentPrg.getHeap());
                }
                else
                    if (expr instanceof RExpr)
                    {
                        value = ((RExpr)expr).eval(currentPrg.getTable(), currentPrg.getHeap());
                    }
                    else
                        if(expr instanceof  LogicalExpr)
                        {
                            value = ((LogicalExpr)expr).eval(currentPrg.getTable(), currentPrg.getHeap());
                        }
                    else
                    {
                        value = 0;
                    }

        if(value != 0)
        {
            currentPrg.getStack().push(st.getSt1());
        }
        else
        {
            currentPrg.getStack().push(st.getSt2());
        }
        //subject.notifyObserver(stackObserver,currentPrg);//Observer
    }

    private void execCompound(CompoundStmt st) {
        currentPrg.getStack().push(st.getStmt2());
        currentPrg.getStack().push(st.getStmt1());
    }
    //endregion

    //region Utils
    private void InitPrgState(Integer programID) {
        currentPrg = repo.getPrgStateById(programID);
    }
    //endregion

    public int getPrgStateNumber()
    {
        return repo.getProgramsNumber();
    }

    public List<Statement> getAllStatements()
    {
        return repo.getStatementList();
    }



    public PrgState getPrgStateById(int i) {
        return repo.getPrgStateById(i);
    }


    public PrgState GetCurrentPrgState()
    {
        return this.currentPrg;
    }

    public Stack<Statement> GetCurrentStack(int programID)
    {
        InitPrgState(programID);
        return currentPrg.getStack();
    }

    public void removePrgState(int programID)
    {
        repo.removePrgState(programID);
    }
    public void addStatement(Statement s)
    {
        repo.addStatement(s);
    }

    public void  NewPrgState(int programID)
    {
        repo.NewPrgState(programID);
    }
    public void printTable(PrgState prgState, boolean isStep, int threadId)
    {
        String exeStack , outBuff ,symTable , heap ;
        //string separator = "";
        String leftAlignFormat = "| %-50s | %-50s | %-50ss | %-50s%n";

        System.out.format("+-----------------+------+%n");
        System.out.printf("| Column name     | ID   |%n");
        System.out.format("+-----------------+------+%n");
        System.out.format("+-----------------+------+%n");
        exeStack = outBuff = symTable = heap = "";
        if (threadId == -1)
        {
            exeStack = outBuff = symTable = heap = "";
        }
        else
        {
            //exeStack = outBuff = symTable = heap = "[Thread "+((int)thread).ToString()+"]\n";
        }
        //Stack content;
        Stack<Statement> stack = prgState.getStack();
        for(Statement st : stack)
        {
            exeStack += st.toString() + "\n";
        }
        //SymTable
        Map<String, Integer> s = (Map<String, Integer>) prgState.getTable();
        for(String key : s.keySet()) {
            symTable+=key +"->"+ s.get(key);
        }
        //Out content
        for (String st : prgState.getOut())
        {
            outBuff += st + "\n";
        }
        //Heap content
        for (int i = 0; i < prgState.getHeap().Count(); i++)
        {
            heap += "[" + i + "]=" + prgState.getHeap().GetAt(i) + "\n";
        }


       /* if (isStep)
        {
            separator = string.Concat(Enumerable.Repeat("=", 10));
            separator += "STEP " + step.ToString();
            separator += string.Concat(Enumerable.Repeat("=", 10));
            separator += "\n";
        }
        else
        {
            separator = string.Concat(Enumerable.Repeat("=", 32));
            separator += "\n";
        }*/

        System.out.format(leftAlignFormat, outBuff,exeStack,symTable,heap);
    }

    public boolean prgStateStackIsEmpty(int i) {
        return repo.prgStateStackIsEmpty(i);
    }
}
