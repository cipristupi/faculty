package models.Expressions;

import tests.ExpressionTests;
import utils.Heap.Heap;

import java.util.Dictionary;

/**
 * Created by Ciprian on 1/26/2015.
 */
public class LogicalExpr extends Expression {
    private Expression expr1, expr2;
    private String sign;
    public LogicalExpr(Expression ex1, Expression ex2, String s)
    {
        expr1 = ex1;
        expr2 = ex2;
        sign = s;
    }
    public int eval(Dictionary<String, Integer> s, Heap h) {
        int resultEx1;
        int resultEx2;
        boolean result =false;

        resultEx1 = GetValueExpression(expr1, s, h);
        resultEx2 = GetValueExpression(expr2, s, h);
        if (this.sign.equals("<")) {
            result = resultEx1 < resultEx2;
        } else if (this.sign.equals("<=")) {
            result = resultEx1 <= resultEx2;
        } else if (this.sign.equals(">")) {
            result = resultEx1 > resultEx2;
        } else if (this.sign.equals(">=")) {
            result = resultEx1 >= resultEx2;
        } else if (this.sign.equals("==")) {
            result = resultEx1 == resultEx2;
        } else if (this.sign.equals("!=")) {
            result = resultEx1 != resultEx2;
        }
        if(result == true) {
            return 1;
        }
        return 0;
    }
    private int GetValueExpression(Expression e, Dictionary<String, Integer> s, Heap h)
    {
        if (e instanceof ConstExpr)
        {
            return ((ConstExpr)e).getConstant();
        }
        else
        if (e instanceof VarExpr)
        {
            String key = ((VarExpr)e).getName();
            return s.get(key);
        }
        else
        if (e instanceof ArithExpr)
        {
            return ((ArithExpr)e).eval(s, h);
        }
        else
        if (e instanceof RExpr)
        {
            return ((RExpr)e).eval(s, h);
        }
        else
        if (e instanceof NewExpr)
        {
            return ((NewExpr)e).eval(h);//this can generate heap overflow
        }
        else
        if(e instanceof LogicalExpr)
        {
            return ((LogicalExpr)e).eval(s, h);
        }
        return 0;
    }

    public String toString(){
        String s = "(";
        s += expr1.toString();
        if (sign == "<")
            s += "<";
        if (sign == "<=")
            s += "<=";
        if (sign == ">")
            s += ">";
        if (sign == ">=")
            s += ">=";
        if (sign == "==")
            s += "==";
        if (sign == "!=")
            s += "!=";
        s += expr2.toString();
        s += ")";
        return s;
    }
}
