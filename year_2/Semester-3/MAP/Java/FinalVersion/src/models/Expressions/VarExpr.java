package models.Expressions;

/**
 * Created by Ciprian on 10/18/2014.
 */
public class VarExpr extends Expression {
    private String varName;
    public VarExpr(String v)
    {
        varName = v;
    }
    public String getName()
    {
        return varName;
    }

    public String toString()
    {
        return varName;
    }
}
