package models.Expressions;

import utils.Heap.Heap;

import java.util.Dictionary;

/**
 * Created by Ciprian on 1/13/2015.
 */
public class RExpr extends Expression {
    private String varName;

    public RExpr(String var)
    {
        this.varName = var;
    }

    public Integer eval(Dictionary<String,Integer> s , Heap h)
    {
        int address = s.get(this.varName);
        int valueHeap = h.GetAt(address);
        return valueHeap;
    }

    @Override
    public String toString() {
        return "r(" + varName + ")";
    }
}
