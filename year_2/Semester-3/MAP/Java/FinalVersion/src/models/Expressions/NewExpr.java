package models.Expressions;

import utils.Heap.Heap;

/**
 * Created by Ciprian on 1/13/2015.
 */
public class NewExpr extends  Expression {

    private int val;
    public NewExpr(int v)
    {
        this.val = v;
    }

    public int getValue()
    {
        return val;
    }

    @Override
    public String toString() {
        return "New(" + val + ")";
    }

    public int eval(Heap h) {
        return h.New(this.val);
    }
}

