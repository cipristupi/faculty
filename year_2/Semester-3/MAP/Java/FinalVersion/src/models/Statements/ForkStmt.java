package models.Statements;

/**
 * Created by Ciprian on 1/13/2015.
 */
public class ForkStmt  extends Statement{

    private Statement toFork;

    public ForkStmt(Statement s)
    {
        this.toFork = s;
    }

    public Statement getFork()
    {
        return toFork;
    }

    @Override
    public String toString() {
        return "fork(" + toFork.toString() + ")";
    }
}
