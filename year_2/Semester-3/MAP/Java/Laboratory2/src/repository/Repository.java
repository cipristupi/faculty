package repository;

import models.Statement;

import java.io.*;
import java.util.*;
/*import utils.Dictionary;
import utils.List;*/

/**
 * Created by Ciprian on 10/26/2014.
 */
public class Repository {
   private Dictionary<Integer,PrgState> prgstateList;
    private int n;

    public Repository()
    {
        //prgstateList = new Dictionary<Integer, PrgState>();
        prgstateList = new Hashtable<Integer, PrgState>();
    }

    /**
     *
     * @param s - statement to be added into program state
     * @param i - index of program state where the statement to be added
     */
    public void addToState(Statement s, int i)
    {
       // prgstateList.searchKey(i).getStack().push(s);
        prgstateList.get(i).getStack().push(s);
    }

    public void addPrgState(Statement initialState)
    {
        //prgstateList.add(0,new PrgState(initialState));
        prgstateList.put(0, new PrgState(initialState));
    }

    public List<PrgState> getAllPrgState()
    {
        //List<PrgState> stmtList = new List<PrgState>();
        List<PrgState> stmtList = new ArrayList<PrgState>();
        //List<Integer> keys = prgstateList.getKeys();
        List<Integer> keys = ( ArrayList<Integer>) prgstateList.keys();
        for (int i = 0; i < keys.size(); i++)//for (int i = 0; i < keys.getSize(); i++)
        {
            //stmtList.add(prgstateList.searchKey(keys.getAt(i)));
            stmtList.add(prgstateList.get(keys.get(i)));
        }
        return stmtList;
    }

    public PrgState getPrgStateById(int i)
    {
        //return prgstateList.searchKey(i);
        return prgstateList.get(i);
    }

    public boolean programExists(int id)
    {
        //return prgstateList.searchKey(id) != null ? true : false;
        return prgstateList.get(id) != null ? true : false;
    }

    /*public PrgState getLastPrgState()
    {
        return stmtList.getAt(n);
    }*/

    public void Serialize(int programId)//it's more good to send the program state instade of programId?
    {
        PrgState p =this.getPrgStateById(programId);
        try
        {
            FileOutputStream fileOut =
                    new FileOutputStream(System.getProperty("user.dir")+"/outputs/"+programId +".txt");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(p);
            out.close();
            fileOut.close();
            System.out.printf("Serialized data is saved in "+System.getProperty("user.dir")+"/outputs/"+programId +".txt");
        }catch(IOException i)
        {
            i.printStackTrace();
        }
    }


    public PrgState Deserialize(int programId)
    {
        PrgState e = null;
        try
        {
            FileInputStream fileIn = new FileInputStream(System.getProperty("user.dir")+"/outputs/"+programId +".txt");
            ObjectInputStream in = new ObjectInputStream(fileIn);
            e = (PrgState) in.readObject();
            in.close();
            fileIn.close();
            return  e;
        }catch(IOException i)
        {
            i.printStackTrace();
            return null;
        }catch(ClassNotFoundException c)
        {
            c.printStackTrace();
            return null;
        }
    }

    public  void saveToFile(int programId)
    {
        PrgState r = this.getPrgStateById(programId);
        this.saveToFile(r);
    }

    public  void saveToFile(PrgState r)
    {
        String filePath = System.getProperty("user.dir")+"/outputs/" +"prgState.txt";
        PrintWriter out = null;
        try {
            out = new PrintWriter(filePath);
            out.println(r.toString());
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }
}
