package repository;

import models.Statement;
import utils.*;
/*import utils.Dictionary;
import utils.List;
import utils.Stack;*/

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.*;
import java.util.Dictionary;
import java.util.List;
import java.util.Stack;

/**
 * Created by Ciprian on 10/25/2014.
 */
public class PrgState implements java.io.Serializable
{
    private Dictionary<String,Integer> tableOfSymbols;
    private Stack<Statement> exeStack;
    private List<String> outBuff;

    public PrgState(Statement initialStatement)
    {
        /*tableOfSymbols = new Dictionary<String, Integer>();
        exeStack = new Stack<Statement>();
        outBuff = new List<String>();*/
        outBuff = new ArrayList<String>();
        exeStack = new Stack<Statement>();
        tableOfSymbols = new Hashtable<String, Integer>();
        exeStack.push(initialStatement);
    }

    public Dictionary<String,Integer> getTable()
    {
        return tableOfSymbols;
    }
    public Stack<Statement> getStack()
    {
        return exeStack;
    }
    public  List<String> getOut()
    {
        return outBuff;
    }

    public String toString(){
        String o ="";
        List<Statement> stackList =new  ArrayList<Statement>();
        Stack<Statement> stack = this.getStack();
        Enumeration symTableKeys = this.getTable().keys();
        List<String> outBuff = (ArrayList<String>)this.getOut();
        while(!stack.empty())
        {
            stackList.add(stack.pop());
        }
            o += "//Stack content \n";
            for(int i=stackList.size()-1 ;i>=0;i--) {
                o +=stackList.get(i).toString() +"\n";
            }
            o+="\n";
            o+="//SymTable content \n";
            while(symTableKeys.hasMoreElements()){

                // Get the key (convert to string)
                String strKey = (String)symTableKeys.nextElement();
                // Use key to fetch the item and print out its value
               o+= strKey +"->" + this.getTable().get(strKey).toString() +"\n";
            }
        o+="\n";
            o+="//Out content \n";
            for(String s : outBuff)
            {
                o+=s +"\n";
            }
        return o;
    }

}
