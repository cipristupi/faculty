package models;

/**
 * Created by Ciprian on 10/18/2014.
 */
public class ConstExpr extends Expression {
    private int val;
    public ConstExpr(int v)
    {
        val=v;
    }

    public int getConstant()
    {
        return val;
    }

    public String toString()
    {
        return String.valueOf(val);
    }
}
