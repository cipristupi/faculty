package models;

/**
 * Created by Ciprian on 10/18/2014.
 */
public class IfStmt extends Statement {
    private Expression expr;
    private Statement st1;
    private Statement st2;
    public IfStmt(Expression ex,Statement t, Statement e)
    {
        expr=ex;
        st1=t;
        st2=e;
    }

    public Expression getExpr()
    {
        return expr;
    }
    public Statement getSt1()
    {
        return st1;
    }
    public Statement getSt2()
    {
        return st2;
    }

    public String toString()
    {
        /*String s = "";
        s += "If ";
        s += expr.toString();
        s += " then ";
        s += st1.toString();
        if(st2.toString() != "")
        {
            s += " else ";
            s += st2.toString();
        }
        return s;*/
        return "ifStatement("+expr.toString()+", "+st1.toString()+", "+st2.toString()+")";
    }

    /*public Statement exec(TableOfSymbols s)//TODO move to controller
    {
        int value;
        if(expr instanceof ConstExpr) {
            value = ((ConstExpr) expr).getConstant();
        }
        else
            if(expr instanceof VarExpr) {
                String id = ((VarExpr) expr).getName();
                value = s.getValue(id);
            }
            else
                if(expr instanceof ArithExpr)
                {
                    value = ((ArithExpr) expr).exec(s);
                }
                else
                {
                    value = 0;
                }

        if(value != 0)
        {
            return this.st1;
        }
        else
        {
            return this.st2;
        }
    }*/
}
