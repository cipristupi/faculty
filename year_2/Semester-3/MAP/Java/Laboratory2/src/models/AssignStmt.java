package models;

/**
 * Created by Ciprian on 10/15/2014.
 */
public class AssignStmt extends Statement {
    private String varName;
    private Expression expr;
    public AssignStmt(String varname,Expression e)
    {
        varName = varname;
        expr =e;
    }

    public String getName()
    {
        return varName;
    }

    public Expression getExpr()
    {
        return expr;
    }

    public String toString()
    {
        return varName + " = " + expr.toString();
    }
    /*public void exec(TableOfSymbols s)//TODO move in controller
    {
        if(expr instanceof ConstExpr) {
            int value = ((ConstExpr) expr).getConstant();
            s.addToTable(varName, value);
        } else if(expr instanceof VarExpr) {
            String id = ((VarExpr) expr).getName();
            int value = s.getValue(id);
            s.addToTable(varName, value);
        } else if(expr instanceof ArithExpr) {
            int value = ((ArithExpr) expr).exec(s);
            s.addToTable(varName, value);
        }
    }*/

}
