package models;

//import utils.Dictionary;

import java.util.Dictionary;

/**
 * Created by Ciprian on 10/18/2014.
 */
public class ArithExpr extends Expression {
    private Expression expr1,expr2;
    private char sign;
    public ArithExpr(Expression ex1,Expression ex2, char t)
    {
        expr1=ex1;
        expr2=ex2;
        sign=t;
    }

    public int eval(Dictionary<String,Integer> s)
    {
        int resultEx1;
        int resultEx2;

        resultEx1 = GetValueExpression(expr1,s);
        resultEx2 = GetValueExpression(expr2,s);
        switch (this.sign) {
            case '+':
                return resultEx1 + resultEx2;
            case '-':
                return resultEx1 - resultEx2;
            case '*':
                return resultEx1 * resultEx2;
            case '/':
                return resultEx1 / resultEx2;
        }
        return 0;
    }

    private int GetValueExpression(Expression e,Dictionary<String,Integer> s)
    {
        if(e instanceof ConstExpr) {
            return ((ConstExpr) e).getConstant();
        }
        else
            if(e instanceof VarExpr)
            {
                String id = ((VarExpr) e).getName();
                //return  s.searchKey(id);
                return  s.get(id);
            }
            else
                if(e instanceof ArithExpr) {
                    return ((ArithExpr) e).eval(s);
                }

        return 0;
    }

    public String toString()
    {
        String s = "";
        s += expr1.toString();
        if (sign == '+')
            s += "+";
        if (sign == '-')
            s += "-";
        if (sign == '*')
            s += "*";
        if (sign == '/')
            s += "/";
        s += expr2.toString();
        return s;
    }
}
