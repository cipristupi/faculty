package models;

/**
 * Created by Ciprian on 10/15/2014.
 */
public class PrintStmt extends Statement {
    private Expression expr;
    public PrintStmt(Expression e)
    {
        expr = e;
    }

    public Expression getExpr(){
        return expr;
    }

    public String print()
    {
        return "Print("+expr.toString()+") ";
    }

    public String toString()
    {
        return expr.toString();
    }
    /*public int exec(TableOfSymbols s)//TODO move to controller
    {
        if(expr instanceof ConstExpr) {
            return ((ConstExpr) expr).getConstant();
        }
        else
            if(expr instanceof VarExpr) {
                String id = ((VarExpr) expr).getName();
                return s.getValue(id);
            }
            else
                if(expr instanceof ArithExpr) {
                    return ((ArithExpr) expr).exec(s);
                }
        return 0;
    }*/
}
