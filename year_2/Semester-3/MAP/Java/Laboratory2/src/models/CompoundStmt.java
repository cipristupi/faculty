package models;

/**
 * Created by Ciprian on 10/15/2014.
 */
public class CompoundStmt extends Statement {
    private Statement stmt1;
    private Statement stmt2;
    public CompoundStmt(Statement s1, Statement s2)
    {
        stmt1 =s1;
        stmt2 =s2;
    }

    public Statement getStmt1()
    {
        return stmt1;
    }
    public Statement getStmt2()
    {
        return stmt2;
    }
    public String toString()
    {
        String s = "compoundStatement( ";
        s += stmt1.toString();
        s += ";";
        s += stmt2.toString();
        s += " )";
        return s;
    }
    /*public Statement[] exec()//TODO move to controller
    {
        Statement[] toReturn = new Statement[2];
        toReturn[0] = stmt1;
        toReturn[1] = stmt2;
        return toReturn;
    }*/
}
