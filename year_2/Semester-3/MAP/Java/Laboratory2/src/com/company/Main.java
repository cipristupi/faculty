package com.company;

import controller.Controller;
import models.*;
import repository.Repository;
import tests.ExpressionTests;
import tests.SerializationTests;
import utils.Dictionary;
import utils.List;
import utils.ObserverPattern.OutObserver;
import utils.ObserverPattern.StackObserver;
import utils.ObserverPattern.Subject;
import utils.ObserverPattern.TableObserver;
import utils.Stack;
import view.Console;

import java.util.Observer;

public class Main {

    public static void main(String[] args) {
       // ExpressionTests t =new ExpressionTests();
        //t.run();
        //SerializationTests st = new SerializationTests();
        //st.run();
        //Statement s = new IfStmt(new ConstExpr(1), new PrintStmt(new ConstExpr(0)), new PrintStmt(new ConstExpr(1)));
        Statement s = new CompoundStmt(new AssignStmt("v", new ConstExpr(2)), new PrintStmt(new ArithExpr(new VarExpr("v"), new ConstExpr(3), '+')));
        Repository repo = new Repository();
        Subject sub =new Subject();

        Observer stackObserver =new StackObserver();
        Observer outObserver =new OutObserver();
        Observer tableObserver =new TableObserver();
        sub.addObserver(stackObserver);
        sub.addObserver(outObserver);
        sub.addObserver(tableObserver);
        Controller cntr= new Controller(repo,sub);
        Console console = new Console(cntr);
        console.EntryPoint();
    }
}
