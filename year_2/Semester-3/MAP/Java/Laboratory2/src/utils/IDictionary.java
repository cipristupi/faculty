package utils;

/**
 * Created by Ciprian on 10/15/2014.
 */
public interface IDictionary<TKey,TValue> {
    void add(TKey k, TValue v);
    TValue searchKey(TKey k);
    List<TKey> getKeys();
    void updateValue(TKey k, TValue v);
}
