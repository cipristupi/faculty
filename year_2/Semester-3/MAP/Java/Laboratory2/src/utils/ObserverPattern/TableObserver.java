package utils.ObserverPattern;

import repository.PrgState;

import java.util.Enumeration;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by Ciprian on 12/1/2014.
 */
public class TableObserver implements Observer {
    @Override
    public void update(Observable o, Object arg) {
        String s="//SymTable content \n";
        Enumeration symTableKeys = ((PrgState)arg).getTable().keys();
        while(symTableKeys.hasMoreElements()){

            // Get the key (convert to string)
            String strKey = (String)symTableKeys.nextElement();
            // Use key to fetch the item and print out its value
            s+= strKey +"->" + ((PrgState)arg).getTable().get(strKey).toString() +"\n";
        }
        System.out.println(s);
    }
}
