package utils.ObserverPattern;

import repository.PrgState;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by Ciprian on 12/1/2014.
 */
public class OutObserver implements Observer{
    @Override
    public void update(Observable o, Object arg) {
        String s="//Out content \n";
        List<String> outBuff = (ArrayList<String>) ((PrgState)arg).getOut();
        for(String st : outBuff)
        {
            s+=st +"\n";
        }
        System.out.println(s);
    }
}
