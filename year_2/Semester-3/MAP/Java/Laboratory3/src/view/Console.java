package view;

//import com.sun.deploy.util.StringUtils;
import controller.Controller;
import models.*;
import repository.PrgState;
//import utils.List;

import java.util.List;
import java.util.Scanner;

/**
 * Created by Ciprian on 10/18/2014.
 */
public class Console {

    private Controller ctr;
    public Console(Controller c)
    {
        ctr=c;
    }
    public void EntryPoint()
    {
        readMenu();
    }

    private void readMenu() {
        mainMenuPrint();
        Scanner kb = new Scanner(System.in);
        System.out.println("Command: ");
        int cmd = kb.nextInt();
        switch (cmd)
        {
            case 0:
                break;
            case 1:
            {
                //read statement and execute
                Statement s = readStatement("");
                ctr.AddStatement(s);
                break;
            }
            case 2:
            {
                //step by step
                System.out.println("Program id: ");
                int programId = kb.nextInt();
                OneStepEvaluation(false,programId);
                break;
            }
            case 3:
            {
                //complete evaluation
                System.out.println("Program id: ");
                int programId = kb.nextInt();
                ctr.parallelRun(programId);
                //PrintResult(ctr.run(0).getOut());
                break;
            }
            case 4:
            {
                //debug
                System.out.println("Program id: ");
                int programId = kb.nextInt();
                OneStepEvaluation(true, programId);
                break;
            }
            case 5:
            {
                printAvailablePrograms();
                break;
            }
        }
        System.out.println("*************END OF EXECUTION*************");
    }

    private void printAvailablePrograms() {
        List<Statement> s = ctr.AllPrograms();
        for (int i = 0; i < s.size(); i++)
        {
            System.out.println(i + "  " + s.get(i).toString());
        }
        this.readMenu();
    }

    private void mainMenuPrint() {
        System.out.println("Menu: ");
        System.out.println("0 - to exit");
        System.out.println("1 - to enter a program");
        System.out.println("2 - for one step evaluation");
        System.out.println("3 - for complete evaluation");
        System.out.println("4 - for debuging");
        System.out.println("5 - list of available programs");
        System.out.println("---------------------------------");
    }

    private void PrintResult(List<String> outBuff) {
        System.out.println("Results");
        for (int i = 0; i < outBuff.size(); i++)//for (int i = 0; i < outBuff.getSize(); i++)
        {
            System.out.println(outBuff.get(i).toString());//System.out.println(outBuff.getAt(i).toString());
        }
    }

    private void OneStepEvaluation(boolean b, int programId) {
        PrgState prg = ctr.GetPrgState(0);
        int cmd = 1;
        int c = 1;
        while (ctr.GetCurrentStack(0).isEmpty() != true && cmd != 0)
        {
            System.out.println("Press 1 for continue: ");
            Scanner kb = new Scanner(System.in);
            cmd = kb.nextInt();
            System.out.println("Step " +c);
            if(b) {
                System.out.printf("***DEBUG*** %s ***END DEBUG*** \n\n", ctr.runStepByStep(0).toString());
            }
            else {
                PrintResult(ctr.runStepByStep(0).getOut());
            }
            c++;
        }
    }

    public void printReadStatementMenu(String msg) {
        if(msg != null && !msg.isEmpty()) {
            System.out.println("---------------------------------");
            System.out.println(msg);
            System.out.println();
            System.out.println("---------------------------------");
        }
        System.out.println("Choose statement type: ");
        System.out.println("0 - compound statement");
        System.out.println("1 - assignment statement");
        System.out.println("2 - print statement");
        System.out.println("3 - if statement");
    }

    public void printReadExpressionMenu(String msg) {
        if(msg != null && !msg.isEmpty()) {
            System.out.println("---------------------------------");
            System.out.println(msg);
            System.out.println();
            System.out.println("---------------------------------");
        }
        System.out.println("Choose expression type: ");
        System.out.println("0 - arithmetic expression");
        System.out.println("1 - constant expression");
        System.out.println("2 - var expression");
        System.out.println("3 - r expression");
        System.out.println("4 - new expression");
    }

    public int readStatementMenu() {
        Scanner kb = new Scanner(System.in);
        System.out.println("Command: ");
        int toReturn = kb.nextInt();
        return toReturn;
    }

    public void printSignMenu() {
        System.out.println("Choose sign (+,-,*,/): ");
    }

    public char readSign() {
        System.out.print("Choose: ");
        Scanner kb = new Scanner(System.in);
        char toReturn = kb.next().charAt(0);
        return toReturn;
    }

    public String readVar() {
        Scanner kb = new Scanner(System.in);
        System.out.println("Enter var name: ");
        String toReturn = kb.nextLine();
        return toReturn;
    }

    public int readConstant() {
        Scanner kb = new Scanner(System.in);
        System.out.println("Constant value: ");
        int toReturn = kb.nextInt();
        return toReturn;
    }

    public Expression readExpression(String msg) {
        printReadExpressionMenu(msg);
        int expr = readStatementMenu();
        Scanner kb = new Scanner(System.in);
        switch (expr) {
            case 0:
                //arithmetic expression
                Expression ex1 = readExpression("Expression one");
                Expression ex2 = readExpression("Expression two");
                printSignMenu();
                char sign = readSign();
                return new ArithExpr(ex1, ex2, sign);
            case 1:
                //constant expression
                int constantX = readConstant();
                return new ConstExpr(constantX);
            case 2:
                //var expression
                String varX = readVar();
                return new VarExpr(varX);
            case 3:
                //r expression
                System.out.println("Enter variable name: ");
                String toReturn = kb.nextLine();
                return new RExpr(toReturn);
            case 4:
                //new expression
                System.out.println("New: ");
                int val = kb.nextInt();
                return new NewExpr(val);
        }

        return null;
    }

    public Statement readStatement(String msg) {
        printReadStatementMenu(msg);
        int stmt = readStatementMenu();
        switch (stmt) {
            case 0:
                //compound statement
                Statement st1 = this.readStatement("Statement one");
                Statement st2 = this.readStatement("Statement two");
                return new CompoundStmt(st1, st2);
            case 1:
                //assignment statement
                String varX = readVar();
                Expression ex = readExpression("");
                return new AssignStmt(varX, ex);
            case 2:
                //print statement
                Expression ex2 = readExpression("");
                return new PrintStmt(ex2);
            case 3:
                //if statement
                Expression ex3 = this.readExpression("");
                Statement stm1 = this.readStatement("Statement one");
                Statement stm2 = this.readStatement("Statement two");
                return new IfStmt(ex3, stm1, stm2);
            case 4:
                //fork statement
                Statement stm = this.readStatement("Statement");
                return new ForkStmt(stm);



        }
        return null;
    }
}
