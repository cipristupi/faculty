package tests;

import models.*;
import repository.PrgState;
import repository.Repository;
/*import utils.Dictionary;
import utils.List;
import utils.Stack;*/

import javax.swing.plaf.nimbus.State;
import java.util.*;

/**
 * Created by Ciprian on 10/28/2014.
 */
public class ExpressionTests {
    public void run() {
        Stack<Statement> exeStack = new Stack<Statement>();
        Expression x = new ConstExpr(2);
        assert ((ConstExpr) x).getConstant() == 2;

        Statement s = new AssignStmt("x", x);
        assert ((AssignStmt)s).getName() == "x";
        assert ((AssignStmt)s).getExpr() == x;

        exeStack.push(s);
        assert !exeStack.isEmpty();
        Statement testX = exeStack.pop();
        assert exeStack.isEmpty();
        assert testX == s;

        /*Dictionary<String,Integer> symTable = new Dictionary<String,Integer>();
        symTable.add("x", 2);
        assert symTable.searchKey("y") == null;
        assert symTable.searchKey("x") == 2;*/

        Dictionary<String,Integer> symTable = new Hashtable<String, Integer>();
        int[] heap = new int[50];;
        symTable.put("x", 2);
        assert symTable.get("y") == null;
        assert symTable.get("x") == 2;

        Statement printSt = new PrintStmt(x);
        assert ((PrintStmt) printSt).getExpr() == x;

        Statement printSt2 = new PrintStmt(new ConstExpr(3));

        Statement ifSt = new IfStmt(x, printSt, printSt2);

        assert ((IfStmt) ifSt).getExpr() == x;
        assert ((IfStmt) ifSt).getSt1() == printSt;
        assert ((IfStmt) ifSt).getSt2() == printSt2;

        CompoundStmt compSt = new CompoundStmt(ifSt, s);
        assert compSt.getStmt1() == ifSt;
        assert compSt.getStmt2() == s;

        Expression varEx = new VarExpr("x");
        assert ((VarExpr) varEx).getName() == "x";

        //List<String> outBuff = new List<String>();
        List<String> outBuff = new ArrayList<String>();
        outBuff.add("test");
        assert outBuff.get(0) == "test";

        Expression arithExpr = new ArithExpr(x, varEx, '+');
        assert ((ArithExpr) arithExpr).eval(symTable ,heap) == 4;

        Repository r = new Repository();
        r.addPrgState(s);
    }
}
