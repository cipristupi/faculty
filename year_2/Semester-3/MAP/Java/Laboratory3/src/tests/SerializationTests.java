package tests;

import models.*;
import repository.PrgState;
import repository.Repository;

/**
 * Created by Ciprian on 11/10/2014.
 */
public class SerializationTests {
    public void run() {
        Repository r=new Repository();
        Statement s = new CompoundStmt(new AssignStmt("v", new ConstExpr(2)), new PrintStmt(new ArithExpr(new VarExpr("v"), new ConstExpr(3), '+')));
        r.addPrgState(s);
        r.Serialize(0);
        PrgState prgS = r.Deserialize(0);
        assert prgS == r.getPrgStateById(0);

        r.saveToFile(0);
    }
}
