package utils;

/**
 * Created by Ciprian on 10/15/2014.
 */
public interface IStack<TEntity> {
    void push(TEntity el);
    TEntity pop();
    boolean isEmpty();
    public TEntity puk();
}
