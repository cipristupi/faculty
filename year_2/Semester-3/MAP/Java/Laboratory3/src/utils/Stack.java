package utils;

/**
 * Created by Ciprian on 10/8/2014.
 */
public class Stack<TEntity> implements IStack<TEntity>
{
    private int top;
    private TEntity elem[];//fara dimensiune; private Object[] elem;
    private int capacity;
    public Stack()
    {
        capacity=10;
        //elem = new TEntity[capacity];
        elem = (TEntity[]) new Object[capacity];
        top=0;
    }
    public void push(TEntity el)
    {
        if(top == capacity)
        {
            resize();
        }
        elem[top]=el; // == elem[top++]=el;
        top++;
    }
    private void resize()
    {
        TEntity[] tmp =(TEntity[]) new Object[capacity];
        for(int i=0;i<top;i++)
        {
            tmp[i]=elem[i];
        }
        elem = tmp;
        capacity = capacity*2;
    }
    public TEntity pop()
    {
        if(top == 0)
            return null;
        return elem[--top];
    }
    public boolean isEmpty()
    {
        return top==0;
    }
    public TEntity puk()
    {
        if(top == 0 )
        {
            return null;
        }
        return elem[top-1];
    }

    public String toString()
    {
        String result = "";
        for (int i = top -1; i >= 0; i--)
        {
            result += elem[i].toString() + "\n";
        }
        return result;
    }
}
