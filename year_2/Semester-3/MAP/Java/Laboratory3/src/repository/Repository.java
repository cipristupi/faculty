package repository;

import models.Statement;

import javax.swing.plaf.nimbus.State;
import java.io.*;
import java.util.*;
/*import utils.Dictionary;
import utils.List;*/

/**
 * Created by Ciprian on 10/26/2014.
 */
public class Repository {
    private List<Statement> programList;
    private List<PrgState> prgstateList;

    public Repository()
    {
        //prgstateList = new Dictionary<Integer, PrgState>();
        prgstateList = new ArrayList<PrgState>();
        programList = new ArrayList<Statement>();
    }

    /**
     *
     * @param s - statement to be added into program state
     * @param i - index of program state where the statement to be added
     */
    public void addToState(Statement s, int i)
    {
        prgstateList.get(i).getStack().push(s);
    }
    public void AddStatement(Statement statement)
    {
        programList.add(statement);
        //prgstateList.Add(id, new PrgState(statement));
    }

    public void addPrgState(Statement initialState)
    {
        prgstateList.add(new PrgState(initialState));
    }

    public List<PrgState> getAllPrgState()
    {
        /*List<PrgState> stmtList = new ArrayList<PrgState>();
        List<Integer> keys = ( ArrayList<Integer>) prgstateList.keys();
        for (int i = 0; i < keys.size(); i++)
        {
            stmtList.add(prgstateList.get(keys.get(i)));
        }
        return stmtList;*/
        return prgstateList;
    }

    public List<Statement> getAllPrograms()
    {
        return programList;
    }

    public void NewPrgState(int id)
    {
        Statement s = programList.get(id);
        PrgState p  =new PrgState(s);
        prgstateList.add(p);
    }

    public PrgState getPrgStateById(int i)
    {
        return prgstateList.get(i);
    }

    public void addFork(Statement s, PrgState parent)
    {
        prgstateList.add(new PrgState(s));
        int last = prgstateList.size() - 1;
        PrgState f = prgstateList.get(last);
        f.setHeap(parent.getHeap());
        f.copyTable((Map<String, Integer>) parent.getTable());
        f.setOut(parent.getOut());
    }

    public boolean programExists(int id)
    {
        //return prgstateList.searchKey(id) != null ? true : false;
        return prgstateList.get(id) != null ? true : false;
    }

    public int  GetProgramsNumber()
    {
        return prgstateList.size();
    }

    public  Boolean checkProgramStackIsEmpty(int i)
    {
        System.out.println("check== "+getPrgStateById(i).getStack()+" , i="+i);
        if (getPrgStateById(i).getStack().size() == 0)
        {
            System.out.println("check --remove");
            prgstateList.remove(i);
            return false;
        }
        return true;
    }


    //region  Serialization and File operations
    public void Serialize(int programId)
    {
        PrgState p =this.getPrgStateById(programId);
        try
        {
            FileOutputStream fileOut =
                    new FileOutputStream(System.getProperty("user.dir")+"/outputs/"+programId +".txt");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(p);
            out.close();
            fileOut.close();
            System.out.printf("Serialized data is saved in "+System.getProperty("user.dir")+"/outputs/"+programId +".txt");
        }catch(IOException i)
        {
            i.printStackTrace();
        }
    }


    public PrgState Deserialize(int programId)
    {
        PrgState e = null;
        try
        {
            FileInputStream fileIn = new FileInputStream(System.getProperty("user.dir")+"/outputs/"+programId +".txt");
            ObjectInputStream in = new ObjectInputStream(fileIn);
            e = (PrgState) in.readObject();
            in.close();
            fileIn.close();
            return  e;
        }catch(IOException i)
        {
            i.printStackTrace();
            return null;
        }catch(ClassNotFoundException c)
        {
            c.printStackTrace();
            return null;
        }
    }

    public  void saveToFile(int programId)
    {
        PrgState r = this.getPrgStateById(programId);
        this.saveToFile(r);
    }

    public  void saveToFile(PrgState r)
    {
        String filePath = System.getProperty("user.dir")+"/outputs/" +"prgState.txt";
        PrintWriter out = null;
        try {
            out = new PrintWriter(filePath);
            out.println(r.toString());
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }
    //endregion
}
