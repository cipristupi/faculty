package repository;

import models.Statement;
import utils.*;
/*import utils.Dictionary;
import utils.List;
import utils.Stack;*/

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.*;
import java.util.Dictionary;
import java.util.List;
import java.util.Stack;

/**
 * Created by Ciprian on 10/25/2014.
 */
public class PrgState implements java.io.Serializable
{
    private Dictionary<String,Integer> tableOfSymbols;
    private Stack<Statement> exeStack;
    private List<String> outBuff;
    private int[] heap;

    public PrgState(Statement initialStatement)
    {
        outBuff = new ArrayList<String>();
        exeStack = new Stack<Statement>();
        tableOfSymbols = new Hashtable<String, Integer>();
        heap = new int[50];
        exeStack.push(initialStatement);
        System.out.println("stack= "+exeStack);
    }

    public PrgState(Dictionary<String,Integer> t, Stack<Statement> s,  List<String> o , int[] h)
    {
        this.outBuff = o;
        this.exeStack = s;
        this.tableOfSymbols = t;
        this.heap =h;
    }
    public void AddStatement(Statement s)
    {
        exeStack.push(s);
    }

    public Dictionary<String,Integer> getTable()
    {
        return tableOfSymbols;
    }
    public Stack getStack()
    {
        return exeStack;
    }
    public  List<String> getOut()
    {
        return outBuff;
    }

    public int[] getHeap()
    {
        return heap;
    }


    public void setOut(List<String> n)
    {
        outBuff = n;
    }
    public void copyTable(Map<String, Integer> s)
    {
       // Iterator<String> it=s.keySet().iterator();
        for(String key :s.keySet())
            tableOfSymbols.put(key, s.get(key));
        /*foreach (var pair in s)
        {
            tableOfSymbols.Add(pair.Key, pair.Value);
        }*/
    }

    public int addToHeap(int value)
    {
        int pos=0;
        for (int i = 0; i < 50; i++)
        {
            heap[i] = value;
            pos = i;
            break;
        }
        return pos;
    }

    public String toString(){
        String o ="";
        List<Statement> stackList =new  ArrayList<Statement>();
        Stack<Statement> stack = this.getStack();
        Enumeration symTableKeys = this.getTable().keys();
        List<String> outBuff = (ArrayList<String>)this.getOut();
        while(!stack.empty())
        {
            stackList.add(stack.pop());
        }
            o += "//Stack content \n";
            for(int i=stackList.size()-1 ;i>=0;i--) {
                o +=stackList.get(i).toString() +"\n";
            }
            o+="\n";
            o+="//SymTable content \n";
            while(symTableKeys.hasMoreElements()){

                // Get the key (convert to string)
                String strKey = (String)symTableKeys.nextElement();
                // Use key to fetch the item and print out its value
               o+= strKey +"->" + this.getTable().get(strKey).toString() +"\n";
            }
        o+="\n";
            o+="//Out content \n";
            for(String s : outBuff)
            {
                o+=s +"\n";
            }
        return o;
    }

    public void setHeap(int[] heap) {
        this.heap = heap;
    }
}
