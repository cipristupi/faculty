package controller;

import models.*;
import repository.PrgState;
import repository.Repository;
import utils.ObserverPattern.OutObserver;
import utils.ObserverPattern.StackObserver;
import utils.ObserverPattern.Subject;
import utils.ObserverPattern.TableObserver;


import java.util.Dictionary;
import java.util.List;
import java.util.Observer;
import java.util.Stack;
/*import utils.Dictionary;
import utils.Stack;
import utils.*;
import utils.List;*/

/**
 * Created by Ciprian on 10/18/2014.
 */
public class Controller {
    private Repository repo;
    private PrgState currentPrg;
    private Subject subject;

    public Controller(Repository r,Subject s)
    {
        repo=r;
        subject =s;

    }

    public void parallelRun(Integer programID)
    {
        int programNumber;
        repo.NewPrgState(programID);
        while (true)
        {
            programNumber = repo.GetProgramsNumber();
            if (programNumber == 0)
            {
                return;
            }

            for (int i = 0; i < programNumber; i++)
            {
                System.out.println("for ...");
                if (repo.checkProgramStackIsEmpty(i))
                {
                    System.out.println("step by step "+i);
                    this.runStepByStep(i);
                }
                else
                {
                    programNumber--;
                }
            }
        }
    }

    public PrgState run(int programID)
    {
        if(!repo.programExists(programID))
        {
            return null;
        }
        currentPrg = repo.getPrgStateById(programID);
        while(!currentPrg.getStack().isEmpty())
        {
            runStepByStep(programID);
        }
        return currentPrg;
    }

    public PrgState runStepByStep(Integer programID)
    {
        if (!repo.programExists(programID))
        {
            return null;
        }
        InitPrgState(programID);
        Statement st = (Statement) GetCurrentStack(programID).pop();
        //subject.notifyObserver(stackObserver,currentPrg);//Observer
       // subject.notifyObservers(currentPrg);
        System.out.println("st="+st);
        if (st instanceof CompoundStmt)
        {
            System.out.println("st = " + st);
            execCompound((CompoundStmt)st,programID);
        }
        else
            if (st instanceof IfStmt)
            {
                execIf((IfStmt)st);
            }
            else
                if (st instanceof PrintStmt)
                {
                    execPrint((PrintStmt)st);
                }
                else
                    if (st instanceof AssignStmt)
                    {
                        execAssign((AssignStmt)st);
                    }
                    else
                        if(st instanceof ForkStmt)
                        {
                            execFork((ForkStmt)st);
                        }
        return currentPrg;
    }

    private void execFork(ForkStmt st) {
        Statement x = st.getFork();
        repo.addFork(x, currentPrg);
    }

    private void InitPrgState(Integer programID) {
           currentPrg = repo.getPrgStateById(programID);
    }

    private void execAssign(AssignStmt st) {
        Expression expr=st.getExpr();
        String varName = st.getName();
        if(expr instanceof ConstExpr) {
            int value = ((ConstExpr) expr).getConstant();
            currentPrg.getTable().put(varName, value);
        } else if(expr instanceof VarExpr) {
            String id = ((VarExpr) expr).getName();
            int value = currentPrg.getTable().get(id);
            currentPrg.getTable().put(st.getName(), value);
        } else if(expr instanceof ArithExpr) {
            int value = ((ArithExpr) expr).eval(currentPrg.getTable(),currentPrg.getHeap());
            currentPrg.getTable().put(st.getName(), value);
        }else if(expr instanceof RExpr){
            int value = ((RExpr)expr).eval(currentPrg.getTable(), currentPrg.getHeap());
            currentPrg.getTable().put(varName, value);
        }else if(expr instanceof  NewExpr) {
            int value = ((NewExpr)expr).getValue();
            int address = currentPrg.addToHeap(value);
            currentPrg.getTable().put(varName, address);
        }

        //subject.notifyObserver(tableObserver,currentPrg);//Observer
        subject.notifyObservers(currentPrg);
    }

    private void execPrint(PrintStmt st) {
        Expression toPrint = st.getExpr();
        try {
            int result = 0;
            if (toPrint instanceof ConstExpr) {
                result = ((ConstExpr) toPrint).getConstant();
            } else if (toPrint instanceof VarExpr) {
                String id = ((VarExpr) toPrint).getName();
                result = currentPrg.getTable().get(id);
            } else if (toPrint instanceof ArithExpr) {
                result = ((ArithExpr) toPrint).eval(currentPrg.getTable(),currentPrg.getHeap());
            }else if(toPrint instanceof RExpr){
                result = ((RExpr)toPrint).eval(currentPrg.getTable(), currentPrg.getHeap());
            }else if(toPrint instanceof  NewExpr) {
                int value = ((NewExpr)toPrint).getValue();
                result = currentPrg.addToHeap(value);
            }
            String x = Integer.toString(result);
            currentPrg.getOut().add(x);
            //consoleX.printLastMessageInBuffer();
        } catch(NumberFormatException ex) {
            currentPrg.getOut().add(ex.toString());
            //consoleX.printLastMessageInBuffer();
        }
        //subject.notifyObserver(outObserver,currentPrg);//Observer
        subject.notifyObservers(currentPrg);
    }

    private void execIf(IfStmt st) {
        Expression expr=st.getExpr();
        int value;
        if(expr instanceof ConstExpr) {
            value = ((ConstExpr) expr).getConstant();
        }
        else
        if(expr instanceof VarExpr) {
            String id = ((VarExpr) expr).getName();
            value = currentPrg.getTable().get(id);
        }
        else
            if(expr instanceof ArithExpr)
            {
                value = ((ArithExpr) expr).eval(currentPrg.getTable(),currentPrg.getHeap());
            }
            if (expr instanceof NewExpr)
            {
                value = currentPrg.addToHeap(((NewExpr)expr).getValue());
            }
            else if (expr instanceof RExpr)
            {
                value = ((RExpr)expr).eval(currentPrg.getTable(), currentPrg.getHeap());
            }
            else
            {
                value = 0;
            }

        if(value != 0)
        {
            currentPrg.getStack().push(st.getSt1());
        }
        else
        {
            currentPrg.getStack().push(st.getSt2());
        }
        //subject.notifyObserver(stackObserver,currentPrg);//Observer
        subject.notifyObservers(currentPrg);
    }

    private void execCompound(CompoundStmt st,int programID) {

        repo.addToState(st.getStmt2(),programID);
        repo.addToState(st.getStmt1(),programID);
        //currentPrg.getStack().push(st.getStmt2());
        //currentPrg.getStack().push(st.getStmt1());
        //subject.notifyObserver(stackObserver,currentPrg);//Observer
        subject.notifyObservers(currentPrg);
    }

    public void addPrgState(Statement s)
    {
        repo.addPrgState(s);
    }

    public PrgState GetPrgState(int i) {
        return repo.getPrgStateById(i);
    }


    public PrgState GetCurrentPrgState()
    {
        return this.currentPrg;
    }

    public Stack<Statement> GetCurrentStack(int programID)
    {
        InitPrgState(programID);
        return currentPrg.getStack();
    }

    public List<Statement> AllPrograms()
    {
        return repo.getAllPrograms();
    }

    public void AddStatement(Statement s)
    {
        repo.AddStatement(s);
    }
}
