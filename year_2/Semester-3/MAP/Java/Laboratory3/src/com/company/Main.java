package com.company;

import controller.Controller;
import models.*;
import repository.Repository;
import tests.ExpressionTests;
import tests.SerializationTests;
import utils.Dictionary;
import utils.List;
import utils.ObserverPattern.OutObserver;
import utils.ObserverPattern.StackObserver;
import utils.ObserverPattern.Subject;
import utils.ObserverPattern.TableObserver;
import utils.Stack;
import view.Console;

import java.util.Observer;

public class Main {

    public static void main(String[] args) {
       // ExpressionTests t =new ExpressionTests();
        //t.run();
        //SerializationTests st = new SerializationTests();
        //st.run();
        //Statement s = new IfStmt(new ConstExpr(1), new PrintStmt(new ConstExpr(0)), new PrintStmt(new ConstExpr(1)));
        Statement s = new CompoundStmt(new AssignStmt("v", new ConstExpr(2)), new PrintStmt(new ArithExpr(new VarExpr("v"), new ConstExpr(3), '+')));
        Statement s4 = new IfStmt(new ConstExpr(1), new PrintStmt(new ConstExpr(0)), new PrintStmt(new ConstExpr(1)));
        Statement s1 = new CompoundStmt(new AssignStmt("v", new ConstExpr(2)), new PrintStmt(new ArithExpr(new VarExpr("v"), new ConstExpr(3), '+')));
        Statement s2 = new CompoundStmt(new PrintStmt(new ConstExpr(3)), new ForkStmt(new CompoundStmt(new AssignStmt("v", new ConstExpr(2)), new PrintStmt(new ArithExpr(new VarExpr("v"), new ConstExpr(3), '+')))));
        Statement s3 = new CompoundStmt(new AssignStmt("v", new NewExpr(2)), new ForkStmt(new PrintStmt(new ArithExpr(new RExpr("v"), new ConstExpr(3), '+'))));
        Repository repo = new Repository();
        repo.AddStatement(s);
        repo.AddStatement(s1);
        repo.AddStatement(s2);
        repo.AddStatement(s3);
        repo.AddStatement(s4);
        //region Observer pattern
        Subject sub =new Subject();

        Observer stackObserver =new StackObserver();
        Observer outObserver =new OutObserver();
        Observer tableObserver =new TableObserver();
        sub.addObserver(stackObserver);
        sub.addObserver(outObserver);
        sub.addObserver(tableObserver);
        //endregion
        Controller cntr= new Controller(repo,sub);
        Console console = new Console(cntr);
        console.EntryPoint();
    }
}
