package models;

import java.util.Dictionary;

/**
 * Created by Ciprian on 1/13/2015.
 */
public class RExpr extends Expression {
    private String varName;

    public RExpr(String var)
    {
        this.varName = var;
    }

    public Integer eval(Dictionary<String,Integer> s , int[] h)
    {
        int address = s.get(this.varName);
        int valueHeap = (int)h[address];
        return valueHeap;
    }

    @Override
    public String toString() {
        return "r(" + varName + ")";
    }
}
