#include <iostream>
#include "StaticLinkedList.h"
#include "StaticLinkedListIterator.h"
#include "Expense.h"

using namespace std;

StaticLinkedList<Expense>* entities;


void addInit()
{
    Expense ex(1,1,1,1,"one");
    entities->add(ex);
    Expense ex1(2,2,2,2,"two");
    entities->add(ex1);
    Expense ex2(3,3,3,3,"three");
    entities->add(ex2);
    Expense ex3(4,4,4,4,"four");
    entities->add(ex3);
    Expense ex4(5,5,5,5,"five");
    entities->add(ex4);
}

void addNewExpense()
{
    int month,day,id;
    float value;
    string category;
    cout<<"ID: ";
    cin>>id;
    cout<<"Month: ";
    cin>>month;
    cout<<"Day: ";
    cin>>day;
    cout<<"Value: ";
    cin>>value;
    cout<<"Category: ";
    cin>>category;
    Expense ex(id,month,day,value,category);
    //ex = new Expense(id,month,day,value,category);
    entities->add(ex);


}

void printExpense(Expense ex)
{
    cout<<"ID: " << ex.getID() <<endl;
    cout<<"Month: " << ex.getMonth() <<endl;
    cout<<"Day: " << ex.getDay() <<endl;
    cout<<"Value: " << ex.getValue() <<endl;
    cout<<"Category: "<< ex.getCategory() <<endl;
}

void removeID()
{
    int id;
    cout<<"ID: ";
    cin>>id;
    vector<Expense> res;
    LinkedListIterator<Expense>* it=entities->getIterator();
    while (it->hasNext())
        {
			res.push_back(it->getValue());
			++(*it);
		}
    Expense e;
    for(int i=0;i<res.size();i++)
    {
        e = res[i];
        cout<<"ID " << e.getID()<<endl;
        if(e.getID()==id)
        {
            entities->remove(i);
            break;
        }
    }
}

void printAll()
{
    vector<Expense> res;
    LinkedListIterator<Expense>* it=entities->getIterator();
    while (it->hasNext())
        {
			res.push_back(it->getValue());
			++(*it);
		}
	for(int i=0;i<res.size();i++)
    {
        printExpense(res[i]);
    }
}

vector<Expense> getAll()
{
    vector<Expense> res;
    LinkedListIterator<Expense>* it=entities->getIterator();
    while (it->hasNext())
        {
			res.push_back(it->getValue());
			++(*it);
		}
	return res;
}

int main()
{
    entities=new StaticLinkedList<Expense>;
    addInit();
    int cmd;
        do
        {
            cout<<"Command: ";
            cin>>cmd;
            switch(cmd)
            {
                case 1:
                {
                        addNewExpense();
                        break;
                }
                case 2:
                {
                        printAll();
                        break;
                }
                case 3:
                    {
                        removeID();
                        break;
                    }

            }

        }while(cmd);

    return 0;
}
