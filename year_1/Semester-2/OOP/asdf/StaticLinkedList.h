#ifndef STATICLINKEDLIST_H_INCLUDED
#define STATICLINKEDLIST_H_INCLUDED

#include "Node.h"
#include "StaticLinkedListIterator.h"
#include <vector>
using namespace std;



template <typename T>
class StaticLinkedList
{

	public:
    friend class LinkedListIterator<T>;
    StaticLinkedList();
    virtual ~StaticLinkedList();

    int size();
    void add(T el);
	T& operator[](int k);
	void remove(int k);
	LinkedListIterator<T>* getIterator();

	private:
	void addFirst(T& el);
	int getEmpty();
	void updateEmpty();
	void ensureCapacity();
	void removeFirst();


	int first;
	int len;
	int emptyP;//empty position
	vector<Node<T> > elems;
	//DynamicArray<Expense>* elems;

};

template<typename T>
StaticLinkedList<T>::StaticLinkedList()
{
	first = -1;
	emptyP = 0;
	len = 0;
}


template<typename T>
StaticLinkedList<T>::~StaticLinkedList()
{

}

template<typename T>
int StaticLinkedList<T>::size()
{
	return len;
}

template<typename T>
void StaticLinkedList<T>::add(T el)
{
    ensureCapacity();
	if (first == -1)
	{
		addFirst(el);
		return;
	}
	Node<T>* p = &elems[first];
	while (p->getNext() != -1)
	{
		p = &elems[p->getNext()];
	}
	int k = getEmpty();
	elems[k] = Node<T>(el);
	p->setNext(k);
	p=&elems[p->getPrev()];
	//elems[k].setPrev(p->getNext());//HERE problems
	len++;
	updateEmpty();
}


template<typename T>
T& StaticLinkedList<T>::operator[](int k)
{
	if (k < 0 || k >= len)
	{

	}
	Node<T>* p = &elems[first];
	for (int i = 0; i<k; i++, p = &elems[p->getNext()])
	{

	}
	return p->getInf();
}

template<typename T>
void StaticLinkedList<T>::remove(int k)
{
    if(k==0)
    {
        removeFirst();
        return;
    }
    if(k>=0 && k<len)
    {
        Node<T>* p = &elems[first];
        Node<T>* f;
        for(int i=0;i<k;i++)
        {
            p = &elems[p->getNext()];
        }
        int posF;
        f = p;
        f = &elems[f->getPrev()];
        posF = f->getNext();//here get the index which will be free

        p = &elems[p->getNext()];
        p->setPrev(f->getPrev());

        p = &elems[p->getPrev()];
        f->setNext(p->getNext());
        //elems[posF]=NULL;
        emptyP=posF;
        len--;
    }
}

template<typename T>
LinkedListIterator<T>* StaticLinkedList<T>::getIterator()
{
	return new LinkedListIterator<T>(this);
}

template<typename T>
void StaticLinkedList<T>::addFirst(T& el)
{
    ensureCapacity();
	int k = getEmpty();
	elems[k] = Node<T>(el, first, -1);
	first = k;
	len++;
	updateEmpty();
}

template<typename T>
int StaticLinkedList<T>::getEmpty()
{
	return emptyP;
}

template<typename T>
void StaticLinkedList<T>::updateEmpty()
{
	if (elems[emptyP].getNext() != -1)
	{
		emptyP = elems[emptyP].getNext();
		return;
	}
	emptyP = len;
}

template<typename T>
void StaticLinkedList<T>::ensureCapacity()
{
		if (elems.size() == len)
        {
			Node<T> e = Node<T>();
			elems.push_back(e);
		}
}

template<typename T>
void StaticLinkedList<T>::removeFirst()
{

}

#endif // LINKEDLIST_H_INCLUDED
