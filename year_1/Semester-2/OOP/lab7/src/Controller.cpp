#include "Controller.h"

Controller::Controller()
{
    //ctor
}

Controller::~Controller()
{
    //dtor
}
Controller::Controller(AbstractRepository<Expense,int>* r)
{
    repo=r;
}

void Controller::addExpenseCtr(int id, int month, int day, float value, string category)
{
    Expense ex(id,month,day,value,category);
    repo->save(ex);
}

void Controller::modifyExpenseCtr(int id, float nvalue)
{
    Expense ex;
    ex = *(repo->findById(id));
    repo->update(id,ex);
}

void Controller::deleteExpenseCtr(int id)
{

}

int Controller::getSizeCtr()
{

}

int Controller::validator(Expense* ex)
{

}

Expense Controller::getByIDCtr(int id)
{
    return *(repo->findById(id));
}

void Controller::undoCtr()
{

}

vector<Expense> Controller::getAllCtr()
{
    return repo->getAll();

}


vector<Expense> Controller::getAllOrderDay()
{
    vector<Expense> res;
    res = repo->getAll();
    Expense ex1,ex2;
    for(int i=0;i<res.size()-1;i++)
        for(int j=i+1;j<res.size();i++)
        {
            ex1=res[i];
            ex2 = res[j];
            if(ex1.getDay() > ex2.getDay())
            {
                swap(res[i],res[j]);
            }
        }
        return res;
}

vector<Expense> Controller::getAllOrderCategory()
{
    vector<Expense> res;
    res = repo->getAll();
    Expense ex1,ex2,aux;
    for(int i=0;i<res.size()-1;i++)
        for(int j=i+1;j<res.size();i++)
        {
            ex1=res[i];
            ex2 = res[j];
            if(ex1.getCategory() > ex2.getCategory())
            {
                aux=res[i];
                res[i]=res[j];
                res[j]=aux;
                //swap(res[i],res[j]);
            }
        }
        return res;
}

vector<Expense> Controller::getbyAmountGreater(float value)
{
    vector<Expense> res,all;
    all = repo->getAll();
    Expense ex1;
    for(int i=0;i<all.size();i++)
    {
        ex1=all[i];
        if(ex1.getValue()>value)
        {
            res.push_back(ex1);
        }
    }

    return res;
}

vector<Expense> Controller::getbyCategory(string category)
{
     vector<Expense> res,all;
        all = repo->getAll();
        Expense ex1;
        for(int i=0;i<all.size();i++)
        {
            ex1=all[i];
            if(ex1.getCategory()==category)
            {
                res.push_back(ex1);
            }
        }

        return res;
}

vector<Expense> Controller::getByCatAmount(float value, string category)
{
 vector<Expense> res,all;
    all = repo->getAll();
    Expense ex1;
    for(int i=0;i<all.size();i++)
    {
        ex1=all[i];
        if(ex1.getValue()>value && ex1.getCategory()==category)
        {
            res.push_back(ex1);
        }
    }

    return res;
}

