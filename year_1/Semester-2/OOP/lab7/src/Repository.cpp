#include "Repository.h"
#include "Expense.h"
#include "InMemRepo.h"
#include "AbstractRepository.h"
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
using namespace std;

Repository::Repository(string f)
{
    filename=f;
    loadFromFile();
}


Repository::~Repository()
{
    //dtor
}

void Repository::loadFromFile()
{
    ifstream fin(filename.c_str(),ios::in);
    if(!fin.is_open())
    {
        cout<<"ERROR again"<<endl;
        cout<<"ERROR again"<<endl;
    }
    while(fin.good())
    {
        Expense ex;
        fin>>ex;
        InMemRepo::save(ex);
        //cout<<ex.getID()<< " " << ex.getMonth() << "  " << ex.getDay() << " " << ex.getValue() << "  " << ex.getCategory() << endl;
    }
    fin.close();
}

void Repository::saveToFile()
{
    ofstream file;
    vector<Expense> res;
    res = InMemRepo::getAll();
    file.open(filename.c_str());
    Expense ex;
	for (int i = 0; i< res.size(); i++)
	{
	    ex=res[i];
		file <<ex.getID()<<","<<ex.getMonth()<<","<<ex.getDay()<<","<<ex.getValue()<<","<<ex.getCategory()<< endl;
	}
	file.close();
}

void Repository::save(Expense ex)
{
    InMemRepo::save(ex);
    ofstream fout(filename.c_str(),ios::app);
    fout<<"\n"<<ex.getID()<<","<<ex.getMonth()<<","<<ex.getDay()<<","<<ex.getValue()<<","<<ex.getCategory();
    fout.close();

}

