#include "Expense.h"
#include <iostream>
#include <sstream>

using namespace std;

Expense::Expense()
{
    //ctor
}

Expense::Expense(int i, int m, int d, float v, string c)
{
    id=i;
    month = m;
    day = d;
    value = v;
    category = c;
}

Expense::~Expense()
{
    //dtor
}

/** @brief (one liner)
  *
  * (documentation goes here)
  */
void Expense::setCategory(string ncategory)
{
    category = ncategory;
}

/** @brief (one liner)
  *
  * (documentation goes here)
  */
void Expense::setValue(float nvalue)
{
    this->value=nvalue;
}

/** @brief (one liner)
  *
  * (documentation goes here)
  */
void Expense::setDay(int nday)
{
    day=nday;
}

/** @brief (one liner)
  *
  * (documentation goes here)
  */
void Expense::setMonth(int nmonth)
{
    month=nmonth;
}

/** @brief (one liner)
  *
  * (documentation goes here)
  */
void Expense::setID(int nid)
{
    id=nid;
}

/** @brief (one liner)
  *
  * (documentation goes here)
  */
string Expense::getCategory()
{
    return category;
}

/** @brief (one liner)
  *
  * (documentation goes here)
  */
float Expense::getValue()
{
    return value;
}

/** @brief (one liner)
  *
  * (documentation goes here)
  */
int Expense::getDay()
{
    return day;
}

/** @brief (one liner)
  *
  * (documentation goes here)
  */
int Expense::getMonth()
{
    return month;
}

/** @brief (one liner)
  *
  * (documentation goes here)
  */
int Expense::getID()
{
    return id;
}

/*
 * Operator overloading, overload the << operator for printing expenses.
 */

std::ostream& operator<<(std::ostream& os, Expense& ex)
 {
	//id,month,day,value,category
	os << "Id: " << ex.getID() << std::endl << "Month: " << ex.getMonth()
			<< std::endl
			<< "Day: " << ex.getDay() << std::endl << "Value: "
			<< ex.getValue()
			<< std::endl << "Category: " << ex.getCategory()
			<< std::endl;
	return os;
}

/*
 * Operator overloading, overload the >> operator for reading expenses.
 */
istream& operator>>(istream&is , Expense& ex)
{
	//id, month, day, value, category
	int id, month, day;
	float value;
	string line, token;

	is >> line;
	//stringstream ss(line);
	stringstream ss(line);

	getline(ss, token, ',');
	stringstream(token) >> id;
	ex.setID(id);

	getline(ss, token, ',');
	stringstream(token) >> month;
	ex.setMonth(month);

	getline(ss, token, ',');
	stringstream(token) >> day;
	ex.setDay(day);

	getline(ss, token, ',');
	stringstream(token) >> value;
	ex.setValue(value);

	getline(ss, token, ',');
	ex.setCategory(token);


	return is;

}
