#include <iostream>
#include "Repository.h"
#include "Expense.h"
#include "Controller.h"
#include "AbstractRepository.h"
#include "InMemRepo.h"
#include "Ui.h"
using namespace std;

int main()
{
    AbstractRepository<Expense,int> *r =new Repository("expenses.txt");


    Controller c(r);
    Ui userInt(c);
    userInt.startUI();
    cout << "Goodbye world!" << endl;
    return 0;
}
