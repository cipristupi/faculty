#ifndef REPOSITORY_H
#define REPOSITORY_H

#include "AbstractRepository.h"
#include "InMemRepo.h"
#include "Validator.h"
#include "Expense.h"

class Repository : public InMemRepo
{
    public:
        //Repository(string f): InMemRepo(),filename(f){ loadFromFile();}
        Repository(string f);
        virtual ~Repository();

        void saveToFile();
        void save(Expense ex);
    protected:
    private:
        string filename;
        void loadFromFile();
};



#endif // REPOSITORY_H
