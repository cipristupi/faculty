#ifndef LINKEDLISTITERATOR_H_INCLUDED
#define LINKEDLISTITERATOR_H_INCLUDED

#include "StaticLinkedList.h"
#include "Node.h"
template<class T>
class StaticLinkedList;

template<typename T>
class LinkedListIterator
{
private:
	StaticLinkedList<T>* l;
	int crt;
public:
	LinkedListIterator(StaticLinkedList<T>* li);
	virtual ~LinkedListIterator();
	virtual bool hasNext();
	virtual void operator++();
	virtual void operator--();
	virtual T& operator()();
	virtual T getValue();
	virtual void next();
	virtual void back();
	virtual void begin();
	virtual int getCrt();

};
    template<typename T>
	LinkedListIterator<T>::LinkedListIterator(StaticLinkedList<T>* li)
	{
	    l=li;
		crt = li->first;
	}
	template<typename T>
	LinkedListIterator<T>::~LinkedListIterator()
	{

	}
	template<typename T>
	bool LinkedListIterator<T>::hasNext()
	{
		return crt != -1;
	}
	template<typename T>
	void LinkedListIterator<T>::operator++()
	{
		crt = l->elems[crt].getNext();
	}
	template<typename T>
	void LinkedListIterator<T>::operator--()
	{
		crt = l->elems[crt].getPrev();
	}
	template<typename T>
	T& LinkedListIterator<T>::operator()()
	{
		//return l->elems[crt].getInf();
	}

	template<typename T>
	T LinkedListIterator<T>::getValue()
	{
	    return l->elems[crt].getInf();
	}

	template<typename T>
	void LinkedListIterator<T>::next()
	{
	    crt = l->elems[crt].getNext();
	}
	template<typename T>
	void LinkedListIterator<T>::back()
	{
	    crt = l->elems[crt].getPrev();
	}

	template<typename T>
	void LinkedListIterator<T>::begin()
	{
	    crt = l->first;
	}

	template<typename T>
	int LinkedListIterator<T>::getCrt()
	{
	    return crt;
	}

#endif // LINKEDLISTITERATOR_H_INCLUDED
