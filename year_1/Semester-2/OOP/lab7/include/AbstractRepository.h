#ifndef ABSTRACTREPOSITORY_H_INCLUDED
#define ABSTRACTREPOSITORY_H_INCLUDED

#include <vector>
using namespace std;

template<class Entity, class Id>
class AbstractRepository
{
public:
	virtual Entity* findById(Id id)=0;
	virtual void save(Entity e)=0;
	virtual void removeE(Id id)=0;
	virtual void update(Id id, Entity entity)=0;
	virtual vector<Entity> getAll()=0;
	virtual ~AbstractRepository() {
	}
};

#endif // ABSTRACTREPOSITORY_H_INCLUDED
