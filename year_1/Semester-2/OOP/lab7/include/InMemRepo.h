#ifndef INMEMREPO_H_INCLUDED
#define INMEMREPO_H_INCLUDED

#include<string>
#include<vector>
#include "AbstractRepository.h"
#include "Expense.h"
#include "StaticLinkedList.h"
#include "StaticLinkedListIterator.h"
#include "Node.h"
#include "Validator.h"
using namespace std;



class InMemRepo: public AbstractRepository<Expense,int>
{
    //vector<Expense> entities;
    StaticLinkedList<Expense>* entities;
public:
    InMemRepo()
    {
        entities = new StaticLinkedList<Expense>;
    }
    virtual ~InMemRepo()
    {
       // delete entities;
    }

    Expense* findById(int id)
    {
        Expense* ex=new Expense(-1,-1,-1,-1,"zero");
        Expense e;
        LinkedListIterator<Expense>* it=entities->getIterator();
        while (it->hasNext())
        {
			e = it->getValue();
			if(e.getID() == id)
            {
                ex->setID(e.getID());
                ex->setMonth(e.getMonth());
                ex->setDay(e.getDay());
                ex->setValue(e.getValue());
                ex->setCategory(e.getCategory());
                return ex;
            }
			++(*it);
		}
        return ex;
    }

    void save(Expense el)
    {
		//entities.push_back(el);
		Validator val;
        val.validate(&el);
        if(findById(el.getID())->getID()!=-1)
            throw(ExpenseException("The same ID exists.\n"));
		entities->add(el);
    }
    void update(int id, Expense ex)
    {
        if(findById(id)->getID() != -1)
        {
            Expense* e;
            e  =findById(id);
            e->setValue(ex.getValue());
            LinkedListIterator<Expense>* it=entities->getIterator();
            while (it->hasNext())
            {
                //(*it)().setValue(ex.getValue());
                    ++(*it);
            }
        }
        else
            throw(ExpenseException("The id doesn't exists.\n"));
    }

    void removeE(int id)
    {

    }
    vector<Expense> getAll()
    {
        vector<Expense> res;
        //return entities;
        LinkedListIterator<Expense>* it=entities->getIterator();
    while (it->hasNext())
        {
			res.push_back(it->getValue());
			++(*it);
		}
		delete it;
		return res;
    }
};

#endif // INMEMREPO_H_INCLUDED
