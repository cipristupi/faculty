#ifndef NODE_H_INCLUDED
#define NODE_H_INCLUDED

template <typename T>
class Node
{
private:
	T inf;
	int next;
	int prev;
public:
	Node<T>()
	{
	    inf = T();
		next = -1;
		prev = -1;
	}
	Node<T>(T newValue, int newNext, int newPrev)
	{
		inf = newValue;
		next = newNext;
		prev = newPrev;
	}

	Node<T>(T newValue)
	{
	    inf=newValue;
	    next=prev=-1;
	}

	virtual ~Node<T>()
	{

	}

	T getInf()
	{
		return inf;
	}

	int getNext()
	{
		return next;
	}

	int getPrev()
	{
		return prev;
	}

	void setInf(T newInf)
	{
		inf = newInf;
	}

	void setNext(int newNext)
	{
		next = newNext;
	}

	void setPrev(int newPrev)
	{
		prev = newPrev;
	}
};

#endif // NODE_H_INCLUDED
