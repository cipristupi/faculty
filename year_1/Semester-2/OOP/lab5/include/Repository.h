#ifndef REPOSITORY_H
#define REPOSITORY_H
#include "Expense.h"
#include "DynamicVector.h"
#include "DynamicArray.h"
#include <vector>
#include "Validator.h"
using namespace std;

class Repository
{
    public:
        Repository();
        virtual ~Repository();
        //Antal methods
        void storeRepo(Expense* ex);
        void deleteExpense(int id);
        int getSizeRepo();
        //New added methods
        Expense* getByID(int id);
        DynamicArray<Expense*>* getAll();
        void undo();
        void modifyExpense(int id, Expense* ex);


    protected:
    private:
        /*VectorDinamic<Expense>* v;
        VectorDinamic<Expense>* backup_list;*/

        DynamicArray<Expense*>* v;
        DynamicArray<Expense*>* backup;
        void createBackup();
        void restoreBackup();
};

#endif // REPOSITORY_H
