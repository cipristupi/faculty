#ifndef CONTROLLER_H
#define CONTROLLER_H

#include "string"
#include "Repository.h"
#include "Expense.h"
#include "DynamicArray.h"
#include "Validator.h"
using namespace std;

class Controller
{
    public:
        Controller();
        Controller(Repository* r);
        virtual ~Controller();
        int addExpenseCtr(int id, int month, int day, float value, string category);
        void modifyExpenseCtr(int id, float nvalue);
        void deleteExpenseCtr(int id);
        int getSizeCtr();

        //New methods
        int validator(Expense *ex);
        Expense* getByIDCtr(int id);
        void undoCtr();
        DynamicArray<Expense*>* getAllCtr();
        DynamicArray<Expense*>* getAllOrderDay();
        DynamicArray<Expense*>* getAllOrderCategory();
        DynamicArray<Expense*>* getbyAmountGreater(float value);
        DynamicArray<Expense*>* getbyCategory(string category);
        DynamicArray<Expense*>* getByCatAmount(float value, string category);

    protected:
    private:
        Repository* repo;
};

#endif // CONTROLLER_H
