#ifndef VALIDATOR_H
#define VALIDATOR_H

#include "Expense.h"
#include <string>
#include <exception>

class Validator
{
    public:
        /*Validator();
        virtual ~Validator();*/
        void validate(Expense* ex);
    protected:
    private:
};

/*
class ExpenseException
{
    public:
        ExpenseException(string msg)
        {
            this->msg=msg;
        }
    private:
        string msg;
};

class ValidatorException: public ExpenseException
{
    public:
        ValidatorException(string msg)
        {
            ExpenseException(msg);
        }
};*/

class ExpenseException : public exception
{
    public:
    ExpenseException(string m) : msg(m) {}
    ~ExpenseException() throw() {}
    const char* what() const throw() { return msg.c_str(); }

    private:
    string msg;
};
#endif // VALIDATOR_H
