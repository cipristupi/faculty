#ifndef DYNAMICVECTOR_H_INCLUDED
#define DYNAMICVECTOR_H_INCLUDED

#include <stdlib.h>
#define INIT_CAPACITY 10

template <class Element>
class VectorDinamic
{
        Element* elems;
        int lg;
        int capacitate;

    public:
        VectorDinamic()
        {
            elems = new Element[INIT_CAPACITY];
            this->capacitate = INIT_CAPACITY;
            this->lg = 0;
        }

        /**
        * Adds more memory to the vector
        */
        void resize()
        {
            int nCap = this->capacitate + this->capacitate / 2;
            Element* nElems = new Element[nCap];
            int i;
            for (i = 0; i < this->lg; i++) {
                nElems[i] = this->elems[i];
            }

            free(this->elems);

            this->elems = nElems;
            this->capacitate = nCap;
        }

        /**
        * Adds an element in vector
        * v - dynamic vector
        * el - element to add
        */
        void add(Element el)
        {
            if (this->lg == this->capacitate)
            {
                resize();
            }
            elems[lg] = el;
            lg++;
        }

        /**
        * Returns element from vector from given position
        * v - vector
        * poz - position, poz>=0
        * returns element from position poz
        */
        Element* get(int poz)
        {
            return &this->elems[poz];
        }

        /**
        * Number of elements
        */
        int getNrElements()
        {
            return this->lg;
        }

        void Swap(int poz1, int poz2)
        {
            Element aux;
            aux = this->elems[poz1];
            this->elems[poz1] = this->elems[poz2];
            this->elems[poz2] = aux;
        }
};

#endif // DYNAMICVECTOR_H_INCLUDED
