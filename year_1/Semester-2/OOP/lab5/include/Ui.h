#ifndef UI_H
#define UI_H

#include <iostream>
#include "Repository.h"
#include "Controller.h"
#include "Expense.h"
#include "Validator.h"
using namespace std;

class Ui
{
    public:
        Ui();
        Ui(Controller ctr);
        void startUI();
        virtual ~Ui();
    protected:
    private:
        Controller cont;
        void printMainMenu();
        void addNewExpense();
        void printAll();
        void getByID();
        void printNoExp();
        void uptValue();
        void sortByDay();
        void sortByCategory();
        void removeExpU();
        void getByAmount();
        void printExpense(Expense* ex);
        void getByCategory();
        void getByCatAm();
};

#endif // UI_H
