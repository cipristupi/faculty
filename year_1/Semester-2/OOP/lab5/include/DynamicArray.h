#ifndef DYNAMICARRAY_H_INCLUDED
#define DYNAMICARRAY_H_INCLUDED

template<typename Element>
class DynamicArray
{
    public:
    DynamicArray();
    ~DynamicArray();
    DynamicArray& operator=(const DynamicArray& ot);
    void addE(Element r);
    Element deleteElem(int poz);
    Element get(int poz);
    Element& getR(int poz);//return reference
    int getSize();
    void clear();
    void Swap(int poz1, int poz2);

    private:
        Element *elems;
        int capacity;
        int size;
        void ensureCapacity(int nrElems);
};

template<typename Element>
DynamicArray<Element>::DynamicArray()
{
    capacity=10;
    elems=new Element[capacity];
    size =0;
}
template<typename Element>
DynamicArray<Element>::~DynamicArray()
{
    delete[] elems;
}

template<typename Element>
DynamicArray<Element>& DynamicArray<Element>::operator=(
		const DynamicArray<Element>& ot) {
	if (this == &ot) {
		// protect against self-assignment (a = a)
		return *this;
	}
	//delete the allocated memory
	delete this->elems;

	this->elems = new Element[ot.capacity];
	for (int i = 0; i < ot.size; i++) {
		this->elems[i] = ot.elems[i];
	}
	this->capacity = ot.capacity;
	this->size = ot.size;
	return *this;
}

template<typename Element>
void DynamicArray<Element>::ensureCapacity(int nrElems)
{
    if(capacity >=nrElems)
    {
        return;
    }
    Element *aux = elems;
    capacity = capacity *2;
    elems = new Element[capacity];
    for(int i=0;i<size;i++)
    {
        elems[i]=aux[i];
    }
    delete[] aux;
}

template<typename Element>
void DynamicArray<Element>::addE(Element r)
{
	ensureCapacity(size + 1);
	elems[size] = r;
	size++;
}

template<typename Element>
Element DynamicArray<Element>::get(int poz) {
	return elems[poz];
}

template<typename Element>
Element& DynamicArray<Element>::getR(int poz) {
	return elems[poz];
}

template<typename Element>
int DynamicArray<Element>::getSize() {
	return size;
}

template<typename Element>
void DynamicArray<Element>::clear() {
	size = 0;
}

template<typename Element>
Element DynamicArray<Element>::deleteElem(int poz) {
	Element rez = elems[poz];
	for (int i = poz; i < size - 1; i++) {
		elems[i] = elems[i + 1];
	}
	size--;
	return rez;
}
template<typename Element>
void DynamicArray<Element>::Swap(int poz1, int poz2)
{
		Element aux;
		aux = this->elems[poz1];
		this->elems[poz1] = this->elems[poz2];
		this->elems[poz2] = aux;
	}

#endif // DYNAMICARRAY_H_INCLUDED
