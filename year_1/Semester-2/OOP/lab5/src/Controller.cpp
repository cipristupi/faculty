#include "Controller.h"

Controller::Controller()
{
    //ctor
}

Controller::~Controller()
{
    //dtor
}
/** @brief (one liner)
  *
  * (documentation goes here)
  */
 Controller::Controller(Repository* r)
{
    repo=r;
}

int Controller::getSizeCtr()
{
    return repo->getSizeRepo();
}
/** @brief (one liner)
  *
  * (documentation goes here)
  */
void Controller::deleteExpenseCtr(int id) //Here return type need to be changed
{
    if(getByIDCtr(id)==NULL)
        throw(ExpenseException("The given ID doesn't exists.\n"));
    repo->deleteExpense(id);
}




/** @brief (one liner)
  *
  * (documentation goes here)
  */
void Controller::modifyExpenseCtr(int id, float nvalue)//Return type need to be changed
{
    Expense* ex;
    if(getByIDCtr(id)==NULL)
        throw(ExpenseException("The given ID doesn't exists.\n"));
    ex=getByIDCtr(id);
    ex->setValue(nvalue);
    repo->modifyExpense(id,ex);
}

/** @brief (one liner)
  *
  * (documentation goes here)
  */
int Controller::addExpenseCtr(int id, int month, int day, float value, string category)
{
    Expense* ex=new Expense(id,month,day,value,category);
    repo->storeRepo(ex);
    return repo->getSizeRepo();
}

DynamicArray<Expense*>* Controller::getAllCtr()
{
    return repo->getAll();
}

Expense* Controller::getByIDCtr(int id)
{
    return repo->getByID(id);
}


DynamicArray<Expense*>* Controller::getAllOrderDay()
{
    DynamicArray<Expense*>* res=new DynamicArray<Expense*>();
    DynamicArray<Expense*>* all;
    all = repo->getAll();
    Expense* ex1;
    Expense* ex2;
    int nrElems;
    nrElems= all->getSize();
    for(int i=0;i<nrElems;i++)
    {
        res->addE(all->get(i));
    }

    for(int i=0;i<nrElems-1;i++)
        for(int j=i+1;j<nrElems;j++)
        {
            ex1=res->get(i);
            ex2=res->get(j);
            if(ex1->getDay() > ex2->getDay())//crescator
            {
                res->Swap(i,j);
            }
        }
    return res;
}

DynamicArray<Expense*>* Controller::getAllOrderCategory()
{
    DynamicArray<Expense*>* res=new DynamicArray<Expense*>();
    DynamicArray<Expense*>* all;
    all = repo->getAll();
    Expense* ex1;
    Expense* ex2;
    int nrElems;
    nrElems= all->getSize();
    for(int i=0;i<nrElems;i++)
    {
        res->addE(all->get(i));
    }
    for(int i=0;i<nrElems-1;i++)
        for(int j=i+1;j<nrElems;j++)
        {
            ex1=res->get(i);
            ex2=res->get(j);
            if(ex1->getCategory() > ex2->getCategory())//crescator
            {
                res->Swap(i,j);
            }
        }
    return res;
}

DynamicArray<Expense*>* Controller::getbyAmountGreater(float value)
{
    DynamicArray<Expense*>* v= repo->getAll();
    DynamicArray<Expense*>* rez = new DynamicArray<Expense*>();
    int nrElems;
    nrElems = v->getSize();
    int i;
    for(i=0;i<nrElems;i++)
    {
        Expense* ex = v->get(i);
        if(ex->getValue() > value)
        {
            rez->addE(ex);
        }
    }
    return rez;
}

DynamicArray<Expense*>* Controller::getbyCategory(string category)
{
    DynamicArray<Expense*>* v= repo->getAll();
    DynamicArray<Expense*>* rez = new DynamicArray<Expense*>();
    int nrElems = v->getSize();
    int i;
    for(i=0;i<nrElems;i++)
    {
        Expense* ex = v->get(i);
        if(ex->getCategory() == category)
        {
                rez->addE(ex);
        }
    }
    return rez;
}


void Controller::undoCtr()
{
    repo->undo();
}

DynamicArray<Expense*>* Controller::getByCatAmount(float value, string category)
{
    DynamicArray<Expense*>* v= repo->getAll();
    DynamicArray<Expense*>* rez = new DynamicArray<Expense*>();
    int nrElems = v->getSize();
    int i;
    for(i=0;i<nrElems;i++)
    {
        Expense* ex = v->get(i);
        if(ex->getCategory() == category && ex->getValue() > value)
        {
                rez->addE(ex);
        }
    }
    return rez;
}

