#include "Expense.h"
#include <iostream>


Expense::Expense()
{
    //ctor
}

Expense::Expense(int i, int m, int d, float v, string c)
{
    id=i;
    month = m;
    day = d;
    value = v;
    category = c;
}

Expense::~Expense()
{
    //dtor
}

/** @brief (one liner)
  *
  * (documentation goes here)
  */
void Expense::setCategory(string ncategory)
{
    category = ncategory;
}

/** @brief (one liner)
  *
  * (documentation goes here)
  */
void Expense::setValue(float nvalue)
{
    this->value=nvalue;
}

/** @brief (one liner)
  *
  * (documentation goes here)
  */
void Expense::setDay(int nday)
{
    day=nday;
}

/** @brief (one liner)
  *
  * (documentation goes here)
  */
void Expense::setMonth(int nmonth)
{
    month=nmonth;
}

/** @brief (one liner)
  *
  * (documentation goes here)
  */
void Expense::setID(int nid)
{
    id=nid;
}

/** @brief (one liner)
  *
  * (documentation goes here)
  */
string Expense::getCategory()
{
    return category;
}

/** @brief (one liner)
  *
  * (documentation goes here)
  */
float Expense::getValue()
{
    return value;
}

/** @brief (one liner)
  *
  * (documentation goes here)
  */
int Expense::getDay()
{
    return day;
}

/** @brief (one liner)
  *
  * (documentation goes here)
  */
int Expense::getMonth()
{
    return month;
}

/** @brief (one liner)
  *
  * (documentation goes here)
  */
int Expense::getID()
{
    return id;
}

/** @brief (one liner)
  *
  * (documentation goes here)
  */
Expense Expense::copyExpense(Expense ex)
{

}


