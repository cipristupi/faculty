#include "Repository.h"
#include<iostream>
#include "Expense.h"


Repository::Repository()
{
   v=new DynamicArray<Expense*>();
   backup=new DynamicArray<Expense*>();
}

Repository::~Repository()
{
    //dtor
}


int Repository::getSizeRepo()
{
    return v->getSize();
}


/** @brief (one liner)
  *
  * (documentation goes here)
  */
void Repository::undo()
{
    restoreBackup();
}


/** @brief (one liner)
  *
  * (documentation goes here)
  */
DynamicArray<Expense*>* Repository::getAll()
{
    return v;
}

/** @brief (one liner)
  *
  * (documentation goes here)
  */
Expense* Repository::getByID(int id)
{
    for(int i=0;i<v->getSize();i++)
    {
        if(v->get(i)->getID()==id)
        {
            return v->get(i);
        }
    }
    return NULL;
}



/** @brief (one liner)
  *
  * (documentation goes here)
  */
void Repository::deleteExpense(int id)
{
    createBackup();
    DynamicArray<Expense*>* res=new DynamicArray<Expense*>();
    //v=new DynamicArray<Expense*>();
    Expense* ex;
    for(int i=0;i<v->getSize();i++)
    {
        ex=v->get(i);
        if(ex->getID()!=id)
        {
            res->addE(ex);
        }
    }
    v=res;
}



/** @brief (one liner)
  *
  * (documentation goes here)
  */
void Repository::modifyExpense(int id, Expense* ex)
{
    createBackup();
    int i,nrElem;
    float nvalue;
    nrElem=getSizeRepo();
    nvalue = ex->getValue();
    for(i=0;i<nrElem;i++)
    {
        Expense* ex =v->get(i);
        if(ex->getID()==id)
        {

            v->getR(i)->setValue(nvalue);
        }
    }
}

/** @brief (one liner)
  *
  * (documentation goes here)
  */
void Repository::storeRepo(Expense* ex)
{
    Validator val;
    val.validate(ex);
    if(getByID(ex->getID())!=NULL)
        throw(ExpenseException("The same ID exists.\n"));
    createBackup();
    v->addE(ex);
}

void Repository::createBackup()
{
    //backup=v;
    backup->clear();
    int nrElem;
    nrElem=v->getSize();
    for(int i=0;i<nrElem;i++)
    {
        Expense* ex =v->get(i);
        backup->addE(ex);
    }
}

void Repository::restoreBackup()
{
    //v=backup;
    int nrElem;
    v->clear();
    nrElem=backup->getSize();
    for(int i=0;i<nrElem;i++)
    {
        Expense* ex =backup->get(i);
        v->addE(ex);
    }
}



