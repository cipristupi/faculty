#include "Validator.h"
using namespace std;

/*void Validator::validate(Expense* ex) throw (ValidatorException)
{
    //if()
}*/



/*Validator::Validator()
{

}

Validator::~Validator()
{

}*/

void Validator::validate(Expense* ex)
{
    string err;
    int arr[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    if(ex->getID() < 0 )
    {
        err+="The id cannot be negative;\n";
    }
    if(ex->getCategory().empty())
    {
        err+="The category cannot be empty.\n";
    }
    if(ex->getMonth()<1 || ex->getMonth()>12)
    {
        err+="The month is not correct.\n";
    }
    else
    {
        if(ex->getDay()<1 || ex->getDay() > arr[ex->getMonth()])
        {
            err+="Invalid day.\n";
        }
    }
    if(!err.empty())
    {
        throw(ExpenseException(err));
    }
}

