#include "Ui.h"
#include <iostream>
using namespace std;

Ui::Ui()
{
    //ctor
}

Ui::~Ui()
{
    //dtor
}

/** @brief (one liner)
  *
  * (documentation goes here)
  */
 Ui::Ui(Controller ctr)
{
    cont=ctr;
}


void Ui::startUI()
{
        int cmd;
        printMainMenu();

        do
        {
            cout<<"Command: ";
            cin>>cmd;
            switch(cmd)
            {
                case 1:
                {
                        addNewExpense();
                        break;
                }
                case 2:
                {
                        //cout<<"2" <<endl;
                        printAll();
                        break;
                }
                case 3:
                {
                        uptValue();
                        break;
                }
                case 4:
                {
                        removeExpU();
                        break;
                }
                case 5:
                {
                        sortByDay();
                        break;
                }
                case 6:
                {
                        sortByCategory();
                        break;
                }
                case 7:
                {
                        //printf("7 \n");
                        //undoModification();
                        cont.undoCtr();
                        break;
                }
                case 8:
                    {
                        getByID();
                        break;
                    }
                case 9:
                    {
                        printNoExp();
                        break;
                    }
                case 10:
                    {
                        printMainMenu();
                        break;
                    }
                case 11:
                    {
                        getByAmount();
                        break;
                    }
                case 12:
                    {
                        getByCategory();
                        break;
                    }
                case 13:
                    {
                        getByCatAm();
                        break;
                    }
            }

        }while(cmd);

}

void Ui::printMainMenu()
{
    cout<<"Main menu: \n";
    cout<<"-------------------------------- \n";
    cout<<"1 - To add a new expense. \n"; //Work
    cout<<"2 - To show all expenses. \n"; //Work
    cout<<"3 - To modify value of an expense. \n";
    cout<<"4 - To remove an expense. \n"; //Work
    cout<<"5 - To sort the list ascending after days. \n"; //Work
    cout<<"6 - To sort the list ascending after category. \n"; //Work
    cout<<"7 - To undo. \n";//Work
    cout<<"8 - To get expense by id. \n"; //Work
    cout<<"9 - To get number of expenses. \n"; //Work
    cout<<"10 - To print the menu. \n"; //Work
    cout<<"11 - Filter greater by amount.\n"; //Work
    cout<<"12 - Filter by category.\n"; //Work
    cout<<"13 -Filter by category and greater by amount.\n";
    cout<<"0 - To exit. \n"; //Work
    cout<<"-------------------------------- \n";
}

void Ui::addNewExpense()
{
    int month,day,id;
    float value;
    string category;
    cout<<"ID: ";
    cin>>id;
    cout<<"Month: ";
    cin>>month;
    cout<<"Day: ";
    cin>>day;
    cout<<"Value: ";
    cin>>value;
    cout<<"Category: ";
    cin>>category;
    try
    {
        int noExpense = cont.addExpenseCtr(id,month,day,value,category);
        cout<< noExpense<<" expenses \n";
    }
    catch(ExpenseException& e)
    {
        cout << e.what() << endl;
    }

}

void Ui::printExpense(Expense* ex)
{
    cout<<"ID: " << ex->getID() <<endl;
    cout<<"Month: " << ex->getMonth() <<endl;
    cout<<"Day: " << ex->getDay() <<endl;
    cout<<"Value: " << ex->getValue() <<endl;
    cout<<"Category: "<< ex->getCategory() <<endl;
}


void Ui::printAll()
{
    DynamicArray<Expense*>* all= cont.getAllCtr();
    for(unsigned int i=0;i<all->getSize();i++)
    {
        Expense* ex =all->get(i);
        printExpense(ex);
        cout<<"-------------------------------------"<<endl;
    }
}

void Ui::getByID()
{
    int id;
    Expense *ex;
    cout<<"ID: ";
    cin>>id;
    ex=cont.getByIDCtr(id);
    if(ex==NULL)
    {
        cout<<"Expenses doesn't exist\n";
    }
    else
    {
        printExpense(ex);
    }
    cout<<endl;
}

void Ui::printNoExp()
{
    int n;
    n = cont.getSizeCtr();
    cout<<n << " expenses."<<endl;
}

void Ui::uptValue()
{
    int id;
    float nvalue;
    cout<<"ID: ";
    cin>>id;
    cout<<"New Value: ";
    cin>>nvalue;
    try
    {
        cont.modifyExpenseCtr(id,nvalue);
    }
    catch(ExpenseException& e)
    {
        cout << e.what() << endl;
    }
}

void Ui::sortByDay()
{
    DynamicArray<Expense*>* all= cont.getAllOrderDay();
    for(unsigned int i=0;i<all->getSize();i++)
    {
        Expense* ex =all->get(i);
        printExpense(ex);
        cout<<"-------------------------------------"<<endl;
    }
}

void Ui::sortByCategory()
{
    DynamicArray<Expense*>* all= cont.getAllOrderCategory();
    for(unsigned int i=0;i<all->getSize();i++)
    {
        Expense* ex =all->get(i);
        printExpense(ex);
        cout<<"-------------------------------------"<<endl;
    }
}

void Ui::removeExpU()
{
    int id;
    cout<<"ID: ";
    cin>>id;
    try
    {
        cont.deleteExpenseCtr(id);
    }
    catch(ExpenseException& e)
    {
        cout << e.what() << endl;
    }
}

void Ui::getByAmount()
{
    float value;
    cout<<"Value: ";
    cin>>value;
    DynamicArray<Expense*>* all= cont.getbyAmountGreater(value);
    for(unsigned int i=0;i<all->getSize();i++)
    {
        Expense* ex =all->get(i);
        printExpense(ex);
        cout<<"-------------------------------------"<<endl;
    }
}


void Ui::getByCategory()
{
    string category;
    cout<<"Category: ";
    cin>>category;
    DynamicArray<Expense*>* all= cont.getbyCategory(category);
    for(unsigned int i=0;i<all->getSize();i++)
    {
        Expense* ex =all->get(i);
        printExpense(ex);
        cout<<"-------------------------------------"<<endl;
    }
}

void Ui::getByCatAm()
{
    string category;
    float value;
    cout<<"Category: ";
    cin>>category;
    cout<<"Value: ";
    cin>>value;
    DynamicArray<Expense*>* all= cont.getByCatAmount(value,category);
    for(unsigned int i=0;i<all->getSize();i++)
    {
        Expense* ex =all->get(i);
        printExpense(ex);
        cout<<"-------------------------------------"<<endl;
    }
}

