#include <stdio.h>
#include <stdlib.h>
//9. Print the exponent of a prime number p from the decomposition in prime factors of a
//given number N = 1*2*...*n (n is a non-null natural number).

void printDiv(int n)
{
    int i,k;
    i=2;
    while(n!=1)
    {
        if(n%i==0)
        {
            k=0;
            while(n%i==0)
            {
                k++;
                n/=i;
            }
            printf("%d at power %d \n",i,k);
        }
        i++;
    }
    //return;
}
void printDivPow(int n, int x)
{
    int k;
    //i=2;
    if(EPrim(x)==1)
    {
        if(n%x==0)
        {
            k=0;
            while(n%x==0)
            {
                k++;
                n/=x;
            }
            printf("%d at power %d \n",x,k);
        }
    }
    else
        printf("Provide please a prime number.\n");
}
int EPrim(int x)
{
    int i;
    if(x==0||x==1)
        return 0;
    else
        if(x==2)
            return 1;
        else
            if(x%2==0)
                return 0;
            else
            {
                for(i=3;i<=x/2;i++)
                    if(x%i==0)
                        return 0;
            }
    return 1;
}
void menu()
{
    int cmd,n,p;
    printf("Available commands:\n");
    printf("1 - for decompose in prime factors. \n");
    printf("2 - print the exponent of a prime number p from the decomposition in prime factors. \n");
    printf("0 - exit. \n");
    do
    {
        printf("Command: ");
        scanf("%d",&cmd);
        printf("\n");
        switch(cmd)
        {
            case(1):
            {
                printf("Value(n): ");
                scanf("%d",&n);
                printf("\n");
                printDiv(n);
                printf("\n");
                break;
            }
            case(2):
                {
                    printf("Value(n): ");
                    scanf("%d",&n);
                    printf("\n");
                    printf("Prime number(p): ");
                    scanf("%d",&p);
                    printf("\n");
                    printDivPow(n,p);
                    printf("\n");
                    break;
                }

        }

    }while(cmd);

}

int main()
{
    menu();
    return 0;
}
