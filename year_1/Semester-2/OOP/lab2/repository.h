#ifndef REPOSITORY_H_INCLUDED
#define REPOSITORY_H_INCLUDED

#include "darray.h"

typedef struct
{
    DArray darray;
}Repository;

void repositoryInit(Repository*);
void repositoryDestroy(Repository*);

void repositoryAdd(Repository* r, Expense ex);
void repositoryUpdate(Repository*,int,float); // recheck this function
void repositoryDelete(Repository*,int);
int getLengthR(Repository*);

Expense repositoryGetByIndex(const Repository*,int);
Expense repositoryGetByCategory(const Repository*, char*);
Expense repositoryGetByID(const Repository*, int);
Expense* repositoryGetAll(const Repository*);
#endif // REPOSITORY_H_INCLUDED
