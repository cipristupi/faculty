#ifndef DARRAY_H_INCLUDED
#define DARRAY_H_INCLUDED

#include "expense.h"

#define DIM 10

typedef struct
{
    Expense elems[DIM];
    int len;
}DArray;

void darrayInit(DArray*);
void darrayDestroy(DArray*);
int darrayGetLength(const DArray*);
void darrayAdd(DArray*, Expense);

Expense darrayGetElemAt(const DArray*, int);
int darrayIndexOf(const DArray*, Expense);
void darrayRemoveAt(DArray*, int);
int darrayRemove(DArray*, Expense);

#endif // DARRAY_H_INCLUDED
