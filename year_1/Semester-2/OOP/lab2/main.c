#include <stdio.h>
#include <stdlib.h>

#include "expense.h"
#include "controller.h"
#include "darray.h"
#include "repository.h"
#include "controller.h"
#include "test_domain.h"
#include "ui.h"

void testAll()
{
    testDomain();
}

int main()
{
    testAll();
    Expense ex;
    Console console;
    consoleInit(&console);
    consoleRun(&console);
    consoleDestroy(&console);
    printf("GoodBye\n");
    return 0;
}
