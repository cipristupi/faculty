#include "controller.h"

void controllerInit(Controller* c)
{
    repositoryInit(&c->repo);
}

void controllerDestroy(Controller* c)
{
    repositoryDestroy(&c->repo);
}

void controllerAddExpense(Controller* c,int id, int month, int day, float value, char* category)
{
    Expense ex;
    expenseInit(&ex,id,month,day,value,category);
    repositoryAdd(&c->repo,ex);
    //here we don't need a destroy????
}

Expense controllerGetByCategory(const Controller* c, char* category)
{
    return repositoryGetByCategory(&c->repo,category);
}

Expense controllerGetByID(const Controller* c, int id)
{
    return repositoryGetByID(&c->repo,id);
}

void controllerRemoveExpense(Controller* c, int id)
{
    repositoryDelete(&c->repo,id);
}

Expense* controllerGetAll(const Controller* c)
{
    return repositoryGetAll(&c->repo);
}

int getLength(const Controller* c)
{
    //return darraGetLenght(&c->repo);
    return getLengthR(&c->repo);
}


Expense controllerGetByIndex(const Controller* c, int ind)
{
    return repositoryGetByIndex(&c->repo,ind);
}
