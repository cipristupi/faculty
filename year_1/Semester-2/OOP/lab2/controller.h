#ifndef CONTROLLER_H_INCLUDED
#define CONTROLLER_H_INCLUDED

#include "repository.h"

typedef struct
{
    Repository repo;
}Controller;

void controllerInit(Controller*);
void controllerDestroy(Controller*);

void controllerAddExpense(Controller*,int,int, int, float, char* );
void controllerRemoveExpense(Controller*,int);
void controllerUpdate(Controller*,int,float);
//void controllerRemoveExpense(Controller*,int);
int getLength(const Controller*);

Expense controllerGetByIndex(const Controller*, int);
Expense controllerGetByCategory(const Controller*, char*);
Expense controllerGetByID(const Controller*,int);
Expense* controllerGetAll(const Controller*);

#endif // CONTROLLER_H_INCLUDED
