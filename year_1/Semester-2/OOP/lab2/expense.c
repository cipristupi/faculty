#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include "expense.h"

void expenseInit(Expense* ex,int id, int month, int day, float value, char* category)
{
    ex->id=id;
    ex->month = month;
    ex->day = day;
    ex->value = value;
    //ex->category = strdup(category);
    strcpy(ex->category,category);
}

void expenseDestroy(Expense* ex)
{
    /*free(ex->category);
    ex->month = 0;
    ex->day = 0;
    ex->value = 0;
    ex->id=0;*/
    free(ex);
}

int getID(const Expense* ex)
{
    return ex->id;
}
int getMonth(const Expense* ex)
{
    return ex->month;
}


int getDay(const Expense* ex)
{
    return ex->day;
}


float getValue(const Expense* ex)
{
    return ex->value;
}


char* getCategory(const Expense* ex)
{
    return ex->category;
}

//Setters from here
void setID(Expense* ex,int nid)
{
    ex->id=nid;
}

void setMonth(Expense* ex, int nmonth)
{
    ex->month=nmonth;
}
void setDay(Expense* ex,int nday)
{
    ex->day=nday;
}
void setValue(Expense* ex,float nvalue)
{
    ex->value=nvalue;
}

void setCategory(Expense* ex, char* ncategory)
{
    free(ex->category);
    //ex->category=strdup(ncategory); // little error here
    strcpy(ex->category,ncategory);
}

void expenseClone(Expense* ex1, Expense ex2)
{
    ex1->id=ex2.id;
    ex1->month = ex2.month;
    ex1->day= ex2.day;
    ex1->value = ex2.value;
    //ex1->category =strdup(ex2.category);
    strcpy(ex1->category,ex2.category);
}

