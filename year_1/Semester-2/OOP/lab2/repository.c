#include "repository.h"

void repositoryInit(Repository* r)
{
    darrayInit(&r->darray);
}

void repositoryDestroy(Repository* r)
{
    darrayDestroy(&r->darray);
}

void repositoryAdd(Repository* r, Expense ex)
{
    darrayAdd(&r->darray,ex);
}

Expense repositoryGetByCategory(const Repository *r, char* category)
{
    int i;
    for(i=0;i< darrayGetLength(&r->darray);i++)
    {
        Expense ex = darrayGetElemAt(&r->darray,i);
        char* catName = getCategory(&ex);
        if(strcmp(catName,category)==0)
        {
            return darrayGetElemAt(&r->darray,i);
        }
    }
    Expense ex;
    return ex;
}

void repositoryDelete(Repository* r, int id)
{

}

Expense repositoryGetByID(const Repository *r, int id)
{
    int i;
    int idex;
    for(i=0;i< darrayGetLength(&r->darray);i++)
    {
        Expense ex = darrayGetElemAt(&r->darray,i);
        idex= getID(&ex);
        if(id == idex)
        {
            return darrayGetElemAt(&r->darray,i);
        }
    }
    Expense ex;
    return ex;
}

Expense* repositoryGetAll(const Repository* r)
{

}

int getLengthR(Repository* r)
{
    return darrayGetLength(&r->darray);
}

Expense repositoryGetByIndex(const Repository* r,int ind)
{
    return darrayGetElemAt(&r->darray,ind);
}
