#ifndef EXPENSE_H_INCLUDED
#define EXPENSE_H_INCLUDED

typedef struct
{
    int id;
    int month;
    int day;
    float value;
    char category[20];
} Expense;

void expenseInit(Expense*,int,int,int,float,char*);
void expenseDestroy(Expense*);
//Getters here
int getID(const Expense*);
int getMonth(const Expense*);
int getDay(const Expense*);
float getValue(const Expense*);
char* getCategory(const Expense*);
//Setters here
void setID(Expense*,int);
void setMonth(Expense*, int);
void setDay(Expense* ,int);
void setValue(Expense* ,float);
void setCategory(Expense* ,char*);

void expenseClone(Expense*, Expense);

#endif // EXPENSE_H_INCLUDED
