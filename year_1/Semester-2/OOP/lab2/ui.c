#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include "ui.h"

void printMainMenu()
{
    printf("Main menu: \n");
    printf("-------------------------------- \n");
    printf("1 - To add a new expense. \n");
    printf("2 - To show all expenses. \n");
    printf("3 - To modify value of an expense. \n");
    printf("4 - To remove an expense. \n");
    printf("5 - To sort the list ascending after days. \n");
    printf("6 - To sort the list ascending after category. \n");
    printf("7 - To undo. \n");
    printf("8 - To get expense by id. \n");
    printf("0 - To exit. \n");
    printf("-------------------------------- \n");
}

void addNewExpense(Console* c)
{
    int month,day,id;
    float value;
    char category[20];
    printf("ID: ");
    scanf("%d",&id);
    printf("Month: ");
    scanf("%d",&month);
    printf("Day: ");
    scanf("%d",&day);
    printf("Value: ");
    scanf("%f",&value);
    printf("Category: ");
    scanf("%s",category);
    controllerAddExpense(&c->ctrl,id,month,day,value,category);

}
void getByCategory(Console* c)
{
    char  category[100];
    Expense ex;
    printf("Category: ");
    scanf("%s",category);
    ex = controllerGetByCategory(&c->ctrl,category);
    printf("ID: %d Month: %d Day: %d Value: %f Category: %s \n",getID(&ex),getMonth(&ex),getDay(&ex),getValue(&ex),getCategory(&ex));
    expenseDestroy(&ex);
}

void getByID(Console* c)
{
    int id;
    Expense ex;
    printf("ID: ");
    scanf("%d",&id);
    ex = controllerGetByID(&c->ctrl,id);
    printf("ID: %d Month: %d Day: %d Value: %f Category: %s \n",getID(&ex),getMonth(&ex),getDay(&ex),getValue(&ex),getCategory(&ex));
    expenseDestroy(&ex);

}


void addInit(Console* c)
{
    char* categories[] ={"food","medic","book"};
    int days[] ={25,30,20};
    int month[] ={10,9,10};
    int ids[] ={1,2,3};
    float values[] = {300,1,2000};
    int i;
    for(i=0; i<3 ;i++)
    {
        controllerAddExpense(&c->ctrl,ids[i],month[i],days[i],values[i],categories[i]);
    }
}

void printAll(Console* c)
{
    int darrayL,i;
    darrayL = getLength(&c->ctrl);
    Expense ex;
    for(i=0;i<darrayL;i++)
    {
        ex = controllerGetByIndex(&c->ctrl,i);
        printf("ID: %d Month: %d Day: %d Value: %f Category: %s \n",getID(&ex),getMonth(&ex),getDay(&ex),getValue(&ex),getCategory(&ex));
        //expenseDestroy(&ex);
    }
    expenseDestroy(&ex);
}

void removeExpense(Console* c)
{
    int id;
    printf("ID: ");
    scanf("%d",&id);
    controllerRemoveExpense(&c->ctrl,id);
}

void consoleInit(Console* c)
{
    controllerInit(&c->ctrl);
}
void consoleDestroy(Console* c)
{
    controllerDestroy(&c->ctrl);
}

void consoleRun(Console* c)
{
    int cmd;
    printMainMenu();
    addInit(c);
    do
    {
        printf("Command: ");
        scanf("%d",&cmd);
        switch(cmd)
        {
            case 1:
            {
                    //printf("1 \n");
                    addNewExpense(c);
                    break;
            }
            case 2:
            {
                    //printf("2 \n");
                    //getByCategory(c);
                    printAll(c);
                    break;
            }
            case 3:
            {
                    printf("3 \n");
                    break;
            }
            case 4:
            {
                    printf("4 \n");
                    break;
            }
            case 5:
            {
                    printf("5 \n");
                    break;
            }
            case 6:
            {
                    printf("6 \n");
                    break;
            }
            case 7:
            {
                    printf("7 \n");
                    break;
            }
            case 8:
                {
                    getByID(c);
                }
        }

    }while(cmd);
}
