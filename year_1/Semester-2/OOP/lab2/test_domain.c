#include <string.h>
#include <assert.h>
#include "test_domain.h"

#ifndef Expense_Init
#define Expense_Init Expense ex; expenseInit(&ex,1,1,1,0,"");

static void testInitDestroy()
{

    Expense ex;
    expenseInit(&ex,1,10,5,25,"test");
    char* s;
    s=getCategory(&ex);
    assert(strcmp(s,"test")==0);//CHECK here why SEGMENTATION FAULT
    /*printf(getCategory(&ex) );
    printf("\n");*/
    assert(getID(&ex)==1);
    assert(getMonth(&ex)==10);
    assert(getDay(&ex)==5);
    assert(getValue(&ex)==25);
    expenseDestroy(&ex);
}

static void testGetSetID()
{
    Expense_Init
    setID(&ex,5);
    assert(getID(&ex)==5);
    expenseDestroy(&ex);
}
static void testGetSetMonth()
{
    Expense_Init
    setMonth(&ex,5);
    assert(getMonth(&ex)==5);
    expenseDestroy(&ex);
}

static void testGetSetDay()
{
    Expense_Init
    setDay(&ex,25);
    assert(getDay(&ex)==25);
    expenseDestroy(&ex);

}
static void testGetSetValue()
{
    Expense_Init
    setValue(&ex,250);
    assert(getValue(&ex)==250);
    expenseDestroy(&ex);

}

static void testGetSetCategory()
{
    Expense_Init
    setCategory(&ex,"Something");
    assert(strcmp(getCategory(&ex),"Something")==0);
    expenseDestroy(&ex);

}

void testExpense()
{
    testInitDestroy();
    testGetSetMonth();
    testGetSetDay();
    testGetSetValue();
    testGetSetCategory();
}

void testDomain()
{
    testExpense();
}
#endif // Expense_Init
