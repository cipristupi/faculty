#ifndef UI_H_INCLUDED
#define UI_H_INCLUDED

#include "controller.h"

typedef struct
{
    Controller ctrl;
}Console;

void consoleInit(Console*);
void consoleDestroy(Console*);
void consoleRun(Console*);
void printMainMenu();
void addNewExpense(Console*);
void getByCategory(Console*);
void getByID(Console*);
void printAll(Console*);
void addInit(Console*);


#endif // UI_H_INCLUDED
