#include "darray.h"
#include <assert.h>
void darrayInit(DArray* da)
{
	da->len = 0;
}

void darrayDestroy(DArray* da)
{
	da->len = 0;
}

int darrayGetLength(const DArray* da)
{
	return da->len;
}

void darrayAdd(DArray* da, Expense ex)
{
	assert(da->len<DIM);
	Expense ex2;
	expenseClone(&ex2, ex);
	da->elems[da->len++] = ex2;
}

Expense darrayGetElemAt(const DArray* da, int k)
{
	assert(0 <= k && k < da->len);
	return da->elems[k];
}

