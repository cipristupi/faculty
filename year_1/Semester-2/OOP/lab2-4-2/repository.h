#ifndef REPOSITORY_H_INCLUDED
#define REPOSITORY_H_INCLUDED

#include "expense.h"
#include "vectorDinamic.h"

typedef struct
{
    VectorDinamic *v;
    VectorDinamic *backup_list;
}ExpenseRepo;

ExpenseRepo* createRepo();
void destroyRepo(ExpenseRepo* repo);
void save(ExpenseRepo* repo,Expense* ex);
int getNrExpenses(ExpenseRepo* repo);
VectorDinamic* getAll(ExpenseRepo* repo);

//int getIndexByID(ExpenseRepo* repo);
void removeExp(ExpenseRepo* repo, int id);
void updateValueExpense(ExpenseRepo* repo, int id, float nvalue);

void createBackup(ExpenseRepo* repo);
void restoreBackup(ExpenseRepo* repo);
void undo(ExpenseRepo* repo);
#endif // REPOSITORY_H_INCLUDED
