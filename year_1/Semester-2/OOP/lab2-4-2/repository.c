#include "repository.h"
#include "vectorDinamic.h"
#include "expense.h"
#include <assert.h>
#include <stdlib.h>

VectorDinamic *v; //here store the list of objects;

/**
Description: Create a new repository
Output: A repository type object containing 2 vectors
*/
ExpenseRepo* createRepo()
{
    ExpenseRepo *rez = malloc(sizeof(ExpenseRepo));
    rez->v = createDynamicVector();
    rez->backup_list =createDynamicVector();
    return rez;
}

/**
Description: Free the memory for the repository object
*/
void destroyRepo(ExpenseRepo* repo)
{
    destroy(repo->v);
    destroy(repo->backup_list);
    free(repo);
}
/**
Description: Adding a new expense into the vector
Input: Expense object to be added to the vector
*/
void save(ExpenseRepo* repo, Expense* ex)
{
    createBackup(repo);
    add(repo->v,ex);
}

/**
Description: Getter for the number of expenses from the vector
Output: integer number represent number of expense from vector
*/
int getNrExpenses(ExpenseRepo* repo)
{
    return getNrElements(repo->v);
}

/**
Description: Getter for all elements
Output: v-VectorDinamic
*/
VectorDinamic* getAll(ExpenseRepo* repo)
{
    return repo->v;
}

/**
Description: remove an expense with the given ID
Input: ID of the expense
*/
void removeExp(ExpenseRepo* repo,int id)
{
    createBackup(repo);
    VectorDinamic *l =createDynamicVector();
    int i;
    for(i=0;i<repo->v->lg;i++)
    {
        Expense* ex = get(repo->v,i);
        if(getID(ex)!= id)
        {
            add(l,ex);
        }
    }
    repo->v = l;
}

/**
Description: Update the value of an expense with a given id
Input:  ID- id of the expense, nvalue - the new value of the expense
*/
void updateValueExpense(ExpenseRepo* repo, int id,float nvalue)
{
    createBackup(repo);
    int i,nrElem;
    nrElem=getNrElements(repo->v);
    for(i=0;i<nrElem;i++)
    {
        Expense* ex =get(repo->v,i);
        if(getID(ex)==id)
        {
            setValue(repo->v->elems[i],nvalue);
        }
    }
}

/**
Description: Function for create backup of the list after an action
*/
void createBackup(ExpenseRepo* repo)
{
    VectorDinamic *rez = createDynamicVector();
    int i;
    for(i=0;i<repo->v->lg;i++)
    {
        Expense* ex = get(repo->v,i);
        add(rez,copyExpense(ex));
    }
    repo->backup_list = rez;
}

/**
Description: Restore Backup Function
*/
void restoreBackup(ExpenseRepo* repo)
{
    VectorDinamic *rez=createDynamicVector();
    int i;
    for(i=0;i<repo->backup_list->lg;i++)
    {
        Expense* ex=get(repo->backup_list,i);
        add(rez,copyExpense(ex));
    }
    repo->v=rez;
}

/**
Decription: Undo function
*/
void undo(ExpenseRepo* repo)
{
    restoreBackup(repo);
}
