#include "ui.h"
#include "controller.h"
#include <stdlib.h>

void printMainMenu()
{
    printf("Main menu: \n");
    printf("-------------------------------- \n");
    printf("1 - To add a new expense. \n"); //work
    printf("2 - To show all expenses. \n"); //work
    printf("3 - To modify value of an expense. \n");
    printf("4 - To remove an expense. \n"); //work
    printf("5 - To sort the list ascending after days. \n"); //work
    printf("6 - To sort the list ascending after category. \n"); //work
    printf("7 - To undo. \n");
    printf("8 - To get expense by id. \n"); //work
    printf("9 - To get number of expenses. \n"); //work
    printf("10 - To print the menu. \n"); //work
    printf("11 - Filter greater by amount.\n");
    printf("0 - To exit. \n"); //work
    printf("-------------------------------- \n");
}

void addNewExpense(ExpenseCont *cont)
{
    int month,day,id;
    float value;
    char category[20];
    printf("ID: ");
    scanf("%d",&id);
    printf("Month: ");
    scanf("%d",&month);
    printf("Day: ");
    scanf("%d",&day);
    printf("Value: ");
    scanf("%f",&value);
    printf("Category: ");
    scanf("%s",category);
    int noExpense = addExpense(cont,id,month,day,value,category);
    printf("%d expenses \n", noExpense);
}

void printAll(VectorDinamic* v)
{
    int noElems = getNrElements(v);
    int i;
    for(i=0;i<noElems;i++)
    {
        Expense* ex=get(v,i);
        printf("ID: %d Month: %d Day: %d Value: %f Category: %s \n",getID(ex),getMonth(ex),getDay(ex),getValue(ex),getCategory(ex));
    }
    printf("------------------------------\n");
}

void getById(ExpenseCont *cont)
{
    Expense* ex;
    int id;
    printf("ID: ");
    scanf("%d",&id);
    ex = getByID(cont,id);
    if(getID(ex)==0)
        printf("Expense doesn't exist. \n");
    else
        printf("ID: %d Month: %d Day: %d Value: %f Category: %s \n",getID(ex),getMonth(ex),getDay(ex),getValue(ex),getCategory(ex));
    destroyExpense(ex);
}

void printNoExp(ExpenseCont *cont)
{
    int n;
    n= getNrExpenses(cont->repo);
    printf("Number of expenses in memory is %d. \n" , n);
}

void printAllExpenses(ExpenseCont *cont)
{
    VectorDinamic* rez = getAllExpenses(cont);
    printAll(rez);
    //destroy(rez);
}

void uptValue(ExpenseCont *cont)
{
    int id;
    float nvalue;
    printf("ID: ");
    scanf("%d",&id);

    printf("Value: ");
    scanf("%f",&nvalue);
    updateValue(cont,id,nvalue);
    printf("Update done.\n");
}

void sortByDay(ExpenseCont *cont)
{
    VectorDinamic* rez = getAllOrderDay(cont);
    printAll(rez);
    destroy(rez);
}

void sortByCategory(ExpenseCont *cont)
{
    VectorDinamic* rez = getAllOrderCategory(cont);
    printAll(rez);
    destroy(rez);
}

void removeExpU(ExpenseCont *cont)
{
    int id;
    printf("ID: ");
    scanf("%d",&id);
    removeExpense(cont,id);
    printf("Expense removed.\n");
}

void getByAmount(ExpenseCont *cont)
{
    float nvalue;
    printf("Value: ");
    scanf("%f",&nvalue);
    VectorDinamic* rez  = getbyAmountGreater(cont,nvalue);
    printAll(rez);
    destroy(rez);
}

void startUI(ExpenseCont *cont)
{
    int cmd;
        printMainMenu();

        do
        {
            printf("Command: ");
            scanf("%d",&cmd);
            switch(cmd)
            {
                case 1:
                {
                        addNewExpense(cont);
                        break;
                }
                case 2:
                {
                        printAllExpenses(cont);
                        break;
                }
                case 3:
                {
                        uptValue(cont);
                        break;
                }
                case 4:
                {
                        removeExpU(cont);
                        break;
                }
                case 5:
                {
                        sortByDay(cont);
                        break;
                }
                case 6:
                {
                        sortByCategory(cont);
                        break;
                }
                case 7:
                {
                        //printf("7 \n");
                        undoModification(cont);
                        break;
                }
                case 8:
                    {
                        getById(cont);
                        break;
                    }
                case 9:
                    {
                        printNoExp(cont);
                        break;
                    }
                case 10:
                    {
                        printMainMenu();
                        break;
                    }
                case 11:
                    {
                        getByAmount(cont);
                    }
            }

        }while(cmd);
}
