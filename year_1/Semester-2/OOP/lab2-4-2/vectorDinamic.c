#include "vectorDinamic.h"
#include "expense.h"
#include <stdlib.h>

#define INIT_CAPACITY 10

/**
* Initialises the vector
* v vector
* post: vector is empty
*/
VectorDinamic * createDynamicVector()
{
	VectorDinamic *v = malloc(sizeof(VectorDinamic));
	v->elems = malloc(INIT_CAPACITY * sizeof(Element));
	v->capacitate = INIT_CAPACITY;
	v->lg = 0;
	return v;
}
/**
* Empties the memory used by the vector
*/
void destroy(VectorDinamic *v)
{
	int i;
	for (i = 0; i < v->lg; i++) {
		free(v->elems[i]);
	}
	free(v->elems);
	free(v);
}
/**
* Adds more memory to the vector
*/
void resize(VectorDinamic *v)
 {
	int nCap = v->capacitate + v->capacitate / 2;
	Element* nElems = malloc(nCap * sizeof(Element));

	int i;
	for (i = 0; i < v->lg; i++) {
		nElems[i] = v->elems[i];
	}

	free(v->elems);
	v->elems = nElems;
	v->capacitate = nCap;
}

/**
* Adds an element in vector
* v - dynamic vector
* el - element to add
*/
void add(VectorDinamic *v, Element el)
{
	if (v->lg == v->capacitate) {
		resize(v);
	}
	v->elems[v->lg] = el;
	v->lg++;
}

/**
* Returns element from vector from given position
* v - vector
* poz - position, poz>=0
* returns element from position poz
*/
Element get(VectorDinamic *v, int poz)
{
	return v->elems[poz];
}

/**
* Number of elements
*/
int getNrElements(VectorDinamic *v) {
	return v->lg;
}


