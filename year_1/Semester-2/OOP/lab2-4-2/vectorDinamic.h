#ifndef VECTORDINAMIC_H_INCLUDED
#define VECTORDINAMIC_H_INCLUDED


#include <assert.h>

typedef void* Element;

typedef struct {
	Element* elems;
	int lg;
	int capacitate;
} VectorDinamic;

/**
*Creates a dynamic vector
* v vector
* post: vector is empty
*/

VectorDinamic * createDynamicVector();
/**
* Adds an element in vector
* v - dynamic vector
* el - element to add
*/
void add(VectorDinamic *v, Element el);

/**
* Returns element from vector from given position
* v - vector
* poz - position, poz>=0
* returns element from position poz
*/
Element get(VectorDinamic *v, int poz);

/**
* Number of elements
*/
int getNrElements(VectorDinamic *v);

/**
* Empties the memory used by the vector
*/
void destroy(VectorDinamic *v);

#endif // VECTORDINAMIC_H_INCLUDED
