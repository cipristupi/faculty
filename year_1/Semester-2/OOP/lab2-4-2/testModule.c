#include "expense.h"
#include "repository.h"
#include "controller.h"
#include "vectorDinamic.h"
#include "testModule.h"

void testAll()
{
    testController();
    testExpense();
    testRepository();
    testVectorDinamic();
}


void testExpense()
{
    Expense* ex=createExpense(1,1,1,100,"test");
    setID(ex,5);
    assert(getID(ex)==5);

    setMonth(ex,5);
    assert(getMonth(ex)==5);

    setDay(ex,25);
    assert(getDay(ex)==25);

    setValue(ex,250);
    assert(getValue(ex)==250);

    setCategory(ex,"Something");
    assert(strcmp(getCategory(ex),"Something")==0);

    destroyExpense(ex);
}

void testController()
{
    ExpenseRepo *r = createRepo();
    ExpenseCont *c = createController(r);
    Expense* ex;

    //Testing adding expenses
    assert(addExpense(c,5,1,1,1,"one"));
    assert(addExpense(c,6,2,2,2,"two"));
    assert(addExpense(c,7,3,3,3,"three"));
    assert(getNrElements(c->repo->v)==3);

    //Testing changing value
    updateValue(c,5,60);
    assert(ex == getByID(c,5));
    //assert(getValue(ex)==60);

    //Testing removing
    removeExpense(c,5);
    assert(getNrElements(c->repo->v)==2);

    undoModification(c);
    assert(getNrElements(c->repo->v)==3);
    destroyRepo(r);
    destroyController(c);
    destroyExpense(ex);
}

void testRepository()
{
    ExpenseRepo* r=createRepo();
    Expense* ex=createExpense(1,1,1,100,"test");
    save(r,ex);
    assert(getNrElements(r->v)==1);
    destroyExpense(ex);
    destroyRepo(r);
}

void testVectorDinamic()
{
    int ints[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 };

	VectorDinamic *v = createDynamicVector();

	int nrInts = sizeof(ints) / sizeof(int);

	int i;
	for (i = 0; i < nrInts; i++) {
		add(v, &ints[i]);
	}
	assert(getNrElements(v) == nrInts);

	for (i = 0; i < getNrElements(v); i++) {
		int *element = (int *)get(v, i);
			assert(ints[i] == *element);
	}
	destroy(v);
}

