#ifndef EXPENSE_H_INCLUDED
#define EXPENSE_H_INCLUDED

typedef struct
{
    int id;
    int month;
    int day;
    float value;
    char category[20];
}Expense;

/*
Create a expense in the heap
id- id of expense
month -month of the expense
day - day of the expense
value- value of the expense
category - type
*/
Expense* createExpense(int id, int month, int day, float value, char* category);
//Create a copy
Expense* copyExpense(Expense* ex);
//Free the memory for expense
void distroyExpense(Expense* ex);


//getter for id
int getID(Expense *ex);
//getter for month
int getMonth(Expense *ex);
//getter for day
int getDay(Expense *ex);
//getter for value
float getValue(Expense *ex);
//getter for category
char* getCategory(Expense *ex);

//setter for id
void setID(Expense *ex, int nid);
//setter for month
void setMonth(Expense *ex,int nmonth);
//setter for day
void setDay(Expense *ex, int nday);
//setter for value
void setValue(Expense *ex,float nvalue);
//setter for category
void setCategory(Expense *ex,char *ncategory);

#endif // EXPENSE_H_INCLUDED
