#ifndef CONTROLLER_H_INCLUDED
#define CONTROLLER_H_INCLUDED

#include "repository.h"

typedef struct
{
    ExpenseRepo* repo;
}ExpenseCont;

ExpenseCont* createController(ExpenseRepo *repo);

void destroyController(ExpenseCont* cont);

int addExpense(ExpenseCont *cont,int id,int month, int day, float value, char* category);

VectorDinamic* getAllExpenses(ExpenseCont *cont);

Expense* getByID(ExpenseCont *cont,int id);
VectorDinamic* getAllOrderDay(ExpenseCont* cont);
VectorDinamic* getAllOrderCategory(ExpenseCont* cont);
VectorDinamic* getbyAmountGreater(ExpenseCont* cont, float value);

void removeExpense(ExpenseCont* cont, int id);
void updateValue(ExpenseCont* cont, int id, float nvalue);
void undoModification(ExpenseCont* cont);
#endif // CONTROLLER_H_INCLUDED
