#include "controller.h"
#include "expense.h"
#include "repository.h"
#include <stdlib.h>
#include <assert.h>

/**
Description: Create a new controller
Output:  A new object of type controller
*/
ExpenseCont* createController(ExpenseRepo *repo)
{
    ExpenseCont* rez= malloc(sizeof(ExpenseCont));
    rez->repo = repo;
    return rez;
}

/**
Description:  Adding a new expense
Input: id-id of new expense, month, day, value, category
Output:  Number of Expenses into the list;
*/
int addExpense(ExpenseCont *cont, int id,int month, int day, float value, char* category)
{
    Expense* ex=createExpense(id,month,day,value,category);
    save(cont->repo,ex);
    return getNrExpenses(cont->repo);
}

/**
Description: Destroyer for the controller
*/
void destroyController(ExpenseCont* cont)
{
    destroyRepo(cont->repo);
    free(cont);
}

/**
Description: getter for all expenses
Output: rez-VectorDinamic
*/
VectorDinamic* getAllExpenses(ExpenseCont *cont)
{
    VectorDinamic* rez = getAll(cont->repo);
    return rez;
}

/**
Description: Get an expense by ID
Input: ID- of the expense
Output: The expense with the given ID, or empty expense if doesn't exist
*/
Expense* getByID(ExpenseCont* cont,int id)
{
    VectorDinamic* rez = getAll(cont->repo);
    int nrElems=  getNrElements(rez);
    int i;
    Expense* ex;
    for(i=0;i<nrElems;i++)
    {
        ex = get(rez,i);
        if(getID(ex)== id)
            return ex;
    }
    ex= createExpense(0,0,0,0,"0");
    destroy(rez);
    return ex;
}

/**
Description: Update the value of an expense with a given Id
Input: Id of the expense, nvalue-the new value of the expense
*/
void updateValue(ExpenseCont* cont, int id, float nvalue)
{
    updateValueExpense(cont->repo,id,nvalue);
}

/**
Description: Get all expenses ordered by day
*/
VectorDinamic* getAllOrderDay(ExpenseCont* cont)
{
    VectorDinamic* v= getAll(cont->repo);
    VectorDinamic* rez = createDynamicVector();
    int nrElems = getNrElements(v);
    int i,j;
    for(i=0;i<nrElems;i++)
    {
        Expense* ex = get(v,i);
        add(rez,copyExpense(ex));

    }
    Expense* aux;
    for(i=0;i<nrElems-1;i++)
        for(j=i+1;j<nrElems;j++)
        {
            if(getDay(rez->elems[i])> getDay(rez->elems[j]))//crescator
            {
                aux = rez->elems[i];
                rez->elems[i] = rez->elems[j];
                rez->elems[j] = aux;
            }
        }
    return rez;
}

VectorDinamic* getbyAmountGreater(ExpenseCont* cont, float value)
{
    VectorDinamic* v= getAll(cont->repo);
    VectorDinamic* rez = createDynamicVector();
    int nrElems = getNrElements(v);
    int i,j;
    for(i=0;i<nrElems;i++)
    {
        Expense* ex = get(v,i);
        if(getValue(ex) > value)
            add(rez,copyExpense(ex));
    }
    return rez;
}

/**
Description: Get all expenses ordered by category
*/
VectorDinamic* getAllOrderCategory(ExpenseCont* cont)
{
    VectorDinamic* v = getAll(cont->repo);
    VectorDinamic* rez = createDynamicVector();
    int nrElems = getNrElements(v);
    int i,j;
    for(i=0;i<nrElems;i++)
    {
        Expense* ex = get(v,i);
        add(rez,copyExpense(ex));
    }

    Expense* aux;
    for(i=0;i<nrElems-1;i++)
        for(j=i+1;j<nrElems;j++)
        {
            if(strcmp(getCategory(rez->elems[i]), getCategory(rez->elems[j]))>0)
            {
                aux = rez->elems[i];
                rez->elems[i] = rez->elems[j];
                rez->elems[j] = aux;
            }
        }
    return rez;
}

/**
Description: Remove an expense
Input: Id of expense to be remove
*/
void removeExpense(ExpenseCont* cont, int id)
{
    removeExp(cont->repo,id);
}

/**
Undo
*/
void undoModification(ExpenseCont* cont)
{
    undo(cont->repo);
}
