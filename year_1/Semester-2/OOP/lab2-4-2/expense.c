#include "expense.h"
#include <string.h>
#include <assert.h>
#include <stdlib.h>

Expense* createExpense(int id, int month, int day, float value, char* category)
{
    Expense *ex = malloc(sizeof(Expense));
    ex->id=id;
    ex->month = month;
    ex->day = day;
    ex->value = value;
    strcpy(ex->category,category);
    return ex;
}

void destroyExpense(Expense* ex)
{
    free(ex);
}

Expense* copyExpense(Expense* ex)
{
    return createExpense(ex->id,ex->month,ex->day,ex->value,ex->category);
}

//Getters here

int getID(Expense *ex)
{
    return ex->id;
}
int getMonth(Expense *ex)
{
    return ex->month;
}


int getDay(Expense *ex)
{
    return ex->day;
}


float getValue(Expense *ex)
{
    return ex->value;
}


char* getCategory(Expense *ex)
{
    return ex->category;
}

//Setters from here
void setID(Expense* ex,int nid)
{
    ex->id=nid;
}

void setMonth(Expense* ex, int nmonth)
{
    ex->month=nmonth;
}
void setDay(Expense* ex,int nday)
{
    ex->day=nday;
}
void setValue(Expense* ex,float nvalue)
{
    ex->value=nvalue;
}

void setCategory(Expense* ex, char* ncategory)
{
    strcpy(ex->category,ncategory);
}
