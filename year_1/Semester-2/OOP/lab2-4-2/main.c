#include <stdio.h>
#include <stdlib.h>

#include "expense.h"
#include "repository.h"
#include "controller.h"
#include "vectorDinamic.h"
#include "testModule.h"


void addSampleExpese(ExpenseCont* cont)
{

    addExpense(cont,1,1,1,1,"one");
    addExpense(cont,2,2,2,2,"two");
    addExpense(cont,3,3,3,3,"three");
}
int main()
{
    //testAll();
    ExpenseRepo *repo = createRepo();
    ExpenseCont* cont = createController(repo);
    addSampleExpese(cont);
    startUI(cont);
    printf("GoodBye\n");
    return 0;
}
