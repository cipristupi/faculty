#include <iostream>
#include "Controller.h"
#include "Product.h"
#include "Repository.h"
#include "Ui.h"
#include "tests.h"
using namespace std;

int main()
{
    /*
    bread,provider1,food,3.5
 dex,provider2,book,10
 bread,provider3,food,3
 dex,provider4,book,9
 book2,provider5,book,12

    */

    tests* t=new tests;
    t->runTest();
    Product p1(1,"bread","provider1","food",3.5);
    Product p2(2,"dex","provider2","book",3);
    Product p3(3,"bread","provider3","food",9);
    Product p4(4,"book2","provider4","book",12);
    Product p5(5,"dex","provider4","book",9);
    Product p6(6,"oop book","gigel","book",100);
    Product p7(7,"food","food","food",897);
    Product p8(8,"something","something","food",789);
    Product p9(9,"asdf","kadfsjgjf","book",785);
    Product p10(10,"how to understand womens","you cant","book",1);

    Repository* r = new Repository();

    r->addEntitie(p1);
    r->addEntitie(p2);
    r->addEntitie(p3);
    r->addEntitie(p4);
    r->addEntitie(p5);
    r->addEntitie(p6);
    r->addEntitie(p7);
    r->addEntitie(p8);
    r->addEntitie(p9);
    r->addEntitie(p10);

    Controller ctr(r);
    Ui uiL(ctr);
    uiL.initUi();
    cout << "Hello world!" << endl;
    return 0;
}
