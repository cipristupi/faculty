#include "tests.h"

tests::tests()
{
    //ctor
}

tests::~tests()
{
    //dtor
}

void tests::runTest()
{
    testAddCtr();
    testAddRepo();
}

void tests::testAddCtr()
{
    Repository* r = new Repository();
    Controller cont(r);
    cont.addCtr(1,"1","1","1",1);
    cont.addCtr(2,"2","2","2",2);
    vector<Product> res;
    res =cont.getAllCtr();

    assert(res.size()==2);
}

void tests::testAddRepo()
{

    Product p1(1,"bread","provider1","food",3.5);
    Product p2(2,"dex","provider2","book",3);
    Product p3(3,"bread","provider3","food",9);
    Product p4(4,"book2","provider4","book",12);
    Product p5(5,"dex","provider4","book",9);
    Product p6(6,"oop book","gigel","book",100);
    Product p7(7,"food","food","food",897);
    Product p8(8,"something","something","food",789);
    Product p9(9,"asdf","kadfsjgjf","book",785);
    Product p10(10,"how to understand womens","you cant","book",1);

    Repository* r = new Repository();

    r->addEntitie(p1);
    r->addEntitie(p2);
    r->addEntitie(p3);
    r->addEntitie(p4);
    r->addEntitie(p5);
    r->addEntitie(p6);
    r->addEntitie(p7);
    r->addEntitie(p8);
    r->addEntitie(p9);
    r->addEntitie(p10);

    vector<Product> res;
    res = r->getAll();
    assert(res.size()==10);
}

