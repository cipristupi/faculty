#include "Product.h"

Product::Product()
{
    //ctor
}

Product::~Product()
{
    //dtor
}
Product::Product(int i,string n, string p,string t, float pr)
{
    Id=i;
    name=n;
    type=t;
    price=pr;
    provider = p;
}

string Product::getName()
{
    return name;
}

string Product::getType()
{
    return type;
}

string Product::getProvider()
{
    return provider;
}

float Product::getPrice()
{
    return price;
}

void Product::setName(string nname)
{
    name=nname;
}

void Product::setType(string ntype)
{
    type=ntype;
}

void Product::setProvider(string nprovider)
{
    provider=nprovider;
}

void Product::setPrice(float nprice)
{
    price=nprice;
}


int Product::getID()
{
    return Id;
}

void Product::setId(int nid)
{
    Id=nid;
}
