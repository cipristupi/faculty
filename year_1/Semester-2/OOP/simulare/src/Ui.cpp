#include "Ui.h"

Ui::Ui()
{
    //ctor
}

Ui::~Ui()
{
    //dtor
}


void Ui::initUi()
{
    printMenu();
    int cmd;
    while(cmd)
    {
        cout<<"Command: ";
        cin>>cmd;
        switch(cmd)
        {
            case 1:
                {
                     addUi();
                     break;
                }
            case 2:
                {
                    printAll();
                    break;
                }
            case 3:
                {
                    printAllByName();
                    break;
                }
            case 4:
                {
                    printAllSorted();
                    break;
                }
            case 5:
                {
                    getAllPrint();
                    break;
                }
        }
    }
}

Ui::Ui(Controller c)
{
    cont=c;
}

void Ui::addUi()
{
    int id;
    float price;
    string name,provider,type;
    cout<<"ID: ";
    cin>>id;
    cout<<"Name: ";
    cin>>name;
    cout<<"Provider: ";
    cin>>provider;
    cout<<"Type: ";
    cin>>type;
    cout <<"Price: ";
    cin>> price;
    cont.addCtr(id,name,provider,type,price);
}

void Ui::printAll()
{
    vector<Product> res;
    res=cont.getAllCtr();
    for(int i=0;i<res.size();i++)
    {
        //printEntity(res[i]);
        cout<<res[i].getName() << endl;
    }
}
void Ui::printEntity(Product p)
{
    cout<<"---------------------------------"<<endl;
    cout<<"Name: " << p.getName() <<endl;
    cout<<"Provider: "<< p.getProvider()<<endl;
    cout<<"Type: "<< p.getType() << endl;
    cout<<"Price: " << p.getPrice() << endl;
    cout<<"---------------------------------"<<endl;
}



void Ui::printAllByName()
{
    string name;
    cout<<"Name: ";
    cin>>name;
    vector<Product> res;
    res= cont.getAllName(name);
    for(int i=0;i<res.size();i++)
    {
        printEntity(res[i]);
    }
}

void Ui::printAllSorted()
{
    vector<Product> res;
    res= cont.getAllSorted();
    for(int i=0;i<res.size();i++)
    {
        printEntity(res[i]);
    }
}

void Ui::printMenu()
{
    cout<<"1 - add new product"<<endl;
    cout<<"2 - get all  names"<<endl;
    cout<<"3 - get all by name"<<endl;
    cout<<"4 - get all ordered by type and price" <<endl;
}
void Ui::getAllPrint()
{
    vector<Product> res;
    res=cont.getAllCtr();
    for(int i=0;i<res.size();i++)
    {
        printEntity(res[i]);
    }
}



