#ifndef REPOSITORY_H
#define REPOSITORY_H

#include <iostream>
#include <algorithm>
#include <vector>
#include "Product.h"
using namespace std;

class Repository
{
    public:
        Repository();
        virtual ~Repository();
        void addEntitie(Product p);
        vector<Product> getAll();

    protected:
    private:
        vector<Product> entities;
};

#endif // REPOSITORY_H
