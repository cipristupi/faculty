#ifndef TESTS_H
#define TESTS_H


#include "Controller.h"
#include "Product.h"
#include "Repository.h"
#include "Ui.h"
using namespace std;
#include "assert.h"
class tests
{
    public:
        tests();
        void runTest();
        virtual ~tests();
    protected:
    private:
        void testAddCtr();
        void testAddRepo();
};

#endif // TESTS_H
