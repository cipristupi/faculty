#ifndef UI_H
#define UI_H

#include "Controller.h"
#include <iostream>
#include "string"
#include <vector>
#include <algorithm>
#include "Product.h"
#include "Repository.h"
using namespace std;


class Ui
{
    public:
        Ui();
        virtual ~Ui();
        void initUi();
        Ui(Controller c);
    protected:
    private:
        Controller cont;
        void addUi();
        void printAll();
        void printAllByName();
        void printAllSorted();
        void printEntity(Product p);
        void printMenu();
        void getAllPrint();
};

#endif // UI_H
