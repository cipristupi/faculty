#ifndef PRODUCT_H
#define PRODUCT_H

#include "string"
using namespace std;
class Product
{
    public:
        Product();
        virtual ~Product();
        Product(int i, string n, string t, string p, float pr);
        string getName();
        string getType();
        string getProvider();
        int getID();
        float getPrice();
        void setName(string nname);
        void setType(string ntype);
        void setProvider(string nprovider);
        void setPrice(float nprice);
        void setId(int nid);
    protected:
    private:
        int Id;
        string name;
        string provider;
        string type;
        float price;
};

#endif // PRODUCT_H
