#ifndef CONTROLLER_H
#define CONTROLLER_H

#include<iostream>
#include "Repository.h"
#include "Product.h"
#include <vector>
#include <algorithm>
#include "string"
using namespace std;
class Controller
{
    public:
        Controller();
        Controller(Repository* r);
        virtual ~Controller();
        vector<Product> getAllCtr();
        vector<Product> getAllName(string name);
        vector<Product> getAllSorted();
        void addCtr(int id, string name, string provider, string type, float price);
    protected:
    private:
        Repository* repo;
};

#endif // CONTROLLER_H
