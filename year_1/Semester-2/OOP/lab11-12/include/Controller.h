#ifndef CONTROLLER_H
#define CONTROLLER_H

#include "string"
#include "Repository.h"
#include "Expense.h"
#include "Validator.h"
#include "AbstractRepository.h"
#include "InMemRepo.h"
using namespace std;

class Controller
{
    public:
        Controller();
        Controller(AbstractRepository<Expense,int>* r);
        virtual ~Controller();
        void addExpenseCtr(int id, int month, int day, float value, string category);
        void modifyExpenseCtr(int id, float nvalue);
        void deleteExpenseCtr(int id);
        int getSizeCtr();

        //New methods
        int validator(Expense *ex);
        Expense getByIDCtr(int id);
        void undoCtr();
        vector<Expense> getAllCtr();

        vector<Expense> getAllOrderDay();
        vector<Expense> getAllOrderCategory();
        vector<Expense> getbyAmountGreater(float value);
        vector<Expense> getbyCategory(string category);
        vector<Expense> getByCatAmount(float value, string category);

    protected:
    private:
        AbstractRepository<Expense,int>* repo;
        //bool GreateDay(Expense& a,Expense& b);
        //bool GreateCategory( Expense& a, Expense& b);
};

#endif // CONTROLLER_H
