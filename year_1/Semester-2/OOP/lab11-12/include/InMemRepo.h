#ifndef INMEMREPO_H_INCLUDED
#define INMEMREPO_H_INCLUDED

#include<string>
#include<vector>
#include "AbstractRepository.h"
#include "Expense.h"
#include "Validator.h"
#include<algorithm>
using namespace std;



class InMemRepo: public AbstractRepository<Expense,int>
{
    vector<Expense> entities;
    vector<Expense> backup;

public:
    InMemRepo()
    {

    }
    virtual ~InMemRepo()
    {
       // delete entities;
    }

    Expense* findById(int id)
    {
        Expense* ex=new Expense(-1,-1,-1,-1,"zero");
        int d;//distance from begin to iterator
        vector<int> idVector;
        for(int i=0;i<entities.size();i++)
        {
            idVector.push_back(entities[i].getID());
        }
        vector<int>::iterator it;
        it = find(idVector.begin(),idVector.end(),id);
        if(it != idVector.end())
        {
            d =distance(idVector.begin(),it);
            return &entities[d];
        }
        return ex;
    }

    void save(Expense el)
    {
        createBackup();
        Validator val;
        val.validate(&el);
        if(findById(el.getID())->getID()!=-1)
        {
            throw(ExpenseException("The same ID exists.\n"));
        }
        entities.push_back(el);
    }
    void update(int id, Expense ex)
    {
        createBackup();
    }

    void removeE(int id)
    {
        createBackup();
        int d;//distance from begin to iterator
        vector<int> idVector;
        for(int i=0;i<entities.size();i++)
        {
            idVector.push_back(entities[i].getID());
        }
        vector<int>::iterator it;
        it = find(idVector.begin(),idVector.end(),id);
        d =distance(idVector.begin(),it);
        entities.erase(entities.begin()+d);
    }
    vector<Expense> getAll()
    {
        return entities;
    }

    void undo()
    {
        entities=backup;
    }

private:
    void createBackup()
    {
        backup=entities;
    }

};

#endif // INMEMREPO_H_INCLUDED
