#ifndef EXPENSE_H
#define EXPENSE_H
#include "string"
using namespace std;


class Expense
{
    public:
        Expense();
        Expense(int i, int m, int d, float v, string c);
        Expense copyExpense(Expense ex);
        //getter for id
        int getID();
        //getter for month
        int getMonth();
        //getter for day
        int getDay();
        //getter for value
        float getValue();
        //getter for category
        string getCategory();

        //setter for id
        void setID(int nid);
        //setter for month
        void setMonth(int nmonth);
        //setter for day
        void setDay(int nday);
        //setter for value
        void setValue(float nvalue);
        //setter for category
        void setCategory(string ncategory);

        virtual ~Expense();

        friend std::ostream& operator<<(std::ostream& os, const Expense& ex);
	friend std::istream& operator>>(std::istream&is, Expense& ex);
    protected:
    private:
        int id;
        int month;
        int day;
        float value;
        string category;
};

#endif // EXPENSE_H
