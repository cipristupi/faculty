#ifndef REPOSITORY_H
#define REPOSITORY_H

#include "AbstractRepository.h"
#include "InMemRepo.h"
#include "Validator.h"
#include "Expense.h"

class Repository : public InMemRepo
{
    public:
        //Repository(string f): InMemRepo(),filename(f){ loadFromFile();}
        Repository(string f);
        virtual ~Repository();

        void saveToFile();
        void save(Expense ex);
        void undoRepo();
    protected:
    private:
        string filename;
        void loadFromFile();
};

class RepositoryException : public exception
{
    public:
    RepositoryException(string m) : msg(m) {}
    ~RepositoryException() throw() {}
    const char* what() const throw() { return msg.c_str(); }

    private:
    string msg;
};

#endif // REPOSITORY_H
