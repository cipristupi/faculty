#include <iostream>
#include "Repository.h"
#include "Expense.h"
#include "Controller.h"
#include "AbstractRepository.h"
#include "InMemRepo.h"
#include "Ui.h"
using namespace std;

int main()
{
    try
    {
        AbstractRepository<Expense,int> *r =new Repository("expenses.txt");
        Controller c(r);
        Ui userInt(c);
        userInt.startUI();
    }
    catch(RepositoryException& e)
    {
        cout << e.what() << endl;
    }
    cout << "Goodbye world!" << endl;
    return 0;
}
