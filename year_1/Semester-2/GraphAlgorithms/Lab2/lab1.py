__author__ = 'Ciprian'
#from queue import *

class Vertex:
    def __init__(self):  #n -no of vertex
        #self.__nodID=nodID
        self.__inbound = {}
        self.__outbound = {}

    def getInbound(self):
        return self.__inbound

    def getOutbound(self):
        return self.__outbound

    def getInGr(self):
        return len(self.__inbound)

    def getOutGr(self):
        return len(self.__outbound)

    def addEdgeOut(self, x, cost):
        self.__outbound[x] = cost

    def addEdgeIn(self, x, cost):
        self.__inbound[x] = cost

    def parseNin(self):
        return self.__inbound.keys()

    def parseNout(self):
        return self.__outbound.keys()

    def isEdge(self, y):
        return y in self.__outbound.keys()
    def updateCostOut(self,y,c):
        self.__outbound[y]=c
    def updateCostIn(self,y,c):
        self.__inbound[y]=c

    def getCostOut(self,x):
        return self.__outbound[x]
    def getCostIn(self,x):
        return self.__inbound[x]

class Queue:
    def __init__(self):
        self.__q=[]
        pass
    def add(self,x):
        self.__q.append(x)
    def extract(self):
        return self.__q.pop(0)

    def isEmpty(self):
        return len(self.__q) == 0


class PriorityQueue:
    def __init__(self):
        self.__values = {}

    def isEmpty(self):
        return len(self.__values) == 0

    def pop(self):
        topPriority = None
        topObject = None
        for obj in self.__values:
            objPriority = self.__values[obj]
            if topPriority is None or topPriority>objPriority:
                topPriority = objPriority
                topObject = obj
        del self.__values[topObject]
        return topObject

    def add(self, obj, priority):
        self.__values[obj] = priority

    def contains(self, val):
        return val in self.__values

class Graph:
    def __init__(self, filepath):
        self.__filename = filepath
        self.__graph = {}
        self.__nVertex = 0
        self.__nEdge = 0
        self.__loadFile()

    def __loadFile(self):
        try:
            f = open(self.__filename, "r")
        except IOError as ex:
            print "Error: " + str(ex)

        line = f.readline().strip()
        t = line.split(" ")
        #print t
        self.__nVertex = int(t[0])
        self.__nEdge = int(t[1])
        for i in range(self.__nVertex):
            v = Vertex ()
            self.__graph[i] = v
        #print str(len(self.__graph))
        line = f.readline().strip()
        while line != "":
            t = line.split(" ")
            s = int(t[0])
            fi = int(t[1])
            c = int(t[2])
            #print str(s)+ "   " + str(fi)  + "  Cost "  + str(c)
            #try:
            self.__graph[s].addEdgeOut(fi, c)
            #except Exception as ex:
                #print ex
            #self.__graph[fi].addEdgeIn(s, c)
            line = f.readline().strip()


    def getNoVertext(self): #I don't need it
        return len(self.__graph)

    def getInD(self,v):
        return self.__graph[v].getInGr()

    def getOutD(self,v):
        return self.__graph[v].getOutGr()

    def isEdge(self,x,y):
        return self.__graph[x].isEdge(y)

    def parseInEdge(self,x):
        #x = int(raw_input("X vertex: "))
        l = self.__graph[x].parseNin()
        return l

    def parseOutEdge(self,x):
        #x = int(raw_input("X vertex: "))
        l = self.__graph[x].parseNout()
        return l

    def update(self,x,y,c):
        '''x = int(raw_input("X vertex: "))
        y = int(raw_input("Y vertex: "))
        c =int(raw_input("New cost: "))'''
        self.__graph[x].updateCostOut(y,c)
        self.__graph[y].updateCostIn(x,c)

    def getNoVertex(self):
        return self.__nVertex

    def getGraph(self):
        return self.__graph

    def getInBound(self,x):
        return self.__graph[x].getInbound()

    def getOutBound(self,x):
        return self.__graph[x].getOutbound()

    def BFS(self,s):
        d = []
        v = [] #visited
        print str(self.__nVertex)
        for i in range(self.__nVertex):
            d.append("inf")
        d[s] = 0
        q = Queue()
        q.add(s)
        v.append(s)
        while not q.isEmpty():
            x = q.extract()
            for y in self.__graph[x].getOutbound():
                if y not in v:
                    v.append(y)
                    q.add(y)
                    d[y]=x

        print "Tree "
        for i in d:
            print i,
        return v

    def DFS(self,s):
        visited = set()
        visited.add(s)
        return self.DFSaux(s,visited)

    def DFSaux(self, x , visited):
        #print visited
        #13
        # print self.__graph[x].getOutbound()
        for y in self.__graph[x].getOutbound():
            if y not in visited:
                visited.add(y)
                self.DFSaux(y,visited)

        return visited


    def getChildren(self, x , prev):
        list=[]
        for i in prev:
            if prev[i] == x:
                list.append(i)
        return list


    def printDijkstraTree(self , s, q, d, prev, indent):
        if q.contains(s):
            star = ''
        else:
            star = '*'
        print "%s%s [%s]%s" % (indent,s, d[s], star)


        for x in self.getChildren(s,prev):
            self.printDijkstraTree(x,q,d,prev,indent+'    ')


    def printDijkstraStep(self , s, x, q, d, prev):
        print '----'
        if x is not None:
            print 'x=%s [%s]' % (x, d[x])
        self.printDijkstraTree(s,q,d,prev,'')

    def dijkstra(self , s):
        prev = {}
        q = PriorityQueue()
        q.add(s, 0)
        d = {}
        d[s] = 0
        visited = set()
        visited.add(s)
        #self.printDijkstraStep(s, None, q, d, prev)
        while not q.isEmpty():
            x = q.pop()
            for y in self.parseOutEdge(x):
                if y not in visited or d[y] > d[x] + self.__graph[x].getCostOut(y):
                    d[y] = d[x] +  self.__graph[x].getCostOut(y)
                    visited.add(y)
                    q.add(y, d[y])
                    prev[y] = x
            #self.printDijkstraStep(s, x, q, d, prev)

        return (d, prev)

    def printPath(self,s,f,prev):
        path =[]
        while 1:
            path.append(f)
            if s == f: break
            f=prev[f]
        path.reverse()
        '''for i in path:
            print str(i) + "  ",
        print
        for i in range(len(path)):
            print str(path[i])

        print'''
        print path


    def getLowestWalk(self,s,f):
        l = self.dijkstra(s)
        d = l[0]
        prev = l[1]
       # print self.getChildren(f,prev)

        if f in d:
            print "The shortest path from " + str(s) + " to " + str(f) + " is " + str(d[f])
            self.printPath(s,f,prev)
        else:
             print "There is no path from " + str(s) + " to " + str(f) + "."


class Ui:
    def __init__(self, filepath):
        self.__g = Graph(filepath)

    def printVertex(self):
        l= self.__g.getGraph()
        for i in range(self.__nVertex):
            print l[i]

    def getNoVertext(self):
        print str(self.__g.getNoVertex())

    def getInOutD(self):
        v = int(raw_input("Vertex: "))
        '''print "In grade " + str(self.__graph[v].getInGr())
        print "Out grade " + str(self.__graph[v].getOutGr())'''
        print "In grade " + str(self.__g.getInD(v))
        print "Out grade " + str(self.__g.getOutD(v))

    def isEdge(self):
        x = int(raw_input("X vertex: "))
        y = int(raw_input("Y vertex: "))
        print self.__g.isEdge(x,y)
        #print self.__graph[x].isEdge(y)

    def parseInEdge(self):
        x = int(raw_input("X vertex: "))
        #l = self.__graph[x].parseNin()
        l = self.__g.parseInEdge(x)
        for i in l:
            print str(i) + " ",
        print

    def parseOutEdge(self):
        x = int(raw_input("X vertex: "))
        #l = self.__graph[x].parseNout()
        l = self.__g.parseOutEdge(x)
        for i in l:
            print str(i) + " ",
        print

    def update(self):
        x = int(raw_input("X vertex: "))
        y = int(raw_input("Y vertex: "))
        c =int(raw_input("New cost: "))
        self.__g.update(x,y,c)
        #self.__graph[x].updateCostOut(y,c)
        #self.__graph[y].updateCostIn(x,c)

    def printAllEdge(self):
        n=self.__g.getNoVertex()
        for i in range(n):
            print "Vertex " + str(i)
            l = self.__g.getInBound(i)

            #print type(l)
            print "In bound"
            for ii in l:
                print str(ii) + "-("+ str(l[ii])+")",
            print
            print "Out bound"
            #lo = self.__graph[i].getOutbound()
            lo= self.__g.getOutBound(i)
            #print type(lo)
            for io in lo:
                print str(io) + "-("+str(lo[io])+")",
            print "\n---------------------------"

    def parseOut(self):
        n = self.__g.getNoVertex()
        for i in range(n):
            #l = self.__graph[i].parseNout()
            #l = self.__g.parseOutEdge(i)
            l = self.__g.getOutBound(i)
            print "Vertex " + str(i) + "=",
            for ii in l:
                print str(ii) + "(" + str(l[ii])+"),",
            print

    def parseIn(self):
        n = self.__g.getNoVertex()
        for i in range(n):
            #l = self.__graph[i].parseNin()
            #l = self.__g.parseInEdge(i)
            l = self.__g.getInBound(i)
            print "Vertex " + str(i) + "=",
            for ii in l:
                print str(ii) + "(" + str(l[ii])+"),",
            print

    def printDFS(self):
        s = int(raw_input("Start vertex: "))
        l = self.__g.DFS(s)
        #print "Length of list " + str(len(l))
        print
        for i in l:
            print str(i) + "  ",

        print

    def printBFS(self):
        s = int(raw_input("Start vertex: "))
        l = self.__g.BFS(s)
        #print "Length of list " + str(len(l))
        print
        for i in l:
            print str(i) + " ",
        print

    def connectetComp(self):
        v = []
        for i in range(self.__g.getNoVertex()):
            if i not in v:
                v.append(i)
                print "Connected component: "
                c = self.__g.DFS(i)
                #print v
                for ic in c:
                    v.append(ic)
                    print str(ic) + "  ",
                print

    def lowestCostWalk(self):
        # 1. Write a program that, given a graph with positive costs and two vertices,
        #  finds a lowest cost walk between the given vertices, using the Dijkstra algorithm.
        s = int(raw_input("Start vertex: "))
        f = int(raw_input("Finish vertex: "))
        self.__g.getLowestWalk(s,f)


    def printMenu(self):
        print "m - to print the menu"
        print "1-Get number of vertices " #Work
        print "2-Check if is edge " #Work
        print "3-Get in degree and out degree of a specified vertex" #Work
        print "4-Parse the set of outbound edges" #Work
        print "5- Parse the set of inbound edges" #Work
        print "6- Update " #Work
        print "7- Show Edges" #Work
        print "8- Print list of vertex" #Work
        print "9- Parse outbound" #Work
        print "10 - Parse inbound" #Work
        print "11 - Breadth first" #Work
        print "12 - Depth first" #Work
        print "13 - Lab 2"
        print "14 - Lab 3"
        print "0-exit"


    def runF(self):
        self.printMenu()
        while True:
            cmd = raw_input("Enter command: ")
            if cmd == '0':
                return
            elif cmd == 'm':
                self.printMenu()
            elif cmd == '1':
                self.getNoVertext()
            elif cmd == '3':
                self.getInOutD()
            elif cmd == '2':
                self.isEdge()
            elif cmd == '4':
                self.parseOutEdge()
            elif cmd == '5':
                self.parseInEdge()
            elif cmd == '6':
                self.update()
            elif cmd == '7':
                self.printAllEdge()
            elif cmd == '8':
                self.printVertex()
            elif cmd == '9':
                self.parseOut()
            elif cmd == '10':
                self.parseIn()
            elif cmd == '11':
                self.printBFS()
            elif cmd == '12':
                self.printDFS()
            elif cmd == '13':
                self.connectetComp()
            elif cmd == '14':
                self.lowestCostWalk()

#p = Ui("graph.txt")
p= Ui("d.txt")
p.runF()


#3. Write a program that finds the connected components of an undirected graph using a depth-first traversal of the graph.