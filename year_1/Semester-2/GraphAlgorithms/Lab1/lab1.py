__author__ = 'Ciprian'


class Vertex:
    def __init__(self):  #n -no of vertex
        #self.__nodID=nodID
        self.__inbound = {}
        self.__outbound = {}

    def getInbound(self):
        return self.__inbound

    def getOutbound(self):
        return self.__outbound

    def getInGr(self):
        return len(self.__inbound)

    def getOutGr(self):
        return len(self.__outbound)

    def addEdgeOut(self, x, cost):
        self.__outbound[x] = cost

    def addEdgeIn(self, x, cost):
        self.__inbound[x] = cost

    def parseNin(self):
        return self.__inbound.keys()

    def parseNout(self):
        return self.__outbound.keys()

    def isEdge(self, y):
        return y in self.__outbound.keys()
    def updateCostOut(self,y,c):
        self.__outbound[y]=c
    def updateCostIn(self,y,c):
        self.__inbound[y]=c



class Graph:
    def __init__(self, filepath):
        self.__filename = filepath
        self.__graph = {}
        self.__nVertex = 0
        self.__nEdge = 0
        self.__loadFile()

    def __loadFile(self):
        try:
            f = open(self.__filename, "r")
        except IOError as ex:
            print "Error: " + str(ex)

        line = f.readline().strip()
        t = line.split(" ")
        self.__nVertex = int(t[0])
        self.__nEdge = int(t[1])
        for i in range(self.__nVertex):
            v = Vertex ()
            self.__graph[i] = v

        line = f.readline().strip()
        while line != "":
            t = line.split(" ")
            s = int(t[0])
            fi = int(t[1])
            c = int(t[2])
            self.__graph[s].addEdgeOut(fi, c)
            self.__graph[fi].addEdgeIn(s, c)
            line = f.readline().strip()

    def printVertex(self):
        for i in range(self.__nVertex):
            print self.__graph[i]

    def getNoVertext(self):
        print len(self.__graph)

    def getInOutD(self):
        v = int(raw_input("Vertex: "))

        print "In grade " + str(self.__graph[v].getInGr())
        print "Out grade " + str(self.__graph[v].getOutGr())

    def isEdge(self):
        x = int(raw_input("X vertex: "))
        y = int(raw_input("Y vertex: "))
        print self.__graph[x].isEdge(y)

    def parseInEdge(self):
        x = int(raw_input("X vertex: "))
        l = self.__graph[x].parseNin()
        for i in l:
            print str(i) + " ",
        print

    def parseOutEdge(self):
        x = int(raw_input("X vertex: "))
        l = self.__graph[x].parseNout()
        for i in l:
            print str(i) + " ",
        print

    def update(self):
        x = int(raw_input("X vertex: "))
        y = int(raw_input("Y vertex: "))
        c =int(raw_input("New cost: "))
        self.__graph[x].updateCostOut(y,c)
        self.__graph[y].updateCostIn(x,c)

    def printAllEdge(self):

        for i in range(self.__nVertex):
            print "Vertex " + str(i)
            l = self.__graph[i].getInbound()
            #print type(l)
            print "In bound"
            for ii in l:
                print str(ii) + "-("+ str(l[ii])+")",
            print
            print "Out bound"
            lo = self.__graph[i].getOutbound()
            #print type(lo)
            for io in lo:
                print str(io) + "-("+str(lo[io])+")",
            print "\n---------------------------"

    def parseOut(self):
        for i in range(self.__nVertex):
            l = self.__graph[i].parseNout()
            print "Vertex " + str(i) + "=",
            for i in l:
                print str(i) + "(" + str(l[i])+"),",
            print
    def parseIn(self):
        for i in range(self.__nVertex):
            l = self.__graph[i].parseNin()
            print "Vertex " + str(i) + "=",
            for i in l:
                print str(i) + "(" + str(l[i])+"),",
            print
    def runF(self):
        print "1-Get number of vertices " #Work
        print "2-Check if is edge " #Work
        print "3-Get in degree and out degree of a specified vertex" #Work
        print "4-Parse the set of outbound edges"
        print "5- Parse the set of inbound edges"
        print "6- Update "
        print "7- Show Edges"
        print "0-exit"
        while True:
            cmd = raw_input("Enter command: ")
            if cmd == '0':
                return
            elif cmd == '1':
                self.getNoVertext()
            elif cmd == '3':
                self.getInOutD()
            elif cmd == '2':
                self.isEdge()
            elif cmd == '4':
                self.parseOutEdge()
            elif cmd == '5':
                self.parseInEdge()
            elif cmd == '6':
                self.update()
            elif cmd == '7':
                self.printAllEdge()
            elif cmd == '8':
                self.printVertex()
            elif cmd =='9':
                self.parseOut()
            elif cmd =='10':
                self.parseIn()



g = Graph("graph.txt")
g.runF()