;12. (a*b+2)/(a+7-c)
;a,c-byte; b-word
;unsigned convention
ASSUME CS: code, DS:data
data SEGMENT  ;DX:AX + CX:BX = adc DX,CX add AX,BX
	a DB 11
	c DB 13
	b DW 10

	x DD ?
	y DW ?
	q DW ?
	r DW ?
data ENDS

code SEGMENT
start:
   mov AX,data    
   mov DS,AX    

   mov AL,a
   mov AH,0

   mul b ;DX:AX:=AX*b || doubleword

   mov CX,0; here create a double word
   mov BL,2
   mov BH,0

   add AX,BX
   adc DX,CX

   mov word PTR x,AX
   mov word PTR x+2,DX
   ;mov x,DX:AX ;doubleword

   mov AL,a
   add Al,7
   sub AL,c
   mov AH,0

   mov y,AX

   mov  AX,word PTR x
   mov  DX,word PTR x+2

   div y

   mov q,AX ;here we put the quotient
   mov r,DX ;here we put the remainder

   mov ax,4C00h
   int 21h   
code ENDS
END start