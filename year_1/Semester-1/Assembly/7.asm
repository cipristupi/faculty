;4. Being given two strings of bytes, compute all positions ,where the second string appears as a substring in the first string.
ASSUME CS: code, DS:data
data SEGMENT 
	s1 DB '123abc123scd'
	lens1 EQU $-s1
	s2 DB '123b'
	lens2 EQU $-s2
	d DB lens1 DUP(?) 
	x DB ?   ;the starting index of s2 into s1
	n DB ?	 ;counter for equality of s2 into s1
	xx DB ?  ;another variable for fun         
data ENDS
code SEGMENT

start:
   mov AX,data    
   mov DS,AX

   mov SI,OFFSET s1
   mov ES,AX
   mov DI,OFFSET d

   CLD
   mov CX,lens1
   mov BP,0


   repeat:
   		LODSB 					; AL<-byte PTR DS:[SI]
   		mov xx,AL 				; only for fun
   		cmp AL,s2[BP]
   		jne here 				;jump if the elements are different
   			cmp BP,0 			; check here if is the first element from s1 the current element from s2
   			jne here2 			;if is not we go here2 where increment the BP index and the n
   				mov n,0
   				mov BX,SI
   				mov x,BL 		;move the starting index of s2 in s1
   				;sub x,1
   			here2:
   				inc BP
   				inc n
   				jmp here4
   		here:
   			cmp n,lens2 		;check here if n==lens2 if are not equal we  put the starting index into d otherwise jump over
   			jne here3
   				mov AL,x
   				STOSB 			;move the starting index of s2 in s1 into d
   				mov BP,0
   				mov n,0
   			here3:
   				mov BP,0
   				mov n,0
   		here4:					;only for jump over other blocks


   loop repeat

   mov AX,4C00h
   int 21h   
code ENDS
END start

; at the beginning me and GOD know what this program does