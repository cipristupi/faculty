;6. Implement an authentication program. The program has the string 'password' defined in its data segment. The program repeatedly asks the user ;to input the password at the keyboard and verifies if the password given by the user is identical with the string 'password' from the data ;segment and prints a suitable message on the screen. The program exits when the user guessed the password.
ASSUME CS: code, DS:data
data SEGMENT 
	msg DB 'Password= $'
	pass DB 20,?,21 DUP(?)
	password DB 'password'
	final DB 'Correct$'
data ENDS
code SEGMENT
ReadPass Proc 			;procedure for reading a string from standard input
	mov AH,0Ah			;read the password from user
   	lea DX,pass
   	int 21h
   	jmp here2
ReadPass ENDP

start:
   mov AX,data    
   mov DS,AX
   mov ES,AX

   here:
	   mov DL,10 ;print an empty line
	   mov AH,02h
	   int 21h

	   mov AH,09h	;print the message password
	   lea DX,msg
	   int 21h

	   call ReadPass ; call the procedure
	   here2:
		   mov BL,byte PTR pass+1
		   mov BH,0
		   mov CX, BX ;get here the length of the read string

		   lea SI,pass + 2 ;get the offset of the read string
		   lea DI,password ;get the offset of the password string
		   CLD 			   ;parse the string from left to right
		   cmp BX,8
		   jne here
		   repeat:
		   		CMPSB      ;compare if the string read from keyboard is equal with the password string
		   		jne here   ;compare each character from each string 
		   		;jump when the character are different
		   loop repeat

	mov DL,10
	mov AH,02h
	int 21h

	mov AH,09h
	lea DX,final
	int 21h

   mov AX,4C00h
   int 21h   
code ENDS
END start