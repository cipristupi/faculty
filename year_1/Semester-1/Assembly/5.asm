;14. The word A is given.
;Obtain the integer number n represented on the bits 13-15 of A.
; Then obtain the word B by rotating A n positions to the left.
ASSUME CS: code, DS:data
data SEGMENT 
	a DW 1110110110110010b ; DEC:=60850 , HEX:=EDB2h
	b DW ?
	n DB ?
data ENDS
code SEGMENT

start:
   mov AX,data    
   mov DS,AX  
   mov AX,a
   shr AX,13
   mov n,AL	;n:=AL:=0000 0111 (7h)
   mov AX,a
   mov CL,n
   rol AX,CL
   mov B,AX

   mov AX,4C00h
   int 21h   
code ENDS
END start