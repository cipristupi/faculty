;9. A byte string S is given. Obtain the string D1 which contains all the positive numbers of S and the string D2 which contains all the negative numbers of S.
;Exemple:
;S: 1, 3, -2, -5, 3, -8, 5, 0
;D1: 1, 3, 3, 5, 0
;D2: -2, -5, -8

ASSUME CS: code, DS:data
data SEGMENT 
	S DB 1,3,-2,-5,3,-8,5,0
	lens EQU $-S ; len 8
	D1 DB lens DUP(?)
	D2 DB lens DUP(?)
	n DB ?
	p DB ?
data ENDS
code SEGMENT

start:
   mov AX,data    
   mov DS,AX  

   mov BX,0;index counter
   mov BP,0 ; negative counter
   mov DI,0 ; positive counter
   repeat:
   		cmp S[BX],0 ; 
   		jl nega
   		jge pos
   		nega:  
   			mov AL,S[BX]
   			mov n,AL
   			mov D2[BP],AL
   			add BP,1
   			jmp nega_l

   		pos:
   			mov AL,S[BX]
   			mov p,AL
   			mov D1[DI],AL
   			add DI,1
   		nega_l:
   		add BX,1
   	cmp BX,lens
   	jb repeat

   	

   mov AX,4C00h
   int 21h   
code ENDS
END start