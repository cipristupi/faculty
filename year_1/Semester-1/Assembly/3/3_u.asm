;The program compute the following formula x:=(84+yy+yy)-(d+d)
;I used the unsigned representation
assume CS:code, DS:data
data segment
	yy DB 99
	d DB 31
	x DW ?
data ends
code segment
start:
	;x:=(84+yy+yy)-(d+d)
	mov AX,data
	mov DS,AX
	mov AL,84
	add AL,yy;AL:=84+99=173
	mov AH,0

	mov BX,AX

	mov AX,0
	mov AL,yy
	mov AH,0

	add BX,AX
	mov AX,0

	mov AL,d
	add AL,d
	mov AH,0

	sub BX,AX
	mov x,BX

	mov AX,4c00h
	int 21h
code ends
end start