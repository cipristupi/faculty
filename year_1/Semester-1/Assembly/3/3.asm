;The program compute the following formula x:=(84+yy+yy)-(d+d)
;I used the signed representation
assume CS:code, DS:data
data segment
	yy DB 99
	d DB 31
	x DB ?
data ends
code segment
start: 
	mov AX,data
	mov DS,AX
	mov AL,84
	add AL,yy;AL:=84+99=173
	cbw

	mov BX,AX
	mov AL,yy
	cbw
	add BX,AX

	mov AL,d
	add AL,d
	cbw
	sub BX,AX
	mov x,BL

	mov AX,4c00h
	int 21h
code ends
end start