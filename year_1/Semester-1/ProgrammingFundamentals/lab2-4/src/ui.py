'''
User interface module
'''
from datetime import datetime
from calendar import monthrange

now=datetime.now()#the current date
year=now.year
month=now.month

def read_value():
    '''
    Description: Read the value of expenses
    Input: value from keyboard
    Output: the readed value
    '''
    value_s=raw_input("Value:")
    if value_s.isdigit()!=True:
        print "Wrong value please tape again"
        value_s=raw_input("Value:")
        while value_s.isdigit()==False :
            print "Wrong value please tape again"
            value_s=raw_input("Value:")     
    return value_s 

def read_day():
    '''
    Description: Read a given day
    Input: day from keyboard
    Output: the readed day
    '''
    day=raw_input("Day:")
    if check_day(day)!=True:
        print "Wrong value please tape again"
        day=raw_input("Day: ")
        while check_day(day)!=True:
            print "Wrong value please tape again"
            day=raw_input("Day: ")
    return day
    
    
def read_category():
    '''
    Description: Read the category
    Input: category from keyboard
    Output: the readed category
    '''
    category=raw_input("Category: ")
    return category

def check_day(s):
    '''
    Description: Check if a given day is correct
    Input: s-day
    Output: True if the day is correct, False otherwise
    '''
    if s.isdigit() == True:
        if int(s) >= 1 and int(s) <= monthrange(year,month)[1]:
            return True
        else:
            return False
    else:
        return False

def show_all(l):
    '''
    Description:This function print all expenses
    Input: list of expenses
    Output: print all expenses
    '''
    #Work
    print
    for i in l:
        print "Month: " + str(i[0])+  " Day: "+ str(i[1]) + " Value: " + str(i[2]) + " Category: " + str(i[3])
         
def show_for(l,category):
    '''
    Description:This function print all expenses for a gived category
    Input: list of expenses
    Output: print all expenses for a category
    '''
    #Work
    print
    for i in l:
        if str(i[3])== category:
            print "Month: " + str(i[0]) + " Day: "+ str(i[1])+" Value: " + str(i[2]) + " Category: " + str(i[3])
            print
            
def show_less(l,value,day): 
    '''
    Description: This function print all expenses less that a gived value before a gived day
    Input: l-list of expenses, value-integer, day-integer
    Output:
    '''
    #Work
    print
    for i in l:
        if int(i[2])<int(value) and int(i[1])<=int(day):
            print "Month: " + str(i[0]) + " Day: "+ str(i[1]) + " Value: " + str(i[2]) + "Category: " + str(i[3])
            
def show_greater(l,value): 
    '''
    Description: This function print all expenses greater that a gived value
    Input: l-list of expenses, value - number for search the values greater
    Output:
    '''
    #Work
    print
    for i in l:
        if int(i[2])>int(value):
            print "Month: " + str(i[0]) + " Day: "+ str(i[1]) + " Value: " + str(i[2]) + " Category: " + str(i[3])               
            
            
def show_menu():
    print "Help section"
    print "1- command to add a new expenses with today day"
    print "2 - command to add a new expenses with a given day"
    print "3 - show all expenses from memory"
    print "4 - show all expenses for a gived category"
    print "5 - print all expenses less that a gived value before a gived day"
    print "6 - print all expenses greater that a gived value"
    print "7 - remove all expenses from a gived day"
    print "8 - remove all expenses for a gived category"
    print "9 - remove all expenses from a periode of time"
    print "10 - replace the value from a given day and category with a new one"
    print "11 - show the sum of expenses for a given category"
    print "12 - show the day with the maximum expenses"
    print "13 - show the category for an exact value"
    print "14 - sort the list ascending after days , 15 sort descending"
    print "16 - sort the list ascending after type, 17 sort descending"
    print "18 - filter after a given category"
    print "19 - filter greater values for a given category "
    print "undo - undo the last operation"
    print
    

def max_day_show(l):
    '''
    Description: This function print the day with the maximum expenses
    Input: l-list of expenses
    Output: 
    '''
    expenses_max=int(l[0][2])
    day=l[0][1]
    i=1
    while i < len(l):
        if int(l[i][2]) > expenses_max:
            expenses_max=int(l[i][2])
            day=l[i][1]
        i=i+1
    print "The day with maximum expenses is  " + str(day) + ". Expenses value "+ str(expenses_max)
     
def sum_show(category,sum_i):
    print "The sum for " + category + " is " + str(sum_i)
    
def exact(l,value):
    '''
    Description:This function print the expenses with a exact number for value
    Input:  l-list of expenses, value-a number for search
    '''
    for i in l:
        if int(i[2])  == value:
            print "Month: " + str(i[0]) + " Day: " + str(i[1])
    
