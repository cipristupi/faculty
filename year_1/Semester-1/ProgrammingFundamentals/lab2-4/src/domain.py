from datetime import datetime
from calendar import monthrange
#from ui import *


now=datetime.now()#the current date
year=now.year
month=now.month
day=now.day


def add_today(list_l,value_s,category):#T
    '''
    Description: This function add a new expenses to the list with today day
    Input: the list for expenses
    Output: the modified list
    '''

    list_l.append([int(month),int(day),int(value_s),str(category)])
    return

def add_day(list_l,value_s,category,day):#T
    '''
    Description:This function add a new expenses to the list with a gived day
    Input: list_l-the list 
    Output: the modified list
    '''  
    list_l.append([int(month),int(day),int(value_s),str(category)])
    return



def remove(l,day):#T
    '''
    Description:This function remove expenses for a gived day
    Input: list- list of expenses
    Output: modified list
    '''
    #Work
    
    i=0
    while i<len(l):
        if int(l[i][1])==int(day):
            l.pop(i)
        i=i+1
    return  
            
def remove_from(l,s_day,f_day):
    '''
    Description: Remove expenses between two days
    Input: l-list, s_day-start day, f_day-final day
    Output:the modified list
    '''
    #Almost working
    i=0
    while i<len(l):
        if int(l[i][1])>=int(s_day) and int(l[i][1])<=int(f_day):
            l.pop(i)
        i=i+1
    return 
                  
def remove_for(l,category):#T
    '''
    Description: This function remove expenses for a gived category from curent month
    Input: the list
    Output: the modified list
    '''
    #Wiork
    i=0
    while i<len(l):
        if str(l[i][3])==category:
            l.pop(i)
        i=i+1
    return


def replace(l,day,category,value):#T
    '''
    Description: This function replace the expenses value from a given day and category
    Input: l-list of expenses, day-day after we search , category - search critaria , value-the new value 
    Output: the modified list
    '''
    i=0
    while i<len(l):
        if int(l[i][1])==int(day) and l[i][3]==category:
            l[i][2]=int(value)
        i=i+1
    return
    
def sum_f(l,category):#T
    '''
    Description: Function calculate the sum of expenses for a given category
    Input: l-list ,category-category for whith we calculate the sum
    Output: sum for a given category
    '''
    sum_i=0
    for i in l:
        if str(i[3])==category:
            sum_i=sum_i+int(i[2])
    return sum_i


def sort_day(l,sort_type):
    '''
    Description: This function sort the list after day
    Input: l-list for sorting, sort_type - sort asc or desc
    Output:the modified list
    '''
    i=0
    if sort_type == "asc":
        while i<len(l)-1:
            j=i+1
            while j<len(l):
                if int(l[i][1]) > int(l[j][1]):
                    aux = l[i]
                    l[i]=l[j]
                    l[j]=aux
                j=j+1
            i=i+1
    else:
        while i<len(l)-1:
            j=i+1
            while j<len(l):
                if int(l[i][1]) < int(l[j][1]):
                    aux = l[i]
                    l[i]=l[j]
                    l[j]=aux
                j=j+1
            i=i+1
   
def sort_type(l,sort_type):
    '''
    Description:This function sort the list after type
    Input: l-list for sorting, sort_type- sort asc or desc
    Output: the modified list
    '''
    i=0
    if sort_type == "asc":
        while i<len(l)-1:
            j=i+1
            while j<len(l):
                if l[i][3] > l[j][3]:
                    aux = l[i]
                    l[i]=l[j]
                    l[j]=aux
                j=j+1
            i=i+1
    else:
        while i<len(l)-1:
            j=i+1
            while j<len(l):
                if l[i][3] < l[j][3]:
                    aux = l[i]
                    l[i]=l[j]
                    l[j]=aux
                j=j+1
            i=i+1 

def filter_category(l,category):#T
    '''
    Description: This function create a new list after a given filter, in this case a category
    Input: l-the principal list, category:the filter
    Output: lf-a new list with that filter
    '''
    lf=[]
    i=0
    while i<len(l):
        if l[i][3]==category:
            lf.append(l[i])
        i=i+1
    return lf

def filter_greater(l,category,value):#T
    '''
    Description: This function create a new list after a given category and a given value
    Input:l-the principal list, category-a category for filter, value-a value for filter
    Output: lf - a new list with the given properties
    '''
    lf=[]
    i=0
    while i<len(l):
        if l[i][3] == category and int(l[i][2])>int(value):
            lf.append(l[i])
        i=i+1
    return lf
