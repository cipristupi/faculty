'''
Autor: Stupinean Ciprian-Viorel
Date: 10.11.2011
Group: 915


'''
#import section
from ui import *
from domain import *
from tests import *


def c_l(l):
    l.append([10,25,300,"food"])
    l.append([9,30,1,"medic"])
    l.append([8,10,1000,"transport"])
    l.append([10,20,2000,"book"])
    l.append([8,20,100,"school"])
    l.append([1,1,50,"BOOK"])
    l.append([1,4,10,"INTERNET"])

def MainTest(l):
    testAdd_Day(l)
    testAdd_Today(l)
    testRemove(l)
    testRemove_from(l)
    testRemove_For(l)
    testReplace(l)
    testFiltercategory(l)
    testFiltergreater(l)
    

def main():
    expenses_list=[]
    #expenses_back_up=[]
    c_l(expenses_list)
    #MainTest(expenses_list)
    expenses_back_up=expenses_list[:]
    print "For help tape 'help' "
    print 
    cmd=raw_input("Command: ")
    while cmd!="exit":
        if cmd=="help":
            show_menu()
            cmd=raw_input("Command: ")
        elif cmd=="1":#today add
            value=read_value()
            category=read_category()
            expenses_back_up=expenses_list[:]
            add_today(expenses_list,value,category)
            #expenses_back_up=expenses_list
            print "Succes"+"\n"
            cmd=raw_input("Command: ")
        elif cmd=="2":#day add
            day=read_day()
            category=read_category()
            value=read_value()
            expenses_back_up=expenses_list[:]
            add_day(expenses_list,value,category,day)
            print "Succes" +"\n"
            #expenses_back_up=expenses_list
            cmd=raw_input("Command: ")
        elif cmd=="3":#show all
            show_all(expenses_list)
            print 
            cmd=raw_input("Command: ")
        elif cmd=="4":#show for category
            category=read_category()
            show_for(expenses_list,category)
            print
            cmd=raw_input("Command: ")
        elif cmd=="5":#expenses less that a given value before a day
            value=read_value()
            day=read_day()
            show_less(expenses_list,value,day)
            print
            cmd=raw_input("Command: ")
        elif cmd=="6":#show greater that a value
            value=read_value()
            show_greater(expenses_list,value)
            print
            cmd=raw_input("Command: ")
        elif cmd=="7":#remove for a day
            day=read_day()
            expenses_back_up=expenses_list[:]
            remove(expenses_list,day)
            cmd=raw_input("Command: ")
        elif cmd=="8":#remove for a category
            category=read_category()
            expenses_back_up=expenses_list[:]
            remove_for(expenses_list,category)
            cmd=raw_input("Command: ")
        elif cmd=="9":#work!!!!
            print "From",
            s_day=read_day()
            print "To",
            f_day=read_day()
            expenses_back_up=expenses_list[:]
            remove_from(expenses_list,s_day,f_day)
            cmd=raw_input("Command: ")
        elif cmd=="10":#replace a value
            day=read_day()
            category=read_category()
            print "New",
            value=read_value()
            expenses_back_up=expenses_list[:]
            replace(expenses_list,day,category,value)
            cmd=raw_input("Command: ")
        elif cmd=="11":#sum for a category
            category=read_category()
            sum_i = sum_f(expenses_list,category)
            sum_show(category,sum_i)
            print 
            cmd=raw_input("Command: ")
        elif cmd=="12":#day with maximum expenses
            max_day_show(expenses_list)
            print 
            cmd=raw_input("Command: ")
        elif cmd=="13":#show category for a exact value
            value=read_value()
            exact(expenses_list,value)
            print
            cmd=raw_input("Command: ")
        elif cmd=="14":#sort asc day
            sort_day(expenses_list,"asc")
            show_all(expenses_list)
            print
            expenses_back_up.append(expenses_list)
            cmd=raw_input("Command: ")
        elif cmd=="15":#sort desc day
            sort_day(expenses_list,"desc")
            show_all(expenses_list)
            print
            expenses_back_up.append(expenses_list)
            cmd=raw_input("Command: ")
        elif cmd=="16":#sort asc type
            expenses_back_up=expenses_list
            sort_type(expenses_list,"asc")
            show_all(expenses_list)
            cmd=raw_input("Command: ")
        elif cmd=="17":#sort desc type
            expenses_back_up=expenses_list
            sort_type(expenses_list,"desc")
            show_all(expenses_list)
            cmd=raw_input("Command: ")
        elif cmd=="18":#filter after a category
            #expenses_back_up=expenses_list
            category=read_category()
            lf=filter_category(expenses_list,category)
            show_all(lf)
            cmd=raw_input("Command: ")
        elif cmd=="19":#filter after value and category
            category=read_category()
            value=read_value()
            #expenses_back_up=expenses_list
            lf=filter_greater(expenses_list,category,value)
            show_all(lf)
            cmd=raw_input("Command: ")
        elif cmd=="undo":
            #print expenses_back_up
            expenses_list=expenses_back_up[:]
            cmd=raw_input("Command: ")
        else:
            print "Bad command. Please try again"
            cmd=raw_input("Command:")
            
    print "Goodbye"


main()