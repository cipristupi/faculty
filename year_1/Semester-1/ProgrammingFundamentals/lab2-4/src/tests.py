from domain import *

def testSum(l):
    assert(sum_f(l,"food")==300)
    assert(sum_f(l,"book")==2050)
    
def testAdd_Today(l):
    add_today(l,100,"test")
    assert(len(l)==7)
    add_today(l,200,"food")
    assert(len(l)==8)
    
def testAdd_Day(l):
    add_day(l,200,"food",25)
    assert(len(l)==7)
    add_day(l,100,"book",20)
    assert(len(l)==8)
    
def testRemove(l):
    remove(l,1)
    a=[1,1,50,"book"]
    assert((a in l)==False)
    remove(l,10)
    a=[8,10,1000,"transport"]
    assert((a in l)==False)
    
def testRemove_from(l):
    
    i=len(l)
    remove_from(l,1,6)
    assert((len(l)!=i)==True)
    
def testRemove_For(l):
    i=len(l)
    remove_for(l,"food")
    assert((len(l)!=i)==True)
    
def testReplace(l):
    replace(l,1,"BOOK",51)
    a=[1,1,51,"BOOK"]
    assert((a in l) == True)
    
def testFiltercategory(l):
    lf=filter_category(l,"BOOK")
    assert([1,1,50,"BOOK"] in lf)  
    
def testFiltergreater(l):
    lf=filter_greater(l,"BOOK",1)
    assert([1,1,50,"BOOK"] in lf)  
    
    
    
    
    