from domain.activity import *
from domain.person import *
from controller.activityController import *
from controller.personController import *
from domain.validator import *
from repository.personRepository import RepositoryException
from datetime import datetime
from genericM import *
from NVUtils.Utils import  *

class UI:
    def __init__(self,activity_controller,person_controller):
        self.__activity_controller=activity_controller
        self.__person_controller=person_controller
        
    def printMenu(self):
        menu_s = '\nAvailable commands:\n'
        menu_s += '\t 1 - Add person\n'#work
        menu_s += '\t 2 - Add activity\n'#work
        menu_s += '\t 3 - Remove person\n'#work
        menu_s += '\t 4 - Remove activity\n'#work
        menu_s += '\t 5 - Update person name\n'#work
        menu_s += '\t 6 - Update activity date\n'#work
        menu_s += '\t 7 - Search for a person after ID.\n'#work
        menu_s += '\t 8 - Search for a person after Name.\n'#work
        menu_s += '\t 9 - Search for a activity after date\n'#work
        menu_s += '\t 10 - Search for a activity after time\n'#work
        menu_s += '\t 11 - Display all persons\n'#work
        menu_s += '\t 12 - Display activities for a person\n'#work
        menu_s += '\t 13 - Display activities for a person ordered alphabetically\n'#work
        menu_s += '\t 14 - Display activities for a person ordered after date  \n'#work
        menu_s += '\t 15 - Display activities for a person ordered after time \n'#work
        menu_s += '\t 16 - Display persons having activities in a certain interval [date1,date2]\n'#work
        menu_s += '\t 17 - Display persons having activities in a certain interval ordered alphabetically\n'#work
        menu_s += '\t 18 - Update person address\n'#work
        menu_s += '\t 19 - Update person phone\n'#work
        menu_s += '\t 20 - Update activity time\n'#Work
        menu_s += '\t 21 - Update activity description.\n'
        menu_s += '\t 0 - Exit\n'
        print menu_s
        
    def validInputCommand(self,command):
        availableCommands = ['1', '2', '3', '4', '5', '6', '7', '8','9','10','11','12','13','14','15','16','17','18','19','20','21','22', '0','50'];
        return (command in availableCommands)
       
    def mainMenu(self):
        UI.printMenu(self) 
        while True:
            command = raw_input("Enter command: ").strip()
            while not UI.validInputCommand(self, command):
                print "Please give a valid command!"
                command=raw_input("Enter command: ")
            if command=='0':
                print "exit.."
                return
            elif command=="1":#add person
                self.addPerson()
                print
            elif command=="2":#add activity
                self.addActivity()#check if the id is in the list of persons
                print
            elif command=="3":#remove person
                persID=self.readID()
                p=self.__person_controller.getPersonByID(persID)
                if p!=None:
                    self.__person_controller.deleteByID(persID)
                    self.__activity_controller.deleteByID(persID)
                    print "Succefully deleted."
                else:
                    print "Person doesn't exist."
                print 
            elif command=="4":#remove activity
                persID=self.readID()
                date=self.readDate()
                time=self.readTime()
                self.__activity_controller.deleteBy(persID,date,time)
                a=self.__activity_controller.getActivity(persID,date,time)
                if a == None:
                    print "Succefully deleted."
                else:
                    print "Please try again."
                print 
            elif command=="5":#update person name
                persID=self.readID()
                name=raw_input("New name: ")
                self.__person_controller.setNewName(persID,name)
                print 
            elif command=="6":#update date activity
                persID=self.readID()
                date=self.readDate()
                time=self.readTime()
                print "New ",
                newDate=self.readDate()
                self.__activity_controller.setNewDate(persID,date,time,newDate)
                print  
            elif command=="7":#search for a person after ID
                persID=self.readID()
                p=self.__person_controller.getPersonByID(persID)
                if p != None:
                    print p.printStr()
                else:
                    print "Person doesn't exist."
                print 
            elif command=="8":#search for a person after Name
                name=raw_input("Person name: ")
                p=self.__person_controller.getPersonByName(name)
                if p != None:
                    print p.printStr()
                else:
                    print "Person doesn't exist."
                print 
            elif command=="9":#search for activity after date
                date=self.readDate()
                a=self.__activity_controller.getActivityDate(date)
                if a!=None:
                    print a.printStr()
                else:
                    print "Activity doesn't exist."
                print 
            elif command=="10":#search for activity after time
                time=self.readTime()
                a=self.__activity_controller.getActivityTime(time)
                if a!=None:
                    print a.printStr()
                else:
                    print "Activity doesn't exist."
                print 
            elif command=="11":#display all persons
                self.displayPers()
                print    
            elif command=="12":#diplay all activities for a person
                persID=self.readID()
                self.displayActivities(persID)
                print 
            elif command=="13":#display all activities for a person ordered alphabetically
                persID=self.readID()
                self.displayActivitiesOrder(persID,"1")
                print 
            elif command=="14":#display activities for a person ordered after date
                persID=self.readID()
                self.displayActivitiesOrder(persID,"2")
                print 
            elif command=="15":#display activities for a person ordered after time
                persID=self.readID()
                self.displayActivitiesOrder(persID,"3")
                print 
            elif command=="16":#display persons having activities in a certain interval
                print "Start ",
                s_date=self.readDate()
                print "Final ",
                f_date=self.readDate()
                self.displayPersonInterval(s_date, f_date)
                #self.test()
                print 
            elif command=="17":#display persons having activities in a certain interval ordered alphabetically
                print "Start ",
                s_date=self.readDate()
                print "Final ",
                f_date=self.readDate()
                self.displayPersonIntervalOrder(s_date, f_date)
                # self.test2()
                print 
            elif command=="18":#Update person address
                persID=self.readID()
                address=raw_input("New address:")
                self.__person_controller.setNewAddress(persID,address)
                print 
            elif command=="19":#Update person phone
                persID=self.readID()
                phone=raw_input("New phone: ")
                self.__person_controller.setNewPhone(persID,phone)
                print 
            elif command=="20":#Update activity time
                persID=self.readID()
                date=self.readDate()
                time=self.readTime()
                print "New ",
                newTime=self.readTime()
                self.__activity_controller.setNewTime(persID,date,time,newTime)
                print 
            elif command=="21":#Update activity description.
                persID=self.readID()
                date=self.readDate()
                time=self.readTime()
                description=raw_input("New description: ")
                self.__activity_controller.setNewDescription(persID,date,time,description)
                print 
            elif command=="22":
                a=self.__activity_controller.getAllActivities()
                self.printActivities(a)
                print "22"
            elif command=="50":
                self.VictorGeneric()
                print "50"
                
                
                
    def readID(self):#Work
        '''
        Description: reads the id of person until a numeric value is entered
        Input:-
        Output: the readed id
        '''
        stop=True
        while stop:
            try:
                persID=int(raw_input("Person ID: "))
                stop=False
            except:
                print "Please enter a numeric value!\n"
        return persID  
    
    def readDate(self):#Work
        '''
        Descriptin: reads the date of activity until a valid date is entered
        Input: nothing
        Output: the readed date
        '''  
        stop=True
        while stop:
            try:
                dt_s=str(raw_input("Date(format: day.month.year):"))
                dt  = datetime.strptime(dt_s,'%d.%m.%Y').date()
                stop=False
            except:
                print "The date format is not valid."
        return dt
    
    def readTime(self):#Work
        '''
        Description: reads the time of activity until a valid format is entered
        Input:nothing
        Output: the readed time
        '''
        stop=True
        while stop:
            try:
                t_s=str(raw_input("Time: "))
                time=datetime.strptime(t_s,'%H:%M').time()
                stop=False
            except:
                print "The time format is not valid."
        return time
                
    def addPerson(self):#Work
        '''
        Description:Reads a person and adds
        Input: -
        Output: 
        '''
        try:
            persID=self.readID()
            name=raw_input("Name: ")
            phone=str(raw_input("Phone: "))
            address=raw_input("Address: ")
            self.__person_controller.addPerson(persID,name,phone,address)
            print "Person successfully added!\n"
        except ValidatorException as ex:
            print ex.getErrors()
        except RepositoryException as ex:
            print ex.getErrors()
            
        
    def addActivity(self):#Work
        '''
        Description: reads a activity and adds
        Input:-
        Output:
        '''
        try:
            persID=self.readID()
            date=self.readDate()
            time=self.readTime()
            description=str(raw_input("Description: "))
            self.__activity_controller.addActivity(persID,date,time,description)
            print "Activity successfully added! \n"
        except ValidatorException as ex:
            print ex.getErrors()
        
    def printPersons(self,perslist):#Work
        '''
        Description:Prints a list of objects on console.
        Input: perslist
        '''
        for p in perslist:
            print p.printStr()
            print
            
    
    def printActivities(self,al):
        '''
        Description: prints the list of activities
        Input:al- the list of activities
        Output: nothing
        '''
        for i in al:
            print i.printStr()
            print
            

            
    def displayPers(self):
        '''
        Description: display the list of persons without order prop
        '''
        persList=self.__person_controller.getAllPersons()
        self.printPersons(persList)
        
    def displayActivities(self,persID):
        '''
        Description: display the list o activities
        '''
        actList=self.__activity_controller.getAllActivitiesByID(persID)
        self.printActivities(actList)
        
    def displayActivitiesOrder(self,persID,t):
        '''
        Description: display the list of activities ordered alphabeticaly
        '''
        actList=self.__activity_controller.order(persID,t)
        self.printActivities(actList)
        
    def displayPersonInterval(self,date1,date2):#Work
        '''
        Description: display the persons which have activities in a certain interval
        Input: date1-start date, date2-final date
        Output: f
        '''
        actList=self.__activity_controller.getByIntervalID(date1,date2)
        #print str(len(actList))
        '''for i in actList:
            p=self.__person_controller.getPersonByID(i)
            if p!=None:
                print p.printStr()'''
        a=self.__person_controller.getListPersID(actList)
        self.printPersons(a)
        
            
    def displayPersonIntervalOrder(self,date1,date2):
        '''
        Description: display the persons which have activities in a certain interval
        Input: date1-start date, date2-final date
        Output: 
        '''
        actList=self.__activity_controller.getByIntervalID(date1,date2)
        a=self.__person_controller.getListPersID(actList)
        self.printPersons(a)
                

        
    def test(self):#Not important
        dt=datetime.strptime("01.01.2000",'%d.%m.%Y').date()
        dt2=datetime.strptime("30.12.2015",'%d.%m.%Y').date()
        self.displayPersonInterval(dt, dt2)
        
    def test2(self):#Not important
        dt=datetime.strptime("01.01.2000",'%d.%m.%Y').date()
        dt2=datetime.strptime("30.12.2015",'%d.%m.%Y').date()
        self.displayPersonIntervalOrder(dt,dt2)
        
        
    def genericModuleTest(self):
        l=self.__person_controller.getAllPersons()
        ln = sort_fct(l,self.greaterNo,True)
        self.printPersons(ln)
        p=self.__person_controller.getPersonByID(5)
        print inStruct(l,p)
        la = filterList(l,self.filterC)
        self.printPersons(la)
        pass
    
    def VictorGeneric(self):
        l=self.__person_controller.getAllPersons()
        sortd= Sorter()
        fiterd= Filter()
        srcd=Searcher()
        
        ln  = sortd.bubbleSort(l, self.greaterNo)#Work
        self.printPersons(ln)
        
        p=self.__person_controller.getPersonByID(3)#Work
        print srcd.isinList(l,p)
        
        la= fiterd.filter(l, self.filterC)#Work
        self.printPersons(la)
        pass
    
    def greaterNo(self,s1,s2):
        return s1.getID() < s2.getID()
    
    def filterC(self,s1):
        return s1.getName()=="Fuego"
        
        
    
        
            