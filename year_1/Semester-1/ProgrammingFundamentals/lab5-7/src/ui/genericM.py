#Generic Module with contain functions for searching,sorting and filtering


def sort_fct(l,cmpr,reverse=False):#Selection Sort
    '''
    This function sort a given list after a given criterion of comparasion
    Input:  l -list to be sort
            cmpr - a function to be used in sorting, the compare function must have 2 parameters represent the number which will be compare
            reverse- bool value, True - sorted as if each comparison are reverse
    Output: the sorted list
    '''
    for i in range(0,len(l)-1):
        for j in range(i+1,len(l)):
            if(cmpr(l[i],l[j]) and reverse==False) or (not cmpr(l[i],l[j]) and reverse==True):
                aux=l[i]
                l[i]=l[j]
                l[j]=aux
    return l
    
def shaker_sort(l):
    '''
    Shaker sort or 'bidirectional bubble sort'
    Orders in descending order
    Input:  l - list to be sorted
    '''
    lenl = len(l) 
    for i in range(0,lenl/2):
        swapped = False
        for j in range(i,lenl - i - 1):#from left to right
            if l[j]  < l[j+1]:
                aux=l[j]
                l[j]=l[j+1]
                l[j+1]=aux
                swapped = True
                
        for j in range(lenl - 2 - i , i ,-1):#from right to left
            if l[j] > l[j-1]:
                aux=l[j]
                l[j]=l[j-1]
                l[j-1]=aux
                swapped = True
        if not swapped:
            break

def inStruct(l,e):#Check if the given element is into the list
    '''
    This function check if the given element is into the given list
    Input: l - list where we check if the given element exist
            e- element to check if exist
    Output:  True - if the given element exit into the list, False otherwise
    '''
    i=0
    while i < len(l):
        if l[i]==e:
            return True
        i=i+1
    return False

def posInStruct(l,e):#Return the position where find the given element
    '''
    This function return position where the given element is into the given list
    Input: l - list where we check if the given element exist
            e - element to check if exist
    Output:  return position if the given element exist or -1 if doesn't exist
    '''
    i=0
    while i < len(l):
        if l[i]==e:
            return i
        i=i+1
    return -1


def filterList(l,cmpr):
    '''
    This function return a new list contains elements with the given propertie
    Input: l-list for filtring
            cmpr -  propertie for the elements, is a function 
    Output:  the new list with elemets having the given properti
    '''
    ln=[]
    i=0
    while i<len(l):
        if cmpr(l[i])==True:
            ln.append(l[i])
        i=i+1

    return ln

def greaterNo(no1,no2):
    return no1>no2
def lessNo(no1,no2):
    return no1<no2

def testSort():
    l=[1,2,55,-100,0,0,1,2,5,3]
    l = sort_fct(l,lessNo)
    #l.sort()
    #shaker_sort(l)
    print l

def testInStruct():
    l=[1,2,55,-100,0,0,1,2,5,3]
    assert(inStruct(l,10)==False)
    assert(inStruct(l,0)==True)

def testPosInStruct(l,e):
    l=[1,2,55,-100,0,0,1,2,5,3]
    assert(inStruct(l,10)==-1)
    assert(inStruct(l,2)==1)

testSort()
testInStruct()
