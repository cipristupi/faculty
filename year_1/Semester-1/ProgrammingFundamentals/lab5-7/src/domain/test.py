from activityRepository import *
from activityFileRepository import *
from personFileRepository import *
from personRepository import *
from domain.person import *
from domain.validator import  PersonValidator
from controller.personController import *
from controller.activityController import *
import unittest
from unittest import TestCase
from genericModule import *

class testGeneric(TestCase):
    
    def setUp(self):
        self.persVal=PersonValidator()
        self.persRepo = PersonRepository(self.persVal)
        #self.persRepo = PersonRepository(self.persVal)
        self.persCont= PersonController(self.persRepo)
        self.persCont.testAddInit()
        
    def tearDown(self):
        pass
    
    def testSort(self):
        l=self.persRepo.getAll()
        ln = sort_fct(l,self.greaterByName)
        print ln
        
    def greaterByName(self,s1,s2):
        return s1.getName() > s2.getName()
        
        