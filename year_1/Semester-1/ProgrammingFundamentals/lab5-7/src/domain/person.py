class Person:
    def __init__(self,personID,name,phone,address):
        '''
        Constructor for Person class
        Input: personID-integer,name,phone,address-strings
        '''
        self.__personID=personID
        self.__name=name
        self.__phone=phone
        self.__address=address
        
    def getID(self):
        '''
        Getter for persID of an person
        Output: The id of an person
        Postconditions: returns the ID of an person
        '''
        return self.__personID
    
    def getName(self):
        '''
        Getter for name of an person
        Output: the name of an person
        Postconditions: returns the name of an person
        '''
        return self.__name
    
    def getPhone(self):
        '''
        Getter for phone of an person
        Output: the phone of an person
        Postconditions: returns the phone of an person 
        '''
        return self.__phone
    
    def getAddress(self):
        '''
        Getter for address of an person
        Output:  the address of an person
        Postconditions: returns the address of an person
        '''
        return self.__address
    
    
    def setID(self,newID):
        '''
        Setter for ID
        Input:  newID-integer 
        Preconditions: the newID must be unique and non-negative integer
        '''
        self.__personID=newID
    
    def setName(self,newname):
        '''
        Setter for name
        Input: newname- string
        Preconditions: newname must be a non-empty string
        '''
        self.__name=newname
        
    def setPhone(self,newphone):
        '''
        Setter for phone
        Input: newphone-string
        Preconditions: newphone must be a non-empty string
        '''
        self.__phone=newphone
        
    def setAddress(self,newaddress):
        '''
        Setter for address
        Input: newaddress -string
        Preconditions: newaddress must be a non-empty string
        '''
        self.__address=newaddress
        
    def printStr(self):
        '''
        This function return the string for printing
        '''
        s=""
        s+="Person ID: " + str(self.__personID) + "\n"
        s+="Name: " + str(self.__name) +"\n"
        s+="Phone number: " + str(self.__phone) + "\n"
        s+="Address: " +str(self.__address)
        return s 
    
    def __eq__(self,p):
        if p == None:
            return False
        return self.__personID == p.__personID and self.__name == p.__name and self.__address == p.__address and self.__phone ==p.__phone 
    
    
def testCreatePerson():
    a=Person(1,"Ciprian","0741409706","Cluj")
    assert(a.getID()==1)
    assert(a.getName()=="Ciprian")
    assert(a.getPhone()=="0741409706")
    assert(a.getAddress()=="Cluj")
    
    a.setID(2)
    assert(a.getID()==2)
    a.setName("Stupinean")
    assert(a.getName()=="Stupinean")
    a.setPhone("0745549832")
    assert(a.getPhone()=="0745549832")
    a.setAddress("Bistrita")
    assert(a.getAddress()=="Bistrita")
    
testCreatePerson()


        
    