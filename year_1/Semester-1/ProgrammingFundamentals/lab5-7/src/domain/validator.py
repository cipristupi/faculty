from domain.person import Person
from domain.activity import Activity

class ValidatorException(Exception):
    def __init__(self,errors):
        self.__errors=errors
    
    def getErrors(self):
        return self.__errors
    
class PersonValidator:
    def validate(self,person):
        '''
        Description: validate a person
        Input: person: a person
        Raise: ValidationException if the personID is negative or name is empty or phone is empty or address
        '''
        errors=""
        if person.getID()<0:
            errors =errors + "Person ID cannot be negative. \n"
        if person.getName() =="":
            errors = errors + "Name cannot be empty. \n"
        if person.getPhone() =="":
            errors = errors + "Phone cannot be empty. \n"
        if person.getAddress() =="":
            errors = errors + "Address cannot be empty."
        
        if errors!="":
            raise ValidatorException(errors)
        return errors   
    
    
class ActivityValidator:
    def validate(self,activity):
        '''
        Description: validate a activity
        Input: activity
        Raise ValidationException if the personID is negative or date is empty or time is empty
        '''
        errors=""
        if activity.getID()<0:
            errors =errors + "Person ID cannot be negative. \n"
        if activity.getDate()==" ":
            errors=errors+"Date cannot be empty. \n"
        if activity.getTime()==" ":
            errors=errors+"Time cannot be empty. \n"
            
        if errors!="":
            raise ValidatorException(errors)
        return errors
    
    
def testPersonValidator():
    validator=PersonValidator()
    person=Person(-1,"","","") 
    try:
        validator.validate(person)
        assert False
    except ValidatorException as ex:
        assert ex!=""
        
testPersonValidator()