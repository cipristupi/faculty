from person import *
from activity import *
import unittest
from unittest import TestCase
from datetime import datetime

#test for the persons
class PersonTestCase(TestCase):
    def setUp(self):
        self.pers=Person(16,"Amalia","0745549832","Suceava")
        
    def tearDown(self):
        pass
    
    def testPerson1(self):
        #assertEqual is used to check for an expected result
        self.assertEqual(self.pers.getID(),16)
        self.assertEqual(self.pers.getName(),"Amalia")
        self.assertEqual(self.pers.getAddress(),"Suceava")
        self.assertEqual(self.pers.getPhone(),"0745549832")
        
    def testPerson2(self):
        #assertTrue is used to verify a condition
        self.pers.setID(15)
        self.pers.setAddress("Bistrita")
        self.pers.setName("Iftimut")
        self.pers.setPhone("0745549833")
        #
        self.assertEqual(self.pers.getID(),15)
        self.assertEqual(self.pers.getName(),"Iftimut")
        self.assertEqual(self.pers.getAddress(),"Bistrita")
        self.assertEqual(self.pers.getPhone(),"0745549833")
        
class ActivityTestCase(TestCase):
    def setUp(self):
        dt=datetime.strptime("13.10.2013",'%d.%m.%Y').date()
        t=datetime.strptime("15:30",'%H:%M').time()
        self.act=Activity(1,dt,t,"Something")
        

    def tearDown(self):
        pass
    
    def testActivity1(self):
        dt=datetime.strptime("13.10.2013",'%d.%m.%Y').date()
        t=datetime.strptime("15:30",'%H:%M').time()
        self.assertTrue(self.act.getID()==1)
        self.assertTrue(self.act.getDate()==dt)
        self.assertTrue(self.act.getTime()==t)
        self.assertTrue(self.act.getDescription()=="Something")
        
    def testActivity2(self):
        dt=datetime.strptime("15.10.2013",'%d.%m.%Y').date()
        t=datetime.strptime("10:30",'%H:%M').time()
        self.act.setDate(dt)
        self.act.setDescription("other")
        self.act.setID(5)
        self.act.setTime(t)
        
        self.assertTrue(self.act.getID()==5)
        self.assertTrue(self.act.getDate()==dt)
        self.assertTrue(self.act.getTime()==t)
        self.assertTrue(self.act.getDescription()=="other")
        
if __name__=='__name__':
    unittest.main()
        
        