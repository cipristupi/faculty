
class Activity:
    def __init__(self,personID,date,time,description):
        '''
        Constructor for Activities class
        Input: personID-integer,date -date type,time -time type,description-strings
        '''
        self.__personID=personID
        self.__date=date
        self.__time=time
        self.__description=description
        
    def getID(self):#T
        '''
        Getter for personID of an activity
        Output:The personID
        Postconditions: returns the ID of an activity
        '''
        return self.__personID
    
    def getDate(self):#T
        '''
        Getter for date of a activity
        Output:The date of an activity
        Postconditions: returns the date of an activity
        '''
        return self.__date
    
    def getTime(self):#T
        '''
        Getter for time of an activity
        Output: The time of an activity
        Postconditions: returns the time of an activity
        '''
        return self.__time
    
    def getDescription(self):#T
        '''
        Getter for description of an activity
        Output: The description of an activity
        Postconditions: returns the description of an activity 
        '''
        return self.__description
    
    def setID(self,newID):
        '''
        The setter for ID
        Input: newID-integer
        Preconditions: newID must be an non-negative integer
        '''
        self.__personID=newID
    
    def setDate(self,newdate):
        '''
        The setter for Date
        Input: newdate - a date
        Preconditions: newdate must be an valid date
        '''
        self.__date=newdate
        
    def setTime(self,newtime):
        '''
        The setter for Time
        Input: newtime - a time
        Preconditions: newtime must be an valid time
        '''
        self.__time=newtime
        
    def setDescription(self,newdescription):
        '''
        The setter for Description
        Input: newdescription - a string
        Preconditions: setDescription must be a string(empty of non-empty)
        '''
        self.__description=newdescription
        
    def printStr(self):
        '''
        Description:Return all information of the activities
        '''
        s=""
        s+="Person ID: " + str(self.__personID) + "\n"
        s+= "Date: " + str(self.__date) + "\n"
        s+="Time: " + str(self.__time) +"\n"
        s+="Description: " + str(self.__description)
        return s
    
    def __eq__(self,a):
        if a==None:
            return False
        return self.__personID == a.__personID and self.__date == a.__date and self.__description == a.__description and self.__time == a.__time
        
    
def testCreateActivity(): 
    a=Activity(1,"25.10.2013","10:20","Something")
    assert(a.getID()==1)
    assert(a.getDate()=="25.10.2013")
    assert(a.getTime()=="10:20")
    assert(a.getDescription()=="Something")
    
testCreateActivity()