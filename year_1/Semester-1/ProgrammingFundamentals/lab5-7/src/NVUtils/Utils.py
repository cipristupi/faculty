class Filter():
    def __init__(self):
        pass

    def filter(self,list,condition):
        res=[]
        for el in list:
            if condition(el):
                res.append(el)
        return res

class Searcher():
    def __init__(self):
        pass

    def isinList(self,list,val):
        '''
            Finds an element in a list by the provided value
            in: list, array to search through
                val, int, a unique value to search for
            out: True if found, False otherwise
        '''
        for e in list:
            if e == val:
                return True
        return False

    def findIndex(self,list,val):
        '''
            Returns the index of an element in the list (-1 if not found)
        '''
        for i in range(len(list)):
            if list[i] == val:
                return i
        return -1

    def findElement(self,list,val):
        '''
            Returns an element in a list if found by the provided value
            (None is returned if not found)
        '''
        for i in range(len(list)):
            if list[i] == val:
                return list[i]
        return None

class Sorter():
    def __init__(self):
        pass

    def bubbleSort(self,l,compare):
        '''
            Sorts a list using Bubble Sort algorithm
            in: l, list of elements to sort
                compare, function for comparisons (True if first param > second)
            out: l, list, the ordered list, according to 'compare' function
        '''
        sorted = False
        offset = 0    #offset sets the end of the list iteration
        length = len(l)    #length of the list
        if length == 1:    #sort only if there is more than one element
            return l
        while not sorted:
            sorted = True
            for i in range(1,length - offset):
                if compare(l[i-1],l[i]):    #use boolean function to compare
                    tmp = l[i]    #swapping two elements
                    l[i] = l[i-1]
                    l[i-1] = tmp
                    sorted = False    #keep sorting if there was a swap
            offset+=1    #make the for iterations shorter after each while loop
        return l    #returns the sorted list (might be useful.. maybe..)

    def shellSort(self,l,compare):
        '''
            Sorts a list using Shell Sort algorithm
            in: l, list of elements to sort
                compare, function for comparisons (True if first param > second)
            out: l, list, the ordered list, according to 'compare' function
        '''
        gap = len(l)/2      #gaps are 1/(2^n)..1
        while gap > 0:        #for each gap value; 1 will be last
             for i in range(gap, len(l)):    #insertion sort
                 val = l[i]
                 j = i
                 while j>=gap and compare(l[j-gap],val):
                     l[j] = l[j - gap]
                     j = j-gap
                     #print l
                 l[j] = val
             gap = gap/2    #next gap
             #print "\n"
        return l

    def insertSort(self,list,compare):
        '''
            Sorts a list using Insertion Sort algorithm
            in: l, list of elements to sort
                compare, function for comparisons (True if first param > second)
            out: l, list, the ordered list, according to 'compare' function
        '''
        for ri in range(1, len(list)):
            rval = list[ri]
            i = ri
            while i>0 and compare(list[i-1],rval):
                list[i] = list[i-1]
                i = i-1
            list[i] = rval
        return list

    def combSort(self,l,compare):
        '''
            Sorts a list using Comb Sort algorithm
            in: l, list of elements to sort
                compare, function for comparisons (True if end param > second)
            out: l, list, the ordered list, according to 'compare' function
        '''
        gap = len(l)
        swapped = True
        if gap < 2:
            return l    #don't sort a list of just one element
        while gap > 1 or swapped:
            gap = int(gap//1.3)    #calculate next gap (shrink factor is 1.3)
            gap = max(1, gap)    #gap cannot be 0; smallest allowed is 1
            swapped = False    #"BubbleSort" with custom gap
            for i in range(0, len(l)-gap, gap):
                if compare(l[i], l[i+gap]):
                    l[i], l[i+gap] = l[i+gap], l[i]
                    swapped = True
        return l

    def rSort(self,l,i,last,compare):
        '''
            Recursive Bubble Sort
            in: l, list of elements to sort
                i, int, iterator, beginning of list
                last, int, index, end of list
            out: l, list, the ordered list, according to 'compare' function
        '''
        if i<last and last>0:    #list must not be empty/single element
            if compare(l[i], l[i+1]):    #compare two neighbours
                l[i],l[i+1]=l[i+1],l[i]    #swap
            self.rSort(l,i+1,last,compare) #'for' iterates through list
            self.rSort(l,i,last-1,compare) #'while' repeat for with shorter list
        return l                           #as the last element is in position


def testFilter():
    flt=Filter()

    l=[5,1,0,9,8,3]
    r=flt.filter(l,lambda a:a<6)
    assert len(r)==4
    #print "\nElements less than 6 in "+str(l)
    #print r

    l=[5,1,0,9,8,3]
    r=flt.filter(l,lambda a:a>3)
    assert len(r)==3
    #print "\nElements greater than 3 in "+str(l)
    #print r

    l=[5,1,0,9,0,8,0]
    r=flt.filter(l,lambda a:a==0)
    assert len(r)==3
    #print "\nElements equal to 0 in "+str(l)
    #print r

def testSearch():
    list=[3,2,6,4,8,2,0]
    src=Searcher()

    assert src.isinList(list,3)==True
    assert src.isinList(list,1)==False
    assert src.isinList(list,8)==True

    assert src.findIndex(list,3)==0
    assert src.findIndex(list,1)==-1
    assert src.findIndex(list,0)==6

    assert src.findElement(list,3)==3
    assert src.findElement(list,6)==6
    assert src.findElement(list,1)==None

def testSort():
    srt = Sorter()    #sorter object to deal with sorting

    l = [4,6,2,7,1,3,9,0,3,6,3,2,6,7,4]
    ll=[0,2,3]
    assert not isSorted(l)
    assert isSorted(ll)

    l = [4,6,2,7,1,3,9,0,3,6,3,2,6,7,4]
    #print "\nBubble Sort explicit compare function call\n"
    #print l
    bl=srt.bubbleSort(l,compareFunction)    #using explicit boolean function
    assert isSorted(bl)
    #print bl

    l = [4,6,2,7,1,3,9,0,3,6,3,2,6,7,4]
    #print "\nBubble Sort lambda call\n"
    #print l
    bl=srt.bubbleSort(l,lambda a,b:a>b)     #call with lambda function
    assert isSorted(bl)
    #print bl

    l = [3,2,1,8,3,5,4,6,1,7,0,2,5,6,3]
    #print "\nShellSort lambda call\n"
    #print l
    bl=srt.shellSort(l,lambda a,b:a>b)
    assert isSorted(bl)
    #print bl

    l = [4,6,2,7,1,3,9,0,3,6,3,2,6,7,4]
    #print "\nInsertSort lambda call\n"
    #print l
    bl=srt.insertSort(l,lambda a,b:a>b)
    assert isSorted(bl)
    #print bl

    l = [4,6,2,7,1,3,9,0,3,6,3,2,6,7,4]
    #print "\nCombSort lambda call\n"
    #print l
    bl=srt.combSort(l,lambda a,b:a>b)
    assert isSorted(bl)
    #print bl

    l = [4,6,2,7,1,3,9,0,3,6,3,2,6,7,4]
    #print "\nBubbleSort recursive\n"
    #print l
    bl=srt.rSort(l,0,len(l)-1,lambda a,b:a>b)
    assert isSorted(l)
    #print bl

testFilter()
testSearch()
testFilter()