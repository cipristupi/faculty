'''
Created on Oct 31, 2013

@author: Ciprian-Viorel Stupinean 
P5. Name and address book (NAB) management
In a NAB the following information may be stored:
 persons: <personID>, <name>, <phone number>, <address>
 activities: <personID>, <date>, <time>, <description>
Create an application which allows to:
 manage the list of persons and activities.
 add, remove, update and list persons and activities
 search for a person; search for an activity.
 searching a person based on his/her name, and search for an activity on a given date.
 create statistics: list of activities for a person ordered: alphabetically, by their date; list of
persons having activities in a certain interval [date1, date2], ordered according to different
criteria (date1, alphabetic order of the description), etc
'''

'''
from domain.activity import *
from domain.person import *
'''
from ui.ui import *
from repository.activityRepository import *
from repository.personRepository import *
from repository.personFileRepository import *
from repository.activityFileRepository import *
from controller.activityController import *
from controller.personController import *
from domain.validator import *

personValidator=PersonValidator()
activityValidator=ActivityValidator()

#repo_activity = ActivityRepository()
#repo_person = PersonRepository(personValidator)

repo_activity_file= FileActivityRepository("activity.txt")#activity
repo_person_file= FilePersonRepository("person.txt",personValidator)


activity_controller = ActivityController(repo_activity_file)
person_controller= PersonController(repo_person_file)

'''person_controller.testAddInit()
activity_controller.testAddInit()'''

'''repo_activity_file.__storeToFile()
repo_person_file.__storeToFile()
'''


#ends controller initialize of data

'''activity_controller.testCoord()
person_controller.testCoord()
#ends controller tests

repo_activity.testCoord()#Work
repo_person.testCoord()#Work
#ends repository tests'''

ui = UI(activity_controller,person_controller)
ui.mainMenu()

'''
app->ui->controller->repository->domain
'''

