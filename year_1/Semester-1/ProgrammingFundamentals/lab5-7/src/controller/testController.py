from domain.person import *
from domain.activity import *
from domain.validator import PersonValidator
from datetime import datetime
from activityController import *
from personController import *
from repository.personRepository import *
from repository.activityRepository import *
import unittest
from unittest import TestCase
from datetime import datetime

class PersonTestCase(TestCase):
    def setUp(self):
        persVal=PersonValidator()
        persRepo=PersonRepository(persVal)
        self.persCont= PersonController(persRepo)
        self.pers=Person(10,"person1","0741111111","address1")
        self.persCont.addPerson(10, "person1","0741111111","address1")
        self.persCont.testAddInit()
        
    def tearDown(self):
        pass
    
    def testAddPerson(self):
        self.persCont.addPerson(15, "name", "phone", "address")
        pers2=Person(15,"name","phone","address")
        self.assertEqual(self.persCont.getPersonByID(15),pers2)
        
    def testGetAllPersons(self):
        self.assertEqual(len(self.persCont.getAllPersons()),4)
        
    def testGetPersonByID(self):
        self.assertEqual(self.persCont.getPersonByID(10),self.pers)
        self.assertEqual(self.persCont.getPersonByID(25),None)
        
    def testGetByName(self):
        self.assertEqual(self.persCont.getPersonByName("person1"),self.pers)
        self.assertEqual(self.persCont.getPersonByName("something"),None)
        
    def testDeleteByID(self):
        self.persCont.deleteByID(10)
        p=self.persCont.getAllPersons()
        self.assertFalse(self.pers in p)
        
    def testSets(self):
        self.persCont.setNewName(10, "name")
        self.persCont.setNewPhone(10, "phone")
        self.persCont.setNewAddress(10, "address")
        p=self.persCont.getPersonByID(10)
        self.assertEqual(p.getName(),"name")
        self.assertEqual(p.getPhone(),"phone")
        self.assertEqual(p.getAddress(),"address")
        
        
class ActivityTestCase(TestCase):
    def setUp(self):
        actRepo=ActivityRepository()
        self.actCont=ActivityController(actRepo)
        self.actCont.testAddInit()
        dt=datetime.strptime("13.10.2013",'%d.%m.%Y').date()
        t=datetime.strptime("15:30",'%H:%M').time()
        self.act=Activity(1,dt,t,"Something")
        self.actCont.addActivity(1, dt, t, "Something")
        pass
    def tearDown(self):
        pass
    
    def testAddActivity(self):
        dt=datetime.strptime("01.01.1991","%d.%m.%Y").date()
        t=datetime.strptime("00:00","%H:%M").time()
        self.actCont.addActivity(1,dt,t,"test")
        a=Activity(1,dt,t,"test")
        self.assertEqual(self.actCont.getActivity(1, dt, t),a)
        
    def testGetActivity(self):
        dt=datetime.strptime("13.10.2013",'%d.%m.%Y').date()
        t=datetime.strptime("15:30",'%H:%M').time()

        self.assertEqual(self.actCont.getActivity(1, dt, t),self.act)
        dt=datetime.strptime("01.10.1001","%d.%m.%Y").date()
        t=datetime.strptime("11:11","%H:%M").time()
        self.assertEqual(self.actCont.getActivity(50, dt, t),None)
        
    def testGetAll(self):
        self.assertEqual(len(self.actCont.getAllActivities()),9)
        
    def testGetByID(self):
        a=self.actCont.getAllActivitiesByID(1)
        self.assertEqual(len(a),5)
        
    def testGetByInterval(self):#Black Box
        dt=datetime.strptime("14.10.2015",'%d.%m.%Y').date()
        dt2=datetime.strptime("15.10.2015",'%d.%m.%Y').date()
        a=self.actCont.getByInterval(dt, dt2)
        self.assertEqual(len(a),0)
        
    def testGetByIntervalID(self):
        dt=datetime.strptime("14.10.2015",'%d.%m.%Y').date()
        dt2=datetime.strptime("15.10.2015",'%d.%m.%Y').date()
        a=self.actCont.getByInterval(dt, dt2)
        self.assertEqual(len(a),0)
    
    def testDeleteByID(self):
        self.actCont.deleteByID(1)
        a=self.actCont.getAllActivitiesByID(1)
        self.assertEqual(len(a),0)
        
    def testDeleteBy(self):
        dt=datetime.strptime("13.10.2013",'%d.%m.%Y').date()
        t=datetime.strptime("15:30",'%H:%M').time()
        self.actCont.deleteBy(1, dt, t)
        self.assertEqual(self.actCont.getActivity(1, dt, t),None)
        
    def testSets(self):
        dt=datetime.strptime("13.10.2013",'%d.%m.%Y').date()
        t=datetime.strptime("15:30",'%H:%M').time()
        ndt=datetime.strptime("01.01.2000",'%d.%m.%Y').date()
        nt=datetime.strptime("20:20",'%H:%M').time()
        self.actCont.setNewDescription(1,dt,t,"Text Changed")
        self.actCont.setNewDate(1,dt,t,ndt)
        self.actCont.setNewTime(1,ndt,t,nt)
        a=Activity(1,ndt,nt,"Text Changed")
        b= self.actCont.getActivity(1, ndt, nt)
        self.assertEqual(a,b)
        
    
    