from domain.person import *

#All functions are tested
#All functions are tested with PyUnit

class PersonController:

    def __init__(self,personRepository):
        self.__personRepository=personRepository
        
    def addPerson(self,personID,name,phone,address):#T/P
        '''
        Description:Adds a person
        Input: personID,name,phone,address
        Raises: RepositoryException if there is another person with the same id
                ValidatorException if the person is not valid.
        Preconditions: the input date to be valid 
        '''
        person=Person(personID,name,phone,address)#create a person object
        self.__personRepository.addPerson(person)
        
    def getAllPersons(self):#T/P
        '''
        Description:Gets the list of all persons
        Output: a list of persons
        Postconditions: returns all person from the list
        '''
        return self.__personRepository.getAll()
    
    def getPersonByID(self,persID):#T/P
        '''
        Description: Gets a person
        Output: a person
        Postconditions: returns the person with the given ID
        '''
        return self.__personRepository.findByID(persID)
    
    def getPersonByName(self,name):#T/P
        '''
        Description: get a person by name
        Input: name after we search
        Output: a person with the given name or none if doesn't exist
        Postconditions: returns the person with the given name or none if doesn't exist
        '''
        return self.__personRepository.findByName(name)
    
    def deleteByID(self,persID):#T/P
        '''
        Description: delete a person with a given id
        Input: the persID
        Output: 
        Preconditions: persID must be a valid person ID
        '''
        self.__personRepository.delete(persID)
        
    def setNewName(self,persID,name):#T/P
        '''
        Description: set a new name to a person
        Input: the persID, the new name
        Output: 
        Preconditions: the person with the given ID to exist
        '''
        self.__personRepository.setNewName(persID,name)
        
    def setNewAddress(self,persID,address):#T/P
        '''
        Description: set a new address to a person
        Input: the persID, the new address
        Output: 
        Preconditions: the person with the given ID to exist
        '''
        self.__personRepository.setNewAddress(persID,address)
        
    def setNewPhone(self,persID,phone):#T/P
        '''
        Description: set a new phone to a person
        Input: the persID, the new phone
        Output: 
        Preconditions: the person with the given ID to exist
        '''
        self.__personRepository.setNewPhone(persID,phone)
        
    def getListPersID(self,IDList):#T
        '''
        Description:  for a set of IDs get the list of persons
        Input: set of IDs
        Output: set of persons
        Postconditions: returns a list of ID , ID for each person
        '''
        a=[]
        for i in IDList:
            p=self.getPersonByID(i)
            if p!=None:
                a.append(p)
        return a
    
    def getListPersIDOrder(self,IDList):#T
        '''
        Description: for a set of IDs get the list of persons ordered
        Input: set of IDs
        Output: set of persons ordered
        Postconditions: returns the list of person with the ID's from list and sort it
        '''
        a=[]
        for i in IDList:
            p=self.getPersonByID(i)
            if p!=None:
                a.append(p)
        
        a.sort(key=lambda p: p.getName(), reverse=False)
        return a
        
    def testAddInit(self):
        person=Person(1,"Gigel","0741409706","Somewhere")#create a person object
        self.__personRepository.addPerson(person)
        person=Person(2,"Costel","0740000001","Acasa")#create a person object
        self.__personRepository.addPerson(person)
        person=Person(3,"Fuego","0711111111","Acasa la mama")#create a person object
        self.__personRepository.addPerson(person)
        
    def testCoord(self):
        self.testAdd()
        self.testDelete()
        self.testGetAll()
        self.testGetName()
        self.testSets()
        
    def testAdd(self):
        #person=Person(4,"GigeL Hackerul","0741479999","Rusia")
        self.addPerson(4,"GigeL Hackerul","0741479999","Rusia")
        a=self.getPersonByID(4)
        assert(a.getID()==4)
        
    def testGetAll(self):
        l=self.getAllPersons()
        assert(len(l)==3)
        
    def testGetName(self): 
        #person=Person(10,"Gigel","0741409706","Somewhere")
        a=self.getPersonByName("Gigel")
        assert(a.getName()=="Gigel")
        
    def testDelete(self):
        self.deleteByID(3)
        a=self.getPersonByID(3)
        assert(a==None)
        
    def testSets(self):
        self.setNewAddress(1, "ASDF")
        self.setNewName(1, "Gigel Hackerul")
        self.setNewPhone(1, "Unknown")
        a=self.getPersonByID(1)
        assert(a.getName()=="Gigel Hackerul")
        assert(a.getAddress()=="ASDF")
        assert(a.getPhone()=="Unknown")
        