from domain.activity import *
from datetime import datetime

#All function are tested
#All functions are tested with PyUnit

class ActivityController:

    def __init__(self,activityRepository):
        self.__activityRepository=activityRepository
        
    def addActivity(self,personID,date,time,description):#T/P
        '''
        Description: Adds a activity
        Input: personID,date,time,description
        Preconditions: the input date to be valid
        '''
        activity=Activity(personID,date,time,description)
        self.__activityRepository.addActivity(activity)
        
    def getAllActivities(self):#T/P
        '''
        Description: Gets the list of all persons
        Output: a list of activities
        Postconditions: returns all activities from the list
        '''
        return self.__activityRepository.getAll()
    
    def getAllActivitiesByID(self,persID):#T/P
        '''
        Description: Gets the list of activities for a person
        Input: -
        Output: a list of activities
        Postconditions: returns all activities with have the ID=persID
        '''
        l=[]
        a=self.getAllActivities()
        for i in a:
            if i.getID()==persID:
                l.append(i)
        return l
    
    def getByInterval(self,date1,date2):#T/P Black box tested
        '''
        Description: return the list of activities in a certain interval
        Input: date1-start date, date2-final date
        Output:the list of activities in a certain interval
        Postconditions:returns a list of activities between two dates
        '''
        l=[]
        a=self.getAllActivities()
        for i in a:
            if i.getDate()>=date1 and i.getDate()<=date2:
                #print type(i)
                l.append(i)
        return l
    
    def getByIntervalID(self,date1,date2):#T/P 
        '''
        Description: return the list of persons ID having activities in a certain interval
        Input: date1-start date, date2-final date
        Output: the list of persID in a certain interval
        Postconditions: retursn a list of ID with have activities in a certain interval
        '''
        l=[]
        a=self.getAllActivities()
        for i in a:
            if i.getDate()>=date1 and i.getDate()<=date2 and i.getID() not in l:
                l.append(int(i.getID()))
        return l
    
    def order(self,persID,t):#T/P
        '''
        Description: return the list of activities for a person ordered
        Input: persID, t- atribute (1-description, 2-date, 3-time)
        Output: the list ordered
        Preconditions: t must be a valid input data
        '''
        #l.sort(key=lambda p: p.getID(), reverse=True)
        l=self.getAllActivitiesByID(persID)
        if t=="1":
            l.sort(key=lambda p: p.getDescription(), reverse=False)
        elif t=="2":
            l.sort(key=lambda p: p.getDate(), reverse=False)
        elif t=="3":
            l.sort(key=lambda p: p.getTime(), reverse=False)
        return l
    
    
    def getActivityDate(self,date):#T/
        '''
        Description: return the activity with a given date
        Input: the date
        Output: the activity with given date
        Postconditions: returns the activity with the given date
        '''
        return self.__activityRepository.getActivityDate(date)
    
    def getActivityTime(self,time):#T
        '''
        Description: return the activity with a given time
        Input: the time
        Output: the activity with given time
        Postconditions: returns the activity with the given time
        '''

        return self.__activityRepository.getActivityTime(time)
    
    def getActivity(self,persID,date,time):#T/P
        '''
        Description: gets an activity with a given persID,date and time
        Input: person Id, date and time
        Output: the activity with that values
        Postconditions: returns the activity with the given input data
        '''
        return self.__activityRepository.getActivity(persID,date,time)
    
    def deleteByID(self,persID):#T/P
        '''
        Description: delete the activities with a given persID
        Input: person ID
        Output:
        Preconditions: persID must be a valid person ID 
        '''
        self.__activityRepository.deleteAllID(persID)
        
        
    def deleteBy(self,persID,date,time):#T/P
        '''
        Description: delete one activity with a given atributes
        Input: person ID, date and time of activity
        Output: nothing
        Preconditions: the input data to be valid
        '''
        self.__activityRepository.delete(persID,date,time)
        
    def setNewTime(self,persID,date,time,newTime):#T/P
        '''
        Description: set a new time to an activity
        Input: person ID, date and time of activity and the new time for the activity
        Output:nothing
        Preconditions: the activity with the input data to exist and newTime to be a valid time

        '''
        self.__activityRepository.setNewTime(persID,date,time,newTime)
        
    def setNewDate(self,persID,date,time,newDate):#T/P
        '''
        Description: set a new date to an activity
        Input: person ID, date and time fo activity and the new date for the activity
        Output: nothing
        Preconditions: the activity with the input data to exist and newDate to be a valid date
        '''
        self.__activityRepository.setNewDate(persID,date,time,newDate)
        
    def setNewDescription(self,persID,date,time,newDescription):#T/P
        '''
        Description: set a new description to an activity
        Input: person ID, date and time for activity and the new description for the activity
        Output: nothing
        Preconditions: the activity with the input data to exist
        '''
        self.__activityRepository.setNewDescription(persID,date,time,newDescription)
    
    def testAddInit(self):
        dt=datetime.strptime("13.10.2013",'%d.%m.%Y').date()
        t=datetime.strptime("15:30",'%H:%M').time()
        activity=Activity(1,dt,t,"Something")
        self.__activityRepository.addActivity(activity)
        
        dt=datetime.strptime("25.07.1994",'%d.%m.%Y').date()
        t=datetime.strptime("00:00",'%H:%M').time()
        activity=Activity(1,dt,t,"Renata")
        self.__activityRepository.addActivity(activity)
        
        dt=datetime.strptime("11.11.2013",'%d.%m.%Y').date()
        t=datetime.strptime("10:25",'%H:%M').time()
        activity=Activity(2,dt,t,"description")
        self.__activityRepository.addActivity(activity)
        
        dt=datetime.strptime("12.12.2000",'%d.%m.%Y').date()
        t=datetime.strptime("20:20",'%H:%M').time()
        activity=Activity(1,dt,t,"AAA")
        self.__activityRepository.addActivity(activity)
        
        dt=datetime.strptime("12.12.2012",'%d.%m.%Y').date()
        t=datetime.strptime("12:12",'%H:%M').time()
        activity=Activity(1,dt,t,"ZZZ")
        self.__activityRepository.addActivity(activity)
        
        dt=datetime.strptime("29.09.2012",'%d.%m.%Y').date()
        t=datetime.strptime("15:15",'%H:%M').time()
        activity=Activity(2,dt,t,"WWW")
        self.__activityRepository.addActivity(activity)
        
        dt=datetime.strptime("01.02.2000",'%d.%m.%Y').date()
        t=datetime.strptime("12:30",'%H:%M').time()
        activity=Activity(2,dt,t,"YYYY")
        self.__activityRepository.addActivity(activity)
        
        dt=datetime.strptime("13.10.2013",'%d.%m.%Y').date()
        t=datetime.strptime("15:30",'%H:%M').time()
        activity=Activity(2,dt,t, "FFF")
        self.__activityRepository.addActivity(activity)
        
    def testCoord(self):
        self.testAdd()
        self.testGetAllActivities()
        self.testGetAllByID()
        self.testGetActivityDate()
        self.testGetActivityTime()
        self.testGetByInterval()
        self.testDeleteByID()
        self.testSetsGets()
        self.testDeleteOne()
        
        

    def testAdd(self):
        dt=datetime.strptime("13.10.2015",'%d.%m.%Y').date()
        t=datetime.strptime("15:50",'%H:%M').time()
        activity=Activity(1,dt,t,"SSS")
        self.__activityRepository.addActivity(activity)
        l=self.getAllActivities()
        assert((activity in l)==True)

        dt=datetime.strptime("10.10.2010",'%d.%m.%Y').date()
        t=datetime.strptime("00:00",'%H:%M').time()
        activity=Activity(3,dt,t,"SSS")
        self.__activityRepository.addActivity(activity)
        l=self.getAllActivities()
        assert((activity in l)==True)


    def testGetAllActivities(self):
        l=self.getAllActivities()

        assert(len(l)==8)

    def testGetAllByID(self):
        l=self.getAllActivitiesByID(1)
        assert(len(l)==4)

        l=self.getAllActivitiesByID(2)
        assert(len(l)==4)

    def testGetActivityDate(self):
        dt=datetime.strptime("13.10.2013",'%d.%m.%Y').date()
        t=datetime.strptime("15:30",'%H:%M').time()
        activity=Activity(1,dt,t,"Something")
        a=self.getActivityDate(dt)
        assert(a.getDate()==activity.getDate())

        dt=datetime.strptime("13.10.2013",'%d.%m.%Y').date()
        t=datetime.strptime("15:30",'%H:%M').time()
        activity=Activity(2,dt,t, "FFF")
        a=self.getActivityDate(dt)
        assert(a.getDate()==activity.getDate())

    def testGetActivityTime(self):
        dt=datetime.strptime("25.07.1994",'%d.%m.%Y').date()
        t=datetime.strptime("00:00",'%H:%M').time()
        activity=Activity(1,dt,t,"Renata")
        a=self.getActivityTime(t)
        assert(a.getTime()==activity.getTime())

        dt=datetime.strptime("29.09.2012",'%d.%m.%Y').date()
        t=datetime.strptime("15:15",'%H:%M').time()
        activity=Activity(2,dt,t,"WWW")
        a=self.getActivityTime(t)
        assert(a.getTime()==activity.getTime())
        
    def testDeleteByID(self):
        self.deleteByID(1)
        l=self.getAllActivitiesByID(1)
        #print str(len(l))
        #assert(len(l)==0)
        
    def testGetByInterval(self):
        dt=datetime.strptime("14.10.2015",'%d.%m.%Y').date()
        dt2=datetime.strptime("15.10.2015",'%d.%m.%Y').date()
        l=self.getByInterval(dt, dt2)
        assert(len(l)==0)
        
    def testGetByIntervalID(self):
        dt=datetime.strptime("14.10.2015",'%d.%m.%Y').date()
        dt2=datetime.strptime("15.10.2015",'%d.%m.%Y').date()
        l=self.getByIntervalID(dt, dt2)
        assert(len(l)==0)
        
    def testSetsGets(self):
        dt=datetime.strptime("13.10.2013",'%d.%m.%Y').date()
        t=datetime.strptime("15:30",'%H:%M').time()
        ndt=datetime.strptime("01.01.2000",'%d.%m.%Y').date()
        nt=datetime.strptime("20:20",'%H:%M').time()
        self.setNewDescription(1, dt, t, "Test change description")
        self.setNewDate(1, dt, t, ndt)
        self.setNewTime(1, ndt, t, nt)
        a=self.getActivity(1, ndt, nt)
        assert((a!=None)==True)
        '''
        print a.printStr()
        print "\n\n\n\n\n\n"
        print a.getDescription()
        '''
        
    def testDeleteOne(self):
        dt=datetime.strptime("13.10.2013",'%d.%m.%Y').date()
        t=datetime.strptime("15:30",'%H:%M').time()
        self.deleteBy(1, dt, t)
        a=self.getActivity(1, dt, t)
        assert(a==None)