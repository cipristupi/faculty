from domain.activity import *
from datetime import datetime

#All function are tested

class ActivityRepository:
    def __init__(self):
        self.__data=[]
        
    def addActivity(self,activity):#T/P
        self.__data.append(activity)
        
    def getAll(self):#T/P
        '''
        Description: Gets the list of activities
        Output: the list of all activities
        Postconditions: returs the list of activities
        '''
        return self.__data
    
    
    def deleteAllID(self,persID):#T/P
        '''
        Description: delete all activities with a given person ID
        Input: persID - person ID
        Output:-
        Preconditions:  persID -must be a non-negative integer, and the ID to exist into the list
        '''
        l=[]
        i=0
        while i<len(self.__data):
            if self.__data[i].getID()!=persID:
                l.append(self.__data[i])
            i=i+1
            
        self.__data=l[:]
    
    def delete(self,persID,date,time):#T/P
        '''
        Description: Delete a activity with a given id and date
        Input: persID-person id, date- date type, time-time type
        Output:
        Preconditions: persID-must be non-negative integer and the ID to exist into the list, and the activity with the given date and time to exist
        '''
        i=0
        while i<len(self.__data):
            if self.__data[i].getID()==persID and self.__data[i].getDate()==date and self.__data[i].getTime()==time:
                self.__data.pop(i)
            i=i+1
            
    def getActivityDate(self,date):#T/P
        '''
        Description: Gets the activity with a given date
        Input: date- a date
        Output: the activity with that date or none if don't exist
        Postconditions: returns the activity with a given date
        '''
        for i in self.__data:
            if i.getDate()==date:
                return i
        return None
    
    def getActivityTime(self,time):#T/P
        '''
        Description: Gets the activity with a given time
        Input: time - a time 
        Output:  the activity with that date or none if don't exist
        Postconditions: returns the activity with a given time
        '''
        for i in self.__data:
            if i.getTime()==time:
                return i
        return None
    
    def getActivity(self,persID,date,time):#T/P
        '''
        Description: gets an activity with a given persID,date and time
        Input: person Id, date and time
        Output: the activity with that values or none if don't exist
        Postconditions: returns the activity with a given persID, date, time
        '''
        for i in self.__data:
            if i.getDate()==date and i.getTime()==time and i.getID()==persID:
                return i
        return None
    
    def setNewTime(self,persID,date,time,newTime):#T/P
        '''
        Description: set a new time to an activity
        Input: the person id, old time of activity and the new one
        Output:-
        Preconditions: the activity with the given persID,date,time to exist, and newTime to be a valid time
        '''
        for i in self.__data:
            if i.getID()==persID and i.getTime()==time and i.getDate()==date:
                i.setTime(newTime)
    def setNewDate(self,persID,date,time,newDate):#T/P
        '''
        Description: set a new date to an activity
        Input: the person id, old date of activity and the new one
        Output:
        Preconditions: the activity with the given persID,date,time to exist and newDate to be a valid date
        '''
        for i in self.__data:
            if i.getID()==persID and i.getTime()==time and i.getDate()==date:
                i.setDate(newDate)
    
    def setNewDescription(self,persID,date,time,newDescription):#T/P
        '''
        Description: set a new description to an activity
        Input: the person id and the new description
        Ouput:-
        Preconditions:the activity with the given persID, date,time to exist 
        '''
        for i in self.__data:
            if i.getID()==persID and i.getTime()==time and i.getDate()==date:
                i.setDescription(newDescription)
                
    def testCoord(self):
        self.testAddActivity()
        self.testGetActivityDate()
        self.testGetActivityTime()
        self.testGetAll()
        self.testDelete()
        self.testSetsGets()
        self.testDeleteOne()
        
    
    def testGetAll(self):
        a=self.getAll()
        #print str(len(a))
        assert(len(a)==8)
        
    def testAddActivity(self):
        dt=datetime.strptime("13.10.2013",'%d.%m.%Y').date()
        t=datetime.strptime("15:30",'%H:%M').time()
        activity=Activity(1,dt,t,"Something")
        self.addActivity(activity)
        assert((activity in self.__data)==True)
        
    def testGetActivityDate(self):
        dt=datetime.strptime("11.11.2011",'%d.%m.%Y').date()
        assert(self.getActivityDate(dt)==None)
        
    def testGetActivityTime(self):
        t=datetime.strptime("11:11",'%H:%M').time()
        assert(self.getActivityTime(t)==None)
        
    def testDelete(self):
        self.deleteAllID(1)
        l=self.getAll()
        ll=[]
        #print str(len(l))
        for i in l:
            if i.getID()==1:
                ll.append(i)
        assert(len(ll)==0)
                
    def testSetsGets(self):
        dt=datetime.strptime("13.10.2013",'%d.%m.%Y').date()
        t=datetime.strptime("15:30",'%H:%M').time()
        ndt=datetime.strptime("01.01.2000",'%d.%m.%Y').date()
        nt=datetime.strptime("20:20",'%H:%M').time()
        self.setNewDescription(1, dt, t, "Test change description")
        self.setNewDate(1, dt, t, ndt)
        self.setNewTime(1, ndt, t, nt)
        a=self.getActivity(1, ndt, nt)
        assert((a!=None)==True)
        #print a.printStr()
        #print "\n\n\n\n\n\n"
        #print a.getDescription()
        
    def testDeleteOne(self):
        #test for deleting one activity
        dt=datetime.strptime("13.10.2013",'%d.%m.%Y').date()
        t=datetime.strptime("15:30",'%H:%M').time()
        self.delete(1, dt, t)
        a=self.getActivity(1, dt, t)
        #print a.printStr()
        assert(a==None)
        
            
    
        