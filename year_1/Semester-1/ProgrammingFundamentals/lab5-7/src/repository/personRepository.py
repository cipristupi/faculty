from domain.person import *
from domain.activity import *
from domain.validator import PersonValidator
from activityRepository import *

#All functions are tested
#T-come from test and P come from PyUnit Test

class RepositoryException(Exception):
    
    def __init__(self,errors):
        '''
        Constructor
        '''
        self.errors = errors
    

    def getErrors(self):
        '''
        Getter for errors.
        '''
        return self.errors
    
class PersonRepository:
    
    def __init__(self,personValidator):
        self.__data=[]
        self.__personValidator=personValidator
        
    def addPerson(self,pers):#T/P
        '''
        Description:add a new person
        Input: pers-person
        Output:
        Raise RepositoryException if the person exist 
        Preconditions: pers must be a valid person
        '''
        if self.containsPerson(pers):
            raise RepositoryException("Person with ID " + str(pers.getID()) + "already exists!")
        self.__personValidator.validate(pers)
        self.__data.append(pers)
        
    def getAll(self):#T/P
        '''
        Description:Gets the list of persons.
        Output:The list of persons
        Postconditions: returns all person from the list
        '''
        return self.__data
    
    def findByID(self,persID):#T/P
        '''
        Description: Find a person by id
        Input: persID: the id of person
        Output: the founded person or None if the person was not found
        Postconditions: returns the person with the given persID, or none if doesn't exists
        '''
        '''
        Complexity
        =>Best case  =Worst Case =  Average Case = O(n) liniar algorithm
        '''
        for p in self.__data:
            #print "here person"
            #print str(type(p.getID())) + " \n  " + str(type(persID))
            if p.getID()==persID:
                return p
        return None
    
    def findByID_RecInit(self,persID):#Recursive
        '''
        This function initialize the findByID_Rec
        '''  
        return self.findByID_Rec(self.__data,persID, 0)
        
    def findByID_Rec(self,l,persID,i):#Recursive Function
        '''
        Description:Find a person by ID||Recursively implementation
        All specification are the same like the findByID
        '''
        if len(l)==0:
            return None
        elif l[0].getID() == persID:
            return l[0]
        else:
            return self.findByID_Rec(l[1:],persID,i+1)
    
    def findByName(self,name):#T/P
        '''
        Description: Find a person by id
        Input: persID: the id of person
        Output: the founded person or None if the person was not found
        Postconditions: returns the person with the given name, or none if doesn't exists
        '''
        for p in self.__data:
            if p.getName()==name:
                return p
        return None
    
    def delete(self,persID):#T/P
        '''
        Description: The function delete the person with the given id
        Input: persID person id
        Output:
        Preconditions: persID must be a valid person ID and that person to exist into the list
        '''
        #activityRepository.ActivityRepository.deleteAllID(persID)
        i=0
        while i<len(self.__data):
            if self.__data[i].getID()==persID:
                self.__data.pop(i)
            i=i+1
    
    def setNewName(self,persID,name):#T/P
        '''
        Description: set a new name to a person
        Input: the persID, the new name
        Output:
        Preconditions: persID must be a non-negative integer and name to be a non-empty string
        '''
        for i in self.__data:
            if i.getID()==persID:
                i.setName(name)
    
    def setNewAddress(self,persID,address):#T/P
        '''
        Description: set a new address to a person
        Input: the persID, the new address
        Output: 
        Preconditions: persID must be a non-negative integer and address to be a non-empty string
        '''
        for i in self.__data:
            if i.getID()==persID:
                i.setAddress(address)
        
    def setNewPhone(self,persID,phone):#T/P
        '''
        Description: set a new phone to a person
        Input: the persID, the new phone
        Output: 
        Preconditions: persID must be a non-negative integer and phone to be a non-empty string
        '''
        for i in self.__data:
            if i.getID()==persID:
                i.setPhone(phone)
        
    
    def containsPerson(self,person):#T/P
        '''
        Description: Checks if there is another person with the same id
        Input: person:  a person 
        Output: True if another person with the same id is found, False otherwise
        Postconditions: returns True if the person exist into the list or False if doesn't exist
        '''
        for p in self.__data:
            if p.getID()==person.getID():
                return True
        return False
    
    def containPersonRecInit(self,person):#The second recursive function
        return self.containsPersonRec(person,self.__data)
    
    def containsPersonRec(self,person,l):
        if len(l)==0:
            return False
        elif l[0]==person:
            return True
        return self.containsPersonRec(person, l[1:])
    
    def testCoord(self):
        #something
        self.testContains()
        self.testAdd()
        self.testDelete()
        self.testGetAll()
        self.testFindID()
        self.testFindName()
               
    def testContains(self):
        person=Person(1,"Gigel","0741409706","Somewhere")
        assert(self.containsPerson(person)==True)
        person=Person(10,"FFFF","0741409706","Somewhere")
        assert(self.containsPerson(person)==False)
        
    def testAdd(self):
        person=Person(12,"Gigel Hackerul","0741sterge","Somewhere")
        self.addPerson(person)
        assert((person in self.__data)==True)
        
    def testDelete(self):
        person=Person(1,"Gigel","0741409706","Somewhere")
        self.delete(1)
        assert((person in self.__data)==False)
        
    def testGetAll(self):
        l=self.getAll()
        assert(len(l)==3)
        
    def testFindID(self):#Problems Here
        #person=Person(1,"Gigel","0741409706","Somewhere")
        a=self.findByID(1)
        #print a.printStr()
        assert(a!=None)
        
    def testFindName(self):
        #person=Person(1,"Gigel","0741409706","Somewhere")
        a=self.findByName("Gigel Hackerul fdds")
        assert(a == None)
        
        
    def testSets(self):
        self.setNewAddress(1, "ASDF")
        self.setNewName(1, "Gigel Hackerul")
        self.setNewPhone(1, "Unknown")
        a=self.findByID(1)
        assert(a.getName()=="Gigel Hackerul")
        assert(a.getAddress()=="ASDF")
        assert(a.getPhone()=="Unknown")
        
        
    
        