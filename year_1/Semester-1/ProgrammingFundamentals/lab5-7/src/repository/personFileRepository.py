from domain.person import *
from repository.personRepository import *

class FilePersonRepository(PersonRepository):
	def __init__(self,filepath,personValidator):
		PersonRepository.__init__(self, personValidator)
		self.__filename=filepath
		self.__loadFile()
		#self.__storeToFile()
		
	def __loadFile(self):
		'''
		Description: Loads data from txt file
		Input: -
		Output: list of person is fill into the memory repository
		Postcondition: fill the list of persons 
		'''
		try:
			f=open(self.__filename,"r")
		except IOError as ex:
			print "Error: " + str(ex)
		
		line=f.readline().strip()
		while line!="":
			t=line.split("|")#|
			p=Person(int(t[0]),t[1],t[2],t[3])
			PersonRepository.addPerson(self, p)
			line=f.readline().strip()
			
			
	def __storeToFile(self):
		'''
		Description: Write the list of person into file
		Input: list of person is get from memory repository
		Output:
		Postcondition: write the list of person into file
		'''
		f=open(self.__filename,"w")
		pers=PersonRepository.getAll(self)
		for p in pers:
			pf=str(p.getID()) + "|" +p.getName() +"|"  + p.getPhone() + "|" + p.getAddress() +"\n"
			f.write(pf)
		f.close()
		
	def addPerson(self,person):
		'''
		Add a person to the list
		Input:  person-a person
		Output:
		Preconditions: person must be a Person item
		
		'''
		PersonRepository.addPerson(self, person)
		self.__storeToFile()
	
	def delete(self,persID):
		'''
		Delete a person with a given ID
		Input: persID - integer, represent the ID of an person
		Output:
		Preconditions: persID - must be an integer and an valide person id
		'''
		PersonRepository.delete(self,persID)
		self.__storeToFile()
		
	def setNewName(self,persID,name):
		'''
		Set a new name to a given person
		Input: persID - integer, represent person ID, name-string, the new name for the given person
		Preconditions: persID must be an integer and that person to exist into the list, name- must be a string and non-empty
		'''
		PersonRepository.setNewName(self, persID, name)
		self.__storeToFile()
		
	def setNewAddress(self,persID,address):
		'''
		Set a new address for a given person
		Input: persID -integer , represent person ID, address(string)-the new address for the given person
		Preconditions: persID must be an integer and that person to exist into the list, address must be a string and non-empty
		'''
		PersonRepository.setNewAddress(self, persID, address)
		self.__storeToFile()
		
	def setNewPhone(self,persID,phone):
		'''
		Set a new phone for a given person
		Input: persID-integer, represent person ID, phone -string represent the new phone for the given person
		Preconditions: persID must be an integer and that person to exits into the list, phone must be a string and non-empty
		'''
		PersonRepository.setNewPhone(self, persID, phone)
		self.__storeToFile()
