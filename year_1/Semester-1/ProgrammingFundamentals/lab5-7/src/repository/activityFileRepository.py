from domain.activity import *
from repository.activityRepository import *

class FileActivityRepository(ActivityRepository):
    def __init__(self,filepath):
        ActivityRepository.__init__(self)
        self.__filename=filepath
        self.__loadFile()
        #self.__storeToFile()
        
    def __loadFile(self):
        '''
        Description: loads dat from txt filepath
        Output: list of person is fill into the memory repository
        Preconditions: the file must exist
        '''
        try:
            f=open(self.__filename,"r")
        except IOError as ex:
            print "Error: " + str(ex)
            
        line = f.readline().strip()
        while line!="":
            t=line.split("|")
            #time data '2013-10-13' does not match format '%d.%m.%Y'
            dates=datetime.strptime(t[1],'%Y-%m-%d').date()#datetime.strptime(t_s,'%H:%M').time()
            times=datetime.strptime(t[2],'%H:%M:%S').time()#datetime.strptime(dt_s,'%d.%m.%Y').date()
            a=Activity(int(t[0]),dates,times,t[3])#persID,date,time,description
            ActivityRepository.addActivity(self, a)
            line=f.readline().strip()
        f.close()
            
    def __storeToFile(self):#T/P
        '''
        Description: loads dat from txt filepath
        Input: list of activities is get from memory repository
        Preconditions: list of activities from memory
        '''
        f=open(self.__filename,"w")
        a=ActivityRepository.getAll(self)#list of all activities
        for i in a:
            af= str(i.getID()) + "|" + str(i.getDate()) +"|" + str(i.getTime()) + "|" + i.getDescription() +"\n"
            f.write(af)
        f.close()
        
    def addActivity(self,activity):#T/P
        '''
        Add an activity  to the list
        Input: activity-an new activity
        Preconditions: activity must be an Activity item
        '''
        ActivityRepository.addActivity(self, activity)
        self.__storeToFile()
        
    def deleteAllID(self,persID):#T/P
        '''
        Delete all activities for a given getID
        Input: persID -integer, represent the ID of an person
        Preconditions: persID - must be an non-negative integer and valid person ID
        '''
        ActivityRepository.deleteAllID(self, persID)
        self.__storeToFile()
        
    def delete(self,persID,date,time):#T/P
        '''
        Delete an activities with the given persID,date,time
        Input: persID-integer represent the ID of an person, date-date of activity and time -time of activity
        Preconditions: persID-must be an non-negative integer and an valid person ID, date-date type, time-time type 
        '''
        ActivityRepository.delete(self, persID, date, time)
        self.__storeToFile()
    
    def setNewTime(self,persID,date,time,newTime):
        '''
        Setter for time
        Input: persID-integer represent the ID of an person,date -date of activity , time - time of activity and newTime-the new value for the time
        Preconditions: persID-must be an non-negative integer and an valid person ID, date-date type, time - time type and newTime -time type
        '''
        ActivityRepository.setNewTime(self, persID, date, time, newTime)
        self.__storeToFile()
        
        
    def setNewDate(self,persID,date,time,newDate):
        '''
        Setter for date
        Input: persID-integer represent the ID of an person,date -date of activity , time - time of activity and newDate -the new value for the date
        Preconditions: persID-must be an non-negative integer and an valid person ID, date-date type, time - time type and newDate-date type
        '''
        ActivityRepository.setNewDate(self, persID, date, time, newDate)
        self.__storeToFile()
        
    def setNewDescription(self,persID,date,time,newDescription):
        '''
        Setter for description
        Input: persID-integer represent the ID of an person,date -date of activity , time - time of activity and newDescription-the new value for the description
        Preconditions: persID-must be an non-negative integer and an valid person ID, date-date type, time - time type and newDescription -string
        '''
        ActivityRepository.setNewDescription(self, persID, date, time, newDescription)
        self.__storeToFile()