from activityRepository import *
from activityFileRepository import *
from personFileRepository import *
from personRepository import *
from domain.person import *
from domain.validator import  PersonValidator
from controller.personController import *
from controller.activityController import *
import unittest
from unittest import TestCase

class PersonTestCase(TestCase):#in memory repository
    
    def setUp(self):
        self.persVal=PersonValidator()
        self.persRepo = PersonRepository(self.persVal)
        self.persCont= PersonController(self.persRepo)
        
        self.pers=Person(10,"person1","0741111111","address1")
        self.persRepo.addPerson(self.pers)
        self.persCont.testAddInit()
        
        pass
    def tearDown(self):
        pass
    
    def testFindByID_Rec(self):
        self.assertEqual(self.persRepo.findByID_RecInit(10),self.pers)
        self.assertEqual(self.persRepo.findByID_RecInit(100),None)
        pass
    
    def testAddPerson(self):#HERE
        pers=Person(10,"Test","0741010111","test")
        #self.assertRaises(RepositoryException.self.persVal.validate,pers)#raise repositoryexcept
        #pers1=Person(15,"","","")
        #self.assertRaises(RepositoryException,self.persVal.validate,pers)#raise repositoryexcept
        pers2=Person(15,"person2","0742222222","address2")
        #self.persRepo.addPerson(pers)#add the person
        #self.assertEqual(self.persRepo.findByID(12),pers)
        pass
        
        
    def testGetAll(self):
        self.assertEqual(len(self.persRepo.getAll()),4)
        
    def testFindByID(self):
        self.assertEqual(self.persRepo.findByID(10),self.pers)
        self.assertEqual(self.persRepo.findByID(100),None)
        
    def testFindByName(self):
        self.assertEqual(self.persRepo.findByName("person1"),self.pers)
        self.assertEqual(self.persRepo.findByName("something"),None)
        
    def testDelete(self):
        self.persRepo.delete(10)
        self.assertEqual(self.persRepo.findByID(10),None)
        self.assertEqual(len(self.persRepo.getAll()),3)
        
    def testSetNewName(self):
        self.persRepo.setNewName(10, "name")
        self.persRepo.setNewPhone(10, "phone")
        self.persRepo.setNewAddress(10, "address")
        self.assertEqual(self.persRepo.findByName("name"),self.pers)
        self.assertEqual(self.pers.getPhone(),"phone")
        self.assertEqual(self.pers.getAddress(),"address")
    
    def testContainsPerson(self):
        self.assertTrue(self.persRepo.containsPerson(self.pers), True)
        pers2=Person(15,"person2","0742222222","address2")
        self.assertFalse(self.persRepo.containsPerson(pers2), False)
        
    def testContainsPersonRec(self):
        self.assertTrue(self.persRepo.containPersonRecInit(self.pers), True)
        pers2=Person(15,"person2","0742222222","address2")
        self.assertFalse(self.persRepo.containPersonRecInit(pers2), False)
        
class PersonFileTestCase(TestCase):   
    def setUp(self):
        self.persVal=PersonValidator()
        self.persRepo= FilePersonRepository("person.txt",self.persVal)
        self.pers=Person(10,"person1","0741111111","address1")
        pass
    def tearDown(self):
        pass
    def testGetAll(self):#Test here loadFile
        self.assertEqual(len(self.persRepo.getAll()),5)
        
    def testAddPerson(self):#How the test this function
        '''pers=Person(15,"person2","0742222222","address2")
        self.persRepo.addPerson(pers)#add the person
        self.assertEqual(self.persRepo.findByID(15),pers)'''
        pass
        
    def testSetNew(self):
        self.persRepo.setNewName(10, "name")
        self.persRepo.setNewPhone(10, "phone")
        self.persRepo.setNewAddress(10, "address")
        #self.assertEqual(self.persRepo.findByName("name"),self.pers)#question here
        a=Person(10,"name","phone","address")
        p=self.persRepo.findByID(10)
        self.assertEqual(a,p)
        '''self.assertEqual(p.getPhone(),"phone")
        self.assertEqual(p.getAddress(),"address")
        self.assertEqual(p.getName(),"name")'''
        
class ActivityTestCase(TestCase):
    def setUp(self):
        self.actRepo=ActivityRepository()
        self.actCont=ActivityController(self.actRepo)
        self.actCont.testAddInit()
        dt=datetime.strptime("13.10.2013",'%d.%m.%Y').date()
        t=datetime.strptime("15:30",'%H:%M').time()
        self.act=Activity(1,dt,t,"Something")
        self.actRepo.addActivity(self.act)
        
    def testAddActivity(self):
        dt=datetime.strptime("13.12.2013",'%d.%m.%Y').date()
        t=datetime.strptime("15:35",'%H:%M').time()
        acts=Activity(1,dt,t,"test")
        self.actRepo.addActivity(acts)
        self.assertEqual(self.actRepo.getActivity(1, dt, t),acts)
        
    def testGetAll(self):
        self.assertEqual(len(self.actRepo.getAll()),9)
    
    def testDeleteAllID(self):
        self.actRepo.deleteAllID(1)
        self.assertEqual(len(self.actRepo.getAll()),4)
        
    def testDelete(self):
        self.actRepo.delete(1, self.act.getDate(), self.act.getTime())
        acts=self.actRepo.getAll()
        self.assertFalse(self.act in acts)
        
    def testGetActivityDate(self):
        self.assertEqual(self.actRepo.getActivityDate(self.act.getDate()),self.act)
        dt=datetime.strptime("20.11.2000",'%d.%m.%Y').date()
        self.assertEqual(self.actRepo.getActivityDate(dt),None)
        
    def testGetActivityTime(self):
        self.assertEqual(self.actRepo.getActivityTime(self.act.getTime()),self.act)
        t=datetime.strptime("11:11",'%H:%M').time()
        self.assertEqual(self.actRepo.getActivityTime(t),None)
        
    def testGetActivity(self):
        self.assertEqual(self.actRepo.getActivity(self.act.getID(), self.act.getDate(), self.act.getTime()),self.act)
        dt=datetime.strptime("01.01.1990",'%d.%m.%Y').date()
        t=datetime.strptime("05:05",'%H:%M').time()
        self.assertEqual(self.actRepo.getActivity(1, dt, t),None)
        
    def testSets(self):
        dt=datetime.strptime("13.10.2013",'%d.%m.%Y').date()
        t=datetime.strptime("15:30",'%H:%M').time()
        ndt=datetime.strptime("01.01.2000",'%d.%m.%Y').date()
        nt=datetime.strptime("20:20",'%H:%M').time()
        self.actRepo.setNewDescription(1, dt, t, "Test Changed")
        self.actRepo.setNewDate(1, dt,t,ndt)
        self.actRepo.setNewTime(1,ndt,t,nt)
        a=self.actRepo.getActivity(1, ndt, nt)
        self.assertEqual(a.getDescription(),"Test Changed")
        self.assertEqual(a.getDate(),ndt)
        self.assertEqual(a.getTime(),nt)
        
class ActivityFileTestCase(TestCase):
    def setUp(self):
        #1|2013-12-12|15:15:00|fshajkfsja
        self.actRepo=FileActivityRepository("activity.txt")
        dt=datetime.strptime("12.12.2012",'%d.%m.%Y').date()
        t=datetime.strptime("15:15",'%H:%M').time()
        self.act=Activity(1,dt,t,"fshajkfsja")
        act1=Activity(3,dt,t,"something")
        self.actRepo.addActivity(act1)
        pass
    def tearDown(self):
        pass
    
    def testAddActivity(self):
        pass
        dt=datetime.strptime("01.02.2000",'%d.%m.%Y').date()
        t=datetime.strptime("00:59",'%H:%M').time()
        act=Activity(2,dt,t,"test")
        self.actRepo.addActivity(act)
        a = self.actRepo.getActivity(2, dt, t)
        self.assertEqual(a,act)
        
    def deleteAllID(self):
        self.actRepo.deleteAllID(3)
        a=self.actRepo.getAll()
        ll=[]
        for i in a:
            if i.getID()==3:
                ll.append(i)
                
        self.assertEqual(len(ll),0)
        
    def testDelete(self):
        dt=datetime.strptime("12.12.2012",'%d.%m.%Y').date()
        t=datetime.strptime("15:15",'%H:%M').time()
        self.actRepo.delete(3, dt, t)
        a=self.actRepo.getAll()
        self.assertEqual(len(a),11)
        pass
    
    def testSets(self):
        dt=datetime.strptime("01.02.2000",'%d.%m.%Y').date()
        t=datetime.strptime("00:59",'%H:%M').time()
        ndt=datetime.strptime("01.01.2555",'%d.%m.%Y').date()
        nt=datetime.strptime("20:20",'%H:%M').time()
        self.actRepo.setNewDescription(3, dt, t, "Test Changed")
        self.actRepo.setNewDate(3, dt,t,ndt)
        self.actRepo.setNewTime(3,ndt,t,nt)
        a=self.actRepo.getActivity(3, ndt, nt)
        self.assertEqual(a.getDescription(),"Test Changed")
        self.assertEqual(a.getDate(),ndt)
        self.assertEqual(a.getTime(),nt)
        
if __name__=='__name__':
    unittest.main()
        