from Domain.Word import *

class WordsRepository:
    
    def __init__(self,filepath,outpath):
        self.__filepath=filepath
        self.__outpath= outpath
        self.__data=[]
        self.__loadFile()
        
    def __loadFile(self):#T
        '''
        Load word from the file
        '''
        try:
            f=open(self.__filepath,"r")
        except IOError as ex:
            print "Error " + str(ex)
            
        line = f.readline().strip()
        while line!="":
            wl=line.split("|")#|
            w=Word(int(wl[0]),wl[1],wl[2])
            self.__data.append(w)
            line=f.readline().strip()
            
    def __storeFile(self):#T
        '''
        Write word to the file
        '''
        f=open(self.__filepath,"w")
        for w in self.__data:
            pw = str(w.getID()) +"|" + w.getLang() + "|" + w.getWord() +"\n"
            f.write(pw)
        f.close()
        
    def addWord(self,w):#T
        '''
        Function for adding a new word
        Input: A new word
        Output:
        '''
        self.__data.append(w)
        self.__storeFile()
        
    def getAll(self):#T
        '''
        This function return the list of words
        Output: a list of words
        '''
        return self.__data
    
    def find(self,lang,word):#T
        '''
        This function search after a word
        Input: lang-language and word- a word
        Output: True if the word exist and False otherwise
        '''
        for w in self.__data:
            if w.getLang()==lang and w.getWord()==word:
                return True
        return False