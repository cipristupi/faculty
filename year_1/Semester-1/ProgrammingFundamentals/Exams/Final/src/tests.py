from Domain.Word import *
from Domain.WordValidator import *
from Repository.WordRepository import *
from UI.SpellCheckerController import *
from UI.SpellCheckerUI import *
import unittest
from unittest import TestCase

class MyClass(TestCase):
    def setUp(self):
        self.repo = WordsRepository("int.txt","out.txt")
        self.val = WordValidator()
        self.cont = SpellCheckerController(self.repo,self.val)
        self.spellcheckerui = SpellCheckerUI(self.cont)
        pass
    def tearDown(self):
        pass
    
    def testRead(self):
        l= self.repo.getAll()
        #print len(l)
        #self.assertEqual(l,3) 
        pass   
        
    def testDomain(self):
        w=Word(1,"En","Python")
        self.assertEqual(w.getID(),1)
        self.assertEqual(w.getLang(),"En")
        self.assertEqual(w.getWord(),"Python")
        
    def testAddRepo(self):
        w=Word(1,"En","Python")
        self.repo.addWord(w)
        l=self.repo.getAll()
        self.assertTrue(w in l)
        
    def testFindRepo(self):
        self.assertTrue(self.repo.find("En", "Python"))
        self.assertFalse(self.repo.find("Fr", "aaa"))
        
    def testAddCont(self):
        w=Word(1,"Fr","Pythones")
        #self.cont.addWord(w)
        l=self.cont.getAll()
        #self.assertTrue(w in l)
        
    def testSpellCheckCont(self):
        phare="Ele fac cafele"
        l=self.cont.spellCheck(phare, "Ro")
        self.assertEqual(len(l),3)
        
    