from Domain.Word import *
from Domain.WordValidator import *
from Repository.WordRepository import *
from UI.SpellCheckerController import *
from UI.SpellCheckerUI import *


def main():
    #something
    repo = WordsRepository("int.txt","out.txt")
    val = WordValidator()
    cont = SpellCheckerController(repo,val)
    spellcheckerui = SpellCheckerUI(cont)
    spellcheckerui.main()

main()