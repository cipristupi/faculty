from Domain.Word import *


class WordValidator:

    def __init__(self):
        pass
        
    def validator(self,word,wl):
        '''
        This functio validate the word
        Input: a word which will be validate
                wl-list of words
        Raise: ValueError if the word is empty or exist into the list
        '''
        msg=""
        if word.getID() ==  "" :
            msg = "ID cannont be empty \n"
        if word.getLang() =="":
            msg = msg + "Language not valid"
        if word.getWord() == "":
            msg = msg + "Word cannot be null"
            
        if msg!="":
            raise ValueError(msg)
        else:
            for i in wl:
                if i.getWord() == word.getWord():
                    raise ValueError("Word exist into the list")