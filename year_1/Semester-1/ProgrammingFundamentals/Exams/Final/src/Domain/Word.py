
class Word:


    def __init__(self,id,lang,word):
        '''
        Constructor
        '''
        self.__id=id
        self.__lang=lang
        self.__word=word
        
    def getID(self):
        '''
        Getter for the id
        '''
        return self.__id
    
    def getLang(self):
        '''
        Getter for the language
        '''
        return self.__lang
    def getWord(self):
        '''
        Getter for word
        '''
        return self.__word
    
    def __str__(self):
        return str(self.__id) + "  " + self.__lang + "  " + self.__word