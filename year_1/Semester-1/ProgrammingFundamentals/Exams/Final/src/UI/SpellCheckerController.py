from Domain.WordValidator import *
from Domain.Word import *

class SpellCheckerController:
    
    def __init__(self,wordRepo,wordVal):
        self.__wordRepo=wordRepo
        self.__wordVal=wordVal
        
    def addWord(self,word):#T
        '''
        Function add a new word to the list
        Input: a new Word
        Raise: ValueError
        '''
        wl = self.__wordRepo.getAll()
        try:
            self.__wordVal.validator(word,wl)
            self.__wordRepo.addWord(word)
        except ValueError as er:
            raise ValueError(str(er))
        
    def getAll(self):#T
        '''
        Return the list of word
        '''
        return self.__wordRepo.getAll()
    
    def spellCheck(self,phare,lang):#T
        '''
        This function spell check a given phare
        Input: a phare
        Output: a list of words which are not into the list
        '''
        lp=phare.split(" ")
        ln=[]#list of new words
        for i in lp:
            if self.__wordRepo.find(lang,i)==False:
                ln.append(i)
        return ln
    
    def checkFile(self,intfile,lang,outfile):#Work
        '''
        Spellcheck a given file and output the spelled text
        Input: intfile-input file name, lang-language of the text, outfile=output of the file
        Output: the checket text into the new file
        '''
        try:
            f=open(intfile,"r")
            d=open(outfile,"w")#Good boy this python for creating automaticaly the output file
        except IOError  as ex:
            raise IOError(ex)
        line= f.readline().strip()
        ln=[]
        while line !="":
            wl=line.split(" ")
            for i in wl:
                if self.__wordRepo.find(lang,i)==False:
                    print i
                    i = "{"+i+"}"+" "
                ln.append(i)
            line=f.readline().strip()
            ln.append("\n")
                    
        for i in ln:
            d.write(i)
        d.close
        return "Over"
            