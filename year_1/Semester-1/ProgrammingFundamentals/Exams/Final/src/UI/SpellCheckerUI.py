from Domain.Word import *
from Domain.WordValidator import *

class SpellCheckerUI:
    def __init__(self,spellCont):
        self.__spellCont=spellCont
        
    def main(self):
        '''
        The menu
        '''
        print "1-for add a new word"
        print "2-for spell check a phare"
        print "3-for spell check a file"
        print "4-for printing all words"
        print "0-for exit"
        cmd=raw_input("Command: ")
        while cmd != "0":
            if cmd =="1":#Work
                self.readWord()
                cmd = raw_input("Command: ")
            if cmd=="2":#Work
                self.spellCheck()
                cmd = raw_input("Command: ")
            if cmd =="3":
                self.spellCheckFile()
                cmd = raw_input("Command: ")
            if cmd == "4":#Work
               self.printAll()
               cmd = raw_input("Command: ")
            
            
            
    def readWord(self):
        '''
        Read a words
        '''
        id = raw_input("ID: ")
        langs=raw_input("Language: ")
        word=raw_input("Word: ")
        w=Word(int(id),langs,word)
        try:
            self.__spellCont.addWord(w)
            print "Word Added"
        except ValueError as er:
            print str(er)
            
            
    def printAll(self):
        '''
        print all the word
        '''
        wl = self.__spellCont.getAll()
        for w in wl:
            print str(w)
            
    def spellCheck(self):
        '''
        Check a phare
        '''
        lang=raw_input("Language: ")
        phrase=raw_input("Phare: ")
        a=self.__spellCont.spellCheck(phrase,lang)
        for i in a:
            print "{"+str(i)+"}" + "  ",
        print 
        
    def spellCheckFile(self):
        '''
        Spellcheck an file
        '''
        lang=raw_input("Language: ")
        inputF=raw_input("Input File:")
        outputF=raw_input("Output File:")
        try:
            t=self.__spellCont.checkFile(inputF,lang,outputF)
            if t == "Over":
                print "Spellcheck complete"
        except IOError as er:
            print "Error " + str(er)
            